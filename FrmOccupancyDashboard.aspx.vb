﻿Public Partial Class FrmOccupancyDashboard
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdSystem As New OleDb.OleDbCommand
    Dim RdrSystem As OleDb.OleDbDataReader
    Dim CmdProperty As New OleDb.OleDbCommand
    Dim RdrProperty As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "OccupancyDashboard"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            Cmd.CommandText = "SELECT Whcode FROM [System] Where WhCode <> 'FPZ6SD'"
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            TxtYearFrom.Text = Year(Date.Today)
            TxtYearTo.Text = Year(Date.Today)
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Dim dt As DataTable
            Dim dr As DataRow

            Dim ColMonth As DataColumn
            Dim ColYear As DataColumn
            Dim ColZone As DataColumn
            Dim ColTotalSpace As DataColumn
            Dim ColSpaceRented As DataColumn
            Dim ColPercentOcc As DataColumn
            Dim ColSpaceAvailable As DataColumn
            Dim DteStart As Date
            Dim DteEnd As Date
            Dim StrMonth As String
            Dim StrYear As String
            Dim StrZone As String = ""
            Dim LngTotalSpace As Long
            Dim LngSpaceRented As Long
            Dim LngPercentOCC As Long
            Dim LngSpaceAvailable As Long

            ColMonth = New DataColumn("Month", Type.GetType("System.String"))
            ColYear = New DataColumn("Year", Type.GetType("System.String"))
            ColZone = New DataColumn("Zone", Type.GetType("System.String"))
            ColTotalSpace = New DataColumn("TotalSpace", Type.GetType("System.String"))
            ColSpaceRented = New DataColumn("SpaceRented", Type.GetType("System.String"))
            ColPercentOcc = New DataColumn("PercentOcc", Type.GetType("System.String"))
            ColSpaceAvailable = New DataColumn("SpaceAvailable", Type.GetType("System.String"))

            dt = New DataTable()

            dt.Columns.Add(ColMonth)
            dt.Columns.Add(ColYear)
            dt.Columns.Add(ColZone)
            dt.Columns.Add(ColTotalSpace)
            dt.Columns.Add(ColSpaceRented)
            dt.Columns.Add(ColPercentOcc)
            dt.Columns.Add(ColSpaceAvailable)

            CmdSystem.Connection = Con
            CmdProperty.Connection = Con
            If CmbZone.Text = "Select..." Then
                CmdSystem.CommandText = "Select *From [System] Where WhCode <> 'FPZ6SD'"
            Else
                CmdSystem.CommandText = "Select *From [System] Where WhCode = '" & CmbZone.Text & "'"
            End If
            RdrSystem = CmdSystem.ExecuteReader
            While RdrSystem.Read

                DteStart = CDate("01-" & CmbMonthFrom.Text & "-" & TxtYearFrom.Text)
                DteEnd = CDate("28-" & CmbMonthTo.Text & "-" & TxtYearTo.Text)

                Do While DteStart <= DteEnd
                    LngTotalSpace = 0
                    LngSpaceRented = 0
                    LngPercentOCC = 0
                    LngSpaceAvailable = 0
                    StrMonth = Format(DteStart, "MMM")
                    StrYear = DteStart.Year

                    If RdrSystem("WhCode") = "FPZ01" Then
                        StrZone = "FPZ01"
                        LngTotalSpace = 10698
                        StrSql = "SELECT Sum(Property.Area) As Area FROM LeaseAgreement "
                        StrSql = StrSql & "Inner Join Property ON LeaseAgreement.PropCode = Property.PropertyCode "
                        StrSql = StrSql & "WHERE LeaseAgreement.MoveInDate <= '" & Format(DteStart, "MM/dd/yyyy") & "' AND LeaseAgreement.MovedOut = 0 And LeaseAgreement.Zone = 'FPZ01' "
                        StrSql = StrSql & "And (Property.[Type] = 'Warehouse' or Property.[Type] = 'Shed')"
                        CmdProperty.CommandText = StrSql
                        RdrProperty = CmdProperty.ExecuteReader
                        While RdrProperty.Read
                            If Not (IsDBNull(RdrProperty("Area"))) Then
                                LngSpaceRented = Val(RdrProperty("Area"))
                            End If
                        End While
                        RdrProperty.Close()
                    End If
                    If RdrSystem("WhCode") = "FPZ06" Then
                        StrZone = "FPZ06"
                        LngTotalSpace = 19361
                        StrSql = "SELECT sum(Property.Area) As Area FROM LeaseAgreement "
                        StrSql = StrSql & "Inner Join Property ON LeaseAgreement.PropCode = Property.PropertyCode "
                        StrSql = StrSql & "WHERE LeaseAgreement.MoveInDate <= '" & Format(DteStart, "MM/dd/yyyy") & "' AND LeaseAgreement.MovedOut = 0 And LeaseAgreement.Zone = 'FPZ06' "
                        StrSql = StrSql & "And (Property.[Type] = 'Warehouse' or Property.[Type] = 'Shed')"
                        CmdProperty.CommandText = StrSql
                        RdrProperty = CmdProperty.ExecuteReader
                        While RdrProperty.Read
                            If Not (IsDBNull(RdrProperty("Area"))) Then
                                LngSpaceRented = Val(RdrProperty("Area"))
                            End If
                        End While
                        RdrProperty.Close()
                    End If
                    If RdrSystem("WhCode") = "FPZ09" Then
                        StrZone = "FPZ09"
                        LngTotalSpace = 3395
                        StrSql = "SELECT sum(Property.Area) As Area FROM LeaseAgreement "
                        StrSql = StrSql & "Inner Join Property ON LeaseAgreement.PropCode = Property.PropertyCode "
                        StrSql = StrSql & "WHERE LeaseAgreement.MoveInDate <= '" & Format(DteStart, "MM/dd/yyyy") & "' AND LeaseAgreement.MovedOut = 0 And LeaseAgreement.Zone = 'FPZ09' "
                        StrSql = StrSql & "And (Property.[Type] = 'Cabin' or Property.[Type] = 'Office')"
                        CmdProperty.CommandText = StrSql
                        RdrProperty = CmdProperty.ExecuteReader
                        While RdrProperty.Read
                            If Not (IsDBNull(RdrProperty("Area"))) Then
                                LngSpaceRented = Val(RdrProperty("Area"))
                            End If
                        End While
                        RdrProperty.Close()
                    End If

                    dr = dt.NewRow
                    dr.Item("Month") = StrMonth
                    dr.Item("Year") = StrYear
                    dr.Item("Zone") = StrZone
                    dr.Item("TotalSpace") = LngTotalSpace
                    dr.Item("SpaceRented") = LngSpaceRented
                    dr.Item("PercentOcc") = Math.Round((LngSpaceRented / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                    dr.Item("SpaceAvailable") = LngTotalSpace - LngSpaceRented
                    dt.Rows.Add(dr)
                    DteStart = DteStart.AddMonths(1)
                Loop

            End While
            RdrSystem.Close()
            Dim ExcelGrid As New GridView()
            Dim DtProfileUsers As New DataTable("Table")
            ExcelGrid.DataSource = dt
            ' it's my DataTable Name  
            ExcelGrid.DataBind()
            If ExcelGrid.Rows.Count = 0 Then
                LblResult.Text = "No Records"
                Exit Sub
            End If
            Dim ExcelTable As New Table()
            ExcelTable.GridLines = GridLines.Both
            ExcelTable.Rows.Add(ExcelGrid.HeaderRow)
            For Each GdvRow As GridViewRow In ExcelGrid.Rows
                ExcelTable.Rows.Add(GdvRow)
            Next
            Dim wrt As New IO.StringWriter()
            Dim htw As New HtmlTextWriter(wrt)
            ExcelTable.RenderControl(htw)
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=OccupancyDashboard-" & DateTime.Today & ".xls")
            Response.ContentType = "application/ms-excel"
            Response.Write(wrt.ToString())
            Response.[End]()
        End If
    End Sub
End Class