﻿Public Partial Class ListLocations
    Inherits System.Web.UI.Page

    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdName As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim RdrName As OleDb.OleDbDataReader
    Dim CmdZone As New OleDb.OleDbCommand
    Dim RdrZone As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrSel As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbWhCode.Items.Add("Select...")
            CmbWhCode.Items.Add("ALL")
            While Rdr.Read
                CmbWhCode.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            StrSql = "SELECT * FROM Zones ORDER BY ZoneCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZones.Items.Add("Select...")
            CmbZones.Items.Add("ALL")
            While Rdr.Read
                CmbZones.Items.Add(Rdr("ZoneCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Private Sub CmbWhCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbWhCode.SelectedIndexChanged
        CmdName.CommandText = "SELECT [Name] FROM WareHouse WHERE WhCode='" & CmbWhCode.Text & "'"
        CmdName.Connection = Con
        RdrName = CmdName.ExecuteReader
        TxtName.Text = ""
        If RdrName.Read Then
            TxtName.Text = RdrName("Name")
        End If
        RdrName.Close()
    End Sub

    Private Sub CmbZones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbZones.SelectedIndexChanged
        CmdZone.CommandText = "SELECT [Description] FROM Zones WHERE ZoneCode='" & CmbZones.Text & "'"
        CmdZone.Connection = Con
        RdrZone = CmdZone.ExecuteReader
        TxtZoneName.Text = ""
        If RdrZone.Read Then
            TxtZoneName.Text = RdrZone("Description")
        End If
        RdrZone.Close()
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        StrSel = "{Racks.RackCode}<>''"
        If CmbWhCode.Text <> "ALL" Then
            StrSel = StrSel & " And {Racks.WhCode}='" & CmbWhCode.Text & "'"
        End If
        If CmbZones.Text <> "ALL" Then
            StrSel = StrSel & " And {Racks.ZoneCode}='" & CmbZones.Text & "'"
        End If
        Response.Redirect("Report.aspx?RptID=Location List&RPTFileName=ListLocations.aspx&RptFile=RptLocation.rpt&StrSelF=" & StrSel)
    End Sub

End Class