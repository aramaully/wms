﻿Public Partial Class FrmStatusofLicense
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdZone As New OleDb.OleDbCommand
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "StatusOfLicense"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            CmdZone.Connection = Con
            CmdZone.CommandText = "Select WhCode From Warehouse"
            Rdr = CmdZone.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()

            CmdZone.CommandText = "Select Distinct([Status]) As Sta From Client"
            Rdr = CmdZone.ExecuteReader
            CmbStatus.Items.Add("Select...")
            While Rdr.Read
                If Not IsDBNull(Rdr("Sta")) Then
                    CmbStatus.Items.Add(Rdr("Sta"))
                End If
            End While
            Rdr.Close()

        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            StrSql = "SELECT clientid, name From Client"
            CmdZone.Connection = Con
            CmdZone.CommandText = StrSql
            Rdr = CmdZone.ExecuteReader
            GvReport.DataSource = Rdr
            GvReport.DataBind()

            'Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            'Dim StrSelFormula As String
            'StrSelFormula = ""
            'If CmbZone.Text <> "Select..." Then
            '    StrSelFormula = "{Client.Whouses} = '" & CmbZone.Text & "'"
            'End If
            'If CmbStatus.Text <> "Select..." Then
            '    If StrSelFormula <> "" then
            '        StrSelFormula = StrSelFormula & " And {Client.Status} = '" & CmbStatus.Text & "'"
            '    Else
            '        StrSelFormula = "{Client.Status} = '" & CmbStatus.Text & "'"
            '    End If
            'End If
            'If ChkSummary.Checked = True Then
            '    Session("ReportFile") = "RptStatusLicense.rpt"
            'Else
            '    Session("ReportFile") = "RptStatusLicenseDetail.rpt"
            'End If
            'Session("Period") = ""
            'Session("Zone") = ""
            'Session("SelFormula") = StrSelFormula
            'Session("RepHead") = "Status Of Freeport Cetificate And Storage Permit"
            'ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub

End Class