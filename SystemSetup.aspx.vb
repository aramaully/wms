﻿Public Partial Class SystemSetup
    Inherits System.Web.UI.Page

    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not IsPostBack Then

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            CmbWHouse.Items.Clear()
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            If Session("PstrZone") = "ALL" Then
                While Rdr.Read
                    CmbWHouse.Items.Add(Rdr("WhCode"))
                End While
                Rdr.Close()
            Else
                CmbWHouse.Text = Session("PstrZone")
            End If
        End If
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click

        StrSql = "UPDATE System SET Name = '" & TxtName.Text & "',"
        StrSql = StrSql & "Address1='" & Txtaddress1.Text & "',"
        StrSql = StrSql & "Address2='" & TxtAddress2.Text & "',"
        StrSql = StrSql & "Address3='" & TxtAddress3.Text & "',"
        StrSql = StrSql & "Phone='" & TxtPhone.Text & "',"
        StrSql = StrSql & "Fax=" & TxtFax.Text & ","
        StrSql = StrSql & "EMail=" & TxtEmail.Text & ","
        StrSql = StrSql & "Web=" & TxtWeb.Text & ","
        StrSql = StrSql & "NextPalletNo=" & TxtNextPalletNo.Text & ","
        StrSql = StrSql & "NextInvoiceNo=" & TxtNextInvoiceNo.Text & ","
        StrSql = StrSql & "Vat=" & TxtVat.Text & ","
        StrSql = StrSql & "MinimumCharge=" & TxtMinimumCharge.Text & ","
        StrSql = StrSql & "VehCharge=" & TxtVenCharge.Text & ","
        StrSql = StrSql & "UnitDays=" & TxtUnitDays.Text & ","
        StrSql = StrSql & "PalletCharge=" & TxtPalletCharge.Text & ","
        StrSql = StrSql & "Cont20=" & TxtCont20.Text & ","
        StrSql = StrSql & "Cont40=" & TxtCont40.Text & ","
        StrSql = StrSql & "Cont20Day=" & TxtCont20Day.Text & ","
        StrSql = StrSql & "Cont40Day=" & TxtCont40Day.Text & " WHERE WhCode = '" & CmbWHouse.Text & "'"
        ErrHead.Text = "Upate Product Failed"

        Try
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub CmbWHouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbWHouse.SelectedIndexChanged
        StrSql = "SELECT * FROM [System] WHERE WhCode='" & CmbWHouse.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtName.Text = Rdr("Name")
            TxtAddress1.Text = Rdr("Address1")
            TxtAddress2.Text = Rdr("Address2")
            TxtAddress3.Text = Rdr("Address3")
            TxtPhone.Text = Rdr("Phone")
            TxtFax.Text = Rdr("Fax")
            TxtEmail.Text = Rdr("EMail")
            TxtWeb.Text = Rdr("Web")
            TxtNextPalletNo.Text = Rdr("NextPalletNo")
            TxtNextInvoiceNo.Text = Rdr("NextInvoiceNo")
            TxtVat.Text = Rdr("Vat")
            TxtMinimumCharge.Text = Rdr("MinimumCharge")
            TxtVenCharge.Text = Rdr("VehCharge")
            TxtUnitDays.Text = Rdr("UnitDays")
            TxtPalletCharge.Text = Rdr("PalletCharge")
            TxtCont20.Text = Rdr("Cont20")
            TxtCont40.Text = Rdr("Cont40")
            TxtCont20Day.Text = Rdr("Cont20Day")
            TxtCont40Day.Text = Rdr("Cont40Day")
        End If
        Rdr.Close()
    End Sub
End Class