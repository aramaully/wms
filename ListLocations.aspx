﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ListLocations.aspx.vb" Inherits="Wms.ListLocations" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            height: 47px;
        }
        .ModalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .style19
        {
            width: 16px;
        }
        .style20
        {
            height: 35px;
        }
    </style>
</head>
<body style="height: 100%">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" Width="80%" Height="450px" BorderStyle="Groove"
                    BorderWidth="1px">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="List Locations"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="BtnView" runat="server" Height="39px" Text="View" Style="cursor: pointer"
                                    ToolTip="View Report" Width="55px" UseSubmitBehavior="False" />
                               
                                <asp:Button ID="ButExit" runat="server" Height="39px" Text="Close" Style="cursor: pointer"
                                    ToolTip="Close this Module" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButExit_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Close this Module?"
                                    Enabled="True" TargetControlID="ButExit">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    
                    <table align="Left" border="0" cellpadding="0" cellspacing="0" 
                        style="height: 85px; width: 73%">
        <tr>
            <td align="left" bgcolor="#FFFFFF" valign="middle">
                
            </td>
            <td align="left" bgcolor="#FFFFFF" colspan="2" valign="middle" 
                style="text-align: center">
                &nbsp;</td>
            <td style="background-image: url(images/cen_rig.gif)" width="24">
            </td>
        </tr>
       
      
        <tr>
            <td align="left" bgcolor="#FFFFFF"  valign="middle">
            </td>
            <td align="left" bgcolor="#FFFFFF" valign="middle">
                Warehouse Code</td>
            <td align="left" bgcolor="#FFFFFF" valign="middle">
                <asp:DropDownList ID="CmbWhCode" runat="server" AutoPostBack="True" 
                    Height="18px" Width="161px">
                </asp:DropDownList>
            </td>
            <td style="background-image: url(images/cen_rig.gif)" width="24">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" bgcolor="#FFFFFF"  valign="middle">
            </td>
            <td align="left" bgcolor="#FFFFFF" valign="middle">
                Warehouse Name
            </td>
            <td align="left" bgcolor="#FFFFFF" valign="middle" colspan="2">
                <asp:TextBox ID="TxtName" runat="server" Width="350px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" bgcolor="#FFFFFF"  valign="middle" class="style20">
                </td>
            <td align="left" bgcolor="#FFFFFF" valign="middle" class="style20">
                Zone Code</td>
            <td align="left" bgcolor="#FFFFFF" valign="middle" class="style20">
                <asp:DropDownList ID="CmbZones" runat="server" AutoPostBack="True" 
                    Height="17px" Width="161px">
                </asp:DropDownList>
            </td>
            <td style="background-image: url(images/cen_rig.gif)" width="24" 
                class="style20">
                </td>
        </tr>
       
                        <tr>
                            <td align="left" bgcolor="#FFFFFF" valign="middle">
                                &nbsp;</td>
                            <td align="left" bgcolor="#FFFFFF" valign="middle">
                                Zone Name</td>
                            <td align="left" bgcolor="#FFFFFF" colspan="2" valign="middle">
                                <asp:TextBox ID="TxtZoneName" runat="server" Width="350px"></asp:TextBox>
                            </td>
                        </tr>
       
    </table>
                    
                </asp:Panel>
                 <asp:Panel ID="PnlFind" Style="z-index: 101; display:none; left: 100px; position: absolute; top: 60px"
                        runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                        Visible="True" BackColor="White">
                        <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                            <tr>
                                <td  bgcolor="#004080">
                                    <asp:Label ID="LblSearch" runat="server" Font-Bold="True" ForeColor="White" Style="text-align: center;"
                                        Text="Enter the text here to search"></asp:Label>
                                    <asp:TextBox ID="TxtSearch" runat="server" Width="100%" BackColor="White"></asp:TextBox>
                                </td>
                                <td align="right" bgcolor="#004080" class="style7">
                                    <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                        Width="65px" />
                                    <br />
                                    <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                        Width="65px" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Panel ID="Panel8" runat="server" Width="100%" Height="300px" ScrollBars="Vertical">
                                        <asp:GridView ID="GdvFind" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                            ShowFooter="true">
                                            <HeaderStyle BackColor="#0080C0" />
                                            <FooterStyle BackColor="#0080C0" />
                                            <AlternatingRowStyle BackColor="#CAE4FF" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
