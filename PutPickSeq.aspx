﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PutPickSeq.aspx.vb" Inherits="Wms.PutPickSeq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            height: 47px;
        }
        .ModalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .style19
        {
            width: 16px;
        }
    </style>
</head>
<body style="height: 100%">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 250px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" Width="40%" Height="350px" BorderStyle="Groove"
                    BorderWidth="1px">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Putaway Picking Sequence"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Putaway / Pick Sequence" Width="55px" 
                                    UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save Putaway Sequence?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete Charges?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButExit" runat="server" Height="39px" Text="Close" Style="cursor: pointer"
                                    ToolTip="Close this Module" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButExit_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Close this Module?"
                                    Enabled="True" TargetControlID="ButExit">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    
                    <table class="style1">
                        <tr>
                            <td>
                                Zone Code</td>
                            <td>
                                <asp:DropDownList ID="CmbZone" runat="server" Width="100px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                Name</td>
                            <td colspan="2">
                                <asp:TextBox ID="TxtName" runat="server" ReadOnly="True" Width="90%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:RadioButton ID="RbnPut" runat="server" Checked="True" GroupName="PutPick" 
                                    Text="Putaway" AutoPostBack="True" />
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:RadioButton ID="RbnPick" runat="server" GroupName="PutPick" 
                                    Text="Picking" AutoPostBack="True" />
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                1st Order</td>
                            <td>
                                <asp:DropDownList ID="CmbOrder0" runat="server" Width="200px">
                                    <asp:ListItem>Select...</asp:ListItem>
                                    <asp:ListItem>Side</asp:ListItem>
                                    <asp:ListItem>Bay</asp:ListItem>
                                    <asp:ListItem>[Level]</asp:ListItem>
                                    <asp:ListItem>Aisle</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="ChkDesc0" runat="server" Text="Descending" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2nd Order</td>
                            <td>
                                <asp:DropDownList ID="CmbOrder1" runat="server" Width="200px">
                                    <asp:ListItem>Select...</asp:ListItem>
                                    <asp:ListItem>Side</asp:ListItem>
                                    <asp:ListItem>Bay</asp:ListItem>
                                    <asp:ListItem>[Level]</asp:ListItem>
                                    <asp:ListItem>Aisle</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="ChkDesc1" runat="server" Text="Descending" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                3rd Order</td>
                            <td>
                                <asp:DropDownList ID="CmbOrder2" runat="server" Width="200px">
                                    <asp:ListItem>Select...</asp:ListItem>
                                    <asp:ListItem>Side</asp:ListItem>
                                    <asp:ListItem>Bay</asp:ListItem>
                                    <asp:ListItem>[Level]</asp:ListItem>
                                    <asp:ListItem>Aisle</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="ChkDesc2" runat="server" Text="Descending" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                4th Order</td>
                            <td>
                                <asp:DropDownList ID="CmbOrder3" runat="server" Width="200px">
                                    <asp:ListItem>Select...</asp:ListItem>
                                    <asp:ListItem>Side</asp:ListItem>
                                    <asp:ListItem>Bay</asp:ListItem>
                                    <asp:ListItem>[Level]</asp:ListItem>
                                    <asp:ListItem>Aisle</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="ChkDesc3" runat="server" Text="Descending" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                5th Order</td>
                            <td>
                                <asp:DropDownList ID="CmbOrder4" runat="server" Width="200px">
                                    <asp:ListItem>Select...</asp:ListItem>
                                    <asp:ListItem>Side</asp:ListItem>
                                    <asp:ListItem>Bay</asp:ListItem>
                                    <asp:ListItem>[Level]</asp:ListItem>
                                    <asp:ListItem>Aisle</asp:ListItem>
                                    <asp:ListItem>PalletPosition</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="ChkDesc4" runat="server" Text="Descending" />
                            </td>
                        </tr>
                    </table>                    
                </asp:Panel>                 
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
