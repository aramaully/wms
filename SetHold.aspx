﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SetHold.aspx.vb" Inherits="Wms.SetHold" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            height: 47px;
        }
        .ModalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .style19
        {
            width: 16px;
        }
        .style20
        {
            height: 7px;
        }
    </style>
</head>
<body style="height: 100%">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" Width="60%" Height="450px" BorderStyle="Groove"
                    BorderWidth="1px">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Set / Unset Locations on Hold"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButRefresh" runat="server" Height="39px" Text="Refresh" Style="cursor: pointer"
                                    ToolTip="Refresh Location Grid" Width="75px" CausesValidation="False" 
                                    UseSubmitBehavior="False" />
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Charges File" Width="75px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Set/Unset Locations as selected?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButExit" runat="server" Height="39px" Text="Close" Style="cursor: pointer"
                                    ToolTip="Close this Module" Width="75px" CausesValidation="False" 
                                    UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButExit_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Close this Module?"
                                    Enabled="True" TargetControlID="ButExit">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    
                    <table cellpadding="0" class="style1">
                        <tr>
                            <td>
                                FP Zone</td>
                            <td>
                                <asp:DropDownList ID="CmbFpZone" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                From</td>
                            <td align="left">
                                To</td>
                        </tr>
                        <tr>
                            <td>
                                Zone</td>
                            <td>
                                <asp:DropDownList ID="CmbZone" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Location</td>
                            <td>
                                <asp:TextBox ID="TxtLocFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtLocTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style20">
                                <asp:RadioButton ID="RbnSet" runat="server" Checked="True" GroupName="Set" 
                                    Text="Set Hold" />
                            </td>
                            <td class="style20">
                                <asp:RadioButton ID="RbnUnset" runat="server" GroupName="Set" 
                                    Text="UnSetHold" />
                            </td>
                            <td class="style20">
                                </td>
                            <td class="style20">
                                Rack</td>
                            <td class="style20">
                                <asp:TextBox ID="TxtRackFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td class="style20">
                                <asp:TextBox ID="TxtRackTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="RbnPut" runat="server" Checked="True" GroupName="PutPick" 
                                    Text="Putaway" />
                            </td>
                            <td valign="middle">
                                <asp:RadioButton ID="RbnPick" runat="server" GroupName="PutPick" 
                                    Text="Picking" />
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Aisle</td>
                            <td>
                                <asp:TextBox ID="TxtAisleFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtAisleTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="RbnRacked" runat="server" Checked="True" GroupName="Rack" 
                                    Text="Racked" />
                            </td>
                            <td>
                                <asp:RadioButton ID="RbnNonRacked" runat="server" GroupName="Rack" 
                                    Text="Non-Racked" />
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Side</td>
                            <td>
                                <asp:TextBox ID="TxtSideFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtSideTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Remarks</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Bay</td>
                            <td>
                                <asp:TextBox ID="TxtBayFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtBayTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="2">
                                <asp:TextBox ID="TxtRemarks" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Level</td>
                            <td>
                                <asp:TextBox ID="TxtlevelFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtLevelTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Position</td>
                            <td>
                                <asp:TextBox ID="TxtPosFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtPosTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="Panel2" runat="server" Height="206px" ScrollBars="Auto">
                    
                        <asp:GridView ID="GdvLoc" runat="server" AutoGenerateColumns="False" 
                            Width="100%">
                            <Columns>
                                <asp:BoundField DataField="LocCode" HeaderText="Location" />
                                <asp:BoundField DataField="WhCode" HeaderText="Ware House">
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ZoneCode" HeaderText="Zone">
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CurPRatio" HeaderText="Cur P Ratio">
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="LastBoeIn" HeaderText="Last Inbound">
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:CheckBoxField DataField="PutHold" HeaderText="Put Hold">
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:CheckBoxField>
                                <asp:CheckBoxField DataField="PickHold" HeaderText="Pick Hold">
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:CheckBoxField>
                            </Columns>
                            <HeaderStyle BackColor="#0066FF" ForeColor="White" />
                            <AlternatingRowStyle BackColor="#99CCFF" />
                        </asp:GridView>
                    
                    </asp:Panel>
                </asp:Panel>                
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>

