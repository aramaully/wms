﻿Public Partial Class Zones
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatLoc As New DataTable
    Dim StrCondition As String = ""

    Const MENUID = "Zones"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            Cmd.CommandText = "SELECT WhCode FROM Warehouse ORDER BY WhCode"
            Rdr = Cmd.ExecuteReader
            CmbWhCode.Items.Add("Select...")
            While Rdr.Read
                CmbWhCode.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Private Sub GdvLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvZone.SelectedIndexChanged
        On Error Resume Next
        TxtZoneCode.Text = ChkSpace(GdvZone.SelectedRow.Cells(0).Text)
        TxtDescription.Text = ChkSpace(GdvZone.SelectedRow.Cells(1).Text)
        ChkOktoBill.Checked = DirectCast(GdvZone.SelectedRow.Cells(2).Controls(0), CheckBox).Checked
        ChkAllClients.Checked = DirectCast(GdvZone.SelectedRow.Cells(3).Controls(0), CheckBox).Checked
        TxtClientID.Text = ChkSpace(GdvZone.SelectedRow.Cells(4).Text)
        TxtStart.Text = ChkSpace(GdvZone.SelectedRow.Cells(5).Text)
        TxtEnd.Text = ChkSpace(GdvZone.SelectedRow.Cells(6).Text)
        TxtCharge.Text = ChkSpace(GdvZone.SelectedRow.Cells(7).Text)
        TxtClientID_TextChanged(Me, e)
    End Sub

    Private Sub BindZone()
        Dim Ev As System.EventArgs
        Cmd.Connection = Con
        StrSql = "SELECT ZoneCode Zone,Description,OKtoBill,AllClient,ClientID,"
        StrSql = StrSql & "CONVERT(Char,StartDate,103) 'Start On',CONVERT(Char,EndDate,103) 'End On',Charge FROM Zones "
        StrSql = StrSql & "WHERE WhCode = '" & CmbWhCode.Text & "' ORDER BY ZoneCode"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar Is Nothing Then Exit Sub
        Rdr = Cmd.ExecuteReader
        DatLoc.Load(Rdr)
        Rdr.Close()
        GdvZone.DataSource = DatLoc
        GdvZone.DataBind()
        If GdvZone.Rows.Count = 1 Then
            On Error Resume Next
            TxtZoneCode.Text = ChkSpace(GdvZone.Rows(0).Cells(0).Text)
            TxtDescription.Text = ChkSpace(GdvZone.Rows(0).Cells(1).Text)
            ChkOktoBill.Checked = DirectCast(GdvZone.Rows(0).Cells(2).Controls(0), CheckBox).Checked
            ChkAllClients.Checked = DirectCast(GdvZone.Rows(0).Cells(3).Controls(0), CheckBox).Checked
            TxtClientID.Text = ChkSpace(GdvZone.Rows(0).Cells(4).Text)
            TxtStart.Text = ChkSpace(GdvZone.Rows(0).Cells(5).Text)
            TxtEnd.Text = ChkSpace(GdvZone.Rows(0).Cells(6).Text)
            TxtCharge.Text = ChkSpace(GdvZone.Rows(0).Cells(7).Text)
            TxtClientID_TextChanged(Me, Ev)
        End If
    End Sub

    Private Sub GdvZone_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvZone.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvZone, "Select$" & e.Row.RowIndex)
            e.Row.Cells(2).Style.Add("text-align", "center")
            e.Row.Cells(3).Style.Add("text-align", "center")
            e.Row.Cells(7).Style.Add("text-align", "right")
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvZone.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvZone.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub GdvZone_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvZone.SelectedIndexChanging
        GdvZone.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        On Error Resume Next
        TxtCharge.Text = ""
        TxtClientID.Text = ""
        TxtDescription.Text = ""
        TxtEnd.Text = ""
        TxtName.Text = ""
        TxtStart.Text = ""
        TxtZoneCode.Text = ""
        ChkAllClients.Checked = False
        ChkOktoBill.Checked = False
        CmbWhCode.Focus()
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Try
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Zones WHERE ZoneCode = '" & TxtZoneCode.Text & "' AND WhCode = '" & CmbWhCode.Text & "'"
            Cmd.ExecuteNonQuery()
            ButNew_Click(Me, e)
            BindZone()
        Catch ex As Exception
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Cmd.CommandText = "SELECT ZoneCode FROM Zones WHERE ZoneCode = '" & TxtZoneCode.Text & "' AND WhCode = '" & CmbWhCode.Text & "'"
        Try
            If Cmd.ExecuteScalar Is Nothing Then
                StrSql = "INSERT INTO Zones (ZoneCode,WhCode,Description,OKtoBill,AllClient,ClientID,StartDate,EndDate,Charge) "
                StrSql = StrSql & "VALUES('" & TxtZoneCode.Text & "','"
                StrSql = StrSql & CmbWhCode.Text & "','"
                StrSql = StrSql & TxtDescription.Text & "',"
                StrSql = StrSql & IIf(ChkOktoBill.Checked, 1, 0) & ","
                StrSql = StrSql & IIf(ChkAllClients.Checked, 1, 0) & ",'"
                StrSql = StrSql & TxtClientID.Text & "','"
                StrSql = StrSql & Format(CDate(TxtStart.Text), "dd-MMM-yyyy") & "','"
                StrSql = StrSql & Format(CDate(TxtEnd.Text), "dd-MMM-yyyy") & "',"
                StrSql = StrSql & Val(TxtCharge.Text) & ")"
            Else
                StrSql = "UPDATE Zones SET ZoneCode = '" & TxtZoneCode.Text & "',"
                StrSql = StrSql & "WhCode='" & CmbWhCode.Text & "',"
                StrSql = StrSql & "Description='" & TxtDescription.Text & "',"
                StrSql = StrSql & "OKtoBill=" & IIf(ChkOktoBill.Checked, 1, 0) & ","
                StrSql = StrSql & "AllClient='" & IIf(ChkAllClients.Checked, 1, 0) & "',"
                StrSql = StrSql & "ClientID='" & TxtClientID.Text & "',"
                If TxtStart.Text = "" Then
                    StrSql = StrSql & "StartDate = NULL,"
                Else
                    StrSql = StrSql & "StartDate='" & CDate(TxtStart.Text).ToString("dd-MMM-yyyy") & "',"
                End If
                If TxtEnd.Text = "" Then
                    StrSql = StrSql & "EndDate = NULL,"
                Else
                    StrSql = StrSql & "EndDate='" & CDate(TxtEnd.Text).ToString("dd-MMM-yyyy") & "',"
                End If
                StrSql = StrSql & "Charge=" & Val(TxtCharge.Text) & " WHERE ZoneCode = '" & TxtZoneCode.Text & "' "
                StrSql = StrSql & "AND WhCode = '" & CmbWhCode.Text & "'"
            End If
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ButNew_Click(Me, e)
            BindZone()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub CmbWhCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbWhCode.SelectedIndexChanged
        TxtCharge.Text = ""
        TxtClientID.Text = ""
        TxtDescription.Text = ""
        TxtEnd.Text = ""
        TxtName.Text = ""
        TxtStart.Text = ""
        TxtZoneCode.Text = ""
        ChkAllClients.Checked = False
        ChkOktoBill.Checked = False
        BindZone()
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Try
            TxtName.Text = Cmd.ExecuteScalar.ToString
        Catch ex As Exception
            TxtName.Text = "Invalid Client!"
        End Try
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        LblSearch.Text = "Please enter name to search"
        PnlFind.Visible = True
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
        StrSql = StrSql & "Name LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Visible = False
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtClientID_TextChanged(Me, e)
        PnlFind.Visible = False
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub
End Class