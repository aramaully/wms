﻿Public Partial Class FrmNewActivity
    Inherits System.Web.UI.Page
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim Con As New OleDb.OleDbConnection

    Const MENUID = "Activities"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT *From Activity"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvCurrency.DataSource = Rdr
        GdvCurrency.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtActivity.Text = ""
            TxtActivity.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Activity Where Activity = '" & TxtActivity.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Activity (Activity) VALUES('"
                StrSql = StrSql & UCase(TxtActivity.Text) & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                TxtActivity.Text = ""
                TxtActivity.Focus()
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Activity Where Activity = '" & TxtActivity.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtActivity.Text = ""
                TxtActivity.Focus()
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvCurrency_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvCurrency.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvCurrency, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvCurrency.SelectedIndexChanged
        TxtActivity.Text = GdvCurrency.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
    End Sub

    Private Sub GdvCurrency_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvCurrency.SelectedIndexChanging
        GdvCurrency.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvCurrency.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCurrency.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

End Class