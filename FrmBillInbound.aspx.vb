﻿Public Partial Class FrmBillInbound
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdClient As New OleDb.OleDbCommand
    Dim RdrClient As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdCont As New OleDb.OleDbCommand
    Dim RdrCont As OleDb.OleDbDataReader
    Dim CmdProd As New OleDb.OleDbCommand
    Dim RdrProd As OleDb.OleDbDataReader


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            StrSql = "SELECT UnitCode FROM Unit ORDER BY UnitCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbProdUnit.Items.Add("Select...")
            While Rdr.Read
                CmbProdUnit.Items.Add(Rdr("UnitCode"))
            End While
            Rdr.Close()
            StrSql = "SELECT Name FROM Traffic ORDER BY Name"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbTrafficOp.Items.Add("Select...")
            CmbTrafficOp.Items.Add("")
            While Rdr.Read
                CmbTrafficOp.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButAddCont_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAddCont.Click
        Dim IntContNo As Integer = 0
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColType As DataColumn
        Dim ColContNo As DataColumn
        Dim ColRate As DataColumn
        dt = New DataTable()
        ColType = New DataColumn("Type", Type.GetType("System.String"))
        ColContNo = New DataColumn("ContNo", Type.GetType("System.String"))
        ColRate = New DataColumn("Rate", Type.GetType("System.String"))
        dt.Columns.Add(ColType)
        dt.Columns.Add(ColContNo)
        dt.Columns.Add(ColRate)
        On Error Resume Next
        For Each row As GridViewRow In GdvCont.Rows
            If row.RowType = DataControlRowType.DataRow Then
                dr = dt.NewRow
                dr.Item("Type") = DirectCast(row.FindControl("Type"), TextBox).Text
                If DirectCast(row.FindControl("Type"), TextBox).Text <> "" Then
                    IntContNo = IntContNo + 1
                    TxtCont.Text = IntContNo
                End If
                dr.Item("ContNo") = DirectCast(row.FindControl("ContNo"), TextBox).Text
                dr.Item("Rate") = DirectCast(row.FindControl("Rate"), TextBox).Text
                dt.Rows.Add(dr)
            End If
        Next
        dr = dt.NewRow()
        dr("Type") = ""
        dr("ContNo") = ""
        dr("Rate") = ""
        dt.Rows.Add(dr)
        GdvCont.DataSource = dt
        GdvCont.DataBind()
    End Sub

    Protected Sub ButDelCont_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDelCont.Click
        Dim IntContNo As Integer = 0
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColType As DataColumn
        Dim ColContNo As DataColumn
        Dim ColRate As DataColumn
        dt = New DataTable()
        ColType = New DataColumn("Type", Type.GetType("System.String"))
        ColContNo = New DataColumn("ContNo", Type.GetType("System.String"))
        ColRate = New DataColumn("Rate", Type.GetType("System.String"))
        dt.Columns.Add(ColType)
        dt.Columns.Add(ColContNo)
        dt.Columns.Add(ColRate)
        On Error Resume Next
        For Each row As GridViewRow In GdvCont.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim ChkDel As CheckBox = DirectCast(row.FindControl("ChkSelect"), CheckBox)
                If Not ChkDel.Checked Then
                    dr = dt.NewRow
                    dr.Item("Type") = DirectCast(row.FindControl("Type"), TextBox).Text
                    If DirectCast(row.FindControl("Type"), TextBox).Text <> "" Then
                        IntContNo = IntContNo + 1
                        TxtCont.Text = IntContNo
                    End If
                    dr.Item("ContNo") = DirectCast(row.FindControl("ContNo"), TextBox).Text
                    dr.Item("Rate") = DirectCast(row.FindControl("Rate"), TextBox).Text
                    dt.Rows.Add(dr)
                End If
            End If
        Next
        GdvCont.DataSource = dt
        GdvCont.DataBind()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ev As New System.EventArgs
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If e.Item.CommandName = "New" Then
            TxtBillIn.Text = ""
            CmbZone.SelectedIndex = 0
            TxtDate.Text = ""
            TxtClientID.Text = ""
            TxtName.Text = ""
            CmbTrafficOp.SelectedIndex = 0
            TxtItems.Text = ""
            TxtTruckNo.Text = ""
            TxtRemarks.Text = ""
            TxtCont.Text = ""
            GdvCont.DataSource = Nothing
            GdvCont.DataBind()
            GdvProduct.DataSource = Nothing
            GdvProduct.DataBind()
            TxtProductCode.Text = ""
            TxtProductName.Text = ""
            TxtItemDesc.Text = ""
            TxtProdQtyUnit.Text = ""
            CmbProdUnit.SelectedIndex = 0
            TxtWeight.Text = ""
            CmbZone.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            If CmbZone.Text = "Select..." Then
                Lblerror.Visible = True
                Lblerror.Text = "Please Select Zone"
                Exit Sub
            End If
            If CmbZone.Text = "" Then
                Lblerror.Visible = True
                Lblerror.Text = "Please Select Zone"
                Exit Sub
            End If
            If TxtDate.Text = "" Then
                Lblerror.Visible = True
                Lblerror.Text = "Please Select Date"
                Exit Sub
            End If
            If TxtBillIn.Text = "" Then
                Lblerror.Visible = True
                Lblerror.Text = "Please Input Bill No"
                Exit Sub
            End If
            If TxtClientID.Text = "" Then
                Lblerror.Visible = True
                Lblerror.Text = "Please Input Client ID"
                Exit Sub
            End If
            If CmbTrafficOp.Text = "Select" Then
                Lblerror.Visible = True
                Lblerror.Text = "Please Traffic Officer"
                Exit Sub
            End If
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM BoeIn WHERE BillNo = '" & TxtBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "SELECT BillNo FROM BoeIn WHERE BillNo = '" & TxtBillIn.Text & "'"
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Try
                StrSql = "INSERT INTO BoeIn (BillNo,BillDate,ClientID,Items,Container,TruckNo,"
                StrSql = StrSql & "TrafficOp,Remarks,UserID,WhCode,Posted,Discrepancy,CrossDock) VALUES('"
                StrSql = StrSql & TxtBillIn.Text & "',"
                If TxtDate.Text = "" Then
                    StrSql = StrSql & "NULL,'"
                Else
                    StrSql = StrSql & "'" & CDate(TxtDate.Text).ToString("dd-MMM-yyyy") & "','"
                End If
                StrSql = StrSql & TxtClientID.Text & "',"
                StrSql = StrSql & Val(TxtItems.Text) & ","
                StrSql = StrSql & Val(TxtCont.Text) & ",'"
                StrSql = StrSql & ChkSpace(TxtTruckNo.Text) & "','"
                StrSql = StrSql & ChkSpace(CmbTrafficOp.Text) & "','"
                StrSql = StrSql & TxtRemarks.Text & "','"
                StrSql = StrSql & Session("UserID") & "','"
                StrSql = StrSql & ChkSpace(CmbZone.Text) & "',"
                StrSql = StrSql & "0,0,0)"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()


                'Update Bill Products
                Cmd.CommandText = "DELETE FROM BoeInProd WHERE BoeNo = '" & TxtBillIn.Text & "'"
                Cmd.ExecuteNonQuery()
                For Each row As GridViewRow In GdvProduct.Rows
                    StrSql = "INSERT INTO BoeInProd (BoeNo,PCode,Name,ProdDescription,Quantity,UnitCode,Weight"
                    StrSql = StrSql & ") VALUES('"
                    StrSql = StrSql & TxtBillIn.Text & "','"
                    StrSql = StrSql & ChkSpace(row.Cells(0).Text) & "','"
                    StrSql = StrSql & ChkSpace(row.Cells(1).Text) & "','"
                    StrSql = StrSql & ChkSpace(row.Cells(2).Text) & "',"
                    StrSql = StrSql & Val(row.Cells(3).Text) & ",'"
                    StrSql = StrSql & ChkSpace(row.Cells(4).Text) & "',"
                    StrSql = StrSql & Val(row.Cells(5).Text) & ")"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                Next

                'Update ContainerDetails
                Cmd.CommandText = "DELETE FROM NewContDetails WHERE BoeNo = '" & TxtBillIn.Text & "'"
                Cmd.ExecuteNonQuery()
                For Each row As GridViewRow In GdvCont.Rows
                    StrSql = "INSERT INTO NewContDetails (BoeNo,ContType,ContainerNo,Rate"
                    StrSql = StrSql & ") VALUES('"
                    StrSql = StrSql & TxtBillIn.Text & "','"
                    If row.RowType = DataControlRowType.DataRow Then
                        StrSql = StrSql & DirectCast(row.FindControl("Type"), TextBox).Text & "','"
                        StrSql = StrSql & DirectCast(row.FindControl("ContNo"), TextBox).Text & "','"
                        StrSql = StrSql & DirectCast(row.FindControl("Rate"), TextBox).Text & "')"
                    End If
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                Next
                TxtBillIn.Text = ""
                CmbZone.SelectedIndex = 0
                TxtDate.Text = ""
                TxtClientID.Text = ""
                TxtName.Text = ""
                CmbTrafficOp.SelectedIndex = 0
                TxtItems.Text = ""
                TxtTruckNo.Text = ""
                TxtRemarks.Text = ""
                TxtCont.Text = ""
                GdvCont.DataSource = Nothing
                GdvCont.DataBind()
                GdvProduct.DataSource = Nothing
                GdvProduct.DataBind()
                TxtProductCode.Text = ""
                TxtProductName.Text = ""
                TxtItemDesc.Text = ""
                TxtProdQtyUnit.Text = ""
                CmbProdUnit.SelectedIndex = 0
                TxtWeight.Text = ""
                CmbZone.Focus()
            Catch ex As Exception
                'ErrHead.Text = "Save Failed"
                Lblerror.Text = ex.Message
            End Try
        End If
        If e.Item.CommandName = "Next" Then

            Cmd.Connection = Con
            Cmd.CommandText = "SELECT TOP 1BillNo FROM BoeIn WHERE BillNo > '" & TxtBillIn.Text & "'"
            Try
                TxtBillIn.Text = Cmd.ExecuteScalar.ToString
                TxtBillIn_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Previous" Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT TOP 1 BillNo FROM BoeIn WHERE BillNo < '" & TxtBillIn.Text & "' ORDER BY BillNo DESC"
            Try
                TxtBillIn.Text = Cmd.ExecuteScalar.ToString
                TxtBillIn_TextChanged(Me, ev)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub ButFindClient_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindClient.Click
        Session.Add("PstrCode", TxtClientID.Text)
        ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        If Trim(TxtClientID.Text) <> "" Then
            CmdClient.Connection = Con
            CmdClient.CommandText = "Select *From Client Where ClientID = '" & TxtClientID.Text & "'"
            RdrClient = CmdClient.ExecuteReader
            If RdrClient.Read Then
                TxtName.Text = RdrClient("Name") & ""
            End If
            RdrClient.Close()
        End If
    End Sub

    Protected Sub ButFindProduct_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindProduct.Click
        Session.Add("PstrCode", TxtProductCode.Text)
        ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFindProd", "<script language='javascript'> ShowFindProd();</script>", False)
    End Sub

    Private Sub TxtProductCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtProductCode.TextChanged
        If Trim(TxtProductCode.Text) <> "" Then
            Cmd.Connection = Con
            Cmd.CommandText = "Select *From Product Where ProdCode = '" & TxtProductCode.Text & "'"
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                TxtProductName.Text = Rdr("Name") & ""
                TxtItemDesc.Text = Rdr("Description") & ""
                CmbProdUnit.Text = Rdr("Unit") & ""
            End If
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButAddProd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAddProd.Click
        Dim CmdProdItem As New OleDb.OleDbCommand
        Dim RdrProdItem As OleDb.OleDbDataReader
        Dim StrProd As String
        If TxtBillIn.Text = "" Then
            Lblerror.Visible = True
            Lblerror.Text = "Please Enter Bill Number"
            Exit Sub
        End If
        CmdProdItem.Connection = Con
        If TxtProductCode.Text <> "" Then
            If ButAddProd.Text = "Add" Then
                StrSql = "Insert Into BoeInProd(BoeNo,PCode,ProdDescription,Quantity,UnitCode, Weight)VALUES('"
                StrSql = StrSql & TxtBillIn.Text & "','"
                StrSql = StrSql & ChkSpace(TxtProductCode.Text) & "','"
                StrSql = StrSql & ChkSpace(TxtItemDesc.Text) & "','"
                StrSql = StrSql & Val(TxtProdQtyUnit.Text) & "','"
                StrSql = StrSql & ChkSpace(CmbProdUnit.Text) & "','"
                StrSql = StrSql & Val(TxtWeight.Text) & "')"
                CmdProdItem.CommandText = StrSql
                CmdProdItem.ExecuteNonQuery()
                TxtProductCode.Text = ""
                TxtProductName.Text = ""
                TxtItemDesc.Text = ""
                TxtProdQtyUnit.Text = ""
                CmbProdUnit.SelectedIndex = -1
                TxtWeight.Text = ""
            End If
        End If
        'Update Products in Grid
        StrProd = "SELECT Pcode,ProdDescription,Quantity,UnitCode,Weight FROM BoeInProd "
        StrProd = StrProd & "WHERE BoeNo = '" & TxtBillIn.Text & "' ORDER By PCode"
        CmdProdItem.CommandText = StrProd
        RdrProdItem = CmdProdItem.ExecuteReader
        GdvProduct.DataSource = RdrProdItem
        GdvProduct.DataBind()
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Or Txt = "Select..." Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Protected Sub ButDelProd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDelProd.Click
        Dim DatCont As New DataTable
        Dim Dtrow As DataRow
        Dim BlnDeleted As Boolean = False
        Dim CmdProdItem As New OleDb.OleDbCommand
        Dim RdrProdItem As OleDb.OleDbDataReader
        Dim StrProd As String
        Cmd.Connection = Con
        CmdProdItem.Connection = Con
        For Each row As GridViewRow In GdvProduct.Rows
            If row.RowType = DataControlRowType.DataRow Then
                If DirectCast(row.FindControl("ChkSelProd"), CheckBox).Checked Then
                    BlnDeleted = True
                    Cmd.CommandText = "Delete From BoeInProd Where BoeNo = '" & TxtBillIn.Text & "' And PCode = '" & ChkSpace(row.Cells(1).Text) & "'"
                    Cmd.ExecuteNonQuery()
                    Dtrow = DatCont.NewRow
                    For I = 1 To GdvProduct.Columns.Count - 1
                        Dtrow.Item(I - 1) = ChkSpace(row.Cells(I).Text)
                    Next
                    DatCont.Rows.Add(Dtrow)
                End If
            End If
        Next
        'Update Products in Grid
        StrProd = "SELECT Pcode,ProdDescription,Quantity,UnitCode,Weight FROM BoeInProd "
        StrProd = StrProd & "WHERE BoeNo = '" & TxtBillIn.Text & "' ORDER By PCode"
        CmdProdItem.CommandText = StrProd
        RdrProdItem = CmdProdItem.ExecuteReader
        GdvProduct.DataSource = RdrProdItem
        GdvProduct.DataBind()
    End Sub


    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvCont.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCont.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvProduct.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvProduct.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtBillIn_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBillIn.TextChanged
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColType As DataColumn
        Dim ColContNo As DataColumn
        Dim ColRate As DataColumn
        Dim StrGetItem As String
        Dim ev As New System.EventArgs
        If Trim(TxtBillIn.Text) <> "" Then
            Cmd.Connection = Con
            Cmd.CommandText = "Select *From BoeIn Where BillNo = '" & TxtBillIn.Text & "'"
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                CmbZone.Text = Rdr("WhCode")
                If Not IsDBNull(Rdr("BillDate")) Then
                    TxtDate.Text = Format(Rdr("BillDate"), "dd/MM/yyyy")
                Else
                    TxtDate.Text = ""
                End If
                TxtClientID.Text = Rdr("ClientID")
                TxtClientID_TextChanged(Me, ev)
                If Rdr("TrafficOp") <> "" Then
                    CmbTrafficOp.Text = Rdr("TrafficOp")
                End If
                TxtItems.Text = Rdr("Items")
                TxtTruckNo.Text = Rdr("TruckNo")
                TxtRemarks.Text = Rdr("Remarks")
                TxtCont.Text = Rdr("Container")
            End If
            Rdr.Close()
            ''Update Container in Grid
            CmdCont.Connection = Con
            dt = New DataTable()
            ColType = New DataColumn("Type", Type.GetType("System.String"))
            ColContNo = New DataColumn("ContNo", Type.GetType("System.String"))
            ColRate = New DataColumn("Rate", Type.GetType("System.String"))
            dt.Columns.Add(ColType)
            dt.Columns.Add(ColContNo)
            dt.Columns.Add(ColRate)
            StrGetItem = "SELECT ContType,ContainerNo,Rate From NewContDetails "
            StrGetItem = StrGetItem & "WHERE BoeNo = '" & TxtBillIn.Text & "' Order By ContType"
            CmdCont.CommandText = StrGetItem
            RdrCont = CmdCont.ExecuteReader
            While RdrCont.Read
                dr = dt.NewRow
                dr.Item("Type") = RdrCont("ContType")
                dr.Item("ContNo") = RdrCont("ContainerNo")
                dr.Item("Rate") = RdrCont("Rate")
                dt.Rows.Add(dr)
            End While
            GdvCont.DataSource = dt
            GdvCont.DataBind()

            'Update Products in Grid
            CmdProd.Connection = Con
            StrGetItem = "SELECT Pcode,Name,ProdDescription,Quantity,UnitCode,Weight FROM BoeInProd "
            StrGetItem = StrGetItem & "WHERE BoeNo = '" & TxtBillIn.Text & "'"
            CmdProd.CommandText = StrGetItem
            RdrProd = CmdProd.ExecuteReader
            GdvProduct.DataSource = RdrProd
            GdvProduct.DataBind()
            RdrProd.Close()
        End If
    End Sub

    Private Sub GdvProduct_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GdvProduct.RowCommand
        Dim Index = Convert.ToInt32(e.CommandArgument)
        If DirectCast(GdvProduct.Rows(Index).FindControl("ChkSelProd"), CheckBox).Checked Then
            ButAddProd.Text = "Update"
        Else
            ButAddProd.Text = "Add"
        End If
    End Sub

    Private Sub GdvProduct_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvProduct.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvProduct, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvProduct.SelectedIndexChanged
        On Error Resume Next
        Dim row As GridViewRow = GdvProduct.SelectedRow
        TxtProductCode.Text = ""
        TxtItemDesc.Text = ""
        TxtProdQtyUnit.Text = ""
        TxtWeight.Text = ""
        CmbProdUnit.SelectedIndex = 0
        TxtProductCode.Text = ChkSpace(row.Cells(1).Text)
        TxtItemDesc.Text = ChkSpace(row.Cells(2).Text)
        TxtProdQtyUnit.Text = ChkSpace(row.Cells(3).Text)
        CmbProdUnit.Text = ChkSpace(row.Cells(4).Text)
        TxtWeight.Text = ChkSpace(row.Cells(5).Text)
    End Sub

    Private Sub GdvProduct_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvProduct.SelectedIndexChanging
        GdvProduct.SelectedIndex = e.NewSelectedIndex
    End Sub
End Class