﻿Public Partial Class FrmCustomRep
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Const MENUID = "CustomRep"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con

            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            Dim risList As New ListItemCollection
            StrSql = "SELECT NAME FROM SYSOBJECTS WHERE TYPE = 'U' ORDER BY Name"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            Dim StrSysTables As String = "dtproperties System User Access MnuMast UserGroup GroupAccess"
            Dim IntValue As Integer = 0
            While Rdr.Read
                If InStr(StrSysTables, Rdr("Name")) = 0 Then
                    Dim li As New ListItem
                    li.Text = Rdr("Name")
                    li.Value = IntValue
                    IntValue += 1
                    LstTables.Items.Add(li)
                End If
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButSelTabOne_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButSelTabOne.Click
        On Error Resume Next
        LstSelTables.Items.Add(LstTables.Items(LstTables.SelectedIndex).Text)
        LstTables.Items.Remove(LstTables.SelectedItem)
    End Sub

    Protected Sub LstSelTables_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles LstSelTables.SelectedIndexChanged
        LstFields.Items.Clear()
        Cmd.Connection = Con
        'StrSql = "SELECT NAME FROM SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE NAME = '" & LstSelTables.Text & "')"
        StrSql = "SELECT column_id, name"
        StrSql = StrSql & " FROM sys.columns"
        StrSql = StrSql & " WHERE object_id = Object_id('" & LstSelTables.Text & "')"
        StrSql = StrSql & " ORDER BY column_id"

        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        While Rdr.Read()
            LstFields.Items.Add(Rdr("Name"))
        End While
        Rdr.Close()
        If LstSelTables.SelectedIndex = 0 And TxtJoin.Text = "" Then
            TxtJoin.Text = " FROM [" & LstSelTables.SelectedItem.Text & "]"
        End If
    End Sub

    Protected Sub ButSelTabAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButSelTabAll.Click
        For Each Item As ListItem In LstTables.Items
            LstSelTables.Items.Add(Item.Text)
        Next
        LstTables.Items.Clear()
    End Sub

    Protected Sub ButUnSelTabOne_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButUnSelTabOne.Click
        On Error Resume Next
        LstTables.Items.Add(LstSelTables.SelectedItem)
        For I As Integer = LstSelFields.Items.Count - 1 To 0 Step -1
            If InStr(LstSelFields.Items(I).Text, LstSelTables.SelectedItem.Text) > 0 Then
                LstSelFields.Items.RemoveAt(I)
            End If
        Next
        LstFields.Items.Clear()
        LstSelTables.Items.Remove(LstSelTables.SelectedItem)
        SortList(LstTables)
        TxtJoin.Text = ""
    End Sub

    Protected Sub ButUnSelTabAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButUnSelTabAll.Click
        For Each Item As ListItem In LstSelTables.Items
            LstTables.Items.Add(Item.Text)
        Next
        LstSelTables.Items.Clear()
        LstFields.Items.Clear()
        LstSelFields.Items.Clear()
        TxtSql.Text = ""
        TxtJoin.Text = ""
        CmbType.SelectedIndex = 0
        CmbCompare.SelectedIndex = 0
        SortList(LstTables)
    End Sub

    Protected Sub ButSelFldOne_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButSelFldOne.Click
        On Error Resume Next
        Dim StrItem As String
        StrItem = "[" & LstSelTables.SelectedItem.Text & "].[" & LstFields.SelectedItem.Text & "]"
        If StrItem <> "" Then
            LstSelFields.Items.Add(StrItem)
            GetSql()
        End If
    End Sub

    Protected Sub ButSelFldAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButSelFldAll.Click
        Dim StrItem As String
        For Each Item As ListItem In LstFields.Items
            StrItem = "[" & LstSelTables.SelectedItem.Text & "].[" & Item.Text & "]"
            LstSelFields.Items.Add(StrItem)
        Next
        GetSql()
    End Sub

    Private Sub GetSql()
        Dim StrSql As String = ""
        For Each Item As ListItem In LstSelFields.Items
            If StrSql = "" Then
                StrSql = StrSql & "SELECT "
            Else
                StrSql = StrSql & ","
            End If
            StrSql = StrSql & Item.Text
        Next
        If LstSelTables.Items.Count > 1 Then
            StrSql = StrSql & TxtJoin.Text
        Else
            StrSql = StrSql & " FROM [" & LstSelTables.Items(0).Text & "]"
        End If
        TxtSql.Text = StrSql
    End Sub

    Protected Sub ButApply_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButApply.Click
        Dim ACompare() As String
        ACompare = New String() {"=", "<>", ">", "<", ">=", "<=", "Is Null", "Is Not Null", "Like"}

        On Error Resume Next
        If CmbCompare.SelectedIndex < 0 Then
            MsgBox("Please select a Comparison operator!", vbCritical, "Operator Required")
            Exit Sub
        End If

        If InStr(TxtSql.Text, "WHERE") = 0 Then
            StrSql = TxtSql.Text & " WHERE "
        Else
            StrSql = TxtSql.Text & IIf(OptAnd.Checked, " AND ", " OR ")
        End If
        StrSql = StrSql & "[" & LstSelTables.Items(LstSelTables.SelectedIndex).Text & "].[" & LstFields.Items(LstFields.SelectedIndex).Text & "]"
        StrSql = StrSql & " " & ACompare(CmbCompare.SelectedIndex)
        If TxtValue.Text <> "" Then
            StrSql = StrSql & " " & TxtValue.Text
        End If
        TxtSql.Text = StrSql
    End Sub

    Protected Sub CmbType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbType.SelectedIndexChanged
        If CmbType.Text <> "Select..." Then
            If TxtJoin.Text <> "" Then
                TxtJoin.Text = TxtJoin.Text & " " & CmbType.Text & " [" & LstSelTables.SelectedItem.Text & "] ON"
            End If
        End If
    End Sub

    Protected Sub ButJoinFld_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButJoinFld.Click
        If Microsoft.VisualBasic.Right(TxtJoin.Text, 2) = "ON" Then
            TxtJoin.Text = TxtJoin.Text & " [" & LstSelTables.SelectedItem.Text & "].[" & LstFields.SelectedItem.Text & "] = "
        Else
            TxtJoin.Text = TxtJoin.Text & " [" & LstSelTables.SelectedItem.Text & "].[" & LstFields.SelectedItem.Text & "]"
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Dim ExcelGrid As New GridView()
            Dim DtProfileUsers As New DataTable("Table")
            Cmd.Connection = Con
            Cmd.CommandText = TxtSql.Text
            Rdr = Cmd.ExecuteReader
            ExcelGrid.DataSource = Rdr
            ' it's my DataTable Name  
            ExcelGrid.DataBind()
            Rdr.Close()
            Dim ExcelTable As New Table()
            ExcelTable.GridLines = GridLines.Both
            ExcelTable.Rows.Add(ExcelGrid.HeaderRow)
            For Each GdvRow As GridViewRow In ExcelGrid.Rows
                ExcelTable.Rows.Add(GdvRow)
            Next
            Dim wrt As New IO.StringWriter()
            Dim htw As New HtmlTextWriter(wrt)
            ExcelTable.RenderControl(htw)
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=CustomRep-" & DateTime.Today & ".xls")
            Response.ContentType = "application/ms-excel"
            Response.Write(wrt.ToString())
            Response.[End]()
        End If
    End Sub

    Protected Sub ButUnSelFldOne_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButUnSelFldOne.Click
        LstSelFields.Items.Remove(LstSelFields.SelectedItem)
        GetSql()
    End Sub

    Protected Sub ButUnSelFldAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButUnSelFldAll.Click
        LstSelFields.Items.Clear()
        TxtSql.Text = ""
    End Sub

    Private Sub SortList(ByVal LstBox As ListBox)
        Dim AList As ArrayList = New ArrayList
        For Each Item As ListItem In LstBox.Items
            AList.Add(Item.Text)
        Next
        AList.Sort()
        LstBox.Items.Clear()
        For Each Item As String In AList
            LstBox.Items.Add(Item)
        Next
    End Sub

End Class