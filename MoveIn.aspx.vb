﻿Public Partial Class MoveIn
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbFPZone.Items.Add("Select...")
            While Rdr.Read
                CmbFPZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            TabContainer1.ActiveTabIndex = 0
        End If
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
        StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID > '" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevious.Click
        StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID < '" & TxtClientID.Text & "' ORDER BY ClientID DESC"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
        If RbtName.Checked Then
            StrSql = StrSql & "Name"
        ElseIf RbtCity.Checked Then
            StrSql = StrSql & "City"
        Else
            StrSql = StrSql & "Contact"
        End If
        StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        PnlFind.Height = 400
        PnlFind.Width = 600
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtClientID_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvCabs.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCabs.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvOpReq.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvOpReq.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            ClearAll()
            TxtClientID.Text = Rdr("ClientID")
            TxtCategory.Text = Trim(Rdr("Category").ToString)
            TxtName.Text = Rdr("Name").ToString
            TxtAddress.Text = Rdr("Address").ToString
            TxtCity.Text = Rdr("City").ToString
            TxtTelephone.Text = Rdr("Telephone").ToString
            TxtFax.Text = Rdr("Fax").ToString
            TxtEmail.Text = Rdr("EMail").ToString
            TxtContact.Text = Rdr("Contact").ToString
            TxtPosition.Text = Rdr("Position").ToString
            TxtDate.Text = Format(Rdr("DateCreated"), "dd-MMM-yyyy")
            TxtVatNo.Text = Rdr("VATNo").ToString
            TxtLicenceNo.Text = Rdr("LicenceNo").ToString
            TxtStoragePermit.Text = Rdr("StoragePermit").ToString
            TxtLicenceExp.Text = Format(Rdr("LicenceExp"), "dd-MMM-yyyy")
            TxtStorageExp.Text = Format(Rdr("PermitExp"), "dd-MMM-yyyy")
            CmbStatus.Text = Rdr("Status").ToString
            TxtProdType.Text = Rdr("ProdType").ToString
            TxtFPActivity.Text = Rdr("Activity").ToString
            TxtZone.Text = Rdr("WHouses").ToString
            Rdr.Close()
            UpDateCabins()
            UpDateReq()
            TxtMoveIn.Focus()
        Else
            Rdr.Close()
            ClearAll()
        End If
    End Sub

    Private Sub UpDateReq()
        StrSql = "SELECT OpReq,Follow,Who,CASE WHEN Done =1 THEN 'True' ELSE 'False' End As Done FROM OpReq WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvOpReq.DataSource = Rdr
        GdvOpReq.DataBind()
        Rdr.Close()
    End Sub

    Private Sub UpDateCabins()
        StrSql = "SELECT * FROM ClientCabin WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvCabs.DataSource = Rdr
        GdvCabs.DataBind()
        Rdr.Close()
    End Sub

    Private Sub CmbCabin_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbCabin.TextChanged
        Dim BlnNew As Boolean
        BlnNew = True
        Dim IRow As Integer
        For Each Row As GridViewRow In GdvCabs.Rows
            If Row.Cells(0).Text = CmbFPZone.Text Then
                If Row.Cells(1).Text = CmbCabin.Text Then
                    BlnNew = False
                    IRow = Row.RowIndex
                    Exit For
                End If
            End If
        Next
        If BlnNew Then
            TxtArea.Text = ""
            TxtMonthlyRent.Text = ""
            TxtDepositAmt.Text = ""
            TxtGuaranteeNo.Text = ""
            TxtGuaranteeExp.Text = Date.Today.AddYears(1).ToString("dd-MMM-yyyy")
            TxtOpHours.Text = ""
            TxtDateofEntry.Text = TxtMoveIn.Text
            TxtInsurance.Text = ""
            TxtLeaseOn.Text = Date.Today.ToString("dd-MMM-yyyy")
            TxtRate.Text = ""
            TxtElecCharges.Text = ""
            TxtWaterCharge.Text = ""
            TxtWasteWater.Text = ""
            TxtTelCharges.Text = ""
            TxtDepositPaidOn.Text = Date.Today.ToString("dd-MMM-yyyy")
            TxtAddRemarks.Text = ""
            TxtLeaseExp.Text = Date.Today.AddYears(1).ToString("dd-MMM-yyyy")
        Else
            Dim Row As GridViewRow = GdvCabs.Rows(IRow)
            TxtArea.Text = Row.Cells(2).Text
            TxtMonthlyRent.Text = Row.Cells(3).Text
            TxtDepositAmt.Text = Row.Cells(4).Text
            TxtGuaranteeNo.Text = Row.Cells(5).Text
            TxtGuaranteeExp.Text = Row.Cells(6).Text
            TxtOpHours.Text = Row.Cells(7).Text
            TxtDateofEntry.Text = Row.Cells(8).Text
            TxtInsurance.Text = Row.Cells(9).Text
            TxtLeaseOn.Text = Row.Cells(10).Text
            TxtRate.Text = Row.Cells(11).Text
            TxtElecCharges.Text = Row.Cells(12).Text
            TxtWaterCharge.Text = Row.Cells(13).Text
            TxtWasteWater.Text = Row.Cells(14).Text
            TxtTelCharges.Text = Row.Cells(15).Text
            TxtDepositPaidOn.Text = Row.Cells(16).Text
            TxtAddRemarks.Text = Row.Cells(17).Text
            TxtLeaseExp.Text = Row.Cells(18).Text
            If TxtGuaranteeNo.Text = "&nbsp;" Then TxtGuaranteeNo.Text = ""
            If TxtOpHours.Text = "&nbsp;" Then TxtOpHours.Text = ""
            If TxtAddRemarks.Text = "&nbsp;" Then TxtOpHours.Text = ""
        End If
    End Sub

    Protected Sub CmbFPZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbFPZone.SelectedIndexChanged
        StrSql = "SELECT * FROM Zones WHERE WhCode = '" & CmbFPZone.Text & "'"
        CmbCabin.Items.Clear()
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        CmbCabin.Items.Add("Select...")
        While Rdr.Read
            CmbCabin.Items.Add(Rdr("ZoneCode"))
        End While
        Rdr.Close()
    End Sub

    Private Sub ClearAll()
        TxtMoveIn.Text = Date.Today.ToString("dd-MMM-yyyy")
        TxtCategory.Text = ""
        TxtName.Text = ""
        TxtAddress.Text = ""
        TxtCity.Text = ""
        TxtTelephone.Text = ""
        TxtFax.Text = ""
        TxtEmail.Text = ""
        TxtContact.Text = ""
        TxtPosition.Text = ""
        TxtVatNo.Text = ""
        TxtLicenceNo.Text = ""
        TxtStoragePermit.Text = ""
        TxtProdType.Text = ""
        TxtFPActivity.Text = ""
        TxtZone.Text = ""
        TxtDate.Text = ""
        TxtLicenceExp.Text = ""
        TxtStorageExp.Text = ""
        CmbStatus.SelectedIndex = -1
        CmbFPZone.SelectedIndex = 0
        CmbCabin.Items.Clear()
        CmbCabin.Items.Add("Select...")
        GdvCabs.DataSource = Nothing
        GdvCabs.DataBind()
        GdvOpReq.DataSource = Nothing
        GdvOpReq.DataBind()
    End Sub

    Protected Sub ButAddCab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAddCab.Click
        For Each Row As GridViewRow In GdvCabs.Rows
            If Row.Cells(0).Text = CmbFPZone.Text And Row.Cells(1).Text = CmbCabin.Text Then
                Row.Cells(2).Text = TxtArea.Text
                Row.Cells(3).Text = TxtMonthlyRent.Text
                Row.Cells(4).Text = TxtDepositAmt.Text
                Row.Cells(5).Text = TxtGuaranteeNo.Text
                Row.Cells(6).Text = TxtGuaranteeExp.Text
                Row.Cells(7).Text = TxtOpHours.Text
                Row.Cells(8).Text = TxtDateofEntry.Text
                Row.Cells(9).Text = TxtInsurance.Text
                Row.Cells(10).Text = TxtLeaseOn.Text
                Row.Cells(11).Text = TxtRate.Text
                Row.Cells(12).Text = TxtElecCharges.Text
                Row.Cells(13).Text = TxtWaterCharge.Text
                Row.Cells(14).Text = TxtWasteWater.Text
                Row.Cells(15).Text = TxtTelCharges.Text
                Row.Cells(16).Text = TxtDepositPaidOn.Text
                Row.Cells(17).Text = TxtAddRemarks.Text
                Row.Cells(18).Text = TxtLeaseExp.Text
                Exit Sub
            End If
        Next
        Dim Dt As New DataTable
        Dim Dr As DataRow
        StrSql = "SELECT WhCode,CabinNo,Capacity,Rent,Deposit,Guarantee,ExpDate,OpHours,EntryDate,Insurance,LeaseCommence,RateSqFt,Electricity,"
        StrSql = StrSql & "Water,Waste,Telephone,DepositPaid,AddRemarks,LeaseExpDate FROM ClientCabin WHERE ClientID = NULL"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        Dt.Load(Rdr)
        For Each Row As GridViewRow In GdvCabs.Rows
            Dr = Dt.NewRow
            For i = 0 To GdvCabs.Columns.Count - 1
                Dr.Item(i) = Row.Cells(i).Text
            Next
            Dt.Rows.Add(Dr)
        Next
        Dr = Dt.NewRow
        Dr.Item(0) = CmbFPZone.Text
        Dr.Item(1) = CmbCabin.Text
        Dr.Item(2) = ConSng(TxtArea.Text)
        Dr.Item(3) = ConSng(TxtMonthlyRent.Text)
        Dr.Item(4) = ConSng(TxtDepositAmt.Text)
        Dr.Item(5) = TxtGuaranteeNo.Text
        Dr.Item(6) = IIf(IsDate(TxtGuaranteeExp.Text), TxtGuaranteeExp.Text, DBNull.Value)
        Dr.Item(7) = TxtOpHours.Text
        Dr.Item(8) = IIf(IsDate(TxtDateofEntry.Text), TxtDateofEntry.Text, DBNull.Value)
        Dr.Item(9) = ConSng(TxtInsurance.Text)
        Dr.Item(10) = IIf(IsDate(TxtLeaseOn.Text), TxtLeaseOn.Text, DBNull.Value)
        Dr.Item(11) = ConSng(TxtRate.Text)
        Dr.Item(12) = ConSng(TxtElecCharges.Text)
        Dr.Item(13) = ConSng(TxtWaterCharge.Text)
        Dr.Item(14) = ConSng(TxtWasteWater.Text)
        Dr.Item(15) = ConSng(TxtTelCharges.Text)
        Dr.Item(16) = IIf(IsDate(TxtDepositPaidOn.Text), TxtDepositPaidOn.Text, DBNull.Value)
        Dr.Item(17) = TxtAddRemarks.Text
        Dr.Item(18) = IIf(IsDate(TxtLeaseExp.Text), TxtLeaseExp.Text, DBNull.Value)
        Dt.Rows.Add(Dr)
        GdvCabs.DataSource = Dt
        GdvCabs.DataBind()
    End Sub

    Private Function ConSng(ByVal Text As String) As Single
        If Text = "" Or Text = "&nbsp;" Then
            Text = 0
        End If
        Return CSng(Text)
    End Function

    Private Sub GdvCabs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvCabs.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvCabs, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvCabs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvCabs.SelectedIndexChanged
        Dim Row As GridViewRow = GdvCabs.SelectedRow
        CmbFPZone.Text = Row.Cells(0).Text
        CmbFPZone_SelectedIndexChanged(Me, e)
        CmbCabin.Text = Row.Cells(1).Text
        TxtArea.Text = Row.Cells(2).Text
        TxtMonthlyRent.Text = Row.Cells(3).Text
        TxtDepositAmt.Text = Row.Cells(4).Text
        TxtGuaranteeNo.Text = Row.Cells(5).Text
        TxtGuaranteeExp.Text = Row.Cells(6).Text
        TxtOpHours.Text = Row.Cells(7).Text
        TxtDateofEntry.Text = Row.Cells(8).Text
        TxtInsurance.Text = Row.Cells(9).Text
        TxtLeaseOn.Text = Row.Cells(10).Text
        TxtRate.Text = Row.Cells(11).Text
        TxtElecCharges.Text = Row.Cells(12).Text
        TxtWaterCharge.Text = Row.Cells(13).Text
        TxtWasteWater.Text = Row.Cells(14).Text
        TxtTelCharges.Text = Row.Cells(15).Text
        TxtDepositPaidOn.Text = Row.Cells(16).Text
        TxtAddRemarks.Text = Row.Cells(17).Text
        TxtLeaseExp.Text = Row.Cells(18).Text
        If TxtGuaranteeNo.Text = "&nbsp;" Then TxtGuaranteeNo.Text = ""
        If TxtOpHours.Text = "&nbsp;" Then TxtOpHours.Text = ""
    End Sub

    Private Sub GdvCabs_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvCabs.SelectedIndexChanging
        GdvCabs.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub TxtArea_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtArea.TextChanged
        TxtMonthlyRent.Text = ConSng(TxtArea.Text) * ConSng(TxtRate.Text)
    End Sub

    Private Sub TxtRate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtRate.TextChanged
        TxtMonthlyRent.Text = ConSng(TxtArea.Text) * ConSng(TxtRate.Text)
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Dim PstrDateFormat As String = "dd-MMM-yyyy"
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            'Update Client First Date
            StrSql = "UPDATE Client SET FirstDate = '" & TxtMoveIn.Text & "' WHERE ClientID = '" & TxtClientID.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            'Delete records from ClientCabin and OpReq
            Cmd.CommandText = "DELETE FROM ClientCabin WHERE ClientID = '" & TxtClientID.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "DELETE FROM OpReq WHERE ClientID = '" & TxtClientID.Text & "'"
            Cmd.ExecuteNonQuery()
            'Save Cabin Details to ClientCabin
            For Each Row As GridViewRow In GdvCabs.Rows
                StrSql = "INSERT INTO ClientCabin VALUES('"
                StrSql = StrSql & TxtClientID.Text & "','"
                StrSql = StrSql & Row.Cells(1).Text & "',"
                StrSql = StrSql & Row.Cells(2).Text & ","
                StrSql = StrSql & "'SqFt','"
                StrSql = StrSql & Row.Cells(0).Text & "',"
                StrSql = StrSql & ConSng(Row.Cells(3).Text) & ","
                StrSql = StrSql & ConSng(Row.Cells(4).Text) & ",'"
                StrSql = StrSql & ChkSpc(Row.Cells(5).Text) & "','"
                StrSql = StrSql & Format(CDate(Row.Cells(6).Text), PstrDateFormat) & "','"
                StrSql = StrSql & ChkSpc(Row.Cells(7).Text) & "','"
                StrSql = StrSql & Format(CDate(Row.Cells(8).Text), PstrDateFormat) & "',"
                StrSql = StrSql & ConSng(Row.Cells(9).Text) & ",'"
                StrSql = StrSql & Format(CDate(Row.Cells(18).Text), PstrDateFormat) & "',"
                StrSql = StrSql & "'',"
                StrSql = StrSql & "'',"
                StrSql = StrSql & "'','"
                StrSql = StrSql & Format(CDate(Row.Cells(10).Text), PstrDateFormat) & "',"
                StrSql = StrSql & ConSng(Row.Cells(11).Text) & ","
                StrSql = StrSql & ConSng(Row.Cells(12).Text) & ","
                StrSql = StrSql & ConSng(Row.Cells(13).Text) & ","
                StrSql = StrSql & ConSng(Row.Cells(14).Text) & ","
                StrSql = StrSql & ConSng(Row.Cells(15).Text) & ",'"
                StrSql = StrSql & Format(CDate(Row.Cells(16).Text), PstrDateFormat) & "','"
                StrSql = StrSql & ChkSpc(Row.Cells(17).Text) & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                'Update Cabin 
                StrSql = "UPDATE Zones SET ClientID = '" & TxtClientID.Text & "',"
                StrSql = StrSql & "StartDate = '" & Format(CDate(Row.Cells(8).Text), PstrDateFormat) & "' WHERE WhCode = '" & Row.Cells(0).Text & "' "
                StrSql = StrSql & "AND ZoneCode = '" & Row.Cells(1).Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            Next
            For Each Row As GridViewRow In GdvOpReq.Rows
                StrSql = "INSERT INTO OpReq VALUES('"
                StrSql = StrSql & TxtClientID.Text & "','"
                StrSql = StrSql & ChkSpc(Row.Cells(0).Text) & "','"
                StrSql = StrSql & ChkSpc(Row.Cells(1).Text) & "','"
                StrSql = StrSql & ChkSpc(Row.Cells(2).Text) & "',"
                StrSql = StrSql & IIf(DirectCast(Row.Cells(3).Controls(0), CheckBox).Checked, 1, 0) & ")"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            Next
            Trans.Commit()
            ClearAll()
            TabContainer1.ActiveTabIndex = 0
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Save Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub GdvOpReq_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvOpReq.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvOpReq, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvOpReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvOpReq.SelectedIndexChanged
        Dim Row As GridViewRow = GdvOpReq.SelectedRow
        TxtOpReq.Text = Row.Cells(0).Text
        TxtFollow.Text = Row.Cells(1).Text
        TxtWho.Text = Row.Cells(2).Text
        ChkDone.Checked = DirectCast(Row.Cells(3).Controls(0), CheckBox).Checked
    End Sub

    Private Sub GdvOpReq_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvOpReq.SelectedIndexChanging
        GdvOpReq.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub ButAddReq_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAddReq.Click
        Dim Dt As New DataTable
        Dim Dr As DataRow
        StrSql = "SELECT OpReq,Follow,Who,Done FROM OpReq WHERE ClientID = NULL"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        Dt.Load(Rdr)
        For Each Row As GridViewRow In GdvOpReq.Rows
            If Row.Cells(0).Text <> TxtOpReq.Text Then
                Dr = Dt.NewRow
                For i = 0 To GdvOpReq.Columns.Count - 1
                    If i = 3 Then
                        Dr.Item(i) = DirectCast(Row.Cells(3).Controls(0), CheckBox).Checked
                    Else
                        Dr.Item(i) = Row.Cells(i).Text
                    End If
                Next
                Dt.Rows.Add(Dr)
            End If
        Next
        Dr = Dt.NewRow
        Dr.Item(0) = TxtOpReq.Text
        Dr.Item(1) = TxtFollow.Text
        Dr.Item(2) = TxtWho.Text
        Dr.Item(3) = ChkDone.Checked
        Dt.Rows.Add(Dr)
        GdvOpReq.DataSource = Dt
        GdvOpReq.DataBind()
    End Sub

    Protected Sub ButDelReq_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDelReq.Click
        Dim Dt As New DataTable
        Dim Dr As DataRow
        StrSql = "SELECT OpReq,Follow,Who,Done FROM OpReq WHERE ClientID = NULL"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        Dt.Load(Rdr)
        For Each Row As GridViewRow In GdvOpReq.Rows
            If Row.Cells(0).Text <> TxtOpReq.Text Then
                Dr = Dt.NewRow
                For i = 0 To GdvOpReq.Columns.Count - 1
                    If i = 3 Then
                        Dr.Item(i) = DirectCast(Row.Cells(3).Controls(0), CheckBox).Checked
                    Else
                        Dr.Item(i) = Row.Cells(i).Text
                    End If
                Next
                Dt.Rows.Add(Dr)
            End If
        Next
        GdvOpReq.DataSource = Dt
        GdvOpReq.DataBind()
    End Sub

    Protected Sub ButDelCab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDelCab.Click
        Dim Dt As New DataTable
        Dim Dr As DataRow
        StrSql = "SELECT WhCode,CabinNo,Capacity,Rent,Deposit,Guarantee,ExpDate,OpHours,EntryDate,Insurance,LeaseCommence,RateSqFt,Electricity,"
        StrSql = StrSql & "Water,Waste,Telephone,DepositPaid,AddRemarks,LeaseExpDate FROM ClientCabin WHERE ClientID = NULL"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        Dt.Load(Rdr)
        For Each Row As GridViewRow In GdvCabs.Rows
            If Row.Cells(1).Text <> CmbCabin.Text Then
                Dr = Dt.NewRow
                For i = 0 To GdvCabs.Columns.Count - 1
                    Dr.Item(i) = Row.Cells(i).Text
                Next
                Dt.Rows.Add(Dr)
            End If
        Next
        GdvCabs.DataSource = Dt
        GdvCabs.DataBind()
    End Sub

    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNew.Click
        ClearAll()
        TabContainer1.ActiveTabIndex = 0
        TxtClientID.Focus()
    End Sub

    Private Function ChkSpc(ByVal Text As String) As String
        If Text = "&nbsp;" Then
            Text = ""
        End If
        Return Text
    End Function
End Class