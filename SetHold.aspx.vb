﻿Public Partial Class SetHold
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbFpZone.Items.Add("Select...")
            While Rdr.Read
                CmbFpZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            StrSql = "SELECT ZoneCode FROM Zones"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("ZoneCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        StrSql = "UPDATE Location SET "
        If RbnPick.Checked Then
            StrSql = StrSql & "PickHold = "
        Else
            StrSql = StrSql & "PutHold = "
        End If
        If RbnSet.Checked Then
            StrSql = StrSql & " 1, "
        Else
            StrSql = StrSql & " 0, "
        End If
        StrSql = StrSql & "Remarks = '" & TxtRemarks.Text & "' WHERE "
        If RbnRacked.Checked Then
            StrSql = StrSql & "(RackCode  Is Not Null OR RackCode <> '') AND "
        Else
            StrSql = StrSql & "(RackCode Is Null OR RackCode = '') AND "
        End If
        If CmbFpZone.Text <> "Select..." Then
            StrSql = StrSql & " WhCode = '" & CmbFpZone.Text & "' AND "
        End If
        If CmbZone.Text <> "Select..." Then
            StrSql = StrSql & " ZoneCode = '" & CmbZone.Text & "' AND "
        End If
        If TxtLocFrom.Text <> "" Then
            StrSql = StrSql & " LocCode >= '" & TxtLocFrom.Text & "' AND "
        End If
        If TxtLocTo.Text <> "" Then
            StrSql = StrSql & " LocCode <= '" & TxtLocTo.Text & "' AND "
        End If
        If TxtRackFrom.Text <> "" Then
            StrSql = StrSql & " RackCode >= '" & TxtRackFrom.Text & "' AND "
        End If
        If TxtRackTo.Text <> "" Then
            StrSql = StrSql & " RackCode <= '" & TxtRackTo.Text & "' AND "
        End If
        If TxtAisleFrom.Text <> "" Then
            StrSql = StrSql & " Aisle >= " & TxtAisleFrom.Text & " AND "
        End If
        If TxtAisleTo.Text <> "" Then
            StrSql = StrSql & " Aisle <= " & TxtAisleTo.Text & " AND "
        End If
        If TxtSideFrom.Text <> "" Then
            StrSql = StrSql & " Side >= " & TxtSideFrom.Text & " AND "
        End If
        If TxtSideTo.Text <> "" Then
            StrSql = StrSql & " Side <= " & TxtSideTo.Text & " AND "
        End If
        If TxtBayFrom.Text <> "" Then
            StrSql = StrSql & " Bay >= " & TxtBayFrom.Text & " AND "
        End If
        If TxtBayTo.Text <> "" Then
            StrSql = StrSql & " Bay <= " & TxtBayTo.Text & " AND "
        End If
        If TxtlevelFrom.Text <> "" Then
            StrSql = StrSql & " Level >= " & TxtlevelFrom.Text & " AND "
        End If
        If TxtLevelTo.Text <> "" Then
            StrSql = StrSql & " Level <= " & TxtLevelTo.Text & " AND "
        End If
        If TxtPosFrom.Text <> "" Then
            StrSql = StrSql & " PalletPosition >= " & TxtPosFrom.Text & " AND "
        End If
        If TxtPosTo.Text <> "" Then
            StrSql = StrSql & " PalletPosition >= " & TxtPosTo.Text
        End If
        If Right(StrSql, 4) = "AND " Then
            StrSql = Left(StrSql, Len(StrSql) - 4)
        End If
        If Right(StrSql, 6) = "WHERE " Then
            StrSql = Left(StrSql, Len(StrSql) - 6)
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Try
            Cmd.ExecuteNonQuery()
            ButRefresh_Click(Me, e)
        Catch ex As Exception
            ErrHead.Text = "Set / Unset Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT * FROM Location WHERE "
        If RbnRacked.Checked Then
            StrSql = StrSql & "(RackCode  Is Not Null OR RackCode <> '') AND "
        Else
            StrSql = StrSql & "(RackCode Is Null OR RackCode = '') AND "
        End If
        If CmbFpZone.Text <> "Select..." Then
            StrSql = StrSql & " WhCode = '" & CmbFpZone.Text & "' AND "
        End If
        If CmbZone.Text <> "Select..." Then
            StrSql = StrSql & " ZoneCode = '" & CmbZone.Text & "' AND "
        End If
        If TxtLocFrom.Text <> "" Then
            StrSql = StrSql & " LocCode >= '" & TxtLocFrom.Text & "' AND "
        End If
        If TxtLocTo.Text <> "" Then
            StrSql = StrSql & " LocCode <= '" & TxtLocTo.Text & "' AND "
        End If
        If TxtRackFrom.Text <> "" Then
            StrSql = StrSql & " RackCode >= '" & TxtRackFrom.Text & "' AND "
        End If
        If TxtRackTo.Text <> "" Then
            StrSql = StrSql & " RackCode <= '" & TxtRackTo.Text & "' AND "
        End If
        If TxtAisleFrom.Text <> "" Then
            StrSql = StrSql & " Aisle >= " & TxtAisleFrom.Text & " AND "
        End If
        If TxtAisleTo.Text <> "" Then
            StrSql = StrSql & " Aisle <= " & TxtAisleTo.Text & " AND "
        End If
        If TxtSideFrom.Text <> "" Then
            StrSql = StrSql & " Side >= " & TxtSideFrom.Text & " AND "
        End If
        If TxtSideTo.Text <> "" Then
            StrSql = StrSql & " Side <= " & TxtSideTo.Text & " AND "
        End If
        If TxtBayFrom.Text <> "" Then
            StrSql = StrSql & " Bay >= " & TxtBayFrom.Text & " AND "
        End If
        If TxtBayTo.Text <> "" Then
            StrSql = StrSql & " Bay <= " & TxtBayTo.Text & " AND "
        End If
        If TxtlevelFrom.Text <> "" Then
            StrSql = StrSql & " Level >= " & TxtlevelFrom.Text & " AND "
        End If
        If TxtLevelTo.Text <> "" Then
            StrSql = StrSql & " Level <= " & TxtLevelTo.Text & " AND "
        End If
        If TxtPosFrom.Text <> "" Then
            StrSql = StrSql & " PalletPosition >= " & TxtPosFrom.Text & " AND "
        End If
        If TxtPosTo.Text <> "" Then
            StrSql = StrSql & " PalletPosition >= " & TxtPosTo.Text
        End If
        If Right(StrSql, 4) = "AND " Then
            StrSql = Left(StrSql, Len(StrSql) - 4)
        End If
        If Right(StrSql, 6) = "WHERE " Then
            StrSql = Left(StrSql, Len(StrSql) - 6)
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvLoc.DataSource = Rdr
        GdvLoc.DataBind()
        Rdr.Close()
    End Sub

    Private Sub GdvLoc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLoc.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLoc, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvLoc.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLoc.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub GdvLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLoc.SelectedIndexChanged
        If TxtLocFrom.Text = "" Then
            TxtLocFrom.Text = GdvLoc.SelectedRow.Cells(0).Text
        Else
            TxtLocTo.Text = GdvLoc.SelectedRow.Cells(0).Text
        End If
    End Sub

    Private Sub GdvLoc_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLoc.SelectedIndexChanging
        GdvLoc.SelectedIndex = e.NewSelectedIndex
    End Sub
End Class