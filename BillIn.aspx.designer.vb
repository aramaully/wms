﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class BillIn
    
    '''<summary>
    '''Head1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead
    
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Panel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel1 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Label1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ButNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButNew As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButSave As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButSave_ConfirmButtonExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButSave_ConfirmButtonExtender As Global.AjaxControlToolkit.ConfirmButtonExtender
    
    '''<summary>
    '''ButFind control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButFind As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButDelete As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButDelete_ConfirmButtonExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButDelete_ConfirmButtonExtender As Global.AjaxControlToolkit.ConfirmButtonExtender
    
    '''<summary>
    '''TabContainer1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TabContainer1 As Global.AjaxControlToolkit.TabContainer
    
    '''<summary>
    '''TabPanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TabPanel1 As Global.AjaxControlToolkit.TabPanel
    
    '''<summary>
    '''CmbZone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CmbZone As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''LblText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblText As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''TxtDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtDate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TxtDate_CalendarExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtDate_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''TxtBillIn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtBillIn As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TxtTruckNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtTruckNo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TxtClientID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtClientID As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ButFindClient control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButFindClient As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''TxtName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtName As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TxtItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtItems As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''CmbTrafficOp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CmbTrafficOp As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''TxtCont control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtCont As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''CmbCPC control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CmbCPC As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''TxtRemarks control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtRemarks As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Label6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ButAddCont control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButAddCont As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButDelCont control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButDelCont As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Panel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel2 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''GdvCont control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GdvCont As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''TabPanel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TabPanel2 As Global.AjaxControlToolkit.TabPanel
    
    '''<summary>
    '''Panel6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel6 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Label2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''TxtHSItemNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtHSItemNo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TxtHsCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtHsCode As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ButNewHSItem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButNewHSItem As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButAddHS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButAddHS As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButDeleteHS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButDeleteHS As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''TxtHSDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtHSDescription As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TxtHsQuantity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtHsQuantity As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''CmbHSUnit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CmbHSUnit As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''TxtHsWeight control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtHsWeight As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TxtFOB control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtFOB As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TxtCIF control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtCIF As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''CmbHSOrigin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CmbHSOrigin As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Label3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''TxtProductCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtProductCode As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ButNewProd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButNewProd As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButFindProd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButFindProd As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButAddProd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButAddProd As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButDelProd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButDelProd As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''TxtItemDesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtItemDesc As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TxtProdQtyUnit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtProdQtyUnit As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''CmbProdUnit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CmbProdUnit As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''CmbItemType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CmbItemType As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''TxtDateIn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtDateIn As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TxtDateIn_CalendarExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtDateIn_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''PnlHs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PnlHs As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''GdvHSCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GdvHSCode As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''PnlProd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PnlProd As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''GdvProduct control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GdvProduct As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Panel9 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel9 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''img1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents img1 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''PnlAmend control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PnlAmend As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''UpdatePanel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Label4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Label5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''TxtAmend control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtAmend As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ButCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButCancel As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ChkNoAmend control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ChkNoAmend As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''ChkClearDisc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ChkClearDisc As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''DragPanel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DragPanel As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''HfLeft control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HfLeft As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''HfTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HfTop As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''PnlFind control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PnlFind As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''RbtName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RbtName As Global.System.Web.UI.WebControls.RadioButton
    
    '''<summary>
    '''ChkPotential control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ChkPotential As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''RbtCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RbtCity As Global.System.Web.UI.WebControls.RadioButton
    
    '''<summary>
    '''RbtContact control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RbtContact As Global.System.Web.UI.WebControls.RadioButton
    
    '''<summary>
    '''Label7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label7 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''TxtSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtSearch As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ButRefresh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButRefresh As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButSelect control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButSelect As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Panel8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel8 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''GdvFind control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GdvFind As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''PnlFind_RoundedCornersExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PnlFind_RoundedCornersExtender As Global.AjaxControlToolkit.RoundedCornersExtender
    
    '''<summary>
    '''PnlFindProp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PnlFindProp As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Image2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image2 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''LblSearchHead control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblSearchHead As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ButCloseProp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButCloseProp As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Panel5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel5 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''GdvProposal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GdvProposal As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''PnlFindProp_RoundedCornersExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PnlFindProp_RoundedCornersExtender As Global.AjaxControlToolkit.RoundedCornersExtender
    
    '''<summary>
    '''PlnFindLease control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlnFindLease As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Image1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image1 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''Label8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label8 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ButCloseLease control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButCloseLease As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Panel7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel7 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''GdvFindLease control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GdvFindLease As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''PlnFindLease_RoundedCornersExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlnFindLease_RoundedCornersExtender As Global.AjaxControlToolkit.RoundedCornersExtender
    
    '''<summary>
    '''ShowAmend control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ShowAmend As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ModalPopupExtender2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ModalPopupExtender2 As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''ErrorPanel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ErrorPanel As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''ErrorUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ErrorUpdate As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''ErrHead control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ErrHead As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''LinkButton1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LinkButton1 As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''ErrMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ErrMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ButCloseErr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButCloseErr As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ShowModal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ShowModal As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ModalPopupExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ModalPopupExtender1 As Global.AjaxControlToolkit.ModalPopupExtender
End Class
