﻿Partial Public Class BillOut
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdDel As New OleDb.OleDbCommand
    Dim CmdChkClient As New OleDb.OleDbCommand
    Dim RdrChkClient As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim BlnNew As Boolean
    Dim StrSelectedBills As String

    Const MENUID = "BillOutbound"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            TxtDate.Text = Date.Today.ToString("dd-MMM-yyyy")
            TabContainer1.ActiveTabIndex = 0
            TxtDate.Text = Date.Today.ToString("dd-MMM-yyyy")
            StrSql = "SELECT [Name] FROM Country ORDER BY CountryCode"
            Cmd.CommandText = StrSql
            CmbCountry.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbCountry.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()

            StrSql = "SELECT CPCCode FROM Regime ORDER BY CpcCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbCpc.Items.Add("Select...")
            While Rdr.Read
                CmbCpc.Items.Add(Rdr("CPCCode"))
            End While
            Rdr.Close()

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()

            StrSql = "SELECT Name FROM Traffic ORDER BY Name"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbTrafficOfficer.Items.Add("Select...")
            While Rdr.Read
                CmbTrafficOfficer.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()
            TxtQtyUnit.Style("text-align") = "right"
            TxtBalUnit.Style("text-align") = "right"
            TxtDelUnit.Style("text-align") = "right"
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        If RbnClient.Text = "Name" Then
            StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
            If RbnClient.Checked Then
                StrSql = StrSql & "Name"
            ElseIf RbnCountry.Checked Then
                StrSql = StrSql & "City"
            Else
                StrSql = StrSql & "Contact"
            End If
            StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Else
            StrSql = "SELECT BoeOut.BillNo, CONVERT(Char,BoeOut.BillDate,103) 'Bill Date', BoeOut.Revision, Client.Name FROM BoeOut, Client WHERE "
            StrSql = StrSql & "BoeOut.ClientID = Client.ClientID "
            If RbnClient.Checked Then
                StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCountry.Checked Then
                StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnTraffic.Checked Then
                StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCont.Checked Then
                StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
            End If
            If ChkDate.Checked Then
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                StrSql = StrSql & "AND BoeOut.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
                StrSql = StrSql & "AND BoeOut.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
            End If
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        RbnClient.Text = "Client"
        RbnCountry.Text = "Country"
        RbnTraffic.Text = "Traffic Op"
        RbnCont.Visible = True
        ChkDate.Visible = True
        TxtFrom.Visible = True
        TxtTo.Visible = True
        LblToDate.Visible = True
        GdvFind.DataSource = Nothing
        TxtTo.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtFrom.Text = Date.Today.AddDays(-10).ToString("dd/MM/yyyy")
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientId.TextChanged
        Dim DatBill As New DataTable
        CmdChkClient.Connection = Con
        CmdChkClient.CommandText = "SELECT *FROM Client WHERE ClientID = '" & TxtClientId.Text & "'"
        Cmd.Connection = Con

        CmdChkClient.Connection = Con
        CmdChkClient.CommandText = "SELECT *FROM Client WHERE ClientID = '" & TxtClientId.Text & "'"
        RdrChkClient = CmdChkClient.ExecuteReader
        If RdrChkClient.Read Then
            TxtClientName.Text = RdrChkClient("Name")
            If RdrChkClient("Category") = "Suspended Duty" Then
                If CDate(RdrChkClient("PermitExp")) < CDate(TxtDate.Text) Then
                    ErrHead.Text = "Save Failed"
                    ErrMsg.Text = "License Expired"
                    ModalPopupExtender1.Show()
                    'ClearAll()
                    Exit Sub
                End If
            ElseIf RdrChkClient("Category") = "Freeport" Then
                If CDate(RdrChkClient("LicenceExp")) < CDate(TxtDate.Text) Then
                    ErrHead.Text = "Save Failed"
                    ErrMsg.Text = "License Expired"
                    ModalPopupExtender1.Show()
                    'ClearAll()
                    Exit Sub
                End If
            End If
            'Update BillIn nos in the grid
            StrSql = "SELECT BillNo,CONVERT(Char,BillDate,103) BDate FROM BoeIn WHERE ClientId = '" & TxtClientId.Text & "' Order By BillNo Desc"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            DatBill.Load(Rdr)
            Rdr.Close()
            GdvBill.DataSource = DatBill
            GdvBill.DataBind()
        End If
        RdrChkClient.Close()
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        If RbnClient.Text = "Client" Then
            TxtBillOut.Text = GdvFind.SelectedRow.Cells(0).Text
            TxtBillOut_TextChanged(Me, e)
        Else
            TxtClientId.Text = GdvFind.SelectedRow.Cells(0).Text
            TxtClientID_TextChanged(Me, e)
        End If
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvInItem.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvInItem.UniqueID, "Select$" & i)
        Next
        'For i As Integer = 0 To Me.GdvInProd.Rows.Count - 1
        '    ClientScript.RegisterForEventValidation(Me.GdvInProd.UniqueID, "Select$" & i)
        'Next
        For i As Integer = 0 To Me.GdvOut.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvOut.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub ClearAll()
        TxtClientId.Text = ""
        TxtContainers.Text = ""
        TxtDate.Text = Date.Today.ToString("dd-MMM-yyyy")
        TxtItems.Text = ""
        TxtClientName.Text = ""
        TxtRemarks.Text = ""
        TxtTransport.Text = ""
        TxtCbNo.Text = ""
        TxtDelUnit.Text = ""
        TxtDescription.Text = ""
        TxtProdCode.Text = ""
        TxtQtyUnit.Text = ""
        CmbTrafficOfficer.SelectedIndex = 0
        CmbCountry.SelectedIndex = 0
        CmbCpc.SelectedIndex = 0
        CmbZone.SelectedIndex = 0
        GdvCont.DataSource = Nothing
        GdvCont.DataBind()
        GdvInItem.DataSource = Nothing
        GdvInItem.DataBind()
        GdvBill.DataSource = Nothing
        GdvBill.DataBind()
        GdvOut.DataSource = Nothing
        GdvOut.DataBind()
    End Sub

    Protected Sub TxtBillOut_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtBillOut.TextChanged
        Dim StrContNo As String
        Dim ArrCont() As String
        Dim ArrCells() As String
        Dim count As Integer
        Dim StrSelectedBill As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColType As DataColumn
        Dim ColContNo As DataColumn
        Dim ColRate As DataColumn
        Dim StrGetItem As String
        Dim CmdCont As New OleDb.OleDbCommand
        Dim RdrCont As OleDb.OleDbDataReader
        StrSelectedBill = ""
        Cmd.Connection = Con
        StrSql = "SELECT * FROM BoeOut WHERE BillNo = '" & TxtBillOut.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtClientId.Text = Rdr("ClientID").ToString
            TxtContainers.Text = Rdr("Container").ToString
            TxtDate.Text = Format(Rdr("BillDate"), "dd/MM/yyyy")
            TxtItems.Text = Rdr("Items").ToString
            TxtClientName.Text = ""
            TxtRemarks.Text = Rdr("Remarks").ToString
            TxtCbNo.Text = Rdr("CBNo").ToString
            TxtTransport.Text = Rdr("Transport").ToString
            TxtTruckNo.Text = Rdr("TruckNo").ToString
            CmbCountry.Text = Rdr("Country").ToString
            CmbCpc.Text = Rdr("TransType").ToString
            CmbZone.Text = Rdr("WhCode")
            StrContNo = Rdr("ContainerNo")
            StrSelectedBill = Rdr("BillInNo").ToString
            Rdr.Close()

            ' Update Client Name and Country name based on the codes
            TxtClientID_TextChanged(Me, e)

            For Each Row As GridViewRow In GdvBill.Rows
                If InStr(1, StrSelectedBill, Row.Cells(1).Text) > 0 Then
                    DirectCast(Row.FindControl("ChkSelBill"), CheckBox).Checked = True
                End If

            Next

            ''Update Container in Grid
            CmdCont.Connection = Con
            dt = New DataTable()
            ColType = New DataColumn("Type", Type.GetType("System.String"))
            ColContNo = New DataColumn("ContNo", Type.GetType("System.String"))
            dt.Columns.Add(ColType)
            dt.Columns.Add(ColContNo)
            StrGetItem = "SELECT ContType,ContainerNo From NewContDetailsOUT "
            StrGetItem = StrGetItem & "WHERE BoeNo = '" & TxtBillOut.Text & "' Order By ContType"
            CmdCont.CommandText = StrGetItem
            RdrCont = CmdCont.ExecuteReader
            While RdrCont.Read
                dr = dt.NewRow
                dr.Item("Type") = RdrCont("ContType")
                dr.Item("ContNo") = RdrCont("ContainerNo")
                dt.Rows.Add(dr)
            End While
            GdvCont.DataSource = dt
            GdvCont.DataBind()


            'StrSql = "SELECT DISTINCT(BoeInNo) FROM BoeOutProd WHERE BoeNo = '" & TxtBillOut.Text & "'"
            'Cmd.Connection = Con
            'Cmd.CommandText = StrSql
            'Rdr = Cmd.ExecuteReader
            'While Rdr.Read
            '    For Each Row As GridViewRow In GdvBill.Rows
            '        If Row.Cells(1).Text = Rdr("BoeInNo") Then
            '            DirectCast(Row.FindControl("ChkSelBill"), CheckBox).Checked = True
            '        End If
            '    Next
            'End While
            'Rdr.Close()
            'Update Bill Items in Grid
            'Update Products in Grid
            'ButRefreshBill_Click(Me, e)
            'Update BillOut Grid

            Dim ColBoeNo As DataColumn
            Dim ColItemNo As DataColumn
            Dim ColLineNo As DataColumn
            Dim ColCode As DataColumn
            Dim ColQuantity As DataColumn
            Dim ColUnit As DataColumn
            Dim ColQtySubUnit As DataColumn
            Dim ColSubUnit As DataColumn
            Dim ColDescription As DataColumn
            Dim ColValue As DataColumn
            Dim ColWeight As DataColumn
            Dim ColBalWt As DataColumn
            Dim ColBalQty As DataColumn
            Dim ColBalSubQty As DataColumn
            'Define Data table with required columns
            dt.Clear()
            ColBoeNo = New DataColumn("BoeNo", Type.GetType("System.String"))
            ColItemNo = New DataColumn("ItemNo", Type.GetType("System.String"))
            ColLineNo = New DataColumn("LineNo", Type.GetType("System.String"))
            ColCode = New DataColumn("Code", Type.GetType("System.String"))
            ColQuantity = New DataColumn("Quantity", Type.GetType("System.String"))
            ColUnit = New DataColumn("Unit", Type.GetType("System.String"))
            ColQtySubUnit = New DataColumn("QtySubUnit", Type.GetType("System.String"))
            ColSubUnit = New DataColumn("SubUnit", Type.GetType("System.String"))
            ColDescription = New DataColumn("Description", Type.GetType("System.String"))
            ColValue = New DataColumn("Value", Type.GetType("System.String"))
            ColWeight = New DataColumn("Weight", Type.GetType("System.String"))
            ColBalWt = New DataColumn("BalWt", Type.GetType("System.String"))
            ColBalQty = New DataColumn("BalQty", Type.GetType("System.String"))
            ColBalSubQty = New DataColumn("BalSubQty", Type.GetType("System.String"))
            dt.Columns.Add(ColBoeNo)
            dt.Columns.Add(ColItemNo)
            dt.Columns.Add(ColLineNo)
            dt.Columns.Add(ColCode)
            dt.Columns.Add(ColQuantity)
            dt.Columns.Add(ColUnit)
            dt.Columns.Add(ColQtySubUnit)
            dt.Columns.Add(ColSubUnit)
            dt.Columns.Add(ColDescription)
            dt.Columns.Add(ColValue)
            dt.Columns.Add(ColWeight)
            dt.Columns.Add(ColBalWt)
            dt.Columns.Add(ColBalQty)
            dt.Columns.Add(ColBalSubQty)
            ''Get Bill outbound Items
            'StrSql = "SELECT * FROM BoeOutItem WHERE BoeNo = '" & TxtBillOut.Text & "'"
            'Cmd.CommandText = StrSql
            'Rdr = Cmd.ExecuteReader
            'While Rdr.Read
            '    dr = dt.NewRow
            '    dr.Item("BoeNo") = Rdr("BoeInNo").ToString
            '    dr.Item("ItemNo") = Rdr("BoeInItem").ToString
            '    dr.Item("LineNo") = Rdr("ItemNo").ToString
            '    dr.Item("Code") = Rdr("HsCode").ToString
            '    dr.Item("Quantity") = Rdr("Quantity").ToString
            '    dr.Item("Unit") = Rdr("Unit").ToString
            '    dr.Item("QtySubUnit") = ""
            '    dr.Item("SubUnit") = "MUR"
            '    dr.Item("Description") = Rdr("HsDescription").ToString
            '    dr.Item("Value") = Rdr("Value").ToString
            '    dr.Item("Weight") = Rdr("Weight").ToString
            '    dr.Item("BalWt") = Rdr("BalWeight") - Rdr("Weight")
            '    dr.Item("BalQty") = "0"
            '    dr.Item("BalSubQty") = Rdr("BalValue") - Rdr("Value")
            '    dt.Rows.Add(dr)
            'End While
            'Rdr.Close()
            'Get Bill outbound Products
            StrSql = "SELECT * FROM BoeOutProd WHERE BoeNo = '" & TxtBillOut.Text & "'"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                dr = dt.NewRow
                dr.Item("BoeNo") = Rdr("BoeInNo").ToString
                dr.Item("ItemNo") = Rdr("BoeInItem").ToString
                dr.Item("LineNo") = Rdr("LineNo").ToString
                dr.Item("Code") = Rdr("PCode").ToString
                dr.Item("Quantity") = Rdr("Quantity").ToString
                dr.Item("Unit") = Rdr("UnitCode").ToString
                dr.Item("QtySubUnit") = Rdr("QtySubUnit").ToString
                dr.Item("SubUnit") = Rdr("SubUnit").ToString
                dr.Item("Description") = Rdr("ProdDescription").ToString
                dr.Item("Value") = ""
                dr.Item("Weight") = ""
                dr.Item("BalWt") = ""
                dr.Item("BalQty") = Rdr("BalQuantity")
                dr.Item("BalSubQty") = Rdr("BalSubQuantity")
                dt.Rows.Add(dr)
            End While
            Rdr.Close()
            dt.DefaultView.Sort = "BoeNo,ItemNo,LineNo"
            GdvOut.Columns(3).Visible = True
            GdvOut.DataSource = dt
            GdvOut.DataBind()
            For Each Row As GridViewRow In GdvOut.Rows
                If Row.RowType = DataControlRowType.DataRow Then
                    If Row.Cells(8).Text = "MUR" Then
                        Row.BackColor = Drawing.Color.Yellow
                    Else
                        'Row.Cells(1).ForeColor = Drawing.Color.White
                    End If
                End If
            Next
            GdvOut.Columns(3).Visible = False
            ButDelete_ConfirmButtonExtender.ConfirmText = "Sure to Delete Bill Outbound " & TxtBillOut.Text & "?"
            ButDelete.Enabled = True
        Else
            Rdr.Close()
            ButDelete.Enabled = False
        End If
    End Sub

    Private Sub ButFindClient_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFindClient.Click
        RbnClient.Text = "Name"
        RbnCountry.Text = "City"
        RbnTraffic.Text = "Contact"
        RbnCont.Visible = False
        ChkDate.Visible = False
        TxtFrom.Visible = False
        TxtTo.Visible = False
        LblToDate.Visible = False
        GdvFind.DataSource = Nothing
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Protected Sub ButRefreshBill_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRefreshBill.Click
        'Update BillIn Items in the Grid
        Dim DatBill As New DataTable

        Dim CmdBoeInItem As New OleDb.OleDbCommand
        Dim RdrBoeInItem As OleDb.OleDbDataReader
        Dim CmdBoeInProd As New OleDb.OleDbCommand
        Dim RdrBoeInProd As OleDb.OleDbDataReader
        Cmd.Connection = Con
        StrSelectedBills = ""
        For Each Row As GridViewRow In GdvBill.Rows
            If DirectCast(Row.FindControl("ChkSelBill"), CheckBox).Checked Then
                If StrSelectedBills.Length > 0 Then
                    StrSelectedBills = StrSelectedBills & "',"
                End If
                StrSelectedBills = StrSelectedBills & "'" & Row.Cells(1).Text
            End If
        Next
        If StrSelectedBills.Length > 0 Then
            StrSelectedBills = StrSelectedBills & "'"
        End If
        If StrSelectedBills <> "" Then
            StrSql = "SELECT * FROM BoeInItem WHERE BoeNo IN (" & StrSelectedBills & ") Order By BoeNo, ItemNo"
        Else
            Exit Sub
        End If

        Dim DtHsItem As DataTable
        Dim Dr As DataRow
        Dim Dc As DataColumn

        DtHsItem = New DataTable()
        Dc = New DataColumn("BoeNo", Type.GetType("System.String"))
        DtHsItem.Columns.Add(Dc)
        Dc = New DataColumn("ItemNo", Type.GetType("System.String"))
        DtHsItem.Columns.Add(Dc)
        Dc = New DataColumn("LineNo", Type.GetType("System.String"))
        DtHsItem.Columns.Add(Dc)
        Dc = New DataColumn("HsCode", Type.GetType("System.String"))
        DtHsItem.Columns.Add(Dc)
        Dc = New DataColumn("HsDescription", Type.GetType("System.String"))
        DtHsItem.Columns.Add(Dc)
        Dc = New DataColumn("Quantity", Type.GetType("System.String"))
        DtHsItem.Columns.Add(Dc)
        Dc = New DataColumn("BalQuantity", Type.GetType("System.String"))
        DtHsItem.Columns.Add(Dc)
        Dc = New DataColumn("Unit", Type.GetType("System.String"))
        DtHsItem.Columns.Add(Dc)
        Dc = New DataColumn("Weight", Type.GetType("System.String"))
        DtHsItem.Columns.Add(Dc)
        Dc = New DataColumn("CountryofOrigin", Type.GetType("System.String"))
        DtHsItem.Columns.Add(Dc)
        CmdBoeInItem.Connection = Con
        CmdBoeInProd.Connection = Con
        CmdBoeInItem.CommandText = StrSql
        RdrBoeInItem = CmdBoeInItem.ExecuteReader
        While RdrBoeInItem.Read
            Dr = DtHsItem.NewRow
            Dr.Item("BoeNo") = RdrBoeInItem("BoeNo")
            Dr.Item("ItemNo") = RdrBoeInItem("ItemNo")
            Dr.Item("HsCode") = RdrBoeInItem("HsCode")
            Dr.Item("HsDescription") = RdrBoeInItem("HsDescription")
            Dr.Item("Quantity") = RdrBoeInItem("Quantity")
            Dr.Item("BalQuantity") = RdrBoeInItem("BalQuantity")
            Dr.Item("Unit") = RdrBoeInItem("Unit")
            Dr.Item("Weight") = RdrBoeInItem("Weight")
            Dr.Item("CountryofOrigin") = RdrBoeInItem("CountryofOrigin")
            DtHsItem.Rows.Add(Dr)
            CmdBoeInProd.CommandText = "Select *From BoeInProd Where BoeNo = '" & RdrBoeInItem("BoeNo") & "' And ItemNo = '" & RdrBoeInItem("ItemNo") & "' Order By ItemNo"
            RdrBoeInProd = CmdBoeInProd.ExecuteReader
            While RdrBoeInProd.Read
                Dr = DtHsItem.NewRow
                Dr.Item("BoeNo") = RdrBoeInProd("BoeNo")
                Dr.Item("ItemNo") = RdrBoeInItem("ItemNo")
                Dr.Item("LineNo") = RdrBoeInProd("LineNo")
                Dr.Item("HsCode") = RdrBoeInProd("PCode")
                Dr.Item("HsDescription") = RdrBoeInProd("ProdDescription")
                Dr.Item("Quantity") = RdrBoeInProd("Quantity")
                Dr.Item("BalQuantity") = RdrBoeInProd("BalQuantity")
                Dr.Item("Unit") = RdrBoeInProd("UnitCode")
                DtHsItem.Rows.Add(Dr)
            End While
            RdrBoeInProd.Close()
        End While
        RdrBoeInItem.Close()
        GdvInItem.DataSource = DtHsItem
        GdvInItem.DataBind()






        For Each Row As GridViewRow In GdvInItem.Rows
            If Row.RowType = DataControlRowType.DataRow Then
                'Row.Visible = False
                If Row.RowIndex > 0 Then
                    If Row.Cells(0).Text = GdvInItem.Rows(Row.RowIndex - 1).Cells(0).Text Then
                        'Row.Cells(0).BackColor = Drawing.Color.White
                        'Row.Cells(0).ForeColor = Drawing.Color.White
                        'Row.Cells(1).BackColor = Drawing.Color.White
                        'Row.Cells(1).ForeColor = Drawing.Color.White
                    Else
                        'Row.Cells(0).ForeColor = Drawing.Color.Black
                        'Row.Cells(1).ForeColor = Drawing.Color.Black
                    End If
                End If
            End If
        Next
        'GdvInProd.Columns(1).Visible = False
        'For Each Row As GridViewRow In GdvInItem.Rows
        '    ' Hide repeated bill nos in grid
        '    If Row.RowType = DataControlRowType.DataRow And Row.RowIndex > 0 Then
        '        If Row.Cells(0).Text = GdvInItem.Rows(Row.RowIndex - 1).Cells(0).Text Then
        '            'If Row.RowState = DataControlRowState.Normal Then
        '            '    Row.Cells(0).ForeColor = Drawing.Color.White
        '            'Else
        '            '    Row.Cells(0).ForeColor = System.Drawing.ColorTranslator.FromHtml("#AFE9EF")
        '            'End If
        '            Row.Cells(0).BackColor = Drawing.Color.White
        '            Row.Cells(0).ForeColor = Drawing.Color.White
        '        Else
        '            Row.Cells(0).ForeColor = Drawing.Color.Black
        '        End If
        '    End If
        'Next
    End Sub

    Private Sub GdvInItem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvInItem.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvInItem, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvInItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvInItem.SelectedIndexChanged

        TxtProdCode.Text = GdvInItem.SelectedRow.Cells(3).Text
        TxtDescription.Text = GdvInItem.SelectedRow.Cells(4).Text
        TxtQtyUnit.Text = GdvInItem.SelectedRow.Cells(5).Text
        TxtBalUnit.Text = GdvInItem.SelectedRow.Cells(6).Text
        LblUnit.Text = GdvInItem.SelectedRow.Cells(7).Text
    End Sub

    Private Sub GdvInItem_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvInItem.SelectedIndexChanging
        GdvInItem.SelectedIndex = e.NewSelectedIndex
    End Sub

    'Private Sub GdvInProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvInProd.RowDataBound
    '    On Error Resume Next
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
    '        e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvInProd, "Select$" & e.Row.RowIndex)
    '    End If
    'End Sub

    'Private Sub GdvInProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvInProd.SelectedIndexChanged
    '    LblCode.Text = "Prod Code"
    ''    TxtProdCode.Text = GdvInProd.SelectedRow.Cells(3).Text
    '    TxtDescription.Text = GdvInProd.SelectedRow.Cells(8).Text
    '    TxtQtyUnit.Text = GdvInProd.SelectedRow.Cells(4).Text
    '    TxtBalUnit.Text = GdvInProd.SelectedRow.Cells(9).Text
    '    LblUnit.Text = GdvInProd.SelectedRow.Cells(5).Text
    '    LblAddHead.Text = "Add Product to Outbound"
    '    TxtDelUnit.Text = ""
    '    TxtDelUnit.Focus()
    'End Sub

    'Private Sub GdvInProd_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvInProd.SelectedIndexChanging
    '    GdvInProd.SelectedIndex = e.NewSelectedIndex
    'End Sub

    Protected Sub ButAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAdd.Click
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColBoeNo As DataColumn
        Dim ColItemNo As DataColumn
        Dim ColLineNo As DataColumn
        Dim ColCode As DataColumn
        Dim ColQuantity As DataColumn
        Dim ColUnit As DataColumn
        Dim ColQtySubUnit As DataColumn
        Dim ColSubUnit As DataColumn
        Dim ColDescription As DataColumn
        Dim ColValue As DataColumn
        Dim ColWeight As DataColumn
        Dim ColBalWt As DataColumn
        Dim ColBalQty As DataColumn
        Dim ColBalSubQty As DataColumn

        If Val(TxtDelUnit.Text) > Val(TxtBalUnit.Text) Then
            LblText.Visible = True
            LblText.Text = "Amount delivered not correct. Please enter correct amount"
            Exit Sub
        Else
            LblText.Visible = False
        End If

        If TxtProdCode.Text = "" Then
            LblText.Visible = True
            LblText.Text = "Please Select Product"
            Exit Sub
        Else
            LblText.Visible = False
        End If

        dt = New DataTable()
        ColBoeNo = New DataColumn("BoeNo", Type.GetType("System.String"))
        ColItemNo = New DataColumn("ItemNo", Type.GetType("System.String"))
        ColLineNo = New DataColumn("LineNo", Type.GetType("System.String"))
        ColCode = New DataColumn("Code", Type.GetType("System.String"))
        ColQuantity = New DataColumn("Quantity", Type.GetType("System.String"))
        ColUnit = New DataColumn("Unit", Type.GetType("System.String"))
        ColQtySubUnit = New DataColumn("QtySubUnit", Type.GetType("System.String"))
        ColSubUnit = New DataColumn("SubUnit", Type.GetType("System.String"))
        ColDescription = New DataColumn("Description", Type.GetType("System.String"))
        ColValue = New DataColumn("Value", Type.GetType("System.String"))
        ColWeight = New DataColumn("Weight", Type.GetType("System.String"))
        ColBalWt = New DataColumn("BalWt", Type.GetType("System.String"))
        ColBalQty = New DataColumn("BalQty", Type.GetType("System.String"))
        ColBalSubQty = New DataColumn("BalSubQty", Type.GetType("System.String"))

        dt.Columns.Add(ColBoeNo)
        dt.Columns.Add(ColItemNo)
        dt.Columns.Add(ColLineNo)
        dt.Columns.Add(ColCode)
        dt.Columns.Add(ColQuantity)
        dt.Columns.Add(ColUnit)
        dt.Columns.Add(ColQtySubUnit)
        dt.Columns.Add(ColSubUnit)
        dt.Columns.Add(ColDescription)
        dt.Columns.Add(ColValue)
        dt.Columns.Add(ColWeight)
        dt.Columns.Add(ColBalWt)
        dt.Columns.Add(ColBalQty)
        dt.Columns.Add(ColBalSubQty)
        'On Error Resume Next
        For Each row As GridViewRow In GdvOut.Rows
            If row.RowType = DataControlRowType.DataRow Then
                dr = dt.NewRow
                dr.Item("BoeNo") = row.Cells(1).Text
                dr.Item("ItemNo") = row.Cells(2).Text
                dr.Item("LineNo") = row.Cells(3).Text
                dr.Item("Code") = row.Cells(4).Text
                dr.Item("Quantity") = row.Cells(5).Text
                dr.Item("Unit") = row.Cells(6).Text
                dr.Item("QtySubUnit") = row.Cells(7).Text
                dr.Item("SubUnit") = row.Cells(8).Text
                dr.Item("Description") = row.Cells(9).Text
                dr.Item("Value") = row.Cells(10).Text
                dr.Item("Weight") = row.Cells(11).Text
                dr.Item("BalWt") = row.Cells(12).Text
                dr.Item("BalQty") = row.Cells(13).Text
                dr.Item("BalSubQty") = row.Cells(14).Text
                If ButAdd.Text = "Add" Then
                    dt.Rows.Add(dr)
                Else
                    If Not DirectCast(row.FindControl("ChkSelect"), CheckBox).Checked Then
                        dt.Rows.Add(dr)
                    End If
                End If
            End If
        Next
        dr = dt.NewRow()
        dr("BoeNo") = GdvInItem.SelectedRow.Cells(0).Text
        dr("ItemNo") = GdvInItem.SelectedRow.Cells(1).Text
        dr("LineNo") = GdvInItem.SelectedRow.Cells(2).Text
        dr("Quantity") = TxtDelUnit.Text
        dr("Unit") = LblUnit.Text
        dr("QtySubUnit") = TxtDelUnit.Text
        dr("Description") = TxtDescription.Text
        dr("Code") = GdvInItem.SelectedRow.Cells(3).Text
        dr("SubUnit") = "MUR"
        dr("BalQty") = TxtBalUnit.Text
        dr("BalSubQty") = TxtBalUnit.Text
        dt.Rows.Add(dr)
        dt.DefaultView.Sort = "BoeNo,ItemNo,LineNo"
        GdvOut.DataSource = dt
        GdvOut.DataBind()
        For Each Row As GridViewRow In GdvOut.Rows
            If Row.RowType = DataControlRowType.DataRow Then
                If Row.Cells(8).Text = "MUR" Then
                    'Row.BackColor = Drawing.Color.Yellow
                Else
                    'Row.Cells(1).ForeColor = Drawing.Color.White
                End If
            End If
        Next
        ButAdd.Text = "Add"
        TxtProdCode.Text = ""
        TxtDescription.Text = ""
        TxtQtyUnit.Text = ""
        TxtBalUnit.Text = ""
        TxtDelUnit.Text = ""
    End Sub

    Protected Sub ButRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRemove.Click
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColBoeNo As DataColumn
        Dim ColItemNo As DataColumn
        Dim ColLineNo As DataColumn
        Dim ColCode As DataColumn
        Dim ColQuantity As DataColumn
        Dim ColUnit As DataColumn
        Dim ColQtySubUnit As DataColumn
        Dim ColSubUnit As DataColumn
        Dim ColDescription As DataColumn
        Dim ColValue As DataColumn
        Dim ColWeight As DataColumn
        Dim ColBalWt As DataColumn
        Dim ColBalQty As DataColumn
        Dim ColBalSubQty As DataColumn
        Dim CmdDeleteProd As New OleDb.OleDbCommand
        CmdDeleteProd.Connection = Con

        dt = New DataTable()
        ColBoeNo = New DataColumn("BoeNo", Type.GetType("System.String"))
        ColItemNo = New DataColumn("ItemNo", Type.GetType("System.String"))
        ColLineNo = New DataColumn("LineNo", Type.GetType("System.String"))
        ColCode = New DataColumn("Code", Type.GetType("System.String"))
        ColQuantity = New DataColumn("Quantity", Type.GetType("System.String"))
        ColUnit = New DataColumn("Unit", Type.GetType("System.String"))
        ColQtySubUnit = New DataColumn("QtySubUnit", Type.GetType("System.String"))
        ColSubUnit = New DataColumn("SubUnit", Type.GetType("System.String"))
        ColDescription = New DataColumn("Description", Type.GetType("System.String"))
        ColValue = New DataColumn("Value", Type.GetType("System.String"))
        ColWeight = New DataColumn("Weight", Type.GetType("System.String"))
        ColBalWt = New DataColumn("BalWt", Type.GetType("System.String"))
        ColBalQty = New DataColumn("BalQty", Type.GetType("System.String"))
        ColBalSubQty = New DataColumn("BalSubQty", Type.GetType("System.String"))
        dt.Columns.Add(ColBoeNo)
        dt.Columns.Add(ColItemNo)
        dt.Columns.Add(ColLineNo)
        dt.Columns.Add(ColCode)
        dt.Columns.Add(ColQuantity)
        dt.Columns.Add(ColUnit)
        dt.Columns.Add(ColQtySubUnit)
        dt.Columns.Add(ColSubUnit)
        dt.Columns.Add(ColDescription)
        dt.Columns.Add(ColValue)
        dt.Columns.Add(ColWeight)
        dt.Columns.Add(ColBalWt)
        dt.Columns.Add(ColBalQty)
        dt.Columns.Add(ColBalSubQty)
        On Error Resume Next
        For Each row As GridViewRow In GdvOut.Rows
            If row.RowType = DataControlRowType.DataRow Then
                If Not DirectCast(row.FindControl("ChkSelect"), CheckBox).Checked Then
                    dr = dt.NewRow
                    dr.Item("BoeNo") = row.Cells(1).Text
                    dr.Item("ItemNo") = row.Cells(2).Text
                    dr.Item("LineNo") = row.Cells(3).Text
                    dr.Item("Code") = row.Cells(4).Text
                    dr.Item("Quantity") = row.Cells(5).Text
                    dr.Item("Unit") = row.Cells(6).Text
                    dr.Item("QtySubUnit") = row.Cells(7).Text
                    dr.Item("SubUnit") = row.Cells(8).Text
                    dr.Item("Description") = row.Cells(9).Text
                    dr.Item("Value") = row.Cells(10).Text
                    dr.Item("Weight") = row.Cells(11).Text
                    dr.Item("BalWt") = row.Cells(12).Text
                    dr.Item("BalQty") = row.Cells(13).Text
                    dr.Item("BalSubQty") = row.Cells(14).Text
                    dt.Rows.Add(dr)
                Else
                    CmdDeleteProd.CommandText = "Delete From BoeOutProd Where BoeNo  = '" & TxtBillOut.Text & "' And ItemNo = '" & row.Cells(2).Text & "' And PCode  = '" & row.Cells(4).Text & "'"
                    CmdDeleteProd.ExecuteNonQuery()
                End If
            End If
        Next
        GdvOut.DataSource = dt
        GdvOut.DataBind()
        For Each Row As GridViewRow In GdvOut.Rows
            If Row.RowType = DataControlRowType.DataRow Then
                If Row.Cells(8).Text = "MUR" Then
                    Row.BackColor = Drawing.Color.Yellow
                Else
                    Row.Cells(1).ForeColor = Drawing.Color.White
                End If
            End If
        Next
    End Sub

    Private Sub GdvOut_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvOut.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvOut, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvOut_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvOut.SelectedIndexChanged
        'ButAdd.Text = "Remove"
        For i = 0 To GdvOut.Rows.Count - 1
            DirectCast(GdvOut.Rows(i).FindControl("ChkSelect"), CheckBox).Checked = False
        Next
        DirectCast(GdvOut.SelectedRow.FindControl("ChkSelect"), CheckBox).Checked = True

        'TxtDelUnit.Text = ""

        If GdvOut.SelectedRow.BackColor = Drawing.Color.Yellow Then
            'This is an Item Line
            'LblCode.Text = "HS Code"
            'LblAddHead.Text = "Add Item to Outbound"
        Else
            'This is a Product line
            'LblCode.Text = "Prod Code"
            'LblAddHead.Text = "Add Product to Outbound"
            'For Each Row As GridViewRow In GdvInProd.Rows
            '    If Row.Cells(0).Text = GdvOut.SelectedRow.Cells(1).Text And Row.Cells(1).Text = GdvOut.SelectedRow.Cells(2).Text Then
            '        If Row.Cells(2).Text = GdvOut.SelectedRow.Cells(3).Text Then
            '            GdvInProd.SelectedIndex = Row.RowIndex
            '            GdvInProd_SelectedIndexChanged(Me, e)
            '            Exit For
            '        End If
            '    End If
            'Next
            'TxtDelUnit.Text = ChkSpace(GdvOut.SelectedRow.Cells(5).Text)
            'LblUnit.Text = ChkSpace(GdvOut.SelectedRow.Cells(6).Text)
            'TxtDescription.Text = ChkSpace(GdvOut.SelectedRow.Cells(9).Text)
            'TxtProdCode.Text = ChkSpace(GdvOut.SelectedRow.Cells(4).Text)
            'TxtBalUnit.Text = ChkSpace(GdvOut.SelectedRow.Cells(13).Text)
        End If
    End Sub

    Private Sub GdvOut_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvOut.SelectedIndexChanging
        GdvOut.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub ButCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCancel.Click
        ModalPopupExtender2.Hide()
        ErrHead.Text = "User Cancelled"
        ErrMsg.Text = "You have cancelled the save process. The data is not saved. To save the data Click on Save again."
        ModalPopupExtender1.Show()
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Or Txt = "Select..." Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim IntLine As Integer
        Dim StrContNo As String
        Dim StrBillIn As String
        Dim IntItem As Integer
        Dim Dt As New DataTable
        Dim Dat As New DataTable
        Dim Trans As OleDb.OleDbTransaction
        Dim CmdGetHSCode As New OleDb.OleDbCommand
        Dim RdrGetHSCode As OleDb.OleDbDataReader
        Dim CmdContainer As New OleDb.OleDbCommand
        Dim CmdBill As New OleDb.OleDbCommand
        Dim CmdCont As New OleDb.OleDbCommand
        StrBillIn = ""

        CmdChkClient.Connection = Con
        CmdChkClient.CommandText = "SELECT *FROM Client WHERE ClientID = '" & TxtClientId.Text & "'"
        RdrChkClient = CmdChkClient.ExecuteReader
        If RdrChkClient.Read Then
            If RdrChkClient("Category") = "Suspended Duty" Then
                If CDate(RdrChkClient("PermitExp")) < CDate(TxtDate.Text) Then
                    ErrHead.Text = "Save Failed"
                    ErrMsg.Text = "License Expired"
                    ModalPopupExtender1.Show()
                    'ClearAll()
                    Exit Sub
                End If
            ElseIf RdrChkClient("Category") = "Freeport" Then
                If CDate(RdrChkClient("LicenceExp")) < CDate(TxtDate.Text) Then
                    ErrHead.Text = "Save Failed"
                    ErrMsg.Text = "License Expired"
                    ModalPopupExtender1.Show()
                    'ClearAll()
                    Exit Sub
                End If
            End If
        End If
        RdrChkClient.Close()


        For Each Row As GridViewRow In GdvBill.Rows
            If DirectCast(Row.FindControl("ChkSelBill"), CheckBox).Checked Then
                If Len(StrBillIn) > 0 Then StrBillIn = StrBillIn & ";"
                StrBillIn = StrBillIn & "" & Row.Cells(1).Text & ""
            End If
        Next
        ' Check this bill is in Database
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        CmdBill.Connection = Con
        CmdCont.Connection = Con
        CmdGetHSCode.Connection = Con
        Cmd.Transaction = Trans
        CmdBill.Transaction = Trans
        CmdCont.Transaction = Trans
        CmdGetHSCode.Transaction = Trans
        'Cmd.CommandText = "SELECT COUNT(BillNo) FROM BoeOut WHERE BillNo = '" & TxtBillOut.Text & "'"
        'If Convert.ToInt32(Cmd.ExecuteScalar()) > 0 Then
        '    BlnNew = False
        '    ' Save with Amendment Routine
        '    ModalPopupExtender2.Show()
        '    Exit Sub
        'Else
        '    BlnNew = True
        'End If
        StrSql = "SELECT * FROM Regime WHERE CpcCode = '" & CmbCpc.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            CmdBill.CommandText = "Delete From BoeOut Where BillNo = '" & TxtBillOut.Text & "'"
            CmdBill.ExecuteNonQuery()
            StrSql = "INSERT INTO BoeOut (BillInNo,BillNo,BillDate,TransType,Regime,ClientID,Items,Containers,"
            StrSql = StrSql & "Transport,TruckNo,TrafficOp,Country,Remarks,WhCode,CbNo,CrossDock,Posted) "
            StrSql = StrSql & "VALUES('" & StrBillIn & "','"
            StrSql = StrSql & TxtBillOut.Text & "','"
            StrSql = StrSql & Format(CDate(TxtDate.Text), "MM/dd/yyyy") & "','"
            StrSql = StrSql & CmbCpc.Text & "','"
            StrSql = StrSql & Rdr("Name").ToString & "','"
            StrSql = StrSql & TxtClientId.Text & "',"
            StrSql = StrSql & Val(TxtItems.Text) & ","

            StrSql = StrSql & Val(TxtContainers.Text) & ",'"
            StrSql = StrSql & TxtTransport.Text & "','"
            StrSql = StrSql & TxtTruckNo.Text & "','"
            StrSql = StrSql & CmbTrafficOfficer.Text & "','"
            StrSql = StrSql & CmbCountry.Text & "','"
            StrSql = StrSql & TxtRemarks.Text & "','"
            StrSql = StrSql & CmbZone.Text & "','"
            StrSql = StrSql & TxtCbNo.Text & "',"
            StrSql = StrSql & "0,0)"
            CmdBill.CommandText = StrSql
            CmdBill.ExecuteNonQuery()

            'Update ContainerDetails
            CmdCont.CommandText = "DELETE FROM NewContDetailsOUT WHERE BoeNo = '" & TxtBillOut.Text & "'"
            CmdCont.ExecuteNonQuery()
            For Each row As GridViewRow In GdvCont.Rows
                StrSql = "INSERT INTO NewContDetailsOUT (BoeNo,ContType,ContainerNo"
                StrSql = StrSql & ") VALUES('"
                StrSql = StrSql & TxtBillOut.Text & "','"
                If row.RowType = DataControlRowType.DataRow Then
                    If DirectCast(row.FindControl("Type"), TextBox).Text <> "" Then
                        StrSql = StrSql & DirectCast(row.FindControl("Type"), TextBox).Text & "','"
                        StrSql = StrSql & DirectCast(row.FindControl("ContNo"), TextBox).Text & "')"
                        CmdCont.CommandText = StrSql
                        CmdCont.ExecuteNonQuery()
                    End If
                End If
            Next
        Else
            'Error msg Invalid CPC Code
            Exit Sub
        End If
        Rdr.Close()
        'Start Database Transaction
        'Dim Trans As OleDb.OleDbTransaction
        'Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        'Cmd.Transaction = Trans
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Try
            Cmd.CommandText = "DELETE FROM BoeOutItem WHERE BoeNo = '" & TxtBillOut.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "DELETE FROM BoeOutProd WHERE BoeNo = '" & TxtBillOut.Text & "'"
            Cmd.ExecuteNonQuery()
            'Update BoeOut Table
            'Cmd.CommandText = StrSql
            'Cmd.ExecuteNonQuery()
            'Update BoeOutItem Table
            For Each Row As GridViewRow In GdvOut.Rows
                IntLine = IntLine + 1
                StrSql = "INSERT INTO BoeOutProd (BoeNo,ItemNo,[LineNo],PCode,ProdDescription,OriginalQuantity,UnitCode,"
                StrSql = StrSql & "OriginalSubQuantity,SubUnit,BalQuantity,BalSubQuantity,Quantity,QtySubUnit,BoeInNo,"
                StrSql = StrSql & "BoeInItem) VALUES ('" & TxtBillOut.Text & "',"
                StrSql = StrSql & Row.Cells(2).Text & ","
                StrSql = StrSql & IntLine & ",'"
                StrSql = StrSql & Row.Cells(4).Text & "','"
                StrSql = StrSql & Row.Cells(9).Text & "',"
                StrSql = StrSql & "0,'"
                StrSql = StrSql & Row.Cells(6).Text & "',"
                StrSql = StrSql & "0,'"
                StrSql = StrSql & Row.Cells(8).Text & "',"
                StrSql = StrSql & "0,"
                StrSql = StrSql & "0,"
                StrSql = StrSql & Val(Row.Cells(5).Text) & ","
                StrSql = StrSql & Val(Row.Cells(7).Text) & ",'"
                StrSql = StrSql & Row.Cells(1).Text & "',"
                StrSql = StrSql & Val(Row.Cells(2).Text) & ")"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()

                StrSql = "INSERT INTO BoeOutItem (BoeNo,ItemNo,HsCode,HsDescription,Value"
                StrSql = StrSql & ",Unit,BoeInNo) VALUES ('" & TxtBillOut.Text & "',"
                StrSql = StrSql & Row.Cells(2).Text & ","
                CmdGetHSCode.CommandText = "Select HsCode, HsDescription,Value From BoeInItem Where BoeNo = '" & Row.Cells(1).Text & "' And ItemNo = '" & Row.Cells(2).Text & "'"
                RdrGetHSCode = CmdGetHSCode.ExecuteReader
                If RdrGetHSCode.Read Then
                    StrSql = StrSql & "'" & RdrGetHSCode("HsCode") & "',"
                    StrSql = StrSql & "'" & RdrGetHSCode("HsDescription") & "',"
                    StrSql = StrSql & "" & RdrGetHSCode("Value") & ",'"
                Else
                    StrSql = StrSql & "NULL,"
                    StrSql = StrSql & "NULL,"
                    StrSql = StrSql & "0,'"
                End If
                RdrGetHSCode.Close()
                StrSql = StrSql & Row.Cells(6).Text & "','"
                StrSql = StrSql & Row.Cells(1).Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()


                'Update BoeIn Products with the balance
                StrSql = "SELECT MAX(BoeInNo),SUM(ISNULL(Quantity,0)),SUM(ISNULL(QtySubUnit,0)), MAX(PCode) FROM BoeOutProd WHERE BoeInNo IN "
                StrSql = StrSql & "(SELECT BoeInNo FROM BoeOutProd WHERE BoeNo = '" & TxtBillOut.Text & "') GROUP BY BoeInNo,PCode"
                Cmd.CommandText = StrSql
                Rdr = Cmd.ExecuteReader

                Dat.Load(Rdr)
                Rdr.Close()
                For I = 0 To Dat.Rows.Count - 1
                    StrSql = "UPDATE BoeInProd SET BalQuantity = Quantity - " & Val(Dat.Rows(I).Item(1)) & ","
                    StrSql = StrSql & "BalSubQuantity = Quantity - " & Val(Dat.Rows(I).Item(1)) & " WHERE BoeNo = '" & Val(Dat.Rows(I).Item(0)) & "' AND "
                    StrSql = StrSql & "PCode = '" & Dat.Rows(I).Item(3) & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                Next
                'Update Original Values and Balances in BoeOut Products
                StrSql = "UPDATE BoeOutProd SET OriginalQuantity = (SELECT Quantity FROM BoeInProd WHERE BoeNo=BoeOutProd.BoeInNo AND PCode=BoeOutProd.PCode And ItemNo = BoeOutProd.ItemNo And [LineNo] = BoeOutProd.[LineNo]),"
                StrSql = StrSql & "OriginalSubQuantity=(SELECT QtySubUnit FROM BoeInProd WHERE BoeNo=BoeOutProd.BoeInNo AND PCode=BoeOutProd.PCode And ItemNo = BoeOutProd.ItemNo And [LineNo] = BoeOutProd.[LineNo]),"
                StrSql = StrSql & "BalQuantity=(SELECT BalQuantity FROM BoeInProd WHERE BoeNo=BoeOutProd.BoeInNo AND PCode=BoeOutProd.PCode  And ItemNo = BoeOutProd.ItemNo And [LineNo] = BoeOutProd.[LineNo]),"
                StrSql = StrSql & "BalSubQuantity=(SELECT BalSubQuantity FROM BoeInProd WHERE BoeNo=BoeOutProd.BoeInNo AND PCode=BoeOutProd.PCode  And ItemNo = BoeOutProd.ItemNo And [LineNo] = BoeOutProd.[LineNo]) "
                StrSql = StrSql & "WHERE BoeNo = '" & TxtBillOut.Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                'End If
            Next
            Trans.Commit()
            ClearAll()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Save Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButAddCont_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAddCont.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColType As DataColumn
        Dim ColContNo As DataColumn
        Dim IntContNo As Integer = 0
        dt = New DataTable()
        ColType = New DataColumn("Type", Type.GetType("System.String"))
        ColContNo = New DataColumn("ContNo", Type.GetType("System.String"))
        dt.Columns.Add(ColType)
        dt.Columns.Add(ColContNo)
        On Error Resume Next
        For Each row As GridViewRow In GdvCont.Rows
            If row.RowType = DataControlRowType.DataRow Then
                dr = dt.NewRow
                dr.Item("Type") = DirectCast(row.FindControl("Type"), TextBox).Text
                If DirectCast(row.FindControl("Type"), TextBox).Text <> "" Then
                    IntContNo = IntContNo + 1
                End If
                dr.Item("ContNo") = DirectCast(row.FindControl("ContNo"), TextBox).Text
                dt.Rows.Add(dr)
            End If
        Next
        dr = dt.NewRow()
        dr("Type") = ""
        dr("ContNo") = ""
        dt.Rows.Add(dr)
        GdvCont.DataSource = dt
        GdvCont.DataBind()
    End Sub

    Protected Sub ButDelCont_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDelCont.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColType As DataColumn
        Dim ColContNo As DataColumn
        Dim IntContNo As Integer = 0
        dt = New DataTable()
        ColType = New DataColumn("Type", Type.GetType("System.String"))
        ColContNo = New DataColumn("ContNo", Type.GetType("System.String"))
        dt.Columns.Add(ColType)
        dt.Columns.Add(ColContNo)

        On Error Resume Next
        For Each row As GridViewRow In GdvCont.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim ChkDel As CheckBox = DirectCast(row.FindControl("ChkSelect"), CheckBox)
                If Not ChkDel.Checked Then
                    dr = dt.NewRow
                    dr.Item("Type") = DirectCast(row.FindControl("Type"), TextBox).Text
                    If DirectCast(row.FindControl("Type"), TextBox).Text <> "" Then
                        IntContNo = IntContNo + 1
                    End If
                    dr.Item("ContNo") = DirectCast(row.FindControl("ContNo"), TextBox).Text
                    dt.Rows.Add(dr)
                End If
            End If
        Next
        GdvCont.DataSource = dt
        GdvCont.DataBind()
    End Sub

    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNew.Click
        TxtBillOut.Text = ""
        ClearAll()
    End Sub

    Protected Sub ButDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDelete.Click
        CmdDel.Connection = Con
        CmdDel.CommandText = "DELETE  FROM BoeOut WHERE BillNo = '" & TxtBillOut.Text & "'"
        CmdDel.ExecuteNonQuery()
        CmdDel.CommandText = "DELETE  FROM BoeOutItem WHERE BoeNo = '" & TxtBillOut.Text & "'"
        CmdDel.ExecuteNonQuery()
        CmdDel.CommandText = "DELETE  FROM BoeOutProd WHERE BoeNo = '" & TxtBillOut.Text & "'"
        CmdDel.ExecuteNonQuery()
        LblText.Visible = True
        LblText.Text = "Bill Deleted, Now Please Update Balance"
    End Sub

    Protected Sub ButUpdateBal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButUpdateBal.Click
        Dim CmdProd As New OleDb.OleDbCommand
        Dim RdrProd As OleDb.OleDbDataReader
        Dim CmdChkProduct As New OleDb.OleDbCommand
        Dim RdrChkProduct As OleDb.OleDbDataReader
        Dim LngBal As Long
        Dim LngSub As Long

        ' use this to tally balance temporarily
        StrSelectedBills = ""
        For Each Row As GridViewRow In GdvBill.Rows
            If DirectCast(Row.FindControl("ChkSelBill"), CheckBox).Checked Then
                If StrSelectedBills.Length > 0 Then
                    StrSelectedBills = StrSelectedBills & "',"
                End If
                StrSelectedBills = StrSelectedBills & "'" & Row.Cells(1).Text
            End If
        Next
        If StrSelectedBills.Length > 0 Then
            StrSelectedBills = StrSelectedBills & "'"
        End If
        If StrSelectedBills <> "" Then
            StrSql = "SELECT * FROM BoeInProd WHERE BoeNo IN (" & StrSelectedBills & ") Order By BoeNo, ItemNo"
        Else
            Exit Sub
        End If
        CmdProd.Connection = Con
        CmdChkProduct.Connection = Con
        CmdProd.CommandText = StrSql
        RdrProd = CmdProd.ExecuteReader
        While RdrProd.Read
            CmdChkProduct.CommandText = "SELECT SUM(QUANTITY)AS DelQty, SUM(QtySubUnit) AS DelSubQty FROM BoeOutProd WHERE BoeInNo = '" & RdrProd("BoeNo") & "' AND Pcode = '" & RdrProd("Pcode") & "' AND BoeInItem = " & RdrProd("ItemNo") & ""
            RdrChkProduct = CmdChkProduct.ExecuteReader
            If RdrChkProduct.Read Then
                LngBal = IIf(IsDBNull(RdrChkProduct("DelQty")), 0, RdrChkProduct("DelQty"))
                LngBal = RdrProd("Quantity") - LngBal
                LngSub = IIf(IsDBNull(RdrChkProduct("DelSubQty")), 0, RdrChkProduct("DelSubQty"))
                LngSub = RdrProd("QtySubUnit") - LngSub
            Else
                LngBal = RdrProd("Quantity")
                LngSub = RdrProd("QtySubUnit")
            End If
            RdrChkProduct.Close()
            StrSql = "UPDATE BoeInProd SET BalQuantity = " & LngBal
            StrSql = StrSql & ", BalSubQuantity = " & LngSub
            StrSql = StrSql & " WHERE Boeno = '" & RdrProd("BoeNo") & "' AND "
            StrSql = StrSql & "PCode = '" & RdrProd("PCode") & "' AND ItemNo = " & RdrProd("ItemNo") & ""
            CmdChkProduct.CommandText = StrSql
            CmdChkProduct.ExecuteNonQuery()

            For Each Row As GridViewRow In GdvInItem.Rows
                If Row.Cells(3).Text = RdrProd("PCode") And Row.Cells(1).Text = RdrProd("ItemNo") Then
                    Row.Cells(6).Text = LngBal
                End If
            Next
        End While
        RdrProd.Close()
        LblText.Visible = True
        LblText.Text = "All the balances have now been updated"
    End Sub
End Class