﻿Public Partial Class EditMonthtlyInv
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim SngVat As Single

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            TxtQuantity.Style("text-align") = "right"
            TxtAmount.Style("text-align") = "right"
            TxtDiscount.Style("text-align") = "right"
            TxtNet.Style("text-align") = "right"
            TxtPrepaid.Style("text-align") = "right"
            TxtRate.Style("text-align") = "right"
            TxtTotal.Style("text-align") = "right"
            TxtVat.Style("text-align") = "right"
            TxtVatRate.Style("text-align") = "right"
            TxtValue.Style("text-align") = "right"
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT InvoiceNo,Client.Name,CONVERT(Varchar,InvDate,103) Date,[Value] FROM Invoice,Client WHERE Client.ClientID = Invoice.ClientID "
        If TxtSearch.Text <> "" Then
            StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
        End If
        If CmbZone.Text <> "ALL" Then
            StrSql = StrSql & "AND WhCode = '" & CmbZone.Text & "' "
        End If
        If ChkDate.Checked Then
            StrSql = StrSql & "AND Invoice.InvDate >= '" & TxtFrom.Text & "' AND Invoice.InvDate <= '" & TxtTo.Text & "'"
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        PnlFind.Style.Item("display") = ""
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtInvNo.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtInvNo_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvInvoice.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvInvoice.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub ChkDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkDate.CheckedChanged
        If ChkDate.Checked Then
            TxtFrom.Enabled = True
            TxtTo.Enabled = True
            TxtTo.Text = Date.Today.ToString("dd-MMM-yyyy")
            TxtFrom.Text = Date.Today.AddMonths(-1).ToString("dd-MMM-yyyy")
        Else
            TxtFrom.Enabled = False
            TxtTo.Enabled = False
            TxtTo.Text = ""
            TxtFrom.Text = ""
        End If
    End Sub

    Private Sub ClearAll()
        TxtClient.Text = ""
        TxtDate.Text = ""
        TxtMonth.Text = ""
        TxtYear.Text = ""
        TxtTotal.Text = ""
        TxtVat.Text = ""
        TxtAmount.Text = ""
        TxtDiscount.Text = ""
        TxtPrepaid.Text = ""
        TxtRemarks.Text = ""
        TxtNet.Text = ""
        TxtVatRate.Text = ""
        TxtValue.Text = ""
        TxtDescription.Text = ""
        TxtQuantity.Text = ""
        TxtUnit.Text = ""
        TxtRate.Text = ""
    End Sub

    Private Sub TxtInvNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtInvNo.TextChanged
        On Error Resume Next
        ClearAll()
        StrSql = "SELECT Invoice.*,Client.Name FROM Invoice,Client WHERE Invoice.ClientID=Client.ClientID "
        StrSql = StrSql & "AND InvoiceNo = " & TxtInvNo.Text & " AND WhCode = '" & CmbZone.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtClient.Text = Rdr("Name").ToString
            TxtDate.Text = Format(Rdr("InvDate"), "dd-MMM-yyyy")
            TxtMonth.Text = Rdr("Month").ToString
            TxtYear.Text = Rdr("Year").ToString
            TxtTotal.Text = Format(Rdr("Amount"), "#,##0.00")
            TxtVat.Text = Format(Rdr("Vat"), "#,##0.00")
            TxtAmount.Text = Format(Rdr("Value"), "#,##0.00")
            TxtDiscount.Text = Format(Rdr("Discount"), "#,##0.00")
            TxtPrepaid.Text = Format(Rdr("PrePaid"), "0.00")
            TxtRemarks.Text = Rdr("Remarks").ToString
            TxtNet.Text = Format(ConSng(TxtTotal.Text) - ConSng(TxtDiscount.Text), "#,##0.00")
            SngVat = ConSng(TxtVat.Text) / ConSng(TxtNet.Text) * 100
            TxtVatRate.Text = Format(SngVat, "0.00")
        Else
            TxtClient.Text = "Invalid Invoice No!"
            Exit Sub
        End If
        Rdr.Close()
        GdvInvoice.Columns(6).Visible = True
        StrSql = "SELECT '' AS Type,Description,Quantity,Unit,Rate,Amount FROM Billing WHERE InvoiceNo = " & TxtInvNo.Text & " AND WhCode = '" & CmbZone.Text & "'"
        StrSql = StrSql & " UNION ALL SELECT 'Logistics' AS Type,Description,Quantity,Unit,Rate,Amount FROM Logistics WHERE InvoiceNo = " & TxtInvNo.Text & " AND WhCode = '" & CmbZone.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvInvoice.DataSource = Rdr
        GdvInvoice.DataBind()
        Rdr.Close()
        For Each Row As GridViewRow In GdvInvoice.Rows
            If Row.Cells(0).Text = "Other Charges" Then
                Row.Visible = False
            End If
        Next
        GdvInvoice.Columns(6).Visible = False
        If GdvInvoice.Rows.Count > 0 Then
            GdvInvoice.SelectedIndex = IIf(GdvInvoice.Rows(0).Visible, 0, 1)
            GdvInvoice_SelectedIndexChanged(Me, e)
        End If
    End Sub

    Private Sub GdvInvoice_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvInvoice.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvInvoice, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvInvoice_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvInvoice.SelectedIndexChanged
        TxtDescription.Text = GdvInvoice.SelectedRow.Cells(0).Text
        TxtQuantity.Text = ConSng(GdvInvoice.SelectedRow.Cells(1).Text)
        TxtUnit.Text = GdvInvoice.SelectedRow.Cells(2).Text
        TxtRate.Text = Format(ConSng(GdvInvoice.SelectedRow.Cells(3).Text), "#,##0.00")
        TxtValue.Text = Format(ConSng(GdvInvoice.SelectedRow.Cells(4).Text), "#,##0.00")
        If TxtUnit.Text = "&nbsp;" Then TxtUnit.Text = ""
        If TxtDescription.Text = "&nbsp;" Then TxtDescription.Text = ""
    End Sub

    Private Sub GdvInvoice_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvInvoice.SelectedIndexChanging
        GdvInvoice.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub ButUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButUpdate.Click
        'UpdatePanel1.Update()
        GdvInvoice.SelectedRow.Cells(0).Text = TxtDescription.Text
        GdvInvoice.SelectedRow.Cells(1).Text = TxtQuantity.Text
        GdvInvoice.SelectedRow.Cells(2).Text = TxtUnit.Text
        GdvInvoice.SelectedRow.Cells(3).Text = Format(ConSng(TxtRate.Text), "#,##0.00")
        GdvInvoice.SelectedRow.Cells(4).Text = Format(ConSng(TxtQuantity.Text) * ConSng(TxtRate.Text), "#,##0.00")
        TotalUp()
    End Sub

    Private Sub TotalUp()
        TxtTotal.Text = ""
        For Each Row As GridViewRow In GdvInvoice.Rows
            If Row.Cells(0).Text <> "Other Charges" Then
                TxtTotal.Text = ConSng(TxtTotal.Text) + ConSng(Row.Cells(4).Text)
            End If
        Next
        TxtTotal.Text = Format(ConSng(TxtTotal.Text), "#,##0.00")
        TxtDiscount.Text = Format(ConSng(TxtDiscount.Text), "#,##0.00")
        TxtNet.Text = ConSng(TxtTotal.Text) - ConSng(TxtDiscount.Text)
        TxtNet.Text = Format(ConSng(TxtNet.Text), "#,##0.00")
        TxtVat.Text = Format(ConSng(TxtVatRate.Text) * ConSng(TxtNet.Text) / 100, "#,##0.00")
        TxtAmount.Text = Format(ConSng(TxtNet.Text) + ConSng(TxtVat.Text) - ConSng(TxtPrepaid.Text), "#,##0.00")
    End Sub

    Private Function ConSng(ByVal StrValue As String) As Single
        If Not IsNumeric(StrValue) Then StrValue = "0"
        If StrValue = "" Then StrValue = "0"
        Return CSng(StrValue)
    End Function

    Private Sub TxtDiscount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtDiscount.TextChanged
        TotalUp()
    End Sub

    Private Sub TxtPrepaid_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtPrepaid.TextChanged
        TotalUp()
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
        StrSql = "SELECT Top 1 InvoiceNo FROM Invoice WHERE InvoiceNo > " & Val(TxtInvNo.Text) & " AND WhCode = '" & CmbZone.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtInvNo.Text = Cmd.ExecuteScalar
        If TxtInvNo.Text = "" Then
            StrSql = "SELECT MIN(InvoiceNo) FROM Invoice WHERE WhCode = '" & CmbZone.Text & "'"
            Cmd.CommandText = StrSql
            TxtInvNo.Text = Cmd.ExecuteScalar
        End If
        TxtInvNo_TextChanged(Me, e)
    End Sub

    Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevious.Click
        StrSql = "SELECT Top 1 InvoiceNo FROM Invoice WHERE InvoiceNo < " & Val(TxtInvNo.Text) & " AND WhCode = '" & CmbZone.Text & "' "
        StrSql = StrSql & "ORDER BY InvoiceNo DESC"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtInvNo.Text = Cmd.ExecuteScalar
        If TxtInvNo.Text = "" Then
            StrSql = "SELECT MAX(InvoiceNo) FROM Invoice WHERE WhCode = '" & CmbZone.Text & "'"
            Cmd.CommandText = StrSql
            TxtInvNo.Text = Cmd.ExecuteScalar
        End If
        TxtInvNo_TextChanged(Me, e)
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        'Check if there are items from logistics table and update Other Items in Billing
        Dim IntRow As Integer = -1
        Dim SngTotal As Single = 0
        For Each Row As GridViewRow In GdvInvoice.Rows
            If Row.Cells(6).Text = "Other Charges" Then
                IntRow = Row.RowIndex
                If Row.Cells(5).Text = "Logistics" Then
                    SngTotal = SngTotal + ConSng(Row.Cells(4).Text)
                End If
            End If
        Next
        'If there are Logistics item and there is an other charges in Billing
        If IntRow >= 0 Then
            GdvInvoice.Rows(IntRow).Cells(4).Text = SngTotal
        End If
        StrSql = "UPDATE Invoice SET "
        StrSql = StrSql & "Amount = " & ConSng(TxtTotal.Text) & ","
        StrSql = StrSql & "Discount = " & ConSng(TxtDiscount.Text) & ","
        StrSql = StrSql & "Vat = " & ConSng(TxtVat.Text) & ","
        StrSql = StrSql & "Value = " & ConSng(TxtAmount.Text) & ","
        StrSql = StrSql & "Prepaid = " & ConSng(TxtPrepaid.Text) & ","
        StrSql = StrSql & "Remarks = '" & TxtRemarks.Text & "',"
        StrSql = StrSql & "Month = '" & TxtMonth.Text & "',"
        StrSql = StrSql & "Year = " & TxtYear.Text & " "
        StrSql = StrSql & "WHERE InvoiceNo = " & TxtInvNo.Text & " AND WhCode = '" & CmbZone.Text & "'"
        Cmd.CommandText = StrSql
        Try
            Cmd.ExecuteNonQuery()
            For Each Row As GridViewRow In GdvInvoice.Rows
                If Row.Cells(5).Text = "Logistics" Then
                    StrSql = "UPDATE Logistics SET "
                    StrSql = StrSql & "Description = '" & Row.Cells(0).Text & "',"
                    StrSql = StrSql & "Quantity = " & ConSng(Row.Cells(1).Text) & ","
                    StrSql = StrSql & "Unit = '" & Row.Cells(2).Text & "',"
                    StrSql = StrSql & "Rate = " & ConSng(Row.Cells(3).Text) & ","
                    StrSql = StrSql & "Amount = " & ConSng(Row.Cells(4).Text)
                    StrSql = StrSql & " WHERE InvoiceNo = " & TxtInvNo.Text
                    StrSql = StrSql & " AND Description = '" & Row.Cells(6).Text & "'"
                Else
                    StrSql = "UPDATE Billing SET "
                    StrSql = StrSql & "Description = '" & Row.Cells(0).Text & "',"
                    StrSql = StrSql & "Quantity = " & ConSng(Row.Cells(1).Text) & ","
                    StrSql = StrSql & "Unit = '" & Row.Cells(2).Text & "',"
                    StrSql = StrSql & "Rate = " & ConSng(Row.Cells(3).Text) & ","
                    StrSql = StrSql & "Amount = " & ConSng(Row.Cells(4).Text)
                    StrSql = StrSql & " WHERE InvoiceNo = " & TxtInvNo.Text
                    StrSql = StrSql & " AND Description = '" & Row.Cells(6).Text & "'"
                End If
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            Next
            Trans.Commit()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub TxtQuantity_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtQuantity.TextChanged
        TxtValue.Text = Format(ConSng(TxtQuantity.Text) * ConSng(TxtRate.Text), "#,##0.00")
        ButUpdate_Click(Me, e)
    End Sub

    Private Sub TxtRate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtRate.TextChanged
        TxtValue.Text = Format(ConSng(TxtQuantity.Text) * ConSng(TxtRate.Text), "#,##0.00")
        ButUpdate_Click(Me, e)
    End Sub

    Protected Sub TxtValue_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtValue.TextChanged
        TxtRate.Text = Format(ConSng(TxtValue.Text) / ConSng(TxtQuantity.Text), "#,##0.00")
        ButUpdate_Click(Me, e)
    End Sub
End Class