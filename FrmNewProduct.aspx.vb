﻿Public Partial Class FrmNewProduct
    Inherits System.Web.UI.Page
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim Con As New OleDb.OleDbConnection

    Const MENUID = "ProductMaster"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            Cmd.CommandText = "SELECT UnitCode FROM Unit"
            Rdr = Cmd.ExecuteReader
            CmbUnit.Items.Add("Select...")
            While Rdr.Read
                CmbUnit.Items.Add(Rdr("UnitCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtProductCode.Text = ""
            TxtHSCode.Text = ""
            CmbUnit.SelectedIndex = 0
            TxtDescription.Text = ""
            TxtExpDate.Text = ""
            TxtRemarks.Text = ""
            TxtProductCode.Focus()
            'StrSql = "SELECT Max(ProdCode) As PCode From NewProduct"
            'Cmd.Connection = Con
            'Cmd.CommandText = StrSql
            'Rdr = Cmd.ExecuteReader
            'If Rdr.Read Then
            '    If Not IsDBNull(Rdr("PCode")) Then
            '        TxtProductCode.Text = Format((Int(Rdr(0)) + 1), "00000")
            '    Else
            '        TxtProductCode.Text = "00001"
            '    End If
            '    Rdr.Close()
            'End If
        End If
        If e.Item.CommandName = "Save" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Product Where ProdCode = '" & TxtProductCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Product (ProdCode,HSCode,Unit,[Description],ExpDate,Remarks) VALUES('"
                StrSql = StrSql & TxtProductCode.Text & "','"
                StrSql = StrSql & UCase(TxtHSCode.Text) & "','"
                StrSql = StrSql & UCase(CmbUnit.Text) & "','"
                StrSql = StrSql & UCase(TxtDescription.Text) & "','"
                StrSql = StrSql & Format(CDate(TxtExpDate.Text), "dd-MMM-yyyy") & "','"
                StrSql = StrSql & UCase(TxtRemarks.Text) & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                TxtProductCode.Text = ""
                TxtHSCode.Text = ""
                CmbUnit.SelectedIndex = 0
                TxtDescription.Text = ""
                TxtExpDate.Text = ""
                TxtRemarks.Text = ""
                TxtProductCode.Focus()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Product Where ProdCode = '" & TxtProductCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtProductCode.Text = ""
                TxtHSCode.Text = ""
                CmbUnit.SelectedIndex = 0
                TxtDescription.Text = ""
                TxtExpDate.Text = ""
                TxtRemarks.Text = ""
                TxtProductCode.Focus()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Next" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 ProdCode FROM Product WHERE ProdCode > '" & TxtProductCode.Text & "'"
                TxtProductCode.Text = Cmd.ExecuteScalar
                TxtProductCode_TextChanged(Me, Ev)
            Catch ex As Exception

            End Try

        End If
        If e.Item.CommandName = "Previous" Then
            Try
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT TOP 1 ProdCode FROM Product WHERE ProdCode < '" & TxtProductCode.Text & "' ORDER BY ProdCode DESC"
                TxtProductCode.Text = Cmd.ExecuteScalar
                TxtProductCode_TextChanged(Me, Ev)
            Catch ex As Exception

            End Try
        End If
        If e.Item.CommandName = "Find" Then
            Session.Add("PstrCode", TxtProductCode.Text)
            ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
        End If
    End Sub

    Private Sub TxtProductCode_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtProductCode.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Cmd.Connection = Con
        StrSql = "Select *From Product Where ProdCode = '" & TxtProductCode.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        TxtHSCode.Text = ""
        CmbUnit.SelectedIndex = 0
        TxtDescription.Text = ""
        TxtExpDate.Text = ""
        TxtRemarks.Text = ""
        If Rdr.Read Then
            TxtHSCode.Text = Rdr("HSCode").ToString
            CmbUnit.Text = Rdr("Unit").ToString
            TxtDescription.Text = Rdr("Description").ToString
            If Not IsDBNull(Rdr("ExpDate")) Then
                TxtExpDate.Text = Format(CDate(Rdr("ExpDate")), "dd/MM/yyyy")
            End If
            TxtRemarks.Text = Rdr("Remarks").ToString
        End If
        Rdr.Close()
    End Sub
End Class