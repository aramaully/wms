﻿Public Partial Class Proposal
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT WhCode FROM WareHouse"
            CmbFpZone.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbFpZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
        If RbtName.Checked Then
            StrSql = StrSql & "Name"
        ElseIf RbtCity.Checked Then
            StrSql = StrSql & "City"
        Else
            StrSql = StrSql & "Contact"
        End If
        StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        PnlFindProp.Height = 400
        PnlFindProp.Width = 600
        PnlFindProp.Style.Item("display") = ""
        TxtClientName.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtClientID_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvFindProp.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFindProp.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub CmbFpZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbFpZone.SelectedIndexChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Name FROM WareHouse WHERE WhCode = '" & CmbFpZone.Text & "'"
        TxtWHouse.Text = Cmd.ExecuteScalar
    End Sub

    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNew.Click
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT MAX(PropNo) FROM Proposal"
        TxtPropNo.Text = Cmd.ExecuteScalar.ToString
        TxtPropNo.Text = Format(Val(TxtPropNo.Text) + 1, "00000000")
    End Sub

    Protected Sub ButPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrint.Click
        Dim StrSelFormula As String
        StrSelFormula = "{Proposal.PropNo} = '" & TxtPropNo.Text & "'"
        Response.Redirect("Report.aspx?RptID=Proposal&RPTFileName=Proposal.aspx&RptFile=RptProposal.rpt&StrSelF=" & StrSelFormula)
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Cmd.CommandText = "DELETE FROM Proposal WHERE PropNo = '" & TxtPropNo.Text & "'"
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Try
            Cmd.ExecuteNonQuery()
            StrSql = "INSERT INTO Proposal (PropNo,PropDate,ClientID,Facilities,Period,WhCode,FacilitySize, "
            StrSql = StrSql & "MonthlyRent,MonthlyService,Total,MonthlyStorage,MinimumStorage,FacilityType) VALUES('"
            StrSql = StrSql & TxtPropNo.Text & "','"
            StrSql = StrSql & CDate(TxtPropDate.Text).ToString("dd-MMM-yyyy") & "','"
            StrSql = StrSql & TxtClient.Text & "','"
            StrSql = StrSql & TxtFacility.Text & "','"
            StrSql = StrSql & TxtPeriod.Text & "','"
            StrSql = StrSql & CmbFpZone.Text & "','"
            StrSql = StrSql & TxtSize.Text & "',"
            StrSql = StrSql & Val(TxtRent.Text) & ","
            StrSql = StrSql & Val(TxtServiceCh.Text) & ","
            StrSql = StrSql & Val(TxtTotal.Text) & ","
            StrSql = StrSql & Val(TxtMinStorage.Text) & ","
            StrSql = StrSql & Val(TxtMinStorage.Text) & ",'"
            StrSql = StrSql & CmbType.Text & "')"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Error Saving Record"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButFindClient_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindClient.Click
        PnlFind.Height = 400
        PnlFind.Width = 600
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        TxtClient.Text = Cmd.ExecuteScalar
    End Sub

    Private Sub ButCloseProp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseProp.Click
        PnlFindProp.Style.Item("display") = "none"
    End Sub

    Private Sub ButFindProp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFindProp.Click
        StrSql = "SELECT PropNo,CONVERT(Varchar,PropDate,103) Date,Client.Name FROM Proposal,Client WHERE Proposal.ClientID = Client.ClientID"
        StrSql = StrSql & " AND Client.Name LIKE '%" + TxtClientName.Text + "%' ORDER BY Client.Name"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFindProp.DataSource = ds
        GdvFindProp.DataBind()
    End Sub

    Private Sub GdvFindProp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFindProp.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFindProp, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFindProp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFindProp.SelectedIndexChanged
        TxtPropNo.Text = GdvFindProp.SelectedRow.Cells(0).Text
        TxtPropNo_TextChanged(Me, e)
        PnlFindProp.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFindProp_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFindProp.SelectedIndexChanging
        GdvFindProp.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub TxtPropNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtPropNo.TextChanged
        If TxtPropNo.Text <> "" Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT Proposal.*,Client.Name FROM Proposal,Client WHERE Proposal.ClientID = Client.ClientID AND PropNo = '" & TxtPropNo.Text & "'"
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                TxtPropDate.Text = Rdr("PropDate")
                TxtClientID.Text = Rdr("ClientID")
                TxtClient.Text = Rdr("Name")
                TxtFacility.Text = Rdr("Facilities")
                TxtPeriod.Text = Rdr("Period")
                CmbFpZone.Text = Rdr("WhCode")
                TxtSize.Text = Rdr("FacilitySize")
                TxtRent.Text = Rdr("MonthlyRent")
                TxtServiceCh.Text = Rdr("MonthlyService")
                TxtTotal.Text = Rdr("Total")
                TxtStorage.Text = Rdr("MonthlyStorage")
                TxtMinStorage.Text = Rdr("MinimumStorage")
                CmbType.Text = Rdr("FacilityType")
            End If
            Rdr.Close()
        End If
    End Sub
End Class