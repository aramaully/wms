﻿Public Partial Class FrmNewWareHouse
    Inherits System.Web.UI.Page
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim Con As New OleDb.OleDbConnection

    Const MENUID = "Warehouse"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT *From WareHouse"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvUnit.DataSource = Rdr
        GdvUnit.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtWarehousCode.Text = ""
            TxtName.Text = ""
            TxtWarehousCode.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM WareHouse Where WhCode = '" & TxtWarehousCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO WareHouse (WhCode,[Name]) VALUES('"
                StrSql = StrSql & UCase(TxtWarehousCode.Text) & "','"
                StrSql = StrSql & UCase(TxtName.Text) & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                TxtWarehousCode.Text = ""
                TxtName.Text = ""
                TxtWarehousCode.Focus()
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM WareHouse Where WhCode = '" & TxtWarehousCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtWarehousCode.Text = ""
                TxtName.Text = ""
                TxtWarehousCode.Focus()
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvCurrency_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvUnit.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvUnit, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvUnit.SelectedIndexChanged
        TxtWarehousCode.Text = GdvUnit.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        TxtName.Text = GdvUnit.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
    End Sub

    Private Sub GdvCurrency_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvUnit.SelectedIndexChanging
        GdvUnit.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvUnit.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvUnit.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

End Class