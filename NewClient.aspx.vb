﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Partial Public Class NewClient
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdSave As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatCabin As New DataTable
    Dim StrClientSequence As String
    Dim StrClientID As String
    Dim CmdUpdateClientID As New OleDb.OleDbCommand

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("Default.aspx")
        End If
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            StrSql = "SELECT DISTINCT(Category) FROM Regime WHERE Category <>'' ORDER BY Category"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbCategory.Items.Clear()
            CmbCategory.Items.Add("Select ...")
            While Rdr.Read
                CmbCategory.Items.Add(Rdr("Category"))
            End While
            CmbCategory.SelectedIndex = -1
            Rdr.Close()
            CmbStatus.Items.Clear()
            CmbStatus.Items.Add("Select ...")
            CmbStatus.Items.Add("Operational")
            CmbStatus.Items.Add("Non Operational")
            CmbStatus.Items.Add("Dormant")
            CmbStatus.SelectedIndex = -1
            CmbType.Items.Clear()
            CmbType.Items.Add("Select...")
            CmbType.Items.Add("Normal")
            CmbType.Items.Add("Potential")
            StrSql = "SELECT [Name] FROM Country ORDER BY CountryCode"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            CmbCountry.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbCountry.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()
            ButNewClient_Click(Me, e)
        End If
        If Page.IsPostBack And PnlFind.Style.Item("display") = "" Then
            DragPanel.Style.Item("left") = HfLeft.Value
            DragPanel.Style.Item("top") = HfTop.Value
        End If
    End Sub

    Private Sub ClearAll()
        On Error Resume Next
        CmbCategory.SelectedIndex = -1
        TxtName.Text = ""
        TxtAddress.Text = ""
        TxtCity.Text = ""
        TxtTel.Text = ""
        TxtFaxNo.Text = ""
        TxtEmail.Text = ""
        TxtContact.Text = ""
        TxtCellNo.Text = ""
        TxtVatNo.Text = ""
        TxtPosition.Text = ""
        TxtDate.Text = Date.Today
        TxtRemarks.Text = ""
        TxtLicExp.Text = ""
        TxtPermitExp.Text = ""
        TxtLicenceNo.Text = ""
        TxtStoragePermit.Text = ""
        CmbStatus.SelectedIndex = -1
        TxtProType.Text = ""
        TxtBRN.Text = ""
        TxtAreaLeased.Text = ""
        TxtActivities.Text = ""
        ChkZone1.Checked = False
        ChkZone6.Checked = False
        ChkZone9.Checked = False
        TxtWeb.Text = ""
        TxtAreaLeased.Text = ""
        TxtAddress2.Text = ""
        LblMsg.Visible = False
        LinkButton1.Visible = False
        CmbCountry.SelectedIndex = -1
    End Sub

    Function GetNewID() As String
        Cmd.Connection = Con
        If CmbType.Text = "Normal" Then
            Cmd.CommandText = "Select ClientID From [System] Where Whcode = 'FPZ01'"
            Dim StrClientSequence = Cmd.ExecuteScalar.ToString
            StrClientID = "400C" & StrClientSequence
        ElseIf CmbType.Text = "Potential" Then
            Cmd.CommandText = "Select ClientID From [System] Where Whcode = 'FPZ06'"
            Dim StrClientSequence = Cmd.ExecuteScalar.ToString
            StrClientID = "400PC" & StrClientSequence
        Else
            StrClientID = ""
        End If
        Return StrClientID
    End Function

    Protected Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtClientID.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            ClearAll()
            TxtClientID.Text = Rdr("ClientID").ToString
            CmbCategory.Text = Trim(Rdr("Category")).ToString
            TxtName.Text = Rdr("Name").ToString
            TxtAddress.Text = Rdr("Address").ToString
            TxtCity.Text = Rdr("City").ToString
            TxtTel.Text = Rdr("Telephone").ToString
            TxtFaxNo.Text = Rdr("Fax").ToString
            TxtEmail.Text = Rdr("EMail").ToString
            TxtContact.Text = Rdr("Contact").ToString
            TxtCellNo.Text = Rdr("CellNo").ToString
            TxtPosition.Text = Rdr("Position").ToString
            TxtDate.Text = Format(Rdr("DateCreated"), "dd/MM/yyyy")
            TxtRemarks.Text = Rdr("Remarks").ToString
            TxtVatNo.Text = Rdr("VATNo").ToString
            TxtLicenceNo.Text = Rdr("LicenceNo").ToString
            TxtStoragePermit.Text = Rdr("StoragePermit").ToString
            TxtLicExp.Text = Format(Rdr("LicenceExp"), "dd/MM/yyyy")
            TxtPermitExp.Text = Format(Rdr("PermitExp"), "dd/MM/yyyy")
            CmbStatus.Text = Rdr("Status").ToString
            TxtProType.Text = Rdr("ProdType").ToString
            TxtActivities.Text = Rdr("Activity").ToString
            TxtBRN.Text = Rdr("ClientBRN").ToString
            TxtWeb.Text = Rdr("WebURL").ToString
            TxtAreaLeased.Text = Rdr("AreaLeased").ToString
            TxtAddress2.Text = Rdr("Address2").ToString
            CmbCountry.Text = Rdr("Country").ToString
            If Rdr("PotentialClient") = True Then
                CmbType.SelectedIndex = 2
            Else
                CmbType.SelectedIndex = 1
            End If
            Select Case IIf(IsDBNull(Rdr("FPZones")), 0, Rdr("FPZones"))
                Case 1
                    ChkZone1.Checked = True
                    ChkZone6.Checked = False
                    ChkZone9.Checked = False
                Case 6
                    ChkZone1.Checked = False
                    ChkZone6.Checked = True
                    ChkZone9.Checked = False
                Case 9
                    ChkZone1.Checked = False
                    ChkZone6.Checked = False
                    ChkZone9.Checked = True
                Case 7
                    ChkZone1.Checked = True
                    ChkZone6.Checked = True
                    ChkZone9.Checked = False
                Case 10
                    ChkZone1.Checked = True
                    ChkZone6.Checked = False
                    ChkZone9.Checked = True
                Case 15
                    ChkZone1.Checked = False
                    ChkZone6.Checked = True
                    ChkZone9.Checked = True
                Case 16
                    ChkZone1.Checked = True
                    ChkZone6.Checked = True
                    ChkZone9.Checked = True
                Case Else
                    ChkZone1.Checked = False
                    ChkZone6.Checked = False
                    ChkZone9.Checked = False
            End Select
            Rdr.Close()
        Else
            ClearAll()
        End If
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNextClient.Click
        If TxtClientID.Text = "" Then
            StrSql = "SELECT TOP 1 ClientID FROM Client"
        Else
            StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID > '" & TxtClientID.Text & "'"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevClient.Click
        If TxtClientID.Text = "" Then
            StrSql = "SELECT TOP 1 ClientID FROM Client ORDER BY ClientID DESC"
        Else
            StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID < '" & TxtClientID.Text & "' ORDER BY ClientID DESC"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindClient.Click
        DragPanel.Style.Item("display") = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Protected Sub ButSelect_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Protected Sub ButRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE ClientID <> '' "
        If TxtSearch.Text <> "" Then
            If RbtName.Checked Then
                StrSql = StrSql & "AND Name"
            ElseIf RbtCity.Checked Then
                StrSql = StrSql & "AND City"
            Else
                StrSql = StrSql & "AND Contact"
            End If
            StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' "
        End If
        If ChkPotential.Checked Then
            StrSql = StrSql & "AND PotentialClient = 1 "
        End If
        StrSql = StrSql & "ORDER BY ClientID"
        Try
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            GdvFind.DataSource = Rdr
            GdvFind.DataBind()
            Rdr.Close()
        Catch ex As Exception
            TxtName.Text = ex.Message
        End Try
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='pointer';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtClientID_TextChanged(Me, e)
        DragPanel.Style.Item("display") = "none"
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        System.Threading.Thread.Sleep(2000)
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSaveClient.Click
        Page.Validate("ClientVal")
        If Not Page.IsValid Then
            Dim StrScript As String = "<script language='javascript'> w.Alert('Please Enter Fields marked *','Data Required','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
            Exit Sub
        End If
        Cmd.Connection = Con
        StrSql = "SELECT COUNT(ClientID) FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Dim IntZones As Integer = 0
        If ChkZone1.Checked Then
            IntZones = IntZones + 1
        End If
        If ChkZone6.Checked Then
            IntZones = IntZones + 6
        End If
        If ChkZone9.Checked Then
            IntZones = IntZones + 9
        End If
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Cmd.ExecuteScalar = 0 Then
            StrSql = "INSERT INTO Client (ClientID,Category,Name,Address,City,Telephone,Fax,EMail,Contact,CellNo,Position,DateCreated,"
            StrSql = StrSql & "VATNo,LicenceNo,LicenceExp,StoragePermit,PermitExp,Remarks,Status,ProdType,"
            StrSql = StrSql & "Address2,ClientBRN,AreaLeased,Activity,WebURL,FPZones,PotentialClient,Country) VALUES('"
            StrSql = StrSql & Replace((TxtClientID.Text), "'", "''") & "','"
            StrSql = StrSql & CmbCategory.Text & "','"
            StrSql = StrSql & Replace((TxtName.Text), "'", "''") & "','"
            StrSql = StrSql & Replace((TxtAddress.Text), "'", "''") & "','"
            StrSql = StrSql & Replace((TxtCity.Text), "'", "''") & "','"
            StrSql = StrSql & Replace((TxtTel.Text), "'", "''") & "','"
            StrSql = StrSql & TxtFaxNo.Text & "','"
            StrSql = StrSql & TxtEmail.Text & "','"
            StrSql = StrSql & Replace((TxtContact.Text), "'", "''") & "','"
            StrSql = StrSql & TxtCellNo.Text & "','"
            StrSql = StrSql & Replace((TxtPosition.Text), "'", "''") & "',"
            If TxtDate.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtDate.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & TxtVatNo.Text & "','"
            StrSql = StrSql & TxtLicenceNo.Text & "',"
            If TxtLicExp.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtLicExp.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & TxtStoragePermit.Text & "',"
            If TxtPermitExp.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtPermitExp.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & Replace((TxtRemarks.Text), "'", "''") & "','"
            StrSql = StrSql & CmbStatus.Text & "','"
            StrSql = StrSql & Replace((TxtProType.Text), "'", "''") & "','"
            StrSql = StrSql & Replace((TxtAddress2.Text), "'", "''") & "','"
            StrSql = StrSql & TxtBRN.Text & "','"
            StrSql = StrSql & TxtAreaLeased.Text & "','"
            StrSql = StrSql & Replace((TxtActivities.Text), "'", "''") & "','"
            StrSql = StrSql & Replace((TxtWeb.Text), "'", "''") & "',"
            StrSql = StrSql & IntZones & ","
            If CmbType.Text = "Potential" Then
                StrSql = StrSql & "1,'" & CmbCountry.Text & "')"
            Else
                StrSql = StrSql & "0,'" & CmbCountry.Text & "')"
            End If
        Else
            StrSql = "UPDATE Client SET "
            StrSql = StrSql & "ClientID = '" & TxtClientID.Text & "',"
            StrSql = StrSql & "Category = '" & CmbCategory.Text & "',"
            StrSql = StrSql & "Name = '" & Replace((TxtName.Text), "'", "''") & "',"
            StrSql = StrSql & "Address = '" & Replace((TxtAddress.Text), "'", "''") & "',"
            StrSql = StrSql & "City = '" & Replace((TxtCity.Text), "'", "''") & "',"
            StrSql = StrSql & "Telephone = '" & TxtTel.Text & "',"
            StrSql = StrSql & "Fax = '" & TxtFaxNo.Text & "',"
            StrSql = StrSql & "EMail = '" & Replace((TxtEmail.Text), "'", "''") & "',"
            StrSql = StrSql & "Contact = '" & TxtContact.Text & "',"
            StrSql = StrSql & "CellNo = '" & TxtCellNo.Text & "',"
            StrSql = StrSql & "Position = '" & Replace((TxtPosition.Text), "'", "''") & "',"
            If TxtDate.Text = "" Then
                StrSql = StrSql & "DateCreated = NULL,"
            Else
                StrSql = StrSql & "DateCreated = '" & CDate(TxtDate.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "VATNo = '" & TxtVatNo.Text & "',"
            StrSql = StrSql & "LicenceNo = '" & TxtLicenceNo.Text & "',"
            If TxtLicExp.Text = "" Then
                StrSql = StrSql & "LicenceExp = NULL,"
            Else
                StrSql = StrSql & "LicenceExp = '" & CDate(TxtLicExp.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "StoragePermit = '" & TxtStoragePermit.Text & "',"
            If TxtPermitExp.Text = "" Then
                StrSql = StrSql & "PermitExp = NULL,"
            Else
                StrSql = StrSql & "PermitExp = '" & CDate(TxtPermitExp.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            If CmbType.Text = "Normal" Then
                StrSql = StrSql & "PotentialClient = 0,"
            ElseIf CmbType.Text = "Potential" Then
                StrSql = StrSql & "PotentialClient = 1,"
            End If
            StrSql = StrSql & "Remarks = '" & Replace((TxtRemarks.Text), "'", "''") & "',"
            StrSql = StrSql & "Country = '" & CmbCountry.Text & "',"
            StrSql = StrSql & "Status = '" & CmbStatus.Text & "',"
            StrSql = StrSql & "ProdType = '" & Replace((TxtProType.Text), "'", "''") & "',"
            StrSql = StrSql & "Address2 = '" & Replace((TxtAddress2.Text), "'", "''") & "',"
            StrSql = StrSql & "ClientBRN = '" & TxtBRN.Text & "',"
            StrSql = StrSql & "AreaLeased = '" & TxtAreaLeased.Text & "',"
            StrSql = StrSql & "Activity = '" & Replace((TxtActivities.Text), "'", "''") & "',"
            StrSql = StrSql & "WebURL = '" & Replace((TxtWeb.Text), "'", "''") & "',"
            StrSql = StrSql & "FPZones = " & IntZones
            StrSql = StrSql & " WHERE ClientID = '" & TxtClientID.Text & "'"
        End If
        CmdUpdateClientID.Connection = Con
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        CmdUpdateClientID.Transaction = Trans
        Cmd.Transaction = Trans
        Cmd.CommandText = StrSql
        If CmbType.Text = "Normal" Then
            CmdUpdateClientID.CommandText = "Update [System] Set ClientID = ClientID + 1 Where Whcode = 'FPZ01'"
        ElseIf CmbType.Text = "Potential" Then
            CmdUpdateClientID.CommandText = "Update [System] Set ClientID = ClientID + 1 Where Whcode = 'FPZ06'"
        End If
        Try
            Cmd.ExecuteNonQuery()
            CmdUpdateClientID.ExecuteNonQuery()
            Trans.Commit()
            LblMsg.Visible = True
            LinkButton1.Visible = True
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "GetID", "<script language='javascript'> GetID();</script>", False)
        Catch ex As Exception
            Trans.Rollback()
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Saving','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try
    End Sub

    Protected Sub ButNewClient_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNewClient.Click
        ClearAll()
        TxtClientID.Text = GetNewID()
        CmbCategory.Focus()
    End Sub

    Protected Sub ButDeleteClient_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDeleteClient.Click
        Cmd.Connection = Con
        Cmd.CommandText = "Delete From Client Where ClientID = '" & TxtClientID.Text & "'"
        Cmd.ExecuteNonQuery()
        ClearAll()
    End Sub

    Protected Sub CmbType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbType.SelectedIndexChanged
        ButNewClient_Click(Me, e)
    End Sub
End Class