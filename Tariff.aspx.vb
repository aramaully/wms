﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Partial Public Class Tariff
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdSave As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatCabin As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("Default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            StrSql = "SELECT WhCode FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbFpZone.Items.Clear()
            CmbFpZone.Items.Add("Select...")
            While Rdr.Read
                CmbFpZone.Items.Add(Rdr("WhCode"))
            End While
            CmbFpZone.SelectedIndex = 0
            Rdr.Close()
            Cmd.CommandText = "SELECT * FROM Charges ORDER BY ChCode"
            Rdr = Cmd.ExecuteReader
            GdvStd.DataSource = Rdr
            GdvStd.DataBind()
            Rdr.Close()
            Rdr = Cmd.ExecuteReader
            GdvPref.DataSource = Rdr
            GdvPref.DataBind()
            Rdr.Close()
            TabContainer1.ActiveTabIndex = 0
            GdvPref.Columns(5).Visible = False
            GdvStd.Columns(5).Visible = False
        End If
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Private Sub ClearAll()
        On Error Resume Next
        TxtName.Text = ""
        CmbFpZone.SelectedIndex = 0
        TxtFPZone.Text = ""
        For Each Row As GridViewRow In GdvStd.Rows
            DirectCast(Row.FindControl("chkSel"), CheckBox).Checked = False
        Next
        For Each Row As GridViewRow In GdvPref.Rows
            DirectCast(Row.FindControl("chkSel"), CheckBox).Checked = False
        Next
    End Sub

    Protected Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            ClearAll()
            TxtName.Text = Rdr("Name").ToString
            Rdr.Close()
        Else
            ClearAll()
            TxtName.Text = "Invalid Client"
        End If
    End Sub

    Protected Sub UpdateRates()
        StrSql = "SELECT * FROM ClientCharges WHERE ClientID = '" & TxtClientID.Text & "' "
        StrSql = StrSql & "AND WhCode = '" & CmbFpZone.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        While Rdr.Read
            If Rdr("Pref") Then
                For Each Row As GridViewRow In GdvPref.Rows
                    If Rdr("ChCode") = Row.Cells(0).Text Then
                        DirectCast(Row.FindControl("ChkSel"), CheckBox).Checked = True
                    End If
                Next
            Else
                For Each Row As GridViewRow In GdvStd.Rows
                    If Rdr("ChCode") = Row.Cells(0).Text Then
                        DirectCast(Row.FindControl("ChkSel"), CheckBox).Checked = True
                    End If
                Next
            End If
        End While
        Rdr.Close()
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
        StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID > '" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevious.Click
        StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID < '" & TxtClientID.Text & "' ORDER BY ClientID DESC"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFind.Click
        PnlFind.Height = 400
        PnlFind.Width = 600
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
     End Sub

    Protected Sub ButSelect_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Protected Sub ButRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
        If RbtName.Checked Then
            StrSql = StrSql & "Name"
        ElseIf RbtCity.Checked Then
            StrSql = StrSql & "City"
        Else
            StrSql = StrSql & "Contact"
        End If
        StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtClientID_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        System.Threading.Thread.Sleep(2000)
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Cmd.Connection = Con
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Transaction = Trans
        Try
            StrSql = "DELETE FROM ClientCharges WHERE ClientID = '" & TxtClientID.Text & "' "
            StrSql = StrSql & "AND WhCode = '" & CmbFpZone.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            'Update Normal Rates
            For Each Row As GridViewRow In GdvStd.Rows
                If DirectCast(Row.FindControl("chkSel"), CheckBox).Checked Then
                    StrSql = "INSERT INTO ClientCharges (ClientID,ChCode,Description,Unit,Charge,AnalysisCode,WhCode,Pref) VALUES('"
                    StrSql = StrSql & TxtClientID.Text & "','"
                    StrSql = StrSql & Row.Cells(0).Text & "','"
                    StrSql = StrSql & Row.Cells(1).Text & "','"
                    StrSql = StrSql & Row.Cells(2).Text & "',"
                    StrSql = StrSql & Val(Row.Cells(3).Text) & ",'"
                    StrSql = StrSql & Row.Cells(5).Text & "','"
                    StrSql = StrSql & CmbFpZone.Text & "',0)"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            For Each Row As GridViewRow In GdvPref.Rows
                If DirectCast(Row.FindControl("chkSel"), CheckBox).Checked Then
                    StrSql = "DELETE FROM ClientCharges WHERE ClientID = '" & TxtClientID.Text & "' AND "
                    StrSql = StrSql & "ChCode = '" & Row.Cells(0).Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                    StrSql = "INSERT INTO ClientCharges (ClientID,ChCode,Description,Unit,Charge,AnalysisCode,WhCode,Pref) VALUES('"
                    StrSql = StrSql & TxtClientID.Text & "','"
                    StrSql = StrSql & Row.Cells(0).Text & "','"
                    StrSql = StrSql & Row.Cells(1).Text & "','"
                    StrSql = StrSql & Row.Cells(2).Text & "',"
                    StrSql = StrSql & Val(DirectCast(Row.FindControl("TxtPref"), TextBox).Text) & ",'"
                    StrSql = StrSql & Row.Cells(5).Text & "','"
                    StrSql = StrSql & CmbFpZone.Text & "',1)"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            Trans.Commit()
            ClearAll()
        Catch ex As Exception
            Trans.Rollback()
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub CmbFpZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbFpZone.SelectedIndexChanged
        If CmbFpZone.Text = "Select..." Then Exit Sub
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Name FROM WareHouse WHERE WhCode = '" & CmbFpZone.Text & "'"
        TxtFPZone.Text = Cmd.ExecuteScalar.ToString
        UpdateRates()
    End Sub

    Private Sub GdvPref_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvPref.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            DirectCast(e.Row.FindControl("TxtPref"), TextBox).Style("text-align") = "right"
            If e.Row.RowIndex Mod 2 > 0 Then
                DirectCast(e.Row.FindControl("TxtPref"), TextBox).BackColor = GdvPref.AlternatingRowStyle.BackColor
            End If
        End If
    End Sub
End Class