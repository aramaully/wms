﻿Public Partial Class FrmNewRegime
    Inherits System.Web.UI.Page
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim Con As New OleDb.OleDbConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            FillGrid()
            CmbType.Items.Add(" ")
            CmbType.Items.Add("In")
            CmbType.Items.Add("Out")
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT *From Regime"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvCurrency.DataSource = Rdr
        GdvCurrency.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtCPCCode.Text = ""
            TxtRegime.Text = ""
            TxtCategory.Text = ""
            TxtDescription.Text = ""
            CmbType.SelectedIndex = 0
            TxtCPCCode.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Regime Where CpcCode = '" & TxtCPCCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Regime (CpcCode,RegimeCode,Category,[Name],[Type]) VALUES('"
                StrSql = StrSql & UCase(TxtCPCCode.Text) & "','"
                StrSql = StrSql & UCase(TxtRegime.Text) & "','"
                StrSql = StrSql & UCase(TxtCategory.Text) & "','"
                StrSql = StrSql & UCase(TxtCPCCode.Text) & "','"
                StrSql = StrSql & CmbType.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                TxtCPCCode.Text = ""
                TxtRegime.Text = ""
                TxtCategory.Text = ""
                TxtDescription.Text = ""
                CmbType.SelectedIndex = 0
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Regime Where CpcCode = '" & TxtCPCCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtCPCCode.Text = ""
                TxtRegime.Text = ""
                TxtCategory.Text = ""
                TxtDescription.Text = ""
                CmbType.SelectedIndex = 0
                TxtCPCCode.Focus()
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvCurrency_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvCurrency.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvCurrency, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvCurrency.SelectedIndexChanged
        TxtCPCCode.Text = GdvCurrency.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        TxtRegime.Text = GdvCurrency.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
        TxtCategory.Text = GdvCurrency.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
        TxtDescription.Text = GdvCurrency.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
        If GdvCurrency.SelectedRow.Cells(4).Text <> "" And GdvCurrency.SelectedRow.Cells(4).Text <> "&nbsp;" Then
            CmbType.Text = GdvCurrency.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
        End If
    End Sub

    Private Sub GdvCurrency_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvCurrency.SelectedIndexChanging
        GdvCurrency.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvCurrency.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCurrency.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

End Class