﻿Public Partial Class PayInv
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "InvoicePayment"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT WhCode FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            TxtTotal.Style("text-align") = "right"
            TxtVat.Style("text-align") = "right"
            TxtAmount.Style("text-align") = "right"
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT Invoice.InvoiceNo, CONVERT(Char,Invoice.InvDate,103) 'Invoice Date', Invoice.Value, Client.Name, Invoice.Paid FROM Invoice, Client WHERE "
        StrSql = StrSql & "Invoice.ClientID = Client.ClientID "
        If RbnClient.Checked Then
            StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
        End If
        If ChkDate.Checked Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = StrSql & "AND Invoice.InvDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND Invoice.InvDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        PnlFind.Style.Item("display") = ""
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtInvoiceNo.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtInvoiceNo_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtInvoiceNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtInvoiceNo.TextChanged
        StrSql = "SELECT Invoice.*,Client.Name FROM Invoice,Client WHERE Invoice.ClientID = Client.ClientID "
        StrSql = StrSql & "AND InvoiceNo = " & TxtInvoiceNo.Text & " AND WhCode = '" & CmbZone.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtDate.Text = Rdr("InvDate")
            TxtName.Text = Rdr("Name")
            TxtAmount.Text = Format(Rdr("Amount"), "#,##0.00")
            TxtVat.Text = Format(Rdr("Vat"), "#,##0.00")
            TxtTotal.Text = Format(Rdr("Value"), "#,##0.00")
            TxtBillNo.Text = Rdr("BoeNo").ToString
            TxtRemarks.Text = Rdr("Remarks").ToString
            TxtReceipt.Text = Rdr("ReceiptNo").ToString
            TxtDatePaid.Text = IIf(IsDBNull(Rdr("DatePaid")), Date.Today, Rdr("DatePaid"))
            If Rdr("Paid") Then
                ChkPaid.Checked = True
                ButSave.Enabled = False
            Else
                ChkPaid.Checked = False
                ButSave.Enabled = True
                TxtReceipt.Focus()
            End If
        Else
            TxtName.Text = "Invalid Invoice No"
            Exit Sub
        End If
        Rdr.Close()
        StrSql = "SELECT Description, Quantity, Unit, Rate, Amount "
        StrSql = StrSql & " FROM Billing WHERE InvoiceNo = " & TxtInvoiceNo.Text & " AND WhCode = '" & CmbZone.Text & "'"
        StrSql = StrSql & "UNION ALL SELECT Description, Quantity, Unit, Rate, Amount "
        StrSql = StrSql & " FROM Logistics WHERE InvoiceNo = " & TxtInvoiceNo.Text & " AND WhCode = '" & CmbZone.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvInv.DataSource = Rdr
        GdvInv.DataBind()
        Rdr.Close()
        For Each Row As GridViewRow In GdvInv.Rows
            If Row.Cells(0).Text = "Other Charges" Then
                Row.Visible = False
            End If
        Next
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        StrSql = "UPDATE Invoice SET Paid = 1,ReceiptNo = " & TxtReceipt.Text & ",DatePaid = '" & TxtDatePaid.Text & "' "
        StrSql = StrSql & "WHERE InvoiceNo = " & TxtInvoiceNo.Text & " AND WhCode = '" & CmbZone.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Try
            Cmd.ExecuteNonQuery()
            TxtInvoiceNo_TextChanged(Me, e)
        Catch ex As Exception
            ErrHead.Text = "Payment Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class