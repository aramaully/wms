﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Partial Public Class CheckClient
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdSave As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatCabin As New DataTable
    Dim StrClientSequence As String
    Dim StrClientID As String
    Dim CmdUpdateClientID As New OleDb.OleDbCommand

    Const MENUID = "ClientMaster"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("Default.aspx")
        End If
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT DISTINCT(Category) FROM Regime WHERE Category <>'' ORDER BY Category"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbCategory.Items.Clear()
            CmbCategory.Items.Add("Select ...")
            While Rdr.Read
                CmbCategory.Items.Add(Rdr("Category"))
            End While
            CmbCategory.SelectedIndex = -1
            Rdr.Close()
            CmbStatus.Items.Clear()
            CmbStatus.Items.Add("Select ...")
            CmbStatus.Items.Add("Operational")
            CmbStatus.Items.Add("Non Operational")
            CmbStatus.Items.Add("Dormant")
            CmbStatus.SelectedIndex = -1
        End If
        If Page.IsPostBack And PnlFind.Style.Item("display") = "" Then
            DragPanel.Style.Item("left") = HfLeft.Value
            DragPanel.Style.Item("top") = HfTop.Value
        End If
    End Sub

    Private Sub ClearAll()
        On Error Resume Next
        CmbCategory.SelectedIndex = -1
        TxtName.Text = ""
        TxtAddress.Text = ""
        TxtCity.Text = ""
        TxtTel.Text = ""
        TxtFaxNo.Text = ""
        TxtEmail.Text = ""
        TxtContact.Text = ""
        TxtCellNo.Text = ""
        TxtVatNo.Text = ""
        TxtPosition.Text = ""
        TxtDate.Text = Date.Today
        TxtRemarks.Text = ""
        TxtLicExp.Text = ""
        TxtPermitExp.Text = ""
        TxtLicenceNo.Text = ""
        TxtStoragePermit.Text = ""
        CmbStatus.SelectedIndex = -1
        TxtProType.Text = ""
        TxtBRN.Text = ""
        TxtAreaLeased.Text = ""
        TxtActivities.Text = ""
        ChkZone1.Checked = False
        ChkZone6.Checked = False
        ChkZone9.Checked = False
        TxtWeb.Text = ""
        TxtAreaLeased.Text = ""
        TxtAddress2.Text = ""
    End Sub

    Function GetNewID() As String
        Cmd.Connection = Con
        Cmd.CommandText = "Select ClientID From [System] Where Whcode = 'FPZ01'"
        Dim StrClientSequence = Cmd.ExecuteScalar.ToString
        StrClientID = "400C" & StrClientSequence
        Return StrClientID
    End Function

    Protected Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtClientID.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            ClearAll()
            TxtClientID.Text = Rdr("ClientID").ToString
            CmbCategory.Text = Trim(Rdr("Category")).ToString
            TxtName.Text = Rdr("Name").ToString
            TxtAddress.Text = Rdr("Address").ToString
            TxtCity.Text = Rdr("City").ToString
            TxtTel.Text = Rdr("Telephone").ToString
            TxtFaxNo.Text = Rdr("Fax").ToString
            TxtEmail.Text = Rdr("EMail").ToString
            TxtContact.Text = Rdr("Contact").ToString
            TxtCellNo.Text = Rdr("CellNo").ToString
            TxtPosition.Text = Rdr("Position").ToString
            TxtDate.Text = Format(Rdr("DateCreated"), "dd/MM/yyyy")
            TxtRemarks.Text = Rdr("Remarks").ToString
            TxtVatNo.Text = Rdr("VATNo").ToString
            TxtLicenceNo.Text = Rdr("LicenceNo").ToString
            TxtStoragePermit.Text = Rdr("StoragePermit").ToString
            TxtLicExp.Text = Format(Rdr("LicenceExp"), "dd/MM/yyyy")
            TxtPermitExp.Text = Format(Rdr("PermitExp"), "dd/MM/yyyy")
            CmbStatus.Text = Rdr("Status").ToString
            TxtProType.Text = Rdr("ProdType").ToString
            TxtActivities.Text = Rdr("Activities").ToString
            TxtBRN.Text = Rdr("ClientBRN").ToString
            TxtWeb.Text = Rdr("WebURL").ToString
            TxtAreaLeased.Text = Rdr("AreaLeased").ToString
            TxtAddress2.Text = Rdr("Address2").ToString
            Select Case IIf(IsDBNull(Rdr("FPZones")), 0, Rdr("FPZones"))
                Case 1
                    ChkZone1.Checked = True
                    ChkZone6.Checked = False
                    ChkZone9.Checked = False
                Case 6
                    ChkZone1.Checked = False
                    ChkZone6.Checked = True
                    ChkZone9.Checked = False
                Case 9
                    ChkZone1.Checked = False
                    ChkZone6.Checked = False
                    ChkZone9.Checked = True
                Case 7
                    ChkZone1.Checked = True
                    ChkZone6.Checked = True
                    ChkZone9.Checked = False
                Case 10
                    ChkZone1.Checked = True
                    ChkZone6.Checked = False
                    ChkZone9.Checked = True
                Case 15
                    ChkZone1.Checked = False
                    ChkZone6.Checked = True
                    ChkZone9.Checked = True
                Case 16
                    ChkZone1.Checked = True
                    ChkZone6.Checked = True
                    ChkZone9.Checked = True
                Case Else
                    ChkZone1.Checked = False
                    ChkZone6.Checked = False
                    ChkZone9.Checked = False
            End Select
            Rdr.Close()
        Else
            ClearAll()
        End If
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNextClient.Click
        If TxtClientID.Text = "" Then
            StrSql = "SELECT TOP 1 ClientID FROM Client"
        Else
            StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID > '" & TxtClientID.Text & "'"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevClient.Click
        If TxtClientID.Text = "" Then
            StrSql = "SELECT TOP 1 ClientID FROM Client ORDER BY ClientID DESC"
        Else
            StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID < '" & TxtClientID.Text & "' ORDER BY ClientID DESC"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindClient.Click
        DragPanel.Style.Item("display") = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Protected Sub ButSelect_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Protected Sub ButRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE ClientID <> '' "
        If TxtSearch.Text <> "" Then
            If RbtName.Checked Then
                StrSql = StrSql & "AND Name"
            ElseIf RbtCity.Checked Then
                StrSql = StrSql & "AND City"
            Else
                StrSql = StrSql & "AND Contact"
            End If
            StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' "
        End If
        If ChkPotential.Checked Then
            StrSql = StrSql & "AND PotentialClient = 1 "
        End If
        StrSql = StrSql & "ORDER BY Name"
        Try
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            GdvFind.DataSource = Rdr
            GdvFind.DataBind()
            Rdr.Close()
        Catch ex As Exception
            TxtName.Text = ex.Message
        End Try
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='pointer';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtClientID_TextChanged(Me, e)
        DragPanel.Style.Item("display") = "none"
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

End Class