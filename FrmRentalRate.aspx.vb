﻿Public Partial Class FrmRentalRate
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "RentalRate"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            Cmd.CommandText = "SELECT Whcode FROM [System]"
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Dim RptChargedPerSq As New RptChargedPerSq
            Dim StrSelFormula As String
            Dim DteFrom As Date = Format(CDate(TxtDateFrom.Text), "dd/MM/yyyy")
            Dim StrFrom As String = Format(DteFrom, "yyyy,MM,dd")
            Dim DteTo As Date = Format(CDate(TxtDateTo.Text), "dd/MM/yyyy")
            Dim StrTo As String = Format(DteTo, "yyyy,MM,dd")
            StrSelFormula = "{LeaseAgreement.LeaseDate}>=date(" & StrFrom & ") And {LeaseAgreement.LeaseDate}<=date(" & StrTo & ")"
            If CmbZone.Text <> "Select..." Then
                StrSelFormula = StrSelFormula & " And {LeaseAgreement.Zone} = '" & CmbZone.Text & "'"
            End If
            Session("ReportFile") = "RptChargedPerSq.rpt"
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Rental rate charged per Sq.m per client and Zone " & TxtDateFrom.Text & " to " & TxtDateTo.Text
            Session("Period") = "'(" & Format(CDate(TxtDateFrom.Text), "dd/MM/yy") & " To " & Format(CDate(TxtDateTo.Text), "dd/MM/yy") & ")'"
            Session("Zone") = "'" & CmbZone.Text & "'"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub

End Class