﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RepOccupancy.aspx.vb" Inherits="Wms.RepOccupancy" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 98%;
        }
        .style3
        {
            height: 47px;
        }
        .ModalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .style19
        {
            width: 16px;
        }
        .style20
        {
            width: 97px;
        }
    </style>
</head>
<body style="height: 100%">
    <form id="form1" runat="server" style="height: 217px">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 224px; position: absolute; top: 113px; height: 93px;"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" 
                    Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server"  Height="174px" BorderStyle="Groove"
                    BorderWidth="1px" Width="523px">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Occupancy Report"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="BtnFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find" Width="55px" UseSubmitBehavior="False" />
                                <asp:Button ID="BtnView" runat="server" Height="39px" Style="cursor: pointer" 
                                    Text="View" ToolTip="View Report" UseSubmitBehavior="False" Width="55px" />
                                <asp:Button ID="ButExit" runat="server" Height="39px" Text="Close" Style="cursor: pointer"
                                    ToolTip="Close this Module" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButExit_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Close this Module?"
                                    Enabled="True" TargetControlID="ButExit">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    
                    <table align="Left" border="0" cellpadding="0" cellspacing="0" 
                        style="height: 85px; width: 50%">
        <tr>
            <td align="left" bgcolor="#FFFFFF" valign="middle">
                
            </td>
            <td align="left" bgcolor="#FFFFFF" colspan="2" valign="middle" 
                style="text-align: center">
                &nbsp;</td>
            <td style="background-image: url(images/cen_rig.gif)" width="24">
            </td>
        </tr>
       
      
        <tr>
            <td align="left" bgcolor="#FFFFFF"  valign="middle">
            </td>
            <td align="left" bgcolor="#FFFFFF" valign="middle" class="style20">
                <asp:Label ID="Label5" runat="server" Text="Client"></asp:Label>
            </td>
            <td align="left" bgcolor="#FFFFFF" valign="middle">
                <asp:TextBox ID="TxtClientID" runat="server" AutoPostBack="True" Width="151px">ALL</asp:TextBox>
            </td>
            <td style="background-image: url(images/cen_rig.gif)" width="24">
                <asp:Label ID="Label2" runat="server" Text="FP Zone" Width="80px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="CmbZone" runat="server" Width="127px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" bgcolor="#FFFFFF"  valign="middle">
                </td>
            <td align="left" bgcolor="#FFFFFF" valign="middle" class="style20">
                Name</td>
            <td align="left" bgcolor="#FFFFFF" valign="middle" colspan="3">
                <asp:TextBox ID="TxtClient" runat="server" Width="382px"></asp:TextBox>
            </td>
        </tr>
       
                        <tr>
                            <td align="left" bgcolor="#FFFFFF" valign="middle">
                                &nbsp;</td>
                            <td align="left" bgcolor="#FFFFFF" class="style20" valign="middle">
                                <asp:CheckBox ID="ChkOcDate" runat="server" Text="Date Range" Width="120px" />
                            </td>
                            <td>
                                <asp:TextBox ID="TxtOcFrom" runat="server" Width="151px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtFrom_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="TxtOcFrom">
                                </cc1:CalendarExtender>
                            </td>
                            <td>
                                <asp:Label ID="Label6" runat="server" Text="To"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#FFFFFF" valign="middle">
                                <asp:TextBox ID="TxtOcTo" runat="server"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtTo_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="TxtOcTo">
                                </cc1:CalendarExtender>
                            </td>
                        </tr>
       
    </table>
                    
                </asp:Panel>
                 <asp:Panel ID="PnlFind" Style="z-index: 101; display:none; left: 100px; position: absolute; top: 60px"
                        runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                        Visible="True" BackColor="White">
                        <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                            <tr>
                                <td  bgcolor="#004080" style="width: 25%">
                                    <asp:RadioButton ID="RbnClient" runat="server" Text="Client" Checked="true" 
                                        GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnCountry" runat="server" Text="Country" Checked="false" 
                                        GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnTraffic" runat="server" Text="Traffic Op" 
                                        Checked="false" GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnCont" runat="server" Text="Container" Checked="false" 
                                        GroupName="Select" ForeColor="White" />
                                </td>
                                <td bgcolor="#004080" style="width:65%" valign="top">
                                    <asp:Label ID="LblSearch" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Enter the text here to search"></asp:Label> <br />
                                    <asp:TextBox ID="TxtSearch" runat="server" BackColor="White" Width="100%"></asp:TextBox>
                                    <br /> <br />
                                    <asp:CheckBox ID="ChkDate" runat="server" Text="Date Range From " 
                                        ForeColor="White"/>                                    
                                    <asp:TextBox ID="TxtFrom" runat="server" Width="100px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtFrom" Format="dd/MM/yyyy">
                                    </cc1:CalendarExtender>
                                    <asp:Label ID="LblToDate" runat="server" Text=" To " ForeColor="White"></asp:Label>
                                    <asp:TextBox ID="TxtTo" runat="server" Width="100px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TxtTo" Format="dd/MM/yyyy">
                                    </cc1:CalendarExtender>
                                </td>
                                <td align="right" bgcolor="#004080" style="width: 10%">
                                    <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                        Width="65px" />
                                    <br />
                                    <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                        Width="65px" />
                                </td>
                            </tr>
                            <tr>
                                
                                <td colspan="3">
                                    <asp:Panel ID="Panel8" runat="server" Height="300px" ScrollBars="Vertical" 
                                        Width="100%">
                                        <asp:GridView ID="GdvFind" runat="server" 
                                            EnableSortingAndPagingCallbacks="false" ShowFooter="true" Width="100%">
                                            <HeaderStyle BackColor="#0080C0" />
                                            <FooterStyle BackColor="#0080C0" />
                                            <AlternatingRowStyle BackColor="#CAE4FF" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
