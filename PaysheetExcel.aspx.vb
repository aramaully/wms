﻿Public Partial Class PaysheetExcel
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdClient As New OleDb.OleDbCommand
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String
    Dim CmdBoeOut As New OleDb.OleDbCommand
    Dim RdrBoeout As OleDb.OleDbDataReader
    Dim Lngdel As Long
    Dim CmdPhTally As New OleDb.OleDbCommand
    Dim RdrPhTally As OleDb.OleDbDataReader
    Dim DteDateIn As Date
    Dim CmdPicking As New OleDb.OleDbCommand
    Dim RdrPicking As OleDb.OleDbDataReader
    Dim DtePickDate As Date
    Dim LngBalance As Long
    Dim LngInQty As Long
    Dim StrBoeOut As String
    Dim StrPickDate As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            CmbMonth.SelectedIndex = Date.Today.Month - 1
            TxtYear.Text = Date.Today.Year
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            'Cmd.Connection = Con
            'Cmd.CommandText = "SELECT DeptName FROM Dept"
            'Rdr = Cmd.ExecuteReader
            'CmbDept.Items.Add("ALL")
            'While Rdr.Read
            '    CmbDept.Items.Add(Rdr("DeptName"))
            'End While
            'Rdr.Close()
        End If
    End Sub

    Protected Sub CmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbCategory.SelectedIndexChanged
        If CmbCategory.Text = "Monthly" Then
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            CmbMonth.Visible = True
            TxtDate.Visible = False
            LblDate.Text = "Month"
        ElseIf CmbCategory.Text = "Weekly" Then
            CmbMonth.Visible = False
            TxtDate.Text = Date.Today
            TxtDate.Visible = True
            LblDate.Text = "Week Ending"
        Else
            CmbMonth.Visible = False
            TxtDate.Text = Date.Today
            TxtDate.Visible = True
            LblDate.Text = "Fortnight Ending"
        End If
    End Sub

    Protected Sub CmbMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbMonth.SelectedIndexChanged, TxtDate.TextChanged
        If CmbCategory.Text = "Monthly" Then
            If CmbMonth.Text = "Bonus" Then
                TxtDate.Text = CDate("28-December" & "-" & TxtYear.Text)
            Else
                TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            End If

        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ColClientID As DataColumn
            Dim ColClientName As DataColumn
            Dim ColProdCode As DataColumn
            Dim ColItemNo As DataColumn
            Dim ColDescription As DataColumn
            Dim ColUnit As DataColumn
            Dim ColReceived As DataColumn
            Dim ColDateIn As DataColumn
            Dim ColBoeIn As DataColumn
            Dim ColDelivered As DataColumn
            Dim ColDelDate As DataColumn
            Dim ColBoeOut As DataColumn
            Dim ColBalance As DataColumn
            Dim BlnContinue As Boolean

            dt = New DataTable()

            ColClientID = New DataColumn("ClientID", Type.GetType("System.String"))
            ColClientName = New DataColumn("Name", Type.GetType("System.String"))
            ColProdCode = New DataColumn("PCode", Type.GetType("System.String"))
            ColItemNo = New DataColumn("ItemNo", Type.GetType("System.String"))
            ColDescription = New DataColumn("ProdDescription", Type.GetType("System.String"))
            ColUnit = New DataColumn("UnitCode", Type.GetType("System.String"))
            ColReceived = New DataColumn("Quantity", Type.GetType("System.String"))
            ColDateIn = New DataColumn("BillDate", Type.GetType("System.String"))
            ColBoeIn = New DataColumn("BoeNo", Type.GetType("System.String"))
            ColDelivered = New DataColumn("Delivered", Type.GetType("System.String"))
            ColDelDate = New DataColumn("DeliveredDate", Type.GetType("System.String"))
            ColBoeOut = New DataColumn("BoeOut", Type.GetType("System.String"))
            ColBalance = New DataColumn("Balance", Type.GetType("System.String"))

            dt.Columns.Add(ColClientID)
            dt.Columns.Add(ColClientName)
            dt.Columns.Add(ColProdCode)
            dt.Columns.Add(ColItemNo)
            dt.Columns.Add(ColDescription)
            dt.Columns.Add(ColUnit)
            dt.Columns.Add(ColReceived)
            dt.Columns.Add(ColDateIn)
            dt.Columns.Add(ColBoeIn)
            dt.Columns.Add(ColDelivered)
            dt.Columns.Add(ColDelDate)
            dt.Columns.Add(ColBoeOut)
            dt.Columns.Add(ColBalance)




            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "SELECT BOEINPROD.BOENO,BOEINPROD.ITEMNO, BoeInProd.UnitCode,BoeInProd.PCode,BoeInProd.ItemNo,0 As Balance, "
            StrSql = StrSql & "BOEINPROD.PCODE, BoeInProd.ProdDescription, BOEINPROD.QUANTITY, BoeIn.ClientID,Client.Category,Client.Name, "
            StrSql = StrSql & "BoeIn.BillDate, BoeIn.WhCode FROM BOEINPROD "
            StrSql = StrSql & "INNER JOIN BoeIn ON BoeinProd.BoeNo = BoeIn.BillNo "
            StrSql = StrSql & "Inner Join Client ON  BoeIn.ClientID = Client.ClientID"
            StrSql = StrSql & " And Client.Category = 'Suspended Duty' "
            StrSql = StrSql & " AND BoeIn.WhCode = 'FPZ6SD' "
            StrSql = StrSql & "WHERE (SELECT MAX(BILLDATE) FROM BOEOUTPROD, BOEOUT WHERE BOEINPROD.BOENO = BOEOUTPROD.BOEINNO AND BOEINPROD.PCODE = BOEOUTPROD.PCODE "
            StrSql = StrSql & "AND BOEOUTPROD.BOENO = BOEOUT.BILLNO) > '" & Format(CDate("01/08/2015"), "MM/dd/yyyy") & "' OR BOEINPROD.QUANTITY - ISNULL((SELECT SUM(QUANTITY) "
            StrSql = StrSql & "FROM  BOEOUTPROD WHERE BOEINPROD.BOENO = BOEOUTPROD.BOEINNO AND BOEINPROD.PCODE = BOEOUTPROD.PCODE AND "
            StrSql = StrSql & "BOEINPROD.ITEMNO = BOEOUTPROD.BOEINITEM),0) > 0 "
            StrSql = StrSql & "ORDER BY BoeIn.ClientID,BOEINPROD.BOENO"

            Cmd.Connection = Con
            CmdBoeOut.Connection = Con
            CmdPhTally.Connection = Con
            CmdPicking.Connection = Con

            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                BlnContinue = True
                LngBalance = 0
                LngInQty = 0
                StrBoeOut = ""
                LngInQty = Rdr("QUANTITY")
                LngBalance = Rdr("QUANTITY")
                CmdPhTally.CommandText = "SELECT DateIn FROM PhTally WHERE BoeNo = '" & Rdr("BoeNo") & "'"
                RdrPhTally = CmdPhTally.ExecuteReader
                If RdrPhTally.Read Then
                    DteDateIn = RdrPhTally("DateIn")
                Else
                    DteDateIn = Rdr("BillDate")
                End If
                RdrPhTally.Close()

                If DteDateIn > CDate("31/08/2015") Then
                    BlnContinue = False
                End If
                StrSql = "SELECT BoeoutProd.*, BoeOut.BillDate FROM BoeOutProd "
                StrSql = StrSql & "INNER JOIN BoeOut ON BoeoutProd.BoeNo = BoeOut.BillNo "
                StrSql = StrSql & "WHERE BoeInNo = '" & Rdr("BoeNo") & "' AND "
                StrSql = StrSql & " BoeoutProd.BoeInItem = '" & Rdr("ItemNo") & "' AND "
                StrSql = StrSql & " BoeoutProd.PCode = '" & Rdr("PCode") & "' "
                StrSql = StrSql & "ORDER BY BoeOut.BillDate"
                CmdBoeOut.CommandText = StrSql
                RdrBoeout = CmdBoeOut.ExecuteReader
                StrPickDate = ""
                Lngdel = 0
                While RdrBoeout.Read
                    Lngdel = 0
                    StrBoeOut = RdrBoeout("BoeNo")
                    CmdPicking.CommandText = "SELECT PickDate FROM PickList WHERE BoeNo = '" & Rdr("BoeNo") & "'"
                    RdrPicking = CmdPicking.ExecuteReader
                    If RdrPicking.Read Then
                        DtePickDate = RdrPicking("PickDate")
                        StrPickDate = Format(DtePickDate, "dd-MMM-yyyy")
                    Else
                        DtePickDate = RdrBoeout("BillDate")
                        StrPickDate = Format(DtePickDate, "dd-MMM-yyyy")
                    End If
                    RdrPicking.Close()
                    If DtePickDate <= CDate("31/08/2015") Then
                        Lngdel = Lngdel + RdrBoeout("Quantity")
                        LngBalance = Math.Round((LngInQty - Lngdel), 2, MidpointRounding.AwayFromZero)
                    Else
                        StrPickDate = ""
                        StrBoeOut = ""
                    End If

                End While
                RdrBoeout.Close()
                If BlnContinue Then
                    dr = dt.NewRow
                    dr.Item("ClientID") = Rdr("ClientID")
                    dr.Item("Name") = Rdr("Name")
                    dr.Item("PCode") = Rdr("PCode")
                    dr.Item("ItemNo") = Rdr("ItemNo")
                    dr.Item("ProdDescription") = Rdr("ProdDescription")
                    dr.Item("UnitCode") = Rdr("UnitCode")
                    dr.Item("Quantity") = Rdr("Quantity")
                    dr.Item("BillDate") = Format(DteDateIn, "dd-MMM-yyyy")
                    dr.Item("BoeNo") = Rdr("BoeNo")
                    dr.Item("Delivered") = Lngdel
                    dr.Item("DeliveredDate") = StrPickDate
                    dr.Item("BoeOut") = StrBoeOut
                    dr.Item("Balance") = LngBalance
                    dt.Rows.Add(dr)
                End If
            End While
            Rdr.Close()

            Dim ExcelGrid As New GridView()
            Dim DtProfileUsers As New DataTable("Table")
            ExcelGrid.DataSource = dt
            ' it's my DataTable Name  
            ExcelGrid.DataBind()
            If ExcelGrid.Rows.Count = 0 Then
                LblResult.Text = "No Records"
                Exit Sub
            End If
            Dim ExcelTable As New Table()
            ExcelTable.GridLines = GridLines.Both
            ExcelTable.Rows.Add(ExcelGrid.HeaderRow)
            For Each GdvRow As GridViewRow In ExcelGrid.Rows
                ExcelTable.Rows.Add(GdvRow)
            Next
            Dim wrt As New IO.StringWriter()
            Dim htw As New HtmlTextWriter(wrt)
            ExcelTable.RenderControl(htw)
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=CustomRep-" & DateTime.Today & ".xls")
            Response.ContentType = "application/ms-excel"
            Response.Write(wrt.ToString())
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End If
    End Sub
End Class