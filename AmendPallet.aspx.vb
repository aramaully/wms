﻿Public Partial Class AmendPallet
    Inherits System.Web.UI.Page

    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdScalar As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim BlnNQtyChange As Boolean = False
    Dim BlnNDelChange As Boolean = False

    Const MENUID = "AmendPallet"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If
            ButSave.Enabled = False
        End If
    End Sub

    Private Sub ClearAll()
        TxtPallet.Text = ""
        TxtDescription.Text = ""
        TxtOQty.Text = ""
        TxtNQty.Text = ""
        TxtUnit.Text = ""
        TxtDUnit.Text = ""
        TxtODel.Text = ""
        TxtNDel.Text = ""
        BlnNQtyChange = False
        BlnNDelChange = False
        ButSave.Enabled = False
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtBoeNo.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtBoeNo_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvCont.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCont.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        If RbnClient.Text = "Name" Then
            StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
            If RbnClient.Checked Then
                StrSql = StrSql & "Name"
            ElseIf RbnCountry.Checked Then
                StrSql = StrSql & "City"
            Else
                StrSql = StrSql & "Contact"
            End If
            StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Else
            StrSql = "SELECT BoeIn.BillNo, CONVERT(Char,BoeIn.BillDate,103) 'Bill Date', BoeIn.Revision, Client.Name FROM BoeIn, Client WHERE "
            StrSql = StrSql & "BoeIn.ClientID = Client.ClientID "
            If RbnClient.Checked Then
                StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCountry.Checked Then
                StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnTraffic.Checked Then
                StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCont.Checked Then
                StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
            End If
            If ChkDate.Checked Then
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                StrSql = StrSql & "AND Boein.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
                StrSql = StrSql & "AND Boein.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
            End If
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub TxtBoeNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBoeNo.TextChanged
        ClearAll()
        Cmd.Connection = Con
        CmdScalar.Connection = Con
        Cmd.CommandText = "SELECT * FROM BoeIn WHERE BillNo = '" & TxtBoeNo.Text & "' ORDER BY BillNo"
        Rdr = Cmd.ExecuteReader
        If Not Rdr.Read Then
            ErrHead.Text = "ERROR"
            ErrMsg.Text = "Invalid Inbound Bill"
            ModalPopupExtender1.Show()
            TxtBoeNo.Focus()
            Rdr.Close()
            Exit Sub
        Else
            If Rdr("WhCode") <> Session("PstrZone") And Session("PstrZone") <> "ALL" Then
                ErrHead.Text = "Access Error"
                ErrMsg.Text = "Sorry! You do not have permission for the records of Zone " & Rdr("WhCode")
                Exit Sub
            End If
            TxtDate.Text = Rdr("BillDate")
            CmdScalar.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & Rdr("ClientID") & "'"
            TxtName.Text = CmdScalar.ExecuteScalar.ToString
        End If
        Rdr.Close()
        UpDateGrid()
    End Sub

    Private Sub UpDateGrid()
        StrSql = "SELECT PaltDetail.PalletID, PaltDetail.Quantity, PaltDetail.Unit, "
        StrSql = StrSql & "PaltDetail.DelQuantity, Product.Description FROM PaltDetail, Product "
        StrSql = StrSql & "WHERE Product.ProdCode = PaltDetail.Pcode AND "
        StrSql = StrSql & "BoeNo = '" & TxtBoeNo.Text & "' ORDER BY PalletID"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Dim da As New OleDb.OleDbDataAdapter(Cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvCont.DataSource = ds
        GdvCont.DataBind()
        'VsfPallets.AutoSize(0, 3)
        'If VsfPallets.Rows > 1 Then VsfPallets_SelChange()
        'RsPallet.Close()
        'RsPltDetail.Close()
    End Sub

    Protected Sub ButFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFind.Click
        RbnClient.Text = "Client"
        RbnCountry.Text = "Country"
        RbnTraffic.Text = "Traffic Op"
        RbnCont.Visible = True
        ChkDate.Visible = True
        TxtFrom.Visible = True
        TxtTo.Visible = True
        LblToDate.Visible = True
        GdvFind.DataSource = Nothing
        TxtTo.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtFrom.Text = Date.Today.AddDays(-10).ToString("dd/MM/yyyy")
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Protected Sub NutNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles NutNext.Click
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT TOP 1 BillNo FROM BoeIn WHERE BillNo > '" & TxtBoeNo.Text & "' ORDER BY BillNo"
        Try
            TxtBoeNo.Text = Cmd.ExecuteScalar.ToString
            TxtBoeNo_TextChanged(Me, e)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ButPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPrev.Click
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT TOP 1 BillNo FROM BoeIn WHERE BillNo > '" & TxtBoeNo.Text & "' ORDER BY BillNo DESC"
        Try
            TxtBoeNo.Text = Cmd.ExecuteScalar.ToString
            TxtBoeNo_TextChanged(Me, e)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            StrSql = "UPDATE PaltDetail SET Quantity=" & Val(TxtNQty.Text) & ", DelQuantity=" & Val(TxtNDel.Text)
            StrSql = StrSql & " WHERE PalletID = '" & TxtPallet.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ClearAll()
            UpDateGrid()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Save Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub GdvCont_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvCont.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvCont, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvCont_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvCont.SelectedIndexChanged
        If BlnNQtyChange Or BlnNDelChange Then
            ButSave_Click(Me, e)
        End If
        TxtPallet.Text = GdvCont.SelectedRow.Cells(0).Text
        TxtDescription.Text = GdvCont.SelectedRow.Cells(1).Text
        TxtOQty.Text = GdvCont.SelectedRow.Cells(2).Text
        TxtODel.Text = GdvCont.SelectedRow.Cells(3).Text
        TxtNQty.Text = TxtOQty.Text
        TxtNDel.Text = TxtODel.Text
        TxtUnit.Text = GdvCont.SelectedRow.Cells(4).Text
        ButSave.Enabled = False
        BlnNQtyChange = False
        BlnNDelChange = False
        TxtNQty.Focus
    End Sub

    Private Sub TxtNQty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtNQty.TextChanged
        BlnNQtyChange = True
        ButSave.Enabled = True
    End Sub

    Private Sub TxtNDel_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtNDel.TextChanged
        BlnNDelChange = True
        ButSave.Enabled = True
    End Sub

    Private Sub GdvCont_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvCont.SelectedIndexChanging
        GdvCont.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub TxtUnit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtUnit.TextChanged
        TxtDUnit.Text = TxtUnit.Text
        ButSave.Enabled = True
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub
End Class