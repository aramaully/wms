﻿Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Xml

Partial Public Class MainMenu
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.CacheControl = "no-cache"
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Response.AddHeader("X-UA-Compatible", "IE=EmulateIE7")
        If Not Session("LoggedIn") Then Response.Redirect("~/Default.aspx")
        Dim StrSql As String = ""
        Dim Cmd As New OleDb.OleDbCommand
        Dim Rdr As OleDb.OleDbDataReader
        Dim conn As New OleDbConnection(Session("ConnString"))
        conn.Open()
        If Not Page.IsPostBack Then
            HeadText.Style("text-align") = "center"
            For Each Ctl As Control In Me.AccordionPane1.Controls.Item(1).Controls
                If Ctl.GetType.Name = "LinkButton" Then
                    Ctl.Visible = False
                End If
            Next
            For Each Ctl As Control In Me.AccordionPane2.Controls.Item(1).Controls
                If Ctl.GetType.Name = "LinkButton" Then
                    Ctl.Visible = False
                End If
            Next
            For Each Ctl As Control In Me.AccordionPane3.Controls.Item(1).Controls
                If Ctl.GetType.Name = "LinkButton" Then
                    Ctl.Visible = False
                End If
            Next
            For Each Ctl As Control In Me.AccordionPane4.Controls.Item(1).Controls
                If Ctl.GetType.Name = "LinkButton" Then
                    Ctl.Visible = False
                End If
            Next
            For Each Ctl As Control In Me.AccordionPane5.Controls.Item(1).Controls
                If Ctl.GetType.Name = "LinkButton" Then
                    Ctl.Visible = False
                End If
            Next
            For Each Ctl As Control In Me.AccordionPane6.Controls.Item(1).Controls
                If Ctl.GetType.Name = "LinkButton" Then
                    Ctl.Visible = False
                End If
            Next
            For Each Ctl As Control In Me.AccordionPane7.Controls.Item(1).Controls
                If Ctl.GetType.Name = "LinkButton" Then
                    Ctl.Visible = False
                End If
            Next
            For Each Ctl As Control In Me.AccordionPane8.Controls.Item(1).Controls
                If Ctl.GetType.Name = "LinkButton" Then
                    Ctl.Visible = False
                End If
            Next
            For Each Ctl As Control In Me.AccordionPane9.Controls.Item(1).Controls
                If Ctl.GetType.Name = "LinkButton" Then
                    Ctl.Visible = False
                End If
            Next
            For Each Ctl As Control In Me.AccordionPane10.Controls.Item(1).Controls
                If Ctl.GetType.Name = "LinkButton" Then
                    Ctl.Visible = False
                End If
            Next
            TlsClient.Visible = False
            TlsInBound.Visible = False
            TlsOutBound.Visible = False
            TlsPicking.Visible = False
            TlsPutaway.Visible = False
            TlsPhTally.Visible = False
            StrSql = "SELECT * FROM Access WHERE UserID = '" & Session("UserID") & "'"
            Cmd.Connection = conn
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                Select Case Rdr("Text").ToString.Trim
                    Case "Activities"
                        ButActivity.Visible = True
                    Case "Clients"
                        ButClient.Visible = True
                        TlsClient.Visible = True
                    Case "Tariff Card"
                        ButTariff.Visible = True
                    Case "Units"
                        ButUnits.Visible = True
                    Case "Warehouses"
                        ButWarehouse.Visible = True
                    Case "Locations"
                        If Rdr("ParentID") = "MnuReports" Then
                            ButLocList.Visible = True
                        Else
                            ButLocation.Visible = True
                        End If
                    Case "Racks"
                        If Rdr("ParentID") = "MnuReports" Then
                            ButRacksList.Visible = True
                        Else
                            ButRacks.Visible = True
                        End If
                    Case "Zones"
                        If Rdr("ParentID") = "MnuReports" Then
                            ButZonesList.Visible = True
                        Else
                            ButZones.Visible = True
                        End If
                    Case "Charges"
                        ButCharges.Visible = True
                    Case "Traffic Officer"
                        ButTrafficOff.Visible = True
                    Case "Bill Inbound"
                        ButInBound.Visible = True
                        TlsInBound.Visible = True
                    Case "Bill Outbound"
                        ButOutbound.Visible = True
                        TlsOutBound.Visible = True
                    Case "ReWarehousing"
                        ButReWarehouse.Visible = True
                    Case "Revise Bills"
                        ButRevBill.Visible = True
                    Case "Physical Tally Sheet"
                        If Rdr("ParentID") = "MnuReports" Then
                            ButPrintPhTally.Visible = True
                        Else
                            ButPhTally.Visible = True
                            TlsPhTally.Visible = True
                        End If
                    Case "Cross Docking"
                        ButCrossDock.Visible = True
                    Case "Store Containers"
                        ButStoreCont.Visible = True
                    Case "Release Containers"
                        ButReleaseCont.Visible = True
                    Case "Automatic Putaway"
                        ButAPutaway.Visible = True
                    Case "Manual Putaway"
                        ButMPutaway.Visible = True
                        TlsPutaway.Visible = True
                    Case "Automatic Picking"
                        ButAPicking.Visible = True
                    Case "Manual Picking"
                        ButMPicking.Visible = True
                        TlsPicking.Visible = True
                    Case "Amend Pallet Details"
                        ButAmendPlt.Visible = True
                    Case "Change Pallet Ratio"
                        ButChangePltRatio.Visible = True
                    Case "Compute Occupancy"
                        ButComputeOcc.Visible = True
                    Case "Create Invoice From Occupancy"
                        ButInvOcc.Visible = True
                    Case "Edit Billing Invoice"
                        ButEditBill.Visible = True
                    Case "Export to Sun"
                        ButExportSun.Visible = True
                    Case "Charges for Logistic Services"
                        ButChLogistic.Visible = True
                    Case "Payment of Invoices"
                        ButInvPayment.Visible = True
                    Case "Set Unset Hold on Locations"
                        ButSetHold.Visible = True
                    Case "Putaway / Picking Sequences"
                        ButPutSequence.Visible = True
                    Case "Clear Locations"
                        ButClearLoc.Visible = True
                    Case "Abandon Goods"
                        ButAbandon.Visible = True
                    Case "Move In Sheet"
                        ButMoveIn.Visible = True
                    Case "Move Out Sheet"
                        ButMoveOut.Visible = True
                    Case "Proposal Builder"
                        ButPropBuilder.Visible = True
                    Case "Bill Status"
                        ButBillStat.Visible = True
                    Case "Custom Report Generator"
                        ButCustomRep.Visible = True
                    Case "Discrepancy Physical Tally Sheet"
                        ButDiscPhTally.Visible = True
                    Case "Expired Inbound"
                        ButExpInbound.Visible = True
                    Case "Occupancy"
                        ButOccupancy.Visible = True
                    Case "Pallet Tags"
                        ButPalletTags.Visible = True
                    Case "Pickick List Summary"
                        ButPickListSumm.Visible = True
                    Case "Picking List"
                        ButPickingList.Visible = True
                    Case "Print Invoice"
                        ButPrintInvoice.Visible = True
                    Case "Putaway List"
                        ButPutawayList.Visible = True
                    Case "Stock by Client"
                        ButStkClient.Visible = True
                    Case "Stock by Inbound"
                        ButStkInbound.Visible = True
                    Case "Stock by Location"
                        ButStkLoc.Visible = True
                    Case "Stock Return"
                        ButStkReturn.Visible = True
                    Case "Stock Status"
                        ButStkStatus.Visible = True
                    Case "Assign Access"
                        ButAccess.Visible = True
                    Case "Audit Trial"
                        ButAuditTrail.Visible = True
                    Case "Change Password"
                        ButPassword.Visible = True
                    Case "Create / Edit User Account"
                        ButAddUser.Visible = True
                    Case "System Setup"
                        ButSystem.Visible = True
                End Select
            End While
            Rdr.Close()
            ButExit.Visible = True
        End If
    End Sub

    Private Sub ButClient_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButClient.Click
        MainFrame.Attributes("src") = "Client.aspx"
    End Sub

    Private Sub ButExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButExit.Click
        Session("LoggedIn") = False
        Response.Redirect("Default.aspx")
    End Sub

    Private Sub ButActivity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButActivity.Click
        MainFrame.Attributes("src") = "Activity.aspx"
    End Sub

    Private Sub ButUnits_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButUnits.Click
        MainFrame.Attributes("src") = "Unit.aspx"
    End Sub

    Private Sub ButWarehouse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButWarehouse.Click
        MainFrame.Attributes("src") = "WHouse.aspx"
    End Sub

    Private Sub ButLocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButLocation.Click
        MainFrame.Attributes("src") = "Location.aspx"
    End Sub

    Private Sub ButRacks_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRacks.Click
        MainFrame.Attributes("src") = "Racks.aspx"
    End Sub

    Private Sub ButZones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButZones.Click
        MainFrame.Attributes("src") = "Zones.aspx"
    End Sub

    Private Sub ButCharges_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCharges.Click
        MainFrame.Attributes("src") = "Charges.aspx"
    End Sub

    Private Sub ButTrafficOff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButTrafficOff.Click
        MainFrame.Attributes("src") = "Traffic.aspx"
    End Sub

    Private Sub ButInBound_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButInBound.Click
        MainFrame.Attributes("src") = "Billin.aspx"
    End Sub

    Private Sub ButOutbound_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButOutbound.Click
        MainFrame.Attributes("src") = "BillOut.aspx"
    End Sub

    Private Sub ButReWarehouse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButReWarehouse.Click
        MainFrame.Attributes("src") = "ReWareHouse.aspx"
    End Sub

    Private Sub ButRevBill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRevBill.Click
        MainFrame.Attributes("src") = "ReviseBoe.aspx"
    End Sub

    Private Sub ButPhTally_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPhTally.Click
        MainFrame.Attributes("src") = "PhTally.aspx"
    End Sub

    Private Sub ButCrossDock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCrossDock.Click
        'MainFrame.Attributes("src") = "CrossDock.aspx"
    End Sub

    Private Sub ButStoreCont_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButStoreCont.Click
        MainFrame.Attributes("src") = "StoreContainers.aspx"
    End Sub

    Private Sub ButAPutaway_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAPutaway.Click
        MainFrame.Attributes("src") = "APutAway.aspx"
    End Sub

    Private Sub ButAPicking_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAPicking.Click
        MainFrame.Attributes("src") = "APicking.aspx"
    End Sub

    Private Sub ButMPutaway_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButMPutaway.Click
        MainFrame.Attributes("src") = "MPutAway.aspx"
    End Sub

    Private Sub ButMPicking_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButMPicking.Click
        MainFrame.Attributes("src") = "MPicking.aspx"
    End Sub

    Private Sub ButAmendPlt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAmendPlt.Click
        MainFrame.Attributes("src") = "AmendPallet.aspx"
    End Sub

    Private Sub ButChangePltRatio_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButChangePltRatio.Click
        MainFrame.Attributes("src") = "ChPalletRatio.aspx"
    End Sub

    Private Sub ButComputeOcc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButComputeOcc.Click
        MainFrame.Attributes("src") = "Occupancy.aspx"
    End Sub

    Private Sub ButInvOcc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButInvOcc.Click
        MainFrame.Attributes("src") = "InvOcc.aspx"
    End Sub

    Private Sub ButEditBill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButEditBill.Click
        MainFrame.Attributes("src") = "EditInv.aspx"
    End Sub

    Private Sub ButChLogistic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButChLogistic.Click
        MainFrame.Attributes("src") = "LogCharge.aspx"
    End Sub

    Private Sub ButInvPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButInvPayment.Click
        MainFrame.Attributes("src") = "PayInv.aspx"
    End Sub

    Private Sub ButSetHold_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSetHold.Click
        MainFrame.Attributes("src") = "SetHold.aspx"
    End Sub

    Private Sub ButPutSequence_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPutSequence.Click
        MainFrame.Attributes("src") = "PutPickSeq.aspx"
    End Sub

    Private Sub ButClearLoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButClearLoc.Click
        MainFrame.Attributes("src") = "ClearLoc.aspx"
    End Sub

    Private Sub ButAbandon_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAbandon.Click
        MainFrame.Attributes("src") = "Abandon.aspx"
    End Sub

    Private Sub ButMoveIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButMoveIn.Click
        MainFrame.Attributes("src") = "MoveIn.aspx"
    End Sub

    Private Sub ButMoveOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButMoveOut.Click
        MainFrame.Attributes("src") = "MoveOut.aspx"
    End Sub

    Private Sub ButReleaseCont_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButReleaseCont.Click
        MainFrame.Attributes("src") = "ReleaseContainers.aspx"
    End Sub

    Private Sub ButBillStat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButBillStat.Click
        MainFrame.Attributes("src") = "BillStatus.aspx"
    End Sub

    Private Sub ButExpInbound_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButExpInbound.Click
        MainFrame.Attributes("src") = "RptExpiredInbound.aspx"
    End Sub

    Private Sub ButLocList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButLocList.Click
        MainFrame.Attributes("src") = "ListLocations.aspx"
    End Sub

    Private Sub ButOccupancy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButOccupancy.Click
        MainFrame.Attributes("src") = "RepOccupancy.aspx"
    End Sub

    Private Sub ButPalletTags_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPalletTags.Click
        MainFrame.Attributes("src") = "PalletTag.aspx"
    End Sub

    Private Sub ButPickListSumm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPickListSumm.Click
        MainFrame.Attributes("src") = "PrintPickSummary.aspx"
    End Sub

    Private Sub ButPickingList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPickingList.Click
        MainFrame.Attributes("src") = "PrintPickList.aspx"
    End Sub

    Private Sub ButPrintInvoice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPrintInvoice.Click
        MainFrame.Attributes("src") = "PrintInvoice.aspx"
    End Sub

    Private Sub ButPutawayList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPutawayList.Click
        MainFrame.Attributes("src") = "PutawayList.aspx"
    End Sub

    Private Sub ButRacksList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRacksList.Click
        MainFrame.Attributes("src") = "ListRacks.aspx"
    End Sub

    Private Sub ButStkClient_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButStkClient.Click
        MainFrame.Attributes("src") = "RepStkClient.aspx"
    End Sub

    Private Sub ButStkInbound_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButStkInbound.Click
        MainFrame.Attributes("src") = "RepStkInbound.aspx"
    End Sub

    Private Sub ButStkLoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButStkLoc.Click
        MainFrame.Attributes("src") = "RepStkLoc.aspx"
    End Sub

    Private Sub ButStkReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButStkReturn.Click
        MainFrame.Attributes("src") = "MonReturn.aspx"
    End Sub

    Private Sub ButStkStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButStkStatus.Click
        MainFrame.Attributes("src") = "StockStatus.aspx"
    End Sub

    Private Sub ButZonesList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButZonesList.Click
        MainFrame.Attributes("src") = "ListZones.aspx"
    End Sub

    Private Sub ButAccess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAccess.Click
        MainFrame.Attributes("src") = "UserAccess.aspx"
    End Sub

    Private Sub ButAuditTrail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAuditTrail.Click
        'MainFrame.Attributes("src") = "UserAccess.aspx"
    End Sub

    Private Sub ButPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPassword.Click
        MainFrame.Attributes("src") = "ChangePassword.aspx"
    End Sub

    Private Sub ButAddUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAddUser.Click
        MainFrame.Attributes("src") = "Users.aspx"
    End Sub

    Private Sub ButSystem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSystem.Click
        MainFrame.Attributes("src") = "SystemSetup.aspx"
    End Sub

    Protected Sub TlsClient_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles TlsClient.Click
        MainFrame.Attributes("src") = "Clients.aspx"
    End Sub

    Protected Sub TlsInBound_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles TlsInBound.Click
        MainFrame.Attributes("src") = "BillIn.aspx"
    End Sub

    Protected Sub TlsOutBound_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles TlsOutBound.Click
        MainFrame.Attributes("src") = "BillOut.aspx"
    End Sub

    Private Sub TlsPicking_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles TlsPicking.Click
        MainFrame.Attributes("src") = "MPicking.aspx"
    End Sub

    Private Sub TlsPutaway_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles TlsPutaway.Click
        MainFrame.Attributes("src") = "MPutaway.aspx"
    End Sub

    Private Sub TlsPhTally_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles TlsPhTally.Click
        MainFrame.Attributes("src") = "PhTally.aspx"
    End Sub

    Private Sub ButTariff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButTariff.Click
        MainFrame.Attributes("src") = "Tariff.aspx"
    End Sub

    Private Sub ButPropBuilder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPropBuilder.Click
        MainFrame.Attributes("src") = "Proposal.aspx"
    End Sub
End Class