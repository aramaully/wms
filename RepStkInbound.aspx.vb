﻿Public Partial Class RepStkInbound
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdClient As New OleDb.OleDbCommand
    Dim RdrClient As OleDb.OleDbDataReader
    Dim CmdBoe As New OleDb.OleDbCommand
    Dim RdrBoe As OleDb.OleDbDataReader
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrSel As String

    Const MENUID = "StockByInbound"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Cmd.Connection = Con
        'Check Access
        StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar = 0 Then
            Response.Redirect("about:blank")
        End If
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub TxtBoeNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBoeNo.TextChanged
        StrSql = "SELECT * FROM Pallets WHERE BoeNo='" & TxtBoeNo.Text & "'"
        CmdBoe.Connection = Con
        CmdBoe.CommandText = StrSql
        RdrBoe = CmdBoe.ExecuteReader
        If RdrBoe.Read Then
            TxtDate.Text = Format(RdrBoe("DateIn"), "dd/MM/yyyy")
            StrSql = "SELECT * FROM Client WHERE ClientID='" & RdrBoe("ClientID") & "'"
            CmdClient.Connection = Con
            CmdClient.CommandText = StrSql
            RdrClient = CmdClient.ExecuteReader
            If RdrClient.Read Then
                TxtClient.Text = RdrBoe("ClientID")
                TxtName.Text = RdrClient("Name")
            End If
            RdrClient.Close()
        End If
        RdrBoe.Close()
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        Dim StrSelFormula As String
      
        StrSelFormula = "{PaltDetail.BalQuantity} > 0 "
        If TxtBoeNo.Text <> "ALL" Then
            StrSelFormula = StrSelFormula & " And {Pallets.BoeNo} = '" & TxtBoeNo.Text & "'"
        Else
            If TxtClient.Text <> "ALL" Then
                StrSelFormula = StrSelFormula & " And {Pallets.ClientID} = '" & TxtClient.Text & "'"
            End If
        End If
        If Session("PstrZone").ToString <> "ALL" Then
            If Len(StrSelFormula) > 0 Then
                StrSelFormula = StrSelFormula & " And "
            End If
            StrSelFormula = StrSelFormula & "{Boein.WhCode} = '" & Session("PstrZone").ToString & "'"
        End If
        Session("ReportFile") = "RptStockInbound.rpt"
        Session("Period") = ""
        Session("Zone") = ""
        Session("SelFormula") = StrSelFormula
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        'Response.Redirect("Report.aspx?RptID=Stock By Inbound&RPTFileName=RepStkInbound.aspx&RptFile=RptStockInbound.rpt&StrSelF=" & StrSelFormula)

    End Sub

    Private Sub TxtClient_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClient.TextChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & TxtClient.Text & "'"
        Try
            TxtName.Text = Cmd.ExecuteScalar.ToString
        Catch ex As Exception
            TxtName.Text = "Invalid Client"
        End Try
    End Sub

    Private Sub BtnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnFind.Click
        Session.Add("PstrCode", TxtBoeNo.Text)
        ScriptManager.RegisterStartupScript(BtnFind, Me.GetType, "ShowFindClientBill", "<script language='javascript'> ShowFindClientBill();</script>", False)
    End Sub
End Class