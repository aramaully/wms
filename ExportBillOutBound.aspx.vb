﻿Imports System.IO
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Configuration
Partial Public Class ExportBillOutBound
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdBoeOutProd As New OleDb.OleDbCommand
    Dim RdrBoeOutProd As OleDb.OleDbDataReader
    Dim CmdBoeOutItem As New OleDb.OleDbCommand
    Dim RdrBoeOutItem As OleDb.OleDbDataReader
    Dim CmdBoe As New OleDb.OleDbCommand
    Dim RdrBoe As OleDb.OleDbDataReader
    Dim CmdClient As New OleDb.OleDbCommand
    Dim RdrClient As OleDb.OleDbDataReader

    Const MENUID = "ExportBillOutbound"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Export" Then
            Dim dt As DataTable
            Dim dr As DataRow

            Dim Col1 As DataColumn
            Dim Col2 As DataColumn
            Dim Col3 As DataColumn
            Dim Col4 As DataColumn
            Dim Col5 As DataColumn
            Dim Col6 As DataColumn
            Dim Col7 As DataColumn
            Dim Col8 As DataColumn
            Dim Col9 As DataColumn
            Dim Col10 As DataColumn
            Dim Col11 As DataColumn
            Dim Col12 As DataColumn
            Dim Col13 As DataColumn
            Dim Col14 As DataColumn
            Dim Col15 As DataColumn
            Dim Col16 As DataColumn

            dt = New DataTable()
            Col1 = New DataColumn("Col1", Type.GetType("System.String"))
            Col2 = New DataColumn("Col2", Type.GetType("System.String"))
            Col3 = New DataColumn("Col3", Type.GetType("System.String"))
            Col4 = New DataColumn("Col4", Type.GetType("System.String"))
            Col5 = New DataColumn("Col5", Type.GetType("System.String"))
            Col6 = New DataColumn("Col6", Type.GetType("System.String"))
            Col7 = New DataColumn("Col7", Type.GetType("System.String"))
            Col8 = New DataColumn("Col8", Type.GetType("System.String"))
            Col9 = New DataColumn("Col9", Type.GetType("System.String"))
            Col10 = New DataColumn("Col10", Type.GetType("System.String"))
            Col11 = New DataColumn("Col11", Type.GetType("System.String"))
            Col12 = New DataColumn("Col12", Type.GetType("System.String"))
            Col13 = New DataColumn("Col13", Type.GetType("System.String"))
            Col14 = New DataColumn("Col14", Type.GetType("System.String"))
            Col15 = New DataColumn("Col15", Type.GetType("System.String"))
            Col16 = New DataColumn("Col16", Type.GetType("System.String"))

            dt.Columns.Add(Col1)
            dt.Columns.Add(Col2)
            dt.Columns.Add(Col3)
            dt.Columns.Add(Col4)
            dt.Columns.Add(Col5)
            dt.Columns.Add(Col6)
            dt.Columns.Add(Col7)
            dt.Columns.Add(Col8)
            dt.Columns.Add(Col9)
            dt.Columns.Add(Col10)
            dt.Columns.Add(Col11)
            dt.Columns.Add(Col12)
            dt.Columns.Add(Col13)
            dt.Columns.Add(Col14)
            dt.Columns.Add(Col15)
            dt.Columns.Add(Col16)

            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")


            Cmd.Connection = Con
            CmdBoeOutProd.Connection = Con
            CmdBoeOutItem.Connection = Con

            StrSql = "Select *From BoeOut"
            StrSql = StrSql & " Inner Join Client ON  BoeOut.ClientID = Client.ClientID"
            StrSql = StrSql & " And Client.Category = 'Suspended Duty'"
            StrSql = StrSql & " Where BillNo =  '" & TxtBoeNo.Text & "'"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            While Rdr.Read

                dr = dt.NewRow
                dr.Item(Col1) = "HDR"
                dr.Item(Col2) = Rdr("WhCode")
                dr.Item(Col3) = Rdr("BillNo")
                dt.Rows.Add(dr)

                StrSql = "Select *From BoeOutItem Where BoeNo = '" & Rdr("BillNo") & "'"
                CmdBoeOutItem.CommandText = StrSql
                RdrBoeOutItem = CmdBoeOutItem.ExecuteReader
                While RdrBoeOutItem.Read
                    dr = dt.NewRow
                    dr.Item("Col1") = "ITM"
                    dr.Item("Col2") = RdrBoeOutItem("ItemNo")
                    dr.Item("Col3") = RdrBoeOutItem("HsCode")
                    StrSql = "Select *From BoeOutProd Where BoeNo = '" & Rdr("BillNo") & "' And ItemNo = '" & RdrBoeOutItem("ItemNo") & "'"
                    CmdBoeOutProd.CommandText = StrSql
                    RdrBoeOutProd = CmdBoeOutProd.ExecuteReader
                    While RdrBoeOutProd.Read
                        dr.Item("Col4") = RdrBoeOutProd("PCode")
                        dr.Item("Col14") = RdrBoeOutProd("ProdDescription")
                        dr.Item("Col15") = RdrBoeOutProd("BoeInNo")
                        dr.Item("Col16") = RdrBoeOutProd("BoeInItem")
                        dr.Item("Col9") = RdrBoeOutProd("Quantity")
                        dr.Item("Col10") = RdrBoeOutProd("Quantity")
                    End While
                    RdrBoeOutProd.Close()
                    dr.Item("Col5") = RdrBoeOutItem("Value")
                    dr.Item("Col6") = "MUR"
                    dr.Item("Col7") = RdrBoeOutItem("Weight")
                    dr.Item("Col8") = RdrBoeOutItem("Weight")
                    dr.Item("Col11") = ""
                    dr.Item("Col12") = ""
                    dr.Item("Col13") = Rdr("WhCode")
                    dt.Rows.Add(dr)
                End While
                RdrBoeOutItem.Close()



                Dim filename As String = Server.MapPath("~/download.csv")
                Dim sw As New StreamWriter(filename, False)

                Dim iColCount As Integer = dt.Columns.Count
                'For i As Integer = 0 To iColCount - 1
                '    sw.Write(dt.Columns(i))
                '    If i < iColCount - 1 Then
                '        sw.Write(",")
                '    End If
                'Next
                'sw.Write(sw.NewLine)

                For Each row As DataRow In dt.Rows
                    If row.Item("Col1") <> "Col1" Then
                        For i As Integer = 0 To iColCount - 1
                            If Not Convert.IsDBNull(row(i)) Then
                                sw.Write(row(i).ToString())
                            End If
                            If i < iColCount - 1 Then
                                sw.Write(",")
                            End If
                        Next
                        sw.Write(sw.NewLine)
                    End If
                Next
                sw.Close()

                Response.Clear()
                Response.ContentType = "application/csv"
                Response.AddHeader("Content-Disposition", "attachment; filename=" & Rdr("BillNo") & ".csv")
                Response.WriteFile(filename)
                Response.Flush()
                Response.End()
            End While
            Rdr.Close()
        End If
    End Sub

    Private Sub TxtBoeNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBoeNo.TextChanged
        StrSql = "SELECT * FROM BoeOut WHERE BillNo='" & TxtBoeNo.Text & "'"
        CmdBoe.Connection = Con
        CmdBoe.CommandText = StrSql
        RdrBoe = CmdBoe.ExecuteReader
        If RdrBoe.Read Then
            TxtClientID.Text = RdrBoe("ClientID")
            TxtDate.Text = Format(RdrBoe("BillDate"), "dd/MM/yyyy")
            StrSql = "SELECT * FROM Client WHERE ClientID='" & RdrBoe("ClientID") & "'"
            CmdClient.Connection = Con
            CmdClient.CommandText = StrSql
            RdrClient = CmdClient.ExecuteReader
            If RdrClient.Read Then
                TxtName.Text = RdrClient("Name")
            End If
            RdrClient.Close()
        End If
        RdrBoe.Close()
    End Sub
End Class