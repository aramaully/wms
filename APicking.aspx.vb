﻿Public Partial Class APicking
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdPicking As New OleDb.OleDbCommand
    Dim RdrPicking As OleDb.OleDbDataReader
    Dim CmdPallets As New OleDb.OleDbCommand
    Dim RdrPallets As OleDb.OleDbDataReader
    Dim CmdUpdatePallets As New OleDb.OleDbCommand
    Dim StrSel As String
    Dim StrSql As String

    Const MENUID = "AutomaticPicking"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If
            'TxtBillNo.Focus()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT BoeOut.BillNo, CONVERT(Char,BoeOut.BillDate,103) 'Bill Date', BoeOut.Revision, Client.Name FROM BoeOut, Client WHERE "
        StrSql = StrSql & "BoeOut.ClientID = Client.ClientID "
        If RbnClient.Checked Then
            StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCountry.Checked Then
            StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnTraffic.Checked Then
            StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCont.Checked Then
            StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
        End If
        If ChkDate.Checked Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = StrSql & "AND BoeOut.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND BoeOut.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        PnlFind.Style.Item("display") = ""
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtBoeNO.Text = GdvFind.SelectedRow.Cells(0).Text
        'TxtBillNo_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvProd.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvProd.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvPick.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvPick.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    'Private Sub TxtBillNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBillNo.TextChanged

    '    'For Each Row As GridViewRow In GdvPick.Rows
    '    '    Row.Visible = False
    '    'Next
    'End Sub

    Private Sub GdvPick_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvPick.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("ondblclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvPick, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvProd.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvProd, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvPick_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvPick.SelectedIndexChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        'Cancel if this pallet is picked 
        If Val(GdvPick.SelectedRow.Cells(7).Text) > 0 Then
            GdvProd.SelectedRow.Cells(5).Text = Val(GdvProd.SelectedRow.Cells(5).Text) + Val(GdvPick.SelectedRow.Cells(7).Text)
            GdvProd.SelectedRow.Cells(4).Text = Val(GdvProd.SelectedRow.Cells(4).Text) - Val(GdvPick.SelectedRow.Cells(7).Text)
            GdvPick.SelectedRow.Cells(7).Text = ""
        Else
            'Pick from the selected pallet
            If Val(GdvProd.SelectedRow.Cells(5).Text) > 0 Then
                If Val(GdvProd.SelectedRow.Cells(5).Text) <= Val(GdvPick.SelectedRow.Cells(6).Text) Then
                    GdvPick.SelectedRow.Cells(7).Text = GdvProd.SelectedRow.Cells(5).Text
                Else
                    GdvPick.SelectedRow.Cells(7).Text = GdvPick.SelectedRow.Cells(6).Text
                End If
                GdvProd.SelectedRow.Cells(5).Text = Val(GdvProd.SelectedRow.Cells(5).Text) - Val(GdvPick.SelectedRow.Cells(7).Text)
                GdvProd.SelectedRow.Cells(4).Text = Val(GdvProd.SelectedRow.Cells(4).Text) + Val(GdvPick.SelectedRow.Cells(7).Text)
            End If
        End If
    End Sub

    Private Sub GdvPick_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvPick.SelectedIndexChanging
        GdvPick.SelectedIndex = e.NewSelectedIndex
    End Sub

    'Private Sub GdvProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvProd.SelectedIndexChanged
    '    For Each Row As GridViewRow In GdvPick.Rows
    '        If Row.Cells(0).Text = GdvProd.SelectedRow.Cells(0).Text Then
    '            Row.Visible = True
    '        Else
    '            Row.Visible = False
    '        End If
    '    Next
    'End Sub

    Private Sub GdvProd_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvProd.SelectedIndexChanging
        GdvProd.SelectedIndex = e.NewSelectedIndex
    End Sub

    'aramaully | V1.0.0.3
    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim CmdDateIn As New OleDb.OleDbCommand
        Dim RdrDateIn As OleDb.OleDbDataReader
        'Check if everthing is picked
        For Each Row As GridViewRow In GdvProd.Rows
            If Row.RowType = DataControlRowType.DataRow Then
                If Val(Row.Cells(5).Text) > 0 Then
                    ErrHead.Text = "Picking Incomplete"
                    ErrMsg.Text = "Please pick Item " & Row.Cells(0).Text
                    ModalPopupExtender1.Show()
                    Exit Sub
                End If
            End If
        Next
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        CmdDateIn.Connection = Con
        CmdDateIn.Transaction = Trans
        Dim StrLoc1 As String
        Dim StrLoc2 As String
        Dim StrLoc3 As String
        Dim SngRatio1 As Single
        Dim SngRatio2 As Single
        Dim SngRatio3 As Single
        Dim BlnCleared As Boolean
        Dim WhCode = ""
        Dim gateOutRate = 200.0
        Dim gateOutFees As Double

        StrSql = "Select TariffValue FROM SDTariff WHERE TariffID='SDGateOut'"
        Cmd.CommandText = StrSql
        gateOutRate = Cmd.ExecuteScalar

        Try
            For Each Row As GridViewRow In GdvPick.Rows
                If Val(Row.Cells(7).Text) > 0 Then
                    Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                    StrSql = "UPDATE PaltDetail SET DelQuantity = " & Val(Row.Cells(7).Text) & ","
                    StrSql = StrSql & "BalQuantity = " & Val(Row.Cells(6).Text) & ","
                    StrSql = StrSql & "CumDelQuantity = CumDelQuantity + " & Val(Row.Cells(7).Text) & " "
                    StrSql = StrSql & "WHERE PalletID = " & Row.Cells(1).Text & " AND PCode = '" & Row.Cells(0).Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                    'If the pallet is empty then mark it as cleared and release location
                    'if there are no other products on it
                    StrSql = "SELECT SUM(BalQuantity) FROM PaltDetail WHERE PalletID = " & Row.Cells(1).Text
                    Cmd.CommandText = StrSql
                    BlnCleared = False

                    If Cmd.ExecuteScalar = 0 Then
                        BlnCleared = True

                        StrSql = "UPDATE Pallets SET Cleared = 1, DateCleared = '" & Format(CDate(TxtDate.Text), "MM/dd/yyyy") & "' "
                        StrSql = StrSql & "WHERE PalletID = '" & Row.Cells(1).Text & "'"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                        StrSql = "SELECT LocCode1, LocCode2, LocCode3, PRatio1, PRatio2, PRatio3, WhCode FROM Pallets WHERE PalletID =" & Row.Cells(1).Text
                        Cmd.CommandText = StrSql
                        Rdr = Cmd.ExecuteReader

                        If Rdr.Read Then
                            WhCode = Rdr("WhCode")
                            If IsDBNull(Rdr("LocCode1")) Then
                                StrLoc1 = ""
                            Else
                                StrLoc1 = Rdr("LocCode1")
                            End If
                            If IsDBNull(Rdr("LocCode2")) Then
                                StrLoc2 = ""
                            Else
                                StrLoc2 = Rdr("LocCode2")
                            End If
                            If IsDBNull(Rdr("LocCode3")) Then
                                StrLoc3 = ""
                            Else
                                StrLoc3 = Rdr("LocCode3")
                            End If
                            If IsDBNull(Rdr("PRatio1")) Then
                                SngRatio1 = 0
                            Else
                                SngRatio1 = Rdr("PRatio1")
                            End If
                            If IsDBNull(Rdr("PRatio2")) Then
                                SngRatio2 = 0
                            Else
                                SngRatio2 = Rdr("PRatio2")
                            End If
                            If IsDBNull(Rdr("PRatio3")) Then
                                SngRatio3 = 0
                            Else
                                SngRatio3 = Rdr("PRatio3")
                            End If
                        End If
                        Rdr.Close()
                        StrSql = "UPDATE Location SET CurPRatio = 0 WHERE LocCode = '" & StrLoc1 & "'"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                        If SngRatio1 + SngRatio2 + SngRatio3 > 2 Then 'Pallet ratio is 2.5 or 3 where all three locations are occupied
                            StrSql = "UPDATE Location SET CurPratio = 0 WHERE LocCode IN ('" & StrLoc1 & "','" & StrLoc2 & "','" & StrLoc3 & "')"
                            Cmd.CommandText = StrSql
                            Cmd.ExecuteNonQuery()
                        ElseIf SngRatio1 + SngRatio2 + SngRatio3 = 2 Then 'Pallet ratio = 2 where location 1 and location 2 are fully occupied
                            StrSql = "UPDATE Location SET CurPratio = 0 WHERE LocCode IN ('" & StrLoc1 & "','" & StrLoc2 & "')"
                            Cmd.CommandText = StrSql
                            Cmd.ExecuteNonQuery()
                        Else 'Pallet ratio is either 1 or 1.5 since 2, 2.5 and 3 is dealt with above
                            'Location 1 is anyway fully occupied
                            StrSql = "UPDATE Location SET CurPRatio = 0 WHERE LocCode = '" & StrLoc1 & "'"
                            Cmd.CommandText = StrSql
                            Cmd.ExecuteNonQuery()
                            If SngRatio2 > 0 Then 'Pallet ratio should be 1.5
                                StrSql = "UPDATE Location SET CurPRatio = CurPRatio - .5 WHERE LocCode = '" & StrLoc2 & "'"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                        End If
                    End If

                    'Create picklist record
                    If WhCode = "FPZ6SD" Then
                        gateOutFees = Row.Cells(7).Text * gateOutRate
                        StrSql = "INSERT INTO PickList (BoeNo, PalletID, PCode, PickDate, Location, Quantity, Balance, Unit, DateIn, [Full], PRatio, GateOutRate, GateOutFees)(SELECT '"
                        StrSql = StrSql & TxtBoeNO.Text & "',"
                        StrSql = StrSql & Row.Cells(1).Text & ",'"
                        StrSql = StrSql & Row.Cells(0).Text & "','"
                        StrSql = StrSql & Format(CDate(TxtDate.Text), "MM/dd/yyyy") & "','"
                        StrSql = StrSql & Row.Cells(2).Text.Replace("&nbsp;", "") & "',"
                        StrSql = StrSql & Row.Cells(7).Text & ","
                        StrSql = StrSql & Val(Row.Cells(6).Text) & ",'"
                        StrSql = StrSql & Row.Cells(4).Text & "',"

                        CmdDateIn.CommandText = "SELECT DateIn FROM Pallets WHERE PalletID = " & Row.Cells(1).Text & " And ClientID = '" & TxtClientID.Text & "'"
                        RdrDateIn = CmdDateIn.ExecuteReader
                        If RdrDateIn.Read Then
                            StrSql = StrSql & "'" & Format(CDate(RdrDateIn("DateIn")), "MM/dd/yyyy") & "',"
                        Else
                            StrSql = StrSql & "NULL,"
                        End If
                        RdrDateIn.Close()
                        StrSql = StrSql & IIf(BlnCleared, 1, 0) & ","
                        CmdDateIn.CommandText = "SELECT PRatio FROM  Pallets WHERE PalletID = " & Row.Cells(1).Text & " And ClientID = '" & TxtClientID.Text & "'"
                        RdrDateIn = CmdDateIn.ExecuteReader
                        If RdrDateIn.Read Then
                            StrSql = StrSql & "" & RdrDateIn("PRatio") & ","
                        Else
                            StrSql = StrSql & "0,"
                        End If
                        'aramaully | V1.0.0.3
                        StrSql = StrSql & gateOutRate & ", "
                        StrSql = StrSql & gateOutFees & ")"

                        RdrDateIn.Close()
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    Else
                        StrSql = "INSERT INTO PickList (BoeNo, PalletID, PCode, PickDate, Location, Quantity, Balance, Unit, DateIn, [Full], PRatio)(SELECT '"
                        StrSql = StrSql & TxtBoeNO.Text & "',"
                        StrSql = StrSql & Row.Cells(1).Text & ",'"
                        StrSql = StrSql & Row.Cells(0).Text & "','"
                        StrSql = StrSql & Format(CDate(TxtDate.Text), "MM/dd/yyyy") & "','"
                        StrSql = StrSql & Row.Cells(2).Text.Replace("&nbsp;", "") & "',"
                        StrSql = StrSql & Row.Cells(7).Text & ","
                        StrSql = StrSql & Val(Row.Cells(6).Text) & ",'"
                        StrSql = StrSql & Row.Cells(4).Text & "',"

                        CmdDateIn.CommandText = "SELECT DateIn FROM Pallets WHERE PalletID = " & Row.Cells(1).Text & " And ClientID = '" & TxtClientID.Text & "'"
                        RdrDateIn = CmdDateIn.ExecuteReader
                        If RdrDateIn.Read Then
                            StrSql = StrSql & "'" & Format(CDate(RdrDateIn("DateIn")), "MM/dd/yyyy") & "',"
                        Else
                            StrSql = StrSql & "NULL,"
                        End If
                        RdrDateIn.Close()
                        StrSql = StrSql & IIf(BlnCleared, 1, 0) & ","
                        CmdDateIn.CommandText = "SELECT PRatio FROM  Pallets WHERE PalletID = " & Row.Cells(1).Text & " And ClientID = '" & TxtClientID.Text & "'"
                        RdrDateIn = CmdDateIn.ExecuteReader
                        If RdrDateIn.Read Then
                            StrSql = StrSql & "" & RdrDateIn("PRatio") & ")"
                        Else
                            StrSql = StrSql & "0)"
                        End If
                        RdrDateIn.Close()
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If

                    'Update BoeInProd table with Balance quantity
                    StrSql = "UPDATE BoeInProd SET BalQuantity = " & Val(Row.Cells(6).Text) & " WHERE "
                    StrSql = StrSql & "BoeNo = '" & TxtBoeNO.Text & "' AND PCode = '" & Row.Cells(0).Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                    'Update Bill out date
                    StrSql = "UPDATE BoeOut SET BillDate = '" & Format(CDate(TxtDate.Text), "MM/dd/yyyy") & "' WHERE BillNo = '" & TxtBoeNO.Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            Trans.Commit()
            LblText.Visible = True
            LblText.Text = "Save Successfull"
            'TxtBillNo_TextChanged(Me, e)
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Save Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNew.Click
        ClearAll()
        TxtBoeNO.Text = ""
    End Sub

    Private Sub ClearAll()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        TxtDate.Text = Date.Today.ToString("dd-MMM-yyyy")
        TxtDelDate.Text = Date.Today.ToString("dd-MMM-yyyy")
        TxtClientID.Text = ""
        TxtDescription.Text = ""
        TxtInstruction.Text = ""
        TxtName.Text = ""
        TxtPCode.Text = ""
        GdvProd.DataSource = Nothing
        GdvProd.DataBind()
        GdvPick.DataSource = Nothing
        GdvPick.DataBind()
    End Sub

    'Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
    '    StrSql = "SELECT Top 1 BillNo FROM BoeOut WHERE BillNo > '" & TxtBillNo.Text & "'"
    '    Cmd.Connection = Con
    '    Cmd.CommandText = StrSql
    '    TxtBillNo.Text = Cmd.ExecuteScalar
    '    If TxtBillNo.Text = "" Then
    '        StrSql = "SELECT MIN(BillNo) FROM BoeIn"
    '        Cmd.CommandText = StrSql
    '        TxtBillNo.Text = Cmd.ExecuteScalar
    '    End If
    '    TxtBillNo_TextChanged(Me, e)
    'End Sub

    'Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevious.Click
    '    StrSql = "SELECT Top 1 BillNo FROM BoeOut WHERE BillNo < '" & TxtBillNo.Text & "'"
    '    StrSql = StrSql & "ORDER BY BillNo DESC"
    '    Cmd.Connection = Con
    '    Cmd.CommandText = StrSql
    '    TxtBoeNO.Text = Cmd.ExecuteScalar
    '    If TxtBoeNO.Text = "" Then
    '        StrSql = "SELECT MAX(BillNo) FROM BoeIn"
    '        Cmd.CommandText = StrSql
    '        TxtBoeNO.Text = Cmd.ExecuteScalar
    '    End If
    '    TxtBillNo_TextChanged(Me, e)
    'End Sub

    Protected Sub ButPick_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPick.Click
        Dim StrPCode As String
        Dim IntQty As Integer
        For Each Row As GridViewRow In GdvProd.Rows
            StrPCode = Row.Cells(0).Text
            IntQty = Val(Row.Cells(5).Text)
            For Each IRow As GridViewRow In GdvPick.Rows
                If IRow.Cells(0).Text = StrPCode Then
                    If Val(IRow.Cells(6).Text) >= IntQty Then
                        IRow.Cells(6).Text = Val(IRow.Cells(6).Text) - IntQty
                        IRow.Cells(7).Text = IntQty
                        IntQty = 0
                    Else
                        IRow.Cells(7).Text = IRow.Cells(6).Text
                        IRow.Cells(6).Text = 0
                        IntQty = IntQty - Val(IRow.Cells(7).Text)
                    End If
                End If
                If Val(IRow.Cells(7).Text) > 0 Then IRow.Visible = True
                If IntQty <= 0 Then Exit For
            Next
            Row.Cells(4).Text = Val(Row.Cells(5).Text) - IntQty
            Row.Cells(5).Text = IntQty
        Next
        LblText.Visible = True
        LblText.Text = "Picking completed. Click on Save."
    End Sub

    Private Sub TxtBoeNO_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBoeNO.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT BillDate,CrossDock,Client.ClientID,Client.Name FROM BoeOut,Client "
        StrSql = StrSql & "WHERE BoeOut.ClientID = Client.ClientID AND BillNo = '" & TxtBoeNO.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        'On Error Resume Next
        If Rdr.Read Then
            If Rdr("CrossDock") Then
                TxtName.Text = "This is Constructive type Bill. Cannot Pick!"
                Exit Sub
            End If
            TxtDate.Text = Rdr("BillDate")
            TxtName.Text = Rdr("Name")
            TxtClientID.Text = Rdr("ClientID")
        End If
        Rdr.Close()
        StrSql = "SELECT PCode,ProdDescription,Quantity,UnitCode,ISNULL((SELECT SUM(Quantity) FROM PickList WHERE PCode = BoeOutProd.PCode "
        StrSql = StrSql & "AND BoeNo = BoeOutProd.Boeno),0) AS Picked, Quantity-ISNULL((SELECT SUM(Quantity) FROM PickList WHERE PCode = BoeOutProd.PCode "
        StrSql = StrSql & "AND BoeNo = BoeOutProd.Boeno),0) AS Balance FROM BoeOutProd WHERE BoeNo = '" & TxtBoeNO.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvProd.DataSource = Rdr
        GdvProd.DataBind()
        Rdr.Close()
        StrSql = "SELECT BoeOutProd.PCode,PaltDetail.PalletID,PaltDetail.Quantity,BoeOutProd.BoeInNo,PickList.Quantity,"
        StrSql = StrSql & "PaltDetail.BalQuantity AS Balance,PaltDetail.Unit,Pallets.LocCode1 AS Location FROM BoeOutProd "
        StrSql = StrSql & "LEFT OUTER JOIN PaltDetail ON PaltDetail.BoeNo IN (BoeoutProd.BoeInNo) AND BoeOutProd.PCode = PaltDetail.PCode "
        StrSql = StrSql & "LEFT OUTER JOIN PickList ON PickList.BoeNo = BoeOutProd.BoeNo AND PaltDetail.PalletID = PickList.PalletID "
        StrSql = StrSql & "LEFT OUTER JOIN Pallets ON PaltDetail.PalletID = Pallets.PalletID "
        StrSql = StrSql & "WHERE BoeOutProd.BoeNo = '" & TxtBoeNO.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvPick.DataSource = Rdr
        GdvPick.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ButPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrint.Click
        StrSel = "{PickList.BoeNo} = '" & TxtBoeNO.Text & "'"
        'Response.Redirect("Report.aspx?RptID=Print Pick List&RPTFileName=PrintPickList.aspx&RptFile=RptPickList.rpt&StrSelF=" & StrSel)
        Session("ReportFile") = "RptPickList.rpt"
        Session("Period") = ""
        Session("Zone") = ""
        Session("SelFormula") = StrSel
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub

    Protected Sub But_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButReverse.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        CmdPicking.Connection = Con
        CmdPallets.Connection = Con
        CmdUpdatePallets.Connection = Con
        CmdPicking.CommandText = "SELECT * FROM PickList WHERE BoeNo='" & TxtBoeNO.Text & "'  ORDER BY BoeNo"
        RdrPicking = CmdPicking.ExecuteReader
        While RdrPicking.Read
            'CmdPallets.CommandText = "SELECT * FROM Pallets WHERE PalletID = " & RdrPicking("PalletID") & ""
            'RdrPallets = CmdPallets.ExecuteReader
            'If RdrPallets.Read Then
            '    If RdrPallets("Cleared") Then
            '        ' For Location 1
            '        StrSql = "UPDATE Location SET CurPRatio = CurPratio + " & Val(RdrPallets("PRatio1"))
            '        StrSql = StrSql & " WHERE LocCode = '" & RdrPallets("LocCode1") & "'"
            '        CmdUpdatePallets.CommandText = StrSql
            '        CmdUpdatePallets.ExecuteNonQuery()
            '        ' For Location 2
            '        StrSql = "UPDATE Location SET CurPRatio = CurPratio + " & Val(RdrPallets("PRatio2"))
            '        StrSql = StrSql & " WHERE LocCode = '" & RdrPallets("LocCode2") & "'"
            '        CmdUpdatePallets.CommandText = StrSql
            '        CmdUpdatePallets.ExecuteNonQuery()
            '        ' For Location 3
            '        StrSql = "UPDATE Location SET CurPRatio = CurPratio + " & Val(RdrPallets("PRatio3"))
            '        StrSql = StrSql & " WHERE LocCode = '" & RdrPallets("LocCode3") & "'"
            '        CmdUpdatePallets.CommandText = StrSql
            '        CmdUpdatePallets.ExecuteNonQuery()
            '    End If
            'End If
            'RdrPallets.Close()
            'If any pallets has been cleared make them not cleared
            StrSql = "UPDATE Pallets SET Cleared = 0, DateCleared = Null WHERE "
            StrSql = StrSql & "PalletID = " & RdrPicking("PalletID")
            CmdUpdatePallets.CommandText = StrSql
            CmdUpdatePallets.ExecuteNonQuery()
            'Reverse quantities picked
            StrSql = "UPDATE PaltDetail SET DelQuantity=DelQuantity - " & RdrPicking("Quantity") & ""
            StrSql = StrSql & ", CumDelQuantity=CumDelQuantity - " & RdrPicking("Quantity") & ""
            StrSql = StrSql & ", BalQuantity=BalQuantity + " & RdrPicking("Quantity") & ""
            StrSql = StrSql & " WHERE PalletID=" & RdrPicking("PalletID") & ""
            StrSql = StrSql & " AND PCode ='" & RdrPicking("PCode") & "'"
            CmdUpdatePallets.CommandText = StrSql
            CmdUpdatePallets.ExecuteNonQuery()
        End While
        RdrPicking.Close()
        StrSql = "DELETE FROM PickList WHERE BoeNo = '" & TxtBoeNO.Text & "'"
        CmdUpdatePallets.CommandText = StrSql
        CmdUpdatePallets.ExecuteNonQuery()
        LblText.Visible = True
        LblText.Text = "Picking Under Outbound " & TxtBoeNO.Text & " has been reversed."
        TxtBoeNO_TextChanged(Me, e)
    End Sub
End Class