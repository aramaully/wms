﻿Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Xml

Partial Public Class MenuScreen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.CacheControl = "no-cache"
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Response.AddHeader("X-UA-Compatible", "IE=EmulateIE7")
        If Not Session("LoggedIn") Then Response.Redirect("~/Default.aspx")
        Dim StrLoc As String = ""
        Dim CmdLoc As New OleDb.OleDbCommand
        Dim conn As New OleDbConnection(Session("ConnString"))
        conn.Open()

        'LblUser.Text = Session("UserName").ToString 
        'LblDate.Text = Today
        'LblDate0.Text = Today
        xmlDataSource.Data = Nothing
        Dim ds As New DataSet()
        'Using conn As New OleDbConnection(ConfigurationManager.ConnectionStrings("SQLConnection").ConnectionString)
        Dim sql As String = "Select MenuID, Text, Description, Navigate, ParentID from Access WHERE UserID='" & Session("UserID").ToString & "'"
        Dim da As New OleDbDataAdapter(sql, conn)
        da.Fill(ds)
        da.Dispose()
        'End Using
        ds.DataSetName = "Menus"
        ds.Tables(0).TableName = "Menu"
        Dim relation As New DataRelation("ParentChild", ds.Tables("Menu").Columns("MenuID"), ds.Tables("Menu").Columns("ParentID"), True)
        relation.Nested = True
        ds.Relations.Add(relation)

        xmlDataSource.Data = ds.GetXml()

        conn.Close()
    End Sub
End Class

