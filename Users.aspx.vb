﻿Public Partial Class Users
    Inherits System.Web.UI.Page

    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdProfil As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatUser As New DataTable
    Dim StrCondition As String = ""
    Const MENUID = "AddUser"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If

        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con

            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            Cmd.CommandText = "SELECT WHCode FROM Warehouse ORDER BY WHCode"
            Rdr = Cmd.ExecuteReader
            CmbWHCode.Items.Add("Select...")
            CmbWHCode.Items.Add("ALL")
            While Rdr.Read
                CmbWHCode.Items.Add(Rdr("WHCode"))
            End While
            Rdr.Close()

            Cmd.Connection = Con
            Cmd.CommandText = "SELECT * FROM Profil ORDER BY ProfilID"
            Rdr = Cmd.ExecuteReader

            While Rdr.Read
                Dim tempList As New ListItem
                tempList.Text = Rdr("Profils")
                tempList.Value = Rdr("ProfilID")
                Me.cmbProfile.Items.Add(tempList)
            End While
            Rdr.Close()
            BindLoc()
        End If
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Private Sub BindLoc()
        Cmd.Connection = Con
        StrSql = "SELECT UserID,[Name],Password,WHCode,Profils AS Profile,ProfilID AS ProfileID FROM [User]"
        StrSql = StrSql & " LEFT JOIN profil on ProfilID = UserProfil"

        If Session("UserID") <> "admin" Then
            StrSql = StrSql & " WHERE [User].UserId <> 'admin'"
        End If
        StrSql = StrSql & " ORDER BY UserID"

        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar Is Nothing Then Exit Sub
        Rdr = Cmd.ExecuteReader
        DatUser.Load(Rdr)
        Rdr.Close()
        GdvLoc.DataSource = DatUser
        GdvLoc.DataBind()
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about: blank")
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        If Trim(TxtPassword.Text) <> Trim(TxtConfirm.Text) Then
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = "Password Not Matching"
            ModalPopupExtender1.Show()
            Exit Sub
        End If
        If CmbWHCode.Text = "Select..." Then
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = "Select Warehouse"
            ModalPopupExtender1.Show()
            Exit Sub
        End If
        If Trim(TxtUserID.Text) = "" Then
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = "Enter UserID"
            Exit Sub
        End If

        Cmd.Connection = Con
        Cmd.CommandText = "SELECT UserID FROM [User] WHERE UserID = '" & TxtUserID.Text & "'"

        If Cmd.ExecuteScalar Is Nothing Then
            If TxtUserID.Text <> "admin" Then
                StrSql = "INSERT INTO [User] (UserID,[Name],Password,WhCode,UserProfil) "
                StrSql = StrSql & "VALUES('" & TxtUserID.Text & "','"
                StrSql = StrSql & TxtName.Text & "','"
                StrSql = StrSql & TxtPassword.Text & "','"
                StrSql = StrSql & CmbWHCode.Text & "',"
                StrSql = StrSql & Me.cmbProfile.Text & ")"
            Else
                ErrHead.Text = "Update Failed"
                ErrMsg.Text = "Select another user id"
                Exit Sub
            End If
        Else
            StrSql = "UPDATE [User] SET [Name] = '" & TxtName.Text & "',"
            StrSql = StrSql & "Password='" & TxtPassword.Text & "',"
            StrSql = StrSql & "WhCode='" & CmbWHCode.Text & "',"
            StrSql = StrSql & "UserProfil=" & Me.cmbProfile.Text & " WHERE UserId = '" & TxtUserID.Text & "'"
        End If
        Cmd.CommandText = StrSql
        Cmd.ExecuteNonQuery()

        If (Me.cmbProfile.Text <> "Select...") Then
            SaveProfil()
        End If

        ButNew_Click(Me, e)
        BindLoc()
    End Sub

    Private Sub GdvLoc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvLoc.PageIndexChanging
        GdvLoc.PageIndex = e.NewPageIndex
        BindLoc()
    End Sub

    Private Sub GdvLoc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLoc.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLoc, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvLoc.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLoc.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub GdvLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLoc.SelectedIndexChanged
        On Error Resume Next
        TxtUserID.Text = ChkSpace(GdvLoc.SelectedRow.Cells(0).Text)
        TxtName.Text = ChkSpace(GdvLoc.SelectedRow.Cells(1).Text)
        TxtPassword.Text = ChkSpace(GdvLoc.SelectedRow.Cells(2).Text)
        TxtConfirm.Text = ChkSpace(GdvLoc.SelectedRow.Cells(2).Text)
        CmbWHCode.Text = ChkSpace(GdvLoc.SelectedRow.Cells(3).Text)
        If (ChkSpace(GdvLoc.SelectedRow.Cells(5).Text) = "") Then
            Me.cmbProfile.SelectedValue = "Select..."
        Else
            Me.cmbProfile.SelectedValue = ChkSpace(GdvLoc.SelectedRow.Cells(5).Text)
        End If
    End Sub

    Private Sub GdvLoc_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLoc.SelectedIndexChanging
        GdvLoc.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNew.Click
        On Error Resume Next
        clear()
        TxtUserID.Focus()
    End Sub

    Private Sub SaveProfil()
        Cmd.Connection = Con
        CmdProfil.Connection = Con

        Cmd.CommandText = "DELETE FROM Access WHERE UserId = '" & Me.TxtUserID.Text & "'"
        Cmd.ExecuteNonQuery()

        StrSql = "SELECT ProfilMenuName FROM ProfilUser pu "
        StrSql = StrSql & "INNER JOIN profil p ON p.ProfilID = pu.ProfilID "
        StrSql = StrSql & "WHERE p.ProfilID = '" & Me.cmbProfile.SelectedValue & "'"


        CmdProfil.CommandText = StrSql
        Rdr = CmdProfil.ExecuteReader

        If (Rdr.FieldCount > 0) Then
            While Rdr.Read
                StrSql = "INSERT INTO Access (UserId,MenuName) "
                StrSql = StrSql & "VALUES('" & TxtUserID.Text & "',"
                StrSql = StrSql & "'" & Rdr("ProfilMenuName") & "')"

                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            End While
        End If

        Rdr.Close()

        clear()
    End Sub

    Private Sub clear()
        TxtUserID.Text = ""
        CmbWHCode.Text = "Select..."
        Me.cmbProfile.Text = "Select..."
        TxtName.Text = ""
        Me.TxtPassword.Text = ""
        Me.TxtConfirm.Text = ""
    End Sub

End Class