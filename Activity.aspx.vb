﻿Public Partial Class Activity
    Inherits System.Web.UI.Page
    Dim con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdSave As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("Default.aspx")
        End If
        con.ConnectionString = Session("ConnString")
        con.Open()
        FillGrid()
    End Sub

    Protected Sub FillGrid()
        Dim cmd As New OleDb.OleDbCommand("SELECT * FROM Activity", con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GridView.DataSource = ds
        GridView.DataBind()
    End Sub

    Private Sub GridView_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView.PageIndexChanged
        Dim cmd As New OleDb.OleDbCommand("SELECT * FROM Activity", con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GridView.DataSource = ds
        GridView.DataBind()
    End Sub

    Private Sub GridView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView.PageIndexChanging
        GridView.PageIndex = e.NewPageIndex
    End Sub

    Private Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GridView, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GridView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView.SelectedIndexChanged
        TxtActivity.Text = GridView.SelectedRow.Cells(0).Text
    End Sub

    Private Sub GridView_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GridView.SelectedIndexChanging
        GridView.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GridView.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GridView.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNew.Click
        TxtActivity.Text = ""
        TxtActivity.Focus()
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        StrSql = "INSERT INTO Activity VALUES('" & TxtActivity.Text & "')"
        CmdSave.Connection = con
        CmdSave.CommandText = StrSql
        CmdSave.ExecuteNonQuery()
        TxtActivity.Text = ""
        FillGrid()
    End Sub

    Protected Sub ButDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDelete.Click
        StrSql = "DELETE FROM Activity WHERE Activity = '" & TxtActivity.Text & "'"
        CmdSave.Connection = con
        CmdSave.CommandText = StrSql
        CmdSave.ExecuteNonQuery()
        TxtActivity.Text = ""
        FillGrid()
    End Sub
End Class