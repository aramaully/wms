﻿Public Partial Class FindClient
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtSearch.Text = Session("PstrCode")
            TxtSearch.Focus()
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Refresh" Then
            Cmd.Connection = Con
            StrSql = "SELECT ClientID,[Name],[Address] FROM Client "
            If TxtSearch.Text <> "" Then
                If OptName.Checked Then
                    StrSql = StrSql & "WHERE [Name] LIKE '%" & TxtSearch.Text & "%'"
                End If
                If OptAddress.Checked Then
                    StrSql = StrSql & "WHERE [Address] LIKE '%" & TxtSearch.Text & "%'"
                End If
                If OptCity.Checked Then
                    StrSql = StrSql & "WHERE [City] LIKE '%" & TxtSearch.Text & "%'"
                End If
            End If
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            GdvEmp.DataSource = Rdr
            GdvEmp.DataBind()
            Rdr.Close()
        End If
    End Sub

    Private Sub GdvEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvEmp.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvEmp, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvEmp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvEmp.SelectedIndexChanged
        TxtClientID.Value = GdvEmp.SelectedRow.Cells(0).Text
        GdvEmp.SelectedRow.Focus()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CloseMe", "GetID();", True)
    End Sub

    Private Sub GdvEmp_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvEmp.SelectedIndexChanging
        GdvEmp.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvEmp.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvEmp.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub TxtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtSearch.TextChanged
        Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(0))
        ToolBar1_ItemClick(Me, ev)
    End Sub
End Class