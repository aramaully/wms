﻿Public Partial Class RepStkLoc
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdZone As New OleDb.OleDbCommand
    Dim RdrZone As OleDb.OleDbDataReader
    Dim CmdBoe As New OleDb.OleDbCommand
    Dim RdrBoe As OleDb.OleDbDataReader
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrSel As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            StrSql = "SELECT * FROM Zones ORDER BY ZoneCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            CmbZone.Items.Add("ALL")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("ZoneCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub CmbZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbZone.SelectedIndexChanged
        StrSql = "SELECT * FROM Zones WHERE ZoneCode='" & CmbZone.Text & "' ORDER BY ZoneCode"
        CmdZone.Connection = Con
        CmdZone.CommandText = StrSql
        RdrZone = CmdZone.ExecuteReader
        If RdrZone.Read Then
            TxtName.Text = RdrZone("Description")
        End If
        RdrZone.Close()
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        Dim StrSelFormula As String
        StrSelFormula = "{PaltDetail.BalQuantity} > 0 "
        If CmbZone.Text <> "ALL" Then
            StrSelFormula = StrSelFormula & "And {Pallets.ZoneCode} = '" & CmbZone.Text & "'"
        End If
        If Session("PstrZone").ToString <> "ALL" Then
            If Len(StrSelFormula) > 0 Then
                StrSelFormula = StrSelFormula & " And "
            End If
            StrSelFormula = StrSelFormula & "{Location.WhCode} = '" & Session("PstrZone").ToString & "'"
        End If
        Response.Redirect("Report.aspx?RptID=Stock By Location&RPTFileName=RepStkLoc.aspx&RptFile=RptStockLocation.rpt&StrSelF=" & StrSelFormula)

    End Sub
End Class