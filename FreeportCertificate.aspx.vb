﻿Partial Public Class FreeportCertificate
    Inherits System.Web.UI.Page
    Dim StrSql As String
    Dim Cmd As New OleDb.OleDbCommand
    Dim Con As New OleDb.OleDbConnection
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdClient As New OleDb.OleDbCommand
    Dim RdrClient As OleDb.OleDbDataReader

    Const MENUID = "FreeportCertificate"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            'StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            'Cmd.CommandText = StrSql
            'If Cmd.ExecuteScalar = 0 Then
            '    Response.Redirect("about:blank")
            'End If

            GetZones()
            GetStatus()
            GetCategory()
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim ExpiryDate As String

        If e.Item.CommandName = "Print" Then
            Dim StrSelFormula As String
            Session("ReportFile") = "RptFreeportCertificate.rpt"

            StrSelFormula = ""
            If TxtName.Text <> "" Then
                StrSelFormula = StrSelFormula & "AND {Client.Name} = '" & TxtName.Text & "' "
            End If

            If cmbStatus.Text <> "ALL" Then
                StrSelFormula = StrSelFormula & "AND {Client.Status} = '" & cmbStatus.Text & "' "
            End If

            If CmbCategory.Text <> "ALL" Then
                StrSelFormula = StrSelFormula & "AND {Client.Category} = '" & CmbCategory.Text & "' "
            End If

            If CmbZone.Text <> "ALL" Then
                StrSelFormula = "{Client.WHouses} = '" & CmbZone.Text & "' "
            End If

            If DtpExpiryDate.Text <> "" Then
                ExpiryDate = Mid(DtpExpiryDate.Text, 7, 4) & "," & Mid(DtpExpiryDate.Text, 4, 2) & "," & Mid(DtpExpiryDate.Text, 1, 2)
                StrSelFormula = StrSelFormula & "AND {Client.LicenceExp} = Date(" & ExpiryDate & ") "
            End If

            StrSelFormula = LTrim(StrSelFormula)

            If StrSelFormula.StartsWith("AND") Then
                StrSelFormula = StrSelFormula.Substring(4)
            End If

            Session("Period") = ""
            Session("Zone") = ""
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "VALIDITY DATE OF FREEPORT CERTIFICATE"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub

    Private Sub GetZones()
        CmbZone.Items.Clear()

        StrSql = "SELECT * FROM WareHouse ORDER By WhCode"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        CmbZone.Items.Add("ALL")
        While Rdr.Read
            CmbZone.Items.Add(Rdr("WhCode"))
        End While
        Rdr.Close()
    End Sub

    Private Sub GetStatus()
        cmbStatus.Items.Clear()

        StrSql = "SELECT DISTINCT([Status]) FROM Client WHERE [Status] IS NOT NULL AND [Status] <> 'Select ...' ORDER BY [Status]"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        cmbStatus.Items.Add("ALL")
        While Rdr.Read
            cmbStatus.Items.Add(Rdr("Status"))
        End While
        Rdr.Close()
    End Sub

    Private Sub GetCategory()
        CmbCategory.Items.Clear()

        StrSql = "SELECT DISTINCT(Category) FROM Client ORDER BY Category"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        CmbCategory.Items.Add("ALL")
        While Rdr.Read
            CmbCategory.Items.Add(Rdr("Category"))
        End While
        Rdr.Close()
    End Sub

    Protected Sub ButFindClient_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindClient.Click
        Session.Add("PstrCode", TxtClientID.Text)
        ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        If Trim(TxtClientID.Text) <> "" Then
            CmdClient.Connection = Con
            CmdClient.CommandText = "Select *From Client Where ClientID = '" & TxtClientID.Text & "'"
            RdrClient = CmdClient.ExecuteReader
            If RdrClient.Read Then
                TxtName.Text = RdrClient("Name") & ""
            End If
            RdrClient.Close()
        Else
            TxtName.Text = ""
        End If
    End Sub
End Class