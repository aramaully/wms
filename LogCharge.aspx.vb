﻿Public Partial Class LogCharge
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdInvoice As New OleDb.OleDbCommand
    Dim RdrInvoice As OleDb.OleDbDataReader
    Dim CmdBill As New OleDb.OleDbCommand
    Dim RdrBill As OleDb.OleDbDataReader
    Dim CmdReq As New OleDb.OleDbCommand
    Dim RdrReq As OleDb.OleDbDataReader
    Dim CmdChkLog As New OleDb.OleDbCommand
    Dim RdrChkLog As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrSel As String

    Const MENUID = "LogCharge"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            StrSql = "SELECT Name FROM Traffic ORDER BY Name"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbTrafficOfficer.Items.Add("Select...")
            While Rdr.Read
                CmbTrafficOfficer.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()
            TxtTotal.Style("text-align") = "right"
            TxtVat.Style("text-align") = "right"
            TxtAmount.Style("text-align") = "right"
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        If RbnClient.Text = "Name" And ChkDate.Visible Then
            StrSql = "SELECT InvoiceNo,Client.Name,CONVERT(Varchar,InvDate,103) Date,[Value] FROM Invoice,Client WHERE Client.ClientID = Invoice.ClientID "
            If TxtSearch.Text <> "" Then
                StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
            End If
            If CmbZone.Text <> "ALL" Then
                StrSql = StrSql & "AND WhCode = '" & CmbZone.Text & "' "
            End If
            If ChkDate.Checked Then
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                StrSql = StrSql & "AND Invoice.InvDate >= '" & Format(CDate(TxtFrom.Text), "MM/dd/yyyy") & "' AND Invoice.InvDate <= '" & Format(CDate(TxtTo.Text), "MM/dd/yyyy") & "'"
            End If
        ElseIf RbnClient.Text = "Name" And Not ChkDate.Visible Then
            StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
            If RbnClient.Checked Then
                StrSql = StrSql & "Name"
            ElseIf RbnCountry.Checked Then
                StrSql = StrSql & "City"
            Else
                StrSql = StrSql & "Contact"
            End If
            StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Else
            StrSql = "SELECT BoeIn.BillNo, CONVERT(Char,BoeIn.BillDate,103) 'Bill Date', BoeIn.Revision, Client.Name FROM BoeIn, Client WHERE "
            StrSql = StrSql & "BoeIn.ClientID = Client.ClientID "
            If RbnClient.Checked Then
                StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCountry.Checked Then
                StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnTraffic.Checked Then
                StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCont.Checked Then
                StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
            End If
            If ChkDate.Checked Then
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                StrSql = StrSql & "AND Boein.BillDate >= '" & CDate(TxtFrom.Text).ToString("MM/dd/yyyy") & "' "
                StrSql = StrSql & "AND Boein.BillDate <= '" & CDate(TxtTo.Text).ToString("MM/dd/yyyy") & "' "
            End If
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        RbnClient.Text = "Client"
        RbnCountry.Text = "Country"
        RbnTraffic.Text = "Traffic Op"
        RbnCont.Visible = True
        ChkDate.Visible = True
        TxtFrom.Visible = True
        TxtTo.Visible = True
        LblToDate.Visible = True
        GdvFind.DataSource = Nothing
        TxtTo.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtFrom.Text = Date.Today.AddDays(-10).ToString("dd/MM/yyyy")
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        If RbnClient.Text = "Client" Then
            TxtBillNo.Text = GdvFind.SelectedRow.Cells(0).Text
            TxtBillNo_TextChanged(Me, e)
        ElseIf Not ChkDate.Visible Then
            TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
            TxtClientID_TextChanged(Me, e)
        Else
            TxtInvoiceNo.Text = GdvFind.SelectedRow.Cells(0).Text
            GetInvoiceDetails()
        End If
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvLog.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLog.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub ButFindClient_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindClient.Click
        RbnClient.Text = "Name"
        RbnCountry.Text = "City"
        RbnTraffic.Text = "Contact"
        RbnCont.Visible = False
        ChkDate.Visible = False
        TxtFrom.Visible = False
        TxtTo.Visible = False
        LblToDate.Visible = False
        GdvFind.DataSource = Nothing
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub TxtBillNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBillNo.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT BoeIn.BillDate,BoeIn.Revision,BoeIn.ContainerNo,BoeIn.WHCode,Client.Name,BoeIn.ClientID "
        StrSql = StrSql & "FROM BoeIn,Client WHERE BoeIn.ClientID = Client.ClientID AND BillNo = '" & TxtBillNo.Text & "'"
        CmdBill.Connection = Con
        CmdBill.CommandText = StrSql
        RdrBill = CmdBill.ExecuteReader
        On Error Resume Next
        If RdrBill.Read Then
            TxtRevision.Text = RdrBill("Revision")
            TxtDate.Text = RdrBill("BillDate")
            TxtRemarks.Text = RdrBill("ContainerNo")
            TxtClientID.Text = RdrBill("ClientID")
            TxtName.Text = RdrBill("Name")
            CmbZone.Text = RdrBill("WHCode")
            TxtClientID_TextChanged(Me, e)
        Else
            TxtName.Text = "Invalid Inbound"
            TxtBillNo.Focus()
        End If
        RdrBill.Close()
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        'ClearAll()
        StrSql = "SELECT * FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtName.Text = Rdr("Name")
            If Rdr("Category") = "Freeport" Then
                ChkVatable.Checked = False
            Else
                ChkVatable.Checked = True
            End If
        Else
            TxtName.Text = "Invalid Client ID"
            TxtClientID.Focus()
            Exit Sub
        End If
        Rdr.Close()
        If TxtBillNo.Text <> "" Then
            StrSql = "SELECT * FROM Logistics WHERE BoeNo='" & TxtBillNo.Text & "'  AND ClientID = '" & TxtClientID.Text & "' AND WhCode = '" & CmbZone.Text & "'"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            GdvLog.DataSource = Rdr
            GdvLog.DataBind()
            Rdr.Close()
        End If
        If GdvLog.Rows.Count = 0 Then
            'We should get that data from Lease Services.
            StrSql = "SELECT ChCode,Description,NULL As TimeIn, NULL As TimeOut,NULL As Hrs,0 AS Quantity,Unit,Rate,0 AS Amount "
            StrSql = StrSql & "FROM TariffCard WHERE ChCode IN (SELECT ChCode FROM ClientTariff "
            StrSql = StrSql & "WHERE ClientId ='" & TxtClientID.Text & "') And StorageContainer = 0"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            GdvLog.DataSource = Rdr
            GdvLog.DataBind()
            Rdr.Close()
        End If
        Dim CmdNextCashInvoice As New OleDb.OleDbCommand
        StrSql = "SELECT Vat FROM [System] WHERE WhCode = '" & CmbZone.Text & "'"
        Cmd.Connection = Con
        CmdNextCashInvoice.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            If TxtInvoiceNo.Text = "" Then
                CmdNextCashInvoice.CommandText = "Select CashInvoice From SysParam"
                TxtInvoiceNo.Text = CmdNextCashInvoice.ExecuteScalar
            End If
            LblVat.Text = "Vat " & Rdr("Vat") & "%"
        End If
        Rdr.Close()
        CmbTrafficOfficer.Focus()
    End Sub

    Protected Sub TxtQuantity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Row As GridViewRow = TryCast(DirectCast(sender, TextBox).Parent.Parent, GridViewRow)
        Row.Cells(8).Text = Val(DirectCast(Row.FindControl("TxtRate"), TextBox).Text) * Val(DirectCast(Row.FindControl("TxtQuantity"), TextBox).Text)
        On Error Resume Next
        Row.Cells(4).Focus()
        TotalGrid()
    End Sub

    Protected Sub TxtTimeIn_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Row As GridViewRow = TryCast(DirectCast(sender, TextBox).Parent.Parent, GridViewRow)
        On Error Resume Next
    End Sub

    Protected Sub TxtTimeOut_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Row As GridViewRow = TryCast(DirectCast(sender, TextBox).Parent.Parent, GridViewRow)
        On Error Resume Next
    End Sub

    Protected Sub TxtHrs_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Row As GridViewRow = TryCast(DirectCast(sender, TextBox).Parent.Parent, GridViewRow)
        On Error Resume Next
    End Sub

    Protected Sub TxtRate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Row As GridViewRow = TryCast(DirectCast(sender, TextBox).Parent.Parent, GridViewRow)
        Row.Cells(8).Text = Val(DirectCast(Row.FindControl("TxtRate"), TextBox).Text) * Val(DirectCast(Row.FindControl("TxtQuantity"), TextBox).Text)
        On Error Resume Next
        'GdvLog.Rows(Row.RowIndex + 1).Cells(2).Focus()
        TotalGrid()
    End Sub

    Private Sub TotalGrid()
        TxtTotal.Text = 0
        For Each Row As GridViewRow In GdvLog.Rows
            TxtTotal.Text = Val(TxtTotal.Text) + Val(Row.Cells(8).Text)
        Next
        TxtTotal.Text = Format(Val(TxtTotal.Text), "0.00")
        If ChkVatable.Checked Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT Vat FROM [System] WHERE WhCode = '" & CmbZone.Text & "'"
            Dim SngVat As Single = Cmd.ExecuteScalar
            TxtVat.Text = Format(Val(TxtTotal.Text) * SngVat / 100, "0.00")
        Else
            TxtVat.Text = Format(0, "0.00")
        End If
        TxtAmount.Text = Format(Val(TxtTotal.Text) + Val(TxtVat.Text), "0.00")
    End Sub

    Private Sub GdvLog_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLog.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim TxtQuantity As TextBox = DirectCast(e.Row.FindControl("TxtQuantity"), TextBox)
            Dim TxtRate As TextBox = DirectCast(e.Row.FindControl("TxtRate"), TextBox)
            TxtQuantity.Style("text-align") = "right"
            TxtRate.Style("text-align") = "right"
            If e.Row.RowIndex Mod 2 > 0 Then
                TxtQuantity.BackColor = GdvLog.AlternatingRowStyle.BackColor
                TxtRate.BackColor = GdvLog.AlternatingRowStyle.BackColor
            End If
        End If
    End Sub

    Private Sub GdvLog_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLog.SelectedIndexChanging
        GdvLog.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click

        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            'Check if this is an existing invoice
            Cmd.CommandText = "SELECT Count(*) FROM Invoice WHERE InvoiceNo = '" & TxtInvoiceNo.Text & "' AND WhCode = '" & CmbZone.Text & "'"
            If Cmd.ExecuteScalar > 0 Then
                StrSql = "UPDATE Invoice SET ClientID = '" & TxtClientID.Text & "',"
                StrSql = StrSql & "InvDate = '" & Format(CDate(TxtDate.Text), "MM/dd/yyyy") & "',"
                StrSql = StrSql & "Month = '" & Format(CDate(TxtDate.Text), "MMMM") & "',"
                StrSql = StrSql & "Year = " & Year(CDate(TxtDate.Text)) & ","
                StrSql = StrSql & "Amount = " & Val(TxtTotal.Text) & ","
                StrSql = StrSql & "Vat = " & Val(TxtVat.Text) & ","
                StrSql = StrSql & "Value = " & Val(TxtAmount.Text) & ","
                StrSql = StrSql & "BoeNo = '" & TxtBillNo.Text & "',"
                StrSql = StrSql & "Remarks = '" & TxtRemarks.Text & "',"
                StrSql = StrSql & "WhCode = '" & CmbZone.Text & "',"
                StrSql = StrSql & "Paid = 0,"
                StrSql = StrSql & "Deleted = 0 WHERE "
                StrSql = StrSql & "InvoiceNo = '" & TxtInvoiceNo.Text & "' AND WhCode = '" & CmbZone.Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            Else
                If Not ChkCredit.Checked Then
                    Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                    Cmd.CommandText = "SELECT CashInvoice From SysParam"
                    StrSql = "INSERT INTO Invoice (InvoiceNo,ClientID,InvDate,Month,Year,Amount,Vat,Value,BoeNo,Remarks,WhCode,Paid,Deleted) VALUES("
                    StrSql = StrSql & Cmd.ExecuteScalar & ",'"
                    StrSql = StrSql & TxtClientID.Text & "','"
                    StrSql = StrSql & Format(CDate(TxtDate.Text), "MM/dd/yyyy") & "','"
                    StrSql = StrSql & Format(CDate(TxtDate.Text), "MMMM") & "',"
                    StrSql = StrSql & Year(CDate(TxtDate.Text)) & ","
                    StrSql = StrSql & Val(TxtTotal.Text) & ","
                    StrSql = StrSql & Val(TxtVat.Text) & ","
                    StrSql = StrSql & Val(TxtAmount.Text) & ",'"
                    StrSql = StrSql & TxtBillNo.Text & "','"
                    StrSql = StrSql & TxtRemarks.Text & "','"
                    StrSql = StrSql & CmbZone.Text & "',"
                    StrSql = StrSql & "0,"
                    StrSql = StrSql & "0)"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                    Cmd.CommandText = "UPDATE [SysParam] SET CashInvoice = CashInvoice + 1"
                    Cmd.ExecuteNonQuery()
                End If
            End If
            'Delete any Logistics record if present
            If ChkCredit.Checked = True Then
                Cmd.CommandText = "DELETE FROM Logistics WHERE WhCode = '" & CmbZone.Text & "' AND RequestNo = '" & TxtReqNo.Text & "'"
            Else
                Cmd.CommandText = "DELETE FROM Logistics WHERE WhCode = '" & CmbZone.Text & "' AND InvoiceNo = '" & TxtInvoiceNo.Text & "'"
            End If
            Cmd.ExecuteNonQuery()
            'Now add details to logistics table
            For Each Row As GridViewRow In GdvLog.Rows
                If Row.RowType = DataControlRowType.DataRow Then
                    If Val(Row.Cells(8).Text) > 0 Then
                        StrSql = "INSERT INTO Logistics (ClientID,BoeNo,ChDate,ChCode,Description,Quantity,Unit,Rate,Amount,TrafficOfficer,WhCode,"
                        StrSql = StrSql & "InvoiceNo,RequestNo,Paid,TimeIn,TimeOut,Hrs) VALUES('"
                        StrSql = StrSql & TxtClientID.Text & "','"
                        StrSql = StrSql & TxtBillNo.Text & "','"
                        StrSql = StrSql & Format(CDate(TxtDate.Text), "MM/dd/yyyy") & "','"
                        StrSql = StrSql & Row.Cells(0).Text & "','"
                        StrSql = StrSql & Row.Cells(1).Text & "',"
                        StrSql = StrSql & Val(DirectCast(Row.FindControl("TxtQuantity"), TextBox).Text) & ",'"
                        StrSql = StrSql & Row.Cells(6).Text & "',"
                        StrSql = StrSql & Val(DirectCast(Row.FindControl("TxtRate"), TextBox).Text) & ","
                        StrSql = StrSql & Val(Row.Cells(8).Text) & ",'"
                        StrSql = StrSql & CmbTrafficOfficer.Text & "','"
                        StrSql = StrSql & CmbZone.Text & "',"
                        If ChkCredit.Checked Then
                            StrSql = StrSql & "0,"
                            StrSql = StrSql & Val(TxtReqNo.Text) & ","
                        Else
                            StrSql = StrSql & Val(TxtInvoiceNo.Text) & ","
                            StrSql = StrSql & "0,"
                        End If
                        StrSql = StrSql & "0,'"
                        StrSql = StrSql & DirectCast(Row.FindControl("TxtTimeIn"), TextBox).Text & "','"
                        StrSql = StrSql & DirectCast(Row.FindControl("TxtTimeOut"), TextBox).Text & "','"
                        StrSql = StrSql & DirectCast(Row.FindControl("TxtHrs"), TextBox).Text & "')"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If
                End If
            Next
            Trans.Commit()
            TxtBillNo.Text = ""
            TxtRevision.Text = ""
            TxtClientID.Text = ""
            TxtName.Text = ""
            CmbTrafficOfficer.SelectedIndex = 0
            TxtReqNo.Text = ""
            GdvLog.DataSource = Nothing
            GdvLog.DataBind()
            TxtRemarks.Text = ""
            TxtTotal.Text = ""
            TxtVat.Text = ""
            TxtAmount.Text = ""
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Save Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ClearAll()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        TxtDate.Text = Format(Date.Today, "dd-MMM-yyyy")
        TxtName.Text = ""
        TxtRemarks.Text = ""
        TxtRevision.Text = ""
        TxtTotal.Text = ""
        TxtVat.Text = ""
        TxtAmount.Text = ""
        GdvLog.DataSource = Nothing
        GdvLog.DataBind()
        'CmbZone.SelectedIndex = 0
        TxtClientID.Text = ""
        CmbTrafficOfficer.SelectedIndex = 0
    End Sub

    Protected Sub ButFindInv_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindInv.Click
        RbnClient.Text = "Name"
        RbnCont.Visible = False
        RbnCountry.Visible = False
        RbnTraffic.Visible = False
        ChkDate.Visible = True
        PnlFind.Style.Item("display") = ""
        TxtFrom.Visible = True
        TxtTo.Visible = True
        LblToDate.Visible = True
        GdvFind.DataSource = Nothing
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub GetInvoiceDetails()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        CmdInvoice.Connection = Con
        If ChkCredit.Checked = True Then
            StrSql = "Select *From Logistics Where RequestNo = " & TxtReqNo.Text & " And WhCode = '" & CmbZone.Text & "'"
            CmdInvoice.Connection = Con
            CmdInvoice.CommandText = StrSql
            RdrInvoice = CmdInvoice.ExecuteReader
            If RdrInvoice.Read Then
                StrSql = "SELECT ChCode,Description,Null As TimeIn, Null As TimeOut, Null As Hrs,0 AS Quantity,Unit,Rate,0 AS Amount "
                StrSql = StrSql & "FROM TariffCard WHERE ChCode IN (SELECT ChCode FROM ClientTariff "
                StrSql = StrSql & "WHERE ClientId ='" & TxtClientID.Text & "') And StorageContainer = 0"
                Cmd.Connection = Con
                Cmd.CommandText = StrSql
                Rdr = Cmd.ExecuteReader
                GdvLog.DataSource = Rdr
                GdvLog.DataBind()
                Rdr.Close()
                CmdChkLog.Connection = Con
                For Each Row As GridViewRow In GdvLog.Rows
                    CmdChkLog.CommandText = "Select *From Logistics Where WhCode  = '" & CmbZone.Text & "' And ChCode  = '" & Row.Cells(0).Text & "' And RequestNo = '" & TxtReqNo.Text & "'"
                    RdrChkLog = CmdChkLog.ExecuteReader
                    If RdrChkLog.Read Then
                        DirectCast(Row.FindControl("TxtQuantity"), TextBox).Text = Val(RdrChkLog("Quantity"))
                        DirectCast(Row.FindControl("TxtRate"), TextBox).Text = Val(RdrChkLog("Rate"))
                        Row.Cells(8).Text = Val(DirectCast(Row.FindControl("TxtRate"), TextBox).Text) * Val(DirectCast(Row.FindControl("TxtQuantity"), TextBox).Text)
                        DirectCast(Row.FindControl("TxtTimeIN"), TextBox).Text = RdrChkLog("TimeIn")
                        DirectCast(Row.FindControl("TxtTimeOUT"), TextBox).Text = RdrChkLog("TimeOUT")
                        DirectCast(Row.FindControl("TxtHrs"), TextBox).Text = RdrChkLog("Hrs")
                    End If
                    RdrChkLog.Close()
                Next
                TotalGrid()
            End If
            RdrInvoice.Close()
        Else
            StrSql = "SELECT Invoice.*,Client.Name,Logistics.TrafficOfficer,Logistics.RequestNo FROM Invoice JOIN Client ON Invoice.ClientID = Client.ClientID "
            StrSql = StrSql & " LEFT OUTER JOIN Logistics ON Invoice.InvoiceNo = Logistics.InvoiceNo WHERE Invoice.InvoiceNo = '" & TxtInvoiceNo.Text & "' "
            StrSql = StrSql & "AND Invoice.WhCode = '" & CmbZone.Text & "'"
            CmdInvoice.Connection = Con
            CmdInvoice.CommandText = StrSql
            RdrInvoice = CmdInvoice.ExecuteReader
            If RdrInvoice.Read Then
                TxtDate.Text = Format(RdrInvoice("InvDate"), "dd/MM/yyyy")
                TxtBillNo.Text = RdrInvoice("BoeNo").ToString
                TxtClientID.Text = RdrInvoice("ClientID").ToString
                TxtName.Text = RdrInvoice("Name").ToString
                TxtTotal.Text = RdrInvoice("Amount")
                TxtVat.Text = RdrInvoice("Vat")
                TxtAmount.Text = RdrInvoice("Value")
                TxtRemarks.Text = RdrInvoice("Remarks").ToString
                CmbTrafficOfficer.Text = RdrInvoice("TrafficOfficer").ToString
                TxtReqNo.Text = RdrInvoice("RequestNo").ToString

                StrSql = "SELECT ChCode,Description,NULL As TimeIn, Null As TimeOut, Null As Hrs,0 AS Quantity,Unit,Rate,0 AS Amount "
                StrSql = StrSql & "FROM TariffCard WHERE ChCode IN (SELECT ChCode FROM ClientTariff "
                StrSql = StrSql & "WHERE ClientId ='" & TxtClientID.Text & "') And StorageContainer = 0"
                Cmd.Connection = Con
                Cmd.CommandText = StrSql
                Rdr = Cmd.ExecuteReader
                GdvLog.DataSource = Rdr
                GdvLog.DataBind()
                Rdr.Close()
                CmdChkLog.Connection = Con
                For Each Row As GridViewRow In GdvLog.Rows
                    CmdChkLog.CommandText = "Select *From Logistics Where WhCode  = '" & CmbZone.Text & "' And ChCode  = '" & Row.Cells(0).Text & "' And InvoiceNo = '" & TxtInvoiceNo.Text & "'"
                    RdrChkLog = CmdChkLog.ExecuteReader
                    If RdrChkLog.Read Then
                        DirectCast(Row.FindControl("TxtQuantity"), TextBox).Text = Val(RdrChkLog("Quantity"))
                        DirectCast(Row.FindControl("TxtRate"), TextBox).Text = Val(RdrChkLog("Rate"))
                        Row.Cells(8).Text = Val(DirectCast(Row.FindControl("TxtRate"), TextBox).Text) * Val(DirectCast(Row.FindControl("TxtQuantity"), TextBox).Text)
                        DirectCast(Row.FindControl("TxtTimeIN"), TextBox).Text = RdrChkLog("TimeIn")
                        DirectCast(Row.FindControl("TxtTimeOUT"), TextBox).Text = RdrChkLog("TimeOUT")
                        DirectCast(Row.FindControl("TxtHrs"), TextBox).Text = RdrChkLog("Hrs")
                    End If
                    RdrChkLog.Close()
                Next
                TotalGrid()
            Else
                TxtBillNo.Text = ""
                TxtClientID.Text = ""
                TxtName.Text = "Invalid InvoiceNo"
            End If
            RdrInvoice.Close()
        End If
    End Sub


    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNew.Click
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT CashInvoice From SysParam"
        TxtInvoiceNo.Text = Cmd.ExecuteScalar
        TxtClientID.Text = ""
        TxtBillNo.Text = ""
        ClearAll()
        TxtBillNo.Focus()
    End Sub

    Protected Sub ChkVatable_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ChkVatable.CheckedChanged
        TotalGrid()
    End Sub

    Protected Sub TxtReqNo_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtReqNo.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        CmdReq.Connection = Con
        CmdReq.CommandText = "Select * From Logistics Where RequestNo = '" & TxtReqNo.Text & "' And WhCode = '" & CmbZone.Text & "'"
        RdrReq = CmdReq.ExecuteReader
        If RdrReq.Read Then
            ChkCredit.Checked = True
            TxtDate.Text = Format(RdrReq("ChDate"), "dd/MM/yyyy")
            TxtClientID.Text = RdrReq("ClientID")
            CmbTrafficOfficer.Text = RdrReq("TrafficOfficer")
            TxtClientID_TextChanged(Me, e)
            GetInvoiceDetails()
        End If
        RdrReq.Close()
    End Sub

    Protected Sub ButPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrint.Click
        StrSel = ""
        StrSel = "{Invoice.WhCode} = '" & CmbZone.Text & "' And {Invoice.InvoiceNo}"
        StrSel = StrSel & "=" & TxtInvoiceNo.Text
        Session("ReportFile") = "RptLogInvoice.rpt"
        Session("SelFormula") = StrSel
        Session("Period") = ""
        Session("Zone") = ""
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub

    Protected Sub ButDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDelete.Click
        Cmd.Connection = Con
        If ChkCredit.Checked = False Then
            Cmd.CommandText = "Delete From Invoice Where InvoiceNo = '" & TxtInvoiceNo.Text & "' And WhCode  = '" & CmbZone.Text & "'"
            Cmd.CommandText = "Delete From Logistics Where InvoiceNo = '" & TxtInvoiceNo.Text & "' And WhCode  = '" & CmbZone.Text & "'"
        Else
            Cmd.CommandText = "Delete From Logistics Where RequestNo  = '" & TxtReqNo.Text & "' And WhCode  = '" & CmbZone.Text & "'"
        End If

        Cmd.ExecuteNonQuery()
        ClearAll()
    End Sub

    Protected Sub TxtInvoiceNo_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtInvoiceNo.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        CmdReq.Connection = Con
        CmdReq.CommandText = "Select * From Logistics Where InvoiceNo = '" & TxtReqNo.Text & "' And WhCode = '" & CmbZone.Text & "'"
        RdrReq = CmdReq.ExecuteReader
        If RdrReq.Read Then
            ChkCredit.Checked = False
            TxtDate.Text = Format(RdrReq("ChDate"), "dd/MM/yyyy")
            TxtClientID.Text = RdrReq("ClientID")
            CmbTrafficOfficer.Text = RdrReq("TrafficOfficer")
            TxtClientID_TextChanged(Me, e)
            GetInvoiceDetails()
        End If
        RdrReq.Close()
    End Sub
End Class