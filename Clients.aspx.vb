﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Partial Public Class Clients
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdSave As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatCabin As New DataTable

    Protected Sub UpdateZone()
        'Update Zones Grid
        StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Dim da As New OleDb.OleDbDataAdapter(Cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvZones.DataSource = ds
        GdvZones.DataBind()
        da.Dispose()
        ds.Clear()
    End Sub

    Protected Sub UpdateCountry(ByVal ImpEx As String)
        StrSql = "SELECT Name FROM Country WHERE Name <> '' ORDER BY Name"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Dim da As New OleDb.OleDbDataAdapter(Cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        If ImpEx = "Import" Then
            GdvImport.DataSource = ds
            GdvImport.DataBind()
        Else
            GdvExport.DataSource = ds
            GdvExport.DataBind()
        End If
        da.Dispose()
        ds.Clear()
    End Sub

    Protected Sub UpdateActivity()
        Cmd.Connection = Con
        StrSql = "SELECT Activity FROM Activity"
        Cmd.CommandText = StrSql
        Dim da = New OleDb.OleDbDataAdapter(Cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvActivity.DataSource = ds
        GdvActivity.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("Default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            StrSql = "SELECT DISTINCT(Category) FROM Regime ORDER BY Category"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbCategory.Items.Clear()
            CmbCategory.Items.Add("Select ...")
            While Rdr.Read
                CmbCategory.Items.Add(Rdr("Category"))
            End While
            CmbCategory.SelectedIndex = -1
            Rdr.Close()
            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Clear()
            CmbZone.Items.Add("Select ...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            CmbZone.SelectedIndex = -1
            Rdr.Close()
            CmbStatus.Items.Clear()
            CmbStatus.Items.Add("Select ...")
            CmbStatus.Items.Add("Operational")
            CmbStatus.Items.Add("Non Operational")
            CmbStatus.Items.Add("Dormant")
            CmbStatus.SelectedIndex = -1
            UpdateZone()
            UpdateCountry("Import")
            UpdateCountry("Export")
            UpdateActivity()
            TabContainer1.ActiveTabIndex = 0
        End If
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButNormal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNormal.Click
        StrSql = "SELECT WhCode,ChCode,AnalysisCode,Description,Unit,Normal Charge FROM Charges ORDER BY WhCode, ChCode"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvLogistic.DataSource = ds
        GdvLogistic.DataBind()
    End Sub

    Private Sub ClearAll()
        On Error Resume Next
        CmbCategory.SelectedIndex = -1
        TxtName.Text = ""
        TxtAddress.Text = ""
        TxtCity.Text = ""
        TxtTel.Text = ""
        TxtFaxNo.Text = ""
        TxtEmail.Text = ""
        TxtContact.Text = ""
        TxtVatNo.Text = ""
        TxtPosition.Text = ""
        TxtDate.Text = Date.Today
        TxtRemarks.Text = ""
        TxtLicExp.Text = ""
        TxtPermitExp.Text = ""
        TxtLicenceNo.Text = ""
        TxtStoragePermit.Text = ""
        CmbStatus.SelectedIndex = -1
        TxtProType.Text = ""
        'TxtClientBRN = ""
        TxtDateofOp.Text = ""
        TxtPallet.Text = ""
        TxtVehicles.Text = ""
        TxtVehicleDays.Text = ""
        TxtCont20.Text = ""
        TxtCont40.Text = ""
        TxtDiscount.Text = ""
        For Each row As GridViewRow In GdvImport.Rows
            Dim ChkMkt As CheckBox = DirectCast(row.Cells(0).FindControl("ChkImport"), CheckBox)
            ChkMkt.Checked = False
        Next
        For Each row As GridViewRow In GdvExport.Rows
            Dim ChkMkt As CheckBox = DirectCast(row.Cells(0).FindControl("ChkExport"), CheckBox)
            ChkMkt.Checked = False
        Next
    End Sub

    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNew.Click
        ClearAll()
        TabContainer1.ActiveTabIndex = 0
        TxtClientID.Focus()
    End Sub

    Protected Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            ClearAll()
            CmbCategory.Text = Trim(Rdr("Category"))
            TxtClientID.Text = Rdr("ClientID")
            TxtName.Text = Rdr("Name") & ""
            TxtAddress.Text = Rdr("Address") & ""
            TxtCity.Text = Rdr("City") & ""
            TxtTel.Text = Rdr("Telephone") & ""
            TxtFaxNo.Text = Rdr("Fax") & ""
            TxtEmail.Text = Rdr("EMail")
            TxtContact.Text = Rdr("Contact") & ""
            TxtPosition.Text = Rdr("Position") & ""
            TxtDate.Text = Rdr("DateCreated") & ""
            'TxtCapacity.Text = Rdr("Capacity") & ""
            TxtRemarks.Text = Rdr("Remarks") & ""
            TxtVatNo.Text = Rdr("VATNo") & ""
            TxtLicenceNo.Text = Rdr("LicenceNo") & ""
            TxtStoragePermit.Text = Rdr("StoragePermit") & ""
            TxtLicExp.Text = Rdr("LicenceExp").ToString
            TxtPermitExp.Text = Rdr("PermitExp") & ""
            CmbStatus.Text = Rdr("Status").ToString
            TxtProType.Text = Rdr("ProdType") & ""
            TxtDateofOp.Text = Rdr("FirstDate").ToString
            TxtPallet.Text = Rdr("PalletCharge").ToString
            TxtVehicles.Text = Rdr("VehCharge").ToString
            TxtVehicleDays.Text = Rdr("UnitDays").ToString
            TxtCont20.Text = Rdr("Cont20") & ""
            TxtCont40.Text = Rdr("Cont40") & ""
            'TxtClientBRN = Rdr("ClientBRN")
            TxtDiscount.Text = Rdr("Discount").ToString
            'Update zones operated
            For Each row As GridViewRow In GdvZones.Rows
                If row.RowType = DataControlRowType.DataRow Then
                    Dim ChkZone As CheckBox = DirectCast(row.Cells(0).FindControl("chkZone"), CheckBox)
                    If InStr(Rdr("WHouses").ToString, row.Cells(1).Text) > 0 Then
                        ChkZone.Checked = True
                    Else
                        ChkZone.Checked = False
                    End If
                End If
            Next
            Rdr.Close()
            UpdateCharges()
            UpdateCabins()
            UpdateMarket()
        Else
            ClearAll()
        End If
    End Sub

    Protected Sub UpdateMarket()
        StrSql = "SELECT * FROM Markets WHERE ClientID = '" & TxtClientID.Text & "' ORDER BY Market"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        While Rdr.Read
            If Rdr("Type") = "Import" Then
                For Each row As GridViewRow In GdvImport.Rows
                    If row.Cells(1).Text = Rdr("Market") Then
                        Dim ChkMkt As CheckBox = DirectCast(row.Cells(0).FindControl("ChkImport"), CheckBox)
                        ChkMkt.Checked = True
                    End If
                Next
            Else
                For Each row As GridViewRow In GdvExport.Rows
                    If row.Cells(1).Text = Rdr("Market") Then
                        Dim ChkMkt As CheckBox = DirectCast(row.Cells(0).FindControl("ChkExport"), CheckBox)
                        ChkMkt.Checked = True
                    End If
                Next
            End If
        End While
        Rdr.Close()
    End Sub

    Protected Sub UpdateCabins()
        StrSql = "SELECT WhCode,CabinNo,Capacity,Unit FROM ClientCabin WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        DatCabin.Clear()
        DatCabin.Load(Rdr)
        Rdr.Close()
        GdvCabins.DataSource = DatCabin
        GdvCabins.DataBind()
    End Sub

    Protected Sub UpdateCharges()
        StrSql = "SELECT WhCode,ChCode,AnalysisCode,Description,Unit,Charge FROM ClientCharges WHERE ClientID = '"
        StrSql = StrSql & TxtClientID.Text & "' ORDER BY WhCode, ChCode"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvLogistic.DataSource = ds
        GdvLogistic.DataBind()
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
        StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID > '" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevious.Click
        StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID < '" & TxtClientID.Text & "' ORDER BY ClientID DESC"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButPreferential_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPreferential.Click
        StrSql = "SELECT WhCode,ChCode,AnalysisCode,Description,Unit,Pref Charge FROM Charges ORDER BY WhCode, ChCode"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvLogistic.DataSource = ds
        GdvLogistic.DataBind()
    End Sub

    Private Sub GdvImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvImport.PageIndexChanging
        UpdateCountry("Import")
        GdvImport.PageIndex = e.NewPageIndex
    End Sub

    Private Sub GdvExport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvExport.PageIndexChanging
        UpdateCountry("Export")
        GdvExport.PageIndex = e.NewPageIndex
    End Sub

    Private Sub GdvActivity_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvActivity.PageIndexChanging
        UpdateActivity()
        GdvActivity.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub ButFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFind.Click
        PnlFind.Height = 400
        PnlFind.Width = 600
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
        'Dim strScript As String = "window.open('Default.aspx', 'Key', 'height=500,width=800,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,titlebar=no');"
        'Dim strScript As String = "window.showModalDialog('Default.aspx', 'Key', 'height=500,width=800,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,titlebar=no');"
        'Dim strScript As String = "window.showModalDialog('Default.aspx','','height=500,width=800,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,titlebar=no')"
        'ScriptManager.RegisterStartupScript(Me, Type.GetType("System.String"), "", strScript, True)
    End Sub

    Protected Sub ButSelect_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Protected Sub ButRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
        If RbtName.Checked Then
            StrSql = StrSql & "Name"
        ElseIf RbtCity.Checked Then
            StrSql = StrSql & "City"
        Else
            StrSql = StrSql & "Contact"
        End If
        StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvLogistic.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLogistic.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To GdvCabins.Rows.Count - 1
            ClientScript.RegisterForEventValidation(GdvCabins.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtClientID_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub GdvLogistic_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLogistic.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim TxtCharge As TextBox = DirectCast(e.Row.FindControl("Charge"), TextBox)
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = "document.getElementById('" & TxtCharge.ClientID & "').select();"
            If e.Row.RowIndex Mod 2 > 0 Then
                TxtCharge.BackColor = GdvLogistic.AlternatingRowStyle.BackColor
            End If
        End If
    End Sub

    Private Sub GdvLogistic_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLogistic.SelectedIndexChanging
        GdvLogistic.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        System.Threading.Thread.Sleep(2000)
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Cmd.Connection = Con
        StrSql = "SELECT COUNT(ClientID) FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Dim StrZones As String = ""
        For Each row As GridViewRow In GdvZones.Rows
            If row.RowType = DataControlRowType.DataRow Then
                If DirectCast(row.FindControl("chkZone"), CheckBox).Checked Then
                    If StrZones <> "" Then StrZones = StrZones & ", "
                    StrZones = StrZones & row.Cells(1).Text
                End If
            End If
        Next
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Cmd.ExecuteScalar = 0 Then
            StrSql = "INSERT INTO Client (ClientID,Category,Name,Address,City,Telephone,Fax,EMail,Contact,Position,DateCreated,"
            StrSql = StrSql & "VATNo,LicenceNo,LicenceExp,StoragePermit,PermitExp,Remarks,Status,ProdType,PalletCharge,VehCharge,"
            StrSql = StrSql & "UnitDays,Cont20,Cont40,Discount,FirstDate,WHouses) VALUES('"
            StrSql = StrSql & TxtClientID.Text & "','"
            StrSql = StrSql & CmbCategory.Text & "','"
            StrSql = StrSql & TxtName.Text & "','"
            StrSql = StrSql & TxtAddress.Text & "','"
            StrSql = StrSql & TxtCity.Text & "','"
            StrSql = StrSql & TxtTel.Text & "','"
            StrSql = StrSql & TxtFaxNo.Text & "','"
            StrSql = StrSql & TxtEmail.Text & "','"
            StrSql = StrSql & TxtContact.Text & "','"
            StrSql = StrSql & TxtPosition.Text & "',"
            If TxtDate.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtDate.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & TxtVatNo.Text & "','"
            StrSql = StrSql & TxtLicenceNo.Text & "',"
            If TxtLicExp.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtLicExp.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & TxtStoragePermit.Text & "',"
            If TxtPermitExp.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtPermitExp.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & TxtRemarks.Text & "','"
            StrSql = StrSql & CmbStatus.Text & "','"
            StrSql = StrSql & TxtProType.Text & "',"
            StrSql = StrSql & Val(TxtPallet.Text) & ","
            StrSql = StrSql & Val(TxtVehicles.Text) & ","
            StrSql = StrSql & Val(TxtVehicleDays.Text) & ","
            StrSql = StrSql & Val(TxtCont20.Text) & ","
            StrSql = StrSql & Val(TxtCont40.Text) & ","
            StrSql = StrSql & Val(TxtDiscount.Text) & ","
            If TxtDateofOp.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtDateofOp.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & StrZones & "')"
            ErrHead.Text = "Insert record Failed"
        Else
            StrSql = "UPDATE Client SET "
            StrSql = StrSql & "ClientID = '" & TxtClientID.Text & "',"
            StrSql = StrSql & "Category = '" & CmbCategory.Text & "',"
            StrSql = StrSql & "Name = '" & TxtName.Text & "',"
            StrSql = StrSql & "Address = '" & TxtAddress.Text & "',"
            StrSql = StrSql & "City = '" & TxtCity.Text & "',"
            StrSql = StrSql & "Telephone = '" & TxtTel.Text & "',"
            StrSql = StrSql & "Fax = '" & TxtFaxNo.Text & "',"
            StrSql = StrSql & "EMail = '" & TxtEmail.Text & "',"
            StrSql = StrSql & "Contact = '" & TxtContact.Text & "',"
            StrSql = StrSql & "Position = '" & TxtPosition.Text & "',"
            If TxtDate.Text = "" Then
                StrSql = StrSql & "DateCreated = NULL,"
            Else
                StrSql = StrSql & "DateCreated = '" & CDate(TxtDate.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "VATNo = '" & TxtVatNo.Text & "',"
            StrSql = StrSql & "LicenceNo = '" & TxtLicenceNo.Text & "',"
            If TxtLicExp.Text = "" Then
                StrSql = StrSql & "LicenceExp = NULL,"
            Else
                StrSql = StrSql & "LicenceExp = '" & CDate(TxtLicExp.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "StoragePermit = '" & TxtStoragePermit.Text & "',"
            If TxtPermitExp.Text = "" Then
                StrSql = StrSql & "PermitExp = NULL,"
            Else
                StrSql = StrSql & "PermitExp = '" & CDate(TxtPermitExp.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "Remarks = '" & TxtRemarks.Text & "',"
            StrSql = StrSql & "Status = '" & CmbStatus.Text & "',"
            StrSql = StrSql & "ProdType = '" & TxtProType.Text & "',"
            StrSql = StrSql & "PalletCharge = " & Val(TxtPallet.Text) & ","
            StrSql = StrSql & "VehCharge = " & Val(TxtVehicles.Text) & ","
            StrSql = StrSql & "UnitDays = " & Val(TxtVehicleDays.Text) & ","
            StrSql = StrSql & "Cont20 = " & Val(TxtCont20.Text) & ","
            StrSql = StrSql & "Cont40 = " & Val(TxtCont40.Text) & ","
            StrSql = StrSql & "Discount = " & Val(TxtDiscount.Text) & ","
            If TxtDateofOp.Text = "" Then
                StrSql = StrSql & "FirstDate = NULL,"
            Else
                StrSql = StrSql & "FirstDate = '" & CDate(TxtDateofOp.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "WHouses = '" & StrZones & "'"
            StrSql = StrSql & " WHERE ClientID = '" & TxtClientID.Text & "'"
            ErrHead.Text = "Update Failed"
        End If
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Transaction = Trans
        Cmd.CommandText = StrSql
        Try
            Cmd.ExecuteNonQuery()

            Trans.Commit()
        Catch ex As Exception
            Trans.Rollback()
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButAddCabin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAddCabin.Click
        Dim DatColumn As DataColumn
        Dim Dtrow As DataRow
        DatCabin.Clear()
        DatColumn = New DataColumn()
        DatColumn.DataType = Type.GetType("System.String")
        DatColumn.ColumnName = "WhCode"
        DatCabin.Columns.Add(DatColumn)
        DatColumn = New DataColumn()
        DatColumn.DataType = Type.GetType("System.String")
        DatColumn.ColumnName = "CabinNo"
        DatCabin.Columns.Add(DatColumn)
        DatColumn = New DataColumn()
        DatColumn.DataType = Type.GetType("System.String")
        DatColumn.ColumnName = "Capacity"
        DatCabin.Columns.Add(DatColumn)
        DatColumn = New DataColumn()
        DatColumn.DataType = Type.GetType("System.String")
        DatColumn.ColumnName = "Unit"
        DatCabin.Columns.Add(DatColumn)
        For Each row As GridViewRow In GdvCabins.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dtrow = DatCabin.NewRow
                Dtrow.Item("WhCode") = DirectCast(row.FindControl("Zone"), TextBox).Text
                Dtrow.Item("CabinNo") = DirectCast(row.FindControl("CabinNo"), TextBox).Text
                Dtrow.Item("Capacity") = DirectCast(row.FindControl("Capacity"), TextBox).Text
                Dtrow.Item("Unit") = DirectCast(row.FindControl("Unit"), TextBox).Text
                DatCabin.Rows.Add(Dtrow)
            End If
        Next
        Dtrow = DatCabin.NewRow
        Dtrow.Item("WhCode") = ""
        Dtrow.Item("CabinNo") = ""
        Dtrow.Item("Capacity") = ""
        Dtrow.Item("Unit") = ""
        DatCabin.Rows.Add(Dtrow)
        GdvCabins.DataSource = DatCabin
        GdvCabins.DataBind()
        GdvCabins.Rows(GdvCabins.Rows.Count - 1).FindControl("Zone").Focus()
    End Sub

    Private Sub ButDelCabin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelCabin.Click
        Dim DatColumn As DataColumn
        Dim Dtrow As DataRow
        DatCabin.Clear()
        DatColumn = New DataColumn()
        DatColumn.DataType = Type.GetType("System.String")
        DatColumn.ColumnName = "WhCode"
        DatCabin.Columns.Add(DatColumn)
        DatColumn = New DataColumn()
        DatColumn.DataType = Type.GetType("System.String")
        DatColumn.ColumnName = "CabinNo"
        DatCabin.Columns.Add(DatColumn)
        DatColumn = New DataColumn()
        DatColumn.DataType = Type.GetType("System.String")
        DatColumn.ColumnName = "Capacity"
        DatCabin.Columns.Add(DatColumn)
        DatColumn = New DataColumn()
        DatColumn.DataType = Type.GetType("System.String")
        DatColumn.ColumnName = "Unit"
        DatCabin.Columns.Add(DatColumn)
        For Each row As GridViewRow In GdvCabins.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim ChkDel As CheckBox = DirectCast(row.FindControl("ChkSelect"), CheckBox)
                If Not ChkDel.Checked Then
                    Dtrow = DatCabin.NewRow
                    Dtrow.Item("WhCode") = DirectCast(row.FindControl("Zone"), TextBox).Text
                    Dtrow.Item("CabinNo") = DirectCast(row.FindControl("CabinNo"), TextBox).Text
                    Dtrow.Item("Capacity") = DirectCast(row.FindControl("Capacity"), TextBox).Text
                    Dtrow.Item("Unit") = DirectCast(row.FindControl("Unit"), TextBox).Text
                    DatCabin.Rows.Add(Dtrow)
                End If
            End If
        Next
        GdvCabins.DataSource = DatCabin
        GdvCabins.DataBind()
    End Sub

    Private Sub GdvCabins_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvCabins.SelectedIndexChanged
        GdvCabins.SelectedRow.FindControl("Unit").Focus()
    End Sub

    Private Sub GdvCabins_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvCabins.SelectedIndexChanging
        GdvCabins.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub
End Class