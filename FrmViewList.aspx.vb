﻿Public Partial Class FrmViewList
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim StrSql As String
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            LblHead.Text = Session("VLHead")
            StrSql = Session("ListSql")
            GdvClient.DataBind()
        End If
    End Sub

    Protected Sub GdvClient_DataBinding(sender As Object, e As EventArgs)
        Dim Adapter As New OleDb.OleDbDataAdapter(StrSql, Con)
        Dim dataSet11 As New DataSet()
        'Create DataTable objects for representing database's tables
        Adapter.Fill(dataSet11, "ClientList")
        'Bind the grid control to the data source
        GdvClient.DataSource = dataSet11.Tables("ClientList")
        'Dim da As New OleDb.OleDbDataAdapter
        'da.SelectCommand.Connection = Con
        'da.SelectCommand.CommandText = StrSql
        'da.SelectCommand.CommandTimeout = 0
        'Using Ds As New DataSet
        '    da.Fill(Ds)
        '    GdvClient.DataSource = Ds.Tables(0)
        'End Using
    End Sub

    'Protected Sub Print_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Print.Click
    '    Cmd.Connection = Con
    '    If Session("VLHead") = "List of Cabins Occupied Currently" Then
    '        Cmd.CommandText = "Delete From TempOccWh"
    '        Cmd.ExecuteNonQuery()
    '        For Each Row As GridViewRow In GdvList.Rows
    '            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
    '            StrSql = "INSERT INTO TempOccWh (CompanyName,Category,Area,PropertyCode,[Type],LeaseStart,LeaseEnd,DateMoveIn,Remarks) VALUES('"
    '            StrSql = StrSql & Row.Cells(0).Text.Replace("&nbsp;", "") & "',"
    '            StrSql = StrSql & "'" & Row.Cells(1).Text.Replace("&nbsp;", "") & "',"
    '            StrSql = StrSql & "" & Row.Cells(2).Text.Replace("&nbsp;", "") & ","
    '            StrSql = StrSql & "'" & Row.Cells(3).Text.Replace("&nbsp;", "") & "',"
    '            StrSql = StrSql & "'" & Row.Cells(4).Text.Replace("&nbsp;", "") & "',"
    '            If Row.Cells(5).Text <> "" Then
    '                StrSql = StrSql & "'" & Format(CDate(Row.Cells(5).Text), "MM/dd/yyyy") & "',"
    '            Else
    '                StrSql = StrSql & "NULL,"
    '            End If
    '            If Row.Cells(6).Text <> "" Then
    '                StrSql = StrSql & "'" & Format(CDate(Row.Cells(6).Text), "MM/dd/yyyy") & "',"
    '            Else
    '                StrSql = StrSql & "NULL,"
    '            End If
    '            If Row.Cells(7).Text <> "" Then
    '                StrSql = StrSql & "'" & Format(CDate(Row.Cells(7).Text), "MM/dd/yyyy") & "',"
    '            Else
    '                StrSql = StrSql & "NULL,"
    '            End If
    '            StrSql = StrSql & "'" & Row.Cells(8).Text.Replace("&nbsp;", "") & "')"
    '            Cmd.CommandText = StrSql
    '            Cmd.ExecuteNonQuery()
    '        Next
    '        Dim StrSelFormula As String = ""
    '        Session("ReportFile") = "RptOccupiedWh.rpt"
    '        Session("Period") = ""
    '        Session("Zone") = ""
    '        Session("SelFormula") = StrSelFormula
    '        Session("RepHead") = "List of Cabins Occupied Currently"
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    '    ElseIf Session("VLHead") = "List of Cabins Lease Expired Currently" Then
    '        Cmd.CommandText = "Delete From TempExpWh"
    '        Cmd.ExecuteNonQuery()
    '        For Each Row As GridViewRow In GdvList.Rows
    '            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
    '            StrSql = "INSERT INTO TempExpWh (CompanyName,Category,Area,PropertyCode,[Type],LeaseStart,LeaseEnd,DateMoveIn,Remarks) VALUES('"
    '            StrSql = StrSql & Row.Cells(0).Text.Replace("&nbsp;", "") & "',"
    '            StrSql = StrSql & "'" & Row.Cells(1).Text.Replace("&nbsp;", "") & "',"
    '            StrSql = StrSql & "" & Row.Cells(2).Text.Replace("&nbsp;", "") & ","
    '            StrSql = StrSql & "'" & Row.Cells(3).Text.Replace("&nbsp;", "") & "',"
    '            StrSql = StrSql & "'" & Row.Cells(4).Text.Replace("&nbsp;", "") & "',"
    '            If Row.Cells(5).Text <> "" Then
    '                StrSql = StrSql & "'" & Format(CDate(Row.Cells(5).Text), "MM/dd/yyyy") & "',"
    '            Else
    '                StrSql = StrSql & "NULL,"
    '            End If
    '            If Row.Cells(6).Text <> "" Then
    '                StrSql = StrSql & "'" & Format(CDate(Row.Cells(6).Text), "MM/dd/yyyy") & "',"
    '            Else
    '                StrSql = StrSql & "NULL,"
    '            End If
    '            If Row.Cells(7).Text <> "" Then
    '                StrSql = StrSql & "'" & Format(CDate(Row.Cells(7).Text), "MM/dd/yyyy") & "',"
    '            Else
    '                StrSql = StrSql & "NULL,"
    '            End If
    '            StrSql = StrSql & "'" & Row.Cells(8).Text.Replace("&nbsp;", "") & "')"
    '            Cmd.CommandText = StrSql
    '            Cmd.ExecuteNonQuery()
    '        Next
    '        Dim StrSelFormula As String = ""
    '        Session("ReportFile") = "RptExpiredWh.rpt"
    '        Session("Period") = ""
    '        Session("Zone") = ""
    '        Session("SelFormula") = StrSelFormula
    '        Session("RepHead") = "List of Cabins Lease Expired Currently"
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    '    ElseIf Session("VLHead") = "List of Cabins Vacant Currently" Then
    '        Cmd.CommandText = "Delete From TempVacWh"
    '        Cmd.ExecuteNonQuery()
    '        For Each Row As GridViewRow In GdvList.Rows
    '            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
    '            StrSql = "INSERT INTO TempVacWh (PropertyCode,Area,[Type],[Name],Remarks) VALUES('"
    '            StrSql = StrSql & Row.Cells(0).Text.Replace("&nbsp;", "") & "',"
    '            StrSql = StrSql & "" & Row.Cells(1).Text.Replace("&nbsp;", "") & ","
    '            StrSql = StrSql & "'" & Row.Cells(2).Text.Replace("&nbsp;", "") & "',"
    '            StrSql = StrSql & "'" & Row.Cells(3).Text.Replace("&nbsp;", "") & "',"
    '            StrSql = StrSql & "'" & Row.Cells(4).Text.Replace("&nbsp;", "") & "')"
    '            Cmd.CommandText = StrSql
    '            Cmd.ExecuteNonQuery()
    '        Next
    '        Dim StrSelFormula As String = ""
    '        Session("ReportFile") = "RptVacantWh.rpt"
    '        Session("Period") = ""
    '        Session("Zone") = ""
    '        Session("SelFormula") = StrSelFormula
    '        Session("RepHead") = "List of Cabins Vacant Currently"
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    '    End If
    'End Sub
End Class