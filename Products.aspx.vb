﻿Public Partial Class Products
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT UnitCode FROM Unit"
            Rdr = Cmd.ExecuteReader
            CmbUnit.Items.Add("Select...")
            While Rdr.Read
                CmbUnit.Items.Add(Rdr("UnitCode"))
            End While
            Rdr.Close()
            Cmd.CommandText = "SELECT UnitCode FROM SubUnit"
            Rdr = Cmd.ExecuteReader
            CmbSubUnit.Items.Add("Select...")
            While Rdr.Read
                CmbSubUnit.Items.Add(Rdr("UnitCode"))
            End While
            Rdr.Close()
            Cmd.CommandText = "SELECT ZoneCode FROM Zones"
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("ZoneCode"))
            End While
            Rdr.Close()
            TxtProdCode.Focus()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        TxtProdCode.Text = ""
        ClearAll()
    End Sub

    Private Sub ClearAll()
        TxtDescription.Text = ""
        TxtHSCode.Text = ""
        TxtLevelFrom.Text = ""
        TxtLevelTo.Text = ""
        TxtName.Text = ""
        TxtPalletRatio.Text = ""
        TxtQtyPerPallet.Text = ""
        TxtRemarks.Text = ""
        CmbSubUnit.SelectedIndex = 0
        CmbUnit.SelectedIndex = 0
        CmbZone.SelectedIndex = 0
        ChkStoreonRack.Checked = False
        ChkVehicle.Checked = False
    End Sub

    Private Sub ButExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Try
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Product WHERE ProdCode = '" & TxtProdCode.Text & "'"
            Cmd.ExecuteNonQuery()
            ButNew_Click(Me, e)
        Catch ex As Exception
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub TxtProdCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtProdCode.TextChanged
        Cmd.Connection = Con
        StrSql = "SELECT Product.*, ProdCategory.Category FROM Product LEFT OUTER JOIN ProdCategory ON Product.HSCode = ProdCategory.HSCode "
        StrSql = StrSql & "WHERE Product.ProdCode = '" & TxtProdCode.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            On Error Resume Next
            TxtDescription.Text = Rdr("Category").ToString
            TxtHSCode.Text = Rdr("HSCode").ToString
            TxtLevelFrom.Text = Rdr("LevelFrom").ToString
            TxtLevelTo.Text = Rdr("LevelTo").ToString
            TxtName.Text = Rdr("Description").ToString
            TxtPalletRatio.Text = Rdr("PRatio").ToString
            TxtQtyPerPallet.Text = Rdr("QtyPallet").ToString
            TxtRemarks.Text = Rdr("Remarks").ToString
            CmbSubUnit.Text = Rdr("SubUnit").ToString
            CmbUnit.Text = Rdr("Unit").ToString
            CmbZone.Text = Rdr("Zone").ToString
            ChkStoreonRack.Checked = Rdr("RackOk").ToString
            If Rdr("PRatio") = 10 Then
                ChkVehicle.Checked = True
            Else
                ChkVehicle.Checked = False
            End If
        End If
        Rdr.Close()
    End Sub

    Private Sub ButNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNext.Click
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT TOP 1 ProdCode FROM Product WHERE ProdCode > '" & TxtProdCode.Text & "'"
        Try
            TxtProdCode.Text = Cmd.ExecuteScalar.ToString
            TxtProdCode_TextChanged(Me, e)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ButPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPrevious.Click
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT TOP 1 ProdCode FROM Product WHERE ProdCode < '" & TxtProdCode.Text & "' ORDER BY ProdCode DESC"
        Try
            TxtProdCode.Text = Cmd.ExecuteScalar.ToString
            TxtProdCode_TextChanged(Me, e)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT ProdCode FROM Product WHERE ProdCode = '" & TxtProdCode.Text & "'"
        If Cmd.ExecuteScalar Is Nothing Then
            StrSql = "INSERT INTO Product (ProdCode,Description,HSCode,Unit,SubUnit,QtyPallet,PRatio,LevelFrom,LevelTo,RackOk,Zone,Remarks) "
            StrSql = StrSql & "VALUES('" & TxtProdCode.Text & "','"
            StrSql = StrSql & TxtName.Text & "','"
            StrSql = StrSql & TxtHSCode.Text & "','"
            StrSql = StrSql & CmbUnit.Text & "','"
            StrSql = StrSql & IIf(CmbSubUnit.Text = "Select...", "", CmbSubUnit.Text) & "',"
            StrSql = StrSql & Val(TxtQtyPerPallet.Text) & ","
            If ChkVehicle.Checked Then
                StrSql = StrSql & 10 & ","
            Else
                StrSql = StrSql & Val(TxtPalletRatio.Text) & ","
            End If
            StrSql = StrSql & Val(TxtLevelFrom.Text) & ","
            StrSql = StrSql & Val(TxtLevelTo.Text) & ","
            StrSql = StrSql & IIf(ChkStoreonRack.Checked, 1, 0) & ",'"
            StrSql = StrSql & IIf(CmbZone.Text = "Select...", "", CmbZone.Text) & "','"
            StrSql = StrSql & TxtRemarks.Text & "')"
            ErrHead.Text = "Add Product Failed"
        Else
            StrSql = "UPDATE Product SET ProdCode = '" & TxtProdCode.Text & "',"
            StrSql = StrSql & "Description='" & TxtDescription.Text & "',"
            StrSql = StrSql & "HSCode='" & TxtHSCode.Text & "',"
            StrSql = StrSql & "Unit='" & CmbUnit.Text & "',"
            StrSql = StrSql & "SubUnit='" & IIf(CmbSubUnit.Text = "Select...", "", CmbSubUnit.Text) & "',"
            StrSql = StrSql & "QtyPallet=" & Val(TxtQtyPerPallet.Text) & ","
            If ChkVehicle.Checked Then
                StrSql = StrSql & "PRatio=" & 10 & ","
            Else
                StrSql = StrSql & "PRatio=" & Val(TxtPalletRatio.Text) & ","
            End If
            StrSql = StrSql & "LevelFrom=" & Val(TxtLevelFrom.Text) & ","
            StrSql = StrSql & "LevelTo=" & Val(TxtLevelTo.Text) & ","
            StrSql = StrSql & "RackOk=" & IIf(ChkStoreonRack.Checked, 1, 0) & ","
            StrSql = StrSql & "Zone='" & IIf(CmbZone.Text = "Select...", "", CmbZone.Text) & "',"
            StrSql = StrSql & "Remarks='" & TxtRemarks.Text & "' WHERE ProdCode = '" & TxtProdCode.Text & "'"
            ErrHead.Text = "Upate Product Failed"
        End If
        Try
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            ButNew_Click(Me, e)
        Catch ex As Exception
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ProdCode,Description,HsCode,Unit,SubUnit FROM Product WHERE "
        StrSql = StrSql & "Description LIKE '%" + TxtSearch.Text + "%' ORDER BY ProdCode"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        PnlFind.Visible = True
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Visible = False
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtProdCode.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtProdCode_TextChanged(Me, e)
        PnlFind.Visible = False
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub
End Class