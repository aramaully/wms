﻿Imports System.Web.Security
Imports System.Data
Imports System.Data.OleDb
Imports System.IO

Imports System.Configuration
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web.CrystalReportViewer
Imports CrystalDecisions.Shared
Imports System.Drawing.Printing

Partial Public Class ListZones
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdName As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim RdrName As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrSel As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbWhCode.Items.Add("Select...")
            CmbWhCode.Items.Add("ALL")
            While Rdr.Read
                CmbWhCode.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Private Sub CmbWhCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbWhCode.SelectedIndexChanged
        CmdName.CommandText = "SELECT [Name] FROM WareHouse WHERE WhCode='" & CmbWhCode.Text & "'"
        CmdName.Connection = Con
        RdrName = CmdName.ExecuteReader
        TxtName.Text = ""
        If RdrName.Read Then
            TxtName.Text = RdrName("Name")
        End If
        RdrName.Close()
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
       
        Dim Report As New ReportDocument
        Dim StrSelFormula As String = ""
        ' Report.Load(Server.MapPath("ZonesList.rpt"))
        Report.FileName = "ZonesList.rpt"

        SetTableLocation(Report.Database.Tables)
        ' CrystalReportSource1.ReportDocument = Report
        CrystalReportViewer1.ReportSource = Report
        CrystalReportViewer1.PrintMode = CrystalDecisions.Web.PrintMode.ActiveX
        If CmbWhCode.Text <> "ALL" Then
            StrSelFormula = "{WareHouse.WhCode}='" & CmbWhCode.Text & "'"
        Else
            StrSelFormula = "{WareHouse.WhCode}<>''"
        End If
        Report.DataDefinition.RecordSelectionFormula = StrSelFormula
    End Sub

    Private Sub SetTableLocation(ByVal tables As Tables)
        Dim connectionInfo As New ConnectionInfo()

        connectionInfo.ServerName = Session("Server")
        connectionInfo.DatabaseName = Session("Database")
        connectionInfo.UserID = "admin"
        connectionInfo.Password = "swathi*"

        For Each table As CrystalDecisions.CrystalReports.Engine.Table In tables
            Dim tableLogOnInfo As TableLogOnInfo = table.LogOnInfo
            tableLogOnInfo.ConnectionInfo = connectionInfo
            table.ApplyLogOnInfo(tableLogOnInfo)
        Next
    End Sub
End Class