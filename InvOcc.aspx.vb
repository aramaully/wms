﻿Public Partial Class InvOcc
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "InvoiceOccupancy"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            TxtYear.Text = Year(Date.Today.AddMonths(-1))
            CmbMonth.Text = Date.Today.AddMonths(-1).ToString("MMMM")
            TxtInvDate.Text = Date.Today.ToString("dd-MMM-yyyy")

            'Load combobox Type
            Me.CmbInvoiceType.Items.Add("Select...")
            Me.CmbInvoiceType.Items.Add("Rental")
            Me.CmbInvoiceType.Items.Add("Other Services")
            Me.CmbInvoiceType.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
        If RbnClient.Checked Then
            StrSql = StrSql & "Name"
        ElseIf RbnCountry.Checked Then
            StrSql = StrSql & "City"
        Else
            StrSql = StrSql & "Contact"
        End If
        StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub ButView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButView.Click
        GdvOcc.DataSource = Nothing
        GdvOcc.DataBind()
        Dim DteOccDate As Date
        DteOccDate = CDate("25, " & CmbMonth.Text & " " & TxtYear.Text)
        StrSql = "SELECT * FROM Billing"
        StrSql = StrSql & " WHERE WHCode = '" & CmbZone.Text & "' "
        StrSql = StrSql & " AND Month = '" & CmbMonth.Text & "'"
        StrSql = StrSql & " AND Year = " & TxtYear.Text & ""
        If Me.CmbInvoiceType.Text = "Rental" Then
            StrSql = StrSql & " AND Unit='Cabin'"
        ElseIf Me.CmbInvoiceType.Text = "Other Services" Then
            StrSql = StrSql & " AND Unit<>'Cabin'"
        Else
            ErrHead.Text = "Error"
            ErrMsg.Text = "Please select type to view invoices!"
            Me.CmbInvoiceType.Focus()
            ModalPopupExtender1.Show()
            Exit Sub
        End If
        StrSql = StrSql & " ORDER BY ClientID"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvOcc.DataSource = Rdr
        GdvOcc.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ButCompute_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCompute.Click
        Dim StrSql As String
        Dim LngRecCount As Long
        Dim LngCurRec As Long
        Dim DteOccDate As Date
        Dim StrClientID As String
        Dim SngQuantity As Single
        Dim IntCars As Integer
        Dim IntPCars As Integer
        Dim DteFirst As Date
        Dim DteLast As Date
        Dim IntCarChargeUnits As Integer
        Dim IntPCarChargeUnits As Integer
        Dim LngInvoiceNo As Long
        Dim SngAmount As Single
        Dim StrMsg As String
        Dim IntTsu As Integer
        Dim IntMonDays As Integer
        Dim StrPrefZone As String
        Dim SngVat As Single
        Dim IntContDays As Integer
        Dim Int20Ft As Integer
        Dim Int40Ft As Integer
        Dim Int20Rate As Double
        Dim Int40Rate As Long
        Dim IntDays As Integer
        Dim RdrOcc As OleDb.OleDbDataReader
        Dim RdrClient As OleDb.OleDbDataReader
        Dim CmdClient As New OleDb.OleDbCommand
        Dim CmdOcc As New OleDb.OleDbCommand
        Dim CmdLoop As New OleDb.OleDbCommand
        Dim ConOcc As New OleDb.OleDbConnection
        Dim CmdOccDetails As New OleDb.OleDbCommand
        Dim RdrOccDetails As OleDb.OleDbDataReader
        Dim Trans As OleDb.OleDbTransaction
        Dim IntSysUnitDays As Integer
        Dim IntSysVehCh As Integer
        Dim IntPrefVehCh As Integer
        Dim IntSysPalletCh As Integer
        Dim IntAddDaily As Integer
        Dim BlnPref As Boolean
        Dim IntCarChargeDays As Integer
        Dim IntPCarChargeDays As Integer
        Dim RdrCar As OleDb.OleDbDataReader
        Dim CmdFreeCont As New OleDb.OleDbCommand
        Dim RdrFreeCont As OleDb.OleDbDataReader
        Dim CmdCheckRenewal As New OleDb.OleDbCommand
        Dim RdrCheckRenewal As OleDb.OleDbDataReader
        Dim DteFirstContDate As Date
        Dim DteLastContDate As Date
        Dim IntFreeCont_20ft_Days As Integer
        Dim IntFreeCont_40ft_Days As Integer
        Dim IntFullNonTariffCh As Integer
        Dim IntHalfNonTariffCh As Integer
        Dim IntNewMotoFullCharge As Integer
        Dim IntNewMotoHalfCharge As Integer
        Dim StrPropertyDesc As String
        Dim StrServiceDesc As String

        Dim IntFortnightMotoCharge As Integer
        Dim IntFullMotoCharge As Integer

        Dim IntDoubleCarChargeNonPref As Integer
        Dim IntDoubleCarChargeNonPrefHalf As Integer

        Dim IntNewFortnightNormal As Integer
        Dim IntNewFull As Integer
        Dim IntNewDoubleFornightNormal As Integer
        Dim IntNewDoubleFull As Integer

        Dim IntNewPrefFortnightNormal As Integer
        Dim IntNewPrefAddDays As Integer
        Dim IntNewPrefortnightNormalDouble As Integer
        Dim IntNewPrefAddDaysDouble As Integer

        Dim GateInRate As Double
        Dim GateInQty As Integer
        Dim GateInFees As Double
        Dim GateOutRate As Double
        Dim GateOutQty As Integer
        Dim GateOutFees As Double

        Dim CmdCheckOccupancy As New OleDb.OleDbCommand
        Dim RdrCheckOccupancy As OleDb.OleDbDataReader
        Dim CmdChkProperty As New OleDb.OleDbCommand
        Dim RdrChkProperty As OleDb.OleDbDataReader
        Dim CmdOccRep As New OleDb.OleDbCommand

        Dim LngNextInvoice As Long = 0
        Dim IsRental As Boolean
        If Me.CmbInvoiceType.Text = "Rental" Then
            IsRental = True
        ElseIf Me.CmbInvoiceType.Text = "Other Services" Then
            IsRental = False
        Else
            ErrHead.Text = "Error"
            ErrMsg.Text = "Please select type to view invoices!"
            Me.CmbInvoiceType.Focus()
            ModalPopupExtender1.Show()
            Exit Sub
        End If

        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")

        ConOcc.ConnectionString = Session("ConnString")
        ConOcc.Open()
        CmdOcc.Connection = ConOcc
        CmdChkProperty.Connection = ConOcc
        CmdCheckOccupancy.Connection = ConOcc
        CmdOccRep.Connection = ConOcc

        If CmbZone.Text = "Select..." Then
            ErrHead.Text = "Data Required"
            ErrMsg.Text = "Please Select the FP Zone!"
            ModalPopupExtender1.Show()
            CmbZone.Focus()
            Exit Sub
        End If

        DteOccDate = CDate("25, " & CmbMonth.Text & " " & TxtYear.Text)
        DteFirst = CDate("01, " & CmbMonth.Text & " " & TxtYear.Text)
        DteLast = DteOccDate
        Do While Month(DteLast.AddDays(1)) = Month(DteOccDate)
            DteLast = DteLast.AddDays(1)
        Loop
        IntMonDays = Day(DteLast)
        If Not ChkReCompute.Checked Then
            StrSql = "SELECT COUNT(*) FROM Billing"
            StrSql = StrSql & " WHERE BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "'"
            StrSql = StrSql & " AND Billing.WHCode ='" & CmbZone.Text & "'"
            If IsRental Then
                StrSql = StrSql & " AND Billing.Unit='Cabin'"
            Else
                StrSql = StrSql & " AND Billing.Unit<>'Cabin'"
            End If
            Cmd.CommandText = StrSql
            Cmd.Connection = Con
            Cmd.CommandTimeout = 0
            If Cmd.ExecuteScalar > 0 Then
                ErrHead.Text = "Error"
                If IsRental Then
                    ErrMsg.Text = "Billing (Rental) for the Month of " & CmbMonth.Text & " is done. Check Re-Create to do again!"
                Else
                    ErrMsg.Text = "Billing for the Month of " & CmbMonth.Text & " is done. Check Re-Create to do again!"
                End If
                ModalPopupExtender1.Show()
                Exit Sub
            End If
        End If

        If Me.CmbZone.Text = "FPZ6SD" Then
            StrSql = "SELECT COUNT(*) FROM Occupancy WHERE BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "'"
            StrSql = StrSql & " AND Occupancy.WHCode = '" & CmbZone.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.Connection = Con
            Cmd.CommandTimeout = 0
            If Cmd.ExecuteScalar < 1 Then
                ErrHead.Text = "Error"
                ErrMsg.Text = "Compute Occupancy for the zone FPZ6SD before creating invoice!"
                ModalPopupExtender1.Show()
                Exit Sub
            End If
        End If
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Transaction = Trans
        Cmd.Connection = Con
        Try
            'Delete Invoices
            StrSql = "DELETE FROM Invoice WHERE Invoiceno IN"
            StrSql = StrSql & " (SELECT Billing.Invoiceno FROM Billing "
            StrSql = StrSql & " WHERE [Month] = '" & CmbMonth.Text & "'"
            StrSql = StrSql & " AND [Year] = '" & TxtYear.Text & "'"
            StrSql = StrSql & " AND Billing.WHCode = '" & CmbZone.Text & "'"
            If IsRental Then
                StrSql = StrSql & " AND Billing.Unit = 'Cabin'"
            Else
                StrSql = StrSql & " AND Billing.Unit <> 'Cabin'"
            End If
            StrSql = StrSql & " ) AND WhCode  = '" & CmbZone.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()

            'Delete Billings
            StrSql = "DELETE FROM Billing WHERE [Month] = '" & CmbMonth.Text & "' AND [Year] = '" & TxtYear.Text & "'"
            StrSql = StrSql & " AND Billing.WhCode ='" & CmbZone.Text & "'"
            If IsRental Then
                StrSql = StrSql & " AND Billing.Unit = 'Cabin'"
            Else
                StrSql = StrSql & " AND Billing.Unit <> 'Cabin'"
            End If
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()

            'Delete Container Details
            If Not IsRental Then
                StrSql = "DELETE FROM ContDetail WHERE [Month] = '" & CmbMonth.Text & "' AND [Year] = '" & TxtYear.Text & "'"
                StrSql = StrSql & " AND ContDetail.WhCode ='" & CmbZone.Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            End If
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            '#################### #################### BILLING (Start) #################### ####################

            '#################### Car Bonded Yard (Start) ####################
            If Not IsRental Then
                Cmd.CommandText = "SELECT * FROM SDTariff"
                Rdr = Cmd.ExecuteReader
                While Rdr.Read
                    If Rdr("TariffID") = "SDFullCar" Then
                        'IntFullNonTariffCh = 1500
                        IntFullNonTariffCh = Rdr("TariffValue")
                    End If
                    If Rdr("TariffID") = "SDHalfCar" Then
                        'IntHalfNonTariffCh = 850
                        IntHalfNonTariffCh = Rdr("TariffValue")
                    End If
                    If Rdr("TariffID") = "SDFullDblCar" Then
                        'IntDoubleCarChargeNonPref = 3000
                        IntDoubleCarChargeNonPref = Rdr("TariffValue")
                    End If
                    If Rdr("TariffID") = "SDHalfDblCar" Then
                        'IntDoubleCarChargeNonPrefHalf = 1700
                        IntDoubleCarChargeNonPrefHalf = Rdr("TariffValue")
                    End If
                    If Rdr("TariffID") = "SDFullDblCar" Then
                        'IntDoubleCarChargeNonPref = 3000
                        IntDoubleCarChargeNonPref = Rdr("TariffValue")
                    End If
                    'Normal Motocycle Charge (ZPC Motors Ltd.)
                    If Rdr("TariffID") = "SDFullZPC" Then
                        'IntNewMotoFullCharge = 800
                        IntNewMotoFullCharge = Rdr("TariffValue")
                    End If
                    If Rdr("TariffID") = "SDHalfZPC" Then
                        'IntNewMotoHalfCharge = 425
                        IntNewMotoHalfCharge = Rdr("TariffValue")
                    End If
                End While
                Rdr.Close()

                IntFortnightMotoCharge = 0
                IntFullMotoCharge = 0

                'Get Clients in Occupancy records for the month selected
                StrSql = "SELECT DISTINCT(ClientID) FROM Occupancy WHERE BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "'"
                StrSql = StrSql & " AND Occupancy.WHCode = '" & CmbZone.Text & "' ORDER BY ClientId"
                CmdOcc.CommandText = StrSql
                CmdOcc.Connection = ConOcc
                CmdOccDetails.Connection = ConOcc
                RdrOcc = CmdOcc.ExecuteReader

                'Start processing Pallets and Cars
                While RdrOcc.Read
                    BlnPref = False
                    SngQuantity = 0
                    IntCars = 0
                    IntPCars = 0
                    IntAddDaily = 0
                    IntCarChargeUnits = 0
                    IntPCarChargeUnits = 0
                    IntCarChargeDays = 0

                    IntFullMotoCharge = 0
                    IntFortnightMotoCharge = 0

                    IntNewFortnightNormal = 0
                    IntNewFull = 0

                    IntNewDoubleFornightNormal = 0
                    IntNewDoubleFull = 0

                    IntNewPrefFortnightNormal = 0
                    IntNewPrefAddDays = 0

                    IntNewPrefortnightNormalDouble = 0
                    IntNewPrefAddDaysDouble = 0

                    GateInQty = 0
                    GateOutQty = 0
                    GateInFees = 0.00
                    GateOutFees = 0.00

                    StrClientID = RdrOcc("ClientId").ToString
                    If StrClientID = "400C9186" Then
                        'Do not create Billing and Invoice for ZPC Motors Ltd for Zone FPZ6SD
                    Else
                        StrSql = "SELECT * FROM Client WHERE ClientID = '" & StrClientID & "'"
                        CmdClient.Connection = ConOcc
                        CmdClient.CommandText = StrSql
                        RdrClient = CmdClient.ExecuteReader
                        If RdrClient.Read Then
                            StrSql = "SELECT * FROM CarTariff WHERE ClientID =  '" & RdrClient("ClientID") & "'"
                            StrSql = StrSql & " AND Fortnight>0 AND AddRate>0 "
                            Cmd.CommandText = StrSql
                            RdrCar = Cmd.ExecuteReader
                            IntPrefVehCh = 0
                            If RdrCar.Read Then
                                IntPrefVehCh = RdrCar("Fortnight")
                                IntAddDaily = RdrCar("AddRate")
                                BlnPref = True
                            End If
                            RdrCar.Close()
                            StrPrefZone = RdrClient("WHouses").ToString

                            'Get Occupancy records for this client
                            StrSql = "SELECT * FROM Occupancy WHERE BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "'"
                            StrSql = StrSql & " AND ClientID = '" & StrClientID & "' AND Occupancy.WHCode = '" & CmbZone.Text & "' ORDER BY ClientId, BoeNo"
                            CmdOccDetails.CommandText = StrSql
                            RdrOccDetails = CmdOccDetails.ExecuteReader
                            While RdrOccDetails.Read
                                If BlnPref Then
                                    If RdrOccDetails("PRatio") = 10 Then
                                        If RdrOccDetails("Days") > 15 Then
                                            IntNewPrefFortnightNormal = IntNewPrefFortnightNormal + 1
                                            IntNewPrefAddDays = IntNewPrefAddDays + (RdrOccDetails("Days") - 15)
                                        Else
                                            IntNewPrefFortnightNormal = IntNewPrefFortnightNormal + 1
                                        End If
                                    End If
                                    If RdrOccDetails("PRatio") = 20 Then
                                        If RdrOccDetails("Days") > 15 Then
                                            IntNewPrefortnightNormalDouble = IntNewPrefortnightNormalDouble + 1
                                            IntNewPrefAddDaysDouble = IntNewPrefAddDaysDouble + (RdrOccDetails("Days") - 15)
                                        Else
                                            IntNewPrefortnightNormalDouble = IntNewPrefortnightNormalDouble + 1
                                        End If
                                    End If
                                Else
                                    If RdrOccDetails("PRatio") = 5 Then
                                        If RdrOccDetails("Days") > 15 Then
                                            IntFullMotoCharge = IntFullMotoCharge + 1
                                        Else
                                            IntFortnightMotoCharge = IntFortnightMotoCharge + 1
                                        End If
                                    End If
                                    If RdrOccDetails("PRatio") = 10 Then
                                        If RdrOccDetails("Days") > 15 Then
                                            IntNewFull = IntNewFull + 1
                                        Else
                                            IntNewFortnightNormal = IntNewFortnightNormal + 1
                                        End If
                                    End If
                                    If RdrOccDetails("PRatio") = 20 Then
                                        If RdrOccDetails("Days") > 15 Then
                                            IntNewDoubleFull = IntNewDoubleFull + 1
                                        Else
                                            IntNewDoubleFornightNormal = IntNewDoubleFornightNormal + 1
                                        End If
                                    End If
                                End If

                                'Summation of Gate In
                                If RdrOccDetails("GateInFees") > 0 Then
                                    GateInQty = GateInQty + 1
                                    GateInRate = RdrOccDetails("GateInRate")
                                    GateInFees = GateInFees + RdrOccDetails("GateInFees")
                                End If
                                'Summation of Gate Out
                                If RdrOccDetails("GateOutFees") > 0 Then
                                    GateOutQty = GateOutQty + 1
                                    GateOutRate = RdrOccDetails("GateOutRate")
                                    GateOutFees = GateOutFees + RdrOccDetails("GateOutFees")
                                End If
                            End While
                            RdrOccDetails.Close()

                            'Create Billing record with summary collected for this client
                            'Moto Charge for Full Month
                            If IntFullMotoCharge > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Storage of Motocycle Full Month',"
                                StrSql = StrSql & IntNewMotoFullCharge & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & IntFullMotoCharge & ","
                                StrSql = StrSql & IntNewMotoFullCharge * IntFullMotoCharge & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                            'Moto Charge for Fortnights
                            If IntFortnightMotoCharge > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Storage of Motocycle Fortnight Charge',"
                                StrSql = StrSql & IntNewMotoHalfCharge & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & IntFortnightMotoCharge & ","
                                StrSql = StrSql & IntNewMotoHalfCharge * IntFortnightMotoCharge & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                            'Bonded
                            'Full charge for Car
                            If IntNewFull > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Storage of Cars Full Month',"
                                StrSql = StrSql & IntFullNonTariffCh & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & IntNewFull & ","
                                StrSql = StrSql & IntFullNonTariffCh * IntNewFull & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                            'Bonded
                            'Fortnight charge for Car
                            If IntNewFortnightNormal > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Storage of Cars Fortnight Charge',"
                                StrSql = StrSql & IntHalfNonTariffCh & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & IntNewFortnightNormal & ","
                                StrSql = StrSql & IntHalfNonTariffCh * IntNewFortnightNormal & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                            'Full Month Gate In Fees
                            If GateInQty > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Gate In Fees Full Month',"
                                StrSql = StrSql & GateInRate & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & GateInQty & ","
                                StrSql = StrSql & GateInFees & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                            'Full Month Gate Out Fees
                            If GateOutQty > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Gate Out Fees Full Month',"
                                StrSql = StrSql & GateOutRate & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & GateOutQty & ","
                                StrSql = StrSql & GateOutFees & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                            'Full Month Charge For Double Parking 
                            If IntNewDoubleFull > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Storage of Cars Full Month',"
                                StrSql = StrSql & IntDoubleCarChargeNonPref & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & IntNewDoubleFull & ","
                                StrSql = StrSql & IntDoubleCarChargeNonPref * IntNewDoubleFull & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                            'Fortnight Charge For Double Parking 
                            If IntNewDoubleFornightNormal > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Storage of Cars Fortnight Charge',"
                                StrSql = StrSql & IntDoubleCarChargeNonPrefHalf & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & IntNewDoubleFornightNormal & ","
                                StrSql = StrSql & IntDoubleCarChargeNonPrefHalf * IntNewDoubleFornightNormal & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                            'Preferential Rate For Fortnight cars
                            If IntNewPrefFortnightNormal > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Storage of Cars Fortnight Charge',"
                                StrSql = StrSql & IntPrefVehCh & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & IntNewPrefFortnightNormal & ","
                                StrSql = StrSql & IntNewPrefFortnightNormal * IntPrefVehCh & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                            'Preferential Rate For additional days cars
                            If IntNewPrefAddDays > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Storage of Cars Additional Days',"
                                StrSql = StrSql & IntAddDaily & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & IntNewPrefAddDays & ","
                                StrSql = StrSql & IntNewPrefAddDays * IntAddDaily & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                            'Preferential Rate For Fortnight cars double 
                            If IntNewPrefortnightNormalDouble > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Storage of Cars Fortnight Charge',"
                                StrSql = StrSql & IntPrefVehCh * 2 & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & IntNewPrefortnightNormalDouble & ","
                                StrSql = StrSql & IntNewPrefortnightNormalDouble * IntPrefVehCh * 2 & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                            'Preferential Rate For additional days cars double
                            If IntNewPrefAddDaysDouble > 0 Then
                                StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Description,Rate,BillDate,Quantity,Amount,WhCode) VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & "Unit','"
                                StrSql = StrSql & "Storage of Cars Additional Days',"
                                StrSql = StrSql & IntAddDaily * 2 & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                                StrSql = StrSql & IntNewPrefAddDaysDouble & ","
                                StrSql = StrSql & IntNewPrefAddDaysDouble * IntAddDaily * 2 & ",'"
                                StrSql = StrSql & CmbZone.Text & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                        End If
                        RdrClient.Close()
                    End If
                End While
                RdrOcc.Close()
            End If
            '#################### Car Bonded Yard (End) ####################

            '#################### Rental (Start) ####################
            If IsRental Then
                'Pallets and Cars Done
                'Now this has to be done from the lease agreements.
                'Select valid and active lease agreements 

                Dim DteStartDate As Date
                Dim DteEndDate As Date

                DteStartDate = CDate("01/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text)
                DteEndDate = Date.DaysInMonth(Val(TxtYear.Text), CmbMonth.SelectedIndex) & "/" & Format(CmbMonth.SelectedIndex, "00") & "/" & TxtYear.Text

                StrSql = "SELECT * From LeaseAgreement"
                StrSql = StrSql & " WHERE Zone = '" & CmbZone.Text & "'"
                StrSql = StrSql & " AND (('" & Format(DteStartDate, "MM/dd/yyyy") & "' Between BillingStart And BillingTo) Or ('" & Format(DteEndDate, "MM/dd/yyyy") & "' Between BillingStart And BillingTo))"
                StrSql = StrSql & " AND MoveOutDate IS NULL"
                StrSql = StrSql & " AND MovedOut = 0"

                CmdOcc.CommandText = StrSql
                RdrOcc = CmdOcc.ExecuteReader
                CmdCheckRenewal.Connection = ConOcc

                Dim IntAmount As Integer
                Dim StrAddPeriod As String
                While RdrOcc.Read
                    Dim StrDesc As String
                    Dim LngServiceCharge As Long = 0
                    Dim LngInsurance As Long = 0
                    Dim LngDeposit As Long = 0

                    StrPropertyDesc = ""
                    StrServiceDesc = ""
                    StrClientID = RdrOcc("ClientID")


                    If Val(RdrOcc("ConCharges")) > 0 Then
                        IntAmount = RdrOcc("MonthlyRent") - Val(RdrOcc("ConCharges"))
                    Else
                        IntAmount = RdrOcc("MonthlyRent")
                    End If

                    StrAddPeriod = ""

                    LngServiceCharge = Val(RdrOcc("ConCharges"))


                    If CDate(RdrOcc("BillingStart")) >= CDate(DteStartDate) And CDate(RdrOcc("BillingStart")) <= CDate(DteEndDate) Then
                        IntAmount = IntAmount * ((DateDiff(DateInterval.Day, CDate(RdrOcc("BillingStart")), DteEndDate, ) + 1) / CLng(DteEndDate.Day))
                        LngServiceCharge = LngServiceCharge * ((DateDiff(DateInterval.Day, CDate(RdrOcc("BillingStart")), DteEndDate, ) + 1) / CLng(DteEndDate.Day))
                        StrAddPeriod = " (Period From: " & Format(CDate(RdrOcc("BillingStart")), "dd-MMM-yyyy") & " To " & Format(DteEndDate, "dd-MMM-yyyy") & ")"
                    End If
                    If CDate(RdrOcc("BillingTo")) >= CDate(DteStartDate) And CDate(RdrOcc("BillingTo")) <= CDate(DteEndDate) Then
                        IntAmount = IntAmount * ((DateDiff(DateInterval.Day, DteStartDate, CDate(RdrOcc("BillingTo"))) + 1) / CLng(DteEndDate.Day))
                        LngServiceCharge = LngServiceCharge * ((DateDiff(DateInterval.Day, DteStartDate, CDate(RdrOcc("BillingTo"))) + 1) / CLng(DteEndDate.Day))
                        StrAddPeriod = " (Period From: " & Format(DteStartDate, "dd-MMM-yyyy") & " To " & Format(CDate(RdrOcc("BillingTo")), "dd-MMM-yyyy") & ")"
                    End If

                    StrDesc = RdrOcc("ProjectDesc") & " " & RdrOcc("Area") & " " & RdrOcc("Unit")

                    CmdChkProperty.CommandText = "SELECT * FROM Property WHERE PropertyCode = '" & RdrOcc("PropCode") & "'"
                    RdrChkProperty = CmdChkProperty.ExecuteReader
                    If RdrChkProperty.Read Then
                        StrPropertyDesc = RdrChkProperty("Description").ToString & " " & RdrChkProperty("Area").ToString & " " & RdrChkProperty("Unit").ToString & StrAddPeriod
                        StrServiceDesc = "Service Charge " & StrAddPeriod
                    End If
                    RdrChkProperty.Close()

                    LngInsurance = Val(RdrOcc("Insurance"))
                    LngDeposit = Val(RdrOcc("Deposit"))

                    If Val(RdrOcc("ConCharges")) > 0 Then
                        StrSql = "INSERT INTO Billing (ClientID, [Month], [Year], Unit, Description, Rate, Amount, BillDate, Quantity, WhCode, PropCode) "
                        StrSql = StrSql & "VALUES('" & RdrOcc("ClientID") & "','"
                        StrSql = StrSql & CmbMonth.Text & "',"
                        StrSql = StrSql & TxtYear.Text & ",'"
                        StrSql = StrSql & "Cabin','"
                        StrSql = StrSql & StrServiceDesc & "',"
                        StrSql = StrSql & RdrOcc("ConCharges") & ","
                        StrSql = StrSql & LngServiceCharge & ",'"
                        StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                        StrSql = StrSql & "1,'"
                        StrSql = StrSql & CmbZone.Text & "','" & RdrOcc("PropCode") & "')"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If

                    StrSql = "INSERT INTO Billing (ClientID, [Month], [Year], Unit, Description, Rate, Amount, BillDate, Quantity, WhCode, PropCode) "
                    StrSql = StrSql & "VALUES('" & RdrOcc("ClientID") & "','"
                    StrSql = StrSql & CmbMonth.Text & "',"
                    StrSql = StrSql & TxtYear.Text & ",'"
                    StrSql = StrSql & "Cabin','"
                    StrSql = StrSql & StrPropertyDesc & "',"
                    StrSql = StrSql & RdrOcc("Rate") & ","
                    StrSql = StrSql & IntAmount & ",'"
                    StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                    StrSql = StrSql & "1,'"
                    StrSql = StrSql & CmbZone.Text & "','" & RdrOcc("PropCode") & "')"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End While
                RdrOcc.Close()
            End If
            '#################### Rental (End) ####################

            '#################### Containers (Start) ####################
            CmdClient.Connection = ConOcc
            If Not IsRental Then
                'Cabins Done
                'Start doing Containers
                Dim IntClientCont As Integer
                Dim IntFreeChargeDays As Integer
                Dim IntCheckPrevMonthDays As Integer
                'Get clients from containers table
                StrSql = "SELECT DISTINCT(ClientID) FROM Containers WHERE (PaidUpTo IS NULL OR PaidUpTo < '" & Format(DteFirst, "dd-MMM-yyyy") & "')"
                StrSql = StrSql & " AND (Dateout IS NULL OR  DateOut >= '" & Format(DteFirst, "dd-MMM-yyyy") & "')"
                StrSql = StrSql & " AND WHCode = '" & CmbZone.Text & "' ORDER BY ClientID"
                CmdOcc.CommandText = StrSql
                RdrOcc = CmdOcc.ExecuteReader
                CmdFreeCont.Connection = ConOcc
                While RdrOcc.Read

                    StrClientID = RdrOcc("ClientID") & ""
                    SngAmount = 0
                    Int20Ft = 0
                    Int40Ft = 0
                    Int20Rate = 0
                    Int40Rate = 0

                    StrSql = "SELECT Cont20 + Cont40 FROM Client WHERE ClientID = '" & StrClientID & "'"
                    CmdClient.CommandText = StrSql
                    IntClientCont = Val(CmdClient.ExecuteScalar)

                    'Get records from Containers for this client
                    StrSql = "SELECT * FROM Containers WHERE (PaidUpTo IS NULL OR PaidUpTo < '" & Format(DteFirst, "dd-MMM-yyyy") & "')"
                    StrSql = StrSql & " AND (Dateout IS NULL OR  DateOut >= '" & Format(DteFirst, "dd-MMM-yyyy") & "')"
                    StrSql = StrSql & " AND ClientID  = '" & StrClientID & "' AND WHCode = '" & CmbZone.Text & "'"
                    CmdLoop.CommandText = StrSql
                    CmdLoop.Connection = ConOcc
                    Rdr = CmdLoop.ExecuteReader
                    While Rdr.Read
                        IntFreeCont_20ft_Days = 0
                        IntFreeCont_40ft_Days = 0
                        CmdFreeCont.CommandText = "Select * FROM CarTariff WHERE ClientID  = '" & StrClientID & "'"
                        RdrFreeCont = CmdFreeCont.ExecuteReader
                        While RdrFreeCont.Read
                            IntFreeCont_20ft_Days = RdrFreeCont("FreeDaysContainer_20ft")
                            IntFreeCont_40ft_Days = RdrFreeCont("FreeDaysContainer_40ft")
                        End While
                        RdrFreeCont.Close()

                        Dim BlnFristMonth As Boolean
                        BlnFristMonth = False
                        If CDate(Rdr("DateIn")) < DteFirst Then
                            DteFirstContDate = DteFirst
                        Else
                            DteFirstContDate = CDate(Rdr("DateIn"))
                        End If

                        If Not IsDBNull(Rdr("DateOut")) Then
                            If CDate(Rdr("DateOut")) >= DteFirst Then
                                DteLastContDate = Rdr("DateOut")
                                If Rdr("DateOut") > DteLast Then
                                    DteLastContDate = DteLast
                                End If
                            Else
                                DteLastContDate = DteLast
                            End If
                            If (CDate(Rdr("DateIn")).Month = DteFirst.Month) And (CDate(Rdr("DateIn")).Year = DteFirst.Year) Then
                                BlnFristMonth = True
                            End If
                        Else
                            DteLastContDate = DteLast
                            If (CDate(Rdr("DateIn")).Month = DteFirst.Month) And (CDate(Rdr("DateIn")).Year = DteFirst.Year) Then
                                BlnFristMonth = True
                            End If
                        End If

                        IntFreeChargeDays = CLng(DateDiff("d", DteFirstContDate, DteLastContDate) + 1)
                        IntCheckPrevMonthDays = CLng(DateDiff("d", Rdr("DateIn"), DteFirst.AddDays(-1)) + 1)
                        If IntCheckPrevMonthDays > 0 Then
                            'aramaully - V1.0.0.4
                            If InStr(Rdr("Description"), "20") > 0 Then
                                If IntCheckPrevMonthDays <= IntFreeCont_20ft_Days Then
                                    IntFreeCont_20ft_Days = IntFreeCont_20ft_Days - IntCheckPrevMonthDays
                                    BlnFristMonth = True
                                End If
                            Else
                                If IntCheckPrevMonthDays <= IntFreeCont_40ft_Days Then
                                    IntFreeCont_40ft_Days = IntFreeCont_40ft_Days - IntCheckPrevMonthDays
                                    BlnFristMonth = True
                                End If
                            End If
                        End If
                        If Not BlnFristMonth Then
                            If (IntFreeChargeDays) > 0 Then
                                StrSql = "INSERT INTO ContDetail (ClientID,WhCode,[Month],[Year],Description,Rate,BillDate,ContNumber,Quantity,Unit,Amount,TSU,DateFrom,DateTo) "
                                StrSql = StrSql & "VALUES('"
                                StrSql = StrSql & StrClientID & "','"
                                StrSql = StrSql & CmbZone.Text & "','"
                                StrSql = StrSql & CmbMonth.Text & "',"
                                StrSql = StrSql & TxtYear.Text & ",'"
                                StrSql = StrSql & Rdr("Description") & "',"
                                StrSql = StrSql & Rdr("Rate") & ",'"
                                StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "','"
                                StrSql = StrSql & Rdr("ContainerNo") & "',"
                                IntContDays = IntMonDays
                                If (Not IsDBNull(Rdr("DateOut"))) AndAlso Rdr("DateOut") < DteLast Then
                                    IntContDays = Day(Rdr("DateOut"))
                                End If
                                If Rdr("DateIn") > DteFirst Then
                                    IntContDays = IntContDays - Day(Rdr("DateIn")) + 1
                                End If

                                StrSql = StrSql & (IntContDays) & ",'Days',"
                                StrSql = StrSql & Rdr("Rate") * (IntContDays) & ","

                                SngAmount = SngAmount + (Rdr("Rate") * (IntContDays))
                                If InStr(Rdr("Description"), "20") > 0 Then
                                    Int20Ft = Int20Ft + 1
                                    Int20Rate = Int20Rate + (Rdr("Rate") * (IntContDays))
                                Else
                                    Int40Ft = Int40Ft + 1
                                    Int40Rate = Int40Rate + (Rdr("Rate") * (IntContDays))
                                End If
                                StrSql = StrSql & "NULL,'" & Format(DteFirstContDate, "MM/dd/yyyy") & "','" & Format(DteLastContDate, "MM/dd/yyyy") & "')"
                                Cmd.CommandText = StrSql
                                Cmd.ExecuteNonQuery()
                            End If
                        Else
                            'aramaully - V1.0.0.4
                            If InStr(Rdr("Description"), "20") > 0 Then
                                If (IntFreeChargeDays - IntFreeCont_20ft_Days) > 0 Then
                                    StrSql = "INSERT INTO ContDetail (ClientID,WhCode,[Month],[Year],Description,Rate,BillDate,ContNumber,Quantity,Unit,Amount,TSU,DateFrom,DateTo) "
                                    StrSql = StrSql & "VALUES('"
                                    StrSql = StrSql & StrClientID & "','"
                                    StrSql = StrSql & CmbZone.Text & "','"
                                    StrSql = StrSql & CmbMonth.Text & "',"
                                    StrSql = StrSql & TxtYear.Text & ",'"
                                    StrSql = StrSql & Rdr("Description") & "',"
                                    StrSql = StrSql & Rdr("Rate") & ",'"
                                    StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "','"
                                    StrSql = StrSql & Rdr("ContainerNo") & "',"
                                    IntContDays = IntMonDays
                                    If (Not IsDBNull(Rdr("DateOut"))) AndAlso Rdr("DateOut") < DteLast Then
                                        IntContDays = Day(Rdr("DateOut"))
                                    End If
                                    If Rdr("DateIn") > DteFirst Then
                                        IntContDays = IntContDays - Day(Rdr("DateIn")) + 1
                                    End If
                                    If BlnFristMonth Then
                                        StrSql = StrSql & (IntContDays - IntFreeCont_20ft_Days) & ",'Days',"
                                    Else
                                        StrSql = StrSql & (IntContDays) & ",'Days',"
                                    End If
                                    If BlnFristMonth Then
                                        StrSql = StrSql & Rdr("Rate") * (IntContDays - IntFreeCont_20ft_Days) & ","
                                    Else
                                        StrSql = StrSql & Rdr("Rate") * (IntContDays) & ","
                                    End If
                                    If BlnFristMonth Then
                                        SngAmount = SngAmount + (Rdr("Rate") * (IntContDays - IntFreeCont_20ft_Days))
                                    Else
                                        SngAmount = SngAmount + (Rdr("Rate") * (IntContDays))
                                    End If
                                    Int20Ft = Int20Ft + 1
                                    If BlnFristMonth Then
                                        Int20Rate = Int20Rate + (Rdr("Rate") * (IntContDays - IntFreeCont_20ft_Days))
                                    Else
                                        Int20Rate = Int20Rate + (Rdr("Rate") * (IntContDays))
                                    End If
                                    StrSql = StrSql & "NULL,'" & Format(DteFirstContDate, "MM/dd/yyyy") & "','" & Format(DteLastContDate, "MM/dd/yyyy") & "')"
                                    Cmd.CommandText = StrSql
                                    Cmd.ExecuteNonQuery()
                                End If
                            Else
                                'aramaully - V1.0.0.4
                                If (IntFreeChargeDays - IntFreeCont_40ft_Days) > 0 Then
                                    StrSql = "INSERT INTO ContDetail (ClientID,WhCode,[Month],[Year],Description,Rate,BillDate,ContNumber,Quantity,Unit,Amount,TSU,DateFrom,DateTo) "
                                    StrSql = StrSql & "VALUES('"
                                    StrSql = StrSql & StrClientID & "','"
                                    StrSql = StrSql & CmbZone.Text & "','"
                                    StrSql = StrSql & CmbMonth.Text & "',"
                                    StrSql = StrSql & TxtYear.Text & ",'"
                                    StrSql = StrSql & Rdr("Description") & "',"
                                    StrSql = StrSql & Rdr("Rate") & ",'"
                                    StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "','"
                                    StrSql = StrSql & Rdr("ContainerNo") & "',"
                                    IntContDays = IntMonDays
                                    If (Not IsDBNull(Rdr("DateOut"))) AndAlso Rdr("DateOut") < DteLast Then
                                        IntContDays = Day(Rdr("DateOut"))
                                    End If
                                    If Rdr("DateIn") > DteFirst Then
                                        IntContDays = IntContDays - Day(Rdr("DateIn")) + 1
                                    End If
                                    If BlnFristMonth Then
                                        StrSql = StrSql & (IntContDays - IntFreeCont_40ft_Days) & ",'Days',"
                                    Else
                                        StrSql = StrSql & (IntContDays) & ",'Days',"
                                    End If
                                    If BlnFristMonth Then
                                        StrSql = StrSql & Rdr("Rate") * (IntContDays - IntFreeCont_40ft_Days) & ","
                                    Else
                                        StrSql = StrSql & Rdr("Rate") * (IntContDays) & ","
                                    End If
                                    If BlnFristMonth Then
                                        SngAmount = SngAmount + (Rdr("Rate") * (IntContDays - IntFreeCont_40ft_Days))
                                    Else
                                        SngAmount = SngAmount + (Rdr("Rate") * (IntContDays))
                                    End If

                                    Int40Ft = Int40Ft + 1
                                    If BlnFristMonth Then
                                        Int40Rate = Int40Rate + (Rdr("Rate") * (IntContDays - IntFreeCont_40ft_Days))
                                    Else
                                        Int40Rate = Int40Rate + (Rdr("Rate") * (IntContDays))
                                    End If
                                    StrSql = StrSql & "NULL,'" & Format(DteFirstContDate, "MM/dd/yyyy") & "','" & Format(DteLastContDate, "MM/dd/yyyy") & "')"
                                    Cmd.CommandText = StrSql
                                    Cmd.ExecuteNonQuery()
                                End If
                            End If

                        End If
                    End While
                    Rdr.Close()
                    ' Enter totals of containers in the billing 
                    'aramaully - V1.0.0.4
                    If (IntFreeChargeDays - IntFreeCont_20ft_Days And Int20Ft > 0) Then
                        StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Quantity,Description,BillDate,Amount,WhCode) VALUES('"
                        StrSql = StrSql & StrClientID & "','"
                        StrSql = StrSql & CmbMonth.Text & "',"
                        StrSql = StrSql & TxtYear.Text & ",'"
                        StrSql = StrSql & "Cont/Day',"
                        StrSql = StrSql & Int20Ft & ",'"
                        StrSql = StrSql & "20 Ft Containers Stored','"
                        StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                        StrSql = StrSql & Int20Rate & ",'"
                        StrSql = StrSql & CmbZone.Text & "')"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If
                    'aramaully - V1.0.0.4
                    If (IntFreeChargeDays - IntFreeCont_40ft_Days And Int40Ft > 0) Then
                        StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Quantity,Description,BillDate,Amount,WhCode) VALUES('"
                        StrSql = StrSql & StrClientID & "','"
                        StrSql = StrSql & CmbMonth.Text & "',"
                        StrSql = StrSql & TxtYear.Text & ",'"
                        StrSql = StrSql & "Cont/Day',"
                        StrSql = StrSql & Int40Ft & ",'"
                        StrSql = StrSql & "40 Ft Containers Stored','"
                        StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                        StrSql = StrSql & Int40Rate & ",'"
                        StrSql = StrSql & CmbZone.Text & "')"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If
                End While
                RdrOcc.Close()
            End If
            '#################### Containers (End) ####################

            '#################### Logistical Charges (Start) ####################
            If Not IsRental Then
                'Process Logistical Charges
                'Clients from Logistics table
                Dim StrLogisticDesc As String
                StrSql = "SELECT DISTINCT(ClientID) FROM Logistics WHERE RequestNo > 0"
                StrSql = StrSql & " AND Month(ChDate) = " & CmbMonth.SelectedIndex
                StrSql = StrSql & " AND Year(ChDate) = " & TxtYear.Text
                StrSql = StrSql & " AND WHCode = '" & CmbZone.Text & "' ORDER BY ClientID"
                CmdOcc.CommandText = StrSql
                RdrOcc = CmdOcc.ExecuteReader
                While RdrOcc.Read
                    StrClientID = RdrOcc("ClientID") & ""
                    SngAmount = 0
                    'Get records from Logistics for this client
                    StrSql = "SELECT * FROM Logistics WHERE RequestNo > 0"
                    StrSql = StrSql & " AND Month(ChDate) = " & CmbMonth.SelectedIndex
                    StrSql = StrSql & " AND Year(ChDate) = " & TxtYear.Text
                    StrSql = StrSql & " AND ClientID = '" & StrClientID & "'"
                    StrSql = StrSql & " AND WHCode = '" & CmbZone.Text & "' ORDER BY ClientID"
                    Cmd.CommandText = StrSql
                    Rdr = Cmd.ExecuteReader
                    While Rdr.Read
                        SngAmount = SngAmount + Rdr("Amount")
                        StrLogisticDesc = Rdr("Description") & ""
                    End While
                    Rdr.Close()
                    If SngAmount > 0 Then
                        StrSql = "INSERT INTO Billing (ClientID,[Month],[Year],Unit,Quantity,Description,BillDate,Amount,WhCode) VALUES('"
                        StrSql = StrSql & StrClientID & "','"
                        StrSql = StrSql & CmbMonth.Text & "',"
                        StrSql = StrSql & TxtYear.Text & ","
                        StrSql = StrSql & "'',"
                        StrSql = StrSql & "0,'"
                        StrSql = StrSql & "Other Charges','"
                        StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "',"
                        StrSql = StrSql & SngAmount & ",'"
                        StrSql = StrSql & CmbZone.Text & "')"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If
                End While
                RdrOcc.Close()
            End If
            '#################### Logistical Charges (End) ####################
            Trans.Commit()
            ButView_Click(Me, e)
            '#################### #################### BILLING (END) #################### ####################
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Error Creating Invoices"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try


        '#################### #################### INVOICES (START) #################### ####################
        'Open Clients from Billing Table for this period already updated above
        Dim CmdInvoice As New OleDb.OleDbCommand
        Dim CmdVat As New OleDb.OleDbCommand
        Dim CmdSystem As New OleDb.OleDbCommand
        StrSql = "SELECT DISTINCT(ClientID) FROM Billing"
        StrSql = StrSql & " WHERE BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "'"
        StrSql = StrSql & " AND Billing.WHCode ='" & CmbZone.Text & "'"
        If IsRental Then
            StrSql = StrSql & " AND Billing.Unit = 'Cabin'"
        Else
            StrSql = StrSql & " AND Billing.Unit <> 'Cabin'"
        End If
        StrSql = StrSql & " ORDER BY ClientID"

        CmdOcc.Connection = ConOcc
        CmdSystem.Connection = ConOcc
        CmdInvoice.Connection = Con
        CmdVat.Connection = Con
        CmdInvoice.CommandText = StrSql
        RdrOcc = CmdInvoice.ExecuteReader
        Dim Dt As New DataTable
        Dt.Load(RdrOcc)
        RdrOcc.Close()

        'To Update System Table with Next InvoiceNo.
        CmdInvoice.CommandText = "Select Max(InvoiceNo) From Billing Where Whcode  = '" & CmbZone.Text & "'"
        LngNextInvoice = CmdInvoice.ExecuteScalar + 1
        CmdInvoice.CommandText = "Update [System] Set NextInvoiceNo = " & LngNextInvoice & " Where WhCode = '" & CmbZone.Text & "'"
        CmdInvoice.ExecuteNonQuery()

        CmdInvoice.CommandText = "SELECT NextInvoiceNo FROM [System] WHERE WhCode = '" & CmbZone.Text & "'"
        LngInvoiceNo = CmdInvoice.ExecuteScalar
        For i = 0 To Dt.Rows.Count - 1
            StrClientID = Dt.Rows(i).Item(0)

            'Get client record for this client
            StrSql = "SELECT * FROM Client WHERE ClientId = '" & StrClientID & "'"
            CmdClient.CommandText = StrSql
            RdrClient = CmdClient.ExecuteReader
            If RdrClient.Read Then
                If RdrClient("Category") = "Freeport" Then
                    SngVat = 0
                Else
                    CmdVat.CommandText = "SELECT Vat FROM [System] WHERE WhCode = '" & CmbZone.Text & "'"
                    SngVat = CmdVat.ExecuteScalar
                End If
            End If

            StrSql = "SELECT * FROM Billing"
            StrSql = StrSql & " WHERE BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "'"
            StrSql = StrSql & " AND ClientID = '" & StrClientID & "'"
            StrSql = StrSql & " AND Billing.WHCode ='" & CmbZone.Text & "'"
            If IsRental Then
                StrSql = StrSql & " AND Billing.Unit = 'Cabin'"
            Else
                StrSql = StrSql & " AND Billing.Unit <> 'Cabin'"
            End If
            CmdInvoice.CommandText = StrSql
            Rdr = CmdInvoice.ExecuteReader
            SngAmount = 0
            IntTsu = 0
            While Rdr.Read
                If IsDBNull(Rdr("TSU")) OrElse Rdr("TSU") = 0 Then
                    SngAmount = SngAmount + Rdr("Amount")
                Else
                    IntTsu = IntTsu + Rdr("TSU")
                    If IntTsu > RdrClient("Discount") Then
                        SngAmount = SngAmount + Rdr("Amount")
                    End If
                End If
            End While
            Rdr.Close()
            If SngAmount > 0 Then
                If Not IsRental Then
                    StrSql = "UPDATE ContDetail SET InvoiceNo = " & LngInvoiceNo
                    StrSql = StrSql & " WHERE BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "'"
                    StrSql = StrSql & " AND ClientID = '" & StrClientID & "' And WhCode = '" & CmbZone.Text & "'"
                    CmdInvoice.CommandText = StrSql
                    CmdInvoice.ExecuteNonQuery()
                End If

                If IsRental Then
                    StrSql = "UPDATE Billing SET InvoiceNo = " & LngInvoiceNo & " WHERE BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy")
                    StrSql = StrSql & "' AND ClientID = '" & StrClientID & "' And WhCode = '" & CmbZone.Text & "'"
                    StrSql = StrSql & " AND Unit='Cabin'"
                    CmdInvoice.CommandText = StrSql
                    CmdInvoice.ExecuteNonQuery()
                Else
                    StrSql = "UPDATE Billing SET InvoiceNo = " & LngInvoiceNo & " WHERE BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy")
                    StrSql = StrSql & "' AND ClientID = '" & StrClientID & "' And WhCode = '" & CmbZone.Text & "'"
                    StrSql = StrSql & " AND Unit<>'Cabin'"
                    CmdInvoice.CommandText = StrSql
                    CmdInvoice.ExecuteNonQuery()
                End If

                If Not IsRental Then
                    StrSql = "UPDATE Logistics SET InvoiceNo = " & LngInvoiceNo
                    StrSql = StrSql & " WHERE RequestNo > 0  AND Month(ChDate) = " & CmbMonth.SelectedIndex
                    StrSql = StrSql & " AND Year(ChDate)= " & TxtYear.Text
                    StrSql = StrSql & " AND ClientID = '" & StrClientID & "' And WhCode = '" & CmbZone.Text & "'"
                    CmdInvoice.CommandText = StrSql
                    CmdInvoice.ExecuteNonQuery()
                End If

                'Create Invoice for the client
                StrSql = "INSERT INTO Invoice (InvoiceNo, ClientID, Amount, Vat, Value, InvDate, Month, Year, Paid, Deleted, WhCode) VALUES("
                StrSql = StrSql & LngInvoiceNo & ",'"
                StrSql = StrSql & StrClientID & "',"
                StrSql = StrSql & SngAmount & ","
                StrSql = StrSql & Math.Round(((SngAmount * SngVat) / 100), 2, MidpointRounding.AwayFromZero) & ","
                StrSql = StrSql & SngAmount + Math.Round(((SngAmount * SngVat) / 100), 2, MidpointRounding.AwayFromZero) & ",'"
                StrSql = StrSql & TxtInvDate.Text & "','"
                StrSql = StrSql & CmbMonth.Text & "',"
                StrSql = StrSql & TxtYear.Text & ","
                StrSql = StrSql & "0,"
                StrSql = StrSql & "0,'"
                StrSql = StrSql & CmbZone.Text & "')"
                CmdInvoice.CommandText = StrSql
                CmdInvoice.ExecuteNonQuery()

                If Not IsRental Then
                    If StrClientID = "400C9186" Then
                        If CmbZone.Text <> "FPZ06" Then
                            StrSql = "UPDATE Occupancy SET InvoiceNo = " & LngInvoiceNo & " WHERE ClientID = '" & StrClientID
                            StrSql = StrSql & "' AND BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "' And WhCode = '" & CmbZone.Text & "'"
                            CmdInvoice.CommandText = StrSql
                            CmdInvoice.ExecuteNonQuery()
                        End If
                    Else
                        StrSql = "UPDATE Occupancy SET InvoiceNo = " & LngInvoiceNo & " WHERE ClientID = '" & StrClientID
                        StrSql = StrSql & "' AND BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "' And WhCode = '" & CmbZone.Text & "'"
                        CmdInvoice.CommandText = StrSql
                        CmdInvoice.ExecuteNonQuery()
                    End If
                End If

                LngInvoiceNo = LngInvoiceNo + 1
                CmdSystem.CommandText = "Update [System] Set NextInvoiceNo = " & LngInvoiceNo & " Where WhCode = '" & CmbZone.Text & "'"
                CmdSystem.ExecuteNonQuery()
            End If
            RdrClient.Close()
        Next
        ButView_Click(Me, e)
        '#################### #################### INVOICES (END) #################### ####################
    End Sub
End Class