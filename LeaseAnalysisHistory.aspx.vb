﻿Partial Public Class LeaseAnalysisHistory
    Inherits System.Web.UI.Page
    Dim UserID As String = ""
    Dim StrSql As String
    Dim StrSqlWhere As String = ""
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Dim CmdR As New OleDb.OleDbCommand
    Dim RdrR As OleDb.OleDbDataReader

    Dim CmdReport As New OleDb.OleDbCommand

    Const MENUID = "LeaseAnalysisHistory"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            'StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            'Cmd.CommandText = StrSql
            'If Cmd.ExecuteScalar = 0 Then
            '    Response.Redirect("about:blank")
            'End If
            GetZones()
            GetWarehouses()
            GetCustomers()
            GetStatus()
            GetCategory()
        End If
    End Sub


    Private Sub PopulateData()
        Try
            CmdR.Connection = Con
            CmdReport.Connection = Con

            Dim FreeportDate As String = ""
            Dim StorageDate As String = ""
            Dim LeaseStart As String = ""
            Dim LeaseEnd As String = ""
            Dim MoveInDate As String = ""
            Dim MoveOutDate As String = ""


            'Clear reports
            StrSql = "DELETE FROM RptLeaseAnalysisHistory WHERE UserID='" & UserID & "'"
            CmdR.CommandText = StrSql
            CmdR.ExecuteNonQuery()

            'Retrieve Data
            StrSql = ""
            StrSql = StrSql & "SELECT Client.Name AS CName, Client.Category AS CCategory, Client.LicenceExp AS CLicenceExp, Client.PermitExp AS CPermitExp,"
            StrSql = StrSql & " LeaseAgreement.Zone AS LZone, LeaseAgreement.Area AS LArea, LeaseAgreement.LeaseNo AS LLeaseNo, LeaseAgreement.MonthlyRent AS LMonthlyRent, LeaseAgreement.Deposit AS LDeposit,"
            StrSql = StrSql & " LeaseAgreement.Insurance AS LInsurance, LeaseAgreement.ConCharges AS LConCharges, LeaseAgreement.Amount AS LAmount, LeaseAgreement.LeaseStatus AS LLeaseStatus,"
            StrSql = StrSql & " LeaseAgreement.PropCode AS LPropCode, LeaseAgreement.ProjectDesc AS LProjectDesc, LeaseAgreement.MoveInDate AS LMoveInDate, LeaseAgreement.MoveOutDate AS LMoveOutDate, LeaseAgreement.AddRemarks AS LAddRemarks,"
            StrSql = StrSql & " Property_1.Type AS PType, Property.LeaseStart AS PLeaseStart, Property.LeaseEnd AS PLeaseEnd"
            StrSql = StrSql & " FROM((LeaseAgreement LeaseAgreement INNER JOIN Client Client ON LeaseAgreement.ClientId=Client.ClientID)"
            StrSql = StrSql & " LEFT OUTER JOIN Property Property On LeaseAgreement.LeaseNo= Property.LeaseNo)"
            StrSql = StrSql & " INNER JOIN Property Property_1 ON LeaseAgreement.PropCode=Property_1.PropertyCode"

            StrSqlWhere = ""
            If CmbZone.Text <> "ALL" Then
                StrSqlWhere = StrSqlWhere & " WHERE LeaseAgreement.Zone = '" & CmbZone.Text & "'"
            End If

            If CmbWarehouse.Text <> "ALL" And StrSqlWhere = "" Then
                StrSqlWhere = StrSqlWhere & " WHERE Property.Type = '" & CmbWarehouse.Text & "'"
            ElseIf CmbWarehouse.Text <> "ALL" And StrSqlWhere <> "" Then
                StrSqlWhere = StrSqlWhere & " AND Property.Type = '" & CmbWarehouse.Text & "'"
            End If

            If cmbCustomer.Text <> "ALL" And StrSqlWhere = "" Then
                StrSqlWhere = StrSqlWhere & " WHERE Client.Name = '" & cmbCustomer.Text & "'"
            ElseIf cmbCustomer.Text <> "ALL" And StrSqlWhere <> "" Then
                StrSqlWhere = StrSqlWhere & " AND Client.Name = '" & cmbCustomer.Text & "'"
            End If

            If cmbStatus.Text <> "ALL" And StrSqlWhere = "" Then
                StrSqlWhere = StrSqlWhere & " WHERE LeaseAgreement.LeaseStatus = '" & cmbStatus.Text & "'"
            ElseIf cmbStatus.Text <> "ALL" And StrSqlWhere <> "" Then
                StrSqlWhere = StrSqlWhere & " AND LeaseAgreement.LeaseStatus = '" & cmbStatus.Text & "'"
            End If

            If CmbCategory.Text <> "ALL" And StrSqlWhere = "" Then
                StrSqlWhere = StrSqlWhere & " WHERE Client.Category = '" & CmbCategory.Text & "'"
            ElseIf CmbCategory.Text <> "ALL" And StrSqlWhere <> "" Then
                StrSqlWhere = StrSqlWhere & " AND Client.Category = '" & CmbCategory.Text & "'"
            End If

            If DtpStartDate.Text <> "" And StrSqlWhere = "" Then
                LeaseStart = String.Format("{0}-{1}-{2}", Mid(DtpStartDate.Text, 7, 4), Mid(DtpStartDate.Text, 4, 2), Mid(DtpStartDate.Text, 1, 2))
                StrSqlWhere = StrSqlWhere & " WHERE Property.LeaseStart >= '" & LeaseStart & "'"
            ElseIf DtpStartDate.Text <> "" And StrSqlWhere <> "" Then
                LeaseStart = String.Format("{0}-{1}-{2}", Mid(DtpStartDate.Text, 7, 4), Mid(DtpStartDate.Text, 4, 2), Mid(DtpStartDate.Text, 1, 2))
                StrSqlWhere = StrSqlWhere & " AND Property.LeaseStart >= '" & LeaseStart & "'"
            End If

            If DtpMoveInDate.Text <> "" And StrSqlWhere = "" Then
                MoveInDate = String.Format("{0}-{1}-{2}", Mid(DtpMoveInDate.Text, 7, 4), Mid(DtpMoveInDate.Text, 4, 2), Mid(DtpMoveInDate.Text, 1, 2))
                StrSqlWhere = StrSqlWhere & " WHERE LeaseAgreement.MoveInDate >= '" & MoveInDate & "'"
            ElseIf DtpMoveInDate.Text <> "" And StrSqlWhere <> "" Then
                MoveInDate = String.Format("{0}-{1}-{2}", Mid(DtpMoveInDate.Text, 7, 4), Mid(DtpMoveInDate.Text, 4, 2), Mid(DtpMoveInDate.Text, 1, 2))
                StrSqlWhere = StrSqlWhere & " AND LeaseAgreement.MoveInDate >= '" & MoveInDate & "'"
            End If

            If DtpEndDate.Text <> "" And StrSqlWhere = "" Then
                LeaseEnd = String.Format("{0}-{1}-{2}", Mid(DtpEndDate.Text, 7, 4), Mid(DtpEndDate.Text, 4, 2), Mid(DtpEndDate.Text, 1, 2))
                StrSqlWhere = StrSqlWhere & " WHERE Property.LeaseEnd >= '" & LeaseEnd & "'"
            ElseIf DtpEndDate.Text <> "" And StrSqlWhere <> "" Then
                LeaseEnd = String.Format("{0}-{1}-{2}", Mid(DtpEndDate.Text, 7, 4), Mid(DtpEndDate.Text, 4, 2), Mid(DtpEndDate.Text, 1, 2))
                StrSqlWhere = StrSqlWhere & " AND Property.LeaseEnd >= '" & LeaseEnd & "'"
            End If

            If DtpMoveOutDate.Text <> "" And StrSqlWhere = "" Then
                MoveOutDate = String.Format("{0}-{1}-{2}", Mid(DtpMoveOutDate.Text, 7, 4), Mid(DtpMoveOutDate.Text, 4, 2), Mid(DtpMoveOutDate.Text, 1, 2))
                StrSqlWhere = StrSqlWhere & " WHERE LeaseAgreement.MoveOutDate >= '" & MoveOutDate & "'"
            ElseIf DtpMoveOutDate.Text <> "" And StrSqlWhere <> "" Then
                MoveOutDate = String.Format("{0}-{1}-{2}", Mid(DtpMoveOutDate.Text, 7, 4), Mid(DtpMoveOutDate.Text, 4, 2), Mid(DtpMoveOutDate.Text, 1, 2))
                StrSqlWhere = StrSqlWhere & " AND LeaseAgreement.MoveOutDate >= '" & MoveOutDate & "'"
            End If

            StrSql = StrSql & StrSqlWhere
            StrSql = StrSql & " ORDER BY Client.Name"

            CmdR.CommandText = StrSql
            RdrR = CmdR.ExecuteReader
            While RdrR.Read
                'Create data for reports
                FreeportDate = ""
                StorageDate = ""
                LeaseStart = ""
                LeaseEnd = ""
                MoveInDate = ""
                MoveOutDate = ""

                StrSql = "INSERT INTO RptLeaseAnalysisHistory VALUES ("
                StrSql = StrSql & "'" & UserID & "',"
                'Client
                StrSql = StrSql & "'" & RdrR("CName") & "',"
                StrSql = StrSql & "'" & RdrR("CCategory") & "',"

                'StrSql = StrSql & "" & RdrR("CLicenceExp") & ","
                If Not IsDBNull(RdrR("CLicenceExp")) Then
                    FreeportDate = String.Format("{0}-{1}-{2}", Mid(RdrR("CLicenceExp"), 7, 4), Mid(RdrR("CLicenceExp"), 4, 2), Mid(RdrR("CLicenceExp"), 1, 2))
                End If
                StrSql = StrSql & "'" & FreeportDate & "',"


                'StrSql = StrSql & "" & RdrR("CPermitExp") & ","
                If Not IsDBNull(RdrR("CPermitExp")) Then
                    StorageDate = String.Format("{0}-{1}-{2}", Mid(RdrR("CPermitExp"), 7, 4), Mid(RdrR("CPermitExp"), 4, 2), Mid(RdrR("CPermitExp"), 1, 2))
                End If
                StrSql = StrSql & "'" & StorageDate & "',"

                'Lease Agreement
                StrSql = StrSql & "'" & RdrR("LZone") & "',"
                StrSql = StrSql & "" & RdrR("LArea") & ","
                StrSql = StrSql & "'" & RdrR("LLeaseNo") & "',"
                StrSql = StrSql & "" & RdrR("LMonthlyRent") & ","
                StrSql = StrSql & "" & RdrR("LDeposit") & ","
                StrSql = StrSql & "" & RdrR("LInsurance") & ","
                StrSql = StrSql & "" & RdrR("LConCharges") & ","
                StrSql = StrSql & "" & RdrR("LAmount") & ","
                StrSql = StrSql & "'" & RdrR("LLeaseStatus") & "',"
                StrSql = StrSql & "'" & RdrR("LPropCode") & "',"
                StrSql = StrSql & "'" & RdrR("LProjectDesc") & "',"

                'StrSql = StrSql & "" & RdrR("LMoveInDate") & ","
                If Not IsDBNull(RdrR("LMoveInDate")) Then
                    MoveInDate = String.Format("{0}-{1}-{2}", Mid(RdrR("LMoveInDate"), 7, 4), Mid(RdrR("LMoveInDate"), 4, 2), Mid(RdrR("LMoveInDate"), 1, 2))
                End If
                StrSql = StrSql & "'" & MoveInDate & "',"

                'StrSql = StrSql & "" & RdrR("LMoveOutDate") & ","
                If Not IsDBNull(RdrR("LMoveOutDate")) Then
                    MoveOutDate = String.Format("{0}-{1}-{2}", Mid(RdrR("LMoveOutDate"), 7, 4), Mid(RdrR("LMoveOutDate"), 4, 2), Mid(RdrR("LMoveOutDate"), 1, 2))
                End If
                StrSql = StrSql & "'" & MoveOutDate & "',"

                StrSql = StrSql & "'" & RdrR("LAddRemarks") & "',"
                'StrSql = StrSql & "'" & "testA" & "',"
                'Property
                StrSql = StrSql & "'" & RdrR("PType") & "',"

                'StrSql = StrSql & "" & RdrR("PLeaseStart") & ","
                If Not IsDBNull(RdrR("PLeaseStart")) Then
                    LeaseStart = String.Format("{0}-{1}-{2}", Mid(RdrR("PLeaseStart"), 7, 4), Mid(RdrR("PLeaseStart"), 4, 2), Mid(RdrR("PLeaseStart"), 1, 2))
                End If
                StrSql = StrSql & "'" & LeaseStart & "',"

                'StrSql = StrSql & "" & RdrR("PLeaseEnd") & ""
                If Not IsDBNull(RdrR("PLeaseEnd")) Then
                    LeaseEnd = String.Format("{0}-{1}-{2}", Mid(RdrR("PLeaseEnd"), 7, 4), Mid(RdrR("PLeaseEnd"), 4, 2), Mid(RdrR("PLeaseEnd"), 1, 2))
                End If
                StrSql = StrSql & "'" & LeaseEnd & "'"

                StrSql = StrSql & ")"
                CmdReport.CommandText = StrSql
                CmdReport.ExecuteNonQuery()
            End While
            RdrR.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            UserID = Session("UserID")
            PopulateData()
            LoadCrystalReport()
        End If
    End Sub


    Private Sub LoadCrystalReport()
        Dim StrSelFormula As String
        Session("ReportFile") = "RptLeaseAnalysisHistory.rpt"

        StrSelFormula = "{RptLeaseAnalysisHistory.UserID} = '" & UserID & "' "

        Session("Period") = ""
        Session("Zone") = ""
        Session("SelFormula") = StrSelFormula
        Session("RepHead") = "Lease Analysis History"
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub

    Private Sub LoadReportArchive()
        Dim StartDate As String
        Dim EndDate As String
        Dim MoveInDate As String
        Dim MoveOutDate As String

        Dim StrSelFormula As String
        Session("ReportFile") = "RptLeaseAnalysisHistory(OLD).rpt"

        StrSelFormula = ""

        If CmbZone.Text <> "ALL" Then
            StrSelFormula = "{LeaseAgreement.Zone} = '" & CmbZone.Text & "' "
        End If

        If CmbWarehouse.Text <> "ALL" Then
            StrSelFormula = StrSelFormula & "AND {Property.Type} = '" & CmbWarehouse.Text & "' "
        End If

        If cmbCustomer.Text <> "ALL" Then
            StrSelFormula = StrSelFormula & "AND {Client.Name} = '" & cmbCustomer.Text & "' "
        End If

        If cmbStatus.Text <> "ALL" Then
            StrSelFormula = StrSelFormula & "AND {LeaseAgreement.LeaseStatus} = '" & cmbStatus.Text & "' "
        End If

        If CmbCategory.Text <> "ALL" Then
            StrSelFormula = StrSelFormula & "AND {Client.Category} = '" & CmbCategory.Text & "' "
        End If

        'If DtpStartDate.Text <> "" Then
        '    StartDate = Mid(DtpStartDate.Text, 7, 4) & "," & Mid(DtpStartDate.Text, 4, 2) & "," & Mid(DtpStartDate.Text, 1, 2)
        '    StrSelFormula = StrSelFormula & "AND {Property.LeaseStart} >= Date(" & StartDate & ") "
        'End If
        'If DtpMoveInDate.Text <> "" Then
        '    MoveInDate = Mid(DtpMoveInDate.Text, 7, 4) & "," & Mid(DtpMoveInDate.Text, 4, 2) & "," & Mid(DtpMoveInDate.Text, 1, 2)
        '    StrSelFormula = StrSelFormula & "AND {LeaseAgreement.MoveInDate} >= Date(" & MoveInDate & ") "
        'End If

        'If DtpEndDate.Text <> "" Then
        '    EndDate = Mid(DtpEndDate.Text, 7, 4) & "," & Mid(DtpEndDate.Text, 4, 2) & "," & Mid(DtpEndDate.Text, 1, 2)
        '    StrSelFormula = StrSelFormula & "AND {Property.LeaseEnd} <= Date(" & EndDate & ") "
        'End If
        'If DtpMoveOutDate.Text <> "" Then
        '    MoveOutDate = Mid(DtpMoveOutDate.Text, 7, 4) & "," & Mid(DtpMoveOutDate.Text, 4, 2) & "," & Mid(DtpMoveOutDate.Text, 1, 2)
        '    StrSelFormula = StrSelFormula & "AND {LeaseAgreement.MoveOutDate} <= Date(" & MoveOutDate & ") "
        'End If

        If DtpStartDate.Text <> "" Then
            StartDate = String.Format("{0}-{1}-{2}", Mid(DtpStartDate.Text, 7, 4), Mid(DtpStartDate.Text, 4, 2), Mid(DtpStartDate.Text, 1, 2))
            StrSelFormula = StrSelFormula & "AND {Property.LeaseStart} >= #" & StartDate & "# "
        End If
        If DtpMoveInDate.Text <> "" Then
            MoveInDate = String.Format("{0}-{1}-{2}", Mid(DtpMoveInDate.Text, 7, 4), Mid(DtpMoveInDate.Text, 4, 2), Mid(DtpMoveInDate.Text, 1, 2))
            StrSelFormula = StrSelFormula & "AND {LeaseAgreement.MoveInDate} >= #" & MoveInDate & "# "
        End If

        If DtpEndDate.Text <> "" Then
            EndDate = String.Format("{0}-{1}-{2}", Mid(DtpEndDate.Text, 7, 4), Mid(DtpEndDate.Text, 4, 2), Mid(DtpEndDate.Text, 1, 2))
            StrSelFormula = StrSelFormula & "AND {Property.LeaseEnd} <= #" & EndDate & "# "
        End If
        If DtpMoveOutDate.Text <> "" Then
            MoveOutDate = String.Format("{0}-{1}-{2}", Mid(DtpMoveOutDate.Text, 7, 4), Mid(DtpMoveOutDate.Text, 4, 2), Mid(DtpMoveOutDate.Text, 1, 2))
            StrSelFormula = StrSelFormula & "AND {LeaseAgreement.MoveOutDate} <= #" & MoveOutDate & "# "
        End If

        StrSelFormula = LTrim(StrSelFormula)

        If StrSelFormula.StartsWith("AND") Then
            StrSelFormula = StrSelFormula.Substring(4)
        End If

        Session("Period") = ""
        Session("Zone") = ""
        Session("SelFormula") = StrSelFormula
        Session("RepHead") = "Lease Analysis History"
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub
    Private Sub GetZones()
        CmbZone.Items.Clear()

        StrSql = "SELECT DISTINCT([zone]) FROM LeaseAgreement WHERE [zone] IS NOT NULL AND [zone] <> '' ORDER BY [zone]"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        CmbZone.Items.Add("ALL")
        While Rdr.Read
            CmbZone.Items.Add(Rdr("zone"))
        End While
        Rdr.Close()
    End Sub

    Private Sub GetWarehouses()
        CmbWarehouse.Items.Clear()

        Cmd.Connection = Con

        StrSql = "SELECT DISTINCT([Type]) FROM Property"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        CmbWarehouse.Items.Add("ALL")

        While Rdr.Read
            CmbWarehouse.Items.Add(Rdr("Type"))
        End While
        Rdr.Close()
    End Sub

    Private Sub GetCustomers()
        cmbCustomer.Items.Clear()

        Cmd.Connection = Con

        StrSql = "Select DISTINCT(c.[Name]) as CustomerName FROM LeaseAgreement l "
        StrSql = StrSql & "INNER Join Property p ON p.PropertyCode = l.PropCode "
        StrSql = StrSql & "INNER Join Client c on c.ClientID = l.ClientId "
        StrSql = StrSql & "WHERE p.Type = 'Warehouse' "
        StrSql = StrSql & "ORDER BY c.[Name]"

        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        cmbCustomer.Items.Add("ALL")

        While Rdr.Read
            cmbCustomer.Items.Add(Rdr("CustomerName"))
        End While
        Rdr.Close()
    End Sub

    Private Sub GetStatus()
        cmbStatus.Items.Clear()

        StrSql = "SELECT DISTINCT(LeaseStatus) AS LeaseStatus FROM LeaseAgreement "
        StrSql = StrSql & "WHERE LeaseStatus IS NOT NULL "
        StrSql = StrSql & "ORDER BY LeaseStatus"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        cmbStatus.Items.Add("ALL")
        While Rdr.Read
            cmbStatus.Items.Add(Rdr("LeaseStatus"))
        End While
        Rdr.Close()
    End Sub

    Private Sub GetCategory()
        CmbCategory.Items.Clear()

        StrSql = "SELECT DISTINCT(Category) AS Category FROM Regime "
        StrSql = StrSql & "WHERE Category <> '' "
        StrSql = StrSql & "ORDER BY Category"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        CmbCategory.Items.Add("ALL")
        While Rdr.Read
            CmbCategory.Items.Add(Rdr("Category"))
        End While
        Rdr.Close()
    End Sub
End Class