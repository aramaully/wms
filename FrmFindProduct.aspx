﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmFindProduct.aspx.vb" Inherits="Wms.FrmFindProduct" %>
<%@ Register Assembly="EO.Web" Namespace="EO.Web" TagPrefix="eo" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Find Product</title>
    <script language="javascript" type="text/javascript">
        function GetID() {
            if (window.opener.document.getElementById("TxtProductCode") == null) {
                var Target = window.opener.document.getElementById("TabContainer1_TabPanel2_TxtProductCode");
            }
            else {
                var Target = window.opener.document.getElementById("TxtProductCode");
            }
            Target.value = document.getElementById("TxtProductCode").value;
            Target.onchange();


        }
         function MeOnTop() {
             if (document.getElementById("TxtSearch").focused) { }
             else {
                     self.focus();
             }
         }
    </script>
</head>
<body style="margin-top: 0" onblur="MeOnTop();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript">
        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('PnlGrid').scrollLeft;
            yPos = $get('PnlGrid').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('PnlGrid').scrollLeft = xPos;
            $get('PnlGrid').scrollTop = yPos;
        }
</script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Width="550px" Height="400px">
                <eo:ToolBar ID="ToolBar1" runat="server" Width="400px" BackgroundImage="00100103"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    AutoPostBack="True">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="Refresh" ImageUrl="~/images/RefreshDocViewHS.png" ToolTip="Refresh Grid">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Select" ImageUrl="~/images/TaskHS.png" ToolTip="Select Client">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Cancel" ImageUrl="~/images/delete.png" ToolTip="Cancel">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
                <table width="100%">
                    <tr>
                        <td>
                            Selection Criteria</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:TextBox ID="TxtSearch" runat="server" Width="250px" onblur="self.focus()" 
                                AutoPostBack="True"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="PnlGrid" runat="server" Width="100%" Height="365px" ScrollBars="Auto"
                    BorderStyle="Inset" BorderWidth="1">
                    <asp:GridView ID="GdvProduct" runat="server" AutoGenerateColumns="False" 
                        Width="100%">
                        <HeaderStyle BackColor="#000099" ForeColor="White" Font-Size="Smaller" />
                        <AlternatingRowStyle BackColor="LightBlue" />
                        <SelectedRowStyle BackColor="Yellow" />
                        <Columns>
                            <asp:BoundField DataField="ProdCode" HeaderText="Product Code" />
                            <asp:BoundField DataField="Description" HeaderText="Description" />
                            <asp:BoundField DataField="Unit" HeaderText="Unit" />
                        </Columns>
                    </asp:GridView>
                    <asp:HiddenField ID="TxtProductCode" runat="server" />
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div >
    </div>
    </form>
     <script type="text/javascript">
         var prm = Sys.WebForms.PageRequestManager.getInstance();
         prm.add_initializeRequest(prm_InitializeRequest);
         prm.add_endRequest(prm_EndRequest);

         function prm_InitializeRequest(sender, args) {
             var panelProg = $get('divImage');
             if (args._postBackElement.id != "Timer1") {
                 panelProg.style.display = '';
             }
         };

         function prm_EndRequest(sender, args) {
             var panelProg = $get('divImage');
             panelProg.style.display = 'none';
         };
    </script>
</body>
</html>
