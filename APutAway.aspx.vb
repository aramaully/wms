﻿Public Partial Class APutAway
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdPrev As New OleDb.OleDbCommand
    Dim CmdNext As New OleDb.OleDbCommand
    Dim StrSql As String

    Const MENUID = "AutomaticPutAway"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If
            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbFpZone.Items.Add("Select...")
            While Rdr.Read
                CmbFpZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            StrSql = "SELECT ZoneCode FROM Zones"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("ZoneCode"))
            End While
            Rdr.Close()
            StrSql = "SELECT LocCode FROM Location WHERE (RackCode  Is Null OR RackCode = '')"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbRack.Items.Add("Select...")
            While Rdr.Read
                CmbRack.Items.Add(Rdr("LocCode"))
            End While
            'TxtLotNo.Style("text-align") = "right"
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ProdCode,Description,HsCode,Unit,SubUnit FROM Product WHERE "
        StrSql = "SELECT BoeIn.BillNo, CONVERT(Char,BoeIn.BillDate,103) 'Bill Date', BoeIn.Revision, Client.Name FROM BoeIn, Client WHERE "
        StrSql = StrSql & "BoeIn.ClientID = Client.ClientID "
        If RbnClient.Checked Then
            StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCountry.Checked Then
            StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnTraffic.Checked Then
            StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCont.Checked Then
            StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
        End If
        If ChkDate.Checked Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = StrSql & "AND Boein.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND Boein.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        PnlFind.Style.Item("display") = ""
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtBillNo.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtBillNo_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtBillNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBillNo.TextChanged
        StrSql = "SELECT BoeIn.WhCode,BoeIn.Revision,BoeIn.Billdate,Client.Name FROM BoeIn,Client "
        StrSql = StrSql & "WHERE BoeIn.ClientId=Client.ClientID AND  BoeIn.BillNo = '" & TxtBillNo.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        On Error Resume Next
        If Rdr.Read Then
            TxtRevision.Text = Rdr("Revision").ToString
            TxtDate.Text = Rdr("BillDate")
            TxtName.Text = Rdr("Name").ToString
            CmbFpZone.Text = Rdr("WhCode").ToString
        Else
            TxtName.Text = "Invalid Inbound"
            Exit Sub
        End If
        Rdr.Close()
        'Update Products in the Drop Down
        StrSql = "SELECT PCode FROM BoeInProd WHERE BoeNo = '" & TxtBillNo.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        CmbPCodeFrom.Items.Add("Select...")
        CmbPcodeTo.Items.Add("Select...")
        While Rdr.Read
            CmbPCodeFrom.Items.Add(Rdr("PCode"))
            CmbPcodeTo.Items.Add(Rdr("PCode"))
        End While
        Rdr.Close()
        GetPallets()
        ButSave.Enabled = False
    End Sub

    Private Sub GetPallets()
        StrSql = "SELECT DateIn,* FROM Pallets WHERE PutAway = 0  AND BoeNo = '" & TxtBillNo.Text & "' "
        StrSql = StrSql & " ORDER BY PalletID"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvPallet.DataSource = Rdr
        GdvPallet.DataBind()
        Rdr.Close()
        Rdr = Cmd.ExecuteReader
        While Rdr.Read
            TxtDateIn.Text = Rdr("DateIn")
            If TxtPalletFrom.Text = "" Then
                TxtPalletFrom.Text = Rdr("PalletID")
            End If
            If TxtPalletTo.Text = "" Then
                TxtPalletTo.Text = Rdr("PalletID")
            Else
                If Rdr("PalletID") > TxtPalletTo.Text Then
                    TxtPalletTo.Text = Rdr("PalletID")
                End If
            End If
        End While
        Rdr.Close()
    End Sub

    Protected Sub ChkRack_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ChkRack.CheckedChanged
        If ChkRack.Checked Then
            CmbRack.Enabled = False
        Else
            CmbRack.Enabled = True
        End If
    End Sub

    Protected Sub ButShowLoc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButShowLoc.Click
        Dim StrPallet As String
        Dim SngPratio As Single
        Dim StrRack As String
        Dim SngAvailable As Single
        Dim I As Integer
        Dim StrAssigned As String = "''"
        Try
            For Each Row As GridViewRow In GdvPallet.Rows
                StrPallet = Row.Cells(0).Text
                If StrPallet >= TxtPalletFrom.Text And StrPallet <= TxtPalletTo.Text Then
                    SngPratio = Val(Row.Cells(1).Text)
                    StrSql = "SELECT TOP 1 MAX(RackCode) FROM Location WHERE RackCode IS NOT NULL AND RackCode <> '' "
                    If TxtRackFrom.Text <> "" Then
                        StrSql = StrSql & "AND RackCode >= '" & TxtRackFrom.Text & "' "
                    End If
                    If TxtRackTo.Text <> "" Then
                        StrSql = StrSql & "AND RackCode <= '" & TxtRackTo.Text & "' "
                    End If
                    If TxtAisleFrom.Text <> "" Then
                        StrSql = StrSql & "AND Aisle >= " & TxtAisleFrom.Text & " "
                    End If
                    If TxtAisleTo.Text <> "" Then
                        StrSql = StrSql & "AND Aisle <= " & TxtAisleTo.Text & " "
                    End If
                    If TxtSideFrom.Text <> "" Then
                        StrSql = StrSql & "AND Side >= " & TxtSideFrom.Text & " "
                    End If
                    If TxtSideTo.Text <> "" Then
                        StrSql = StrSql & "AND Side <= " & TxtSideTo.Text & " "
                    End If
                    If TxtBayFrom.Text <> "" Then
                        StrSql = StrSql & "AND bay >= " & TxtBayFrom.Text & " "
                    End If
                    If TxtBayTo.Text <> "" Then
                        StrSql = StrSql & "AND bay <= " & TxtBayTo.Text & " "
                    End If
                    If TxtlevelFrom.Text <> "" Then
                        StrSql = StrSql & "AND level >= " & TxtlevelFrom.Text & " "
                    End If
                    If TxtLevelTo.Text <> "" Then
                        StrSql = StrSql & "AND level <= " & TxtLevelTo.Text & " "
                    End If
                    If TxtLocationFrom.Text <> "" Then
                        StrSql = StrSql & "AND LocCode >= '" & TxtLocationFrom.Text & "' "
                    End If
                    If TxtLocationTo.Text <> "" Then
                        StrSql = StrSql & "AND LocCode <= '" & TxtLocationTo.Text & "' "
                    End If
                    StrSql = StrSql & "AND LocCode NOT IN (" & StrAssigned & ") "
                    StrSql = StrSql & "GROUP BY RackCode HAVING SUM(MaxPRatio-CurPRatio) >= " & SngPratio
                    Cmd.Connection = Con
                    Cmd.CommandText = StrSql
                    StrRack = Cmd.ExecuteScalar
                    StrSql = "SELECT * FROM Location WHERE RackCode = '" & StrRack & "' "
                    StrSql = StrSql & "AND LocCode NOT IN (" & StrAssigned & ") ORDER BY LocCode"
                    Cmd.CommandText = StrSql
                    Rdr = Cmd.ExecuteReader
                    I = 2
                    While Rdr.Read
                        SngAvailable = Rdr("MaxPRatio") - Rdr("CurPRatio")
                        If SngAvailable > 0 Then
                            If SngPratio <= SngAvailable Then
                                Row.Cells(I).Text = Rdr("LocCode")
                                Row.Cells(I + 1).Text = SngPratio
                                Row.Cells(9).Text = Rdr("ZoneCode")
                                StrAssigned = StrAssigned & ",'" & Rdr("LocCode") & "'"
                                Exit While
                            Else
                                Row.Cells(I).Text = Rdr("LocCode")
                                Row.Cells(I + 1).Text = SngAvailable
                                Row.Cells(9).Text = Rdr("ZoneCode")
                                SngPratio = SngPratio - SngAvailable
                                StrAssigned = StrAssigned & ",'" & Rdr("LocCode") & "'"
                                If SngPratio = 0 Then Exit While
                            End If
                        End If
                        I = I + 2
                    End While
                    Rdr.Close()
                End If
            Next
            ButSave.Enabled = True
        Catch ex As Exception
            ErrHead.Text = "Auto Putaway Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            For Each Row As GridViewRow In GdvPallet.Rows
                'If a Location has been assigned earlier then release that location
                StrSql = "UPDATE Location SET CurPRatio = CurPRatio - " & Val(Row.Cells(3).Text) & " WHERE LocCode = "
                StrSql = StrSql & "(SELECT LocCode1 FROM Pallets WHERE PalletID = '" & ChkSpace(Row.Cells(0).Text) & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                StrSql = "UPDATE Location SET CurPRatio = CurPRatio - " & Val(Row.Cells(5).Text) & " WHERE LocCode = "
                StrSql = StrSql & "(SELECT LocCode2 FROM Pallets WHERE PalletID = '" & ChkSpace(Row.Cells(0).Text) & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                StrSql = "UPDATE Location SET CurPRatio = CurPRatio - " & Val(Row.Cells(7).Text) & " WHERE LocCode = "
                StrSql = StrSql & "(SELECT LocCode3 FROM Pallets WHERE PalletID = '" & ChkSpace(Row.Cells(0).Text) & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                'Update Current Ratio of Locations assigned
                If ChkSpace(Row.Cells(2).Text) <> "" Then
                    StrSql = "UPDATE Location SET CurPRatio = CurPRatio + " & Val(Row.Cells(3).Text) & "WHERE LocCode = '" & Row.Cells(2).Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
                If ChkSpace(Row.Cells(4).Text) <> "" Then
                    StrSql = "UPDATE Location SET CurPRatio = CurPRatio + " & Val(Row.Cells(5).Text) & "WHERE LocCode = '" & Row.Cells(4).Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
                If ChkSpace(Row.Cells(6).Text) <> "" Then
                    StrSql = "UPDATE Location SET CurPRatio = CurPRatio + " & Val(Row.Cells(7).Text) & "WHERE LocCode = '" & Row.Cells(6).Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
                'Update Pallets with new locations and pallet ratios
                If ChkSpace(Row.Cells(2).Text) <> "" Then
                    StrSql = "UPDATE Pallets SET LocCode1 = '" & ChkSpace(Row.Cells(2).Text) & "',"
                    StrSql = StrSql & "LocCode2 = '" & ChkSpace(Row.Cells(4).Text) & "',"
                    StrSql = StrSql & "LocCode3 = '" & ChkSpace(Row.Cells(6).Text) & "',"
                    StrSql = StrSql & "DateIn = '" & ChkSpace(Row.Cells(8).Text) & "',"
                    StrSql = StrSql & "PRatio1 = " & Val(Row.Cells(3).Text) & ","
                    StrSql = StrSql & "PRatio2 = " & Val(Row.Cells(5).Text) & ","
                    StrSql = StrSql & "PRatio3 = " & Val(Row.Cells(7).Text) & ","
                    StrSql = StrSql & "ZoneCode = '" & ChkSpace(Row.Cells(9).Text) & "',"
                    StrSql = StrSql & "PutAway = 1,Cleared = 0 "
                    StrSql = StrSql & "WHERE PalletID = " & ChkSpace(Row.Cells(0).Text)
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            Trans.Commit()
            TxtBillNo_TextChanged(Me, e)
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Putaway Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Function ChkSpace(ByVal StrText As String) As String
        If StrText = "&nbsp;" Then
            Return ""
        Else
            Return StrText
        End If
    End Function

End Class