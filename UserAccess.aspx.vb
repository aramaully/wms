﻿Public Partial Class UserAccess
    Inherits System.Web.UI.Page

    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT * FROM Profil ORDER BY ProfilID"
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                Dim tempList As New ListItem
                tempList.Text = Rdr("Profils")
                tempList.Value = Rdr("ProfilID")
                Me.CmbUserID.Items.Add(tempList)
            End While
            Rdr.Close()
            CmbUserID.Text = "Select..."
            FillGrid()
        End If
    End Sub

    Private Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT Text,Description,MenuID,ParentID,Navigate FROM Menu WHERE ParentID IS NOT NULL ORDER BY Text"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        Dim DsUser As New DataSet()
        Dim CmdUserA As New OleDb.OleDbDataAdapter(StrSql, Con)
        CmdUserA.Fill(DsUser)
        GDAcces.DataSource = DsUser
        GDAcces.DataBind()
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub CmbUserID_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbUserID.SelectedIndexChanged
        StrSql = "SELECT * FROM [User] WHERE UserID='" & Me.CmbUserID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtName.Text = Rdr("Name")
            TxtWhCode.Text = Rdr("WhCode")
        End If
        Rdr.Close()

        For i As Integer = 0 To GDAcces.Rows.Count - 1
            DirectCast(GDAcces.Rows(i).FindControl("chkSelect"), CheckBox).Checked = False
        Next
        StrSql = "SELECT Text,Description,MenuID FROM Access WHERE UserID='" & Me.CmbUserID.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        While Rdr.Read
            For i As Integer = 0 To GDAcces.Rows.Count - 1
                Dim row As GridViewRow = GDAcces.Rows(i)
                If row.Cells(3).Text = Rdr("MenuID").ToString Then
                    DirectCast(row.FindControl("chkSelect"), CheckBox).Checked = True
                End If
            Next
        End While

    End Sub

    Private Sub GDAcces_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GDAcces.RowDataBound
        On Error Resume Next
        e.Row.Cells(3).Visible = False
        e.Row.Cells(4).Visible = False
        e.Row.Cells(5).Visible = False
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        If Me.CmbUserID.Text = "Select..." Then
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = "Select User ID"
            ModalPopupExtender1.Show()
            Exit Sub
        End If
        Dim Trans As OleDb.OleDbTransaction
        Dim CurDateTime As DateTime
        Dim StrEvent As String = ""
        Cmd.Connection = Con
        StrSql = "SELECT GETDATE() As SysDate"
        Cmd.CommandText = StrSql
        CurDateTime = Cmd.ExecuteScalar
        Cmd.Connection = Con
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Transaction = Trans
        Try
            Cmd.CommandText = "DELETE FROM Access WHERE USerID='" & Me.CmbUserID.Text & "'"
            Cmd.ExecuteNonQuery()
            For i As Integer = 0 To GDAcces.Rows.Count - 1
                Dim row As GridViewRow = GDAcces.Rows(i)
                If DirectCast(row.FindControl("chkSelect"), CheckBox).Checked = True Then
                    StrSql = "INSERT INTO Access(UserID,MenuID,Text,Description,ParentID, Navigate) Values('"
                    StrSql = StrSql & Me.CmbUserID.Text & "','"
                    StrSql = StrSql & row.Cells(3).Text & "','"
                    StrSql = StrSql & row.Cells(1).Text & "','"
                    StrSql = StrSql & row.Cells(2).Text & "',"
                    If row.Cells(4).Text = "&nbsp;" Then
                        StrSql = StrSql & "Null,"
                    Else
                        StrSql = StrSql & "'" & row.Cells(4).Text & "',"
                    End If
                    If row.Cells(5).Text = "&nbsp;" Then
                        StrSql = StrSql & "Null)"
                    Else
                        StrSql = StrSql & "'" & row.Cells(5).Text & "')"
                    End If
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            'StrEvent = "User access level changed for the user " & CmbUserID.Text
            'StrSql = "INSERT INTO Audit(UserID,DoneAt,Event)VALUES('"
            'StrSql = StrSql & Session("UserID") & "','"
            'StrSql = StrSql & Format(CurDateTime, "MM/dd/yyyy HH:mm:ss") & "','"
            'StrSql = StrSql & StrEvent & "')"
            'Cmd.CommandText = StrSql
            'Cmd.ExecuteNonQuery()
            Trans.Commit()
            Me.CmbUserID.SelectedIndex = 0
            TxtWhCode.Text = ""
            TxtName.Text = ""
            For i As Integer = 0 To GDAcces.Rows.Count - 1
                DirectCast(GDAcces.Rows(i).FindControl("chkSelect"), CheckBox).Checked = False
            Next
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class