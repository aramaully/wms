﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SystemSetup.aspx.vb" Inherits="Wms.SystemSetup" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>System Setup</title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            height: 47px;
        }
        .ModalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .style19
        {
            width: 16px;
        }
        .style21
        {
        }
        .style23
        {
        }
        .style27
        {
            width: 208px;
        }
    </style>
</head>
    <body style="height: 100%">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 608px">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 102; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" Width="75%" Height="488px" BorderStyle="Groove"
                    BorderWidth="1px">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="System Setup"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Product File" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save this Product?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButExit" runat="server" Height="39px" Text="Close" Style="cursor: pointer"
                                    ToolTip="Close this Module" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButExit_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Close this Module?"
                                    Enabled="True" TargetControlID="ButExit">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    
                    <table class="style1">
                        <tr>
                            <td width="15%">
                                FPZone</td>
                            <td class="style27" width="35%">
                                <asp:DropDownList ID="CmbWHouse" runat="server" Width="128px" 
                                    AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td width="15%">
                                &nbsp;</td>
                            <td width="35%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Name</td>
                            <td class="style21" colspan="2" width="35%">
                                <asp:TextBox ID="TxtName" runat="server" Width="99%"></asp:TextBox>
                            </td>
                            <td width="35%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Address</td>
                            <td class="style21" colspan="2" width="35%">
                                <asp:TextBox ID="TxtAddress1" runat="server" Width="99%"></asp:TextBox>
                            </td>
                            
                            <td  valign="top" width="35%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="15%">
                                &nbsp;</td>
                            <td class="style21" width="35%" colspan="2">
                                <asp:TextBox ID="TxtAddress2" runat="server" Width="99%"></asp:TextBox>
                            </td>
                            <td width="15%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="15%">
                                &nbsp;</td>
                            <td class="style21" width="35%" colspan="2">
                                <asp:TextBox ID="TxtAddress3" runat="server" Width="99%"></asp:TextBox>
                            </td>
                           
                            <td width="15%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Telephone No</td>
                            <td class="style21" width="35%" colspan="2">
                                <asp:TextBox ID="TxtPhone" runat="server" Width="99%" style="text-align:right"></asp:TextBox>
                            </td>
                            
                            <td width="35%">
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Telefax No</td>
                            <td class="style21" width="35%" colspan="2">
                                <asp:TextBox ID="TxtFax" Width="99%" runat="server" style="text-align:right"></asp:TextBox>
                            </td>
                            
                            <td width="35%">
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Email</td>
                            <td class="style21" width="35%" colspan="2">
                                <asp:TextBox ID="TxtEmail" Width="99%" runat="server" style="text-align:right"></asp:TextBox>
                            </td>
                           
                            <td width="35%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Web Site</td>
                            <td class="style21" width="35%" colspan="2">
                                <asp:TextBox ID="TxtWeb" Width="99%" runat="server"></asp:TextBox>
                            </td>
                           
                            <td width="35%">
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Next Pallet No</td>
                            <td class="style27" width="35%">
                                <asp:TextBox ID="TxtNextPalletNo" runat="server"></asp:TextBox>
                            </td>
                            <td width="15%">
                                Next Invoice No</td>
                            <td width="35%">
                                <asp:TextBox ID="TxtNextInvoiceNo" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Vat Rate</td>
                            <td class="style23" width="35%">
                                <asp:TextBox ID="TxtVat" runat="server"></asp:TextBox>
                                &nbsp;%</td>
                             <td width="15%">
                                 Minimum Charge</td>
                                 <td width="15%">
                                     <asp:TextBox ID="TxtMinimumCharge" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                Common Storage Charges</td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Vehicle</td>
                            <td class="style23" width="35%">
                                <asp:TextBox ID="TxtVenCharge" runat="server"></asp:TextBox>
                            </td>
                            <td width="15%">
                                Per</td>
                            <td width="15%">
                                <asp:TextBox ID="TxtUnitDays" runat="server"></asp:TextBox>
                                &nbsp;Day/s</td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Pallets</td>
                            <td class="style23" width="35%">
                                <asp:TextBox ID="TxtPalletCharge" runat="server"></asp:TextBox>
                                &nbsp;Per Day</td>
                            <td width="15%">
                                &nbsp;</td>
                            <td width="15%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Container (20Ft)</td>
                            <td class="style23" width="35%">
                                <asp:TextBox ID="TxtCont20" runat="server"></asp:TextBox>
                            </td>
                            <td width="15%">
                                Per Cal Month</td>
                            <td width="15%">
                                <asp:TextBox ID="TxtCont20Day" runat="server"></asp:TextBox>
                                &nbsp;Per Day</td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Container (40Ft)</td>
                            <td class="style23" width="35%">
                                <asp:TextBox ID="TxtCont40" runat="server"></asp:TextBox>
                            </td>
                            <td width="15%">
                                Per Cal Month</td>
                            <td width="15%">
                                <asp:TextBox ID="TxtCont40Day" runat="server"></asp:TextBox>
                                &nbsp;Per Day</td>
                        </tr>
                    </table>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
