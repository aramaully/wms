﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MainMenu.aspx.vb" Inherits="Wms.MainMenu" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%;">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 30px;
        }
        p.MsoNormal
        {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 0pt;
            margin-left: 0in;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri" , "sans-serif";
        }
        .style8
        {
            height: 45px;
            font-size: large;
            width: 222px;
        }
        .style9
        {
            height: 45px;
            font-weight: bold;
            font-size: large;
        }
        .style12
        {
            height: 45px;
            font-weight: bold;
            font-size: large;
            width: 647px;
        }
        .styleCursor
        {
        	cursor:hand;
        	vertical-align:baseline;
        	border-style:ridge;
        	border-width:thin;
        	background-color:#0080C0;
        	height:25px;
        	font-weight:bold;
        	color:White;
        }
        .styleCursor:hover
       {
       	background-color:Maroon;
       }                 
        .menuStyle
        {
        	text-align:left;
        	border-style:none;
        	height:15px;
        	font-weight:bold;
        	font-variant:normal;
        	color:GrayText;
        }
        .menuStyle:hover
        {
        	color:Maroon; 
        	border-bottom-style:solid; 
        	border-width:thin; 	
        }
        .iconStyle
        {
        	height:24px;
        	width:24px;
        }
        .iconStyle:hover
        {
        	height:32px;
        	width:32px;
        }
    </style>
</head>
<%--onload="javascript:window.history.forward(1);"--%>
<body style="height: 100%; margin-bottom:0;">
    <form id="form1" runat="server" style="height: 100%; margin-bottom:0px">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <asp:Panel ID="Panel1" runat="server" BackColor="#0080C0" Height="44px" Width="100%"
        HorizontalAlign="Center">
        <table class="style1" cellpadding="0" cellspacing="0">
            <tr>
                <td valign="middle" class="style8" bgcolor="#0080C0" align="left">
                    &nbsp;
                    <asp:Image ID="Image1" runat="server" Height="52px" 
                        ImageUrl="~/Images/Logo.gif" Style="text-align: left" Width="200px" />
                </td>
                <td align="left" bgcolor="#0080C0" class="style12" valign="middle">
                    <asp:Label ID="Label1" runat="server" Font-Size="X-Large" ForeColor="White" 
                        Text="Warehouse Management System "></asp:Label>
                </td>
                <td valign="middle" class="style9" bgcolor="#0080C0" align="right">
                    <asp:ImageButton ID="TlsClient" runat="server" CssClass="iconStyle" 
                        ImageUrl="~/Images/superman_24.png" ToolTip="Open Client File" />
                    <asp:ImageButton ID="TlsInBound" runat="server" CssClass="iconStyle" 
                        ImageUrl="~/Images/files_24.png" ToolTip="Open Bill Inbound" />
                    <asp:ImageButton ID="TlsOutBound" runat="server" CssClass="iconStyle" 
                        ImageUrl="~/Images/postage_stamp_24.png" ToolTip="Open Bill Outbound" />
                    <asp:ImageButton ID="TlsPhTally" runat="server" CssClass="iconStyle" 
                        ImageUrl="~/Images/PhTally.png" ToolTip="Physical Tally Sheet" />
                    <asp:ImageButton ID="TlsPutaway" runat="server" CssClass="iconStyle" 
                        ImageUrl="~/Images/Putaway.png" ToolTip="Manual Putaway" />
                    <asp:ImageButton ID="TlsPicking" runat="server" CssClass="iconStyle" 
                        ImageUrl="~/Images/pick.png" ToolTip="Manual Picking" />
                </td>
                <td align="center" bgcolor="#0080C0" class="style9" valign="middle">
                    &nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
    <asp:RoundedCornersExtender ID="Panel1_RoundedCornersExtender" runat="server" Enabled="True"
        Radius="10" TargetControlID="Panel1" Corners="Top">
    </asp:RoundedCornersExtender>
    <table style="height: 80%; width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width:17%" valign="top">
            <asp:TextBox ID="HeadText" runat="server" Font-Bold="true" ReadOnly="true" Font-Size="Large"
                    Width="100%" BackColor="Maroon" ForeColor="White" Text="Main Menu" 
                    BorderStyle="Inset" BorderWidth="1px" Height="30px"></asp:TextBox>
                <asp:Accordion ID="MyAccordion" runat="server" SelectedIndex="-1" HeaderCssClass="accordionHeader"
                    HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                    FadeTransitions="false" FramesPerSecond="40" TransitionDuration="250" AutoSize="None"
                    RequireOpenedPane="false" SuppressHeaderPostbacks="true">
                    <Panes>
                        <asp:AccordionPane ID="AccordionPane1" runat="server">
                            <Header>
                                <asp:Label runat="server" Width="100%"  CssClass="styleCursor"> File </asp:Label>
                                </Header>
                            <Content>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButActivity" runat="server" CssClass="menuStyle" Font-Underline="false">Activities</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButWarehouse" runat="server" CssClass="menuStyle"  Font-Underline="false">Warehouses</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButZones" runat="server" CssClass="menuStyle" Font-Underline="false">Zones</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButRacks" runat="server" CssClass="menuStyle"  Font-Underline="false">Racks</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButLocation" runat="server" CssClass="menuStyle"  Font-Underline="false">Locations</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButUnits" runat="server" CssClass="menuStyle"  Font-Underline="false">Units</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButCharges" runat="server" CssClass="menuStyle" Font-Underline="false">Charges</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButTrafficOff" runat="server" CssClass="menuStyle" Font-Underline="false">Traffic Officer</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButClient" runat="server" CssClass="menuStyle" Font-Underline="false">Client</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButTariff" runat="server" CssClass="menuStyle" Font-Underline="false">Tariff Card</asp:LinkButton><br />
                            </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="AccordionPane2" runat="server">
                            <Header>
                                <asp:Label ID="Label2" runat="server"  Width="100%" CssClass="styleCursor"> Process Bills </asp:Label>
                                </Header>
                            <Content>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButInBound" runat="server" CssClass="menuStyle" Font-Underline="false">Bill Inbound</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButOutbound" runat="server" CssClass="menuStyle" Font-Underline="false">Bill Outbound</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButReWarehouse" runat="server" CssClass="menuStyle" Font-Underline="false">Rewarehousing</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButRevBill" runat="server" CssClass="menuStyle" Font-Underline="false">Revise Bills</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButPhTally" runat="server" CssClass="menuStyle" Font-Underline="false">Physical Tally Sheet</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButCrossDock" runat="server" CssClass="menuStyle" Font-Underline="false">Cross Docking</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButStoreCont" runat="server" CssClass="menuStyle" Font-Underline="false">Store Containers</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButReleaseCont" runat="server" CssClass="menuStyle" Font-Underline="false">Release Containers</asp:LinkButton><br />
                             </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="AccordionPane3" runat="server">
                            <Header>
                                <asp:Label ID="Label3" runat="server"  Width="100%" CssClass="styleCursor"> Process Pallets </asp:Label>
                                </Header>
                            <Content>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButAPutaway" runat="server" CssClass="menuStyle" Font-Underline="false">Putaway Auto</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButMPutaway" runat="server" CssClass="menuStyle" Font-Underline="false">Putaway Manual</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButAPicking" runat="server" CssClass="menuStyle" Font-Underline="false">Picking Auto</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButMPicking" runat="server" CssClass="menuStyle" Font-Underline="false">Picking Manual</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButAmendPlt" runat="server" CssClass="menuStyle" Font-Underline="false">Amend Pallet Details</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButChangePltRatio" runat="server" CssClass="menuStyle" Font-Underline="false">Change Pallet Ratio</asp:LinkButton><br />
                              </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="AccordionPane4" runat="server">
                            <Header>
                                <asp:Label ID="Label4" runat="server"  Width="100%" CssClass="styleCursor"> Process Financials </asp:Label>
                                </Header>
                            <Content>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButComputeOcc" runat="server" CssClass="menuStyle" Font-Underline="false">Compute Occupancy</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButInvOcc" runat="server" CssClass="menuStyle" Font-Underline="false">Create Invoice from Occupancy</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButEditBill" runat="server" CssClass="menuStyle" Font-Underline="false">Edit Billing Invoice</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButExportSun" runat="server" CssClass="menuStyle" Font-Underline="false">Export to Sun</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButChLogistic" runat="server" CssClass="menuStyle" Font-Underline="false">Charges for Logistic Services</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButInvPayment" runat="server" CssClass="menuStyle" Font-Underline="false">Payment of Invoices</asp:LinkButton><br />                            
                             </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="AccordionPane5" runat="server">
                            <Header>
                                <asp:Label ID="Label5" runat="server"  Width="100%" CssClass="styleCursor"> Process Miscellaneous </asp:Label>
                                </Header>
                            <Content>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButSetHold" runat="server" CssClass="menuStyle" Font-Underline="false">Set/Unset Hold on Locations</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButPutSequence" runat="server" CssClass="menuStyle" Font-Underline="false">Putaway/Picking Sequences</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButClearLoc" runat="server" CssClass="menuStyle" Font-Underline="false">Clear Locations</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButAbandon" runat="server" CssClass="menuStyle" Font-Underline="false">Abandon Goods</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButMoveIn" runat="server" CssClass="menuStyle" Font-Underline="false">Move In Sheet</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButMoveOut" runat="server" CssClass="menuStyle" Font-Underline="false">Move Out Sheet</asp:LinkButton><br />
                               <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButPropBuilder" runat="server" CssClass="menuStyle" Font-Underline="false">Proposal Builder</asp:LinkButton><br />                               
                            </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="AccordionPane6" runat="server">
                            <Header>
                                <asp:Label ID="Label6" runat="server"  Width="100%" CssClass="styleCursor"> Stock Reports </asp:Label>
                                </Header>
                            <Content>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButStkReturn" runat="server" CssClass="menuStyle" Font-Underline="false">Stock Return</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButStkInbound" runat="server" CssClass="menuStyle" Font-Underline="false">Stock by Inbound</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButStkLoc" runat="server" CssClass="menuStyle" Font-Underline="false">Stock by Location</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButStkStatus" runat="server" CssClass="menuStyle" Font-Underline="false">Stock Status</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButStkClient" runat="server" CssClass="menuStyle" Font-Underline="false">Stock By Client</asp:LinkButton><br />
                            </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="AccordionPane7" runat="server">
                            <Header>
                                <asp:Label ID="Label7" runat="server"  Width="100%" CssClass="styleCursor"> Facility Reports </asp:Label>
                                </Header>
                            <Content>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButZonesList" runat="server" CssClass="menuStyle" Font-Underline="false">Zones</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButRacksList" runat="server" CssClass="menuStyle" Font-Underline="false">Racks</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButLocList" runat="server" CssClass="menuStyle" Font-Underline="false">Locations</asp:LinkButton><br />
                            </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="AccordionPane8" runat="server">
                            <Header>
                                <asp:Label ID="Label8" runat="server"  Width="100%" CssClass="styleCursor"> Bill Related Reports </asp:Label>
                                </Header>
                            <Content>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButExpInbound" runat="server" CssClass="menuStyle" Font-Underline="false">Expired Inbound</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButBillStat" runat="server" CssClass="menuStyle" Font-Underline="false">Bill Status</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButDiscPhTally" runat="server" CssClass="menuStyle" Font-Underline="false">Discrepancy Physical Tally</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButPrintPhTally" runat="server" CssClass="menuStyle" Font-Underline="false">Physical Tally Sheet</asp:LinkButton><br />
                                <hr />                                
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButPickListSumm" runat="server" CssClass="menuStyle" Font-Underline="false">Picking List Summary</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButPickingList" runat="server" CssClass="menuStyle" Font-Underline="false">Picking List</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButPutawayList" runat="server" CssClass="menuStyle" Font-Underline="false">Putaway List</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButPalletTags" runat="server" CssClass="menuStyle" Font-Underline="false">Pallet Tags</asp:LinkButton><br />
                            </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="AccordionPane9" runat="server">
                            <Header>
                                <asp:Label ID="Label9" runat="server"  Width="100%" CssClass="styleCursor"> Financial Reports </asp:Label>
                                </Header>
                            <Content>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButOccupancy" runat="server" CssClass="menuStyle" Font-Underline="false">Occupancy</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButPrintInvoice" runat="server" CssClass="menuStyle" Font-Underline="false">Print Invoice</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButCustomRep" runat="server" CssClass="menuStyle" Font-Underline="false">Custom Report Generator</asp:LinkButton><br />
                            </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="AccordionPane10" runat="server">
                            <Header>
                                <asp:Label ID="Label10" runat="server"  Width="100%" CssClass="styleCursor"> Utilities </asp:Label>
                                </Header>
                            <Content>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButAddUser" runat="server" CssClass="menuStyle" Font-Underline="false">Create / Edit User Account</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButPassword" runat="server" CssClass="menuStyle" Font-Underline="false">Change Password</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButAccess" runat="server" CssClass="menuStyle" Font-Underline="false">Assign Access</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButAuditTrail" runat="server" CssClass="menuStyle" Font-Underline="false">Audit Trail</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButSystem" runat="server" CssClass="menuStyle" Font-Underline="false">System Setup</asp:LinkButton><br />
                            </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="AccordionPane11" runat="server">
                            <Header>
                                <asp:Label ID="Label11" runat="server"  Width="100%" CssClass="styleCursor"> Help </asp:Label>
                                </Header>
                            <Content>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButContents" runat="server" CssClass="menuStyle" Font-Underline="false">Contents</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButIndex" runat="server" CssClass="menuStyle" Font-Underline="false">Index</asp:LinkButton><br />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButSearch" runat="server" CssClass="menuStyle" Font-Underline="false">Search</asp:LinkButton><br />
                                <hr />
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="ButAbout" runat="server" CssClass="menuStyle" Font-Underline="false">About</asp:LinkButton><br />
                            </Content>
                        </asp:AccordionPane>
                    </Panes>  
                </asp:Accordion>
                <asp:LinkButton ID="ButExit" runat="server" CssClass="styleCursor" 
                    Font-Underline="false" Width="100%" >Exit</asp:LinkButton>
                <asp:ConfirmButtonExtender ID="ButExit_ConfirmButtonExtender" runat="server" 
                    ConfirmText="Sure to Sign Out?" Enabled="True" TargetControlID="ButExit">
                </asp:ConfirmButtonExtender>
                <br />
            </td>
            <td valign="top" style="height: 100%;">
                <iframe id="MainFrame" name="MainArea" height="550px" width="100%" style="border-width:0px"
                    runat="server">
                
                </iframe>
            </td>
        </tr>
        <tr>
            <td align="center" valign="bottom" style="height:10px" colspan="2">
                <p class="MsoNormal">
                    Copyright© 2010 Software Lab Qatar W.L.L All rights reserved.</p>
            </td>
        </tr>
    </table>  
    </ContentTemplate>
    </asp:UpdatePanel>    
    </form>
</body>
</html>
