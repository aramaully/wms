<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Report.aspx.vb" Inherits="Wms.Report" EnableSessionState="True" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" Width="800px" Height="41px" BorderStyle="Groove"
                BorderWidth="1px">
                <table cellpadding="0" cellspacing="0" frame="hsides" width="100%">
                    <tr>
                        <td bgcolor="#004080">
                            <asp:Label ID="LblText" runat="server"></asp:Label>
                        </td>
                        <td align="left" bgcolor="#004080" nowrap="nowrap" width="40%">
                            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Report Viewer"></asp:Label>
                        </td>
                        <td align="right" bgcolor="#004080" nowrap="nowrap">
                            <asp:Button ID="ButPrint" runat="server" Height="39px" Text="Print" Style="cursor: pointer"
                                ToolTip="Print" Width="68px" CausesValidation="False" 
                                UseSubmitBehavior="False" />
                                 <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ButPrint"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
                            <asp:Button ID="ButExit" runat="server" CausesValidation="False" Height="39px" 
                                Style="cursor: pointer" Text="Close" ToolTip="Close this Module" 
                                UseSubmitBehavior="False" Width="68px" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="Panel3" runat="server" Height="600px" Style="left: 2px; position: relative;
                top: 0px; z-index: 102; width: 800px;" ScrollBars="Both">
                <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" Visible="false"
                    AutoDataBind="True" Height="50px" ReportSourceID="CrystalReportSource1" Width="350px"
                    DisplayGroupTree="False" HasCrystalLogo="False" />
                <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
                </CR:CrystalReportSource>
            </asp:Panel>
            
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="CrystalReportViewer1" />
        </Triggers>
    </asp:UpdatePanel>
    
    <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White" Text="Print Dialog"></asp:Label>
                                <asp:LinkButton ID="ButCloseErr" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                        <td></td>
                          <td>
                                <asp:Label ID="LblPrinter" runat="server" Text= "Printer"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="DDLPrinter" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                        <td></td>
                        <td><asp:Label runat="server" ID="LblPages" Text="Pages"></asp:Label></td>
                        <td>From: <asp:TextBox ID="TxtPFrom" runat="server" Text="0" Width="25px"></asp:TextBox>
                        To: <asp:TextBox ID="TxtPTo" runat="server" Text="0" Width="25px"></asp:TextBox>
                        </td>
                        </tr>
                        <tr>
                        <td></td>
                        <td>
                        <asp:Label ID="LblCopies" runat="server" Text="Copies"></asp:Label>
                        </td>
                        <td>
                        <asp:TextBox ID="TxtCopies" runat="server" Text="1" Width="25px"></asp:TextBox>
                        <asp:CheckBox ID="ChkCollated" runat="server" Text="Collated" />
                        </td>
                        </tr>
                        <tr style="height: 50px">
                           
                            <td align="center" style="border-top: #ddd 2px groove;" colspan="3">
                                <asp:Button ID="ButPrintRpt" Height="30px" Width="60" runat="server" Text="Print"
                                    OnClick="ButPrintRpt_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                           
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </form>
</body>
</html>
