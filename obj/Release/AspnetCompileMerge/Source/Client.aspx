﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Client.aspx.vb" Inherits="Wms.Client" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            height: 47px;
        }
        .style4
        {
        }
        .style5
        {
            width: 376px;
            height: 72px;
        }
        .style6
        {
            color: #FFFFFF;
            width: 84px;
            height: 72px;
        }
        .style7
        {
            height: 72px;
        }
                
        .ModalBackground
        {
        background-color: Gray;
	    filter: alpha(opacity=70);
	    opacity: 0.7;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 102; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" Width="80%" Height="100%" BorderStyle="Groove"
                    BorderWidth="1px">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td>
                                <asp:Label ID="lblText" runat="server" Text=""></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="       Create / Edit Client Details"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButNew" runat="server" Height="39px" Text="New" Style="cursor: pointer"
                                    ToolTip="Create New Activity" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Activity" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save Client Data?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find Client" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButNext" runat="server" Height="39px" Text="Next" Style="cursor: pointer"
                                    ToolTip="Go to Next Client" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButPrevious" runat="server" Height="39px" Text="Prev" Style="cursor: pointer"
                                    ToolTip="Go To Previous Client" Width="55px" CausesValidation="False" />
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Activity Selected" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete Client Data?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButExit" runat="server" Height="39px" Text="Close" Style="cursor: pointer"
                                    ToolTip="Close this Module" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButExit_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Close this Module?"
                                    Enabled="True" TargetControlID="ButExit">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    <cc1:TabContainer ID="TabContainer1" runat="server" Width="100%" ActiveTabIndex="3"
                        Height="344px">
                        <cc1:TabPanel ID="GenTab" runat="server" HeaderText="General">
                            <HeaderTemplate>
                                General Company Details
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table class="style1">
                                    <tr>
                                        <td width="25%">
                                            Client ID</td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtClientID" runat="server"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            Name of Company</td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtName" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            Address 1</td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtAddress1" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            Address 2</td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtAddress2" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            City</td>
                                        <td>
                                            <asp:TextBox ID="TxtCity" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Country</td>
                                        <td>
                                            <asp:TextBox ID="TxtCountry" runat="server"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            Business Tel No</td>
                                        <td>
                                            <asp:TextBox ID="TxtTelephone" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Fax No</td>
                                        <td>
                                            <asp:TextBox ID="TxtFax" runat="server"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            VAT No</td>
                                        <td>
                                            <asp:TextBox ID="TxtVatNo" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Date of Reg</td>
                                        <td>
                                            <asp:TextBox ID="TxtDateofReg" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="TxtDateofReg_CalendarExtender" runat="server" 
                                                Enabled="True" TargetControlID="TxtDateofReg" Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            Business Reg No</td>
                                        <td>
                                            <asp:TextBox ID="TxtBRegNo" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Date of Bus Reg</td>
                                        <td>
                                            <asp:TextBox ID="TxtBRegDate" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="TxtBRegDate_CalendarExtender" runat="server" 
                                                Enabled="True" TargetControlID="TxtBRegDate" Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            Certificate of Incorporation No</td>
                                        <td>
                                            <asp:TextBox ID="TxtCertIncNo" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Date of Incorporation</td>
                                        <td>
                                            <asp:TextBox ID="TxtDateInc" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="TxtDateInc_CalendarExtender" runat="server" 
                                                Enabled="True" TargetControlID="TxtDateInc" Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            Corporate E-Mail</td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtEMail" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="25%">
                                            Corporate WEB Page URL</td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtWeb" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </cc1:TabPanel>   
                        <cc1:TabPanel ID="ContactTab" runat="server" HeaderText="Contact Details">
                            <ContentTemplate>
                            
                                <table class="style1">
                                    <tr>
                                        <td class="style8" width="25%">
                                            Title</td>
                                        <td>
                                            <asp:TextBox ID="TxtContactTitle" runat="server"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="25%">
                                            Contact Person</td>
                                        <td>
                                            <asp:TextBox ID="TxtContact" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="25%">
                                            ID No</td>
                                        <td>
                                            <asp:TextBox ID="TxtIDNo" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="25%">
                                            Department</td>
                                        <td>
                                            <asp:TextBox ID="TxtDept" runat="server" Width="70%"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="25%">
                                            Office
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtOffice" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="25%">
                                            Profession</td>
                                        <td>
                                            <asp:TextBox ID="TxtProfession" runat="server" Width="70%"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="25%">
                                            Office Phone No</td>
                                        <td>
                                            <asp:TextBox ID="TxtOfficeTelNo" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="25%">
                                            Mobile No</td>
                                        <td>
                                            <asp:TextBox ID="TxtMobile" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="25%">
                                            Email Address</td>
                                        <td>
                                            <asp:TextBox ID="TxtContactEmail" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" height="75" width="25%">
                                            Remarks</td>
                                        <td height="75">
                                            <asp:TextBox ID="TxtContRemarks" runat="server" Height="100%" Width="100%"></asp:TextBox>
                                        </td>
                                        <td height="75" width="20%">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="LicenseTab" runat="server" HeaderText="Licensing Details">
                            <ContentTemplate>
                            
                                <table class="style1">
                                    <tr>
                                        <td class="style8" width="30%">
                                            Company Category</td>
                                        <td>
                                            <asp:DropDownList ID="CmbCategory" runat="server" Width="60%">
                                            </asp:DropDownList>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="30%">
                                            Freeport Certificate No</td>
                                        <td>
                                            <asp:TextBox ID="TxtFPCertNo" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="30%">
                                            Date of Reg. of Freeport Certificate</td>
                                        <td>
                                            <asp:TextBox ID="TxtFPCertDate" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="TxtFPCertDate_CalendarExtender" runat="server" 
                                                Enabled="True" TargetControlID="TxtFPCertDate" Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="30%">
                                            Validity of Freeport Certificate</td>
                                        <td>
                                            <asp:TextBox ID="TxtFPCertValidity" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="TxtFPCertValidity_CalendarExtender" runat="server" 
                                                Enabled="True" TargetControlID="TxtFPCertValidity" Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="30%">
                                            Storage Permit No</td>
                                        <td>
                                            <asp:TextBox ID="TxtStoragePermit" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="30%">
                                            Date of Reg of Storage Permit</td>
                                        <td>
                                            <asp:TextBox ID="TxtPermitDate" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="TxtPermitDate_CalendarExtender" runat="server" 
                                                Enabled="True" TargetControlID="TxtPermitDate" Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" width="30%">
                                            Validity of Storage Permit</td>
                                        <td>
                                            <asp:TextBox ID="TxtPermitExp" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="TxtPermitExp_CalendarExtender" runat="server" 
                                                Enabled="True" TargetControlID="TxtPermitExp" Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td width="20%">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style8" height="75" width="30%">
                                            Remarks</td>
                                        <td height="75">
                                            <asp:TextBox ID="TxtLicRemarks" runat="server" Height="100%" Width="90%"></asp:TextBox>
                                        </td>
                                        <td height="75" width="20%">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            
                            </ContentTemplate>
                        </cc1:TabPanel>   
                        <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="Operational Details">
                            <ContentTemplate>
                                <table class="style1">
                                    <tr valign="top">
                                        <td width="50%">
                                            <table class="style1">
                                                <tr>
                                                    <td>
                                                        Freeport Zone No</td>
                                                    <td>
                                                        <asp:DropDownList ID="CmbFpZoneNo" runat="server" Width="40%">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Status</td>
                                                    <td>
                                                        <asp:RadioButton ID="OptOp" runat="server" Checked="True" 
                                                            GroupName="Status" Text="Operational" />
                                                        &nbsp;<asp:RadioButton ID="OptNonOp" runat="server" GroupName="Status" 
                                                            Text="Non-Operational" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        No of Facilities Leased</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtFacilities" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Area Leased</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtAreaLeased" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Nature of Goods Handled</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtNatureofGoods" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Hours of Work</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtWorkHours" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:GridView ID="GdvActivity" runat="server" AutoGenerateColumns="False" 
                                                            PageSize="3" Width="100%">
                                                            <AlternatingRowStyle BackColor="#CAE4FF" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkActivity" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Activity" HeaderText="Activity" HtmlEncode="False" />
                                                            </Columns>
                                                            <HeaderStyle BackColor="#0080C0" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table class="style3" width="100%">
                                                <tr>
                                                    <td width="50%">
                                                        <asp:Panel ID="Panel3" runat="server" BorderStyle="Solid" BorderWidth="1px" 
                                                            Height="300px" ScrollBars="Vertical" Width="100%">
                                                            <asp:GridView ID="GdvImport" runat="server" AutoGenerateColumns="False" 
                                                                Width="100%">
                                                                <AlternatingRowStyle BackColor="#CAE4FF" />
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkImport" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Name" HeaderText="Sourcing Markets" />
                                                                </Columns>
                                                                <HeaderStyle BackColor="#0080C0" />
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Panel ID="Panel2" runat="server" BorderStyle="Solid" BorderWidth="1px" 
                                                            Height="300px" ScrollBars="Vertical" Width="100%">
                                                            <asp:GridView ID="GdvExport" runat="server" AutoGenerateColumns="False" 
                                                                Width="100%">
                                                                <AlternatingRowStyle BackColor="#CAE4FF" />
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkExport" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Name" HeaderText="Export Markets" />
                                                                </Columns>
                                                                <HeaderStyle BackColor="#0080C0" />
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>                    
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                </asp:Panel>
                <asp:Panel ID="PnlFind" Style="z-index: 101; display:none; left: 300px; position: absolute; top: 60px"
                    runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                    Visible="True" BackColor="White">
                    <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                        <tr>
                            <td class="style6" bgcolor="#004080" valign="top">
                                <asp:RadioButton ID="RbtName" runat="server" Text="Name" Checked="True" GroupName="FinOption" />
                                <br />
                                <asp:RadioButton ID="RbtCity" runat="server" Text="City" GroupName="FinOption" />
                                <br />
                                <asp:RadioButton ID="RbtContact" runat="server" Text="Contact" GroupName="FinOption" />
                            </td>
                            <td class="style5" bgcolor="#004080">
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" style="text-align:center;"
                                    Text="Enter the text here to search"></asp:Label>
                                <asp:TextBox ID="TxtSearch" runat="server" Width="100%" BackColor="White"></asp:TextBox>
                            </td>
                            <td align="right" bgcolor="#004080" class="style7">
                                <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                    Width="65px" />
                                <br />
                                <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                    Width="65px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="style4" colspan="3">
                                <asp:Panel ID="Panel8" runat="server" Width="100%" Height="300px" ScrollBars="Vertical">
                                    <asp:GridView ID="GdvFind" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                        ShowFooter="true">
                                        <HeaderStyle BackColor="#0080C0" />
                                        <FooterStyle BackColor="#0080C0" />
                                        <AlternatingRowStyle BackColor="#CAE4FF" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:modalpopupextender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:modalpopupextender>
    </div>
    </form>
</body>
</html>

