﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MoveIn.aspx.vb" Inherits="Wms.MoveIn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Move In</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Move In'; //Set the name
            w.Top = 10;
            w.Left = 10;
            w.Width = 1096; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 559;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '1';
                w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Delete") {
                Index = '3';
                w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 236px;
        }
        </style>

    </head>
<body onload="init();">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 350px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <div id="Content" style="width: 100%; height: 100%; padding: 2px; border-right: solid 1px #cddaea;">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Move In Sheet"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButNew" runat="server" Height="39px" Text="New" Style="cursor: pointer"
                                    ToolTip="Create New Move In Sheet" Width="55px" CausesValidation="False" 
                                    UseSubmitBehavior="False" />
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Move In Sheet" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save Move In Sheet?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButNext" runat="server" Height="39px" Text="Next" Style="cursor: pointer"
                                    ToolTip="Go to Next Client" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButPrevious" runat="server" Height="39px" Text="Prev" Style="cursor: pointer"
                                    ToolTip="Go To Previous Client" Width="55px" CausesValidation="False" />
                            </td>
                        </tr>
                    </table>
                    
                    <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="2" 
                        Width="100%">
                        <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="Client">
                        <ContentTemplate>
                            <table class="style1">
                                <tr>
                                    <td>
                                        Client Id</td>
                                    <td valign="middle">
                                        <asp:TextBox ID="TxtClientID" runat="server" AutoPostBack="True" TabIndex="1"></asp:TextBox>
                                        Category&nbsp;
                                        <asp:TextBox ID="TxtCategory" runat="server" ReadOnly="True"></asp:TextBox>
                                    </td>
                                    <td>
                                        Move in Date</td>
                                    <td>
                                        <asp:TextBox ID="TxtMoveIn" runat="server" TabIndex="2"></asp:TextBox>
                                        <cc1:CalendarExtender ID="TxtMoveIn_CalendarExtender" runat="server" 
                                            Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtMoveIn">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Name</td>
                                    <td colspan="3">
                                        <asp:TextBox ID="TxtName" runat="server" Width="400px" ReadOnly="True"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Address</td>
                                    <td colspan="3">
                                        <asp:TextBox ID="TxtAddress" runat="server" Width="400px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        City</td>
                                    <td colspan="3">
                                        <asp:TextBox ID="TxtCity" runat="server" Width="400px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Telephone</td>
                                    <td>
                                        <asp:TextBox ID="TxtTelephone" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Fax</td>
                                    <td>
                                        <asp:TextBox ID="TxtFax" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        E-Mail</td>
                                    <td colspan="3">
                                        <asp:TextBox ID="TxtEmail" runat="server" Width="400px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Contact</td>
                                    <td colspan="3">
                                        <asp:TextBox ID="TxtContact" runat="server" Width="400px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Position</td>
                                    <td colspan="3">
                                        <asp:TextBox ID="TxtPosition" runat="server" Width="400px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Date</td>
                                    <td>
                                        <asp:TextBox ID="TxtDate" runat="server"></asp:TextBox>
                                        <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" 
                                            Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtDate">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td>
                                        Status</td>
                                    <td>
                                        <asp:DropDownList ID="CmbStatus" runat="server">
                                            <asp:ListItem>Operational</asp:ListItem>
                                            <asp:ListItem>Non Operational</asp:ListItem>
                                            <asp:ListItem>Dormant</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        VAT No</td>
                                    <td>
                                        <asp:TextBox ID="TxtVatNo" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Zone</td>
                                    <td>
                                        <asp:TextBox ID="TxtZone" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Licence No</td>
                                    <td>
                                        <asp:TextBox ID="TxtLicenceNo" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Expiry</td>
                                    <td>
                                        <asp:TextBox ID="TxtLicenceExp" runat="server"></asp:TextBox>
                                        <cc1:CalendarExtender ID="TxtLicenceExp_CalendarExtender" runat="server" 
                                            Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtLicenceExp">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Storage Permit</td>
                                    <td>
                                        <asp:TextBox ID="TxtStoragePermit" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Expiry</td>
                                    <td>
                                        <asp:TextBox ID="TxtStorageExp" runat="server"></asp:TextBox>
                                        <cc1:CalendarExtender ID="TxtStorageExp_CalendarExtender" runat="server" 
                                            Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtStorageExp">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Product Type</td>
                                    <td colspan="3">
                                        <asp:TextBox ID="TxtProdType" runat="server" Width="400px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="Cabin Details">
                        <ContentTemplate>
                            <table class="style1">
                                <tr>
                                    <td>
                                        Freeport Zone</td>
                                    <td>
                                        <asp:DropDownList ID="CmbFPZone" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        Cabin No</td>
                                    <td>
                                        <asp:DropDownList ID="CmbCabin" runat="server" AutoPostBack="True">
                                            <asp:ListItem>Select...</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Button ID="ButAddCab" runat="server" Text="Add" Width="55px" />
                                        <asp:Button ID="ButDelCab" runat="server" Text="Delete" Width="55px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Area</td>
                                    <td>
                                        <asp:TextBox ID="TxtArea" runat="server" AutoPostBack="True"></asp:TextBox>
                                    </td>
                                    <td>
                                        Rate / Sq Ft</td>
                                    <td>
                                        <asp:TextBox ID="TxtRate" runat="server" AutoPostBack="True"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Lease Commence On</td>
                                    <td>
                                        <asp:TextBox ID="TxtLeaseOn" runat="server"></asp:TextBox>
                                        <cc1:CalendarExtender ID="TxtLeaseOn_CalendarExtender" runat="server" 
                                            Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtLeaseOn">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td>
                                        Lease Expiry</td>
                                    <td>
                                        <asp:TextBox ID="TxtLeaseExp" runat="server"></asp:TextBox>
                                        <cc1:CalendarExtender ID="TxtLeaseExp_CalendarExtender" runat="server" 
                                            Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtLeaseExp">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Date of Entry</td>
                                    <td>
                                        <asp:TextBox ID="TxtDateofEntry" runat="server"></asp:TextBox>
                                        <cc1:CalendarExtender ID="TxtDateofEntry_CalendarExtender" runat="server" 
                                            Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtDateofEntry">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td>
                                        Monthly Rent</td>
                                    <td>
                                        <asp:TextBox ID="TxtMonthlyRent" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Deposit Amount</td>
                                    <td>
                                        <asp:TextBox ID="TxtDepositAmt" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Deposit Paid On</td>
                                    <td>
                                        <asp:TextBox ID="TxtDepositPaidOn" runat="server"></asp:TextBox>
                                        <cc1:CalendarExtender ID="TxtDepositPaidOn_CalendarExtender" runat="server" 
                                            Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtDepositPaidOn">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Guarantee No</td>
                                    <td>
                                        <asp:TextBox ID="TxtGuaranteeNo" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Expiry Date</td>
                                    <td>
                                        <asp:TextBox ID="TxtGuaranteeExp" runat="server"></asp:TextBox>
                                        <cc1:CalendarExtender ID="TxtGuaranteeExp_CalendarExtender" runat="server" 
                                            Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtGuaranteeExp">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Operational Hours</td>
                                    <td>
                                        <asp:TextBox ID="TxtOpHours" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Insurance Charges</td>
                                    <td>
                                        <asp:TextBox ID="TxtInsurance" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Electricity Charges</td>
                                    <td>
                                        <asp:TextBox ID="TxtElecCharges" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Water Charges</td>
                                    <td>
                                        <asp:TextBox ID="TxtWaterCharge" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Telephone Charges</td>
                                    <td>
                                        <asp:TextBox ID="TxtTelCharges" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Waste Water Charges</td>
                                    <td>
                                        <asp:TextBox ID="TxtWasteWater" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto" Height="128px" 
                                Width="810px" BorderStyle="Solid" BorderWidth="1px">
                                <asp:GridView ID="GdvCabs" runat="server" Width="472px" 
                                    AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:BoundField DataField="WhCode" HeaderText="Zone" HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CabinNo" HeaderText="Cabin No" HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Capacity" HeaderText="Area" HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Rent" HeaderText="Rent" HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Deposit" HeaderText="Deposit" HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Guarantee" HeaderText="Guarantee" 
                                            HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ExpDate" HeaderText="Expiry Date" HtmlEncode="False" 
                                            DataFormatString="{0:dd-MMM-yyyy}" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="OpHours" HeaderText="Op Hours" HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="EntryDate" HeaderText="Date of Entry" HtmlEncode="False" 
                                            DataFormatString="{0:dd-MMM-yyyy}" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Insurance" HeaderText="Insurance" 
                                            HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LeaseCommence" HeaderText="Commence" HtmlEncode="False" 
                                            DataFormatString="{0:dd-MMM-yyyy}" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="RateSqFt" HeaderText="Rate" HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Electricity" HeaderText="Electricity" 
                                            HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Water" HeaderText="Water" HtmlEncode="False"  />
                                        <asp:BoundField DataField="Waste" HeaderText="Waste" HtmlEncode="False"  />
                                        <asp:BoundField DataField="Telephone" HeaderText="Telephone" 
                                            HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DepositPaid" HeaderText="Deposit Paid" HtmlEncode="False" 
                                            DataFormatString="{0:dd-MMM-yyyy}" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AddRemarks" HeaderText="Add Remarks" 
                                            HtmlEncode="False" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LeaseExpDate" HeaderText="Lease Expiry" HtmlEncode="False" 
                                            DataFormatString="{0:dd-MMM-yyyy}" >
                                        <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                    </Columns>
                                    <SelectedRowStyle BackColor="#FF9933" />
                                    <HeaderStyle BackColor="#0066FF" ForeColor="White" Wrap="False" />
                                    <AlternatingRowStyle BackColor="#6699FF" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="Operational Requirements">
                        <ContentTemplate>
                            <table class="style1">
                                <tr>
                                    <td width="25%">
                                        Freeport Activities</td>
                                    <td>
                                        <asp:TextBox ID="TxtFPActivity" runat="server" TextMode="MultiLine" 
                                            Width="100%" Height="70px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Additional Remarks</td>
                                    <td>
                                        <asp:TextBox ID="TxtAddRemarks" runat="server" TextMode="MultiLine" 
                                            Width="100%" Height="70px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                            <tr>
                                <td width="25%">Operational Requirements</td> 
                                <td colspan="4">
                                    <asp:TextBox ID="TxtOpReq" runat="server" Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Follow up Action</td> 
                                <td colspan="4">
                                    <asp:TextBox ID="TxtFollow" runat="server" Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                                <tr>
                                    <td colspan="1">
                                        Who</td>
                                    <td>
                                        <asp:TextBox ID="TxtWho" runat="server" Height="22px" Width="292px"></asp:TextBox>
                                    </td>
                                    <td width="10%">
                                        Done</td>
                                    <td width="10%">
                                        <asp:CheckBox ID="ChkDone" runat="server" Text="Yes" />
                                    </td>
                                    <td align="right" width="15%">
                                        <asp:Button ID="ButAddReq" runat="server" Text="Add" Width="55px" />
                                        <asp:Button ID="ButDelReq" runat="server" Text="Delete" Width="55px" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="117px" 
                                BorderStyle="Solid" BorderWidth="1px">
                                <asp:GridView ID="GdvOpReq" runat="server" Width="100%" 
                                    AutoGenerateColumns="False">
                                    <AlternatingRowStyle BackColor="#6699FF" />
                                    <Columns>
                                        <asp:BoundField DataField="OpReq" HeaderText="Operational Requirement" 
                                            HtmlEncode="False" ReadOnly="True" />
                                        <asp:BoundField DataField="Follow" HeaderText="Follow Up Action" 
                                            HtmlEncode="False" ReadOnly="True" />
                                        <asp:BoundField DataField="Who" HeaderText="Who" HtmlEncode="False" 
                                            ReadOnly="True" />
                                        <asp:CheckBoxField DataField="Done" HeaderText="Done" ReadOnly="True"  >
                                        <ItemStyle HorizontalAlign="Center"  />
                                        </asp:CheckBoxField>
                                    </Columns>
                                    <SelectedRowStyle BackColor="#FF9933" />
                                    <HeaderStyle BackColor="#0066FF" ForeColor="White" Wrap="False" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                    
                </div>
                 <asp:Panel ID="PnlFind" Style="z-index: 101; display:none; left: 100px; position: absolute; top: 60px"
                        runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                        Visible="True" BackColor="White">
                        <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                        <tr>
                            <td class="style6" bgcolor="#004080" valign="top">
                                <asp:RadioButton ID="RbtName" runat="server" Text="Name" Checked="True" GroupName="FinOption" />
                                <br />
                                <asp:RadioButton ID="RbtCity" runat="server" Text="City" GroupName="FinOption" />
                                <br />
                                <asp:RadioButton ID="RbtContact" runat="server" Text="Contact" GroupName="FinOption" />
                            </td>
                            <td class="style5" bgcolor="#004080">
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" style="text-align:center;"
                                    Text="Enter the text here to search"></asp:Label>
                                <asp:TextBox ID="TxtSearch" runat="server" Width="100%" BackColor="White"></asp:TextBox>
                            </td>
                            <td align="right" bgcolor="#004080" class="style7">
                                <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                    Width="65px" />
                                <br />
                                <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                    Width="65px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="style4" colspan="3">
                                <asp:Panel ID="Panel8" runat="server" Width="100%" Height="300px" ScrollBars="Vertical">
                                    <asp:GridView ID="GdvFind" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                        ShowFooter="true">
                                        <HeaderStyle BackColor="#0080C0" />
                                        <FooterStyle BackColor="#0080C0" />
                                        <AlternatingRowStyle BackColor="#CAE4FF" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>