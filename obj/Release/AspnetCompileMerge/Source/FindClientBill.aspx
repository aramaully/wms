﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FindClientBill.aspx.vb" Inherits="Wms.FindClientBill" %>
<%@ Register Assembly="EO.Web" Namespace="EO.Web" TagPrefix="eo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Find Client</title>
    <script language="javascript" type="text/javascript">
        function GetID() {
            var Target = window.opener.document.getElementById("TxtBoeNo");
            Target.value = document.getElementById("TxtBoeNo").value;
            Target.fireEvent('onchange');
        }
        function MeOnTop() {
            if (document.getElementById("TxtSearch").focused) { }
            else {
                self.focus();
            }
        }
    </script>
</head>
<body style="margin-top: 0" onblur="MeOnTop();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript">
        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('PnlGrid').scrollLeft;
            yPos = $get('PnlGrid').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('PnlGrid').scrollLeft = xPos;
            $get('PnlGrid').scrollTop = yPos;
        }
</script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Width="584px" Height="400px">
                <eo:ToolBar ID="ToolBar1" runat="server" Width="754px" BackgroundImage="00100103"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    AutoPostBack="True">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="Refresh" ImageUrl="~/images/RefreshDocViewHS.png" ToolTip="Refresh Grid">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Select" ImageUrl="~/images/TaskHS.png" ToolTip="Select Employee">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Cancel" ImageUrl="~/images/delete.png" ToolTip="Cancel">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
                   <table cellpadding="0" cellspacing="0" style="height: 5; width= 100%">
                        <tr>
                            <td style="width: 25%">
                                <asp:RadioButton ID="RbnClient" runat="server" Text="Client" Checked="true" GroupName="Select"
                                   />
                                <br />
                                <asp:RadioButton ID="RbnCountry" runat="server" Text="Country" Checked="false" GroupName="Select"
                                     />
                                <br />
                                <asp:RadioButton ID="RbnTraffic" runat="server" Text="Traffic Op" Checked="false"
                                    GroupName="Select"  />
                                <br />
                                <asp:RadioButton ID="RbnCont" runat="server" Text="Container" Checked="false" GroupName="Select"
                                   />
                            </td>
                            <td  style="width: 65%" valign="top">
                                <asp:Label ID="LblSearch" runat="server" Font-Bold="True"  Text="Enter the text here to search"></asp:Label>
                                <br />
                                <asp:TextBox ID="TxtSearch" runat="server" BackColor="White" Width="101%"></asp:TextBox>
                                <br />
                                <br />
                                <asp:CheckBox ID="ChkDate" runat="server" Text="Date Range From "  />
                                <asp:TextBox ID="TxtFrom" runat="server" Width="100px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtFrom"
                                    Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                                <asp:Label ID="LblToDate" runat="server" Text=" To " ></asp:Label>
                                <asp:TextBox ID="TxtTo" runat="server" Width="100px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TxtTo"
                                    Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right"  style="width: 10%">
                                <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="27px"
                                    Width="65px" />
                                <br />
                                <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="27px"
                                    Width="65px" />
                            </td>
                        </tr>
<%--                        <tr>
                            <td colspan="3">
                                <asp:Panel ID="Panel8" runat="server" Height="300px" ScrollBars="Vertical" Width="100%">
                                    <asp:GridView ID="GdvFind" runat="server" EnableSortingAndPagingCallbacks="false"
                                        ShowFooter="true" Width="100%">
                                        <HeaderStyle BackColor="#0080C0" />
                                        <FooterStyle BackColor="#0080C0" />
                                        <AlternatingRowStyle BackColor="#CAE4FF" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>--%>
                    </table>
                <asp:Panel ID="PnlGrid" runat="server" Width="100%" Height="365px" ScrollBars="Auto"
                    BorderStyle="Inset" BorderWidth="1">
                    <asp:GridView ID="GdvFind" runat="server" EnableSortingAndPagingCallbacks="false" ShowFooter="true" Width="100%">
                        <HeaderStyle BackColor="#0080C0" Font-Size="Smaller" />
                        <FooterStyle BackColor="#0080C0" />
                        <AlternatingRowStyle BackColor="#CAE4FF" />
                     </asp:GridView>
                    <asp:HiddenField ID="TxtBoeNo" runat="server" />
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div >
    </div>
    </form>
     <script type="text/javascript">
         var prm = Sys.WebForms.PageRequestManager.getInstance();
         prm.add_initializeRequest(prm_InitializeRequest);
         prm.add_endRequest(prm_EndRequest);

         function prm_InitializeRequest(sender, args) {
             var panelProg = $get('divImage');
             if (args._postBackElement.id != "Timer1") {
                 panelProg.style.display = '';
             }
         };

         function prm_EndRequest(sender, args) {
             var panelProg = $get('divImage');
             panelProg.style.display = 'none';
         };
    </script>
</body>
</html>
