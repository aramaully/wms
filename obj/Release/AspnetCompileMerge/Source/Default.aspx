﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="Wms._Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%;">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 90%;
            height: 143px;
        }
        .style2
        {
            font-size: xx-large;
            font-weight: bold;
            height: 57px;
        }
        .style3
        {
        }
        .style4
        {
            width: 158px;
        }
        .style5
        {
            color: #FFFFFF;
            font-weight: bold;
            font-size: large;
        }
        .style6
        {
            color: #FFFFFF;
            font-weight: bold;
            font-size: large;
            height: 35px;
        }
        p.MsoNormal
        {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 10.0pt;
            margin-left: 0in;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri" , "sans-serif";
        }
        .style7
        {
            width: 70px;
        }
    </style>
</head>
<body style="height: 100%;">
    <form id="form1" runat="server">
    <table class="style1" style="height: 100%;">
        <tr>
            <td height="100" valign="top">
                <asp:Panel ID="Panel2" runat="server" Height="99px" Width="1000px" BorderStyle="None"
                    Style="background: url(images/logo.gif) no-repeat left top; padding-left: 10px;
                    padding-right: 10px; padding-bottom: 10px; padding-top: 10px">
               </asp:Panel></td>
        </tr>
        <tr>
            <td align="center" class="style2" valign="top" height="50">
                Warehouse Management System
            </td>
        </tr>
        <tr>
            <td align="center" height="150">
                <asp:Panel ID="Panel1" runat="server" Height="200px" Width="376px" BorderStyle="None"
                    Style="background: url(images/test3.gif) no-repeat left top; padding-left: 10px;
                    padding-right: 10px; padding-bottom: 5px; padding-top: 5px">
                    <table class="style1">
                        <tr>
                            <td align="center" class="style5" colspan="4">
                                Login
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="style6" colspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style3">
                                &nbsp;
                            </td>
                            <td align="left" class="style7">
                                User Name
                            </td>
                            <td align="left" class="style4">
                                <asp:TextBox ID="TxtUserName" runat="server" TabIndex="1" Width="125px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtUserName"
                                    ErrorMessage="User Name Required">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="right">
                                <asp:Button ID="ButLogin" runat="server" TabIndex="3" Text="Login" Width="75px" 
                                    Height="26px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style3">
                                &nbsp;
                            </td>
                            <td align="left" class="style7">
                                Password
                            </td>
                            <td align="left" class="style4">
                                <asp:TextBox ID="TxtPassword" runat="server" TabIndex="2" TextMode="Password" 
                                    Width="125px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtPassword"
                                    ErrorMessage="Password Required">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="right">
                                <asp:Button ID="ButCancel" runat="server" CausesValidation="False" TabIndex="4" Text="Cancel"
                                    Width="75px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style3">
                                &nbsp;
                            </td>
                            <td align="left" class="style3" colspan="3">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="SingleParagraph" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style3">
                                &nbsp;
                            </td>
                            <td class="style3" colspan="3">
                                <asp:Label ID="LblMsg" runat="server" Text="Enter User Name and Password to Login"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center" valign="bottom">
                <%--<p class="MsoNormal">
                    Copyright <span style="mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin">
                        © 2010</span> Software Lab Qatar W.L.L All rights reserved.</p>--%>
            </td>
        </tr>
    </table>
    <div>
    </div>
    </form>
</body>
</html>
