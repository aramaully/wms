﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmViewList.aspx.vb" Inherits="Wms.FrmViewList" %>

<%@ Register Assembly="DevExpress.Web.Bootstrap.v18.1, Version=18.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.Bootstrap" TagPrefix="dx" %>
<%@ Register Assembly="EO.Web" Namespace="EO.Web" TagPrefix="eo" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v18.1, Version=18.1.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Occupancy Status</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    <script language="javascript" type="text/javascript">
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/viewDetailed.gif', 'images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Occupancy Status'; //Set the name
            w.Width = 1024; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 450;
            w.CenterScreen(); // Center the window
            w.SetCloseable = true;
            //w.Resizeable = false; // Disable resize
            w.Minimizeable = false; // Disable minimize
            w.Maximizeable = false; // Disable maximize
            w.ShowDialog(); //Show the window
        };
        function ShowReport() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FrmRpt.aspx', w);
            return false;
        };
    </script>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript">
        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('PnlGrid').scrollLeft;
            yPos = $get('PnlGrid').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('PnlGrid').scrollLeft = xPos;
            $get('PnlGrid').scrollTop = yPos;
        }
        function FunctionConfirm(Tlb, TlbItem) {
            var validated = Page_ClientValidate('Group1');
        }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="~/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <table>
                <tr style="height:20px;">
                    <td>
                        <asp:Label ID="LblHead" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="Blue"></asp:Label>
                        &nbsp;<asp:Button ID="Print" runat="server" Text="Print" />
                    </td>
                </tr>
                <tr style="height:100%;">
                <td>
                    <div>
                        <asp:Panel ID="Panel10" runat="server" ScrollBars="Both"  Height="449px" 
                            Width="1023px" BorderStyle="Inset"  BorderWidth="1px">
                            <dx:BootstrapGridView ID="GdvClient" AutoGenerateColumns="True" Width="100%" runat="server" OnDataBinding="GdvClient_DataBinding">
                                <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"
                                    HideDataCellsAtWindowInnerWidth="560">
                                </SettingsAdaptivity>
                                <SettingsPager EnableAdaptivity="true" PageSize="15" AlwaysShowPager="true"></SettingsPager>
                                <SettingsDataSecurity AllowEdit="false" AllowInsert="false"></SettingsDataSecurity>
                                <SettingsSearchPanel Visible="True"></SettingsSearchPanel>
                                <SettingsBehavior AllowSelectByRowClick="true" AllowFocusedRow="true" />
                            </dx:BootstrapGridView>
                        </asp:Panel>
                    </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
     <script type="text/javascript">
         var prm = Sys.WebForms.PageRequestManager.getInstance();
         prm.add_initializeRequest(prm_InitializeRequest);
         prm.add_endRequest(prm_EndRequest);

         function prm_InitializeRequest(sender, args) {
             var panelProg = $get('divImage');
             if (args._postBackElement.id != "Timer1") {
                 panelProg.style.display = '';
             }
         };

         function prm_EndRequest(sender, args) {
             var panelProg = $get('divImage');
             panelProg.style.display = 'none';
         };
    </script>
</body>
</html>
