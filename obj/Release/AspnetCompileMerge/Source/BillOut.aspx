﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BillOut.aspx.vb" Inherits="Wms.BillOut" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Create Edit Bill OutBound</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Create Edit Bill OutBound'; //Set the name
            w.Top = 10;
            w.Left = 10;
            w.Width = 1089; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 560;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
    </script>

    </head>
<body onload="init();">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <div id="Content" style="width: 100%; height: 100%; padding: 2px; border-right: solid 1px #cddaea;">
                    <table cellpadding="0" cellspacing="0" frame="hsides" width="100%">
                        <tr>
                            <td bgcolor="#004080" width="2%">
                                &nbsp;</td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" width="43%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Bill Outbound"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" nowrap="nowrap" width="55%">
                                <asp:Button ID="ButNew" runat="server" Height="39px" Text="New" Style="cursor: pointer"
                                    ToolTip="Create New Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Bill Outbound" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save this Outbound Bill?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButNext" runat="server" Height="39px" Text="Next" Style="cursor: pointer"
                                    ToolTip="Go to Next Client" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButPrevious" runat="server" Height="39px" Text="Prev" Style="cursor: pointer"
                                    ToolTip="Go To Previous Client" Width="55px" CausesValidation="False" />
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete Charges?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButUpdateBal" runat="server" Height="39px" 
                                    Text="Update Balance" />
                            </td>
                        </tr>
                    </table>
                    <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" 
                        Height="390px" Width="100%">                    
                        <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="Bill Details">
                            <HeaderTemplate>
                                Bill Details
                            </HeaderTemplate>
                        <ContentTemplate>
                                <table width="100%">
                                    <tr>
                                        <td class="style2">
                                            Outbound</td>
                                        <td class="style1">
                                            <asp:TextBox ID="TxtBillOut" runat="server" AutoPostBack="True"></asp:TextBox>
                                        </td>
                                        <td valign="middle" class="style1" colspan="3">
                                            &nbsp;</td>
                                        <td colspan="2" class="style1">
                                            Date</td>
                                        <td class="style1">
                                            <asp:TextBox ID="TxtDate" runat="server" Width="100px"></asp:TextBox>
                                            <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" 
                                                Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtDate">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                            Client ID
                                        </td>
                                        <td valign="middle">
                                            <asp:TextBox ID="TxtClientId" runat="server" AutoPostBack="True"></asp:TextBox>
                                            <asp:Button ID="ButFindClient" runat="server" Text="..." />
                                            <asp:TextBox ID="TxtClientName" runat="server" Width="350px"></asp:TextBox>
                                        </td>
                                        <td colspan="3" valign="middle">
                                            &nbsp;</td>
                                        <td colspan="2">
                                            CPC Code</td>
                                        <td>
                                            <asp:DropDownList ID="CmbCpc" runat="server" Width="159px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                            Traffic Officer</td>
                                        <td valign="middle">
                                            <asp:DropDownList ID="CmbTrafficOfficer" runat="server" Height="22px" 
                                                Width="294px">
                                            </asp:DropDownList>
                                        </td>
                                        <td colspan="3" valign="middle">
                                            &nbsp;</td>
                                        <td colspan="2">
                                            FP Zone</td>
                                        <td>
                                            <asp:DropDownList ID="CmbZone" runat="server" Width="159px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                            Country</td>
                                        <td valign="middle">
                                            <asp:DropDownList ID="CmbCountry" runat="server" Height="22px" Width="294px">
                                            </asp:DropDownList>
                                        </td>
                                        <td colspan="3" valign="middle">
                                            &nbsp;</td>
                                        <td colspan="2">
                                            No of Items</td>
                                        <td>
                                            <asp:TextBox ID="TxtItems" runat="server" Width="97px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                            Warrant No</td>
                                        <td valign="middle">
                                            <asp:TextBox ID="TxtCbNo" runat="server"></asp:TextBox>
                                        </td>
                                        <td colspan="3" valign="middle">
                                            &nbsp;</td>
                                        <td colspan="2">
                                            Containers</td>
                                        <td>
                                            <asp:TextBox ID="TxtContainers" runat="server" Width="97px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style2">
                                            Transport</td>
                                        <td class="style1">
                                            <asp:TextBox ID="TxtTransport" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="style1" colspan="4">
                                            <asp:Button ID="ButAddCont" runat="server" style="cursor:pointer" Text="Add" 
                                                Width="60px" />
                                        </td>
                                        <td colspan="2" style="text-align: center" class="style1">
                                            Container Numbers</td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                            Truck No</td>
                                        <td>
                                            <asp:TextBox ID="TxtTruckNo" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="style4" colspan="4">
                                            <asp:Button ID="ButDelCont" runat="server" style="cursor:pointer" Text="Delete" 
                                                Width="60px" />
                                        </td>
                                        <td colspan="2" rowspan="3" valign="top">
                                            <asp:Panel ID="Panel10" runat="server" BorderColor="#CCCCCC" 
                                                BorderStyle="Solid" BorderWidth="1px" Height="145px" ScrollBars="Vertical" 
                                                Width="100%">
                                                <asp:GridView ID="GdvCont" runat="server" AutoGenerateColumns="False" 
                                                Font-Size="Smaller" Height="85px" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ChkSelect" runat="server" Visible="True" />
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Size="Smaller" />
                                                        <ItemStyle Width="15px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Type">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="Type" runat="server" BorderStyle="None" 
                                                                Text='<%# Eval("Type") %>' Visible="True" Width="100%">
                                                            </asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle BackColor="#6699FF" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Container No">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="ContNo" runat="server" BorderStyle="None" 
                                                                Text='<%# Eval("ContNo") %>' Visible="True" Width="100%">
                                                            </asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle BackColor="#6699FF" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#004080" ForeColor="White" />
                                            </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                
                                    <tr>
                                        <td class="style3">
                                            Remarks</td>
                                        <td colspan="2">
                                            <asp:TextBox ID="TxtRemarks" runat="server" Height="70px" TextMode="MultiLine" 
                                                Width="99%"></asp:TextBox>
                                            </td>
                                        <td class="style11" colspan="3">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="style3">
                                            Inbounds</td>
                                        <td valign="top" colspan="3">
                                            <asp:Panel ID="Panel11" runat="server" BorderColor="#CCCCCC" 
                                                BorderStyle="Solid" BorderWidth="1px" Height="121px" ScrollBars="Vertical">
                                                <asp:GridView ID="GdvBill" runat="server" AutoGenerateColumns="False" 
                                                    Height="37px" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkSelBill" runat="server" Visible="True"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="15px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Bill No" DataField="BillNo" />
                                                        <asp:BoundField DataField="BDate" HeaderText="Date" />
                                                    </Columns>
                                                    <HeaderStyle BackColor="#0066FF" />
                                                    <AlternatingRowStyle BackColor="#B0D8FF" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td valign="top" colspan="2">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="Item Details" Width="500px">
                            
                            <HeaderTemplate>
                                Item Details
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table width="200px" style="height: 334px; width: 100%;">
                                <tr>
                                <td class="style14">
                                    Select Product </td>
                                <td class="style18" align="right">
                                    <asp:Button ID="ButRefreshBill" runat="server" Text="Refresh" />
                                    </td>
                                    <td class="style12">
                                        <asp:Label ID="LblAddHead" runat="server" Font-Bold="True" ForeColor="#003399" 
                                            Text="Add Item to Outbound"></asp:Label>
                                    </td>
                                </tr>
                                 <tr>
                                <td height="48%" valign="top" colspan="3">
                                <asp:Panel ID="Panel3" runat="server" BorderStyle="Inset" BorderWidth="1px" 
                                            Height="170px" ScrollBars="Horizontal"  Width="985px">
                                        <asp:GridView ID="GdvInItem" runat="server" ShowFooter="True" Width="100%" 
                                            Height="171px" >
                                                <AlternatingRowStyle BackColor="#CAE4FF" />
                                                <FooterStyle BackColor="#0080C0" />
                                                <HeaderStyle BackColor="#0080C0" Font-Size="Smaller"/>
                                                <SelectedRowStyle BackColor="Yellow" />
                                            </asp:GridView>
                                        </asp:Panel>
                                </td>
                                </tr>
                                    <tr>
                                        <td class="style15" colspan="2">
                                            <asp:Label ID="LblText" runat="server"></asp:Label>
                                        </td>
                                        <td class="style15">
                                            Bill Outbound</td>
                                    </tr>
                                    <tr>
                                        <td height="48%" valign="top" colspan="2">
                                        <table class="style16">
                                        <tr>
                                            <td>
                                                <asp:Label ID="LblCode" runat="server" Text="Prod Code"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TxtProdCode" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="right" colspan="3">
                                                <asp:Button ID="ButAdd" runat="server" Text="Add" Width="75px" />
                                                <asp:Button ID="ButRemove" runat="server" Text="Remove" Width="75px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Description</td>
                                            <td colspan="4">
                                                <asp:TextBox ID="TxtDescription" runat="server" TextMode="MultiLine" 
                                                    Width="100%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td align="right">
                                                Original</td>
                                            <td align="right">
                                                Balance</td>
                                            <td align="right">
                                                Deliver Now</td>
                                            <td>
                                                Unit</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Quantity</td>
                                            <td align="right">
                                                <asp:TextBox ID="TxtQtyUnit" runat="server" Width="75px" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td class="style17">
                                                <asp:TextBox ID="TxtBalUnit" runat="server" Width="75px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TxtDelUnit" runat="server" Width="90px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="LblUnit" runat="server" Text="Unit"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                        </td>
                                        <td height="48%" valign="top">
                                            <asp:Panel ID="Panel14" runat="server" Height="150px" ScrollBars="Both" 
                                                BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Width="500px">
                                                <asp:GridView ID="GdvOut" runat="server" 
                                                    AutoGenerateColumns="False" style="margin-top: 0px">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkSelect" runat="server" Visible="True"></asp:CheckBox>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="15px" />
                                                        </asp:TemplateField>                                                   
                                                        <asp:BoundField DataField="BoeNo" HeaderText="Bill No" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" />
                                                        <asp:BoundField DataField="ItemNo" HeaderText="Item No" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" >
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="LineNo" HeaderText="Line No" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" >
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Code" HeaderText="Code" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" >
                                                            <ItemStyle Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" >
                                                            <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Unit" HeaderText="Unit" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" />
                                                        <asp:BoundField DataField="QtySubUnit" HeaderText="Quantity" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" >
                                                            <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="SubUnit" HeaderText="Sub Unit" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" />
                                                        <asp:BoundField DataField="Description" HeaderText="Description" 
                                                            HtmlEncode="False" HtmlEncodeFormatString="False" >
                                                            <ItemStyle Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Value" HeaderText="CIF Value" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" >
                                                            <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Weight" HeaderText="Weight" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" >
                                                            <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="BalWt" HeaderText="Bal Weight" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" >
                                                            <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="BalQty" HeaderText="Bal Qty" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" >
                                                            <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="BalSubQty" HeaderText="Bal Qty Sub" HtmlEncode="False" 
                                                            HtmlEncodeFormatString="False" >
                                                            <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#004080" ForeColor="White" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </cc1:TabPanel>                     
                    </cc1:TabContainer>                  
                    
                </div>
               
                 <asp:Panel ID="PnlFind" Style="z-index: 101; display:none; left: 100px; position: absolute; top: 60px"
                        runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                        Visible="True" BackColor="White">
                          <table cellpadding="0" cellspacing="0" style="height: 100%;" width="700px">
                            <tr>
                                <td  bgcolor="#004080" style="width: 25%">
                                    <asp:RadioButton ID="RbnClient" runat="server" Text="Client" Checked="true" 
                                        GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnCountry" runat="server" Text="Country" Checked="false" 
                                        GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnTraffic" runat="server" Text="Traffic Op" 
                                        Checked="false" GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnCont" runat="server" Text="Container" Checked="false" 
                                        GroupName="Select" ForeColor="White" />
                                </td>
                                <td bgcolor="#004080" style="width:65%" valign="top">
                                    <asp:Label ID="LblSearch" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Enter the text here to search"></asp:Label> <br />
                                    <asp:TextBox ID="TxtSearch" runat="server" BackColor="White" Width="100%"></asp:TextBox>
                                    <br /> <br />
                                    <asp:CheckBox ID="ChkDate" runat="server" Text="Date Range From " 
                                        ForeColor="White"/>                                    
                                    <asp:TextBox ID="TxtFrom" runat="server" Width="100px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtFrom" Format="dd/MM/yyyy">
                                    </cc1:CalendarExtender>
                                    <asp:Label ID="LblToDate" runat="server" Text=" To " ForeColor="White"></asp:Label>
                                    <asp:TextBox ID="TxtTo" runat="server" Width="100px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TxtTo" Format="dd/MM/yyyy">
                                    </cc1:CalendarExtender>
                                </td>
                                <td align="right" bgcolor="#004080" style="width: 10%">
                                    <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                        Width="65px" />
                                    <br />
                                    <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                        Width="65px" />
                                </td>
                            </tr>
                            <tr>                                
                                <td colspan="3">
                                    <asp:Panel ID="Panel8" runat="server" Height="300px" ScrollBars="Vertical" 
                                        Width="100%">
                                        <asp:GridView ID="GdvFind" runat="server" 
                                            EnableSortingAndPagingCallbacks="false" ShowFooter="true" Width="100%">
                                            <HeaderStyle BackColor="#0080C0" />
                                            <FooterStyle BackColor="#0080C0" />
                                            <AlternatingRowStyle BackColor="#CAE4FF" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="PnlAmend" Style="z-index: 101; display: none; left: 300px; position: absolute; padding-left:0px; padding-top:10px;
             padding-right:15px; background:url(images/amend.png) no-repeat left top; top: 50px;" runat="server" Height="210px" Width="620px">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table width="100%" style="height: 100%;">
                        <tr>
                            <td align="center" colspan="3" style="height: 25px">
                                <asp:Label ID="Label4" runat="server" Text="Amendment to Bill of Entry" Style="font-size: large;
                                    color: White"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:2%"></td>
                            <td align="left" style="height: 40px; width: 80%" valign="top">
                                <asp:Label ID="Label5" runat="server" Style="color: Black; text-align: center" Text="You have made some changes to an existing Bill of entry. Please enter the
                                details of the changes in the box below for records" Width="98%">
                                </asp:Label>
                            </td>
                            <td align="center" valign="middle">
                            </td>
                        </tr>
                        <tr>
                            <td style="width:2%"></td>
                            <td align="center" style="height: 40px" valign="top">
                                <asp:TextBox ID="TxtAmend" TextMode="MultiLine" Width="100%" runat="server" 
                                    AutoCompleteType="Company"></asp:TextBox>
                            </td>
                            <td align="center" valign="middle">
                                <asp:Button ID="ButCancel" runat="server" Text="Cancel" UseSubmitBehavior="false"
                                   style="cursor:pointer" Width="60px"  CausesValidation="false" OnClick="ButCancel_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:2%"></td>
                            <td colspan="2">
                                <asp:CheckBox ID="ChkNoAmend" runat="server" Text="Save without amendment Details" Checked="false" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:2%"></td>
                            <td colspan="2">
                                <asp:CheckBox ID="ChkClearDisc" runat="server" Text="Clear Discrepancy" Checked="false" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowAmend" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="ShowAmend"
            PopupControlID="PnlAmend" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
        
        
        
        
         <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
