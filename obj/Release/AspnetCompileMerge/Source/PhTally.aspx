﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PhTally.aspx.vb" Inherits="Wms.PhTally" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Physical Tally Sheet</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Physical Tally Sheet'; //Set the name
            w.Top = 10;
            w.Left = 10;
            w.Width = 1096; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 559;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '1';
                w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Delete") {
                Index = '3';
                w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 236px;
        }
        </style>

    </head>
<body onload="init();">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }

            // It is important to place this JavaScript code after ScriptManager1

            var xPos, yPos;
            var xPosTally, yPosTally;
            var xPosStatus, yPosStatus;

            var prm = Sys.WebForms.PageRequestManager.getInstance();



            function BeginRequestHandler(sender, args) {

                if ($get('<%=PnlGrid.ClientID%>') != null) {

                    // Get X and Y positions of scrollbar before the partial postback

                    xPos = $get('<%=PnlGrid.ClientID%>').scrollLeft;

                    yPos = $get('<%=PnlGrid.ClientID%>').scrollTop;

                }

                if ($get('<%=PnlTally.ClientID%>') != null) {

                    // Get X and Y positions of scrollbar before the partial postback

                    xPosTally = $get('<%=PnlTally.ClientID%>').scrollLeft;

                    yPosTally = $get('<%=PnlTally.ClientID%>').scrollTop;

                }
                if ($get('<%=PnlStatus.ClientID%>') != null) {

                    // Get X and Y positions of scrollbar before the partial postback

                    xPosStatus = $get('<%=PnlStatus.ClientID%>').scrollLeft;

                    yPosStatus = $get('<%=PnlStatus.ClientID%>').scrollTop;

                }

            }

            function EndRequestHandler(sender, args) {

                if ($get('<%=PnlGrid.ClientID%>') != null) {

                    // Set X and Y positions back to the scrollbar

                    // after partial postback

                    $get('<%=PnlGrid.ClientID%>').scrollLeft = xPos;

                    $get('<%=PnlGrid.ClientID%>').scrollTop = yPos;

                }

                if ($get('<%=PnlStatus.ClientID%>') != null) {

                    // Set X and Y positions back to the scrollbar

                    // after partial postback

                    $get('<%=PnlStatus.ClientID%>').scrollLeft = xPosStatus;

                    $get('<%=PnlStatus.ClientID%>').scrollTop = yPosStatus;

                }
                if ($get('<%=PnlTally.ClientID%>') != null) {

                    // Set X and Y positions back to the scrollbar

                    // after partial postback

                    $get('<%=PnlTally.ClientID%>').scrollLeft = xPosTally;

                    $get('<%=PnlTally.ClientID%>').scrollTop = yPosTally;

                }

            }

            prm.add_beginRequest(BeginRequestHandler);

            prm.add_endRequest(EndRequestHandler);
            
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <div id="Content" style="width: 100%; height: 100%; padding: 2px; border-right: solid 1px #cddaea;">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Physical Tally Sheet"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Charges File" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save this Physical Tally Sheet?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButPalletId" runat="server" Height="39px" Text="Pallet ID" Style="cursor: pointer"
                                    ToolTip="Generate Pallet IDs" Width="55px" UseSubmitBehavior="False" 
                                    Enabled="False" Font-Size="Smaller" ValidationGroup="Zone" />
                                <cc1:ConfirmButtonExtender ID="ButPalletId_ConfirmButtonExtender" 
                                    runat="server" ConfirmText="Sure to Generate Pallet IDs for received Items?" 
                                    Enabled="True" TargetControlID="ButPalletId">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButChDateIn" runat="server" Enabled="False" Font-Size="Smaller" 
                                    Height="39px" Text="Ch Date" ToolTip="Change Date In for this Lot" 
                                    ValidationGroup="Zone" Width="55px" />
                                <cc1:ConfirmButtonExtender ID="ButChDateIn_ConfirmButtonExtender" 
                                    runat="server" ConfirmText="Sure to Change Date for this Lot?" Enabled="True" 
                                    TargetControlID="ButChDateIn">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete Charges?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>   
                            <table class="style1">
                        <tr>
                            <td>
                                FP Zone</td>
                            <td>
                                <asp:DropDownList ID="CmbZone" runat="server" Width="75px">
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator4" runat="server" 
                                    ControlToValidate="CmbZone" ErrorMessage="Select FP Zone" Operator="NotEqual" 
                                    ValidationGroup="Zone" ValueToCompare="Select...">*</asp:CompareValidator>
                            </td>
                            <td>
                                Inbound</td>
                            <td>
                                <asp:TextBox ID="TxtBillNo" runat="server" AutoPostBack="True"></asp:TextBox>
                            </td>
                            <td>
                                Billdate</td>
                            <td>
                                <asp:TextBox ID="TxtBillDate" runat="server" ReadOnly="True" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                Date In</td>
                            <td>
                                <asp:TextBox ID="TxtDateIn" runat="server" Width="100px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtDateIn_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtDateIn">
                                </cc1:CalendarExtender>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Client</td>
                            <td colspan="3">
                                <asp:TextBox ID="TxtName" runat="server" ReadOnly="True" Width="98%"></asp:TextBox>
                            </td>
                            <td>
                                Lot No</td>
                            <td>
                                <asp:TextBox ID="TxtLotNo" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox>
                            </td>
                            <td>
                                Discrepancy Noted</td>
                            <td>
                                <asp:RadioButton ID="RbnNo" runat="server" Checked="True" Text="No" 
                                    GroupName="Discrepancy" />
                                <asp:RadioButton ID="RbnYes" runat="server" Text="Yes" 
                                    GroupName="Discrepancy" />
                            </td>
                        </tr>
                    </table>
                            <asp:Panel ID="Panel2" runat="server" Height="301px">
                                <table style="height: 100%;" width="100%">
                                    <tr>
                                        <td valign="top" width="50%" height="5%">  
                                            <asp:Label ID="LblMessage" runat="server" Font-Bold="True" ForeColor="#CC0000"></asp:Label>
                                        </td>
                                        <td valign="top" height="5%">
                                            &nbsp;</td>
                                        <td align="right" height="5%" valign="top">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="5%" valign="top" width="50%">
                                            <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="#0033CC" 
                                                Text="Inbound Products"></asp:Label>
                                        </td>
                                        <td height="5%" valign="top">
                                            <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="#0033CC" 
                                                Text="Status"></asp:Label>
                                        </td>
                                        <td align="right" height="5%" valign="top">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="45%" valign="top" width="50%">
                                            <asp:Panel ID="PnlGrid" runat="server" BorderColor="#CCCCCC" BorderStyle="Solid" 
                                                BorderWidth="1px" Height="130px" ScrollBars="Auto">
                                                <asp:GridView ID="GdvBoe" runat="server" AutoGenerateColumns="False" 
                                                    Width="100%">
                                                    <SelectedRowStyle BackColor="#FFD553" />
                                                    <HeaderStyle BackColor="#0066FF" ForeColor="White" />
                                                    <AlternatingRowStyle BackColor="#99CCFF" />
                                                    <SelectedRowStyle BackColor="#FFD553" />
                                                    <Columns>
                                                        <asp:BoundField DataField="PCode" HeaderText="Prod Code" 
                                                            HeaderStyle-HorizontalAlign="Left" >
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ProdDescription" HeaderText="Description" 
                                                            HeaderStyle-HorizontalAlign="Left" >
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" 
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" >
                                                            <HeaderStyle HorizontalAlign="Right" />
                                                            <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="UnitCode" HeaderText="Unit" 
                                                            HeaderStyle-HorizontalAlign="Left" >
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td height="45%" valign="top" colspan="2">
                                            <asp:Panel ID="PnlStatus" runat="server" BorderColor="#CCCCCC" BorderStyle="Solid" 
                                                BorderWidth="1px" Height="130px" ScrollBars="Auto">
                                                <asp:GridView ID="GdvStatus" runat="server" AutoGenerateColumns="False">
                                                    <HeaderStyle BackColor="#0066FF" ForeColor="White" />
                                                    <AlternatingRowStyle BackColor="#99CCFF" />
                                                    <SelectedRowStyle BackColor="#FFD553" />
                                                    <Columns>
                                                        <asp:BoundField DataField="Product" HeaderText="Product" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="Description" HeaderText="Description" HeaderStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="Declared" HeaderText="Declared" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="Received" HeaderText="Received" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="Variance" HeaderText="Variance" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" height="5%">
                                            <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="#0033CC" 
                                                Text="Tally Sheet"></asp:Label>
                                        </td>
                                        <td height="5%" valign="top" align="right" colspan="2">
                                            <asp:Button ID="ButRemove" runat="server" BackColor="#FFAEAE" 
                                                Text="Delete Selected Row" Width="150px" CausesValidation="False" />
                                            <cc1:ConfirmButtonExtender ID="ButRemove_ConfirmButtonExtender" runat="server" 
                                                ConfirmText="Sure to Delete the Selected row of Tally Sheet?" Enabled="True" 
                                                TargetControlID="ButRemove">
                                            </cc1:ConfirmButtonExtender>
                                            <asp:Button ID="ButAdd" runat="server" BackColor="#FFD553" 
                                                Text="Select Product from Inbound to add  to Tally Sheet" Width="325px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" height="45%" valign="top">
                                            
                                            <table class="style1" cellpadding="0">
                                                <tr>
                                                    <td rowspan="6">
                                                        <asp:Panel ID="PnlTally" runat="server" BorderColor="#CCCCCC" 
                                                            BorderStyle="Solid" BorderWidth="1px"
                                                            Height="145px" ScrollBars="Auto" Width="750px">
                                                            <asp:GridView ID="GdvPhTally" runat="server" AutoGenerateColumns="False" Width="100%">
                                                                <HeaderStyle BackColor="#0066FF" ForeColor="White" />
                                                                <AlternatingRowStyle BackColor="#99CCFF" />
                                                                <SelectedRowStyle BackColor="#FFAEAE" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="PCode" HeaderText="Prod Code" HeaderStyle-HorizontalAlign="Left" />
                                                                    <asp:BoundField DataField="DateIn" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Left"
                                                                        HeaderText="Date In" />
                                                                    <asp:BoundField DataField="LotNo" HeaderText="Lot No" HeaderStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="NoPallet" HeaderText="Pallets" ItemStyle-HorizontalAlign="Right"
                                                                        HeaderStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="QtyPerPallet" HeaderText="Per Pallet" ItemStyle-HorizontalAlign="Right"
                                                                        HeaderStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" ItemStyle-HorizontalAlign="Right"
                                                                        HeaderStyle-HorizontalAlign="Right" />
                                                                    <asp:BoundField DataField="Shared" HeaderText="Shared" HeaderStyle-HorizontalAlign="Center" />
                                                                    <asp:BoundField DataField="PRatio" DataFormatString="{0:N1}" HeaderText="P Ratio"
                                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                                    <asp:BoundField DataField="Palleted" HeaderText="Palleted" ItemStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-HorizontalAlign="Center" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </td>
                                                    <td>
                                                        Product</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtPCode" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Pallets</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtPallets" runat="server" Width="60px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Per Pallet</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtPerPallet" runat="server" Width="60px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Quantity</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtQuantityTest" runat="server" Width="100px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        P. Ratio</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtPRatio" runat="server" Width="60px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Shared</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtShared" runat="server"  Width="60px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>                  
                    
                    
                    
                </div>
                <asp:Panel ID="PnlFind" Style="z-index: 101; display:none; left: 100px; position: absolute; top: 60px"
                        runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                        Visible="True" BackColor="White">
                        <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                            <tr>
                                <td  bgcolor="#004080" style="width: 25%">
                                    <asp:RadioButton ID="RbnClient" runat="server" Text="Client" Checked="true" 
                                        GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnCountry" runat="server" Text="Country" Checked="false" 
                                        GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnTraffic" runat="server" Text="Traffic Op" 
                                        Checked="false" GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnCont" runat="server" Text="Container" Checked="false" 
                                        GroupName="Select" ForeColor="White" />
                                </td>
                                <td bgcolor="#004080" style="width:65%" valign="top">
                                    <asp:Label ID="LblSearch" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Enter the text here to search"></asp:Label> <br />
                                    <asp:TextBox ID="TxtSearch" runat="server" BackColor="White" Width="100%"></asp:TextBox>
                                    <br /> <br />
                                    <asp:CheckBox ID="ChkDate" runat="server" Text="Date Range From " 
                                        ForeColor="White"/>                                    
                                    <asp:TextBox ID="TxtFrom" runat="server" Width="100px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtFrom" Format="dd/MM/yyyy">
                                    </cc1:CalendarExtender>
                                    <asp:Label ID="LblToDate" runat="server" Text=" To " ForeColor="White"></asp:Label>
                                    <asp:TextBox ID="TxtTo" runat="server" Width="100px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TxtTo" Format="dd/MM/yyyy">
                                    </cc1:CalendarExtender>
                                </td>
                                <td align="right" bgcolor="#004080" style="width: 10%">
                                    <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                        Width="65px" />
                                    <br />
                                    <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                        Width="65px" />
                                </td>
                            </tr>
                            <tr>
                                
                                <td colspan="3">
                                    <asp:Panel ID="Panel8" runat="server" Height="300px" ScrollBars="Vertical" 
                                        Width="100%">
                                        <asp:GridView ID="GdvFind" runat="server" 
                                            EnableSortingAndPagingCallbacks="false" ShowFooter="true" Width="100%">
                                            <HeaderStyle BackColor="#0080C0" />
                                            <FooterStyle BackColor="#0080C0" />
                                            <AlternatingRowStyle BackColor="#CAE4FF" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
