﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Tariff.aspx.vb" Inherits="Wms.Tariff" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            height: 47px;
        }
        .style4
        {
        }
        .style5
        {
            width: 376px;
            height: 72px;
        }
        .style6
        {
            color: #FFFFFF;
            width: 84px;
            height: 72px;
        }
        .style7
        {
            height: 72px;
        }
                
        .ModalBackground
        {
        background-color: Gray;
	    filter: alpha(opacity=70);
	    opacity: 0.7;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 102; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" Width="60%" Height="100%" BorderStyle="Groove"
                    BorderWidth="1px">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td>
                                <asp:Label ID="lblText" runat="server" Text=""></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Tariff Card"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Activity" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save Client Data?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find Client" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButNext" runat="server" Height="39px" Text="Next" Style="cursor: pointer"
                                    ToolTip="Go to Next Client" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButPrevious" runat="server" Height="39px" Text="Prev" Style="cursor: pointer"
                                    ToolTip="Go To Previous Client" Width="55px" CausesValidation="False" />
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Activity Selected" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete Client Data?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButExit" runat="server" Height="39px" Text="Close" Style="cursor: pointer"
                                    ToolTip="Close this Module" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButExit_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Close this Module?"
                                    Enabled="True" TargetControlID="ButExit">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td width="20%">
                                Customer ID
                            </td>
                            <td>
                                <asp:TextBox ID="TxtClientID" runat="server" AutoPostBack="True"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TxtClientID" ErrorMessage="Enter Client ID">*</asp:RequiredFieldValidator>
                            </td>
                            <td width="20%">
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">
                                Name
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="TxtName" runat="server" Width="100%" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td width="20%">
                                &nbsp;
                                <asp:CompareValidator ID="CompareValidator2" runat="server" 
                                    ControlToValidate="TxtName" ErrorMessage="Invalid Client" Operator="NotEqual" 
                                    ValueToCompare="Invalid Client">*</asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">
                                Freeport Zone No
                            </td>
                            <td>
                                <asp:DropDownList ID="CmbFpZone" runat="server" AutoPostBack="True" Width="75%">
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                    ControlToValidate="CmbFpZone" ErrorMessage="Select Zone" Operator="NotEqual" 
                                    ValueToCompare="Select...">*</asp:CompareValidator>
                            </td>
                            <td width="40%">
                                <asp:TextBox ID="TxtFPZone" runat="server" Width="100%"></asp:TextBox>
                            </td>
                            <td width="20%">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                        <cc1:TabContainer ID="TabContainer1" runat="server" Width="100%" ActiveTabIndex="1"
                            Height="282px">
                            <cc1:TabPanel ID="GenTab" runat="server" HeaderText="General">
                                <HeaderTemplate>
                                    Tariff Card Standard
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel ID="PnlStd" runat="server" Height="200px" Width="100%">
                                        <asp:GridView ID="GdvStd" runat="server" Width="100%" AutoGenerateColumns="False">
                                            <AlternatingRowStyle BackColor="#CAE4FF" />
                                            <Columns>
                                                <asp:BoundField DataField="ChCode" HeaderText="Item Code" HtmlEncode="False" />
                                                <asp:BoundField DataField="Description" HeaderText="Description" HtmlEncode="False" />
                                                <asp:BoundField DataField="Unit" HeaderText="Unit" HtmlEncode="False" />
                                                <asp:BoundField DataField="Normal" HeaderText="Rate" HtmlEncode="False">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Select Rate">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSel" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="AnalysisCode" HeaderText="A. Code"  />
                                            </Columns>
                                            <HeaderStyle BackColor="#0080C0" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="ContactTab" runat="server" HeaderText="Contact Details">
                                <HeaderTemplate>
                                    Tariff Card Preferential
                                </HeaderTemplate>
                                   <ContentTemplate>
                                    <asp:Panel ID="PnlPref" runat="server" Height="200px" Width="100%">
                                        <asp:GridView ID="GdvPref" runat="server" Width="100%" AutoGenerateColumns="False">
                                            <AlternatingRowStyle BackColor="#CAE4FF" />
                                            <Columns>
                                                <asp:BoundField DataField="ChCode" HeaderText="Item Code" HtmlEncode="False" />
                                                <asp:BoundField DataField="Description" HeaderText="Description" HtmlEncode="False" />
                                                <asp:BoundField DataField="Unit" HeaderText="Unit" HtmlEncode="False" />
                                                <asp:TemplateField HeaderText="Rate">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TxtPref" runat="server" Text='<%# Eval("Pref") %>'
                                                            Visible="True" BorderStyle="None" Width="60px">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Select Rate">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSel" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="AnalysisCode" HeaderText="A. Code"/>
                                            </Columns>
                                            <HeaderStyle BackColor="#0080C0" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </ContentTemplate>
                            </cc1:TabPanel>
                        </cc1:TabContainer>
                    </asp:Panel>                
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Height="16px" />
                <asp:Panel ID="PnlFind" Style="z-index: 101; display:none; left: 300px; position: absolute; top: 60px"
                    runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                    Visible="True" BackColor="White">
                    <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                        <tr>
                            <td class="style6" bgcolor="#004080" valign="top">
                                <asp:RadioButton ID="RbtName" runat="server" Text="Name" Checked="True" GroupName="FinOption" />
                                <br />
                                <asp:RadioButton ID="RbtCity" runat="server" Text="City" GroupName="FinOption" />
                                <br />
                                <asp:RadioButton ID="RbtContact" runat="server" Text="Contact" GroupName="FinOption" />
                            </td>
                            <td class="style5" bgcolor="#004080">
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" style="text-align:center;"
                                    Text="Enter the text here to search"></asp:Label>
                                <asp:TextBox ID="TxtSearch" runat="server" Width="100%" BackColor="White"></asp:TextBox>
                            </td>
                            <td align="right" bgcolor="#004080" class="style7">
                                <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                    Width="65px" />
                                <br />
                                <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                    Width="65px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="style4" colspan="3">
                                <asp:Panel ID="Panel8" runat="server" Width="100%" Height="300px" ScrollBars="Vertical">
                                    <asp:GridView ID="GdvFind" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                        ShowFooter="true">
                                        <HeaderStyle BackColor="#0080C0" />
                                        <FooterStyle BackColor="#0080C0" />
                                        <AlternatingRowStyle BackColor="#CAE4FF" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:modalpopupextender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:modalpopupextender>
    </div>
    </form>
</body>
</html>


