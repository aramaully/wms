var IDC_Statusbar = function(documentObject, parentObject) {
    this.Document = documentObject;
    this.Parent = parentObject;
    this.Control = this.Document.createElement('DIV');
    this.Control.className = 'IDC_Statusbar';
    this.Parent.appendChild(this.Control); this.Labels = [];
};
IDC_Statusbar.prototype.AddIcon = function(icon, width, height) {
    var l = this.Document.createElement('SPAN');
    l.style.backgroundImage = 'url(' + icon + ')';
    l.style.width = width;
    l.style.height = height;
    l.className = 'IDC_StatusbarLabel_Icon';
    this.Control.appendChild(l);
    this.Labels.push(l);
    return l;
};
IDC_Statusbar.prototype.AddLabel = function(text) {
    var l = new IDC_Statusbar_Label(this, text);
    this.Labels.push(l); return l;
};
var IDC_Statusbar_Label = function(statusbar, text) {
    this.Statusbar = statusbar;
    this.Control = statusbar.Document.createElement('SPAN');
    this.Control.innerHTML = text;
    this.Statusbar.Control.appendChild(this.Control); this.Control.className = 'IDC_StatusbarLabel'; return this;
};
IDC_Statusbar_Label.prototype.SetNoBorder = function() { this.Control.className = 'IDC_StatusbarLabel'; };
IDC_Statusbar_Label.prototype.SetSunkenBorder = function() { this.Control.className = 'IDC_StatusbarLabel_Sunken'; };
IDC_Statusbar_Label.prototype.SetRaisedBorder = function() {
    this.Control.className = 'IDC_StatusbarLabel_Raised'; 
}; 