﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Activity.aspx.vb" Inherits="Wms.Activity" %>
<%--<%@ Register Namespace="Microsoft.Samples.Alert" TagPrefix="ms" Assembly="Alert" %> --%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Create / Edit Activity</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.

            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Create / Edit Activity'; //Set the name
            w.Width = 568; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 311;
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
    </script>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
        }
        .style3
        {
        }
        .style4
        {
            width: 58px;
        }
    </style>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    <div>
           <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>        
                                
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <div id="toolbar" style="width: 100%;">
                    <table cellpadding="0" cellspacing="0" class="style1">
                    <tr>
                    <td align="left" bgcolor="#004080" nowrap="nowrap" width="50%">
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                            Text="Create / Delete Activities"></asp:Label>
                        </td>
                        <td align="right" bgcolor="#004080" nowrap="nowrap">
                            <asp:Button ID="ButNew" runat="server" Height="39px" Text="New" style="cursor:pointer"
                                ToolTip="Create New Activity" Width="55px" />
                            <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" style="cursor:pointer"
                                ToolTip="Save Activity" Width="55px" />
                            <asp:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" 
                                ConfirmText="Sure to Save this Activity?" Enabled="True" 
                                TargetControlID="ButSave">
                            </asp:ConfirmButtonExtender>
                            <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" style="cursor:pointer"
                                ToolTip="Delete Activity Selected" Width="55px" />
                            <asp:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" 
                                ConfirmText="Sure to Delete this Activity?" Enabled="True" 
                                TargetControlID="ButDelete">
                            </asp:ConfirmButtonExtender>
                        </td>
                    </tr>
                    </table>
                 </div>
                 <div id="Content" style="width: 100%; height: 100%; padding: 2px; border-right: solid 1px #cddaea;">
                    <table class="style1">
                        <tr>
                            <td class="style4">
                                Activity
                            </td>
                            <td>
                                <asp:TextBox ID="TxtActivity" runat="server" Width="99%" MaxLength="50" Style="margin-left: 0px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style3" align="center" colspan="2">
                                Existing List
                            </td>
                        </tr>
                        <tr>
                            <td class="style2" colspan="2">
                                <asp:GridView ID="GridView" runat="server" AllowPaging="True" CellPadding="4" ForeColor="#333333"
                                    EnableSortingAndPagingCallbacks="false" PageSize="5" Width="99%" AutoGenerateColumns="False">
                                    <RowStyle BackColor="#EFF3FB" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField HtmlEncode="false" HeaderText="Activity" DataField="Activity" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
