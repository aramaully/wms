﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Charges.aspx.vb" Inherits="Wms.Charges" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            height: 47px;
        }
        .ModalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .style19
        {
            width: 16px;
        }
        .style20
        {
            width: 302px;
        }
    </style>
</head>
<body style="height: 100%">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" Width="80%" Height="450px" BorderStyle="Groove"
                    BorderWidth="1px">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Logistic Service Charges"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Charges File" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save Logistic Charges?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete Charges for this Zone?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButExit" runat="server" Height="39px" Text="Close" Style="cursor: pointer"
                                    ToolTip="Close this Module" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButExit_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Close this Module?"
                                    Enabled="True" TargetControlID="ButExit">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    <table runat="server" width="100%" style="height:400px" border="1">
                    <tr style="height:25px">
                    <td width="5%">
                        Zone
                    </td>
                    <td width="15%">
                        <asp:DropDownList ID="CmbZone" runat="server" Width="100%" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td width="10%">
                        Description
                    </td>
                    <td class="style20">
                        <asp:TextBox ID="TxtDesc" runat="server" Width="100%"></asp:TextBox>
                        <asp:DropDownList ID="CmbDesc" runat="server" Visible="False">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="13%">
                    
                        <asp:Button ID="ButAdd" runat="server" Text="Add" Width="50px" />
                        <asp:Button ID="ButDelRow" runat="server" Text="Delete" Width="50px" />
                    
                    </td>
                    </tr>
                    <tr>
                        <td colspan="5" valign="top">
                            <asp:Panel ID="Panel2" runat="server" Height="355px" ScrollBars="Vertical">
                                <asp:GridView ID="GdvCharges" runat="server" Width="100%" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkSelect" runat="server" Visible="True"></asp:CheckBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="15px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Code">
                                            <ItemTemplate>
                                                <asp:TextBox ID="Code" runat="server" Text='<%# Eval("ChCode") %>' Width="100%" Visible="True"
                                                    BorderStyle="None">
                                                </asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Acc Code" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="AccCode" runat="server" Text='<%# Eval("AccCode") %>' Width="100%"
                                                    Visible="True" BorderStyle="None">
                                                </asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Analysis Code" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="AnalysisCode" runat="server" Text='<%# Eval("AnalysisCode") %>' Width="100%"
                                                    Style="text-align: left" Visible="True" BorderStyle="None">
                                                </asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="Description" runat="server" Text='<%# Eval("Description") %>' Width="100%"
                                                    Style="text-align: left" Visible="True" BorderStyle="None">
                                                </asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="500px" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unit" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="Unit" runat="server" Text='<%# Eval("Unit") %>' Width="100%" Visible="True"
                                                    BorderStyle="None">
                                                </asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Normal">
                                            <ItemTemplate>
                                                <asp:TextBox ID="Normal" runat="server" Text='<%# Eval("Normal") %>' Width="100%"
                                                    Style="text-align: right" Visible="True" BorderStyle="None">
                                                </asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pref">
                                            <ItemTemplate>
                                                <asp:TextBox ID="Pref" runat="server" Text='<%# Eval("Pref") %>' Width="99%" Style="text-align: right"
                                                    Visible="True" BorderStyle="None">
                                                </asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#0080C0" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
