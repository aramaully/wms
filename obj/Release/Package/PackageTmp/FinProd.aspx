﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FinProd.aspx.vb" Inherits="Wms.FinProd" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Create Edit HSNomenclature</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Create Edit HSNomenclature'; //Set the name
            w.Width = 1042; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 509;
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '1';
                w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Delete") {
                Index = '3';
                w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 236px;
        }
    </style>

    </head>
<body onload="init();">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 102; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <div id="Content" style="width: 100%; height: 100%; padding: 2px; border-right: solid 1px #cddaea;">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Create / Edit Finished Products"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButNew" runat="server" Height="39px" Text="New" Style="cursor: pointer"
                                    ToolTip="Create New Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Charges File" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save this Product?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButNext" runat="server" Height="39px" Text="Next" Style="cursor: pointer"
                                    ToolTip="Go to Next Client" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButPrevious" runat="server" Height="39px" Text="Prev" Style="cursor: pointer"
                                    ToolTip="Go To Previous Client" Width="55px" CausesValidation="False" />
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete this Product?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>                    
                    <table class="style1" cellspacing="0">
                        <tr>
                            <td class="style21">
                                Finished Product Code</td>
                            <td>
                                <asp:TextBox ID="TxtCode" runat="server" AutoPostBack="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style21">
                                Description</td>
                            <td>
                                <asp:TextBox ID="TxtDescription" runat="server" MaxLength="50" Width="75%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style21">
                                Unit</td>
                            <td>
                                <asp:DropDownList ID="CmbUnit" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style21">
                                Sub Unit</td>
                            <td>
                                <asp:DropDownList ID="CmbSubUnit" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr style="border-top-style:solid; border-top-width:thick; border-top-color:Black">
                            <td class="style21" bgcolor="#FFCC00">
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="Black" 
                                    Text="Components"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#FFCC00">
                                <asp:Button ID="ButAddComp" runat="server" Text="Add new Component" 
                                    UseSubmitBehavior="False" />
                                <asp:Button ID="ButDelComp" runat="server" Text="Delete Selected Components" 
                                    UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelComp_ConfirmButtonExtender" runat="server" 
                                    ConfirmText="Sure to Delete Selected Components?" Enabled="True" 
                                    TargetControlID="ButDelComp">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="100%">
                                <asp:Panel ID="Panel11" runat="server" Height="275px" ScrollBars="Vertical" BorderStyle="Groove" BorderWidth="1">
                                    <asp:GridView ID="GdvComp" runat="server" AutoGenerateColumns="False" 
                                        Height="100%" Width="100%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkSelect" runat="server" Visible="True"></asp:CheckBox>
                                                </ItemTemplate>
                                                <ItemStyle Width="15px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Code">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Code" runat="server" Text='<%# Eval("ProdCode") %>' Width="99%"
                                                        Visible="True" BorderStyle="None" AutoPostBack="true" OnTextChanged="ProdCode_Change">
                                                    </asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="ButFindProd" runat="server" Text="Find" OnClick="FindPord_Click" />
                                                </ItemTemplate>
                                                <ItemStyle Width="15px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Description" runat="server" Text='<%# Eval("Description") %>' Width="99%"
                                                        Visible="True" BorderStyle="None" ReadOnly="true">
                                                    </asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle Width="400px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Qty">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Qty" runat="server" Text='<%# Eval("Quantity") %>' Width="99%"
                                                        Style="text-align: right" Visible="True" BorderStyle="None">
                                                    </asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Unit" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="Unit" runat="server" Text='<%# Eval("Unit") %>' Width="99%" Style="text-align: left"
                                                        Visible="True" BorderStyle="None" ReadOnly="true">
                                                    </asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                             </asp:TemplateField>
                                        </Columns>
                                       <HeaderStyle BackColor="#003399" Font-Size="Smaller" ForeColor="White" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>                    
                </div>
                <asp:Panel ID="PnlFind" Style="z-index: 101; left: 300px; position: absolute; top: 60px"
                    runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                    Visible="False" BackColor="White">
                    <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                        <tr>
                            <td class="style5" bgcolor="#004080">
                                <asp:Label ID="LblSearch" runat="server" Font-Bold="True" ForeColor="White" style="text-align:center;"
                                    Text="Enter the text here to search"></asp:Label>
                                <asp:TextBox ID="TxtSearch" runat="server" Width="100%" BackColor="White"></asp:TextBox>
                            </td>
                            <td align="right" bgcolor="#004080" class="style7">
                                <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                    Width="65px" />
                                <br />
                                <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                    Width="65px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="style4" colspan="2">
                                <asp:Panel ID="Panel8" runat="server" Width="100%" Height="300px" ScrollBars="Vertical">
                                    <asp:GridView ID="GdvFind" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                        ShowFooter="true">
                                        <HeaderStyle BackColor="#0080C0" />
                                        <FooterStyle BackColor="#0080C0" />
                                        <AlternatingRowStyle BackColor="#CAE4FF" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <asp:TextBox ID="FindResult" runat="server" Visible="false"></asp:TextBox>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:Panel ID="ErrorPanel" Style="z-index: 101; display: none; left: 400px; position: absolute; background-color:White;
            top: 100px;" runat="server" Height="200px" Width="300px" 
            BorderStyle="Groove" BorderWidth="3px">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="100%" style="height: 100%;">
                        <tr>
                            <td align="center" bgcolor="#FF9900" style="height: 25px">
                                <asp:Label ID="ErrHead" runat="server" Text="Error" Style="font-size: large; color: White"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="height: 135px" valign="top">
                                <asp:Label ID="ErrMsg" runat="server" Style="color: Black; text-align: center" Width="98%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 25px" align="center" valign="bottom">
                                <asp:Button ID="ButCloseErr" runat="server" Text="Close" OnClick="ButCloseErr_Click"
                                    UseSubmitBehavior="False" CausesValidation="False" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
