﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BillIn.aspx.vb" Inherits="Wms.BillIn" EnableEventValidation="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Create / Edit Bill of Entry</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Create / Edit Bill of Entry'; //Set the name
            w.Width = 1165; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 529;
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };        
        function ShowFind() {
            var FindWindow = window.open("FindClient.aspx", "FindClient", "width=565,height=435,top=200, left=575");
        };
        function ShowFindProd() {
            var FindWindow = window.open("FrmFindProduct.aspx", "FindProduct", "width=565,height=435,top=200, left=575");
        };          
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
        }
        </style>

</head>
<body onload="init();">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript">
        // Get the instance of PageRequestManager.
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        // Add initializeRequest and endRequest
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        // Called when async postback begins
        function prm_InitializeRequest(sender, args) {
            // get the divImage and set it to visible
            var panelProg = $get('divImage');
            panelProg.style.display = '';
            // reset label text
            var lbl = $get('<%= me.lblText.ClientID %>');
            lbl.innerHTML = '';

            // Disable button that caused a postback
            // $get(args._postBackElement.id).disabled = true;
        }

        // Called when async postback ends
        function prm_EndRequest(sender, args) {
            // get the divImage and hide it again
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';

            // Enable button that caused a postback
            // $get(sender._postBackSettings.sourceElement.id).disabled = false;
        }

        // It is important to place this JavaScript code after ScriptManager1

        var xPos, yPos;
        var XPosProd, yPosProd;
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        function BeginRequestHandler(sender, args) {

            if ($get('<%=PnlHS.ClientID%>') != null) {
                xPos = $get('<%=PnlHS.ClientID%>').scrollLeft;
                yPos = $get('<%=PnlHS.ClientID%>').scrollTop;
            }
        }

        if ($get('<%=PnlProd.ClientID%>') != null) {

            // Get X and Y positions of scrollbar before the partial postback

            XPosProd = $get('<%=PnlProd.ClientID%>').scrollLeft;

            yPosProd = $get('<%=PnlProd.ClientID%>').scrollTop;

        }

        function EndRequestHandler(sender, args) {
            if ($get('<%=PnlHS.ClientID%>') != null) {
                $get('<%=PnlHS.ClientID%>').scrollLeft = xPos;
                $get('<%=PnlHS.ClientID%>').scrollTop = yPos;
            }

            if ($get('<%=PnlProd.ClientID%>') != null) {

                // Set X and Y positions back to the scrollbar

                // after partial postback

                $get('<%=PnlProd.ClientID%>').scrollLeft = XPosProd;

                $get('<%=PnlProd.ClientID%>').scrollTop = yPosProd;

            }
        }

        prm.add_beginRequest(BeginRequestHandler);

        prm.add_endRequest(EndRequestHandler);
            
        </script>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="Panel1" runat="server" Width="100%" Height="650px" BorderStyle="Groove"
                    BorderWidth="1px">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                &nbsp;</td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Bill Inbound"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" class="style3" nowrap="nowrap" >
                                <asp:Button ID="ButNew" runat="server" Height="39px" Text="New" Style="cursor: pointer"
                                    ToolTip="Create New Bill Inbound" Width="55px" CausesValidation="False" 
                                    UseSubmitBehavior="False" />
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Bill Inbound" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save this Bill Inbound?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find Bills Inbound" Width="55px" CausesValidation="False" 
                                    UseSubmitBehavior="False" />
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Bill Inbound" Width="55px" CausesValidation="False" 
                                    UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete Bill Inbound?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" 
                        Height="518px" Width="100%">                    
                        <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="Bill Inbound">
                            <HeaderTemplate>
                                Inbound Details
                            </HeaderTemplate>
                            <ContentTemplate>
                            <table class="style1">
                                <tr>
                                    <td class="style25">
                                        FP Zone</td>
                                    <td>
                                        <asp:DropDownList ID="CmbZone" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style25" colspan="2" align="right">
                                        <asp:Label ID="LblText" runat="server" ForeColor="Red" style="font-weight: 700"></asp:Label>
                                    </td>
                                    <td align="right" colspan="2">
                                        Date</td>
                                    <td valign="middle">
                                        <asp:TextBox ID="TxtDate" runat="server" Width="100px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" 
                                            Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtDate">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style25">
                                        Bill Inbound</td>
                                    <td>
                                        <asp:TextBox ID="TxtBillIn" runat="server" AutoPostBack="True" 
                                            style="margin-left: 0px" Width="99%"></asp:TextBox>
                                    </td>
                                    <td align="right" class="style25" colspan="2">
                                        &nbsp;</td>
                                    <td colspan="2" align="right">
                                        Truck No</td>
                                    <td valign="middle">
                                        <asp:TextBox ID="TxtTruckNo" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style25">
                                        Client ID</td>
                                    <td>
                                        <asp:TextBox ID="TxtClientID" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:Button ID="ButFindClient" runat="server" CausesValidation="False" 
                                            style="cursor:pointer" Text="..." />
                                    </td>
                                    <td align="right" class="style25" colspan="2">
                                        <asp:TextBox ID="TxtName" runat="server" Width="100%"></asp:TextBox>
                                    </td>
                                    <td align="right" colspan="2">
                                        No of Items</td>
                                    <td valign="middle">
                                        <asp:TextBox ID="TxtItems" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Traffic Officer</td>
                                    <td colspan="4">
                                        <asp:DropDownList ID="CmbTrafficOp" runat="server" Width="70%">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="right">
                                        Containers</td>
                                    <td>
                                        <asp:TextBox ID="TxtCont" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        CPC Code</td>
                                    <td colspan="4">
                                        <asp:DropDownList ID="CmbCPC" runat="server" Width="70%">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="right">
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                    </td>
                                    <td colspan="2" valign="top">
                                        Remarks<asp:TextBox ID="TxtRemarks" runat="server" Height="85px" 
                                            TextMode="MultiLine" Width="100%"></asp:TextBox>
                                    </td>
                                    <td align="right" valign="top">
                                        <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Container Nos"></asp:Label>
                                        <br />
                                        <asp:Button ID="ButAddCont" runat="server" style="cursor:pointer" Text="Add" 
                                            Width="60px" />
                                        <asp:Button ID="ButDelCont" runat="server" style="cursor:pointer" Text="Delete" 
                                            Width="60px" />
                                    </td>
                                    <td align="right" colspan="3">
                                        <asp:Panel ID="Panel2" runat="server" BorderStyle="Inset" BorderWidth="1px" 
                                            Height="85px" ScrollBars="Vertical" Width="350px">
                                            <asp:GridView ID="GdvCont" runat="server" AutoGenerateColumns="False" 
                                                Font-Size="Smaller" Height="85px" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ChkSelect" runat="server" Visible="True" />
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Size="Smaller" />
                                                        <ItemStyle Width="15px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Type">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="Type" runat="server" BorderStyle="None" 
                                                                Text='<%# Eval("Type") %>' Visible="True" Width="100%">
                                                            </asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle BackColor="#6699FF" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Container No">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="ContNo" runat="server" BorderStyle="None" 
                                                                Text='<%# Eval("ContNo") %>' Visible="True" Width="100%">
                                                            </asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle BackColor="#6699FF" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#004080" ForeColor="White" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                             </ContentTemplate>
                        </cc1:TabPanel>     
                        <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="Item Details" Width="518px">        
                            <HeaderTemplate>
                                Item Details
                            </HeaderTemplate>
                            <ContentTemplate>        
                            <table class="style1" style="height: 100%">
                                <tr>
                                    <td runat="server" style="width:40%" valign="top">
                                        <asp:Panel ID="Panel6" runat="server" BorderStyle="Inset" BorderWidth="1px" 
                                            Height="406px" style="margin-right: 0px">
                                            <table class="style1">
                                                <tr>
                                                    <td bgcolor="#004080" colspan="4">
                                                        <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" 
                                                            Text="Item Code"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Item No</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtHSItemNo" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        HS Code</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtHsCode" runat="server" Width="134px" AutoPostBack="True"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="ButNewHSItem" runat="server" Text="New" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="ButAddHS" runat="server" style="cursor:pointer" Text="Add" 
                                                            Width="60px" />
                                                        <asp:Button ID="ButDeleteHS" runat="server" style="cursor:pointer" Text="Delete" 
                                                            Width="60px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Description</td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="TxtHSDescription" runat="server" TextMode="MultiLine" 
                                                            Width="99%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Quantity</td>
                                                    <td class="style23">
                                                        <asp:TextBox ID="TxtHsQuantity" runat="server" Width="134px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        Unit</td>
                                                    <td>
                                                        <asp:DropDownList ID="CmbHSUnit" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Weight</td>
                                                    <td class="style23">
                                                        <asp:TextBox ID="TxtHsWeight" runat="server" Width="134px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        FOB</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtFOB" runat="server" Width="134px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        CIF Value</td>
                                                    <td class="style23">
                                                        <asp:TextBox ID="TxtCIF" runat="server" Width="134px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        Origin</td>
                                                    <td>
                                                        <asp:DropDownList ID="CmbHSOrigin" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="style1">
                                                <tr>
                                                    <td bgcolor="#004080" colspan="4">
                                                        <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" 
                                                            Text="Actual Product Details"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Prod Code</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtProductCode" runat="server" AutoPostBack="True" 
                                                            Width="134px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="ButNewProd" runat="server" Text="New" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="ButFindProd" runat="server" Text="Find" Width="51px" />
                                                        <asp:Button ID="ButAddProd" runat="server" style="cursor:pointer" Text="Add" 
                                                            Width="60px" />
                                                        <asp:Button ID="ButDelProd" runat="server" style="cursor:pointer" Text="Delete" 
                                                            Width="60px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Description</td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="TxtItemDesc" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Quantity</td>
                                                    <td class="style23">
                                                        <asp:TextBox ID="TxtProdQtyUnit" runat="server" Width="134px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        Unit</td>
                                                    <td>
                                                        <asp:DropDownList ID="CmbProdUnit" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Item Type</td>
                                                    <td class="style23">
                                                        <asp:DropDownList ID="CmbItemType" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>Date In</td>
                                                    <td>
                                                        <asp:TextBox ID="TxtDateIn" runat="server" Width="100px"></asp:TextBox>
                                                        <cc1:CalendarExtender ID="TxtDateIn_CalendarExtender" runat="server" Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtDateIn">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                    <td runat="server" style="width:60%" valign="top">
                                    <asp:Panel ID="PnlHs" runat="server" BorderStyle="Inset" BorderWidth="1px" 
                                            Height="190px" ScrollBars="Horizontal"  Width="100%">
                                            <asp:GridView ID="GdvHSCode" runat="server" ShowFooter="True" Width="100%" >
                                                <AlternatingRowStyle BackColor="#CAE4FF" />
                                                <FooterStyle BackColor="#0080C0" />
                                                <HeaderStyle BackColor="#0080C0" Font-Size="Smaller"/>
                                                <SelectedRowStyle BackColor="Yellow" />
                                            </asp:GridView>
                                        </asp:Panel>
                                        <asp:Panel ID="PnlProd" runat="server" BorderStyle="Inset" BorderWidth="1px" 
                                            Height="190px" ScrollBars="Auto" Width="100%">
                                            <asp:GridView ID="GdvProduct" runat="server" ShowFooter="True" Width="100%">
                                                <AlternatingRowStyle BackColor="#CAE4FF" />
                                                <FooterStyle BackColor="#0080C0" />
                                                <HeaderStyle BackColor="#0080C0" Font-Size="Smaller"/>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr> 
                            </table> 
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>                 
                </asp:Panel>
                   
                <asp:Panel ID="Panel9" Style="z-index: 110; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>                      
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="PnlAmend" Style="z-index: 101; display: none; left: 300px; position: absolute; padding-left:0px; padding-top:10px;
             padding-right:15px; background:url(images/amend.png) no-repeat left top; top: 50px;" runat="server" Height="210px" Width="620px">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table width="100%" style="height: 100%;">
                        <tr>
                            <td align="center" colspan="3" style="height: 25px">
                                <asp:Label ID="Label4" runat="server" Text="Amendment to Bill of Entry" Style="font-size: large;
                                    color: White"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:2%"></td>
                            <td align="left" style="height: 40px; width: 80%" valign="top">
                                <asp:Label ID="Label5" runat="server" Style="color: Black; text-align: center" Text="You have made some changes to an existing Bill of entry. Please enter the
                                details of the changes in the box below for records" Width="98%">
                                </asp:Label>
                            </td>
                            <td align="center" valign="middle">
                                <%--<asp:Button ID="ButOk"  runat="server" Text="OK" UseSubmitBehavior="false" CausesValidation="false"
                                   style="cursor:pointer" Width="60px"  OnClick="ButOk_Click" />
                                    <cc1:ConfirmButtonExtender ID="CfmBtnAmend" runat="server" ConfirmText="Sure to Save Bill as Selected?"
                                    Enabled="True" TargetControlID="ButOk">
                                </cc1:ConfirmButtonExtender>--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:2%"></td>
                            <td align="center" style="height: 40px" valign="top">
                                <asp:TextBox ID="TxtAmend" TextMode="MultiLine" Width="100%" runat="server" 
                                    AutoCompleteType="Company"></asp:TextBox>
                            </td>
                            <td align="center" valign="middle">
                                <asp:Button ID="ButCancel" runat="server" Text="Cancel" UseSubmitBehavior="false"
                                   style="cursor:pointer" Width="60px"  CausesValidation="false" OnClick="ButCancel_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:2%"></td>
                            <td colspan="2">
                                <asp:CheckBox ID="ChkNoAmend" runat="server" Text="Save without amendment Details" Checked="false" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:2%"></td>
                            <td colspan="2">
                                <asp:CheckBox ID="ChkClearDisc" runat="server" Text="Clear Discrepancy" Checked="false" />
                            </td>
                        </tr>
                    </table>
                    <div id="DragPanel" style="width: 700px; height: 410px; text-align: center; position: absolute; display:none;
                left: 200px; top: 60px;" onmousedown="fDragging('DragPanel', event, true);" runat="server">
                <asp:HiddenField ID="HfLeft" runat="server" Value="200" />
                <asp:HiddenField ID="HfTop" runat="server" Value="60" />
                <asp:Panel ID="PnlFind" Style="z-index: 101; display: none;" runat="server" Height="410px"
                    Width="700px" Visible="True" BackColor="#B9DCFF" CssClass="pnlBg">
                    <table class="style1" cellpadding="0" cellspacing="0" style="height: 405px; width: 99%;"
                        align="center">
                        <tr>
                            <td bgcolor="#B9DCFF" valign="top" align="left">
                                <asp:RadioButton ID="RbtName" runat="server" Text="Name" Checked="True" GroupName="FinOption" />
                                <asp:CheckBox ID="ChkPotential" runat="server" Text="Potential Client Only" />
                                <br />
                                <asp:RadioButton ID="RbtCity" runat="server" Text="City" GroupName="FinOption" />
                                <br />
                                <asp:RadioButton ID="RbtContact" runat="server" Text="Contact" GroupName="FinOption" />
                            </td>
                            <td bgcolor="#B9DCFF">
                                <asp:Label ID="Label7" runat="server" Font-Bold="True" ForeColor="Black" Style="text-align: center;"
                                    Text="Enter the text here to search"></asp:Label>
                                <asp:TextBox ID="TxtSearch" runat="server" Width="100%" BackColor="White"></asp:TextBox>
                            </td>
                            <td align="right" bgcolor="#B9DCFF">
                                <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                    Width="65px" />
                                <br />
                                <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                    Width="65px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" bgcolor="White" align="center">
                                <asp:Panel ID="Panel8" runat="server" Width="690px" Height="344px" ScrollBars="Vertical"
                                    BorderWidth="1px" BorderStyle="Inset">
                                    <asp:GridView ID="GdvFind" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                        ShowFooter="true">
                                        <HeaderStyle BackColor="#0080C0" />
                                        <FooterStyle BackColor="#0080C0" />
                                        <AlternatingRowStyle BackColor="#CAE4FF" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <cc1:RoundedCornersExtender ID="PnlFind_RoundedCornersExtender" runat="server" Enabled="True"
                    TargetControlID="PnlFind">
                </cc1:RoundedCornersExtender>
                <asp:Panel ID="PnlFindProp" Style="z-index: 101; display: none;" runat="server" Height="410px"
                    Width="700px" Visible="True" BackColor="#B9DCFF" CssClass="pnlBg">
                    <table cellpadding="0" cellspacing="0" style="height: 405px; width: 99%;" align="center">
                        <tr>
                            <td class="style5" bgcolor="#B9DCFF" align="left">
                                <asp:Image ID="Image2" runat="server" ImageUrl="Images/Help.png" />
                                <asp:Label ID="LblSearchHead" runat="server" Font-Bold="True" ForeColor="Black" Style="text-align: center;"
                                    Text="Proposals Submitted"></asp:Label>
                            </td>
                            <td bgcolor="#B9DCFF" style="text-align: right">
                                <asp:Button ID="ButCloseProp" runat="server" CausesValidation="False" Text="Close"
                                    Height="30px" Width="65px" />
                            </td>
                        </tr>
                        <tr style="vertical-align: top">
                            <td colspan="2" bgcolor="White" align="center">
                                <asp:Panel ID="Panel5" runat="server" Width="690px" Height="380px" ScrollBars="Vertical"
                                    BorderWidth="1px" BorderStyle="Inset">
                                    <asp:GridView ID="GdvProposal" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                        ShowFooter="true">
                                        <HeaderStyle BackColor="#0080C0" />
                                        <FooterStyle BackColor="#0080C0" />
                                        <AlternatingRowStyle BackColor="#CAE4FF" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <cc1:RoundedCornersExtender ID="PnlFindProp_RoundedCornersExtender" runat="server"
                    Enabled="True" TargetControlID="PnlFindProp">
                </cc1:RoundedCornersExtender>
                <asp:Panel ID="PlnFindLease" Style="z-index: 85; display: none;" runat="server" Height="410px"
                    Width="750px" Visible="True" BackColor="#B9DCFF" CssClass="pnlBg">
                    <table cellpadding="0" cellspacing="0" style="height: 405px; width: 99%;" align="center">
                        <tr>
                            <td class="style5" bgcolor="#B9DCFF" align="left">
                                <asp:Image ID="Image1" runat="server" ImageUrl="Images/Help.png" />
                                <asp:Label ID="Label8" runat="server" Font-Bold="True" ForeColor="Black" Style="text-align: center;"
                                    Text="Lease Created"></asp:Label>
                            </td>
                            <td bgcolor="#B9DCFF" style="text-align: right">
                                <asp:Button ID="ButCloseLease" runat="server" CausesValidation="False" Text="Close"
                                    Height="30px" Width="65px" />
                            </td>
                        </tr>
                        <tr style="vertical-align: top">
                            <td colspan="2" bgcolor="White" align="center">
                                <asp:Panel ID="Panel7" runat="server" Width="740px" Height="380px" ScrollBars="Vertical"
                                    BorderWidth="1px" BorderStyle="Inset">
                                    <asp:GridView ID="GdvFindLease" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                        ShowFooter="true">
                                        <HeaderStyle BackColor="#0080C0" />
                                        <FooterStyle BackColor="#0080C0" />
                                        <AlternatingRowStyle BackColor="#CAE4FF" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <cc1:RoundedCornersExtender ID="PlnFindLease_RoundedCornersExtender" runat="server"
                    Enabled="True" TargetControlID="PlnFindLease">
                </cc1:RoundedCornersExtender>
            </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowAmend" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="ShowAmend"
            PopupControlID="PnlAmend" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
        <asp:Panel ID="ErrorPanel" Width="359px" runat="server" Style="display: none; background: url(images/ErrPnl.png) no-repeat left top">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="4" cellspacing="4" style="height: 200px; margin-left:8px">
                        <tr style="height: 50px">
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td align="center" style="border-top: #ddd 2px groove; margin-left:3px">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
            <script type="text/javascript">
                // Get the instance of PageRequestManager.
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                // Add initializeRequest and endRequest
                prm.add_initializeRequest(prm_InitializeRequest);
                prm.add_endRequest(prm_EndRequest);

                // Called when async postback begins
                function prm_InitializeRequest(sender, args) {
                    // get the divImage and set it to visible
                    var panelProg = $get('divImage');
                    panelProg.style.display = '';
                    // reset label text
                    var lbl = $get('<%= me.lblText.ClientID %>');
                    lbl.innerHTML = '';

                    // Disable button that caused a postback
                    // $get(args._postBackElement.id).disabled = true;
                }

                // Called when async postback ends
                function prm_EndRequest(sender, args) {
                    // get the divImage and hide it again
                    var panelProg = $get('divImage');
                    panelProg.style.display = 'none';

                    // Enable button that caused a postback
                    // $get(sender._postBackSettings.sourceElement.id).disabled = false;
                }
        </script>
</body>
</html>
