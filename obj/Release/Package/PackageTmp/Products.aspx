﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Products.aspx.vb" Inherits="Wms.Products" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Create / Edit Product</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.

            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Create / Edit Product'; //Set the name
            w.Width = 1054; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 550;
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
    </script>

    <style type="text/css">
        .style3
        {            width: 0%;
        }
        </style>

</head>
<body onload="init();">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 102; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <div id="Content" style="width: 100%; height: 400px; overflow: auto; padding: 2px;
                    border-right: solid 1px #cddaea;">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="50%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Create / Edit Products"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButNew" runat="server" Height="39px" Text="New" Style="cursor: pointer"
                                    ToolTip="Create New Product" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Product File" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save this Product?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find Product" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButNext" runat="server" Height="39px" Text="Next" Style="cursor: pointer"
                                    ToolTip="Go to Next Product" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButPrevious" runat="server" Height="39px" Text="Prev" Style="cursor: pointer"
                                    ToolTip="Go To Previous Product" Width="55px" CausesValidation="False" />
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Product" Width="55px" CausesValidation="False" 
                                    UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete This Product?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButExit" runat="server" Height="39px" Text="Close" Style="cursor: pointer"
                                    ToolTip="Close this Module" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButExit_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Close this Module?"
                                    Enabled="True" TargetControlID="ButExit">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    
                    <table class="style1">
                        <tr>
                            <td width="15%">
                                Product Code</td>
                            <td class="style27" width="35%">
                                <asp:TextBox ID="TxtProdCode" runat="server" AutoPostBack="True"></asp:TextBox>
                            </td>
                            <td width="15%">
                                Vehicle</td>
                            <td width="35%">
                                <asp:CheckBox ID="ChkVehicle" runat="server" Text="Yes" />
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Name</td>
                            <td class="style21" colspan="2" width="35%">
                                <asp:TextBox ID="TxtName" runat="server" Width="99%"></asp:TextBox>
                            </td>
                            <td width="35%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="15%">
                                HS Code</td>
                            <td class="style27" width="35%">
                                <asp:TextBox ID="TxtHSCode" runat="server"></asp:TextBox>
                            </td>
                            <td width="15%">
                                Description</td>
                            <td rowspan="3" valign="top" width="35%">
                                <asp:TextBox ID="TxtDescription" runat="server" Height="70px" TextMode="MultiLine" 
                                    Width="99%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Unit</td>
                            <td class="style27" width="35%">
                                <asp:DropDownList ID="CmbUnit" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td width="15%">
                                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                    ControlToValidate="CmbUnit" ErrorMessage="Select Unit" Operator="NotEqual" 
                                    ValueToCompare="Select...">*</asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Sub Unit</td>
                            <td class="style27" width="35%">
                                <asp:DropDownList ID="CmbSubUnit" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td width="15%">
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Quantity / Pallet</td>
                            <td class="style27" width="35%">
                                <asp:TextBox ID="TxtQtyPerPallet" runat="server" style="text-align:right"></asp:TextBox>
                            </td>
                            <td width="15%">
                            </td>
                            <td width="35%">
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Pallet Ratio</td>
                            <td class="style27" width="35%">
                                <asp:TextBox ID="TxtPalletRatio" runat="server" style="text-align:right"></asp:TextBox>
                            </td>
                            <td width="15%">
                            </td>
                            <td width="35%">
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Level From</td>
                            <td class="style27" width="35%">
                                <asp:TextBox ID="TxtLevelFrom" runat="server" style="text-align:right"></asp:TextBox>
                            </td>
                            <td width="15%">
                                Level To</td>
                            <td width="35%">
                                <asp:TextBox ID="TxtLevelTo" runat="server" style="text-align:right"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Preferred Zone</td>
                            <td class="style27" width="35%">
                                <asp:DropDownList ID="CmbZone" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td width="15%">
                            </td>
                            <td width="35%">
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Store on Rack</td>
                            <td class="style27" width="35%">
                                <asp:CheckBox ID="ChkStoreonRack" runat="server" Text="Yes" />
                            </td>
                            <td width="15%">
                            </td>
                            <td width="35%">
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                Remarks</td>
                            <td class="style23" colspan="3" width="35%">
                                <asp:TextBox ID="TxtRemarks" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Height="38px" 
                        Width="890px" />
                </div>
                 <asp:Panel ID="PnlFind" Style="z-index: 101; left: 100px; position: absolute; top: 60px"
                        runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                        Visible="False" BackColor="White">
                        <table class="style1" cellpadding="0" cellspacing="0" 
                            style="height: 100%; width: 393px;">
                            <tr>
                                <td  bgcolor="#004080">
                                    <asp:Label ID="LblSearch" runat="server" Font-Bold="True" ForeColor="White" Style="text-align: center;"
                                        Text="Enter the text here to search"></asp:Label>
                                    <asp:TextBox ID="TxtSearch" runat="server" Width="100%" BackColor="White"></asp:TextBox>
                                </td>
                                <td align="right" bgcolor="#004080" class="style7">
                                    <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                        Width="65px" />
                                    <br />
                                    <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                        Width="65px" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Panel ID="Panel8" runat="server" Width="100%" Height="300px" ScrollBars="Vertical">
                                        <asp:GridView ID="GdvFind" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                            ShowFooter="true">
                                            <HeaderStyle BackColor="#0080C0" />
                                            <FooterStyle BackColor="#0080C0" />
                                            <AlternatingRowStyle BackColor="#CAE4FF" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>