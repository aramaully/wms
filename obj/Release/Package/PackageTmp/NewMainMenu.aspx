﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewMainMenu.aspx.vb" Inherits="Wms.NewMainMenu" %>


<html>
<head>
    <title>Warehouse Management System</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/windowmanager/000.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Listview.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    <script language="javascript" type="text/javascript" src="js/IDC_WindowManager.js"></script>
    <script language="javascript" type="text/javascript" src="js/IDC_Listview.js"></script>
    <script language="javascript" type="text/javascript" src="js/IDC_Toolbar.js"></script>
    <script language="javascript" type="text/javascript">
        var windowManager = null;
        var core = null;
        var cmdStart = null;
        var widgets = null;
        var desktopContext = null;
        var startMenu = null;
        function init() {
            //Create a new window manager assigned to the divDesktop object
            windowManager = new IDC_WindowManager(document, document.getElementById('divDesktop'), 'styles/windowmanager/000.css');
            core = windowManager.Core;
            //Create taskbar button and quicklinks
            startMenu = document.getElementById('tblStartmenu');
            startMenu.OriginalParent = startMenu.parentNode;
            cmdStart = windowManager.Desktop.Taskbar.CreateTaskbarButton('images/login32.png', 'images/login.png', 'Start',
                function() {
                    //Manual clean up
                    while (windowManager.Desktop.Taskbar.MultipleWindowsControl.hasChildNodes()) {
                        windowManager.Desktop.Taskbar.MultipleWindowsControl.removeChild(windowManager.Desktop.Taskbar.MultipleWindowsControl.lastChild);
                    }
                    //Manual add to contextmenu container
                    windowManager.Desktop.Taskbar.MultipleWindowsControl.appendChild(startMenu);
                    //Manual show of contextmenu
                    windowManager.ContextMenuManager.Show(windowManager.Desktop.Taskbar.MultipleWindowsOuterControl, 0, null, null, 40,
                            function() { return (windowManager.Desktop.Taskbar.MultipleWindowsOuterControl.offsetWidth - startMenu.offsetWidth) / 2; }
                        );
                });
            cmdStart.AnimationEnabled = true;
            cmdStart.Control.oncontextmenu = null;
        };
        function ShowMenu(item) {
            obj.style.display = "";
        }
    </script>
</head>
<body onload="init();">
    <div style="width: 100%; height: 100%;">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
            <tr>
                <td id="divDesktop" valign="top" style="height: 100%;">
                </td>
            </tr>
        </table>
    </div>

    <div style="position: absolute; z-index: -1000; display:none;">
        <table border="0" cellpadding="0" cellspacing="0" id="tblStartmenu" style="width: 100%;
            height: 100%;">
            <tr>
                <td valign="bottom" class="StartmenuButtonsBack" dontcollapse="true">
                    <span class="StartmenuCurrentUser">
                        <span id="spStartmenuUsername" class="StartmenuCurrentUserText">
                            <asp:Label ID="CurrentUser" runat="server" Text="Label"></asp:Label>
                        </span>
                    </span>
                    <a href="#" onmouseover="spDocContent.style.display='';" onmouseout="spDocContent.style.display='none';"
                        dontcollapse="true" onclick="" class="StartmenuButtonChildren">
                        <img alt="Start Menu" dontcollapse="true" src="images/MasterFiles.ico" border="0" class="StartmenuButtonImg" />
                        <span dontcollapse="true" class="StartmenuButtonTxt">Master Files</span> 
                        <span id="spDocContent"
                            style="display: none; top:0px" class="StartmenuSubMenuFrame">
                            <div id="ClientMaster" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('CheckClient.aspx');">
                                Client Master</div>
                             <div id="Activities" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmNewActivity.aspx');">
                                Activity</div>
                              <div id="DashBoard" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmDashBoard.aspx');">
                                DashBoard</div>
                            <div id="ProductMaster" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmNewProduct.aspx');">
                                Product</div> 
                            <div id="Unit" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmNewUnit.aspx');">
                                Unit</div>
                            <div id="Regime" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmNewRegime.aspx');">
                                Regime / CPC Code</div>
                            <div id="Warehouse" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmNewWareHouse.aspx');">
                                Warehouse</div>
                            <div id="Location" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('FrmNewLocation.aspx');">
                                Location</div>
                            <div id="Zones" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('Zones.aspx');">
                                Zones</div>
                            <div id="TrafficOfficer" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('Traffic.aspx');">
                                Traffic Officer</div>
                           <div id="TEst" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                onclick="windowManager.CreateWindow('Country.aspx');">
                                Country</div>      
                        </span>
                    </a>
                    <a href="#" onmouseover="SpnProcess.style.display='';" onmouseout="SpnProcess.style.display='none';"
                           dontcollapse="true" onclick="" class="StartmenuButtonChildren">
                           <img dontcollapse="true" src="images/Process.ico" border="0" class="StartmenuButtonImg" />
                           <span dontcollapse="true" class="StartmenuButtonTxt">Process</span> 
                           <span id="SpnProcess"
                                style="display: none; top:-300px" class="StartmenuSubMenuFrame">
                                <div id="BillInbound" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('BillIn.aspx');">
                                    Bill Inbound</div>
                                <div id="BillOutbound" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('BillOut.aspx');">
                                    Bill OutBound</div>
                               <div id="ReWarehousing" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('ReWareHouse.aspx');">
                                    Re Warehousing</div>
                               <div id="ReviseBills" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('ReviseBOE.aspx');">
                                    Revise Bills</div>
                               <div id="PhTally" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('PhTally.aspx');">
                                    Physical Tally Sheet</div>
                               <div id="StoreContainers" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('StoreContainers.aspx');">
                                    Store Containers</div>
                               <div id="ReleaseContainers" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('ReleaseContainers.aspx');">
                                    Release Containers</div>
                               <div id="AutomaticPutAway" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('APutAway.aspx');">
                                    Automatic PutAway</div>
                               <div id="ManualPutAway" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('MPutAway.aspx');">
                                    Manual PutAway</div>
                               <div id="AutomaticPicking" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('APicking.aspx');">
                                    Automatic Picking</div>
                              <div id="ManualPicking" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('MPicking.aspx');">
                                    Manual Picking</div>
                              <div id="AmendPallet" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('AmendPallet.aspx');">
                                    Amend Pallet Details</div>
                              <div id="ChangePalletRatio" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('ChPalletRatio.aspx');">
                                    Change Pallet Ratio</div>
                              <div id="ComputeOccupancy" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('Occupancy.aspx');">
                                    Compute Occupancy</div>
                              <div id="InvoiceOccupancy" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('InvOcc.aspx');">
                                    Create Invoice From Occupancy</div>
                              <div id="LogCharge" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('LogCharge.aspx');">
                                    Charges For Logistic Services</div>
                             <div id="EditInvoice" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('EditInv.aspx');">
                                    Edit Invoices</div>
                             <div id="InvoicePayment" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('PayInv.aspx');">
                                    Payment of Invoices</div>
                            <div id="ClearLocation" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('ClearLoc.aspx');">
                                    Clear Location</div>
                             <div id="AbandonGoods" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('Abandon.aspx');">
                                    Abandon Goods</div>
                           </span>
                    </a>
                    <a href="#" onmouseover="SpnReports.style.display='';" onmouseout="SpnReports.style.display='none';"
                           dontcollapse="true" onclick="" class="StartmenuButtonChildren">
                           <img dontcollapse="true" src="images/folder32.png" border="0" class="StartmenuButtonImg" />
                           <span dontcollapse="true" class="StartmenuButtonTxt">Reports </span>
                           <span id="SpnReports" style="display: none; top:-115px;" class="StartmenuSubMenuFrame">
                           <div id="PutAwayListRep" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('PutawayList.aspx');">
                                    Put Away List</div>
                                <div id="LeaseAnalysis" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('LeaseAnalysis.aspx');">
                                    Lease Analysis</div>
                                <div id="OpClients" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('OpClients.aspx');">
                                    Operational Clients</div>
                                <div id="OccupancyReport" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmOccupancyReport.aspx');">
                                    Occupancy Report</div>
                                <div id="ContainerReport" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmContAccRep.aspx');">
                                    Container Report</div>
                               <div id="DivTimPro" runat="server" visible="False" onclick="" class="IDCWMDesktopTaskbarContextMenuItem"
                                        style="border-top-style: solid; border-width: 1"
                                        onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover'; SpnTimPro.style.display='';" 
                                        onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem'; SpnTimPro.style.display='none';">
                                       Accounts Report
                                <span id="SpnTimPro" style="display: none; top:20px;"  class="StartmenuSubMenuFrame">
                                    <div id="RequestForm" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                        onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                        onclick="windowManager.CreateWindow('FrmRequestFormRep.aspx');">
                                        Request Form Report</div>
                                    <div id="HandingServicesRep" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                        onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                        onclick="windowManager.CreateWindow('FrmHanldingRep.aspx');">
                                        Handing Services Report</div>
                                     <div id="SupervisionRep" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                        onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                        onclick="windowManager.CreateWindow('FrmAccSupervisionRep.aspx');">
                                        SuperVision Report</div>
                                     <div id="VehicleList" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                        onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                        onclick="windowManager.CreateWindow('FrmVehicleList.aspx');">
                                        List of Vehicle Stored</div>
                                    </span>
                                </div>
                                <div id="RentalRate" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    style="border-top-style: solid; border-width: 1"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmRentalRate.aspx');">
                                    Rental Rate</div>
                                <div id="ProposalStatus" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmProposalStatus.aspx');">
                                    Proposal Status</div>
                                <div id="StatusofLicense" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmStatusoflicense.aspx');">
                                    Status of Freeport Certificate And Permit</div>
                                <div id="OccupancyDashboard" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmOccupancyDashboard.aspx');">
                                    Occupancy Dashboard</div> 
                                <div id="OccupancySummary" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmOccupancySummary.aspx');">
                                    Occupancy By Freeport Zone And Segment</div>                                    
                                <div id="VehicleMovement" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmVehicleMovement.aspx');">
                                    Vehicle Movement </div>
                               <div id="ClientMovement" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmClientMovement.aspx');">
                                    Client Movement </div>
                                <div id="ContainerList" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmContainerReport.aspx');">
                                    Stored Container List </div>
                                <div id="ClientStock" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('MonReturn.aspx');">
                                    Stock Return</div>
                                <div id="PrintInvoice" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('PrintInvoice.aspx');">
                                    Print Invoice</div>
                               <div id="BillStatus" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('BillStatus.aspx');">
                                    Bill Status</div>
                              <div id="StockByClient" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('RepStkClient.aspx');">
                                    Stock By Client</div>
                              <div id="StockByInbound" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('RepStkInBound.aspx');">
                                    Stock By InBound</div>
                            <div id="PickingListRep" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('PrintPickList.aspx');">
                                    Picking List</div>                                    
                             <div id="ExpiredInbound" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('Expired.aspx');">
                                    Expired InBound</div>
                             <div id="ExportInbound" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('ExportBillInbound.aspx');">
                                    Export Bill InBound</div>
                            <div id="ExportBillOutbound" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('ExportBillOutBound.aspx');">
                                    Export Bill OutBound</div>
                              <div id="CustomRep" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmCustomRep.aspx');">
                                    Custom Report Generator</div>                                  
                           </span>
                    </a>
                    <a href="#" onmouseover="SpnUtilities.style.display='';" onmouseout="SpnUtilities.style.display='none';"
                           dontcollapse="true" onclick="" class="StartmenuButtonChildren">
                           <img dontcollapse="true" src="images/Utility.ico" border="0" class="StartmenuButtonImg" />
                           <span dontcollapse="true" class="StartmenuButtonTxt">Utilities </span>
                           <span id="SpnUtilities" style="display: none; top:155px" class="StartmenuSubMenuFrame">
                                <div id="AddUser" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('Users.aspx');">
                                    Create / Edit User</div>
                                <div id="ChPassword" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmPassword.aspx');">
                                    Change Password</div>
                                <div id="UserAccess" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';" onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';"
                                    onclick="windowManager.CreateWindow('FrmAccess.aspx');">
                                    User Profiles</div>
                                <div id="MnuSystem" runat="server" visible="False" class="IDCWMDesktopTaskbarContextMenuItem"
                                    style="border-bottom-style: solid; border-width: 1" onmouseover="this.className='IDCWMDesktopTaskbarContextMenuItemHover';"
                                    onmouseout="this.className='IDCWMDesktopTaskbarContextMenuItem';" onclick="windowManager.CreateWindow('SystemSetup.aspx');">
                                    System Set Up</div>
                           </span>
                     </a>
                     <a href="#" onclick="windowManager.Alert('Help Not available! Please Contact Vendor');"
                        class="StartmenuButton"> <img src="images/Help.ico" border="0" class="StartmenuButtonImg" />
                        <span class="StartmenuButtonTxt">Help</span>
                    </a>
                    <a href="Default.aspx" class="StartmenuButton">
                        <img src="images/sync32.png" border="0" class="StartmenuButtonImg" />
                        <span class="StartmenuButtonTxt">Log off</span>
                    </a>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
