﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MenuScreen.aspx.vb" Inherits="Wms.MenuScreen" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%;">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 38px;
        }
        p.MsoNormal
        {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 0pt;
            margin-left: 0in;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri" , "sans-serif";
        }
        .style8
        {
            height: 38px;
            font-size: large;
        }
        .style9
        {
            height: 38px;
            font-weight: bold;
            font-size: large;
        }
        .style10
        {
            height: 10px;
        }
        .style11
        {
            height: 23px;
        }
        .style12
        {
            text-align:"center"
        }
    </style>
</head>
<%--onload="javascript:window.history.forward(1);"--%>
<body style="height: 100%; margin-bottom:0;">
    <form id="form1" runat="server" style="height: 100%; margin-bottom:0px">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="Panel1" runat="server" BackColor="#0080C0" Height="38px" Width="100%"
        HorizontalAlign="Left">
        <table class="style1" cellpadding="0" cellspacing="0">
            <tr style="height:100%">
                <td valign="middle"  bgcolor="#0080C0" align="left">
                    &nbsp;
                </td>
                <td align="left" bgcolor="#0080C0"  valign="top">                    
                    <asp:Image ID="Image1" runat="server" Height="38px" 
                        ImageUrl="~/Images/Logo.gif" Style="text-align: left" Width="200px" />                    
                </td>
                <td align="left" bgcolor="#0080C0" valign="middle">
                    <asp:Label ID="Label1" runat="server" Text="Warehouse Management System " 
                        ForeColor="White" Font-Size="X-Large" Font-Bold="true"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:RoundedCornersExtender ID="Panel1_RoundedCornersExtender" runat="server" Enabled="True"
        Radius="10" TargetControlID="Panel1" Corners="Top">
    </asp:RoundedCornersExtender>
    <table style="height: 80%; width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" style="width:10%; border-style:inset; border-width:thin;">
                <asp:Label ID="Label2" runat="server" CssClass="style12"  Text="       Main Menu" BackColor="#99CCFF" 
                    BorderStyle="Outset" BorderWidth="1px" Font-Bold="True" Width="100%"></asp:Label>
                    <br /> 
            <asp:Menu ID="MnuAdmin" runat="server" BackColor="White"
                        DynamicHorizontalOffset="3" Font-Names="Tahoma" Font-Size="medium" ForeColor="Black"
                        Height="25px" Orientation="Vertical" StaticSubMenuIndent="10px" 
                        Target="MainArea" DataSourceID="xmlDataSource">
                        <DynamicMenuStyle BackColor="#0080C0" ForeColor="Black" BorderStyle="Outset"
                            BorderWidth="2" BorderColor="White" />
                        <DynamicMenuItemStyle BackColor="Beige" ForeColor="Black" BorderStyle="None"
                            BorderWidth="1" BorderColor="White" HorizontalPadding="12" VerticalPadding="3"
                            Font-Names="Tahoma" Font-Size="medium"/>
                        <DynamicSelectedStyle BackColor="LightBlue" BorderStyle="Solid" BorderColor="Black"
                            BorderWidth="1" />
                        <DynamicHoverStyle BackColor="LightSkyBlue" ForeColor="Black" BorderStyle="Outset"
                            BorderWidth="1" BorderColor="White" />
                        <DataBindings>
                            <asp:MenuItemBinding DataMember="MenuItem" NavigateUrlField="NavigateUrl" TextField="Text"
                                ToolTipField="ToolTip" />
                        </DataBindings>
                        <StaticMenuStyle BorderStyle="None" Width="100%" />
                        <StaticSelectedStyle BackColor="#FFCC66" />
                        <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="5px" BorderColor="Black"
                            BorderStyle="None" />
                        <StaticHoverStyle BackColor="#990000" ForeColor="White" />
                    </asp:Menu>
            </td>
            <td valign="top" style="height: 100%; width:90%">
                <iframe id="Iframe1" name="MainArea" height="100%" width="100%" runat="server"></iframe>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2" valign="bottom" style="height:10px">
                <p class="MsoNormal">
                    Copyright© 2010 Software Lab Mauritius All rights reserved.</p>
            </td>
        </tr>
    </table>    
    <div visible="false">
        <asp:XmlDataSource ID="xmlDataSource" TransformFile="MenuXSLT.xsl" XPath="MenuItems/MenuItem"
            runat="server" EnableCaching="False" />
    </div>
    </form>
</body>
</html>
