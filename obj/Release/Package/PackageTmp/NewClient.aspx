﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewClient.aspx.vb" Inherits="Wms.NewClient" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Create / Edit Client Details</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/Emp.png', 'images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Create New Client'; //Set the name
            w.Width = 875; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 525;
            w.CenterScreen(); // Center the window
            w.SetCloseable = true;
            w.Resizeable = false; // Disable resize
            w.Minimizeable = false; // Disable minimize
            w.Maximizeable = false; // Disable maximize
            w.ShowDialog(); //Show the window
        };
        function GetID() {
            if (w.WindowManager.GetWindowByName('Marketing Department Dashboard').ContentIframe.Document) {
                var Target = w.WindowManager.GetWindowByName('Marketing Department Dashboard').ContentIframe.Document.getElementById('HidClientID');
            }
            else {
                 var Target = w.WindowManager.GetWindowByName('Marketing Department Dashboard').ContentIframe.contentWindow.document.getElementById('HidClientID');
            }
            Target.value = document.getElementById('TxtClientID').value;
            if (document.all) {
                Target.fireEvent("onchange");
            }
            else {
                var clickEvent = window.document.createEvent('HTMLEvents');
                clickEvent.initEvent('change', false, true);
                Target.dispatchEvent(clickEvent);
            }
            w.Close();
        };
        function fDragging(obj, e, limit) {
            if (!e) e = window.event;
            obj = document.getElementById(obj);
            var x = parseInt(obj.style.left);
            var y = parseInt(obj.style.top);
            var x_ = e.clientX - x;
            var y_ = e.clientY - y;
            if (document.addEventListener) {
                document.addEventListener("mousemove", inFmove, true);
                document.addEventListener("mouseup", inFup, true);
            } else if (document.attachEvent) {
                document.attachEvent("onmousemove", inFmove);
                document.attachEvent("onmouseup", inFup);
            }
            inFmove(e);
            inFstop(e);
            inFabort(e);
            function inFmove(e) {
                var evt;
                if (!e) e = window.event;
                if (limit) {
                    var op = obj.parentNode;
                    var opX = parseInt(op.style.left);
                    var opY = parseInt(op.style.top);
                    if ((e.clientX - x_) < 0) return false;
                    else if ((e.clientX - x_ + obj.offsetWidth + opX) > (opX + op.offsetWidth)) return false;
                    if (e.clientY - y_ < 0) return false;
                    else if ((e.clientY - y_ + obj.offsetHeight + opY) > (opY + op.offsetHeight)) return false;
                }
                obj.style.left = e.clientX - x_ + "px";
                obj.style.top = e.clientY - y_ + "px";
                document.getElementById("HfLeft").value = e.clientX - x_ + "px";
                document.getElementById("HfTop").value = e.clientY - y_ + "px";
                inFstop(e);
            }
            function inFup(e) {
                var evt;
                if (!e) e = window.event;
                if (document.removeEventListener) {
                    document.removeEventListener("mousemove", inFmove, true);
                    document.removeEventListener("mouseup", inFup, true);
                } else if (document.detachEvent) {
                    document.detachEvent("onmousemove", inFmove);
                    document.detachEvent("onmouseup", inFup);
                }
                inFstop(e);
            }

            function inFstop(e) {
                if (e.stopPropagation) return e.stopPropagation();
                else return e.cancelBubble = true;
            }
            function inFabort(e) {
                if (e.preventDefault) return e.preventDefault();
                else return e.returnValue = false;
            }
        }         
    </script>
    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 472px;
        }
        .pnlBg
        {
            filter: alpha(opacity=95);
            opacity: 0.95;
        }
        .style2
        {
            height: 22px;
        }
    </style>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    <div align="left">
        <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </cc1:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px;
                    height: 32px; width: 32px;" runat="server" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/Images/Wait.gif" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="Content" style="width: 830px; height: 512px; overflow: auto; padding: 2px;">
                    <table class="style1" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" class="style1">
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td valign="top">
                                            &nbsp;
                                        </td>
                                        <td colspan="3" style="text-align: right">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            Type</td>
                                        <td valign="top">
                                            <asp:DropDownList ID="CmbType" runat="server" AutoPostBack="True" Width="100%">
                                            </asp:DropDownList>
                                        </td>
                                        <td colspan="3" style="text-align: right">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Client ID
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox ID="TxtClientID" runat="server" AutoPostBack="True"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="TxtClientID" ErrorMessage="Please Enter Client ID" 
                                                ValidationGroup="ClientVal">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td colspan="3" style="text-align: right">
                                            <asp:Button ID="ButNewClient" runat="server" CausesValidation="False" 
                                                Font-Bold="True" Font-Overline="False" Text="New Client" Width="100px" />
                                            <asp:Button ID="ButSaveClient" runat="server" CausesValidation="False" 
                                                Font-Bold="True" Text="Save Data" Width="100px" />
                                            <cc1:ConfirmButtonExtender ID="ButSaveClient_ConfirmButtonExtender" 
                                                runat="server" ConfirmText="Sure to Save Client Data?" Enabled="True" 
                                                TargetControlID="ButSaveClient">
                                            </cc1:ConfirmButtonExtender>
                                            <asp:Button ID="ButFindClient" runat="server" CausesValidation="False" 
                                                Font-Bold="True" Text="Find Client" Width="100px" />
                                            <asp:Button ID="ButNextClient" runat="server" CausesValidation="False" 
                                                Font-Bold="True" Text="Next" Width="100px" />
                                            <asp:Button ID="ButPrevClient" runat="server" CausesValidation="False" 
                                                Font-Bold="True" Text="Previous" Width="100px" />
                                            <asp:Button ID="ButDeleteClient" runat="server" CausesValidation="False" 
                                                Font-Bold="True" Text="Delete" Width="100px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Category
                                        </td>
                                        <td valign="top">
                                            <asp:DropDownList ID="CmbCategory" runat="server" Width="100%">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="CmbCategory"
                                                ErrorMessage="Select a Category" Operator="NotEqual" SetFocusOnError="True" ValidationGroup="ClientVal"
                                                ValueToCompare="Select ...">*</asp:CompareValidator>
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="LblMsg" runat="server" Text="Data Saved and Transferred to Dashboard."
                                                Font-Bold="True" ForeColor="#CC3300" Visible="False"></asp:Label>
                                            <asp:LinkButton ID="LinkButton1" Font-Bold="True" ForeColor="#CC3300" Visible="False"
                                                runat="server" OnClientClick="w.Close();">Close this Window</asp:LinkButton>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            Country</td>
                                        <td colspan="3">
                                            <asp:DropDownList ID="CmbCountry" runat="server" Height="22px" Width="294px">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Name
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtName" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                ControlToValidate="TxtName" ErrorMessage="Please Enter Client Name" 
                                                ValidationGroup="ClientVal">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Address
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtAddress" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TxtAddress"
                                                ErrorMessage="Please Enter Address" ValidationGroup="ClientVal">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Address 2
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtAddress2" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            City
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtCity" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TxtCity"
                                                ErrorMessage="Please Enter City" ValidationGroup="ClientVal">*</asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Telephone
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtTel" runat="server" Width="95%"></asp:TextBox>
                                        </td>
                                        <td>
                                            Fax
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtFaxNo" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            E-Mail
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtEmail" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Web URL
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtWeb" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td valign="middle">
                                            Contact
                                        </td>
                                                <td colspan="2" >
                                                <asp:TextBox ID="TxtContact" runat="server" Width="100%" Height="19px"></asp:TextBox>
                                                </td>
                                        <td align="right" valign="top">
                                            <table >
                                                <tr>
                                                    <td>
                                                        Cell No
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TxtCellNo" runat="server" Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TxtContact"
                                                ErrorMessage="Please Enter Contact Name" ValidationGroup="ClientVal">*</asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp; 
                                        </td>
                                        <td>
                                            Position
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtPosition" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Date
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtDate" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" Enabled="True"
                                                Format="dd/MM/yyyy" TargetControlID="TxtDate">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td>
                                            Status
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="CmbStatus" runat="server" Width="75%">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="CmbStatus"
                                                ErrorMessage="Select a Status" Operator="NotEqual" ValueToCompare="Select ...">*</asp:CompareValidator>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            VAT No
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtVatNo" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            FP Zone
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="ChkZone1" runat="server" Text="Zone 1" />
                                            <asp:CheckBox ID="ChkZone6" runat="server" Text="Zone 6" />
                                            <asp:CheckBox ID="ChkZone9" runat="server" Text="Zone 9" />
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Freeport Cert No.
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtLicenceNo" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Expiry
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtLicExp" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="TxtLicExp_CalendarExtender" runat="server" Enabled="True"
                                                Format="dd/MM/yyyy" TargetControlID="TxtLicExp">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style2">
                                            &nbsp;&nbsp;&nbsp; &nbsp;
                                        </td>
                                        <td class="style2">
                                            Storage Permit
                                        </td>
                                        <td class="style2">
                                            <asp:TextBox ID="TxtStoragePermit" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="style2">
                                            Expiry
                                        </td>
                                        <td class="style2">
                                            <asp:TextBox ID="TxtPermitExp" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="TxtPermitExp_CalendarExtender" runat="server" Enabled="True"
                                                Format="dd/MM/yyyy" TargetControlID="TxtPermitExp">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td class="style2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            BRN
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtBRN" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Total Area Leased
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtAreaLeased" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Product Type
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtProType" runat="server" Width="99%"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Description of Activities
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtActivities" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Remarks
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtRemarks" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="DragPanel" style="width: 700px; height: 410px; text-align: center; position: absolute;
                    display: none; left: 20px; top: 20px;" onmousedown="fDragging('DragPanel', event, true);"
                    runat="server">
                    <asp:HiddenField ID="HfLeft" runat="server" Value="20" />
                    <asp:HiddenField ID="HfTop" runat="server" Value="20" />
                    <asp:Panel ID="PnlFind" Style="z-index: 101; display: none;" runat="server" Height="410px"
                        Width="700px" Visible="True" BackColor="#B9DCFF" CssClass="pnlBg">
                        <table class="style1" cellpadding="0" cellspacing="0" style="height: 405px; width: 99%;"
                            align="center">
                            <tr>
                                <td bgcolor="#B9DCFF" valign="top" align="left">
                                    <asp:RadioButton ID="RbtName" runat="server" Text="Name" Checked="True" GroupName="FinOption" />
                                    <asp:CheckBox ID="ChkPotential" runat="server" Text="Potential Client Only" />
                                    <br />
                                    <asp:RadioButton ID="RbtCity" runat="server" Text="City" GroupName="FinOption" />
                                    <br />
                                    <asp:RadioButton ID="RbtContact" runat="server" Text="Contact" GroupName="FinOption" />
                                </td>
                                <td bgcolor="#B9DCFF">
                                    <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="Black" Style="text-align: center;"
                                        Text="Enter the text here to search"></asp:Label>
                                    <asp:TextBox ID="TxtSearch" runat="server" Width="100%" BackColor="White"></asp:TextBox>
                                </td>
                                <td align="right" bgcolor="#B9DCFF">
                                    <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                        Width="65px" />
                                    <br />
                                    <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                        Width="65px" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="White" align="center">
                                    <asp:Panel ID="Panel8" runat="server" Width="690px" Height="344px" ScrollBars="Vertical"
                                        BorderWidth="1px" BorderStyle="Inset">
                                        <asp:GridView ID="GdvFind" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                            ShowFooter="true">
                                            <HeaderStyle BackColor="#0080C0" />
                                            <FooterStyle BackColor="#0080C0" />
                                            <AlternatingRowStyle BackColor="#CAE4FF" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <cc1:RoundedCornersExtender ID="PnlFind_RoundedCornersExtender" runat="server" Enabled="True"
                        TargetControlID="PnlFind">
                    </cc1:RoundedCornersExtender>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
    <script type="text/javascript">
        // Get the instance of PageRequestManager.
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        // Add initializeRequest and endRequest
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        // Called when async postback begins
        function prm_InitializeRequest(sender, args) {
            // get the divImage and set it to visible
            var panelProg = $get('divImage');
            panelProg.style.display = '';
            // reset label text
        }

        // Called when async postback ends
        function prm_EndRequest(sender, args) {
            // get the divImage and hide it again
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        }
    </script>
</body>
</html>
