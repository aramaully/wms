﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditMonthlyInv.aspx.vb" Inherits="Wms.EditMonthtlyInv" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Edit Invoice</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Edit Invoice'; //Set the name
            w.Top = 10;
            w.Left = 10;
            w.Width = 1096; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 559;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '1';
                w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Delete") {
                Index = '3';
                w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 236px;
        }
        </style>

    </head>
<body onload="init();">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 400px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <div id="Content" style="width: 100%; height: 100%; padding: 2px; border-right: solid 1px #cddaea;">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Edit Billing Invoice"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Changes to Invoice" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save changes to Invoice?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find Invoice" Width="55px" CausesValidation="False" 
                                    UseSubmitBehavior="False" />
                                <asp:Button ID="ButNext" runat="server" Height="39px" Text="Next" Style="cursor: pointer"
                                    ToolTip="Go to Next Invoice" Width="55px" CausesValidation="False" 
                                    UseSubmitBehavior="False" />
                                <asp:Button ID="ButPrevious" runat="server" Height="39px" Text="Prev" Style="cursor: pointer"
                                    ToolTip="Go To Previous Invoice" Width="55px" CausesValidation="False" />
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Invoice" Width="55px" CausesValidation="False" 
                                    UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete Charges?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="Panel2" runat="server" BorderStyle="None">
                        <table class="style1" cellpadding="0">
                            <tr>
                                <td>
                                    FP Zone</td>
                                <td>
                                   <asp:DropDownList ID="CmbZone" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Invoice No</td>
                                <td>
                                    <asp:TextBox ID="TxtInvNo" runat="server" AutoPostBack="True"></asp:TextBox>
                                </td>
                                <td>
                                    
                                    &nbsp;</td>
                                <td>
                                    
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    Date</td>
                                <td>
                                    <asp:TextBox ID="TxtDate" runat="server"></asp:TextBox>
                                    <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" 
                                        Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtDate">
                                    </cc1:CalendarExtender>
                                </td>
                                <td>
                                    Month</td>
                                <td>
                                   <asp:TextBox ID="TxtMonth" runat="server" ReadOnly="True"></asp:TextBox>
                                </td>
                                <td>
                                    Year</td>
                                <td>
                                   <asp:TextBox ID="TxtYear" runat="server" ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Client</td>
                                <td colspan="3">
                                    <asp:TextBox ID="TxtClient" runat="server" Width="100%" ReadOnly="True"></asp:TextBox>
                                </td>
                                <td>
                                    
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <table bgcolor="#99CCFF" cellpadding="0" width="100%">
                        <tr>
                            <td>
                                Description</td>
                            <td colspan="3">
                                <asp:TextBox ID="TxtDescription" runat="server" Width="100%"></asp:TextBox>
                            </td>
                            <td align="right" colspan="3">
                                <asp:Button ID="ButUpdate" runat="server" Text="Update Selected Item" 
                                    BackColor="#FFD553" Width="150px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Quantity</td>
                            <td>
                                <asp:TextBox ID="TxtQuantity" runat="server" AutoPostBack="True" Width="75px"></asp:TextBox>
                            </td>
                            <td class="style25">
                                <asp:TextBox ID="TxtUnit" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Rate</td>
                            <td>
                                <asp:TextBox ID="TxtRate" runat="server" AutoPostBack="True" Width="75px"></asp:TextBox>
                            </td>
                            <td align="right" class="style26">
                                Amount</td>
                            <td align="right">
                                <asp:TextBox ID="TxtValue" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="Panel3" runat="server" Height="110px"  BorderStyle="Solid" BorderWidth="1" ScrollBars="Vertical" >
                    <asp:GridView ID="GdvInvoice" runat="server" Width="100%" AlternatingRowStyle-BackColor="#99CCFF"
                            AutoGenerateColumns="False" SelectedRowStyle-BackColor="#FFD553">
                            <SelectedRowStyle BackColor="#FFD553" />
                            <HeaderStyle BackColor="#3366FF"  ForeColor="White"/>
                        <Columns>
                            <asp:BoundField DataField="Description" HeaderText="Description" 
                                HeaderStyle-HorizontalAlign="Left" >
                            <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Quantity" HeaderText="Quantity"  
                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" >
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Unit" HeaderText="Unit" 
                                HeaderStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Rate" HeaderText="Rate"  
                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" 
                                DataFormatString="{0:N2}" >
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Amount" HeaderText="Amount"  ItemStyle-HorizontalAlign="Right" 
                                HeaderStyle-HorizontalAlign="Right" DataFormatString="{0:N2}" >
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Type" HeaderText="Type" />
                            <asp:BoundField DataField="Description" HeaderText="Original Desc" />
                        </Columns>
                            <AlternatingRowStyle BackColor="#99CCFF" />
                    </asp:GridView>                    
                    </asp:Panel>
                    <asp:Panel ID="Panel4" runat="server" Height="165px">
                        <table class="style1" cellpadding="0">
                            <tr>
                                <td class="style23" rowspan="5" valign="top"> Remarks
                                    <asp:TextBox ID="TxtRemarks" runat="server" Height="98px" TextMode="MultiLine" 
                                        Width="90%"></asp:TextBox>
                                </td>
                                <td class="style21">
                                    Total</td>
                                <td align="Right" class="style20">
                                    <asp:TextBox ID="TxtTotal" runat="server" ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style22">
                                    Discount</td>
                                <td align="Right">
                                    <asp:TextBox ID="TxtDiscount" runat="server" AutoPostBack="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Net</td>
                                    <td>
                                        <asp:TextBox ID="TxtNet" runat="server" ReadOnly="True"></asp:TextBox>
                                    </td>
                            </tr>
                            <tr>
                                <td class="style22">
                                    Vat</td>
                                <td align="Right">
                                    <asp:TextBox ID="TxtVat" runat="server" ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style22">
                                    Prepaid Amt</td>
                                <td align="Right">
                                    <asp:TextBox ID="TxtPrepaid" runat="server" AutoPostBack="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style24" valign="middle">
                                    <asp:Label ID="LblVatRate" runat="server" Text="Vat Applied"></asp:Label>
                                    <asp:TextBox ID="TxtVatRate" runat="server" Width="50px" ReadOnly="True"></asp:TextBox>
                                    %</td>
                                <td class="style22">
                                    Amount</td>
                                <td align="Right">
                                    <asp:TextBox ID="TxtAmount" runat="server" ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                 <asp:Panel ID="PnlFind" Style="z-index: 101; display:none; left: 100px; position: absolute; top: 60px"
                        runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                        Visible="True" BackColor="White">
                     <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                         <tr>
                             <td bgcolor="#004080">
                                 <asp:Label ID="LblSearch" runat="server" Font-Bold="True" ForeColor="White"
                                     Text="Client Name"></asp:Label>
                                 <asp:TextBox ID="TxtSearch" runat="server" Width="400px" BackColor="White"></asp:TextBox>
                             </td>
                             <td align="right" bgcolor="#004080" >
                                 <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                     Width="65px" />                                 
                             </td>
                         </tr>
                         
                         <tr>
                             <td bgcolor="#004080">
                                <asp:CheckBox ID="ChkDate" runat="server" Text="Date From" Font-Bold="True" 
                                 AutoPostBack="true" ForeColor="White" />                                 
                                <asp:TextBox ID="TxtFrom" runat="server" Enabled="false"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                        Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtFrom">
                                    </cc1:CalendarExtender>
                                 <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White"
                                     Text=" To "></asp:Label>
                                 <asp:TextBox ID="TxtTo" runat="server" Enabled="false"  BackColor="White"></asp:TextBox>
                                 <cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                                        Enabled="True" Format="dd-MMM-yyyy" TargetControlID="TxtTo">
                                    </cc1:CalendarExtender>
                             </td>
                             <td align="right" bgcolor="#004080" >
                                 <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                     Width="65px" />
                             </td>
                         </tr>   
                  
                         <tr>
                             <td colspan="2">
                                 <asp:Panel ID="Panel8" runat="server" Width="100%" Height="300px" ScrollBars="Vertical">
                                     <asp:GridView ID="GdvFind" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                         ShowFooter="true">
                                         <HeaderStyle BackColor="#0080C0" />
                                         <FooterStyle BackColor="#0080C0" />
                                         <AlternatingRowStyle BackColor="#CAE4FF" />
                                     </asp:GridView>
                                 </asp:Panel>
                             </td>
                         </tr>
                     </table>
                    </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
