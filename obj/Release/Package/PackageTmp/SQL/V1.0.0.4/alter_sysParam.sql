﻿--Version 1.0.0.4
select * from SysParam

ALTER TABLE SysParam
ADD SysID INTEGER DEFAULT(0),
CompanyName VARCHAR(255) NULL,
BeneficiaryBank VARCHAR(255) NULL,
BankAddress1 VARCHAR(255) NULL,
BankAddress2 VARCHAR(255) NULL,
BankAddress3 VARCHAR(255) NULL,
BankAccNo VARCHAR(255) NULL,
IBan VARCHAR(255) NULL,
SwiftCode VARCHAR(255) NULL


UPDATE SysParam SET SysID = 1, 
CompanyName = 'BPML FREEPORT SERVICES LTD',
BeneficiaryBank = 'SBM BANK (Mauritius) Ltd',
BankAddress1 = 'SBM Tower, 1 Queen Elizabeth II Avenue',
BankAddress2 = 'Port Louis',
BankAddress3 = 'Mauritius',
BankAccNo = '610 301 000 26946',
IBan = 'MU95 STCB 1170 0301 000 26946 000MUR',
SwiftCode = 'STCBMUMU'

select * from SysParam