﻿--Version 1.0.0.4
UPDATE [Pallets] SET GateInRate=200, GateInFees=200 WHERE DateIn >= '2020-03-01 00:00:00' AND GateInRate IS NULL
UPDATE [PickList] SET GateOutRate=200, GateOutFees= Quantity*200 WHERE PickDate >= '2020-03-01 00:00:00'  AND GateOutRate is NULL