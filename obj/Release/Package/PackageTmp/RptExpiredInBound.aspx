﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RptExpiredInBound.aspx.vb"
    Inherits="Wms.RptExpiredInBound" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Expired InBound</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Expired InBound'; //Set the name
            w.Top = 10;
            w.Left = 10;
            w.Width = 397; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 236;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindClient.aspx", "FindClient", "width=565,height=435,top=200, left=575");
        };
        function ShowReport() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FrmRpt.aspx', w);
            return false;
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '1';
                w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Delete") {
                Index = '3';
                w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 236px;
        }
        .style6
        {
            height: 22px;
        }
        </style>

    </head>
<body onload="init();">
    <form id="form1" runat="server" style="height: 258px">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 249px; position: absolute; top: 130px; height: 70px;"
                    runat="server" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" Width="100%" Height="232px" BorderStyle="Groove"
                    BorderWidth="1px">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="List Of Expired Inbounds"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="BtnFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find" Width="55px" UseSubmitBehavior="False" />
                                <asp:Button ID="BtnView" runat="server" Height="39px" Style="cursor: pointer" Text="View"
                                    ToolTip="View Report" UseSubmitBehavior="False" Width="55px" />
                                <asp:Button ID="ButExit" runat="server" Height="39px" Text="Close" Style="cursor: pointer"
                                    ToolTip="Close this Module" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                            </td>
                        </tr>
                    </table>
                    <table align="Left" border="0" cellpadding="0" cellspacing="0" style="height: 85px;
                        width: 100%">
                        <tr>
                            <td align="left" bgcolor="#FFFFFF" valign="middle">
                                <asp:Label ID="Label2" runat="server" Text="As On Date"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#FFFFFF" valign="middle">
                                <asp:TextBox ID="TxtDate" runat="server" Width="112px"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtDate_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="TxtDate">
                                </cc1:CalendarExtender>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="TxtDate"
                                    ErrorMessage="Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" bgcolor="#FFFFFF" valign="middle" class="style6">
                                Client
                            </td>
                            <td align="left" bgcolor="#FFFFFF" valign="middle" class="style6">
                                <asp:TextBox ID="TxtClientID" runat="server" Width="112px" AutoPostBack="True">ALL</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" bgcolor="#FFFFFF" valign="middle">
                                Name
                            </td>
                            <td align="left" bgcolor="#FFFFFF" valign="middle">
                                <asp:TextBox ID="TxtName" runat="server" Width="245px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" bgcolor="#FFFFFF" valign="middle">
                                <asp:Label ID="Label3" runat="server" Text="No of Months" Width="100px"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#FFFFFF" valign="middle">
                                <asp:TextBox ID="TxtMonth" runat="server" Width="112px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
