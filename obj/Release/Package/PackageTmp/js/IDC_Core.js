var IDC_Core = function(Document, parentObject) {
    this.Document = Document || document;
    this.ParentObject = parentObject || document.body;
    this.isIE = document.all ? true : false;
    this.IsIE = this.isIE;
    this.Mouse = new IDC_Mouse(this.Document, this.ParentObject);
    this.Keyboard = new IDC_Keyboard(this.Document, this.ParentObject);
    this.DOM = new IDC_DOM(this.Document);
    this.Localization = new IDC_Localization();
    this.WebsiteUrl = document.location.href.toString();
    this.WebsiteUrl = this.WebsiteUrl.substring(0, this.WebsiteUrl.lastIndexOf('/') + 1);
};
var IDC_Mouse = function(document, parentObject) {
    this.isIE = document.all ? true : false;
    this.Document = document; this.ParentObject = parentObject;
    this.X = 0;
    this.Y = 0;
    this.CaptureMouse(parentObject);
};
IDC_Mouse.prototype.CaptureMouse = function(parentObject) {
    if (!this.isIE) document.captureEvents(Event.MOUSEMOVE);
    else {
        parentObject.onselectstart = function() { return event.srcElement.tagName == 'INPUT' || event.srcElement.tagName == 'TEXTAREA'; };
        parentObject.ondragstart = function() { return false; };
    } parentObject.IDCMouse = this;
    parentObject.ommouseover = function(e) {
        if (this.IDCMouse.isIE) {
            this.IDCMouse.X = event.clientX;
            this.IDCMouse.Y = event.clientY;
        } else {
            if (e) {
                this.IDCMouse.X = e.pageX;
                this.IDCMouse.Y = e.pageY;
            } 
        } 
    };
    parentObject.onmousemove = function(e) {
        if (this.IDCMouse.isIE) {
            this.IDCMouse.X = event.clientX;
            this.IDCMouse.Y = event.clientY;
        } else {
            if (e) {
                this.IDCMouse.X = e.pageX; this.IDCMouse.Y = e.pageY;
            } 
        } 
    };
    parentObject.onmousedown = function(e) {
        if (!e) e = window.event; if (e) {
            var tg = e.srcElement || e.target;
            return tg.tagName == 'INPUT' || tg.tagName == 'TEXTAREA' || tg.tagName == 'SELECT';
        } else return false;
    };
};
IDC_Mouse.prototype.RefreshMousePosition = function(e) {
    if (this.isIE) {
        this.X = event.clientX;
        this.Y = event.clientY;
    } else { if (e) { this.X = e.pageX; this.Y = e.pageY; } } 
};
var IDC_Keyboard = function(Document, parentObject) {
    this.Document = Document || document;
    this.ParentObject = parentObject || this.Document.body;
    if (this.Document.IDC_Keyboard) return; this.isIE = this.Document.all ? true : false;
    this.KeyControlPressed = false; this.KeyAltPressed = false;
    this.KeyShiftPressed = false;
    this.KeyUpFunctions = [];
    this.KeyDownFunctions = [];
    this.KeyPressFunctions = [];
    this.PrimaryKeyUpFunction = null;
    this.PrimaryKeyDownFunction = null;
    this.LastKeyCode = null;
    if (!this.isIE) {
        this.Document.captureEvents(Event.KEYDOWN);
        this.Document.captureEvents(Event.KEYUP); this.CaptureKeyboard(this.Document);
    } else this.CaptureKeyboard(this.Document);
};
IDC_Keyboard.prototype.BindKeyUp = function(KeyIndex, Callback) {
    this.KeyUpFunctions[KeyIndex] = Callback;
};
IDC_Keyboard.prototype.BindKeyDown = function(KeyIndex, Callback) { this.KeyDownFunctions[KeyIndex] = Callback; };
IDC_Keyboard.prototype.BindKeyPress = function(KeyIndex, Callback) { this.KeyPressFunctions[KeyIndex] = Callback; };
IDC_Keyboard.prototype.BindKeyUpToObject = function(Object, KeyIndex, Callback) {
    if (!Object.KeyUpCallBackEvents) Object.KeyUpCallBackEvents = [];
    Object.KeyUpCallBackEvents[KeyIndex] = Callback;
    if (this.isIE) {
        Object.onkeyup = function() { if (this.KeyUpCallBackEvents[event.keyCode]) return this.KeyUpCallBackEvents[event.keyCode](this); };
    } else { Object.onkeyup = function(e) { if (this.KeyUpCallBackEvents[e.which]) return this.KeyUpCallBackEvents[e.which](this); }; } 
};
IDC_Keyboard.prototype.BindKeyDownToObject = function(Object, KeyIndex, Callback) {
    if (!Object.KeyDownCallBackEvents) Object.KeyDownCallBackEvents = [];
    Object.KeyDownCallBackEvents[KeyIndex] = Callback;
    if (this.isIE) {
        Object.onkeydown = function() { if (this.KeyDownCallBackEvents[event.keyCode]) return this.KeyDownCallBackEvents[event.keyCode](this); };
    } else { Object.onkeydown = function(e) { if (this.KeyDownCallBackEvents[e.which]) return this.KeyDownCallBackEvents[e.which](this); }; } 
};
IDC_Keyboard.prototype.BindKeyPressToObject = function(Object, KeyIndex, Callback) {
    if (!Object.KeyPressCallBackEvents) Object.KeyPressCallBackEvents = [];
    Object.KeyPressCallBackEvents[KeyIndex] = Callback;
    if (this.isIE) {
        Object.onkeypress = function() { if (this.KeyPressCallBackEvents[event.keyCode]) return this.KeyPressCallBackEvents[event.keyCode](this); };
    } else { Object.onkeypress = function(e) { if (this.KeyPressCallBackEvents[e.which]) return this.KeyPressCallBackEvents[e.which](this); }; } 
};
IDC_Keyboard.prototype.CaptureKeyboard = function(parentObject) {
    if (this.isIE) {
        parentObject.IDC_Keyboard = this;
        parentObject.onkeydown = function() { return this.IDC_Keyboard.KeyDownEvent(event); };
        parentObject.onkeyup = function() { return this.IDC_Keyboard.KeyUpEvent(event); };
    } else {
        this.Document.IDC_Keyboard = this;
        this.Document.onkeydown = function(e) { return this.IDC_Keyboard.KeyDownEvent(e); };
        this.Document.onkeyup = function(e) { return this.IDC_Keyboard.KeyUpEvent(e); };
    } 
};
IDC_Keyboard.prototype.KeyDownEvent = function(e) {
    if (e) {
        var keynum = null;
        if (this.isIE) keynum = e.keyCode; else if (e.which) keynum = e.which;
        if (!this.KeyAltPressed) this.KeyAltPressed = keynum == 18;
        if (!this.KeyControlPressed) this.KeyControlPressed = keynum == 17;
        if (!this.KeyShiftPressed) this.KeyShiftPressed = keynum == 16;
        this.LastKeyCode = keynum;
        if (this.PrimaryKeyDownFunction) this.PrimaryKeyDownFunction(keynum);
        if (this.KeyDownFunctions[keynum]) this.KeyDownFunctions[keynum]();
    } else {
        if (this.PrimaryKeyDownFunction) this.PrimaryKeyDownFunction();
    } return null;
};
IDC_Keyboard.prototype.KeyUpEvent = function(e) {
    if (e) {
        var keynum = null;
        if (this.isIE) keynum = e.keyCode; else if (e.which) keynum = e.which;
        if (this.KeyAltPressed && keynum == 18) this.KeyAltPressed = false;
        if (this.KeyControlPressed && keynum == 17) this.KeyControlPressed = false;
        if (this.KeyShiftPressed && keynum == 16) this.KeyShiftPressed = false; this.LastKeyCode = keynum;
        if (this.PrimaryKeyUpFunction) this.PrimaryKeyUpFunction(keynum);
        if (this.KeyUpFunctions[keynum]) this.KeyUpFunctions[keynum]();
    } else {
        if (this.PrimaryKeyUpFunction) this.PrimaryKeyUpFunction();
    } return null;
};
IDC_Core.prototype.Ajax = function(url) {
    this.xmlhttp = null;
    this.Dispose = function() {
        this.OnLoading = null;
        this.OnLoaded = null; this.OnInteractive = null;
        this.OnCompletion = null; this.OnError = null;
        this.OnFail = null;
        this.method = null;
        this.queryStringSeparator = null;
        this.argumentSeparator = null;
        this.URLString = null;
        this.encodeURIString = null;
        this.execute = null;
        this.element = null;
        this.elementObj = null;
        this.requestFile = null;
        this.vars.length = 0;
        this.vars = null;
        this.ResponseStatus = null;
        this.xmlhttp = null;
        this.Dispose = null;
    };
    this.ResetData = function() {
        this.method = "POST";
        this.queryStringSeparator = "?";
        this.argumentSeparator = "&";
        this.URLString = "";
        this.encodeURIString = true;
        this.execute = false;
        this.element = null;
        this.elementObj = null;
        this.requestFile = url;
        this.vars = {};
        this.ResponseStatus = new Array(2);
    };
    this.ResetFunctions = function() {
        this.OnLoading = function() { };
        this.OnLoaded = function() { };
        this.OnInteractive = function() { };
        this.OnCompletion = function() { };
        this.OnError = function() { };
        this.OnFail = function() { };
    };
    this.Reset = function() {
        this.ResetFunctions();
        this.ResetData();
    };
    this.CreateAJAX = function() {
        try {
            this.xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e1) {
            try {
                this.xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e2) {
                this.xmlhttp = null;
            } 
        } if (!this.xmlhttp) {
            if (typeof XMLHttpRequest != "undefined") {
                this.xmlhttp = new XMLHttpRequest();
            } else { this.failed = true; } 
        } 
    };
    this.setVar = function(name, value) { this.vars[name] = Array(value, false); };
    this.encVar = function(name, value, returnvars) {
        if (true == returnvars) {
            return Array(encodeURIComponent(name), encodeURIComponent(value));
        } else { this.vars[encodeURIComponent(name)] = Array(encodeURIComponent(value), true); } 
    };
    this.processURLString = function(string, encode) {
        encoded = encodeURIComponent(this.argumentSeparator);
        regexp = new RegExp(this.argumentSeparator + "|" + encoded);
        varArray = string.split(regexp);
        for (i = 0; i < varArray.length; i++) {
            urlVars = varArray[i].split("=");
            if (true == encode) {
                this.encVar(urlVars[0], urlVars[1]);
            } else { this.setVar(urlVars[0], urlVars[1]); } 
        } 
    };
    this.createURLString = function(urlstring) {
        if (this.encodeURIString && this.URLString.length) {
            this.processURLString(this.URLString, true);
        } if (urlstring) {
            if (this.URLString.length) {
                this.URLString += this.argumentSeparator + urlstring;
            } else { this.URLString = urlstring; } 
        } this.setVar("rndval", new Date().getTime());
        urlstringtemp = [];
        for (key in this.vars) {
            if (false == this.vars[key][1] && true == this.encodeURIString) {
                encoded = this.encVar(key, this.vars[key][0], true);
                delete this.vars[key];
                this.vars[encoded[0]] = Array(encoded[1], true);
                key = encoded[0];
            } urlstringtemp[urlstringtemp.length] = key + "=" + this.vars[key][0];
        } if (urlstring) {
            this.URLString += this.argumentSeparator + urlstringtemp.join(this.argumentSeparator);
        } else { this.URLString += urlstringtemp.join(this.argumentSeparator); } 
    };
    this.runResponse = function() { eval(this.Response); };
    this.RunAJAX = function(urlstring) {
        if (this.failed) {
            this.OnFail();
        } else {
            this.createURLString(urlstring);
            if (this.element) {
                this.elementObj = document.getElementById(this.element);
            } if (this.xmlhttp) {
                try {
                    var self = this;
                    if (this.method == "GET") {
                        totalurlstring = this.requestFile + this.queryStringSeparator + this.URLString;
                        this.xmlhttp.open(this.method, totalurlstring, true);
                    } else {
                        this.xmlhttp.open(this.method, this.requestFile, true);
                        try {
                            this.xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        } catch (e) { } 
                    } this.xmlhttp.onreadystatechange = function() {
                        switch (self.xmlhttp.readyState) {
                            case 1: self.OnLoading();
                                break;
                            case 2: self.OnLoaded();
                                break;
                            case 3: var tempText = '';
                                try {
                                    tempText = self.xmlhttp.responseText;
                                } catch (exp) {
                                    tempText = '';
                                } self.Response = tempText;
                                self.OnInteractive();
                                break;
                            case 4: self.Response = self.xmlhttp.responseText;
                                self.ResponseXML = self.xmlhttp.responseXML; self.ResponseStatus[0] = self.xmlhttp.status;
                                self.ResponseStatus[1] = self.xmlhttp.statusText;
                                if (self.execute) {
                                    if (self.runResponse) self.runResponse();
                                } if (self.elementObj) {
                                    elemNodeName = self.elementObj.nodeName; elemNodeName.toLowerCase();
                                    if (elemNodeName == "input" || elemNodeName == "select" || elemNodeName == "option" || elemNodeName == "textarea") {
                                        self.elementObj.value = self.Response;
                                    } else {
                                        self.elementObj.innerHTML = self.Response;
                                    } 
                                } try {
                                    if (self.ResponseStatus[0] == "200" || self.ResponseStatus[0] == "0") {
                                        if (self.OnCompletion) self.OnCompletion();
                                    } else {
                                        if (self.OnError) self.OnError();
                                    } 
                                } catch (exp) { } self.URLString = "";
                                break;
                        } 
                    };
                    this.xmlhttp.send(this.URLString);
                } catch (exp) { } 
            } 
        } 
    };
    this.Reset(); this.CreateAJAX();
};
var IDC_DOM = function(document) { };
IDC_DOM.prototype.Purge = function(obj) {
    try {
        var a = obj.attributes, i, l, n; if (a) {
            l = a.length;
            for (i = 0; i < l; i += 1) {
                n = a[i].name; if (typeof obj[n] === 'function') {
                    delete obj[n]; obj[n] = null;
                } 
            } 
        } a = obj.childNodes;
        if (a) {
            l = a.length; for (i = 0; i < l; i += 1) {
            this.Purge(obj.childNodes[i]);
        } 
    } 
} catch (ex) { } 
};
IDC_DOM.prototype.GetObjectPosition = function(obj) {
    var x = 0; var y = 0;
    if (obj.offsetParent) {
        do {
            x += obj.offsetLeft; y += obj.offsetTop;
        } while (obj = obj.offsetParent);
    } var retval = new Array(); retval[0] = x; retval[1] = y;
    return retval;
};
IDC_DOM.prototype.DisableFields = function(obj) {
    this._DisableFields(obj, 'INPUT');
    this._DisableFields(obj, 'SELECT'); this._DisableFields(obj, 'TEXTAREA');
};
IDC_DOM.prototype._DisableFields = function(obj, type) {
    var arr = obj.getElementsByTagName(type);
    for (var i = 0; i < arr.length; i++) {
        arr[i].setAttribute('OrgState', arr[i].disabled ? '1' : '0'); arr[i].disabled = true;
    } 
};
IDC_DOM.prototype.EnableFields = function(obj) {
    this._EnableFields(obj, 'INPUT');
    this._EnableFields(obj, 'SELECT'); this._EnableFields(obj, 'TEXTAREA');
};
IDC_DOM.prototype._EnableFields = function(obj, type) {
    var arr = obj.getElementsByTagName(type);
    for (var i = 0; i < arr.length; i++) {
        arr[i].disabled = arr[i].getAttribute('OrgState') == '1';
    } 
};
var IDC_Localization = function() { };
var IDC_LocalizationSettings = function(decimalPoint, thousandSep, fracDigits) {
    this.decimalPoint = new String(decimalPoint);
    this.thousandSep = new String(thousandSep);
    this.fracDigits = fracDigits;
};
IDC_Localization.prototype.RoundFloat = function(num, fracDigits) {
    var factor = Math.pow(10, fracDigits);
    return (Math.round(num * factor) / factor);
};
IDC_Localization.prototype.ToLcString = function(num, lc) {
    var str = new String(num);
    var aParts = str.split("."); return (aParts.join(lc.decimalPoint));
};
IDC_Localization.prototype.FormatNum = function(num, lc) {
    var sNum = new String(this.RoundFloat(num, lc.fracDigits));
    if (lc.fracDigits > 0) {
        if (sNum.indexOf(".") < 0) sNum = sNum + ".";
        while (sNum.length < 1 + sNum.indexOf(".") + lc.fracDigits) sNum = sNum + "0";
    } return (this.ToLcString(sNum, lc));
};
IDC_Localization.prototype.ParseLcNum = function(str, lc) {
    var sNum = new String(str);
    var aParts = sNum.split(lc.thousandSep);
    sNum = aParts.join(""); 
aParts = sNum.split(lc.decimalPoint); return(parseFloat(aParts.join("."))); }; 