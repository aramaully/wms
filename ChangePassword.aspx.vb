﻿Public Partial Class ChangePassword
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then

        End If
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Dim StrOldPass As String
        StrSql = "SELECT Password FROM  [User] WHERE UserID='" & Session("UserID").ToString & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        StrOldPass = Cmd.ExecuteScalar
        If TxtCurPass.Text <> StrOldPass Then
            ErrHead.Text = "Change Failed"
            ErrMsg.Text = "Old password is not correct"
            ModalPopupExtender1.Show()
            Exit Sub
        End If
        If TxtConfirm.Text <> TxtPassword.Text Then
            ErrHead.Text = "Change Failed"
            ErrMsg.Text = "Password Mismatch"
            ModalPopupExtender1.Show()
            Exit Sub
        End If
        StrSql = "UPDATE [User] SET Password='" & TxtPassword.Text & "' WHERE UserID='" & Session("UserID").ToString & "' AND Password='" & TxtCurPass.Text & "'"
        Cmd.CommandText = StrSql
        Cmd.ExecuteNonQuery()
        Con.Close()
        ErrHead.Text = "Change Success"
        ErrMsg.Text = "Password Changed Successfully"
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub
End Class