﻿Public Partial Class HsCode
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNew.Click
        TxtHsCode.Text = ""
        TxtDescription.Text = ""
        TxtNomen.Text = ""
        TxtHsCode.Focus()
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
        StrSql = "SELECT TOP 1 HsCode FROM ProdCategory WHERE HsCode > '" & TxtHsCode.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtHsCode.Text = Cmd.ExecuteScalar
        TxthsCode_TextChanged(Me, e)
    End Sub

    Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevious.Click
        StrSql = "SELECT TOP 1 HsCode FROM ProdCategory WHERE HsCode < '" & TxtHsCode.Text & "' ORDER BY HsCode DESC"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtHsCode.Text = Cmd.ExecuteScalar
        TxthsCode_TextChanged(Me, e)
    End Sub

    Private Sub TxtHsCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtHsCode.TextChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT * FROM ProdCategory WHERE HsCode = '" & TxtHsCode.Text & "'"
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtDescription.Text = Rdr("Category").ToString
            TxtNomen.Text = Rdr("HSNc").ToString
        Else
            TxtDescription.Text = ""
            TxtNomen.Text = ""
        End If
        TxtDescription.Focus()
        Rdr.Close()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        LblSearch.Text = "Enter Description to Search HS Code"
        PnlFind.Visible = True
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Visible = False
    End Sub

    Protected Sub ButRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT HsCode,Category,Hsnc FROM ProdCategory WHERE "
        StrSql = StrSql & "Category LIKE '%" + TxtSearch.Text + "%' ORDER BY HsCode"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtHsCode.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtHsCode_TextChanged(Me, e)
        PnlFind.Visible = False
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Cmd.Connection = Con
        Cmd.CommandText = "DELETE FROM ProdCategory WHERE HsCode = '" & TxtHsCode.Text & "'"
        Try
            Cmd.ExecuteNonQuery()
            ButNew_Click(Me, e)
        Catch ex As Exception
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            Cmd.CommandText = "DELETE FROM ProdCategory WHERE HsCode = '" & TxtHsCode.Text & "'"
            Cmd.ExecuteNonQuery()
            StrSql = "INSERT INTO ProdCategory (HsCode, Category, HsNc) VALUES('" & TxtHsCode.Text & "','"
            StrSql = StrSql & TxtDescription.Text & "','"
            StrSql = StrSql & TxtNomen.Text & "')"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ButNew_Click(Me, e)
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class