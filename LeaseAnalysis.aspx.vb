﻿Public Partial Class LeaseAnalysis
    Inherits System.Web.UI.Page
    Dim StrSql As String
    Dim Cmd As New OleDb.OleDbCommand
    Dim Con As New OleDb.OleDbConnection
    Dim Rdr As OleDb.OleDbDataReader

    Const MENUID = "LeaseAnalysis"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            CmbType.Items.Add("ALL")
            CmbType.Items.Add("Active")
            CmbType.Items.Add("Expired")
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Dim StrSelFormula As String
            Session("ReportFile") = "RptLeaseAnalysis.rpt"
            If CmbType.Text = "Active" Then
                StrSelFormula = "{LeaseAgreement.MovedIn}=True And {LeaseAgreement.MovedOut}=False "
            Else
                StrSelFormula = "{LeaseAgreement.MovedOut}=True "
            End If
            If CmbZone.Text <> "Select..." Then
                StrSelFormula = StrSelFormula & "And {LeaseAgreement.Zone}='" & CmbZone.Text & "'"
            End If
            Session("Period") = ""
            Session("Zone") = ""
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Lease Analysis"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub
End Class