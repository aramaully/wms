﻿Public Partial Class Abandon
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "AbandonGoods"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ProdCode,Description,HsCode,Unit,SubUnit FROM Product WHERE "
        StrSql = "SELECT BoeIn.BillNo, CONVERT(Char,BoeIn.BillDate,103) 'Bill Date', BoeIn.Revision, Client.Name FROM BoeIn, Client WHERE "
        StrSql = StrSql & "BoeIn.ClientID = Client.ClientID "
        If RbnClient.Checked Then
            StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCountry.Checked Then
            StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnTraffic.Checked Then
            StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCont.Checked Then
            StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
        End If
        If ChkDate.Checked Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = StrSql & "AND Boein.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND Boein.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        PnlFind.Style.Item("display") = ""
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtBillNo.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtBillNo_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtBillNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBillNo.TextChanged
        StrSql = "SELECT BoeIn.WhCode,BoeIn.Revision,BoeIn.Billdate,Client.Name FROM BoeIn,Client "
        StrSql = StrSql & "WHERE BoeIn.ClientId=Client.ClientID AND  BoeIn.BillNo = '" & TxtBillNo.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        'On Error Resume Next
        If Rdr.Read Then
            TxtRevision.Text = Rdr("Revision").ToString
            TxtDate.Text = Rdr("BillDate")
            TxtName.Text = Rdr("Name").ToString
        Else
            TxtName.Text = "Invalid Inbound"
            Exit Sub
        End If
        Rdr.Close()
        Cmd.CommandText = "SELECT DateIn,Abandoned FROM Pallets WHERE BoeNo = '" & TxtBillNo.Text & "'"
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtDatein.Text = Format(Rdr("DateIn"), "dd-MMM-yyyy")
            If Rdr("Abandoned") Then
                ButSave.Text = "Reclaim"
                ButSave_ConfirmButtonExtender.ConfirmText = "Sure to Reclaim the Pallets?"
            Else
                ButSave.Text = "Abandon"
                ButSave_ConfirmButtonExtender.ConfirmText = "Sure to Abandon the Pallets?"
            End If
        End If
        Rdr.Close()
        StrSql = "SELECT * FROM Pallets WHERE BoeNo = '" & TxtBillNo.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvPallet.DataSource = Rdr
        GdvPallet.DataBind()
        Rdr.Close()
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Try
            If ButSave.Text = "Abandon" Then
                StrSql = "UPDATE Pallets SET Abandoned = 1 WHERE BoeNo = '" & TxtBillNo.Text & "'"
            Else
                StrSql = "UPDATE Pallets SET Abandoned = 0 WHERE BoeNo = '" & TxtBillNo.Text & "'"
            End If
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            TxtBillNo_TextChanged(Me, e)
        Catch ex As Exception
            ErrHead.Text = "Abandon / Reclaim Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
        StrSql = "SELECT Top 1 BillNo FROM BoeIn WHERE BillNo > '" & TxtBillNo.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtBillNo.Text = Cmd.ExecuteScalar
        If TxtBillNo.Text = "" Then
            StrSql = "SELECT MIN(BillNo) FROM BoeIn"
            Cmd.CommandText = StrSql
            TxtBillNo.Text = Cmd.ExecuteScalar
        End If
        TxtBillNo_TextChanged(Me, e)
    End Sub

    Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevious.Click
        StrSql = "SELECT Top 1 BillNo FROM BoeIn WHERE BillNo < '" & TxtBillNo.Text & "'"
        StrSql = StrSql & "ORDER BY BillNo DESC"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtBillNo.Text = Cmd.ExecuteScalar
        If TxtBillNo.Text = "" Then
            StrSql = "SELECT MAX(BillNo) FROM BoeIn"
            Cmd.CommandText = StrSql
            TxtBillNo.Text = Cmd.ExecuteScalar
        End If
        TxtBillNo_TextChanged(Me, e)
    End Sub
End Class