﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Partial Public Class Client
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdSave As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatCabin As New DataTable

    Protected Sub UpdateCountry(ByVal ImpEx As String)
        StrSql = "SELECT Name FROM Country WHERE Name <> '' ORDER BY Name"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Dim da As New OleDb.OleDbDataAdapter(Cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        If ImpEx = "Import" Then
            GdvImport.DataSource = ds
            GdvImport.DataBind()
        Else
            GdvExport.DataSource = ds
            GdvExport.DataBind()
        End If
        da.Dispose()
        ds.Clear()
    End Sub

    Protected Sub UpdateActivity()
        Cmd.Connection = Con
        StrSql = "SELECT Activity FROM Activity"
        Cmd.CommandText = StrSql
        Dim da = New OleDb.OleDbDataAdapter(Cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvActivity.DataSource = ds
        GdvActivity.DataBind()
        Cmd.CommandText = "SELECT WhCode FROM WareHouse"
        Rdr = Cmd.ExecuteReader
        CmbFpZoneNo.Items.Add("Select...")
        While Rdr.Read
            CmbFpZoneNo.Items.Add(Rdr("WhCode"))
        End While
        Rdr.Close()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("Default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            StrSql = "SELECT DISTINCT(Category) FROM Regime ORDER BY Category"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbCategory.Items.Clear()
            CmbCategory.Items.Add("Select ...")
            While Rdr.Read
                CmbCategory.Items.Add(Rdr("Category"))
            End While
            CmbCategory.SelectedIndex = -1
            Rdr.Close()
            UpdateCountry("Import")
            UpdateCountry("Export")
            UpdateActivity()
            TabContainer1.ActiveTabIndex = 0
        End If
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Private Sub ClearAll()
        On Error Resume Next
        TxtName.Text = ""
        TxtAddress1.Text = ""
        TxtAddress2.Text = ""
        TxtCity.Text = ""
        TxtCountry.Text = ""
        TxtTelephone.Text = ""
        TxtFax.Text = ""
        TxtVatNo.Text = ""
        TxtDateofReg.Text = ""
        TxtBRegNo.Text = ""
        TxtBRegDate.Text = ""
        TxtCertIncNo.Text = ""
        TxtDateInc.Text = ""
        TxtEMail.Text = ""
        TxtWeb.Text = ""
        TxtContactTitle.Text = ""
        TxtContact.Text = ""
        TxtIDNo.Text = ""
        TxtDept.Text = ""
        TxtOffice.Text = ""
        TxtProfession.Text = ""
        TxtOfficeTelNo.Text = ""
        TxtMobile.Text = ""
        TxtContactEmail.Text = ""
        TxtContRemarks.Text = ""
        CmbCategory.SelectedIndex = -1
        TxtFPCertNo.Text = ""
        TxtFPCertDate.Text = ""
        TxtFPCertValidity.Text = ""
        TxtStoragePermit.Text = ""
        TxtPermitDate.Text = ""
        TxtPermitExp.Text = ""
        TxtLicRemarks.Text = ""
        CmbFpZoneNo.SelectedIndex = 0
        OptOp.Checked = True
        TxtFacilities.Text = ""
        TxtAreaLeased.Text = ""
        TxtNatureofGoods.Text = ""
        TxtWorkHours.Text = ""
        For Each row As GridViewRow In GdvImport.Rows
            Dim ChkMkt As CheckBox = DirectCast(row.Cells(0).FindControl("ChkImport"), CheckBox)
            ChkMkt.Checked = False
        Next
        For Each row As GridViewRow In GdvExport.Rows
            Dim ChkMkt As CheckBox = DirectCast(row.Cells(0).FindControl("ChkExport"), CheckBox)
            ChkMkt.Checked = False
        Next
        For Each row As GridViewRow In GdvActivity.Rows
            Dim ChkMkt As CheckBox = DirectCast(row.Cells(0).FindControl("ChkActivity"), CheckBox)
            ChkMkt.Checked = False
        Next
    End Sub

    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNew.Click
        ClearAll()
        TabContainer1.ActiveTabIndex = 0
        TxtClientID.Focus()
    End Sub

    Protected Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            ClearAll()
            TxtName.Text = Rdr("Name").ToString
            TxtAddress1.Text = Rdr("Address").ToString
            TxtAddress2.Text = Rdr("Address1").ToString
            TxtCity.Text = Rdr("City").ToString
            TxtCountry.Text = Rdr("Country").ToString
            TxtTelephone.Text = Rdr("Telephone").ToString
            TxtFax.Text = Rdr("Fax").ToString
            TxtVatNo.Text = Rdr("VatNo").ToString
            TxtDateofReg.Text = Rdr("DateofReg").ToString
            TxtBRegNo.Text = Rdr("BRegNo").ToString
            TxtBRegDate.Text = Rdr("BRegDate").ToString
            TxtCertIncNo.Text = Rdr("CertIncNo").ToString
            TxtDateInc.Text = Rdr("DateInc").ToString
            TxtEMail.Text = Rdr("EMail").ToString
            TxtWeb.Text = Rdr("Web").ToString
            TxtContactTitle.Text = Rdr("ContactTitle").ToString
            TxtContact.Text = Rdr("Contact").ToString
            TxtIDNo.Text = Rdr("IDNo").ToString
            TxtDept.Text = Rdr("Dept").ToString
            TxtOffice.Text = Rdr("Office").ToString
            TxtProfession.Text = Rdr("Profession").ToString
            TxtOfficeTelNo.Text = Rdr("OfficePhone").ToString
            TxtMobile.Text = Rdr("Mobile").ToString
            TxtContactEmail.Text = Rdr("ContactEmail").ToString
            TxtContRemarks.Text = Rdr("ContRemarks.Text").ToString
            CmbCategory.Text = Rdr("Category").ToString
            TxtFPCertNo.Text = Rdr("FPCertNo").ToString
            TxtFPCertDate.Text = Rdr("FPCertDate").ToString
            TxtFPCertValidity.Text = Rdr("FPCertValidity").ToString
            TxtStoragePermit.Text = Rdr("StoragePermit").ToString
            TxtPermitDate.Text = Rdr("PermitDate").ToString
            TxtPermitExp.Text = Rdr("PermitExp").ToString
            TxtLicRemarks.Text = Rdr("LicRemarks").ToString
            CmbFpZoneNo.Text = Rdr("FPZoneNo").ToString
            OptOp.Checked = IIf(Rdr("Operational"), True, False)
            TxtFacilities.Text = Rdr("Facilities").ToString
            TxtAreaLeased.Text = Rdr("AreaLeased").ToString
            TxtNatureofGoods.Text = Rdr("NatureofGoods").ToString
            TxtWorkHours.Text = Rdr("WorkHours").ToString
            Rdr.Close()
            UpdateMarket()
        Else
            ClearAll()
        End If
    End Sub

    Protected Sub UpdateMarket()
        StrSql = "SELECT * FROM Markets WHERE ClientID = '" & TxtClientID.Text & "' ORDER BY Market"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        While Rdr.Read
            If Rdr("Type") = "Import" Then
                For Each row As GridViewRow In GdvImport.Rows
                    If row.Cells(1).Text = Rdr("Market") Then
                        Dim ChkMkt As CheckBox = DirectCast(row.Cells(0).FindControl("ChkImport"), CheckBox)
                        ChkMkt.Checked = True
                    End If
                Next
            Else
                For Each row As GridViewRow In GdvExport.Rows
                    If row.Cells(1).Text = Rdr("Market") Then
                        Dim ChkMkt As CheckBox = DirectCast(row.Cells(0).FindControl("ChkExport"), CheckBox)
                        ChkMkt.Checked = True
                    End If
                Next
            End If
        End While
        Rdr.Close()
        'Update Activity
        StrSql = "SELECT * FROM ClientActivity WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        While Rdr.Read
            For Each row As GridViewRow In GdvActivity.Rows
                If row.Cells(1).Text = Rdr("Activity") Then
                    Dim ChkAct As CheckBox = DirectCast(row.Cells(0).FindControl("ChkActivity"), CheckBox)
                    ChkAct.Checked = True
                End If
            Next
        End While
        Rdr.Close()
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
        StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID > '" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevious.Click
        StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID < '" & TxtClientID.Text & "' ORDER BY ClientID DESC"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Private Sub GdvImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvImport.PageIndexChanging
        UpdateCountry("Import")
        GdvImport.PageIndex = e.NewPageIndex
    End Sub

    Private Sub GdvExport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvExport.PageIndexChanging
        UpdateCountry("Export")
        GdvExport.PageIndex = e.NewPageIndex
    End Sub

    Private Sub GdvActivity_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvActivity.PageIndexChanging
        UpdateActivity()
        GdvActivity.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub ButFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFind.Click
        PnlFind.Height = 400
        PnlFind.Width = 600
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
        'Dim strScript As String = "window.open('Default.aspx', 'Key', 'height=500,width=800,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,titlebar=no');"
        'Dim strScript As String = "window.showModalDialog('Default.aspx', 'Key', 'height=500,width=800,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,titlebar=no');"
        'Dim strScript As String = "window.showModalDialog('Default.aspx','','height=500,width=800,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,titlebar=no')"
        'ScriptManager.RegisterStartupScript(Me, Type.GetType("System.String"), "", strScript, True)
    End Sub

    Protected Sub ButSelect_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Protected Sub ButRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
        If RbtName.Checked Then
            StrSql = StrSql & "Name"
        ElseIf RbtCity.Checked Then
            StrSql = StrSql & "City"
        Else
            StrSql = StrSql & "Contact"
        End If
        StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtClientID_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        System.Threading.Thread.Sleep(2000)
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Cmd.Connection = Con
        StrSql = "SELECT COUNT(ClientID) FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Cmd.ExecuteScalar = 0 Then
            StrSql = "INSERT INTO Client (ClientID,Name,Address,Address1,City,Country,Telephone,Fax,VatNo,DateofReg,"
            StrSql = StrSql & "BRegNo,BRegDate,CertIncNo,DateInc,EMail,Web,ContactTitle,Contact,IDNo,Dept,Office,"
            StrSql = StrSql & "Profession,OfficePhone,Mobile,ContactEmail,ContRemarks,Category,FPCertNo,FPCertDate,"
            StrSql = StrSql & "FPCertValidity,StoragePermit,PermitDate,PermitExp,LicRemarks,FPZoneNo,Operational,"
            StrSql = StrSql & "Facilities,AreaLeased,NatureofGoods,WorkHours) VALUES('"
            StrSql = StrSql & TxtClientID.Text & "','"
            StrSql = StrSql & TxtName.Text & "','"
            StrSql = StrSql & TxtAddress1.Text & "','"
            StrSql = StrSql & TxtAddress2.Text & "','"
            StrSql = StrSql & TxtCity.Text & "','"
            StrSql = StrSql & TxtCountry.Text & "','"
            StrSql = StrSql & TxtTelephone.Text & "','"
            StrSql = StrSql & TxtFax.Text & "','"
            StrSql = StrSql & TxtVatNo.Text & "',"
            If TxtDateofReg.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtDateofReg.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & TxtBRegNo.Text & "',"
            If TxtBRegDate.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtBRegDate.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & TxtCertIncNo.Text & "',"
            If TxtDateInc.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtDateInc.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & TxtEMail.Text & "','"
            StrSql = StrSql & TxtWeb.Text & "','"
            StrSql = StrSql & TxtContactTitle.Text & "','"
            StrSql = StrSql & TxtContact.Text & "','"
            StrSql = StrSql & TxtIDNo.Text & "','"
            StrSql = StrSql & TxtDept.Text & "','"
            StrSql = StrSql & TxtOffice.Text & "','"
            StrSql = StrSql & TxtProfession.Text & "','"
            StrSql = StrSql & TxtOfficeTelNo.Text & "','"
            StrSql = StrSql & TxtMobile.Text & "','"
            StrSql = StrSql & TxtContactEmail.Text & "','"
            StrSql = StrSql & TxtContRemarks.Text & "','"
            StrSql = StrSql & CmbCategory.Text & "','"
            StrSql = StrSql & TxtFPCertNo.Text & "',"
            If TxtFPCertDate.Text = "" Then
                StrSql = StrSql & "NULL,"
            Else
                StrSql = StrSql & "'" & CDate(TxtFPCertDate.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            If TxtFPCertValidity.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtFPCertValidity.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & TxtStoragePermit.Text & "',"
            If TxtPermitDate.Text = "" Then
                StrSql = StrSql & "NULL,"
            Else
                StrSql = StrSql & "'" & CDate(TxtPermitDate.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            If TxtPermitExp.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtPermitExp.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & TxtLicRemarks.Text & "','"
            If CmbFpZoneNo.Text = "Select..." Then
                StrSql = StrSql & "',"
            Else
                StrSql = StrSql & CmbFpZoneNo.Text & "',"
            End If
            StrSql = StrSql & IIf(OptOp.Checked, 1, 0)
            StrSql = StrSql & Val(TxtFacilities.Text) & ","
            StrSql = StrSql & Val(TxtAreaLeased.Text) & ",'"
            StrSql = StrSql & TxtNatureofGoods.Text & "','"
            StrSql = StrSql & TxtWorkHours.Text & "')"
            ErrHead.Text = "Insert record Failed"
        Else
            StrSql = "UPDATE Client SET "
            StrSql = StrSql & "ClientID = '" & TxtClientID.Text & "',"
            StrSql = StrSql & "Name = '" & TxtName.Text & "',"
            StrSql = StrSql & "Address = '" & TxtAddress1.Text & "',"
            StrSql = StrSql & "Address1 = '" & TxtAddress2.Text & "',"
            StrSql = StrSql & "City = '" & TxtCity.Text & "',"
            StrSql = StrSql & "Country = '" & TxtCountry.Text & "',"
            StrSql = StrSql & "Telephone = '" & TxtTelephone.Text & "',"
            StrSql = StrSql & "Fax = '" & TxtFax.Text & "',"
            StrSql = StrSql & "VatNo = '" & TxtVatNo.Text & "',"
            If TxtDateofReg.Text <> "" Then
                StrSql = StrSql & "DateofReg = '" & CDate(TxtDateofReg.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "BRegNo = '" & TxtBRegNo.Text & "',"
            If TxtBRegDate.Text <> "" Then
                StrSql = StrSql & "BRegDate = '" & CDate(TxtBRegDate.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "CertIncNo = '" & TxtCertIncNo.Text & "',"
            If TxtDateInc.Text <> "" Then
                StrSql = StrSql & "DateInc = '" & CDate(TxtDateInc.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "EMail = '" & TxtEMail.Text & "',"
            StrSql = StrSql & "Web = '" & TxtWeb.Text & "',"
            StrSql = StrSql & "ContactTitle = '" & TxtContactTitle.Text & "',"
            StrSql = StrSql & "Contact = '" & TxtContact.Text & "',"
            StrSql = StrSql & "IDNo = '" & TxtIDNo.Text & "',"
            StrSql = StrSql & "Dept = '" & TxtDept.Text & "',"
            StrSql = StrSql & "Office = '" & TxtOffice.Text & "',"
            StrSql = StrSql & "Profession = '" & TxtProfession.Text & "',"
            StrSql = StrSql & "OfficePhone = '" & TxtOfficeTelNo.Text & "',"
            StrSql = StrSql & "Mobile = '" & TxtMobile.Text & "',"
            StrSql = StrSql & "ContactEmail = '" & TxtContactEmail.Text & "',"
            StrSql = StrSql & "ContRemarks = '" & TxtContRemarks.Text & "',"
            StrSql = StrSql & "Category = '" & CmbCategory.Text & "',"
            StrSql = StrSql & "FPCertNo = '" & TxtFPCertNo.Text & "',"
            If TxtFPCertDate.Text <> "" Then
                StrSql = StrSql & "FPCertDate = '" & CDate(TxtFPCertDate.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            If TxtFPCertValidity.Text <> "" Then
                StrSql = StrSql & "FPCertValidity = '" & CDate(TxtFPCertValidity.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "StoragePermit = '" & TxtStoragePermit.Text & "',"
            If TxtPermitDate.Text <> "" Then
                StrSql = StrSql & "PermitDate = '" & CDate(TxtPermitDate.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            If TxtPermitExp.Text <> "" Then
                StrSql = StrSql & "PermitExp = '" & CDate(TxtPermitExp.Text).ToString("dd-MMM-yyyy") & "',"
            End If
            StrSql = StrSql & "LicRemarks = '" & TxtLicRemarks.Text & "',"
            If CmbFpZoneNo.Text <> "Select..." Then
                StrSql = StrSql & "FPZoneNo = '" & CmbFpZoneNo.Text & "',"
            End If
            StrSql = StrSql & "Operational = " & IIf(OptOp.Checked, 1, 0) & ","
            StrSql = StrSql & "Facilities = " & Val(TxtFacilities.Text) & ","
            StrSql = StrSql & "AreaLeased = " & Val(TxtAreaLeased.Text) & ","
            StrSql = StrSql & "NatureofGoods= '" & TxtNatureofGoods.Text & "',"
            StrSql = StrSql & "WorkHours = '" & TxtWorkHours.Text & "' "
            StrSql = StrSql & " WHERE ClientID = '" & TxtClientID.Text & "'"
            ErrHead.Text = "Update Failed"
        End If
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Transaction = Trans
        Cmd.CommandText = StrSql
        Try
            Cmd.ExecuteNonQuery()
            'Update Markets Table
            StrSql = "DELETE FROM Markets WHERE ClientID = '" & TxtClientID.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            'Import Market
            For Each Row As GridViewRow In GdvImport.Rows
                Dim ChkMkt As CheckBox = DirectCast(Row.Cells(0).FindControl("ChkImport"), CheckBox)
                If ChkMkt.Checked Then
                    StrSql = "INSERT INTO Markets (ClientID,Market,Type) VALUES('"
                    StrSql = StrSql & TxtClientID.Text & "','"
                    StrSql = StrSql & Row.Cells(1).Text & "','Import')"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            'Export Market
            For Each Row As GridViewRow In GdvExport.Rows
                Dim ChkMkt As CheckBox = DirectCast(Row.Cells(0).FindControl("ChkExport"), CheckBox)
                If ChkMkt.Checked Then
                    StrSql = "INSERT INTO Markets (ClientID,Market,Type) VALUES('"
                    StrSql = StrSql & TxtClientID.Text & "','"
                    StrSql = StrSql & Row.Cells(1).Text & "','Export')"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            'Update Activity Table
            StrSql = "DELETE FROM ClientActivity WHERE ClientId = '" & TxtClientID.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            For Each Row As GridViewRow In GdvActivity.Rows
                Dim ChkAct As CheckBox = DirectCast(Row.Cells(0).FindControl("ChkActivity"), CheckBox)
                If ChkAct.Checked Then
                    StrSql = "INSERT INTO ClientActivity (ClientID,Activity) VALUES('"
                    StrSql = StrSql & TxtClientID.Text & "','"
                    StrSql = StrSql & Row.Cells(1).Text & "')"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            Trans.Commit()
            ClearAll()
        Catch ex As Exception
            Trans.Rollback()
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub
End Class