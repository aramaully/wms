﻿Public Partial Class FrmFindProduct
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtSearch.Text = Session("PstrPCode")
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Refresh" Then
            Cmd.Connection = Con
            StrSql = "SELECT ProdCode,[Description],[Unit] FROM Product "
            If TxtSearch.Text <> "" Then
                StrSql = StrSql & "WHERE [Description] LIKE '%" & TxtSearch.Text & "%'"
            End If
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            GdvProduct.DataSource = Rdr
            GdvProduct.DataBind()
            Rdr.Close()
        End If
    End Sub

    Private Sub GdvEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvProduct.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvProduct, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvEmp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvProduct.SelectedIndexChanged
        TxtProductCode.Value = GdvProduct.SelectedRow.Cells(0).Text
        GdvProduct.SelectedRow.Focus()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CloseMe", "GetID();", True)
    End Sub

    Private Sub GdvEmp_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvProduct.SelectedIndexChanging
        GdvProduct.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvProduct.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvProduct.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub TxtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtSearch.TextChanged
        Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(0))
        ToolBar1_ItemClick(Me, ev)
    End Sub
End Class