﻿Public Partial Class MoveOut
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TabContainer1.ActiveTabIndex = 0
        End If
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
        StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID > '" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevious.Click
        StrSql = "SELECT TOP 1 ClientID FROM Client WHERE ClientID < '" & TxtClientID.Text & "' ORDER BY ClientID DESC"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtClientID.Text = Cmd.ExecuteScalar
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
        If RbtName.Checked Then
            StrSql = StrSql & "Name"
        ElseIf RbtCity.Checked Then
            StrSql = StrSql & "City"
        Else
            StrSql = StrSql & "Contact"
        End If
        StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        PnlFind.Height = 400
        PnlFind.Width = 600
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtClientID_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvCabs.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCabs.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            ClearAll()
            TxtClientID.Text = Rdr("ClientID")
            TxtCategory.Text = Trim(Rdr("Category").ToString)
            TxtName.Text = Rdr("Name").ToString
            TxtAddress.Text = Rdr("Address").ToString
            TxtCity.Text = Rdr("City").ToString
            TxtTelephone.Text = Rdr("Telephone").ToString
            TxtFax.Text = Rdr("Fax").ToString
            TxtEmail.Text = Rdr("EMail").ToString
            TxtContact.Text = Rdr("Contact").ToString
            TxtPosition.Text = Rdr("Position").ToString
            TxtDate.Text = Format(Rdr("DateCreated"), "dd-MMM-yyyy")
            TxtVatNo.Text = Rdr("VATNo").ToString
            TxtLicenceNo.Text = Rdr("LicenceNo").ToString
            TxtStoragePermit.Text = Rdr("StoragePermit").ToString
            TxtLicenceExp.Text = Format(Rdr("LicenceExp"), "dd-MMM-yyyy")
            TxtStorageExp.Text = Format(Rdr("PermitExp"), "dd-MMM-yyyy")
            CmbStatus.Text = Rdr("Status").ToString
            TxtProdType.Text = Rdr("ProdType").ToString
            TxtZone.Text = Rdr("WHouses").ToString
            Rdr.Close()
            UpDateCabins()
            TxtMoveOut.Focus()
        Else
            Rdr.Close()
            ClearAll()
        End If
    End Sub

    Private Sub UpDateCabins()
        StrSql = "SELECT * FROM ClientCabin WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvCabs.DataSource = Rdr
        GdvCabs.DataBind()
        Rdr.Close()
        If GdvCabs.Rows.Count > 0 Then
            Dim e As New System.EventArgs
            GdvCabs.SelectedIndex = 0
            GdvCabs_SelectedIndexChanged(Me, e)
        End If
    End Sub

    Private Sub ClearAll()
        TxtMoveOut.Text = Date.Today.ToString("dd-MMM-yyyy")
        TxtCategory.Text = ""
        TxtName.Text = ""
        TxtAddress.Text = ""
        TxtCity.Text = ""
        TxtTelephone.Text = ""
        TxtFax.Text = ""
        TxtEmail.Text = ""
        TxtContact.Text = ""
        TxtPosition.Text = ""
        TxtVatNo.Text = ""
        TxtLicenceNo.Text = ""
        TxtStoragePermit.Text = ""
        TxtProdType.Text = ""
        TxtZone.Text = ""
        TxtDate.Text = ""
        TxtLicenceExp.Text = ""
        TxtStorageExp.Text = ""
        CmbStatus.SelectedIndex = -1
        GdvCabs.DataSource = Nothing
        GdvCabs.DataBind()
        TxtCabin.Text = ""
        TxtFPZone.Text = ""
        TxtDateOut.Text = ""
        TxtDepositAmt.Text = ""
        TxtGoodsRemarks.Text = ""
        TxtKeyRemarks.Text = ""
        TxtLeaseExp.Text = ""
        TxtLeaseOn.Text = ""
        TxtMoveOut.Text = ""
        TxtRate.Text = ""
        TxtReason.Text = ""
        TxtAmountDue.Text = ""
        TxtRefundOn.Text = ""
        ChkKeys.Checked = False
        ChkGoods.Checked = False
        ChkAmount.Checked = False
    End Sub

    Protected Sub ButAddCab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAddCab.Click
        For Each Row As GridViewRow In GdvCabs.Rows
            If Row.Cells(0).Text = TxtFPZone.Text And Row.Cells(1).Text = TxtCabin.Text Then
                Row.Cells(0).Text = TxtFPZone.Text
                Row.Cells(1).Text = TxtCabin.Text
                Row.Cells(2).Text = TxtArea.Text
                Row.Cells(7).Text = TxtAmountDue.Text
                Row.Cells(4).Text = TxtDepositAmt.Text
                Row.Cells(5).Text = TxtLeaseOn.Text
                Row.Cells(3).Text = TxtRate.Text
                Row.Cells(16).Text = TxtReason.Text
                Row.Cells(6).Text = TxtLeaseExp.Text
                DirectCast(Row.Cells(10).Controls(0), CheckBox).Checked = ChkKeys.Checked
                DirectCast(Row.Cells(12).Controls(0), CheckBox).Checked = ChkGoods.Checked
                DirectCast(Row.Cells(14).Controls(0), CheckBox).Checked = ChkAmount.Checked
                Row.Cells(8).Text = TxtDateOut.Text
                Row.Cells(9).Text = TxtRefundOn.Text
                Row.Cells(11).Text = TxtKeyRemarks.Text
                Row.Cells(13).Text = TxtGoodsRemarks.Text
                Row.Cells(15).Text = TxtAmountRemarks.Text
                Exit Sub
            End If
        Next
    End Sub

    Private Function ConSng(ByVal Text As String) As Single
        If Text = "" Or Text = "&nbsp;" Then
            Text = 0
        End If
        Return CSng(Text)
    End Function

    Private Sub GdvCabs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvCabs.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvCabs, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvCabs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvCabs.SelectedIndexChanged
        Dim Row As GridViewRow = GdvCabs.SelectedRow
        TxtFPZone.Text = Row.Cells(0).Text
        TxtCabin.Text = Row.Cells(1).Text
        TxtArea.Text = ChkSpc(Row.Cells(2).Text)
        TxtAmountDue.Text = ChkSpc(Row.Cells(7).Text)
        TxtDepositAmt.Text = ChkSpc(Row.Cells(4).Text)
        TxtLeaseOn.Text = ChkSpc(Row.Cells(5).Text)
        TxtRate.Text = ChkSpc(Row.Cells(3).Text)
        TxtReason.Text = ChkSpc(Row.Cells(16).Text)
        TxtLeaseExp.Text = ChkSpc(Row.Cells(6).Text)
        ChkKeys.Checked = DirectCast(Row.Cells(10).Controls(0), CheckBox).Checked
        ChkGoods.Checked = DirectCast(Row.Cells(12).Controls(0), CheckBox).Checked
        ChkAmount.Checked = DirectCast(Row.Cells(14).Controls(0), CheckBox).Checked
        TxtDateOut.Text = ChkSpc(Row.Cells(8).Text)
        TxtRefundOn.Text = ChkSpc(Row.Cells(9).Text)
        TxtKeyRemarks.Text = ChkSpc(Row.Cells(11).Text)
        TxtGoodsRemarks.Text = ChkSpc(Row.Cells(13).Text)
        TxtAmountRemarks.Text = ChkSpc(Row.Cells(15).Text)
        If TxtDateOut.Text = "" Then TxtDateOut.Text = TxtMoveOut.Text
    End Sub

    Private Sub GdvCabs_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvCabs.SelectedIndexChanging
        GdvCabs.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub TxtArea_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtArea.TextChanged
        TxtAmountDue.Text = ConSng(TxtArea.Text) * ConSng(TxtRate.Text)
    End Sub

    Private Sub TxtRate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtRate.TextChanged
        TxtAmountDue.Text = ConSng(TxtArea.Text) * ConSng(TxtRate.Text)
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Dim PstrDateFormat As String = "dd-MMM-yyyy"
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            For Each Row As GridViewRow In GdvCabs.Rows
                StrSql = "UPDATE ClientCabin SET "
                If IsDate(Row.Cells(8).Text) Then
                    StrSql = StrSql & "MovedOut = '" & Row.Cells(8).Text & "',"
                End If
                StrSql = StrSql & "AmountDue = " & ConSng(Row.Cells(7).Text) & ","
                If IsDate(Row.Cells(8).Text) Then
                    StrSql = StrSql & "ToRefund = '" & Row.Cells(9).Text & "',"
                End If
                StrSql = StrSql & "Reason = '" & Row.Cells(16).Text & "',"
                StrSql = StrSql & "KeyRemarks = '" & Row.Cells(11).Text & "',"
                StrSql = StrSql & "GoodsRemarks = '" & Row.Cells(13).Text & "',"
                StrSql = StrSql & "AmountRemarks = '" & Row.Cells(15).Text & "',"
                StrSql = StrSql & "KeyReturned = " & IIf(DirectCast(Row.Cells(10).Controls(0), CheckBox).Checked, 1, 0) & ","
                StrSql = StrSql & "GoodsinCabin = " & IIf(DirectCast(Row.Cells(12).Controls(0), CheckBox).Checked, 1, 0) & ","
                StrSql = StrSql & "OutstandingAmt = " & IIf(DirectCast(Row.Cells(14).Controls(0), CheckBox).Checked, 1, 0)
                StrSql = StrSql & " WHERE ClientID = '" & TxtClientID.Text & "' AND WhCode = '" & Row.Cells(0).Text & "' "
                StrSql = StrSql & "AND CabinNo = '" & Row.Cells(1).Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                'Update Cabin 
                If IsDate(Row.Cells(8).Text) Then
                    StrSql = "UPDATE Zones SET ClientID = '" & TxtClientID.Text & "',"
                    StrSql = StrSql & "EndDate = '" & Format(CDate(Row.Cells(8).Text), PstrDateFormat) & "' WHERE WhCode = '" & Row.Cells(0).Text & "' "
                    StrSql = StrSql & "AND ZoneCode = '" & Row.Cells(1).Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            Trans.Commit()
            ClearAll()
            TabContainer1.ActiveTabIndex = 0
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Save Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNew.Click
        ClearAll()
        TabContainer1.ActiveTabIndex = 0
        TxtClientID.Focus()
    End Sub

    Private Function ChkSpc(ByVal Text As String) As String
        If Text = "&nbsp;" Then
            Text = ""
        End If
        Return Text
    End Function
End Class