﻿Public Partial Class FrmContAccRep
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "ContainerReport"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then

            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            CmbMonth.SelectedIndex = Date.Today.Month - 1
            TxtYear.Text = Date.Today.Year
            Cmd.CommandText = "SELECT Whcode FROM [System]"
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("ALL")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Dim StrSelFormula As String
            StrSelFormula = "{ContDetail.Month}='" & CmbMonth.Text & "' And {ContDetail.Year} = '" & TxtYear.Text & "'"
            If CmbZone.Text <> "ALL" Then
                StrSelFormula = StrSelFormula & " And {ContDetail.WhCode} = '" & CmbZone.Text & "'"
            End If
            Session("ReportFile") = "RptContAccRep.rpt"
            Session("Period") = ""
            Session("Zone") = ""
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Container Report"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub
End Class