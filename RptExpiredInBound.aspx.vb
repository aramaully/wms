﻿Imports System.Web.Security
Imports System.Data
Imports System.Data.OleDb
Imports System.IO

Imports System.Configuration
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Drawing.Printing

Partial Public Class RptExpiredInBound
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim StrSQL As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtDate.Text = Format(Today, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        Dim StrSelFormula As String
        StrSelFormula = "{ExpiredInbound.WarehousingDate} <= Date(" & Format(DateAdd("M", -Val(TxtMonth.Text), CDate(TxtDate.Text)), "yyyy,MM,dd") & ")"
        StrSelFormula = StrSelFormula & " And {ExpiredInbound.BalQuantity} = 1"
        If TxtClientID.Text <> "ALL" Then
            StrSelFormula = StrSelFormula & " And {ExpiredInbound.ClientID} = '" & TxtClientID.Text & "'"
        End If
        Session("ReportFile") = "RptExpiredInbound.rpt"
        Session("Period") = ""
        Session("Zone") = ""
        Session("SelFormula") = StrSelFormula
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        'Response.Redirect("Report.aspx?RptID=Expired InBound&RPTFileName=RptExpiredInBound.aspx&RptFile=ExpiredInBound.rpt&StrSelF=" & StrSelFormula)
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub BtnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnFind.Click
        Session.Add("PstrCode", TxtClientID.Text)
        ScriptManager.RegisterStartupScript(BtnFind, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
    End Sub

    Private Sub TxtClient_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Try
            TxtName.Text = Cmd.ExecuteScalar.ToString
        Catch ex As Exception
            TxtName.Text = "Invalid Client"
        End Try
    End Sub
End Class