﻿Public Partial Class Occupancy
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "ComputeOccupancy"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            'StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            'Cmd.CommandText = StrSql
            'Rdr = Cmd.ExecuteReader
            'CmbZone.Items.Add("Select...")
            'While Rdr.Read
            '    CmbZone.Items.Add(Rdr("WhCode"))
            'End While
            'Rdr.Close()
            CmbZone.Items.Add("FPZ6SD")
            TxtYear.Text = Year(Date.Today.AddMonths(-1))
            CmbMonth.Text = Date.Today.AddMonths(-1).ToString("MMMM")
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
        If RbnClient.Checked Then
            StrSql = StrSql & "Name"
        ElseIf RbnCountry.Checked Then
            StrSql = StrSql & "City"
        Else
            StrSql = StrSql & "Contact"
        End If
        StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        RbnClient.Text = "Name"
        RbnCountry.Text = "City"
        RbnTraffic.Text = "Contact"
        RbnCont.Visible = False
        ChkDate.Visible = False
        TxtFrom.Visible = False
        TxtTo.Visible = False
        LblToDate.Visible = False
        GdvFind.DataSource = Nothing
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtClientID_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub ButView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButView.Click
        GdvOcc.DataSource = Nothing
        StrSql = "SELECT * FROM Occupancy WHERE Occupancy.WHCode = '" & CmbZone.Text & "' "
        StrSql = StrSql & "AND Month = '" & CmbMonth.Text & "' AND Year = " & TxtYear.Text & " ORDER BY Occupancy.ClientID"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvOcc.DataSource = Rdr
        GdvOcc.DataBind()
        Rdr.Close()
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        If UCase(TxtClientID.Text) <> "ALL" Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
            Try
                TxtName.Text = Cmd.ExecuteScalar.ToString
            Catch ex As Exception
                TxtName.Text = "Invalid Client"
            End Try
        Else
            TxtName.Text = "All Customers"
        End If
    End Sub

    'aramaully | V1.0.0.3
    Protected Sub ButCompute_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCompute.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim DteOccDate As Date
        Dim DteFirst As Date
        Dim DteLast As Date
        Dim CmdOcc As New OleDb.OleDbCommand
        Dim ConOcc As New OleDb.OleDbConnection
        Dim Trans As OleDb.OleDbTransaction

        ConOcc.ConnectionString = Session("ConnString")
        ConOcc.Open()
        If CmbZone.Text = "Select..." Then
            ErrHead.Text = "Data Required"
            ErrMsg.Text = "Please Select the FP Zone!"
            ModalPopupExtender1.Show()
            CmbZone.Focus()
            Exit Sub
        End If

        If TxtClientID.Text = "400C9186" Then
            ErrHead.Text = "Info"
            ErrMsg.Text = "You are not allowed to compute occupancy for ZPC Motors Ltd! There is already a lease agreement in zone 6."
            ModalPopupExtender1.Show()
            TxtClientID.Focus()
            Exit Sub
        End If

        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        DteOccDate = CDate("25, " & CmbMonth.Text & " " & TxtYear.Text)
        DteFirst = CDate("01, " & CmbMonth.Text & " " & TxtYear.Text)
        DteLast = DteOccDate
        Do While Month(DteLast.AddDays(1)) = Month(DteOccDate)
            DteLast = DteLast.AddDays(1)
        Loop
        If TxtClientID.Text <> "ALL" Then
            StrSql = " FROM Occupancy WHERE Occupancy.BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "' And ClientID='" & TxtClientID.Text & "'"
            StrSql = StrSql & "  AND Occupancy.WHCode ='" & CmbZone.Text & "'"
        Else
            StrSql = " FROM Occupancy WHERE Occupancy.BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "'"
            StrSql = StrSql & " AND Occupancy.WHCode ='" & CmbZone.Text & "'"
        End If
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Transaction = Trans
        Cmd.Connection = Con
        CmdOcc.Connection = ConOcc
        Try
            If Not ChkReCompute.Checked Then
                Cmd.CommandText = "SELECT COUNT(*)" & StrSql
                If Cmd.ExecuteScalar > 0 Then
                    ErrHead.Text = "Error"
                    ErrMsg.Text = "This Occupancy has already been computed. Check Re-Compute to do again!"
                    ModalPopupExtender1.Show()
                    Exit Sub
                End If
            End If
            If TxtClientID.Text <> "ALL" Then
                StrSql = "DELETE FROM Occupancy WHERE BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "' And ClientID = '" & TxtClientID.Text & "'"
                StrSql = StrSql & " AND Occupancy.WHCode = '" & CmbZone.Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            Else
                StrSql = "DELETE FROM Occupancy WHERE BillDate = '" & Format(DteOccDate, "dd-MMM-yyyy") & "'"
                StrSql = StrSql & " AND Occupancy.WHCode = '" & CmbZone.Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            End If
            'aramaully | V1.0.0.3
            If TxtClientID.Text <> "ALL" Then
                'StrSql = "SELECT Pallets.*, Boein.WhCode FROM Pallets, Boein WHERE Pallets.BoeNo = BoeIn.Billno AND (DateCleared IS NULL OR DateCleared >= '"
                'StrSql = StrSql & Format(DteFirst, "dd-MMM-yyyy") & "') AND Abandoned = 0 And Boein.ClientID = '" & TxtClientID.Text & "'"
                StrSql = "SELECT Pallets.*, PickList.GateOutRate, PickList.GateOutFees FROM Pallets"
                StrSql = StrSql & " LEFT OUTER JOIN PickList ON Pallets.PalletID = PickList.PalletID"
                StrSql = StrSql & " WHERE (Pallets.DateCleared Is NULL Or Pallets.DateCleared >= '" & Format(DteFirst, "dd-MMM-yyyy") & "')"
                StrSql = StrSql & " And Pallets.Abandoned = 0"
                StrSql = StrSql & " And Pallets.ClientID = '" & TxtClientID.Text & "'"
            Else
                'StrSql = "SELECT Pallets.*, Boein.WhCode FROM Pallets, Boein WHERE Pallets.BoeNo = BoeIn.Billno AND  (DateCleared IS NULL OR DateCleared >= '"
                'StrSql = StrSql & Format(DteFirst, "dd-MMM-yyyy") & "') AND Abandoned = 0 "
                StrSql = "SELECT Pallets.*, PickList.GateOutRate, PickList.GateOutFees FROM Pallets"
                StrSql = StrSql & " LEFT OUTER JOIN PickList ON Pallets.PalletID = PickList.PalletID"
                StrSql = StrSql & " WHERE (Pallets.DateCleared Is NULL Or Pallets.DateCleared >= '" & Format(DteFirst, "dd-MMM-yyyy") & "')"
                StrSql = StrSql & " And Pallets.Abandoned = 0"
            End If
            'StrSql = StrSql & " AND BoeIn.WHCode = '" & CmbZone.Text & "' ORDER BY BoeNo, DateIn, DateCleared"
            StrSql = StrSql & " And Pallets.WhCode = '" & CmbZone.Text & "' ORDER BY Pallets.BoeNo, Pallets.DateIn, Pallets.DateCleared"
            CmdOcc.CommandText = StrSql
            Rdr = CmdOcc.ExecuteReader
            While Rdr.Read
                If Rdr("ClientID") = "400C9186" Then
                    'Do not compute occupancy for ZPC Motors Ltd (zone FPZ6SD)
                Else
                    If Rdr("DateIn") <= DteLast Then
                        Dim palletID = Rdr("PalletID")
                        StrSql = "INSERT INTO Occupancy (ClientID, BoeNo, PalletID, BillDate, [Month], [Year], PRatio, ZoneCode, WhCode, [From], GateInRate, GateInFees, [To], GateOutRate, GateOutFees) "
                        StrSql = StrSql & "Values('" & Rdr("ClientID") & "','"
                        StrSql = StrSql & Rdr("BoeNo") & "','"
                        StrSql = StrSql & Rdr("PalletID") & "','"
                        StrSql = StrSql & DteOccDate.ToString("dd-MMM-yyyy") & "','"
                        StrSql = StrSql & CmbMonth.Text & "',"
                        StrSql = StrSql & Val(TxtYear.Text) & ","
                        StrSql = StrSql & Val(Rdr("PRatio")) & ",'"
                        StrSql = StrSql & Rdr("LocCode1") & "','"
                        StrSql = StrSql & CmbZone.Text & "','"
                        If Rdr("DateIn") < DteFirst Then
                            StrSql = StrSql & DteFirst.ToString("dd-MMM-yyyy") & "',"
                            StrSql = StrSql & "0.00,0.00,'"
                        Else
                            'aramaully | V1.0.0.3
                            StrSql = StrSql & Format(Rdr("DateIn"), "dd-MMM-yyyy") & "',"
                            If IsDBNull(Rdr("GateInRate")) Then
                                StrSql = StrSql & "0.00,0.00,'"
                            Else
                                StrSql = StrSql & Rdr("GateInRate") & ","
                                StrSql = StrSql & Rdr("GateInFees") & ",'"
                            End If
                        End If
                        If IsDBNull(Rdr("DateCleared")) OrElse Rdr("DateCleared") > DteLast Then
                            StrSql = StrSql & DteLast.ToString("dd-MMM-yyyy") & "',"
                            StrSql = StrSql & "0.00,0.00)"
                        Else
                            'aramaully | V1.0.0.3
                            StrSql = StrSql & Format(Rdr("DateCleared"), "dd-MMM-yyyy") & "',"
                            If IsDBNull(Rdr("GateOutRate")) Then
                                StrSql = StrSql & "0.00,0.00)"
                            Else
                                StrSql = StrSql & Rdr("GateOutRate") & ","
                                StrSql = StrSql & Rdr("GateOutFees") & ")"
                            End If

                        End If
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If
                End If
            End While
            Rdr.Close()
            StrSql = "UPDATE Occupancy SET Days = DATEDIFF(Day,[From],[To])+1 WHERE Month = '" & CmbMonth.Text & "' AND Year = " & TxtYear.Text
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ButView_Click(Me, e)
            ErrHead.Text = "Error Computing Occupancy"
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Error Computing Occupancy"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class