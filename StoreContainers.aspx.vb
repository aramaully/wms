﻿Partial Public Class StoreContainers
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdScalar As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim RdrCont As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrComboList As String
    Dim LngInvoiceNo As Long
    Dim CmdDelCont As New OleDb.OleDbCommand
    Dim BlnPrefClient As Boolean

    Const MENUID = "StoreContainers"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If
            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbWHCode.Items.Add("Select...")
            While Rdr.Read
                CmbWHCode.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()

            StrSql = "SELECT * FROM Traffic ORDER BY Name"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbTraffic.Items.Add("Select...")
            While Rdr.Read
                CmbTraffic.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub ButFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFind.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        RbnClient.Text = "Client"
        RbnCountry.Text = "Country"
        RbnTraffic.Text = "Traffic Op"
        RbnCont.Visible = True
        ChkDate.Visible = True
        TxtFrom.Visible = True
        TxtTo.Visible = True
        LblToDate.Visible = True
        GdvFind.DataSource = Nothing
        TxtTo.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtFrom.Text = Date.Today.AddDays(-10).ToString("dd/MM/yyyy")
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        If RbnClient.Text = "Client" Then
            TxtClient.Text = GdvFind.SelectedRow.Cells(3).Text
            TxtClient_TextChanged(Me, e)
            DtpDate.Text = Format(CDate(GdvFind.SelectedRow.Cells(1).Text), "dd/MM/yyyy")
        Else
            TxtClient.Text = GdvFind.SelectedRow.Cells(0).Text
            TxtClient_TextChanged(Me, e)
        End If
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvCont.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCont.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If RbnClient.Text = "Name" Then
            StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
            If RbnClient.Checked Then
                StrSql = StrSql & "Name"
            ElseIf RbnCountry.Checked Then
                StrSql = StrSql & "City"
            Else
                StrSql = StrSql & "Contact"
            End If
            StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Else
            StrSql = "SELECT Containers.ContainerNo, CONVERT(Char,containers.DateIN,103) 'Date IN', Containers.[Description],Client.ClientID, Client.Name FROM Containers, Client WHERE "
            StrSql = StrSql & "Containers.ClientID = Client.ClientID "
            If RbnClient.Checked Then
                StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCountry.Checked Then
                StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnTraffic.Checked Then
                StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCont.Checked Then
                StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
            End If
            If ChkDate.Checked Then
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                StrSql = StrSql & "AND Containers.DateIN >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
                StrSql = StrSql & "AND Containers.DateIN <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
            End If
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFindCli_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFindCli.Click
        If DtpDate.Text = "" Then
            'Check if date is selected
            LblError.Visible = True
            LblError.Text = "Please select a date first"
            Exit Sub
        End If
        RbnClient.Text = "Name"
        RbnCountry.Text = "City"
        RbnTraffic.Text = "Contact"
        RbnCont.Visible = False
        ChkDate.Visible = False
        TxtFrom.Visible = False
        TxtTo.Visible = False
        LblToDate.Visible = False
        GdvFind.DataSource = Nothing
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub TxtClient_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClient.TextChanged
        Dim CmdClient As New OleDb.OleDbCommand
        Dim CmdGetClientTariff As New OleDb.OleDbCommand
        Dim RdrGetClientTariff As OleDb.OleDbDataReader
        Dim CmdClientTariff As New OleDb.OleDbCommand
        Dim RdrClientTariff As OleDb.OleDbDataReader
        CmdClient.Connection = Con
        CmdGetClientTariff.Connection = Con
        CmdClientTariff.Connection = Con



        CmdClient.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & TxtClient.Text & "'"
        Try
            TxtName.Text = CmdClient.ExecuteScalar.ToString
            'Get the Data from Tariff Card Depending on the Client
            StrSql = "SELECT DISTINCT(ChCode) FROM ClientTariff WHERE ClientID = '" & TxtClient.Text & "'"
            StrSql = StrSql & " AND ChCode IN (SELECT ChCode FROM TariffCard WHERE StorageContainer=1 AND FPZone='" & CmbWHCode.Text & "')"
            CmdClientTariff.CommandText = StrSql
            RdrClientTariff = CmdClientTariff.ExecuteReader
            DDLCharge.Items.Clear()
            DdlRate.Items.Clear()
            DdlChCode.Items.Clear()
            DDLCharge.Items.Add("Select...")
            DdlRate.Items.Add("Select...")
            DdlChCode.Items.Add("Select...")
            While RdrClientTariff.Read
                StrSql = "SELECT ChCode,Description,Unit,Rate FROM TariffCard WHERE ChCode = '" & RdrClientTariff("ChCode") & "'"
                CmdGetClientTariff.CommandText = StrSql
                RdrGetClientTariff = CmdGetClientTariff.ExecuteReader
                While RdrGetClientTariff.Read
                    DDLCharge.Items.Add(RdrGetClientTariff("Description"))
                    DdlRate.Items.Add(RdrGetClientTariff("Rate"))
                    DdlChCode.Items.Add(RdrGetClientTariff("ChCode"))
                End While
                RdrGetClientTariff.Close()
            End While
            RdrClientTariff.Close()
            GetContDetails()
        Catch ex As Exception
            TxtName.Text = "Invalid Client"
        End Try
    End Sub

    Protected Sub TxtBoeNo_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtBoeNo.TextChanged
        Dim StrCont As String
        Dim I As Integer
        Dim ArrCont() As String
        Dim ArrCells() As String
        Dim count As Integer
        If TxtBoeNo.Text = "" Then
            DtpDate.Text = Today.Date
            TxtClient.Focus()
            Exit Sub
        End If
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Cmd.Connection = Con
        CmdScalar.Connection = Con
        Cmd.CommandText = "SELECT * FROM BoeIn WHERE BillNo = '" & TxtBoeNo.Text & "' ORDER BY BillNo"
        Rdr = Cmd.ExecuteReader
        If Not Rdr.Read Then
            ErrHead.Text = "ERROR"
            ErrMsg.Text = "Invalid Inbound Bill"
            ModalPopupExtender1.Show()
            TxtBoeNo.Focus()
        Else
            TxtContNumber.Visible = False
            CmbContainerNo.Visible = True
            On Error Resume Next
            TxtRevision.Text = Rdr("Revision")
            DtpDate.Text = Rdr("BillDate")
            CmbTraffic.Text = Rdr("TrafficOp")
            CmbWHCode.Text = Rdr("WhCode")
            TxtClient.Text = Rdr("ClientID")
            TxtClient_TextChanged(Me, e)
            DtpDate.Text = Today.Date
            DtpPaidTo.Text = Today.Date
            Do While Month(DateAdd(DateInterval.Day, 1, Convert.ToDateTime(DtpPaidTo.Text))) = Month(Today.Date)
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                DtpPaidTo.Text = DateAdd(DateInterval.Day, 1, Convert.ToDateTime(DtpPaidTo.Text))
            Loop
            Rdr.Close()
            Cmd.CommandText = "Select *From NewContDetails Where BoeNo = '" & TxtBoeNo.Text & "'"
            Rdr = Cmd.ExecuteReader
            CmbContainerNo.Items.Clear()
            CmbContainerNo.Items.Add("Select...")
            DdlContType.Items.Add("Select...")
            While Rdr.Read
                CmbContainerNo.Items.Add(Rdr("ContainerNo"))
                DdlContType.Items.Add(Rdr("ContType"))
            End While
            Rdr.Close()

        End If
    End Sub

    Private Sub GdvCont_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvCont.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvCont, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvCont_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvCont.SelectedIndexChanged
        DDLCharge.Text = ChkSpace(GdvCont.SelectedRow.Cells(3).Text)
        DdlRate.Text = ChkSpace(GdvCont.SelectedRow.Cells(4).Text)
        DdlChCode.Text = ChkSpace(GdvCont.SelectedRow.Cells(1).Text)
        TxtContNumber.Text = ChkSpace(GdvCont.SelectedRow.Cells(2).Text)
        TxtContDate.Text = ChkSpace(GdvCont.SelectedRow.Cells(5).Text)
        TxtRate.Text = ChkSpace(GdvCont.SelectedRow.Cells(4).Text)
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Or Txt = "Select..." Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Private Sub GdvCont_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvCont.SelectedIndexChanging
        GdvCont.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub ButAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAdd.Click
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColChCode As DataColumn
        Dim ColContNo As DataColumn
        Dim ColDesc As DataColumn
        Dim ColRate As DataColumn
        Dim ContDate As DataColumn
        dt = New DataTable()
        If CmbContainerNo.SelectedIndex = 0 Then
            Exit Sub
        End If
        For Each Row As GridViewRow In GdvCont.Rows
            If Row.Cells(2).Text = CmbContainerNo.Text Then
                LblError.Visible = True
                LblError.Text = "Container Already Added. Please Select New"
                DDLCharge.SelectedIndex = 0
                DdlRate.SelectedIndex = 0
                DdlChCode.SelectedIndex = 0
                CmbContainerNo.SelectedIndex = -1
                TxtContNumber.Text = ""
                TxtContDate.Text = ""
                TxtRate.Text = ""
                Exit Sub
            End If
        Next

        ColChCode = New DataColumn("ChCode", Type.GetType("System.String"))
        ColContNo = New DataColumn("ContNo", Type.GetType("System.String"))
        ColDesc = New DataColumn("Description", Type.GetType("System.String"))
        ColRate = New DataColumn("Rate", Type.GetType("System.String"))
        ContDate = New DataColumn("ContDate", Type.GetType("System.String"))
        dt.Columns.Add(ColChCode)
        dt.Columns.Add(ColContNo)
        dt.Columns.Add(ColDesc)
        dt.Columns.Add(ColRate)
        dt.Columns.Add(ContDate)
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        For Each Row As GridViewRow In GdvCont.Rows
            dr = dt.NewRow
            dr.Item("ChCode") = Row.Cells(1).Text
            dr.Item("ContNo") = Row.Cells(2).Text
            dr.Item("Description") = Row.Cells(3).Text
            dr.Item("Rate") = Row.Cells(4).Text
            dr.Item("ContDate") = Row.Cells(5).Text
            dt.Rows.Add(dr)
        Next
        dr = dt.NewRow
        dr.Item("ChCode") = DdlChCode.Text
        If TxtBoeNo.Text <> "" Then
            dr.Item("ContNo") = CmbContainerNo.Text
        Else
            dr.Item("ContNo") = TxtContNumber.Text
        End If
        dr.Item("Description") = DDLCharge.Text
        dr.Item("Rate") = DdlRate.Text
        dr.Item("ContDate") = TxtContDate.Text
        dt.Rows.Add(dr)
        GdvCont.DataSource = dt
        GdvCont.DataBind()
        DDLCharge.SelectedIndex = 0
        DdlRate.SelectedIndex = 0
        DdlChCode.SelectedIndex = 0
        CmbContainerNo.SelectedIndex = -1
        TxtContNumber.Text = ""
        TxtContDate.Text = ""
        TxtRate.Text = ""
    End Sub

    Private Sub DDLCharge_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCharge.SelectedIndexChanged
        'On Error Resume Next
        DdlRate.SelectedIndex = Val(DDLCharge.SelectedIndex)
        DdlChCode.SelectedIndex = Val(DDLCharge.SelectedIndex)
        TxtRate.Text = DdlRate.Text
        TxtChCode.Text = DdlChCode.Text
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Dim SngAmount As Single
        SngAmount = 0
        Dim DtpDateIn As Date
        If DtpDate.Text <> "" Then
            DtpDateIn = CDate(Mid(DtpDate.Text, 1, 2) & "/" & Mid(DtpDate.Text, 4, 2) & "/" & Mid(DtpDate.Text, 7, 4))
        End If
        For Each row As GridViewRow In GdvCont.Rows
            If DirectCast(row.FindControl("ChkSelect"), CheckBox).Checked And row.Cells(1).Text = "" Then
                ErrHead.Text = "Error"
                ErrMsg.Text = "Please Select Charge Code for this container!"
                ModalPopupExtender1.Show()
                Exit Sub
            End If
        Next
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            For Each row As GridViewRow In GdvCont.Rows
                Cmd.CommandText = "Delete From Containers Where ContainerNo = '" & row.Cells(2).Text & "' And DateIn = '" & Format(CDate(row.Cells(5).Text), "MM/dd/yyyy") & "' And ClientID = '" & TxtClient.Text & "'"
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Containers(ContainerNo,ChCode,DateIn,WhCode,ClientID,BoeNo,Description,PaidUpTo,TrafficOfficer,Rate)"
                StrSql = StrSql & "VALUES('" & Left(row.Cells(2).Text, 15) & "','"
                StrSql = StrSql & row.Cells(1).Text & "','"
                StrSql = StrSql & Format(CDate(row.Cells(5).Text), "MM/dd/yyyy") & "','"
                StrSql = StrSql & CmbWHCode.Text & "','"
                StrSql = StrSql & TxtClient.Text & "','"
                StrSql = StrSql & TxtBoeNo.Text & "','"
                StrSql = StrSql & row.Cells(3).Text & "',Null,'" & CmbTraffic.Text & "'," & Val(row.Cells(4).Text) & ")"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            Next
            Trans.Commit()
            CmbWHCode.SelectedIndex = 0
            DtpDate.Text = ""
            TxtClient.Text = ""
            CmbTraffic.SelectedIndex = 0
            TxtBoeNo.Text = ""
            TxtRevision.Text = ""
            TxtName.Text = ""
            DdlRate.SelectedIndex = 0
            DDLCharge.SelectedIndex = 0
            DtpPaidTo.Text = ""
            DDLCharge.SelectedIndex = 0
            CmbContainerNo.SelectedIndex = -1
            TxtChCode.Text = ""
            TxtContDate.Text = ""
            TxtContNumber.Text = ""
            TxtRate.Text = ""
            GdvCont.DataSource = Nothing
            GdvCont.DataBind()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Save Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        CmbWHCode.SelectedIndex = 0
        DtpDate.Text = ""
        TxtClient.Text = ""
        CmbTraffic.SelectedIndex = 0
        TxtBoeNo.Text = ""
        TxtRevision.Text = ""
        TxtName.Text = ""
        DdlRate.SelectedIndex = 0
        DDLCharge.SelectedIndex = 0
        DtpPaidTo.Text = ""
        DDLCharge.SelectedIndex = 0
        TxtChCode.Text = ""
        TxtContDate.Text = ""
        TxtContNumber.Text = ""
        CmbContainerNo.SelectedIndex = -1
        TxtRate.Text = ""
        GdvCont.DataSource = Nothing
        GdvCont.DataBind()
    End Sub

    Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDel.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ChType As DataColumn
        Dim Number As DataColumn
        Dim Description As DataColumn
        Dim Rate As DataColumn
        Dim ContDate As DataColumn

        Dim IntContNo As Integer = 0

        dt = New DataTable()
        ChType = New DataColumn("ChCode", Type.GetType("System.String"))
        Number = New DataColumn("ContNo", Type.GetType("System.String"))
        Description = New DataColumn("Description", Type.GetType("System.String"))
        Rate = New DataColumn("Rate", Type.GetType("System.String"))
        ContDate = New DataColumn("ContDate", Type.GetType("System.String"))


        dt.Columns.Add(ChType)
        dt.Columns.Add(Number)
        dt.Columns.Add(Description)
        dt.Columns.Add(Rate)
        dt.Columns.Add(ContDate)

        On Error Resume Next
        For Each row As GridViewRow In GdvCont.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim ChkDel As CheckBox = DirectCast(row.FindControl("ChkSelect"), CheckBox)
                If Not ChkDel.Checked Then
                    dr = dt.NewRow
                    dr.Item("ChCode") = row.Cells(1).Text
                    dr.Item("ContNo") = row.Cells(2).Text
                    dr.Item("Description") = row.Cells(3).Text
                    dr.Item("Rate") = row.Cells(4).Text
                    dr.Item("ContDate") = row.Cells(5).Text
                    dt.Rows.Add(dr)
                Else
                    CmdDelCont.Connection = Con
                    CmdDelCont.CommandText = "Delete From Containers Where ContainerNo = '" & row.Cells(2).Text & "' And DateIn = '" & Format(CDate(row.Cells(5).Text), "MM/dd/yyyy") & "' And ClientID = '" & TxtClient.Text & "'"
                    CmdDelCont.ExecuteNonQuery()
                End If
            End If
        Next
        GdvCont.DataSource = dt
        GdvCont.DataBind()
        DDLCharge.SelectedIndex = 0
        DdlRate.SelectedIndex = 0
        DdlChCode.SelectedIndex = 0
        CmbContainerNo.SelectedIndex = -1
        TxtContNumber.Text = ""
        TxtContDate.Text = ""
        TxtRate.Text = ""
    End Sub

    Private Sub GetContDetails()
        Dim CmdCnt As New OleDb.OleDbCommand
        Dim RdrCnt As OleDb.OleDbDataReader
        CmdCnt.Connection = Con
        If TxtBoeNo.Text <> "" And TxtClient.Text <> "" Then
            CmdCnt.CommandText = "Select *From Containers Where ClientId = '" & TxtClient.Text & "' And BoeNo = '" & TxtBoeNo.Text & "'"
            RdrCnt = CmdCnt.ExecuteReader
            While RdrCnt.Read
                CmbWHCode.Text = RdrCnt("WhCode")
                TxtBoeNo.Text = RdrCnt("BoeNo")
                If Not IsDBNull(RdrCnt("TrafficOfficer")) Then
                    CmbTraffic.Text = RdrCnt("TrafficOfficer")
                End If
            End While
            RdrCnt.Close()
            CmdCnt.CommandText = "Select ChCode,ContainerNo as ContNo,Description,Rate,DateIn As ContDate From Containers Where ClientId = '" & TxtClient.Text & "' And BoeNo = '" & TxtBoeNo.Text & "'"
            RdrCnt = CmdCnt.ExecuteReader
            GdvCont.DataSource = RdrCnt
            GdvCont.DataBind()
        End If
        If TxtBoeNo.Text = "" And TxtClient.Text <> "" Then
            If TxtClient.Text <> "" Then
                CmdCnt.CommandText = "Select * From Containers Where ClientId = '" & TxtClient.Text & "' And DateIn = '" & Format(CDate(DtpDate.Text), "MM/dd/yyyy") & "'"
                RdrCnt = CmdCnt.ExecuteReader
                While RdrCnt.Read
                    CmbWHCode.Text = RdrCnt("WhCode")
                    TxtBoeNo.Text = RdrCnt("BoeNo")
                    If Not IsDBNull(RdrCnt("TrafficOfficer")) Then
                        CmbTraffic.Text = RdrCnt("TrafficOfficer")
                    End If
                End While
                RdrCnt.Close()
                CmdCnt.CommandText = "Select ChCode,ContainerNo as ContNo,Description,Rate,DateIn As ContDate From Containers Where ClientId = '" & TxtClient.Text & "' And DateIn = '" & Format(CDate(DtpDate.Text), "MM/dd/yyyy") & "'"
                RdrCnt = CmdCnt.ExecuteReader
                GdvCont.DataSource = RdrCnt
                GdvCont.DataBind()
            End If
        End If
    End Sub

    Protected Sub DtpDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DtpDate.TextChanged
        LblError.Visible = False
        GetContDetails()
    End Sub

    Protected Sub CmbContainerNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbContainerNo.SelectedIndexChanged
        LblError.Visible = False
        DdlContType.SelectedIndex = Val(CmbContainerNo.SelectedIndex)
        If DdlContType.Text = "20 FT" Or DdlContType.Text = "20ft" Or DdlContType.Text = "20 ft" Then
            DDLCharge.SelectedIndex = 1
            DDLCharge_SelectedIndexChanged(Me, e)
        End If
        If DdlContType.Text = "40 FT" Or DdlContType.Text = "40ft" Or DdlContType.Text = "40 ft" Or DdlContType.Text = "40 FT" Then
            DDLCharge.SelectedIndex = 2
            DDLCharge_SelectedIndexChanged(Me, e)
        End If
    End Sub

    Protected Sub TxtContNumber_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtContNumber.TextChanged
        LblError.Visible = False
    End Sub
End Class