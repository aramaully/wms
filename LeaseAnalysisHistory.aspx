﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LeaseAnalysisHistory.aspx.vb" Inherits="Wms.LeaseAnalysisHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Lease Analysis History</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.

            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Lease Analysis History'; //Set the name

            w.Width = 500; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 500;
            w.Top = 10;
            w.Left = 10;              
            w.Show(); //Show the window
        };
        function ShowReport() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FrmRpt.aspx', w);
            return false;
        };
    </script>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .auto-style3 {
            width: 122px;
        }
        .auto-style7 {
            width: 268px;
        }
        .auto-style8 {
            width: 176px;
        }
    </style>
    </head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px"
                runat="server" Height="32px" Width="32px" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" 
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%" AutoPostBack="True">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="Print" ImageUrl="~/images/print32.png" ToolTip="Print">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 357px; overflow: auto; padding: 2px;
                border-right: solid 1px #cddaea;">
                <table class="style1">
                    <tr>
                        <td class="auto-style8">&nbsp;</td>
                        <td class="auto-style7">&nbsp;</td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style8">Freeport Zone</td>
                        <td class="auto-style7">
                            <asp:DropDownList ID="CmbZone" runat="server" Width="150px" AutoPostBack="True"></asp:DropDownList>
                        </td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style8">Type of Property</td>
                        <td class="auto-style7">
                            <asp:DropDownList ID="CmbWarehouse" runat="server" Width="300px" AutoPostBack="True"></asp:DropDownList>
                        </td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style8">Customer</td>
                        <td class="auto-style7">
                            <asp:DropDownList ID="cmbCustomer" runat="server" Width="300px"></asp:DropDownList>
                        </td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style8">Status</td>
                        <td class="auto-style7">
                            <asp:DropDownList ID="cmbStatus" runat="server" Width="300px" AutoPostBack="True"></asp:DropDownList>
                        </td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style8">Category</td>
                        <td class="auto-style7">
                            <asp:DropDownList ID="CmbCategory" runat="server" AutoPostBack="True" Width="300px">
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>         
                    <tr>
                        <td class="auto-style8">&nbsp;</td>
                        <td class="auto-style7">&nbsp;</td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style8">Start Date<br />
                            <asp:TextBox ID="DtpStartDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="DtpStartDate_CalendarExtender" runat="server" Enabled="True" Format="dd/MM/yyyy" TargetControlID="DtpStartDate">
                            </cc1:CalendarExtender>
                            <br />
                        </td>
                        <td class="auto-style7">End Date<br />
                            <asp:TextBox ID="DtpEndDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="DtpEndDate_CalendarExtender" runat="server" Enabled="True" Format="dd/MM/yyyy" TargetControlID="DtpEndDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style8">&nbsp;</td>
                        <td class="auto-style7">
                            &nbsp;</td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style8">Move In Date<br />
                            <asp:TextBox ID="DtpMoveInDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="DtpMoveInDate_CalendarExtender" runat="server" Enabled="True" Format="dd/MM/yyyy" TargetControlID="DtpMoveInDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="auto-style7">Move Out Date<br />
                            <asp:TextBox ID="DtpMoveOutDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="DtpMoveOutDate_CalendarExtender" runat="server" Enabled="True" Format="dd/MM/yyyy" TargetControlID="DtpMoveOutDate">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style8">&nbsp;</td>
                        <td class="auto-style7">&nbsp;</td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style8">&nbsp;</td>
                        <td class="auto-style7">&nbsp;</td>
                        <td class="auto-style3">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
            panelProg.style.display = '';}
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>
</body>
</html>


