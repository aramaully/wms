﻿Partial Public Class FrmClientMovement
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdZone As New OleDb.OleDbCommand
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "ClientMovement"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            CmdZone.Connection = Con
            CmdZone.CommandText = "Select WhCode From Warehouse"
            Rdr = CmdZone.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            CmbType.Items.Clear()
            CmbType.Items.Add("MoveIn")
            CmbType.Items.Add("MoveOut")
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Dim StrSelFormula As String
            Dim StrFrom As String = Format(CDate(TxtDateFrom.Text), "yyyy,MM,dd")
            Dim StrTo As String = Format(CDate(TxtDateTo.Text), "yyyy,MM,dd")

            If CmbType.Text = "MoveIn" Then
                StrSelFormula = "{LeaseAgreement.MoveInDate}>=date(" & StrFrom & ") And {LeaseAgreement.MoveInDate}<=date(" & StrTo & ") "
                Session("ReportFile") = "RptClientMovement.rpt"
            Else
                StrSelFormula = "{LeaseAgreement.MoveOutDate}>=date(" & StrFrom & ") And {LeaseAgreement.MoveOutDate}<=date(" & StrTo & ") "
                Session("ReportFile") = "RptClientMovementOut.rpt"
            End If
            If CmbZone.Text <> "Select..." Then
                StrSelFormula = StrSelFormula & " And {LeaseAgreement.Zone}= '" & CmbZone.Text & "'"
            End If
            Session("Zone") = ""
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Client Move In From Period " & TxtDateFrom.Text & " to " & TxtDateTo.Text
            Session("Period") = "'(" & Format(CDate(TxtDateFrom.Text), "dd/MM/yy") & " To " & Format(CDate(TxtDateTo.Text), "dd/MM/yy") & ")'"

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub

End Class