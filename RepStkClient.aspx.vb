﻿Public Partial Class RepStkClient
    Inherits System.Web.UI.Page

    Dim Con As New OleDb.OleDbConnection
    Dim CmdClient As New OleDb.OleDbCommand
    Dim RdrClient As OleDb.OleDbDataReader
    Dim CmdBoe As New OleDb.OleDbCommand
    Dim RdrBoe As OleDb.OleDbDataReader
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrSel As String

    Const MENUID = "StockByClient"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Cmd.Connection = Con
        'Check Access
        StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar = 0 Then
            Response.Redirect("about:blank")
        End If
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Try
            TxtClient.Text = Cmd.ExecuteScalar.ToString
        Catch ex As Exception
            TxtClient.Text = "Invalid Client"
        End Try
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        Dim StrSelFormula As String
        StrSelFormula = "{PaltDetail.BalQuantity} > 0 "
        If TxtClientID.Text <> "ALL" Then
            StrSelFormula = StrSelFormula & "And {Pallets.ClientID} = '" & TxtClientID.Text & "'"
        End If
        If Session("PstrZone").ToString <> "ALL" Then
            If Len(StrSelFormula) > 0 Then
                StrSelFormula = StrSelFormula & " And "
            End If
            StrSelFormula = StrSelFormula & "{Boein.WhCode} = '" & Session("PstrZone").ToString & "'"
        End If
        Session("ReportFile") = "RptStockClient.rpt"
        Session("Period") = ""
        Session("Zone") = ""
        Session("SelFormula") = StrSelFormula
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        'Response.Redirect("Report.aspx?RptID=Stock By Client&RPTFileName=RepStkClient.aspx&RptFile=RptStockClient.rpt&StrSelF=" & StrSelFormula)


    End Sub

    Protected Sub BtnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnFind.Click
        Session.Add("PstrCode", TxtClientID.Text)
        ScriptManager.RegisterStartupScript(BtnFind, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
    End Sub
End Class