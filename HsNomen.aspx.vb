﻿Public Partial Class HsNomen
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatNc As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            HsNcBind()
        End If
    End Sub

    Private Sub HsNcBind()
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT * FROM HSNomen ORDER BY HSNc"
        Rdr = Cmd.ExecuteReader
        DatNc.Load(Rdr)
        Rdr.Close()
        GdvHsNc.DataSource = DatNc
        GdvHsNc.DataBind()
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvHsNc.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvHsNc.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub GdvHsNc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvHsNc.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvHsNc, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvHsNc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvHsNc.SelectedIndexChanged
        TxtNomen.Text = GdvHsNc.SelectedRow.Cells(0).Text
        TxtDescription.Text = GdvHsNc.SelectedRow.Cells(1).Text
    End Sub

    Private Sub GdvHsNc_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvHsNc.SelectedIndexChanging
        GdvHsNc.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtNomen.Text = ""
            TxtDescription.Text = ""
            TxtNomen.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Try
                Cmd.CommandText = "DELETE FROM HSNomen WHERE HSNc = '" & TxtNomen.Text & "'"
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO HSNomen (HSNc,Description) VALUES('" & TxtNomen.Text & "','" & TxtDescription.Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                Trans.Commit()
                TxtNomen.Text = ""
                TxtDescription.Text = ""
                HsNcBind()
            Catch ex As Exception
                Trans.Rollback()
                ErrHead.Text = "Update Failed"
                ErrMsg.Text = ex.Message
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM HSNomen WHERE HSNc = '" & TxtNomen.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtNomen.Text = ""
                TxtDescription.Text = ""
                HsNcBind()
            Catch ex As Exception
                ErrHead.Text = "Delete Failed"
                ErrMsg.Text = ex.Message
            End Try
        End If
    End Sub
End Class