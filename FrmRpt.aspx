<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmRpt.aspx.vb" Inherits="Wms.FrmRpt" ClientTarget="uplevel" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<html>
<head id="Head1" runat="server">
    <title>View Report</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.

            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = document.getElementById("RepHead").value; //Set the name
            w.CenterScreen();
            w.Width = 800; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 600;
            w.Top = 10;
            w.Left = 50;
            w.Show(); //Show the window
        };
        function ShowFind() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FindBank.aspx', w);
            return false;
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '1';
                w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Delete") {
                Index = '3';
                w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
    </script>

    </head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <asp:Button ID="btnPrint" runat="server" Visible ="True" Text="Print Directly" OnClientClick="Print()"></asp:Button>
        <asp:Panel ID="Panel1" Style="z-index: 101; left:100px; position: absolute;"
                runat="server" Height="530px" Width="1200px" ScrollBars="Both" BorderStyle="None" Visible="True">
                <div id="div1" style="display: none">
                    <asp:Image ID="Image1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px"
                runat="server" Height="32px" Width="32px" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
                <asp:HiddenField runat="server" ID="RepHead" />
            <div id="dvReport">
            <CR:CrystalReportViewer ID="CrystalViewer" runat="server" SeparatePages ="False" ToolPanelView = "None"
                AutoDataBind="true" BestFitPage="False" 
                    Height="1000px" Width="1200px" HasCrystalLogo="False" 
                ReuseParameterValuesOnRefresh="true"  
                CssFilename="default.css" />
            <CR:CrystalReportSource ID="RptSource" runat="server">
            </CR:CrystalReportSource></div>
       </asp:Panel>
    </form>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
                panelProg.style.display = '';
            }
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
        function Print() {
            var dvReport = document.getElementById("dvReport");
            var frame1 = dvReport.getElementsByTagName("iframe")[0];
            if (navigator.appName.indexOf("Internet Explorer") != -1) {
                frame1.name = frame1.id;
                window.frames[frame1.id].focus();
                window.frames[frame1.id].print();
            }
            else {
                var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
                frameDoc.print();
            }
        };

        function Print() {
            var dvReport = document.getElementById("dvReport");
            var frame1 = dvReport.getElementsByTagName("iframe")[0];
            if (navigator.appName.indexOf("Internet Explorer") != -1 || navigator.appVersion.indexOf("Trident") != -1) {
                frame1.name = frame1.id;
                window.frames[frame1.id].focus();
                window.frames[frame1.id].print();
            } else {
                var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
                frameDoc.print();
            }
        };
    </script>
</body>
</html>