﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmAccess.aspx.vb" Inherits="Wms.FrmAccess" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>User Profiles</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'User Profiles'; //Set the name
            w.Width = 570; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 500;
            w.Top = 10;
            w.Left = 10;              
            w.Show(); //Show the window
        };
        function ShowFind() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FindBank.aspx', w);
            return false;
        };
        function FunctionConfirm(Tlb,TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                var validated = Page_ClientValidate('Group1');
                if (validated) {
                    Index = '1'
                    w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
                }
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "SelectAll") {
            __doPostBack('ToolBar1', '3');
            }
            else if (TlbCmd == "ClearAll") {
                __doPostBack('ToolBar1', '4');
            }
        }
        function SaveFunction(Sender,RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }

        function DataPrint() {
            var prtContent = document.getElementById('<%= GdvMenu.ClientID %>');
            prtContent.border = 0; //set no border here
            var Print = window.open('', '', 'left=100,top=100,width=1000,height=1000,toolbar=0,scrollbars=1,status=0,resizable=1');
            Print.document.write(prtContent.outerHTML);
            Print.document.close();
            Print.focus();
            Print.print();
            Print.close();
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 0;
        }
        .style3
        {
            width: 14px;
        }
    </style>

</head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px"
                runat="server" Height="32px" Width="32px" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" ClientSideOnItemClick="FunctionConfirm"
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="New" ImageUrl="00101001" ToolTip="New">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Save" ImageUrl="00101003" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="SelectAll" ImageUrl="~/images/taskhs.png" ToolTip="Select All">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="ClearAll" ImageUrl="~/images/delete.png" ToolTip="Clear All">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
            <div id="Content" style="width: 100%; height: 100%; overflow: auto; padding: 2px;
                border-right: solid 1px #cddaea;">
                <table class="style1" width="100%" border ="0">
                    <tr>
                        <td>Profile</td>
                        <td class="style3">
                            <asp:DropDownList ID="CmbUserID" runat="server" AutoPostBack="True" 
                                Width="150px" ValidationGroup="Group1">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:BFSLAPPSERVER_WMS_UAT %>" SelectCommand="SELECT * FROM [Profil]"></asp:SqlDataSource>
                        </td>
                        <td class="style3">
                            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                ControlToValidate="CmbUserID" ErrorMessage="Select User" Operator="NotEqual" 
                                ValidationGroup="Group1" ValueToCompare="Select...">*</asp:CompareValidator>
                        </td>
                        <td class="style2" valign="middle">&nbsp;</td>
                    </tr>
                </table>
                <asp:Panel ID="Panel8" runat="server" Height="440px" ScrollBars="Vertical" Width="100%"
                    BorderStyle="Inset" BorderWidth="1px">
                    <asp:GridView ID="GdvMenu" runat="server" Width="100%" 
                        AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Menu ID" >
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Caption" HeaderText="Description" >
                                <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Allowed" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle Font-Size="Smaller" ForeColor="White" Wrap="False" />
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="ChkSel" />
                            </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="#0080C0" />
                        <AlternatingRowStyle BackColor="#CAE4FF" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
            panelProg.style.display = '';}
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>
</body>
</html>

