﻿Public Partial Class WHouse
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatLoc As New DataTable
    Dim StrCondition As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            BindWarehouse()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Private Sub GdvWarehouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvWHouse.SelectedIndexChanged
        On Error Resume Next
        TxtWhCode.Text = ChkSpace(GdvWHouse.SelectedRow.Cells(0).Text)
        TxtName.Text = ChkSpace(GdvWHouse.SelectedRow.Cells(1).Text)
    End Sub

    Private Sub BindWarehouse()
        Cmd.Connection = Con
        StrSql = "SELECT WhCode,Name FROM Warehouse"
        StrSql = StrSql & StrCondition & " ORDER BY WhCode"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar Is Nothing Then Exit Sub
        Rdr = Cmd.ExecuteReader
        DatLoc.Load(Rdr)
        Rdr.Close()
        GdvWHouse.DataSource = DatLoc
        GdvWHouse.DataBind()
        If GdvWHouse.Rows.Count = 1 Then
            On Error Resume Next
            TxtWhCode.Text = ChkSpace(GdvWHouse.Rows(0).Cells(0).Text)
            TxtName.Text = ChkSpace(GdvWHouse.Rows(0).Cells(1).Text)
        End If
    End Sub

    Private Sub GdvWarehouse_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvWHouse.PageIndexChanging
        GdvWHouse.PageIndex = e.NewPageIndex
        BindWarehouse()
    End Sub

    Private Sub GdvWarehouse_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvWHouse.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvWHouse, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvWHouse.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvWHouse.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub GdvWarehouse_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvWHouse.SelectedIndexChanging
        GdvWHouse.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub TxtWhCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtWhCode.TextChanged
        If TxtWhCode.Text <> "" Then
            StrCondition = " WHERE WhCode LIKE '%" & TxtWhCode.Text & "%'"
        Else
            StrCondition = ""
        End If
        TxtName.Text = ""
        BindWarehouse()
    End Sub

    Private Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        On Error Resume Next
        TxtWhCode.Text = ""
        TxtName.Text = ""
        TxtWhCode.Focus()
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Try
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Warehouse WHERE WhCode = '" & TxtWhCode.Text & "'"
            Cmd.ExecuteNonQuery()
            ButNew_Click(Me, e)
            BindWarehouse()
        Catch ex As Exception
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Cmd.CommandText = "SELECT WhCode FROM Warehouse WHERE WhCode = '" & TxtWhCode.Text & "'"
        Try
            If Cmd.ExecuteScalar Is Nothing Then
                StrSql = "INSERT INTO Warehouse (WhCode,Name) "
                StrSql = StrSql & "VALUES('" & TxtWhCode.Text & "','"
                StrSql = StrSql & TxtName.Text & "')"
            Else
                StrSql = "UPDATE Warehouse SET WhCode = '" & TxtWhCode.Text & "',"
                StrSql = StrSql & "Name='" & TxtName.Text & "' WHERE WhCode = '" & TxtWhCode.Text & "'"
            End If
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ButNew_Click(Me, e)
            BindWarehouse()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class