﻿Partial Public Class OpClients
    Inherits System.Web.UI.Page
    Dim StrSql As String
    Dim Cmd As New OleDb.OleDbCommand
    Dim Con As New OleDb.OleDbConnection
    Dim Rdr As OleDb.OleDbDataReader

    Const MENUID = "OpClients"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                If Rdr("WhCode") <> "FPZ6SD" Then
                    CmbZone.Items.Add(Rdr("WhCode"))
                End If
            End While
            Rdr.Close()
            CmbStatus.Items.Add("ALL")
            CmbStatus.Items.Add("Operational")
            CmbStatus.Items.Add("Non Operational & Dormant")
        End If
    End Sub

    Protected Sub ToolbarOpClients_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolbarOpClients.ItemClick
        If e.Item.CommandName = "Print" Then
            Dim StrSelFormula As String
            Dim StrZoneSelection As String = ""
            Session("ReportFile") = "RptOpClients.rpt"
            StrSelFormula = "{Client.ClientID}<>'' "

            'Condition Client.Status
            If CmbStatus.Text = "Operational" Then
                StrSelFormula = StrSelFormula & "And {Client.Status}='Operational' "
            ElseIf CmbStatus.Text = "Non Operational & Dormant" Then
                StrSelFormula = StrSelFormula & "And {Client.Status} in [""Non Operational"", ""Dormant""] "
            End If

            'Condition Client.FPZones
            If CmbZone.Text <> "Select..." Then
                Dim StrZoneCode As Char = CmbZone.Text.Last
                Dim arrListZones As New ArrayList
                Select Case StrZoneCode
                    Case "1"
                        StrZoneSelection = "in [" & 1 & "," & 7 & "," & 10 & "," & 16 & "]"
                    Case "6"
                        StrZoneSelection = "in [" & 6 & "," & 7 & "," & 15 & "," & 16 & "]"
                    Case "9"
                        StrZoneSelection = "in [" & 9 & "," & 10 & "," & 15 & "," & 16 & "]"
                    Case Else
                        System.Diagnostics.Debug.WriteLine("NOT 1, 6, 9")
                End Select
                StrSelFormula = StrSelFormula & "And {Client.FPZones}" & StrZoneSelection
            End If
            Session("Period") = ""
            Session("Zone") = ""
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Operational Clients"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub
End Class