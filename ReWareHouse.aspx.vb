﻿Public Partial Class ReWareHouse
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "ReWarehousing"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtOldBillIn.Focus()
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT CPCCode FROM Regime ORDER BY CpcCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbCpcCode.Items.Add("Select...")
            While Rdr.Read
                CmbCpcCode.Items.Add(Rdr("CPCCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT BoeIn.BillNo, CONVERT(Char,BoeIn.BillDate,103) 'Bill Date', BoeIn.Revision, Client.Name FROM BoeIn, Client WHERE "
        StrSql = StrSql & "BoeIn.ClientID = Client.ClientID "
        If RbnClient.Checked Then
            StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCountry.Checked Then
            StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnTraffic.Checked Then
            StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCont.Checked Then
            StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
        End If
        If ChkDate.Checked Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = StrSql & "AND Boein.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND Boein.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        GdvFind.DataSource = Nothing
        TxtTo.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtFrom.Text = Date.Today.AddDays(-10).ToString("dd/MM/yyyy")
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtOldBillIn.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtOldBillIn_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtOldBillIn_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtOldBillIn.TextChanged
        ClearAll()
        StrSql = "SELECT BoeIn.BillNo, BoeIn.BillDate, BoeIn.TransType, Client.Name FROM BoeIn, Client WHERE "
        StrSql = StrSql & "BoeIn.ClientID = Client.ClientID AND BillNo = '" & TxtOldBillIn.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            On Error Resume Next
            TxtClient.Text = Rdr("Name").ToString
            TxtBillDate.Text = Format(Rdr("BillDate"), "dd/MM/yyyy")
            TxtOldCpcCode.Text = Rdr("TransType").ToString
            TxtNewBillIn.Focus()
        End If
        Rdr.Close()
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT BillNo FROM BoeIn WHERE BillNo = '" & TxtNewBillIn.Text & "'"
        If Not Cmd.ExecuteScalar Is Nothing Then
            ErrHead.Text = "Duplicate Inbound"
            ErrMsg.Text = "This Inbound Bill No is already in the Database. Cannot Duplicate!"
            ModalPopupExtender1.Show()
            Exit Sub
        End If
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Try
            Cmd.CommandText = "UPDATE Pallets SET BoeNo='" & TxtNewBillIn.Text & "',RenewalDate='" & CDate(TxtDate.Text).ToString("dd-MMM-yyyy") & "' WHERE BoeNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE BoeIn SET TransType='" & CmbCpcCode.Text & "', BillNo='" & TxtNewBillIn.Text & "', Remarks='" & TxtRemarks.Text & "' WHERE BillNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE BoeInItem SET BoeNo='" & TxtNewBillIn.Text & "' WHERE BoeNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE BoeInProd SET BoeNo='" & TxtNewBillIn.Text & "' WHERE BoeNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE BoeOut SET BillInNo='" & TxtNewBillIn.Text & "' WHERE BillInNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE BoeOutProd SET BoeInNo='" & TxtNewBillIn.Text & "' WHERE BoeInNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE BoeOutItem SET BoeInNo='" & TxtNewBillIn.Text & "' WHERE BoeInNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE Containers SET BoeNo='" & TxtNewBillIn.Text & "' WHERE BoeNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE Invoice SET BoeNo='" & TxtNewBillIn.Text & "' WHERE BoeNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE Logistics SET BoeNo='" & TxtNewBillIn.Text & "' WHERE BoeNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE Occupancy SET BoeNo='" & TxtNewBillIn.Text & "' WHERE BoeNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE PalletSold SET BoeNo='" & TxtNewBillIn.Text & "' WHERE BoeNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE PaltDetail SET BoeNo='" & TxtNewBillIn.Text & "' WHERE BoeNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "UPDATE PhTally SET BoeNo='" & TxtNewBillIn.Text & "' WHERE BoeNo='" & TxtOldBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ClearAll()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Error Re-Warehousing"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ClearAll()
        TxtBillDate.Text = ""
        TxtDate.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtOldCpcCode.Text = ""
        TxtClient.Text = ""
        CmbCpcCode.SelectedIndex = 0
        TxtNewBillIn.Text = ""
        TxtRemarks.Text = ""
    End Sub
End Class