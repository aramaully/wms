﻿Public Partial Class PalletTag
    Inherits System.Web.UI.Page
    Dim CmdBoe As New OleDb.OleDbCommand
    Dim RdrBoe As OleDb.OleDbDataReader
    Dim CmdPallet As New OleDb.OleDbCommand
    Dim RdrPallet As OleDb.OleDbDataReader
    Dim CmdClient As New OleDb.OleDbCommand
    Dim RdrClient As OleDb.OleDbDataReader
    Dim Con As New OleDb.OleDbConnection
    Dim StrSql As String
    Dim StrSel As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub TxtBoeNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBoeNo.TextChanged
        StrSql = "SELECT * FROM BoeIn WHERE BillNo='" & TxtBoeNo.Text & "'"
        CmdBoe.Connection = Con
        CmdBoe.CommandText = StrSql
        RdrBoe = CmdBoe.ExecuteReader
        If RdrBoe.Read Then
            TxtBoeOut.Text = ""
            StrSql = "SELECT * FROM Pallets WHERE BoeNo = '" & TxtBoeNo.Text & "' ORDER BY PalletID"
            CmdPallet.Connection = Con
            CmdPallet.CommandText = StrSql
            RdrPallet = CmdPallet.ExecuteReader
            If RdrPallet.Read Then
                TxtPIDFrom.Text = RdrPallet("PalletID")
            End If
            RdrPallet.Close()
            StrSql = "SELECT MAX(PalletID) AS MPID FROM Pallets WHERE BoeNo = '" & TxtBoeNo.Text & "'"
            CmdPallet.Connection = Con
            CmdPallet.CommandText = StrSql
            RdrPallet = CmdPallet.ExecuteReader
            If RdrPallet.Read Then
                TxtPIDTo.Text = RdrPallet("MPID")
            End If
            RdrPallet.Close()
            TxtDate.Text = Format(RdrBoe("BillDate"), "dd/MM/yyyy")
            StrSql = "SELECT * FROM Client WHERE ClientID='" & RdrBoe("ClientID") & "'"
            CmdClient.Connection = Con
            CmdClient.CommandText = StrSql
            RdrClient = CmdClient.ExecuteReader
            If RdrClient.Read Then
                TxtName.Text = RdrClient("Name")
                TxtAddress.Text = RdrClient("Address")
            End If
            RdrClient.Close()
        End If
        RdrBoe.Close()
       End Sub

    Private Sub TxtBoeOut_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBoeOut.TextChanged
        StrSql = "SELECT * FROM BoeOut WHERE BillNo='" & TxtBoeOut.Text & "'"
        CmdBoe.Connection = Con
        CmdBoe.CommandText = StrSql
        RdrBoe = CmdBoe.ExecuteReader
        If RdrBoe.Read Then
            TxtBoeNo.Text = ""
            StrSql = "SELECT * FROM Pallets WHERE BoeNo = '" & TxtBoeOut.Text & "' ORDER BY PalletID"
            CmdPallet.Connection = Con
            CmdPallet.CommandText = StrSql
            RdrPallet = CmdPallet.ExecuteReader
            If RdrPallet.Read Then
                TxtPIDFrom.Text = RdrPallet("PalletID")
            End If
            RdrPallet.Close()
            StrSql = "SELECT MAX(PalletID) AS MPID FROM Pallets WHERE BoeNo = '" & TxtBoeOut.Text & "'"
            CmdPallet.Connection = Con
            CmdPallet.CommandText = StrSql
            RdrPallet = CmdPallet.ExecuteReader
            If RdrPallet.Read Then
                TxtPIDTo.Text = RdrPallet("MPID")
            End If
            RdrPallet.Close()
            TxtDate.Text = Format(RdrBoe("BillDate"), "dd/MM/yyyy")
            StrSql = "SELECT * FROM Client WHERE ClientID='" & RdrBoe("ClientID") & "'"
            CmdClient.Connection = Con
            CmdClient.CommandText = StrSql
            RdrClient = CmdClient.ExecuteReader
            If RdrClient.Read Then
                TxtName.Text = RdrClient("Name")
                TxtAddress.Text = RdrClient("Address")
            End If
            RdrClient.Close()
        End If
        RdrBoe.Close()
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        If TxtBoeNo.Text <> "" Then
            StrSel = "{BoeIn.BillNo} = '" & TxtBoeNo.Text & "'"
            If TxtLot.Text <> "ALL" Then
                StrSel = StrSel & " And {Pallets.LotNo} = " & TxtLot.Text
            End If
            StrSel = StrSel & "And {Pallets.PalletID} >= " & TxtPIDFrom.Text
            StrSel = StrSel & "And {Pallets.PalletID} <= " & TxtPIDTo.Text
        End If
        If TxtBoeOut.Text <> "" Then
            StrSel = "{BoeIn.BillNo} ='" & TxtBoeOut.Text & "'"
            If TxtLot.Text <> "ALL" Then
                StrSel = StrSel & " And {Pallets.LotNo} = " & TxtLot.Text
            End If
            StrSel = StrSel & "And {Pallets.PalletID} >= " & TxtPIDFrom.Text
            StrSel = StrSel & "And {Pallets.PalletID} <= " & TxtPIDTo.Text
        End If
        If OptOne.Checked Then
            Response.Redirect("Report.aspx?RptID=Print Pallet Tag&RPTFileName=PalletTag.aspx&RptFile=RptPalletTag.rpt&StrSelF=" & StrSel)
        Else
            Response.Redirect("Report.aspx?RptID=Print Pallet Tag&RPTFileName=PalletTag.aspx&RptFile=RptPalletTag2.rpt&StrSelF=" & StrSel)
        End If
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        If RbnClient.Text = "Name" Then
            StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
            If RbnClient.Checked Then
                StrSql = StrSql & "Name"
            ElseIf RbnCountry.Checked Then
                StrSql = StrSql & "City"
            Else
                StrSql = StrSql & "Contact"
            End If
            StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Else
            StrSql = "SELECT BoeIn.BillNo, CONVERT(Char,BoeIn.BillDate,103) 'Bill Date', BoeIn.Revision, Client.Name FROM BoeIn, Client WHERE "
            StrSql = StrSql & "BoeIn.ClientID = Client.ClientID "
            If RbnClient.Checked Then
                StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCountry.Checked Then
                StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnTraffic.Checked Then
                StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCont.Checked Then
                StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
            End If
            If ChkDate.Checked Then
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                StrSql = StrSql & "AND Boein.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
                StrSql = StrSql & "AND Boein.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
            End If
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        If RbnClient.Text = "Client" Then
            TxtBoeNo.Text = GdvFind.SelectedRow.Cells(0).Text
            TxtBoeNo_TextChanged(Me, e)
        End If
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)

        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub BtnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnFind.Click
        RbnClient.Text = "Client"
        RbnCountry.Text = "Country"
        RbnTraffic.Text = "Traffic Op"
        RbnCont.Visible = True
        ChkDate.Visible = True
        TxtFrom.Visible = True
        TxtTo.Visible = True
        LblToDate.Visible = True
        GdvFind.DataSource = Nothing
        TxtTo.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtFrom.Text = Date.Today.AddDays(-10).ToString("dd/MM/yyyy")
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub
End Class