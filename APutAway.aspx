﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="APutAway.aspx.vb" Inherits="Wms.APutAway" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Automatic Put Away</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.
            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Automatic Put Away'; //Set the name
            w.Top = 10;
            w.Left = 10;
            w.Width = 909; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 540;
            w.Show(); //Show the window
        };
        function ShowFind() {
            var FindWindow = window.open("FindEmployee.aspx", "FindEmployee", "width=565,height=435,top=200, left=575");
        };
        function FunctionConfirm(Tlb, TlbItem) {
            var TlbCmd = TlbItem.getCommandName();
            if (TlbCmd == "Save") {
                Index = '1';
                w.Confirm("Sure to Save", "Confirm", null, SaveFunction);
            }
            else if (TlbCmd == "New") {
                __doPostBack('ToolBar1', '0');
            }
            else if (TlbCmd == "Delete") {
                Index = '3';
                w.Confirm("Sure to Delete", "Confirm", null, SaveFunction);
            }
        }
        function SaveFunction(Sender, RetVal) {
            if (RetVal) {
                __doPostBack('ToolBar1', Index);
            }
        }
        function FunPic() {
            var myArgs = 'nothing';
            myargs = window.showModalDialog('Snap.aspx', myArgs, "dialogHeight:350px;dialogWidth:650px");
            if (myArgs == 'nothing') {
                PageMethods.AjaxSetSession("True");
            }
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 236px;
        }
        </style>

    </head>
<body onload="init();">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <div id="Content" style="width: 100%; height: 100%; padding: 2px; border-right: solid 1px #cddaea;">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Automatic Putaway"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButShowLoc" runat="server" Height="39px" Text="Show" Style="cursor: pointer"
                                    ToolTip="Create New Charges" Width="55px" CausesValidation="False" 
                                    UseSubmitBehavior="False" />
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Charges File" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Logistic Charges?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Charges" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete Charges?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    
                    <table cellpadding="0" class="style1">
                        <tr>
                            <td>
                                Inbound</td>
                            <td>
                                <asp:TextBox ID="TxtBillNo" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Revision</td>
                            <td>
                                <asp:TextBox ID="TxtRevision" runat="server" Width="50px"></asp:TextBox>
                            </td>
                            <td>
                                Date</td>
                            <td>
                                <asp:TextBox ID="TxtDate" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Client</td>
                            <td colspan="3">
                                <asp:TextBox ID="TxtName" runat="server" Width="99%"></asp:TextBox>
                            </td>
                            <td>
                                Date In</td>
                            <td>
                                <asp:TextBox ID="TxtDateIn" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                FP Zone</td>
                            <td>
                                <asp:DropDownList ID="CmbFpZone" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                From</td>
                            <td align="left">
                                To</td>
                        </tr>
                        <tr>
                            <td>
                                Zone</td>
                            <td>
                                <asp:DropDownList ID="CmbZone" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Pallet ID</td>
                            <td>
                                <asp:TextBox ID="TxtPalletFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtPalletTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="ChkRack" runat="server" AutoPostBack="True" Checked="True" 
                                    Text="Rack    " TextAlign="Left" />
                            </td>
                            <td>
                                <asp:DropDownList ID="CmbRack" runat="server" Enabled="False">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Rack</td>
                            <td>
                                <asp:TextBox ID="TxtRackFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtRackTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Lot No From</td>
                            <td valign="middle">
                                <asp:TextBox ID="TxtLotFrom" runat="server" Width="50px"></asp:TextBox>
                                To
                                <asp:TextBox ID="TxtLotTo" runat="server" Width="50px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Aisle</td>
                            <td>
                                <asp:TextBox ID="TxtAisleFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtAisleTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                P Code From</td>
                            <td>
                                <asp:DropDownList ID="CmbPCodeFrom" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Side</td>
                            <td>
                                <asp:TextBox ID="TxtSideFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtSideTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                P Code To</td>
                            <td>
                                <asp:DropDownList ID="CmbPcodeTo" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Bay</td>
                            <td>
                                <asp:TextBox ID="TxtBayFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtBayTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="2">
                                <asp:TextBox ID="TxtPCode" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                Level</td>
                            <td>
                                <asp:TextBox ID="TxtlevelFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtLevelTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                Location</td>
                            <td>
                                <asp:TextBox ID="TxtLocationFrom" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtLocationTo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="Panel2" runat="server" Height="155px" ScrollBars="Auto">
                    
                        <asp:GridView ID="GdvPallet" runat="server" AutoGenerateColumns="False" 
                            Width="100%">
                            <Columns>
                                <asp:BoundField DataField="PalletID" HeaderText="Pallet ID" />
                                <asp:BoundField DataField="PRatio" HeaderText="P Ratio" 
                                    DataFormatString="{0:N1}" >
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="LocCode1" HeaderText="Location 1" 
                                    HtmlEncode="False" />
                                <asp:BoundField DataField="PRatio1" HeaderText="Ratio 1" 
                                    DataFormatString="{0:N1}" HtmlEncode="False" >
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="LocCode2" HeaderText="Location 2" 
                                    HtmlEncode="False" />
                                <asp:BoundField DataField="PRatio2" HeaderText="Ratio2" 
                                    DataFormatString="{0:N1}" HtmlEncode="False" >
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="LocCode3" HeaderText="Location 3" 
                                    HtmlEncode="False" />
                                <asp:BoundField DataField="PRatio3" HeaderText="Ratio 3" 
                                    DataFormatString="{0:N1}" HtmlEncode="False" >
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DateIn" DataFormatString="{0:dd-MMM-yyyy}" 
                                    HeaderText="Date In" />
                                <asp:BoundField DataField="ZoneCode" HeaderText="Zone Code" />
                                <asp:BoundField DataField="LotNo" HeaderText="Lot No" />
                            </Columns>
                            <HeaderStyle BackColor="#0066FF" ForeColor="White" />
                            <AlternatingRowStyle BackColor="#99CCFF" />
                        </asp:GridView>
                    
                    </asp:Panel>
                </div>
                 <asp:Panel ID="PnlFind" Style="z-index: 101; display:none; left: 100px; position: absolute; top: 60px"
                        runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                        Visible="True" BackColor="White">
                       <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                            <tr>
                                <td  bgcolor="#004080" style="width: 25%">
                                    <asp:RadioButton ID="RbnClient" runat="server" Text="Client" Checked="true"
                                        GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnCountry" runat="server" Text="Country" Checked="false" 
                                        GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnTraffic" runat="server" Text="Traffic Op" 
                                        Checked="false" GroupName="Select" ForeColor="White" />
                                    <br />
                                    <asp:RadioButton ID="RbnCont" runat="server" Text="Container" Checked="false" 
                                        GroupName="Select" ForeColor="White" />
                                </td>
                                <td bgcolor="#004080" style="width:65%" valign="top">
                                    <asp:Label ID="LblSearch" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Enter the text here to search"></asp:Label> <br />
                                    <asp:TextBox ID="TxtSearch" runat="server" BackColor="White" Width="100%"></asp:TextBox>
                                    <br /> <br />
                                    <asp:CheckBox ID="ChkDate" runat="server" Text="Date Range From " 
                                        ForeColor="White"/>                                    
                                    <asp:TextBox ID="TxtFrom" runat="server" Width="100px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtFrom" Format="dd/MM/yyyy">
                                    </cc1:CalendarExtender>
                                    <asp:Label ID="LblToDate" runat="server" Text=" To " ForeColor="White"></asp:Label>
                                    <asp:TextBox ID="TxtTo" runat="server" Width="100px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TxtTo" Format="dd/MM/yyyy">
                                    </cc1:CalendarExtender>
                                </td>
                                <td align="right" bgcolor="#004080" style="width: 10%">
                                    <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                        Width="65px" />
                                    <br />
                                    <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                        Width="65px" />
                                </td>
                            </tr>
                            <tr>
                                
                                <td colspan="3">
                                    <asp:Panel ID="Panel8" runat="server" Height="300px" ScrollBars="Vertical" 
                                        Width="100%">
                                        <asp:GridView ID="GdvFind" runat="server" 
                                            EnableSortingAndPagingCallbacks="false" ShowFooter="true" Width="100%">
                                            <HeaderStyle BackColor="#0080C0" />
                                            <FooterStyle BackColor="#0080C0" />
                                            <AlternatingRowStyle BackColor="#CAE4FF" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
