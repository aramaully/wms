﻿Public Partial Class _Exit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("LoggedIn") = False
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Redirect", "window.parent.location='default.aspx';", True)
    End Sub

End Class