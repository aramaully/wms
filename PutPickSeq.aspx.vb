﻿Public Partial Class PutPickSeq
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            StrSql = "SELECT * FROM Zones ORDER BY ZoneCode"
            Cmd.CommandText = StrSql
            CmbZone.Items.Add("Select...")
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                CmbZone.Items.Add(Rdr("ZoneCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Private Sub ClearAll()
        TxtName.Text = ""
        CmbOrder0.SelectedIndex = 0
        CmbOrder1.SelectedIndex = 0
        CmbOrder2.SelectedIndex = 0
        CmbOrder3.SelectedIndex = 0
        CmbOrder4.SelectedIndex = 0
        ChkDesc0.Checked = False
        ChkDesc1.Checked = False
        ChkDesc2.Checked = False
        ChkDesc3.Checked = False
        ChkDesc4.Checked = False
    End Sub

    Protected Sub CmbZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbZone.SelectedIndexChanged
        On Error Resume Next
        StrSql = "SELECT * FROM Zones WHERE ZoneCode = '" & CmbZone.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        If Rdr.Read Then
            TxtName.Text = Rdr("Description")
            If RbnPut.Checked Then
                CmbOrder0.Text = Rdr("0Order")
                CmbOrder1.Text = Rdr("1Order")
                CmbOrder2.Text = Rdr("2Order")
                CmbOrder3.Text = Rdr("3Order")
                CmbOrder4.Text = Rdr("4Order")
                ChkDesc0.Checked = IIf(Rdr("0Desc"), True, False)
                ChkDesc1.Checked = IIf(Rdr("1Desc"), True, False)
                ChkDesc2.Checked = IIf(Rdr("2Desc"), True, False)
                ChkDesc3.Checked = IIf(Rdr("3Desc"), True, False)
                ChkDesc4.Checked = IIf(Rdr("4Desc"), True, False)
                ButSave_ConfirmButtonExtender.ConfirmText = "Sure to Save Putaway Sequence?"
            Else
                CmbOrder0.Text = Rdr("0OrderPick")
                CmbOrder1.Text = Rdr("1OrderPick")
                CmbOrder2.Text = Rdr("2OrderPick")
                CmbOrder3.Text = Rdr("3OrderPick")
                CmbOrder4.Text = Rdr("4OrderPick")
                ChkDesc0.Checked = IIf(Rdr("0DescPick"), True, False)
                ChkDesc1.Checked = IIf(Rdr("1DescPick"), True, False)
                ChkDesc2.Checked = IIf(Rdr("2DescPick"), True, False)
                ChkDesc3.Checked = IIf(Rdr("3DescPick"), True, False)
                ChkDesc4.Checked = IIf(Rdr("4DescPick"), True, False)
                ButSave_ConfirmButtonExtender.ConfirmText = "Sure to Save Picking Sequence?"
            End If
        End If
        Rdr.Close()
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        StrSql = "UPDATE Zones SET "
        If RbnPut.Checked Then
            If CmbOrder0.Text <> "Select..." Then
                StrSql = StrSql & "[0Order] = '" & CmbOrder0.Text & "',"
            Else
                StrSql = StrSql & "[0Order] = '',"
            End If
            If CmbOrder1.Text <> "Select..." Then
                StrSql = StrSql & "[1Order] = '" & CmbOrder1.Text & "',"
            Else
                StrSql = StrSql & "[1Order] = '',"
            End If
            If CmbOrder2.Text <> "Select..." Then
                StrSql = StrSql & "[2Order] = '" & CmbOrder2.Text & "',"
            Else
                StrSql = StrSql & "[2Order] = '',"
            End If
            If CmbOrder3.Text <> "Select..." Then
                StrSql = StrSql & "[3Order] = '" & CmbOrder3.Text & "',"
            Else
                StrSql = StrSql & "[3Order] = '',"
            End If
            If CmbOrder4.Text <> "Select..." Then
                StrSql = StrSql & "[4Order] = '" & CmbOrder4.Text & "',"
            Else
                StrSql = StrSql & "[4Order] = '',"
            End If
            If ChkDesc0.Checked Then
                StrSql = StrSql & "[0Desc] = 1,"
            Else
                StrSql = StrSql & "[0Desc] = 0,"
            End If
            If ChkDesc1.Checked Then
                StrSql = StrSql & "[1Desc] = 1,"
            Else
                StrSql = StrSql & "[1Desc] = 0,"
            End If
            If ChkDesc2.Checked Then
                StrSql = StrSql & "[2Desc] = 1,"
            Else
                StrSql = StrSql & "[2Desc] = 0,"
            End If
            If ChkDesc3.Checked Then
                StrSql = StrSql & "[3Desc] = 1,"
            Else
                StrSql = StrSql & "[3Desc] = 0,"
            End If
            If ChkDesc4.Checked Then
                StrSql = StrSql & "[4Desc] = 1"
            Else
                StrSql = StrSql & "[4Desc] = 0"
            End If
        Else
            If CmbOrder0.Text <> "Select..." Then
                StrSql = StrSql & "[0OrderPick] = '" & CmbOrder0.Text & "',"
            Else
                StrSql = StrSql & "[0OrderPick] = '',"
            End If
            If CmbOrder1.Text <> "Select..." Then
                StrSql = StrSql & "[1OrderPick] = '" & CmbOrder1.Text & "',"
            Else
                StrSql = StrSql & "[1OrderPick] = '',"
            End If
            If CmbOrder2.Text <> "Select..." Then
                StrSql = StrSql & "[2OrderPick] = '" & CmbOrder2.Text & "',"
            Else
                StrSql = StrSql & "[2OrderPick] = '',"
            End If
            If CmbOrder3.Text <> "Select..." Then
                StrSql = StrSql & "[3OrderPick] = '" & CmbOrder3.Text & "',"
            Else
                StrSql = StrSql & "[3OrderPick] = '',"
            End If
            If CmbOrder4.Text <> "Select..." Then
                StrSql = StrSql & "[4OrderPick] = '" & CmbOrder4.Text & "',"
            Else
                StrSql = StrSql & "[4OrderPick] = '',"
            End If
            If ChkDesc0.Checked Then
                StrSql = StrSql & "[0DescPick] = 1,"
            Else
                StrSql = StrSql & "[0DescPick] = 0,"
            End If
            If ChkDesc1.Checked Then
                StrSql = StrSql & "[1DescPick] = 1,"
            Else
                StrSql = StrSql & "[1DescPick] = 0,"
            End If
            If ChkDesc2.Checked Then
                StrSql = StrSql & "[2DescPick] = 1,"
            Else
                StrSql = StrSql & "[2DescPick] = 0,"
            End If
            If ChkDesc3.Checked Then
                StrSql = StrSql & "[3DescPick] = 1,"
            Else
                StrSql = StrSql & "[3DescPick] = 0,"
            End If
            If ChkDesc4.Checked Then
                StrSql = StrSql & "[4DescPick] = 1"
            Else
                StrSql = StrSql & "[4DescPick] = 0"
            End If
        End If
        StrSql = StrSql & " WHERE ZoneCode = '" & CmbZone.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Try
            Cmd.ExecuteNonQuery()
            CmbZone_SelectedIndexChanged(Me, e)
        Catch ex As Exception
            ErrHead.Text = "Sequence Set Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub RbnPut_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles RbnPut.CheckedChanged
        CmbZone_SelectedIndexChanged(Me, e)
    End Sub

    Protected Sub RbnPick_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles RbnPick.CheckedChanged
        CmbZone_SelectedIndexChanged(Me, e)
    End Sub
End Class