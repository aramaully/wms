﻿Imports System.IO
Partial Public Class FrmAccess
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT * FROM MenuMaster"
            Rdr = Cmd.ExecuteReader
            GdvMenu.DataSource = Rdr
            GdvMenu.DataBind()
            Rdr.Close()
            Cmd.CommandText = "SELECT UserID FROM [User]"
            Rdr = Cmd.ExecuteReader
            CmbUserID.Items.Add("Select...")
            While Rdr.Read
                CmbUserID.Items.Add(Rdr("UserID"))
            End While
            Rdr.Close()
        End If
    End Sub

    Private Sub CmbUserID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbUserID.SelectedIndexChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Name FROM [User] WHERE UserID = '" & CmbUserID.Text & "'"
        TxtUserName.Text = Cmd.ExecuteScalar
        Cmd.CommandText = "SELECT * FROM [Access] WHERE UserID = '" & CmbUserID.Text & "'"
        Rdr = Cmd.ExecuteReader
        For Each Row As GridViewRow In GdvMenu.Rows
            DirectCast(Row.FindControl("ChkSel"), CheckBox).Checked = False
        Next
        While Rdr.Read
            For Each Row As GridViewRow In GdvMenu.Rows
                If Row.Cells(0).Text = Rdr("MenuName") Then
                    DirectCast(Row.FindControl("ChkSel"), CheckBox).Checked = True
                    Exit For
                End If
            Next
        End While
    End Sub

    Private Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "ClearAll" Then
            For Each row As GridViewRow In GdvMenu.Rows
                DirectCast(row.FindControl("ChkSel"), CheckBox).Checked = False
            Next
        End If
        If e.Item.CommandName = "SelectAll" Then
            For Each row As GridViewRow In GdvMenu.Rows
                DirectCast(row.FindControl("ChkSel"), CheckBox).Checked = True
            Next
        End If
        If e.Item.CommandName = "Save" Then
            Dim Trans As OleDb.OleDbTransaction
            Trans = Con.BeginTransaction(IsolationLevel.Chaos)
            Cmd.Connection = Con
            Cmd.Transaction = Trans
            Cmd.CommandText = "DELETE FROM [Access] WHERE UserID = '" & CmbUserID.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                For Each Row As GridViewRow In GdvMenu.Rows
                    If DirectCast(Row.FindControl("ChkSel"), CheckBox).Checked Then
                        StrSql = "INSERT INTO [Access] (UserID,MenuName) VALUES('"
                        StrSql = StrSql & CmbUserID.Text & "','"
                        StrSql = StrSql & Row.Cells(0).Text & "')"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If
                Next
                Trans.Commit()
            Catch ex As Exception

            End Try

        End If
    End Sub

End Class