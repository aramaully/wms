﻿Public Partial Class Country
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim StrSql As String

    Const MENUID = "Country"
    Private Sub Country_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        FreezeGridviewHeader(GridView, HeadTbl, PanelScroll)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("Exit.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If
            FillGrid()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub GridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GridView, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GridView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView.SelectedIndexChanged
        TxtCountryCode.Text = GridView.SelectedRow.Cells(0).Text
        TxtName.Text = GridView.SelectedRow.Cells(1).Text
        TxtCurrency.Text = GridView.SelectedRow.Cells(2).Text
        If TxtCurrency.Text = "&nbsp;" Then TxtCurrency.Text = ""
        If TxtName.Text = "&nbsp;" Then TxtName.Text = ""
    End Sub

    Private Sub GridView_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GridView.SelectedIndexChanging
        GridView.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub FillGrid()
        Dim da As New OleDb.OleDbDataAdapter("SELECT * FROM Country", con)
        Dim ds As New DataSet()
        da.Fill(ds)
        GridView.DataSource = ds
        GridView.DataBind()
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GridView.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GridView.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Cmd As New OleDb.OleDbCommand
        Dim Trans As OleDb.OleDbTransaction
        Dim StrSql As String
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Cmd.CommandText = "DELETE FROM Country WHERE CountryCode = '" & TxtCountryCode.Text & "'"
        Try
            Cmd.ExecuteNonQuery()
            StrSql = "INSERT INTO Country (CountryCode,Name,Currency) VALUES('" & TxtCountryCode.Text & "','"
            StrSql = StrSql & TxtName.Text & "','"
            StrSql = StrSql & TxtCurrency.Text & "')"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            TxtCountryCode.Text = ""
            TxtCurrency.Text = ""
            TxtName.Text = ""
            FillGrid()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Error"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Dim cmd As New OleDb.OleDbCommand
        cmd.Connection = Con
        cmd.CommandText = "DELETE FROM Country WHERE CountryCode = '" & TxtCountryCode.Text & "'"
        Try
            cmd.ExecuteNonQuery()
            TxtCountryCode.Text = ""
            TxtCurrency.Text = ""
            TxtName.Text = ""
            FillGrid()
        Catch ex As Exception
            ErrHead.Text = "Delete Error"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub FreezeGridviewHeader(ByVal _gv1 As GridView, ByVal _tb1 As Table, ByVal _pc1 As Panel)
        'Page.EnableViewState = False
        ''[English]Copying a header row data and properties 
        '_tb1.Rows.Add(_gv1.HeaderRow)
        '_tb1.Rows(0).ControlStyle.CopyFrom(_gv1.HeaderStyle)
        ''_tb1.CellPadding = _gv1.CellPadding
        ''_tb1.CellSpacing = _gv1.CellSpacing
        ''_tb1.BorderWidth = _gv1.BorderWidth
        ''_tb1.Width = _gv1.Width
        'For i = 0 To _gv1.Columns.Count - 1
        '    _tb1.Rows(0).Cells(i).Width = _gv1.Columns(i).ItemStyle.Width
        'Next
        ''If Not _gv1.Width.IsEmpty Then
        ''    _gv1.Width = Unit.Pixel(Convert.ToInt32(_gv1.Width.Value) + Convert.ToInt32(_tb1.Width.Value) + 13)
        ''End If
        ' ''[English]Copying  each cells properties to the new header cells properties
        ''Dim Count As Integer = 0
        ''_pc1.Width = Unit.Pixel(100)
        ''For Count = 0 To _gv1.HeaderRow.Cells.Count - 1
        ''    _tb1.Rows(0).Cells(Count).Width = _gv1.Columns(Count).ItemStyle.Width
        ''    _tb1.Rows(0).Cells(Count).BorderWidth = _gv1.Columns(Count).HeaderStyle.BorderWidth
        ''    _tb1.Rows(0).Cells(Count).BorderStyle = _gv1.Columns(Count).HeaderStyle.BorderStyle
        ''    '_pc1.Width = Unit.Pixel(Convert.ToInt32(_tb1.Rows(0).Cells(Count).Width.Value) + Convert.ToInt32(_pc1.Width.Value) + 14)
        ''Next
        ''Panel1.Width = Unit.Pixel(Convert.ToInt32(_tb1.Rows(0).Cells(Count - 1).Width.Value) + 12)
        '_tb1.Width = New Unit(20, UnitType.Percentage)
        Dim HRow As New TableRow
        _tb1.Width = _gv1.Width
        For i = 0 To _gv1.Columns.Count - 1
            Dim HCell As New TableCell
            Dim Col As DataControlField = _gv1.Columns(i)
            HCell.BorderStyle = BorderStyle.Solid
            HCell.BorderWidth = 1
            HCell.Font.Bold = True
            If i < _gv1.Columns.Count - 1 Then
                HCell.Width = Col.ItemStyle.Width
            End If
            HCell.HorizontalAlign = HorizontalAlign.Left
            HCell.Text = Col.HeaderText
            HRow.Cells.Add(HCell)
        Next
        _tb1.Rows.Add(HRow)
        _gv1.ShowHeader = False
    End Sub

End Class