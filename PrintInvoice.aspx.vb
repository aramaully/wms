﻿Public Partial Class PrintInvoice
    Inherits System.Web.UI.Page

    Dim Con As New OleDb.OleDbConnection
    Dim CmdClient As New OleDb.OleDbCommand
    Dim RdrClient As OleDb.OleDbDataReader
    Dim CmdBoe As New OleDb.OleDbCommand
    Dim RdrBoe As OleDb.OleDbDataReader
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrSel As String

    Const MENUID = "PrintInvoice"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            CmbZone.Items.Add("ALL")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub TxtInvoiceNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtInvoiceNo.TextChanged
        If TxtInvoiceNo.Text <> "" And TxtInvoiceTo.Text = "" Then
            StrSql = "SELECT * FROM Invoice WHERE InvoiceNo='" & TxtInvoiceNo.Text & "' AND WhCode='" & CmbZone.Text & "'"
            CmdBoe.CommandText = StrSql
            CmdBoe.Connection = Con
            RdrBoe = CmdBoe.ExecuteReader
            If RdrBoe.Read Then
                TxtDate.Text = Format(RdrBoe("InvDate"), "dd/MM/yyyy")
                StrSql = "SELECT * FROM Client WHERE ClientID='" & RdrBoe("ClientID") & "'"
                CmdClient.CommandText = StrSql
                CmdClient.Connection = Con
                RdrClient = CmdClient.ExecuteReader
                If RdrClient.Read Then
                    TxtClientID.Text = RdrClient("ClientID")
                    TxtName.Text = RdrClient("Name")
                End If
            End If
            RdrBoe.Close()
        Else
            TxtClientID.Text = ""
            TxtName.Text = ""
        End If
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        StrSel = ""
        StrSel = "{Invoice.WhCode} = '" & CmbZone.Text & "' And {Invoice.InvoiceNo}"
        If TxtInvoiceTo.Text = "" Then
            StrSel = StrSel & "=" & TxtInvoiceNo.Text
        Else
            StrSel = StrSel & ">= " & TxtInvoiceNo.Text & "And {Invoice.InvoiceNo} <= " & TxtInvoiceTo.Text
        End If
        If ChkMonthly.Checked = True Then
            Session("ReportFile") = "RptInvoice.rpt"
            Session("Period") = ""
            Session("Zone") = ""
            Session("SelFormula") = StrSel

            Session("PayableTo") = "9999"


            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
            'Response.Redirect("Report.aspx?RptID=Print Invoice&RPTFileName=PrintInvoice.aspx&RptFile=RptInvoice.rpt&StrSelF=" & StrSel)
        Else
            Session("Period") = ""
            Session("Zone") = ""
            Session("ReportFile") = "RptLogInvoice.rpt"
            Session("SelFormula") = StrSel
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
            'Response.Redirect("Report.aspx?RptID=Print Invoice&RPTFileName=PrintInvoice.aspx&RptFile=RptLogInvoice.rpt&StrSelF=" & StrSel)
        End If

    End Sub

    Protected Sub BtnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnFind.Click
        PnlFind.Style.Item("display") = ""
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT InvoiceNo,Client.Name,CONVERT(Varchar,InvDate,103) Date,[Value] FROM Invoice,Client WHERE Client.ClientID = Invoice.ClientID "
        If TxtSearch.Text <> "" Then
            StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
        End If
        If CmbZone.Text <> "Select..." Then
            StrSql = StrSql & " And Invoice.WhCode = '" & CmbZone.Text & "'"
        End If
        If ChkDate.Checked Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = StrSql & "AND Invoice.InvDate >= '" & Format(CDate(TxtFrom.Text), "MM/dd/yyyy") & "' AND Invoice.InvDate <= '" & Format(CDate(TxtTo.Text), "MM/dd/yyyy") & "'"
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtInvoiceNo.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtInvoiceNo_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub ChkDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkDate.CheckedChanged
        If ChkDate.Checked Then
            TxtFrom.Enabled = True
            TxtTo.Enabled = True
        Else
            TxtFrom.Enabled = False
            TxtTo.Enabled = False
            TxtTo.Text = ""
            TxtFrom.Text = ""
        End If
    End Sub

End Class