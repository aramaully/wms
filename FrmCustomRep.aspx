﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmCustomRep.aspx.vb" Inherits="Wms.FrmCustomRep" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="EO.Web" namespace="EO.Web" tagprefix="eo" %>

<html>
<head id="Head1" runat="server">
    <title>Custom Report Generator</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.

            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Custom Report Generator'; //Set the name

            w.Width = 800; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 600;
            w.Top = 10;
            w.Left = 10;              
            w.Show(); //Show the window
        };
        function ShowReport() {
            wfind = window.frameElement.IDCWindow;
            wfind.CreateWindow('FrmRpt.aspx', w);
            return false;
        };
    </script>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            height: 19px;
        }
    </style>
    </head>
<body onload="init();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
            <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px"
                runat="server" Height="32px" Width="32px" BorderStyle="None" Visible="True">
                <div id="divImage" style="display: none">
                    <asp:Image ID="img1" runat="server" ImageUrl="/Images/Wait.gif" />
                </div>
            </asp:Panel>
            <div id="toolbar" style="width: 100%;">
                <eo:ToolBar ID="ToolBar1" runat="server" BackgroundImage="00100103" 
                    BackgroundImageLeft="00100101" BackgroundImageRight="00100102" SeparatorImage="00100104"
                    Width="100%" AutoPostBack="True">
                    <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 1px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: #335ea8 1px solid; background-color: #99afd4; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                    <Items>
                        <eo:ToolBarItem CommandName="Print" ImageUrl="~/images/printHS.png" ToolTip="Save">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="Separator">
                        </eo:ToolBarItem>
                        <eo:ToolBarItem CommandName="Help" ImageUrl="~/images/help.png" ToolTip="Help">
                        </eo:ToolBarItem>
                    </Items>
                    <ItemTemplates>
                        <eo:ToolBarItem Type="Custom">
                            <NormalStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <HoverStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                            <DownStyle CssText="background-color:transparent;border-bottom-style:none;border-left-style:none;border-right-style:none;border-top-style:none;padding-bottom:1px;padding-left:2px;padding-right:2px;padding-top:2px;" />
                        </eo:ToolBarItem>
                        <eo:ToolBarItem Type="DropDownMenu">
                            <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                            <HoverStyle CssText="border-right: #335ea8 1px solid; padding-right: 3px; border-top: #335ea8 1px solid; padding-left: 3px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 2px; border-bottom: #335ea8 1px solid; background-color: #c2cfe5; cursor:hand; background-image: url(00100106); background-position-x: right;" />
                            <DownStyle CssText="border-right: #335ea8 1px solid; padding-right: 2px; border-top: #335ea8 1px solid; padding-left: 4px; padding-bottom: 2px; border-left: #335ea8 1px solid; padding-top: 3px; border-bottom: none; background-color:transparent; cursor:hand; background-image: url(00100105); background-position-x: right;" />
                        </eo:ToolBarItem>
                    </ItemTemplates>
                    <NormalStyle CssText="padding-right: 4px; padding-left: 4px; padding-bottom: 3px; border-top-style: none; padding-top: 3px; border-right-style: none; border-left-style: none; border-bottom-style: none; cursor:hand; FONT-SIZE: 12px; FONT-FAMILY: Tahoma;" />
                </eo:ToolBar>
            </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="Content" style="width: 100%; height: 100%; overflow: auto; padding: 2px;
                border-right: solid 1px #cddaea;">
                <table class="style1">
                    <tr>
                        <td>
                            Heading
                        </td>
                        <td colspan="6" unselectable="off">
                            <asp:TextBox ID="TxtHeading" runat="server" Width="400px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="#0033CC" Text="Tables"></asp:Label>
                        </td>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="#0033CC" Text="Selected Tables"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="#0033CC" Text="Fields"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="#0033CC" Text="Selected Fields"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="5" valign="top">
                            <asp:ListBox ID="LstTables" runat="server" Height="210px" Width="150px" AutoPostBack="True" SelectionMode="Multiple" ViewStateMode="Enabled"></asp:ListBox>
                        </td>
                        <td align="center" colspan="2">
                            <asp:ImageButton runat="server" ID="ButSelTabOne" BorderStyle="Outset" BorderWidth="1px"
                                ImageUrl="~/images/DataContainer_MoveNextHS.png" />
                        </td>
                        <td rowspan="5" valign="top">
                            <asp:ListBox ID="LstSelTables" runat="server" AutoPostBack="True" Height="210px"
                                Width="150px"></asp:ListBox>
                        </td>
                        <td rowspan="5" valign="top">
                            <asp:ListBox ID="LstFields" runat="server" Height="210px" Width="150px" 
                                SelectionMode="Multiple"></asp:ListBox>
                        </td>
                        <td align="Center">
                            <asp:ImageButton ID="ButSelFldOne" runat="server" BorderStyle="Outset" BorderWidth="1px"
                                ImageUrl="~/images/DataContainer_MoveNextHS.png" />
                        </td>
                        <td rowspan="5" valign="top">
                            <asp:ListBox ID="LstSelFields" runat="server" Height="210px" Width="150px"></asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:ImageButton ID="ButSelTabAll" runat="server" BorderStyle="Outset" BorderWidth="1px"
                                ImageUrl="~/images/DataContainer_MoveLastHS.png" />
                        </td>
                        <td align="Center">
                            <asp:ImageButton ID="ButSelFldAll" runat="server" BorderStyle="Outset" BorderWidth="1px"
                                ImageUrl="~/images/DataContainer_MoveLastHS.png" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style2" align="center" colspan="2">
                            <asp:ImageButton ID="ButUnSelTabOne" runat="server" BorderStyle="Outset" BorderWidth="1px"
                                ImageUrl="~/images/DataContainer_MovePreviousHS.png" />
                        </td>
                        <td class="style2" align="Center">
                            <asp:ImageButton ID="ButUnSelFldOne" runat="server" BorderStyle="Outset" BorderWidth="1px"
                                ImageUrl="~/images/DataContainer_MovePreviousHS.png" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:ImageButton ID="ButUnSelTabAll" runat="server" BorderStyle="Outset" BorderWidth="1px"
                                ImageUrl="~/images/DataContainer_MoveFirstHS.png" />
                        </td>
                        <td align="Center">
                            <asp:ImageButton ID="ButUnSelFldAll" runat="server" BorderStyle="Outset" BorderWidth="1px"
                                ImageUrl="~/images/DataContainer_MoveFirstHS.png" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Join Tables"></asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="CmbType" runat="server" AutoPostBack="True">
                                <asp:ListItem>Select...</asp:ListItem>
                                <asp:ListItem>INNER JOIN</asp:ListItem>
                                <asp:ListItem>LEFT OUTER JOIN</asp:ListItem>
                                <asp:ListItem>RIGHT OUTER JOIN</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="ButJoinFld" runat="server" Text="Select Join Field" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <asp:TextBox ID="TxtJoin" runat="server" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Record Selection Conditions"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="CmbCompare" runat="server" Width="150px">
                                <asp:ListItem>is equal to</asp:ListItem>
                                <asp:ListItem>is not equal to</asp:ListItem>
                                <asp:ListItem>is greater than</asp:ListItem>
                                <asp:ListItem>is less than</asp:ListItem>
                                <asp:ListItem>is greater than or equal to</asp:ListItem>
                                <asp:ListItem>is less than or equal to</asp:ListItem>
                                <asp:ListItem>is Null</asp:ListItem>
                                <asp:ListItem>is not Null</asp:ListItem>
                                <asp:ListItem>like</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="2">
                            Value
                        </td>
                        <td>
                            <asp:TextBox ID="TxtValue" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RadioButton ID="OptAnd" runat="server" Checked="True" GroupName="Option" Text="And" />
                            <asp:RadioButton ID="OptOr" runat="server" GroupName="Option" Text="Or" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Button ID="ButApply" runat="server" Text="Apply" />
                        </td>
                    </tr>
                </table>
                <asp:Panel runat="server" ID="SqlString" Height="210px">
                    <asp:TextBox ID="TxtSql" runat="server" Height="200px" TextMode="MultiLine" 
                        Width="100%"></asp:TextBox>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_InitializeRequest);
        prm.add_endRequest(prm_EndRequest);

        function prm_InitializeRequest(sender, args) {
            var panelProg = $get('divImage');
            if (args._postBackElement.id != "Timer1") {
            panelProg.style.display = '';}
        };

        function prm_EndRequest(sender, args) {
            var panelProg = $get('divImage');
            panelProg.style.display = 'none';
        };
    </script>
</body>
</html>

