﻿Public Partial Class ChPalletRatio
    Inherits System.Web.UI.Page

    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdScalar As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "ChangePalletRatio"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            ButRef.Enabled = False
            ButSave.Enabled = False
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtBoeNo.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtBoeNo_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvCont.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCont.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        If RbnClient.Text = "Name" Then
            StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
            If RbnClient.Checked Then
                StrSql = StrSql & "Name"
            ElseIf RbnCountry.Checked Then
                StrSql = StrSql & "City"
            Else
                StrSql = StrSql & "Contact"
            End If
            StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Else
            StrSql = "SELECT BoeIn.BillNo, CONVERT(Char,BoeIn.BillDate,103) 'Bill Date', BoeIn.Revision, Client.Name FROM BoeIn, Client WHERE "
            StrSql = StrSql & "BoeIn.ClientID = Client.ClientID "
            If RbnClient.Checked Then
                StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCountry.Checked Then
                StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnTraffic.Checked Then
                StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCont.Checked Then
                StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
            End If
            If ChkDate.Checked Then
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                StrSql = StrSql & "AND Boein.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
                StrSql = StrSql & "AND Boein.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
            End If
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ClearALL()
        TxtClient.Text = ""
        TxtDate.Text = ""
        TxtDateIn.Text = ""
        TxtRevision.Text = ""
        GdvCont.DataSource = Nothing
        GdvCont.DataBind()
        ButSave.Enabled = False
    End Sub

    Private Sub TxtBoeNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBoeNo.TextChanged
        ClearALL()
        Cmd.Connection = Con
        CmdScalar.Connection = Con
        Cmd.CommandText = "SELECT * FROM BoeIn WHERE BillNo = '" & TxtBoeNo.Text & "' ORDER BY BillNo"
        Rdr = Cmd.ExecuteReader
        If Not Rdr.Read Then
            ErrHead.Text = "ERROR"
            ErrMsg.Text = "Invalid Inbound Bill"
            ModalPopupExtender1.Show()
            TxtBoeNo.Focus()
            Rdr.Close()
            ButRef.Enabled = False
            Exit Sub
        Else
            'On Error Resume Next
            If Rdr("WhCode") <> Session("PstrZone") And Session("PstrZone") <> "ALL" Then
                ErrHead.Text = "Access Error"
                ErrMsg.Text = "Sorry! You do not have permission for the records of Zone " & Rdr("WhCode")
                ButRef.Enabled = False
                ButSave.Enabled = False
                Exit Sub
            End If
            TxtRevision.Text = Rdr("Revision")
            TxtDate.Text = Rdr("BillDate")
            CmdScalar.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & Rdr("ClientID") & "'"
            TxtClient.Text = CmdScalar.ExecuteScalar.ToString
            ButRef.Enabled = True
        End If
        Rdr.Close()
    End Sub

    Private Sub UpDateGrid()
        StrSql = "SELECT PalletID,BoeNo,LocCode1,PRatio,DateIn FROM Pallets WHERE BoeNo = '" & TxtBoeNo.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Dim da As New OleDb.OleDbDataAdapter(Cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvCont.DataSource = ds
        GdvCont.DataBind()
        If GdvCont.Rows.Count > 0 Then
            ButSave.Enabled = True
        Else
            ButSave.Enabled = False
        End If
    End Sub

    Protected Sub ButFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFind.Click
        RbnClient.Text = "Client"
        RbnCountry.Text = "Country"
        RbnTraffic.Text = "Traffic Op"
        RbnCont.Visible = True
        ChkDate.Visible = True
        TxtFrom.Visible = True
        TxtTo.Visible = True
        LblToDate.Visible = True
        GdvFind.DataSource = Nothing
        TxtTo.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtFrom.Text = Date.Today.AddDays(-10).ToString("dd/MM/yyyy")
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRef.Click
        UpDateGrid()
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            For Each row As GridViewRow In GdvCont.Rows
                Select Case Val(DirectCast(row.FindControl("PRatio"), TextBox).Text)
                    Case 1.5
                        StrSql = "UPDATE Pallets SET PRatio2=0.5, PRatio3=0 WHERE PalletID=" & row.Cells(0).Text
                    Case 2
                        StrSql = "UPDATE Pallets SET PRatio2=1, PRatio3=0 WHERE PalletID=" & row.Cells(0).Text
                    Case 3
                        StrSql = "UPDATE Pallets SET PRatio2=1, PRatio3=1 WHERE PalletID=" & row.Cells(0).Text
                    Case Else
                        StrSql = "UPDATE Pallets SET PRatio2=0, PRatio3=0 WHERE PalletID=" & row.Cells(0).Text
                End Select
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            Next
            Trans.Commit()
            ClearALL()
            TxtBoeNo.Focus()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Save Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class