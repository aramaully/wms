﻿Public Partial Class Traffic
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatLoc As New DataTable
    Dim StrCondition As String = ""

    Const MENUID = "TrafficOfficer"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If
            BindTO()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Private Sub GdvTO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvTO.SelectedIndexChanged
        On Error Resume Next
        TxtName.Text = ChkSpace(GdvTO.SelectedRow.Cells(0).Text)
    End Sub

    Private Sub BindTO()
        Cmd.Connection = Con
        StrSql = "SELECT Name FROM Traffic"
        StrSql = StrSql & StrCondition & " ORDER BY Name"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar Is Nothing Then Exit Sub
        Rdr = Cmd.ExecuteReader
        DatLoc.Load(Rdr)
        Rdr.Close()
        GdvTO.DataSource = DatLoc
        GdvTO.DataBind()
        If GdvTO.Rows.Count = 1 Then
            On Error Resume Next
            TxtName.Text = ChkSpace(GdvTO.Rows(0).Cells(0).Text)
        End If
    End Sub

    Private Sub GdvTO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvTO.PageIndexChanging
        GdvTO.PageIndex = e.NewPageIndex
        BindTO()
    End Sub

    Private Sub GdvTO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvTO.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvTO, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvTO.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvTO.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub GdvTO_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvTO.SelectedIndexChanging
        GdvTO.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        On Error Resume Next
        TxtName.Text = ""
        TxtName.Focus()
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Try
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Traffic WHERE Name = '" & TxtName.Text & "'"
            Cmd.ExecuteNonQuery()
            ButNew_Click(Me, e)
            BindTO()
        Catch ex As Exception
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Cmd.CommandText = "SELECT Name FROM Traffic WHERE Name = '" & TxtName.Text & "'"
        Try
            If Cmd.ExecuteScalar Is Nothing Then
                StrSql = "INSERT INTO Traffic (Name) "
                StrSql = StrSql & "VALUES('" & TxtName.Text & "')"
            Else
                StrSql = "UPDATE Traffic SET Name = '" & TxtName.Text & "' WHERE Name = '" & TxtName.Text & "'"
            End If
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ButNew_Click(Me, e)
            BindTO()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class