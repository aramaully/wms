﻿Public Partial Class BillStatus
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdClient As New OleDb.OleDbCommand
    Dim RdrClient As OleDb.OleDbDataReader
    Dim CmdBoe As New OleDb.OleDbCommand
    Dim RdrBoe As OleDb.OleDbDataReader
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrSel As String

    Const MENUID = "BillStatus"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Cmd.Connection = Con
        'Check Access
        StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar = 0 Then
            Response.Redirect("about:blank")
        End If
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub TxtBoeNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBoeNo.TextChanged
        StrSql = "SELECT * FROM BoeIN WHERE BillNo='" & TxtBoeNo.Text & "'"
        CmdBoe.Connection = Con
        CmdBoe.CommandText = StrSql
        RdrBoe = CmdBoe.ExecuteReader
        If RdrBoe.Read Then
            TxtClientID.Text = RdrBoe("ClientID")
            TxtDate.Text = Format(RdrBoe("BillDate"), "dd/MM/yyyy")
            StrSql = "SELECT * FROM Client WHERE ClientID='" & RdrBoe("ClientID") & "'"
            CmdClient.Connection = Con
            CmdClient.CommandText = StrSql
            RdrClient = CmdClient.ExecuteReader
            If RdrClient.Read Then
                TxtName.Text = RdrClient("Name")
            End If
            RdrClient.Close()
        End If
        RdrBoe.Close()
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        'StrSel = ""
        'If TxtBoeNo.Text <> "ALL" Then
        '    StrSel = "{BoeIn.BillNo} = '" & TxtBoeNo.Text & "'"
        'Else
        '    If TxtClient.Text <> "ALL" Then
        '        StrSel = "{BoeIn.ClientID} = '" & TxtClient.Text & "'"
        '    End If
        '    If ChkBDate.Checked = True Then
        '        If Len(StrSel) > 0 Then
        '            StrSel = StrSel & " And "
        '        End If
        '        StrSel = StrSel & "{Boein.BillDate} >= Date(" & Format(CDate(TxtBFrom.Text), "yyyy,MM,dd") & ") And "
        '        StrSel = StrSel & "{Boein.BillDate} <= Date(" & Format(CDate(TxtBTo.Text), "yyyy,MM,dd") & ")"
        '    End If
        'End If
        'If Session("PstrZone").ToString <> "ALL" Then
        '    If Len(StrSel) > 0 Then
        '        StrSel = StrSel & " And "
        '    End If
        '    StrSel = StrSel & "{Boein.WhCode} = '" & Session("PstrZone").ToString & "'"
        'End If
        Dim StrSelFormula As String


        StrSelFormula = ""
        If TxtBoeNo.Text <> "ALL" Then
            StrSelFormula = "{BoeIn.BillNo} = '" & TxtBoeNo.Text & "'"
        Else
            If TxtClientID.Text <> "ALL" Then
                StrSelFormula = "{BoeIn.ClientID} = '" & TxtClientID.Text & "'"
            End If
            If ChkBDate.Checked Then
                If Len(StrSelFormula) > 0 Then
                    StrSelFormula = StrSelFormula & " And "
                End If
                StrSelFormula = StrSelFormula & "{Boein.BillDate} >= Date(" & Format(TxtBFrom.Text, "yyyy,MM,DD") & ") And "
                StrSelFormula = StrSelFormula & "{Boein.BillDate} <= Date(" & Format(TxtBTo, "yyyy,MM,DD") & ")"
            End If
        End If
        If Session("PstrZone").ToString <> "ALL" Then
            If Len(StrSelFormula) > 0 Then
                StrSelFormula = StrSelFormula & " And "
            End If
            StrSelFormula = StrSelFormula & "{Boein.WhCode} = '" & Session("PstrZone").ToString & "'"
        End If

        Session("ReportFile") = "RptBillStatus.rpt"
        Session("Period") = ""
        Session("Zone") = ""
        Session("SelFormula") = StrSelFormula
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        'Response.Redirect("Report.aspx?RptID=Bill Status&RPTFileName=BillStatus.aspx&RptFile=RptBillStatus.rpt&StrSelF=" & StrSel)

    End Sub

    Private Sub TxtClient_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        StrSql = "SELECT * FROM Client WHERE ClientID='" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtName.Text = Rdr("Name")
        End If
        Rdr.Close()
    End Sub

    Protected Sub BtnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnFind.Click
        Session.Add("PstrCode", TxtClientID.Text)
        ScriptManager.RegisterStartupScript(BtnFind, Me.GetType, "ShowFindBillStatus", "<script language='javascript'> ShowFind();</script>", False)
    End Sub
End Class