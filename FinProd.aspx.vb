﻿Public Partial Class FinProd
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim StrSql As String
    Dim Cmd As New OleDb.OleDbCommand
    Dim rdr As OleDb.OleDbDataReader
    Dim DatComp As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT UnitCode FROM Unit"
            rdr = Cmd.ExecuteReader
            CmbUnit.Items.Add("Select...")
            While rdr.Read
                CmbUnit.Items.Add(rdr("UnitCode"))
            End While
            rdr.Close()
            Cmd.CommandText = "SELECT UnitCode FROM SubUnit"
            rdr = Cmd.ExecuteReader
            CmbSubUnit.Items.Add("Select...")
            While rdr.Read
                CmbSubUnit.Items.Add(rdr("UnitCode"))
            End While
            rdr.Close()
            TxtCode.Focus()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub TxtCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtCode.TextChanged
        TxtDescription.Text = ""
        CmbSubUnit.SelectedIndex = 0
        CmbUnit.SelectedIndex = 0
        GdvComp.DataBind()
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT * FROM FProduct WHERE FProdCode = '" & TxtCode.Text & "'"
        rdr = Cmd.ExecuteReader
        If rdr.Read Then
            TxtDescription.Text = rdr("Description")
            CmbUnit.Text = rdr("UnitCode")
            CmbSubUnit.Text = rdr("SubUnitCode")
        End If
        rdr.Close()
        'Update Components Table
        StrSql = "SELECT A.ProdCode, B.Description,A.Quantity,A.Unit FROM FProdComp A, Product B "
        StrSql = StrSql & "WHERE A.ProdCode = B.ProdCode AND A.FProdCode = '" & TxtCode.Text & "'"
        Cmd.CommandText = StrSql
        rdr = Cmd.ExecuteReader
        DatComp.Load(rdr)
        rdr.Close()
        GdvComp.DataSource = DatComp
        GdvComp.DataBind()
        TxtDescription.Focus()
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ProdCode_Change(ByVal sender As Object, ByVal e As EventArgs)
        Dim ProdCode As TextBox = DirectCast(sender, TextBox)
        Dim Gdv As GridViewRow = DirectCast(ProdCode.Parent.Parent, GridViewRow)
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Description,Unit FROM Product WHERE ProdCode = '" & ProdCode.Text & "'"
        rdr = Cmd.ExecuteReader
        If rdr.Read Then
            DirectCast(Gdv.FindControl("Description"), TextBox).Text = rdr("Description").ToString
            DirectCast(Gdv.FindControl("Unit"), TextBox).Text = rdr("Unit").ToString
            DirectCast(Gdv.FindControl("Qty"), TextBox).Focus()
        Else
            DirectCast(Gdv.FindControl("Description"), TextBox).Text = "Invalid Product"
            DirectCast(Gdv.FindControl("Unit"), TextBox).Text = ""
            DirectCast(Gdv.FindControl("Code"), TextBox).Focus()
        End If

    End Sub

    Protected Sub FindPord_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim FindButton As Button = DirectCast(sender, Button)
        Dim Gdv As GridViewRow = DirectCast(FindButton.Parent.Parent, GridViewRow)
        LblSearch.Text = "Enter the product name to Search"
        FindResult.Text = Gdv.RowIndex
        PnlFind.Visible = True
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Visible = False
    End Sub

    Protected Sub ButRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRefresh.Click
        If LblSearch.Text = "Enter the product name to Search" Then
            StrSql = "SELECT ProdCode,Description,Unit,SubUnit FROM Product WHERE "
            StrSql = StrSql & "Description LIKE '%" + TxtSearch.Text + "%' ORDER BY ProdCode"
        Else
            StrSql = "SELECT FProdCode,Description,UnitCode,SubUnitCode FROM FProduct WHERE "
            StrSql = StrSql & "Description LIKE '%" + TxtSearch.Text + "%' ORDER BY FProdCode"
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GdvFind.SelectedIndexChanged
        If Val(FindResult.Text) >= 0 Then
            Dim Row As GridViewRow = GdvComp.Rows(Val(FindResult.Text))
            Dim ProdCode As TextBox = DirectCast(Row.FindControl("Code"), TextBox)
            ProdCode.Text = GdvFind.SelectedRow.Cells(0).Text
            ProdCode_Change(ProdCode, e)
        Else
            TxtCode.Text = GdvFind.SelectedRow.Cells(0).Text
            TxtCode_TextChanged(Me, e)
        End If
        PnlFind.Visible = False
    End Sub

    Private Sub ButAddComp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAddComp.Click
        StrSql = "SELECT A.ProdCode, B.Description,A.Quantity,A.Unit FROM FProdComp A, Product B "
        StrSql = StrSql & "WHERE A.ProdCode = B.ProdCode AND A.FProdCode IS NULL"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        rdr = Cmd.ExecuteReader
        DatComp.Load(rdr)
        rdr.Close()
        Dim Datrow As DataRow
        On Error Resume Next
        For Each row As GridViewRow In GdvComp.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Datrow = DatComp.NewRow
                Datrow("ProdCode") = DirectCast(row.FindControl("Code"), TextBox).Text
                Datrow("Description") = DirectCast(row.FindControl("Description"), TextBox).Text
                Datrow("Quantity") = DirectCast(row.FindControl("Qty"), TextBox).Text
                Datrow("Unit") = DirectCast(row.FindControl("Unit"), TextBox).Text
                DatComp.Rows.Add(Datrow)
            End If
        Next
        Datrow = DatComp.NewRow
        DatComp.Rows.Add(Datrow)
        GdvComp.DataSource = DatComp
        GdvComp.DataBind()
        GdvComp.Rows(GdvComp.Rows.Count - 1).FindControl("Code").Focus()
    End Sub

    Private Sub ButDelComp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelComp.Click
        StrSql = "SELECT A.ProdCode, B.Description,A.Quantity,A.Unit FROM FProdComp A, Product B "
        StrSql = StrSql & "WHERE A.ProdCode = B.ProdCode AND A.FProdCode IS NULL"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        rdr = Cmd.ExecuteReader
        DatComp.Load(rdr)
        rdr.Close()
        Dim Datrow As DataRow
        On Error Resume Next
        For Each row As GridViewRow In GdvComp.Rows
            If row.RowType = DataControlRowType.DataRow Then
                If Not DirectCast(row.FindControl("ChkSelect"), CheckBox).Checked Then
                    Datrow = DatComp.NewRow
                    Datrow("ProdCode") = DirectCast(row.FindControl("Code"), TextBox).Text
                    Datrow("Description") = DirectCast(row.FindControl("Description"), TextBox).Text
                    Datrow("Quantity") = DirectCast(row.FindControl("Qty"), TextBox).Text
                    Datrow("Unit") = DirectCast(row.FindControl("Unit"), TextBox).Text
                    DatComp.Rows.Add(Datrow)
                End If
            End If
        Next
        GdvComp.DataSource = DatComp
        GdvComp.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        LblSearch.Text = "Enter the Finished Product Name to Search"
        FindResult.Text = "-1"
        PnlFind.Visible = True
    End Sub

    Private Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        TxtCode.Text = ""
        TxtDescription.Text = ""
        CmbSubUnit.SelectedIndex = 0
        CmbUnit.SelectedIndex = 0
        GdvComp.DataBind()
        TxtCode.Focus()
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
        StrSql = "SELECT TOP 1 FProdCode FROM FProduct WHERE FProdCode > '" & TxtCode.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtCode.Text = Cmd.ExecuteScalar
        TxtCode_TextChanged(Me, e)
    End Sub

    Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevious.Click
        StrSql = "SELECT TOP 1 FProdCode FROM FProduct WHERE FProdCode < '" & TxtCode.Text & "' ORDER BY FProdCode DESC"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        TxtCode.Text = Cmd.ExecuteScalar
        TxtCode_TextChanged(Me, e)
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            Cmd.CommandText = "DELETE FROM FProduct WHERE FProdCode = '" & TxtCode.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "DELETE FROM FProdComp WHERE FProdCode = '" & TxtCode.Text & "'"
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ButNew_Click(Me, e)
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            Cmd.CommandText = "DELETE FROM FProduct WHERE FProdCode = '" & TxtCode.Text & "'"
            Cmd.ExecuteNonQuery()
            StrSql = "INSERT INTO FProduct (FProdCode,Description,UnitCode,SubUnitCode) VALUES('"
            StrSql = StrSql & TxtCode.Text & "','"
            StrSql = StrSql & TxtDescription.Text & "','"
            StrSql = StrSql & CmbUnit.Text & "','"
            StrSql = StrSql & CmbSubUnit.Text & "')"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "DELETE FROM FProdComp WHERE FProdCode = '" & TxtCode.Text & "'"
            Cmd.ExecuteNonQuery()
            For Each row As GridViewRow In GdvComp.Rows
                StrSql = "INSERT INTO FProdComp (FProdCode,ProdCode,Quantity,Unit) VALUES('" & TxtCode.Text & "','"
                StrSql = StrSql & DirectCast(row.FindControl("Code"), TextBox).Text & "',"
                StrSql = StrSql & Val(DirectCast(row.FindControl("Qty"), TextBox).Text) & ",'"
                StrSql = StrSql & DirectCast(row.FindControl("Unit"), TextBox).Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            Next
            Trans.Commit()
            ButNew_Click(Me, e)
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class