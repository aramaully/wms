﻿Public Partial Class ReviseBoe
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "ReviseBills"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT BoeIn.BillNo, CONVERT(Char,BoeIn.BillDate,103) 'Bill Date', BoeIn.Revision, Client.Name FROM BoeIn, Client WHERE "
        StrSql = StrSql & "BoeIn.ClientID = Client.ClientID "
        If RbnClient.Checked Then
            StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCountry.Checked Then
            StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnTraffic.Checked Then
            StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCont.Checked Then
            StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
        End If
        If ChkDate.Checked Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = StrSql & "AND Boein.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND Boein.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtBillin.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtBillin_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub



    Private Sub TxtBillin_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBillin.TextChanged
        Dim DatBoe As New DataTable
        ClearAll()
        Cmd.Connection = Con
        StrSql = "SELECT BoeIn.BillDate, Client.Name, BoeIn.Revision FROM BoeIn, Client WHERE BoeIn.ClientID = Client.ClientID "
        StrSql = StrSql & "AND BoeIn.BillNo = '" & TxtBillin.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtDate.Text = Format(Rdr("BillDate"), "dd/MM/yyyy")
            TxtClient.Text = Rdr("Name").ToString
            TxtRevision.Text = Rdr("Revision").ToString
        End If
        Rdr.Close()
        StrSql = "SELECT HSCode, HSDescription, Quantity, Unit FROM BoeInItem WHERE BoeNo = '" & TxtBillin.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        DatBoe.Load(Rdr)
        Rdr.Close()
        GdvBoe.DataSource = DatBoe
        GdvBoe.DataBind()
    End Sub

    Private Sub ClearAll()
        TxtDate.Text = ""
        TxtClient.Text = ""
        TxtRevDate.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtRevision.Text = ""
        GdvBoe.DataSource = Nothing
        GdvBoe.DataBind()
    End Sub

    Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT TOP 1 BillNo FROM BoeIn WHERE BillNo > '" & TxtBillin.Text & "'"
        Try
            TxtBillin.Text = Cmd.ExecuteScalar.ToString
            TxtBillin_TextChanged(Me, e)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ButPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButPrevious.Click
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT TOP 1 BillNo FROM BoeIn WHERE BillNo < '" & TxtBillin.Text & "' ORDER BY BillNo DESC"
        Try
            TxtBillin.Text = Cmd.ExecuteScalar.ToString
            TxtBillin_TextChanged(Me, e)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub GdvBoe_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvBoe.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim TxtQty As TextBox = DirectCast(e.Row.FindControl("TxtNewQty"), TextBox)
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = "document.getElementById('" & TxtQty.ClientID & "').select();"
            If e.Row.RowIndex Mod 2 > 0 Then
                TxtQty.BackColor = GdvBoe.AlternatingRowStyle.BackColor
            End If
        End If
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Try
            For Each row As GridViewRow In GdvBoe.Rows
                If row.RowType = DataControlRowType.DataRow Then
                    Dim NewQty As TextBox = DirectCast(row.FindControl("TxtNewQty"), TextBox)
                    If NewQty.Text <> "" Then
                        StrSql = "INSERT INTO Revision (BoeNo,RevisionNo,RevDate,HSCode,HSDescription,OQuantity,NewQuantity,Unit) VALUES('"
                        StrSql = StrSql & TxtBillin.Text & "',"
                        StrSql = StrSql & Val(TxtRevision.Text) & ",'"
                        StrSql = StrSql & CDate(TxtRevDate.Text).ToString("dd-MMM-yyyy") & "','"
                        StrSql = StrSql & ChkSpace(row.Cells(0).Text) & "','"
                        StrSql = StrSql & ChkSpace(row.Cells(1).Text) & "',"
                        StrSql = StrSql & Val(row.Cells(2).Text) & ","
                        StrSql = StrSql & Val(NewQty.Text) & ",'"
                        StrSql = StrSql & ChkSpace(row.Cells(4).Text) & "')"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If
                End If
            Next
            StrSql = "UPDATE BoeIn SET Revision = " & Val(TxtRevision.Text) & ", Discrepancy = 0 "
            StrSql = StrSql & "WHERE BillNo = '" & TxtBillin.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ClearAll()
            TxtBillin.Focus()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Error Saving Data"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Or Txt = "Select..." Then
            Return ""
        Else
            Return Txt
        End If
    End Function
End Class