﻿Public Partial Class ReleaseContainers
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdScalar As New OleDb.OleDbCommand
    Dim RdrScalar As OleDb.OleDbDataReader
    Dim Rdr As OleDb.OleDbDataReader
    Dim RdrCont As OleDb.OleDbDataReader
    Dim CmdSystem As New OleDb.OleDbCommand
    Dim RdrSystem As OleDb.OleDbDataReader
    Dim CmdInvoice As New OleDb.OleDbCommand
    Dim RdrInvoice As OleDb.OleDbDataReader
    Dim CmdContDetail As New OleDb.OleDbCommand
    Dim RdrContDetail As OleDb.OleDbDataReader
    Dim CmdContainer As New OleDb.OleDbCommand
    Dim RdrConatiner As OleDb.OleDbDataReader
    Dim CmdBilling As New OleDb.OleDbCommand
    Dim RdrBilling As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrComboList As String
    Dim LngInvoiceNo As Long
    Dim BlnPrefClient As Boolean
    Dim SngVat As Single
    Dim StrClientID As String

    Const MENUID = "ReleaseContainers"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT * FROM Traffic ORDER BY Name"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbTraffic.Items.Add("Select...")
            While Rdr.Read
                CmbTraffic.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub BtnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnFind.Click
        GdvFind.DataSource = Nothing
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        If RbnClient.Text = "Client" Then
            TxtBoeNo.Text = GdvFind.SelectedRow.Cells(0).Text
            TxtBoeNo_TextChanged(Me, e)
        Else
            TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
            TxtClientID_TextChanged(Me, e)
        End If
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        If RbnClient.Text = "Name" Then
            StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
            If RbnClient.Checked Then
                StrSql = StrSql & "Name"
            ElseIf RbnCountry.Checked Then
                StrSql = StrSql & "City"
            Else
                StrSql = StrSql & "Contact"
            End If
            StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Else
            StrSql = "SELECT Containers.ContainerNo, CONVERT(Char,Containers.DateIn,103) 'Container Date', Containers.[Type], Client.Name FROM Containers, Client WHERE "
            StrSql = StrSql & "Containers.ClientID = Client.ClientID "
            If RbnClient.Checked Then
                StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCountry.Checked Then
                StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnTraffic.Checked Then
                StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCont.Checked Then
                StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
            End If
            If ChkDate.Checked Then
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                StrSql = StrSql & "AND Containers.DateIn >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
                StrSql = StrSql & "AND Containers.DateIn <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
            End If
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvCont.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCont.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

  Protected Sub TxtBoeNo_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtBoeNo.TextChanged
        'Dim StrCont As String
        'Dim I As Integer
        'Dim IntTab As Integer
        'Dim ArrCont() As String
        'Dim ArrCells() As String
        'Dim count As Integer
        'Dim dt As DataTable
        'Dim dr As DataRow
        'Dim ColType As DataColumn
        'Dim ColContNo As DataColumn
        'Dim ColRelease As DataColumn
        'If TxtBoeNo.Text = "" Then
        '    TxtDate.Text = Today.Date
        '    'TxtClient.Focus()
        '    Exit Sub
        'End If
        'Cmd.Connection = Con
        'CmdScalar.Connection = Con
        'CmdSystem.Connection = Con
        'Cmd.CommandText = "SELECT * FROM BoeOut WHERE BillNo = '" & TxtBoeNo.Text & "' ORDER BY BillNo"
        'Rdr = Cmd.ExecuteReader
        'If Not Rdr.Read Then
        '    LblMessage.Visible = True
        '    LblMessage.Text = "Invalid Outbound Bill"
        '    TxtBoeNo.Focus()
        'Else
        '    'On Error Resume Next
        '    TxtRevision.Text = Rdr("Revision")
        '    TxtDate.Text = Rdr("BillDate")
        '    CmbTraffic.Text = Rdr("TrafficOp")
        '    'TxtCountry = Rdr("Country")
        '    TxtWhCode.Text = Rdr("WhCode")
        '    TxtBoeIn.Text = Rdr("BillInNo")
        '    CmdSystem.CommandText = "SELECT * FROM [System] WHERE WhCode='" & TxtWhCode.Text & "'"
        '    CmdSystem.Connection = Con
        '    RdrSystem = CmdSystem.ExecuteReader
        '    If RdrSystem.Read Then
        '        SngVat = RdrSystem("Vat")
        '    End If
        '    RdrSystem.Close()
        '    StrClientID = Rdr("ClientID")
        '    CmdScalar.CommandText = "SELECT * FROM Client WHERE ClientID = '" & Rdr("ClientID") & "'"
        '    RdrScalar = CmdScalar.ExecuteReader
        '    If RdrScalar.Read Then
        '        'TxtClient.Text = RdrScalar("Name")
        '        If RdrScalar("Category") = "Freeport" Then
        '            SngVat = 0
        '        End If
        '    End If
        '    RdrScalar.Close()
        '    DtpDate.Text = Today.Date
        '    StrCont = Rdr("ContainerNo")

        '    dt = New DataTable()
        '    ColType = New DataColumn("Type", Type.GetType("System.String"))
        '    ColContNo = New DataColumn("Number", Type.GetType("System.String"))
        '    ColRelease = New DataColumn("Release", Type.GetType("System.String"))
        '    dt.Columns.Add(ColType)
        '    dt.Columns.Add(ColContNo)
        '    dt.Columns.Add(ColRelease)
        '    ArrCont = StrCont.Split(Chr(13))
        '    For count = 0 To ArrCont.Length - 1
        '        If Len(ArrCont(count)) > 3 Then
        '            ArrCells = ArrCont(count).Split(vbTab)
        '            If ArrCells.Length = 2 Then
        '                dr = dt.NewRow
        '                dr.Item("Release") = 0
        '                dr.Item("Type") = ArrCells(0)
        '                dr.Item("Number") = ArrCells(1)
        '                dt.Rows.Add(dr)
        '            End If
        '        End If
        '    Next
        '    StrSql = "SELECT * FROM Containers WHERE (BoeNo IN (" & Rdr("BillInNo") & ") OR ClientID='" & Rdr("ClientID") & "') ORDER BY BoeNo"
        '    CmdScalar.CommandText = StrSql
        '    RdrCont = CmdScalar.ExecuteReader
        '    While RdrCont.Read
        '        For I = 1 To dt.Rows.Count - 1
        '            If dt.Rows(I).Item("Number").ToString = RdrCont("ContainerNo").ToString Then
        '                dt.Rows(I).Delete()
        '            End If
        '        Next
        '    End While
        '    RdrCont.Close()
        '    GdvCont.DataSource = dt
        '    GdvCont.DataBind()
        'End If
        'Rdr.Close()
    End Sub

    Private Sub GdvCont_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvCont.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvCont, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    'Protected Sub BtnInvoice_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnInvoice.Click
    '    Dim Trans As OleDb.OleDbTransaction
    '    Dim Int20Ft As Integer
    '    Dim Int40Ft As Integer
    '    Dim Lng20Amt As Long
    '    Dim Lng40Amt As Integer
    '    Dim Int20Rate As Integer
    '    Dim Int40Rate As Integer
    '    Dim LngInvNo As Long
    '    Dim IntQty As Integer
    '    Dim SngAmount As Single
    '    Dim SngActAmount As Single

    '    CmdSystem.Connection = Con
    '    CmdSystem.CommandText = "SELECT NextInvoiceNo FROM [System]"
    '    RdrSystem = CmdSystem.ExecuteReader
    '    If RdrSystem.Read Then
    '        LngInvNo = RdrSystem("NextInvoiceNo")
    '    End If
    '    RdrSystem.Close()
    '    Cmd.CommandText = "UPDATE [System] SET NextInvoiceNo=NextInvoiceNo+1"
    '    Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
    '    Cmd.Connection = Con
    '    Cmd.Transaction = Trans
    '    CmdContainer.Connection = Con
    '    CmdContainer.Transaction = Trans
    '    Cmd.ExecuteNonQuery()
    '    For Each row As GridViewRow In GdvCont.Rows
    '        If row.RowType = DataControlRowType.DataRow Then
    '            Dim Release As CheckBox = DirectCast(row.FindControl("ChkRelease"), CheckBox)
    '            Dim Number As TextBox = DirectCast(row.FindControl("Number"), TextBox)
    '            If Release.Checked = True Then
    '                CmdContainer.CommandText = "SELECT * FROM Containers WHERE ContainerNo='" & row.Cells(1).Text & "'"
    '                RdrConatiner = CmdContainer.ExecuteReader
    '                While RdrConatiner.Read
    '                    StrClientID = RdrConatiner("ClientID")
    '                    StrSql = "INSERT INTO ContDetail(ClientID,InvoiceNo,WhCode,Month,Year,Unit,Description,Rate,BillDate,Quantity,ContNumber,Amount) VALUES('"
    '                    StrSql = StrSql & RdrConatiner("ClientID") & "','"
    '                    StrSql = StrSql & LngInvNo & "','"
    '                    StrSql = StrSql & RdrConatiner("WhCode") & "','"
    '                    StrSql = StrSql & Format(CDate(DtpDate.Text), "MMMM") & "','"
    '                    StrSql = StrSql & Year(CDate(DtpDate.Text)) & "','"
    '                    StrSql = StrSql & "Conatiner" & "','"
    '                    StrSql = StrSql & RdrConatiner("Description") & "','"
    '                    StrSql = StrSql & RdrConatiner("Rate") & "','"
    '                    StrSql = StrSql & Format(CDate(DtpDate.Text), "MM/dd/yyyy") & "','"
    '                    If IsDBNull(RdrConatiner("PaidUpto")) Then
    '                        StrSql = StrSql & (Format(CDate(DtpDate.Text), "dd/MM/yyyy") - Format(RdrConatiner("DateIn"), "dd/MM/yyyy")) + 1 & "','"
    '                        IntQty = (Format(CDate(DtpDate.Text), "dd/MM/yyyy") - Format(RdrConatiner("DateIn"), "dd/MM/yyyy")) + 1
    '                    Else
    '                        StrSql = StrSql & (Format(CDate(DtpDate.Text), "dd/MM/yyyy") - Format(RdrConatiner("PaidUpto"), "dd/MM/yyyy")) + 1 & "','"
    '                        IntQty = (Format(CDate(DtpDate.Text), "dd/MM/yyyy") - Format(RdrConatiner("PaidUpto"), "dd/MM/yyyy")) + 1
    '                    End If
    '                    StrSql = StrSql & RdrConatiner("ContainerNo") & "',"
    '                    If IsDBNull(RdrConatiner("PaidUpto")) Then
    '                        StrSql = StrSql & ((Format(CDate(DtpDate.Text), "dd/MM/yyyy") - Format(RdrConatiner("DateIn"), "dd/MM/yyyy")) + 1) * RdrConatiner("Rate") & ")"
    '                        SngAmount = ((Format(CDate(DtpDate.Text), "dd/MM/yyyy") - Format(RdrConatiner("DateIn"), "dd/MM/yyyy")) + 1) * RdrConatiner("Rate")
    '                    Else
    '                        StrSql = StrSql & ((Format(CDate(DtpDate.Text), "dd/MM/yyyy") - Format(RdrConatiner("PaidUpto"), "dd/MM/yyyy")) + 1) * RdrConatiner("Rate") & ")"
    '                        SngAmount = ((Format(CDate(DtpDate.Text), "dd/MM/yyyy") - Format(RdrConatiner("PaidUpto"), "dd/MM/yyyy")) + 1) * RdrConatiner("Rate")
    '                    End If
    '                    SngActAmount = SngActAmount + SngAmount
    '                    If Left(RdrConatiner("Description"), 2) = "20" Then
    '                        Int20Ft = Int20Ft + IntQty
    '                        Lng20Amt = Lng20Amt + SngAmount
    '                        Int20Rate = RdrConatiner("Rate")
    '                    Else
    '                        Int40Ft = Int40Ft + IntQty
    '                        Lng40Amt = Lng40Amt + SngAmount
    '                        Int40Rate = RdrConatiner("Rate")
    '                        CmdContDetail.CommandText = StrSql
    '                        CmdContDetail.Connection = Con
    '                        CmdContDetail.Transaction = Trans
    '                        CmdContDetail.ExecuteNonQuery()
    '                        StrSql = "UPDATE Containers SET PaidUpto='" & Format(CDate(DtpDate.Text), "MM/dd/yyyy") & "'"
    '                        StrSql = StrSql & " WHERE ContainerNo='" & Number.Text & "'"
    '                        CmdContDetail.CommandText = StrSql
    '                        CmdContDetail.Connection = Con
    '                        CmdContDetail.Transaction = Trans
    '                        CmdContDetail.ExecuteNonQuery()
    '                    End If
    '                End While
    '                RdrConatiner.Close()
    '            End If
    '        End If
    '    Next
    '    ' Add details in billing
    '    If Int20Ft > 0 Then
    '        StrSql = "INSERT INTO Billing(ClientID,Month,Year,Unit,Quantity,Rate,Description,BillDate,Amount,WhCode,InvoiceNo) VALUES('"
    '        StrSql = StrSql & StrClientID & "','"
    '        StrSql = StrSql & Format(CDate(DtpDate.Text), "MMMM") & "','"
    '        StrSql = StrSql & Year(CDate(DtpDate.Text)) & "','"
    '        StrSql = StrSql & "Cont/Day','"
    '        StrSql = StrSql & Int20Ft & "','"
    '        StrSql = StrSql & Int20Rate & "','"
    '        StrSql = StrSql & "20 Ft Containers Stored','"
    '        StrSql = StrSql & Format(CDate(DtpDate.Text), "MM/dd/yyyy") & "',"
    '        StrSql = StrSql & Lng20Amt & ",'"
    '        StrSql = StrSql & RdrConatiner("WhCode") & "','"
    '        StrSql = StrSql & LngInvNo & "')"
    '        CmdBilling.CommandText = StrSql
    '        CmdBilling.Connection = Con
    '        CmdBilling.Transaction = Trans
    '        CmdBilling.ExecuteNonQuery()
    '    End If
    '    If Int40Ft > 0 Then
    '        StrSql = "INSERT INTO Billing(ClientID,Month,Year,Unit,Quantity,Rate,Description,BillDate,Amount,WhCode,InvoiceNo) VALUES('"
    '        StrSql = StrSql & StrClientID & "','"
    '        StrSql = StrSql & Format(CDate(DtpDate.Text), "MMMM") & "','"
    '        StrSql = StrSql & Year(CDate(DtpDate.Text)) & "','"
    '        StrSql = StrSql & "Cont/Day','"
    '        StrSql = StrSql & Int40Ft & "','"
    '        StrSql = StrSql & Int40Rate & "','"
    '        StrSql = StrSql & "40 Ft Containers Stored','"
    '        StrSql = StrSql & Format(CDate(DtpDate.Text), "MM/dd/yyyy") & "',"
    '        StrSql = StrSql & Lng40Amt & ",'"
    '        StrSql = StrSql & RdrConatiner("WhCode") & "','"
    '        StrSql = StrSql & LngInvNo & "')"
    '        CmdBilling.CommandText = StrSql
    '        CmdBilling.Connection = Con
    '        CmdBilling.Transaction = Trans
    '        CmdBilling.ExecuteNonQuery()
    '    End If
    '    If SngActAmount > 0 Then
    '        StrSql = "INSERT INTO Invoice(InvoiceNo,ClientID,Amount,Vat,Value,InvDate,Month,Year,Paid,Deleted,WhCode,Remarks) VALUES('"
    '        StrSql = StrSql & LngInvNo & "','"
    '        StrSql = StrSql & StrClientID & "',"
    '        StrSql = StrSql & SngActAmount & ","
    '        StrSql = StrSql & Math.Round(SngAmount * SngVat / 100) & ","
    '        StrSql = StrSql & SngActAmount + (Math.Round(SngAmount * SngVat / 100)) & ",'"
    '        StrSql = StrSql & Format(CDate(DtpDate.Text), "MM/dd/yyyy") & "','"
    '        StrSql = StrSql & Format(CDate(DtpDate.Text), "MMMM") & "','"
    '        StrSql = StrSql & Year(CDate(DtpDate.Text)) & "',0,0,'"
    '        StrSql = StrSql & RdrConatiner("WhCode") & "','"
    '        StrSql = StrSql & "Outbound : " & TxtBoeNo.Text & "')"
    '        CmdInvoice.CommandText = StrSql
    '        CmdInvoice.Connection = Con
    '        CmdInvoice.Transaction = Trans
    '        CmdInvoice.ExecuteNonQuery()
    '    End If
    '    Trans.Commit()
    'End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim Trans As OleDb.OleDbTransaction
        For Each row As GridViewRow In GdvCont.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim Release As CheckBox = DirectCast(row.FindControl("ChkRelease"), CheckBox)
                Dim Number As TextBox = DirectCast(row.FindControl("Number"), TextBox)
                If Release.Checked = True Then
                    StrSql = "UPDATE Containers SET "
                    StrSql = StrSql & "DateOut='" & Format(CDate(DtpDate.Text), "MM/dd/yyyy") & "',"
                    StrSql = StrSql & "BoeOut='" & TxtBoeNo.Text & "'"
                    StrSql = StrSql & " WHERE ContainerNo='" & row.Cells(1).Text & "' And DateIn = '" & Format(CDate(row.Cells(5).Text), "MM/dd/yyyy") & "'"
                    Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
                    CmdInvoice.CommandText = StrSql
                    CmdInvoice.Connection = Con
                    CmdInvoice.Transaction = Trans
                    CmdInvoice.ExecuteNonQuery()
                    Trans.Commit()
                End If
            End If
        Next
        TxtClientID.Text = ""
        TxtName.Text = ""
        TxtBoeNo.Text = ""
        CmbTraffic.SelectedIndex = -1
        DtpDate.Text = ""
        GdvCont.DataSource = Nothing
        GdvCont.DataBind()
        LblMessage.Visible = True
        CmbZone.SelectedIndex = 0
        LblMessage.Text = "Data Save Successfully"
    End Sub

    Private Sub ClearAll()
        TxtBoeNo.Text = ""
        CmbTraffic.SelectedIndex = -1
        DtpDate.Text = ""
        GdvCont.DataSource = Nothing
        GdvCont.DataBind()
    End Sub

    Protected Sub ButFindCli_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindCli.Click
        RbnClient.Text = "Name"
        RbnCountry.Text = "City"
        RbnTraffic.Text = "Contact"
        RbnCont.Visible = False
        ChkDate.Visible = False
        TxtFrom.Visible = False
        TxtTo.Visible = False
        LblToDate.Visible = False
        GdvFind.DataSource = Nothing
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Protected Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Try
            TxtName.Text = Cmd.ExecuteScalar.ToString
            If ChkReleased.Checked = True Then
                Cmd.CommandText = "Select 0, ContainerNo, [Description], [Rate], WhCode, DateIn, DateOut From Containers Where ClientID = '" & TxtClientID.Text & "' And DateOut Is Not Null And WhCode = '" & CmbZone.Text & "' Order By DateOut Desc"
            Else
                Cmd.CommandText = "Select 0, ContainerNo, [Description], [Rate], WhCode, DateIn,DateOut From Containers Where ClientID = '" & TxtClientID.Text & "' And DateOut Is Null And WhCode = '" & CmbZone.Text & "'"
            End If
            Rdr = Cmd.ExecuteReader
            GdvCont.DataSource = Rdr
            GdvCont.DataBind()
            Rdr.Close()
        Catch ex As Exception
            TxtName.Text = "Invalid Client"
        End Try
    End Sub


    Protected Sub ButRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRef.Click
        TxtClientID_TextChanged(Me, e)
    End Sub
End Class