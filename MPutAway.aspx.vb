﻿Public Partial Class MPutAway
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "ManualPutAway"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbFpZone.Items.Add("Select...")
            While Rdr.Read
                CmbFpZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            StrSql = "SELECT ZoneCode FROM Zones"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("ZoneCode"))
            End While
            Rdr.Close()
            TxtLotNo.Style("text-align") = "right"
            TxtLotNo.Text = "1"
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT BoeIn.BillNo, CONVERT(Char,BoeIn.BillDate,103) 'Bill Date', BoeIn.Revision, Client.Name FROM BoeIn, Client WHERE "
        StrSql = StrSql & "BoeIn.ClientID = Client.ClientID "
        If RbnClient.Checked Then
            StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCountry.Checked Then
            StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnTraffic.Checked Then
            StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCont.Checked Then
            StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
        End If
        If ChkDate.Checked Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = StrSql & "AND Boein.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND Boein.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        RbnClient.Text = "Client"
        RbnClient.Checked = True
        RbnCountry.Text = "Country"
        RbnTraffic.Text = "Traffic Op"
        RbnCont.Visible = True
        ChkDate.Visible = True
        TxtFrom.Visible = True
        TxtTo.Visible = True
        LblToDate.Visible = True
        GdvFind.DataSource = Nothing
        TxtTo.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtFrom.Text = Date.Today.AddDays(-10).ToString("dd/MM/yyyy")
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtBillNo.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtBillNo_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvPallet.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvPallet.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvFindLoc.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFindLoc.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtBillNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBillNo.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT BoeIn.WhCode,BoeIn.Billdate,Client.Name FROM BoeIn,Client WHERE BoeIn.ClientID=Client.ClientID AND  BoeIn.BillNo = '" & TxtBillNo.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearAll()
        On Error Resume Next
        If Rdr.Read Then
            TxtDate.Text = Rdr("BillDate")
            TxtName.Text = Rdr("Name")
            CmbFpZone.Text = Rdr("WhCode")
        Else
            TxtName.Text = "Invalid Inbound"
            Exit Sub
        End If
        Rdr.Close()
        GetPallets()
    End Sub

    Private Sub GetPallets()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT DateIn,* FROM Pallets WHERE LotNo = " & TxtLotNo.Text & " AND BoeNo = '" & TxtBillNo.Text & "' "
        StrSql = StrSql & " ORDER BY PalletID"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvPallet.DataSource = Rdr
        GdvPallet.DataBind()
        Rdr.Close()
        TxtDatein.Text = Cmd.ExecuteScalar
    End Sub

    Private Sub GdvPallet_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvPallet.RowDataBound
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvPallet, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvPallet_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvPallet.SelectedIndexChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        TxtPalletID.Text = ChkSpace(GdvPallet.SelectedRow.Cells(0).Text)
        TxtLoc1.Text = ChkSpace(GdvPallet.SelectedRow.Cells(2).Text)
        TxtLoc2.Text = ChkSpace(GdvPallet.SelectedRow.Cells(4).Text)
        TxtLoc3.Text = ChkSpace(GdvPallet.SelectedRow.Cells(6).Text)
        Select Case Val(GdvPallet.SelectedRow.Cells(1).Text)
            Case 1
                TxtRatio1.Text = "1.0"
                TxtRatio2.Text = ""
                TxtRatio3.Text = ""
            Case 1.5
                TxtRatio1.Text = "1.0"
                TxtRatio2.Text = "0.5"
                TxtRatio3.Text = ""
            Case 2
                TxtRatio1.Text = "1"
                TxtRatio2.Text = "1"
                TxtRatio3.Text = ""
            Case 2.5
                TxtRatio1.Text = "1.0"
                TxtRatio2.Text = "1.0"
                TxtRatio3.Text = "0.5"
            Case 3
                TxtRatio1.Text = "1.0"
                TxtRatio2.Text = "1.0"
                TxtRatio3.Text = "1.0"
        End Select
        StrSql = "SELECT PaltDetail.PCode,Product.Description,PaltDetail.Quantity,PaltDetail.Unit,Product.RackOk AS Rackable  "
        StrSql = StrSql & "FROM PaltDetail,Product WHERE PaltDetail.PCode = Product.ProdCode AND PalletID = " & TxtPalletID.Text
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvProd.DataSource = Rdr
        GdvProd.DataBind()
        Rdr.Close()
        TxtLoc1.Focus()
    End Sub

    Private Sub GdvPallet_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvPallet.SelectedIndexChanging
        GdvPallet.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub ButSelLoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelLoc.Click
        PnlFindLoc.Style.Item("display") = "none"
    End Sub

    Private Sub ButRefLoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefLoc.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT * FROM Location WHERE "
        If TxtZone.Text <> "ALL" Then
            StrSql = StrSql & "ZoneCode LIKE '%" & Trim(TxtZone.Text) & "%' AND "
        End If
        If TxtRack.Text <> "ALL" Then
            If TxtRack.Text = "" Then
                StrSql = StrSql & "RackCode Is Null AND "
            Else
                StrSql = StrSql & "RackCode LIKE '%" & Trim(TxtRack.Text) & "%'AND "
            End If
        End If
        If TxtAisle.Text <> "ALL" Then
            StrSql = StrSql & "Aisle LIKE '%" & Trim(TxtAisle.Text) & "%' AND "
        End If
        If TxtBay.Text <> "ALL" Then
            StrSql = StrSql & "Bay LIKE '%" & Trim(TxtBay.Text) & "%' AND "
        End If
        If TxtLevel.Text <> "ALL" Then
            StrSql = StrSql & "Level LIKE '%" & Trim(TxtLevel.Text) & "%' AND "
        End If
        If RbnEmpty.Checked Then
            StrSql = StrSql & " CurPRatio = 0"
        End If
        If RbnPartial.Checked Then
            StrSql = StrSql & " CurPRatio > 0 AND CurPRatio < 1"
        End If
        If RbnFull.Checked Then
            StrSql = StrSql & " CurPRatio >= 1"
        End If
        If Right(StrSql, 4) = "AND " Then
            StrSql = Left(StrSql, Len(StrSql) - 4)
        End If
        If Right(StrSql, 6) = "WHERE " Then
            StrSql = "SELECT * FROM Location"
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFindLoc.DataSource = ds
        GdvFindLoc.DataBind()
    End Sub

    Private Sub GdvFindLoc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFindLoc.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFindLoc, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFindLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFindLoc.SelectedIndexChanged
        Select Case TxtFindLoc.Text
            Case "1"
                TxtLoc1.Text = GdvFindLoc.SelectedRow.Cells(0).Text
                TxtLoc1_TextChanged(Me, e)
            Case "2"
                TxtLoc2.Text = GdvFindLoc.SelectedRow.Cells(0).Text
                TxtLoc2_TextChanged(Me, e)
            Case "3"
                TxtLoc3.Text = GdvFindLoc.SelectedRow.Cells(0).Text
                TxtLoc3_TextChanged(Me, e)
        End Select
        PnlFindLoc.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFindLoc_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFindLoc.SelectedIndexChanging
        GdvFindLoc.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub ButFindLoc1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindLoc1.Click
        If CmbZone.Text <> "Select..." Then
            TxtZone.Text = CmbZone.Text
        End If
        PnlFindLoc.Style.Item("display") = ""
        TxtZone.Focus()
        TxtFindLoc.Text = "1"
    End Sub

    Private Sub ButFindLoc2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFindLoc2.Click
        If CmbZone.Text <> "Select..." Then
            TxtZone.Text = CmbZone.Text
        End If
        PnlFindLoc.Style.Item("display") = ""
        TxtZone.Focus()
        TxtFindLoc.Text = "2"
    End Sub

    Private Sub ButFindLoc3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFindLoc3.Click
        If CmbZone.Text <> "Select..." Then
            TxtZone.Text = CmbZone.Text
        End If
        PnlFindLoc.Style.Item("display") = ""
        TxtZone.Focus()
        TxtFindLoc.Text = "3"
    End Sub

    Protected Sub TxtLoc1_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtLoc1.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim SngAssigned As Single
        Dim StrRackCode As String = ""

        If TxtPalletID.Text <> "" Then
            Dim BlnNoRack As Boolean = False
            StrSql = "SELECT * FROM Location WHERE LocCode = '" & TxtLoc1.Text & "'"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                If IsDBNull(Rdr("RackCode")) Then
                    StrRackCode = ""
                Else
                    StrRackCode = Rdr("RackCode")
                End If
                If GdvProd.Rows(0).Cells(4).Text = "False" And StrRackCode <> "" Then
                    ErrMsg.Text = "This product cannot be racked. Location " & TxtLoc1.Text & " is racked. Sorry!"
                    ErrHead.Text = "Non Rackable Item"
                    ModalPopupExtender1.Show()
                    TxtLoc1.Text = ""
                    TxtLoc1.Focus()
                    Exit Sub
                End If
                ButSave.Enabled = True
                If Rdr("PutHold") Then
                    ErrMsg.Text = "Location " & TxtLoc1.Text & " is on hold for put away. Sorry!"
                    ErrHead.Text = "Putaway Hold"
                    ModalPopupExtender1.Show()
                    TxtLoc1.Text = ""
                    TxtLoc1.Focus()
                    Exit Sub
                End If
                If IsDBNull(Rdr("MaxPRatio")) Then
                    BlnNoRack = True
                    ButSave.Enabled = True
                    Exit Sub
                Else
                    If Rdr("MaxPRatio") = 0 Then
                        BlnNoRack = True
                        ButSave.Enabled = True
                        Exit Sub
                    End If
                End If
                'Check Also if the location has been assigned here
                SngAssigned = 0
                For Each Row As GridViewRow In GdvPallet.Rows
                    If TxtLoc1.Text = Row.Cells(2).Text Then
                        SngAssigned = SngAssigned + Val(Row.Cells(3).Text)
                    End If
                    If TxtLoc2.Text = Row.Cells(4).Text Then
                        SngAssigned = SngAssigned + Val(Row.Cells(5).Text)
                    End If
                    If TxtLoc3.Text = Row.Cells(6).Text Then
                        SngAssigned = SngAssigned + Val(Row.Cells(7).Text)
                    End If
                Next
                If Rdr("MaxPRatio") - Rdr("CurPRatio") - SngAssigned < Val(TxtRatio1.Text) Then
                    ErrMsg.Text = "No space at location " & TxtLoc1.Text & " Sorry!"
                    ErrHead.Text = "No Space"
                    ModalPopupExtender1.Show()
                    TxtLoc1.Text = ""
                    TxtLoc1.Focus()
                    Exit Sub
                End If
                GdvPallet.SelectedRow.Cells(9).Text = Rdr("ZoneCode")
            Else
                ErrMsg.Text = "Location Don't Exit. Select other location"
                ErrHead.Text = "Putaway Hold"
                ModalPopupExtender1.Show()
                Exit Sub
            End If
            Rdr.Close()
            If Val(TxtRatio2.Text) > 0 Then
                StrSql = "SELECT * FROM Location WHERE LocCode = '" & Left(TxtLoc1.Text, Len(TxtLoc1.Text) - 1) & "2'"
                Cmd.CommandText = StrSql
                Rdr = Cmd.ExecuteReader
                If Rdr.Read Then
                    If Rdr("MaxPRatio") - Rdr("CurPRatio") < Val(TxtRatio2.Text) Then
                        ErrMsg.Text = "Not enough space in Location 2!"
                        ErrHead.Text = "No space to Over Hang"
                        ModalPopupExtender1.Show()
                        TxtLoc1.Text = ""
                        TxtLoc1.Focus()
                        Exit Sub
                    Else
                        TxtLoc2.Text = Left(TxtLoc1.Text, Len(TxtLoc1.Text) - 1) & "2"
                    End If
                End If
                Rdr.Close()
            End If
            If Val(TxtRatio3.Text) > 0 Then
                StrSql = "SELECT * FROM Location WHERE LocCode = '" & Left(TxtLoc1.Text, Len(TxtLoc1.Text) - 1) & "3"
                Cmd.CommandText = StrSql
                Rdr = Cmd.ExecuteReader
                If Rdr.Read Then
                    If Rdr("MaxPRatio") - Rdr("CurPRatio") < Val(TxtRatio3.Text) Then
                        MsgBox("Not enough space in Location 3!", vbCritical, "No space to Over Hang")
                        ModalPopupExtender1.Show()
                        TxtLoc1.Text = ""
                        TxtLoc1.Focus()
                        Exit Sub
                    Else
                        TxtLoc3.Text = Left(TxtLoc1.Text, Len(TxtLoc1.Text) - 1) & "3"
                    End If
                End If
                Rdr.Close()
            End If
        End If
        GdvPallet.SelectedRow.Cells(2).Text = TxtLoc1.Text
        GdvPallet.SelectedRow.Cells(3).Text = TxtRatio1.Text
        GdvPallet.SelectedRow.Cells(4).Text = TxtLoc2.Text
        GdvPallet.SelectedRow.Cells(5).Text = TxtRatio2.Text
        GdvPallet.SelectedRow.Cells(6).Text = TxtLoc3.Text
        GdvPallet.SelectedRow.Cells(7).Text = TxtRatio3.Text
    End Sub

    Private Sub TxtLoc2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtLoc2.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If TxtPalletID.Text <> "" Then
            StrSql = "SELECT * FROM Location WHERE LocCode = '" & TxtLoc2.Text & "'"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                If Rdr("PutHold") Then
                    ErrMsg.Text = "Location " & TxtLoc2.Text & " is on hold for put away. Sorry!"
                    ErrHead.Text = "Putaway Hold"
                    ModalPopupExtender1.Show()
                    TxtLoc2.Text = ""
                    TxtLoc2.Focus()
                    Exit Sub
                End If
                If Rdr("MaxPRatio") - Rdr("CurPRatio") < Val(TxtRatio2.Text) Then
                    ErrMsg.Text = "No space at location " & TxtLoc2.Text & " Sorry!"
                    ErrHead.Text = "No Space"
                    ModalPopupExtender1.Show()
                    TxtLoc2.Text = ""
                    TxtLoc2.Focus()
                    Exit Sub
                End If
                If GdvProd.Rows(0).Cells(4).Text = "False" And Rdr("RackCode") <> "" Then
                    ErrMsg.Text = "This product cannot be racked. Location " & TxtLoc1.Text & " is racked. Sorry!"
                    ErrHead.Text = "Non Rackable Item"
                    ModalPopupExtender1.Show()
                    TxtLoc2.Text = ""
                    TxtLoc2.Focus()
                    Exit Sub
                End If
                GdvPallet.SelectedRow.Cells(9).Text = Rdr("ZoneCode")
            Else
                ErrMsg.Text = "Location Don't Exit. Select other location"
                ErrHead.Text = "Putaway Hold"
                ModalPopupExtender1.Show()
                Exit Sub
            End If
            Rdr.Close()
        End If
        GdvPallet.SelectedRow.Cells(2).Text = TxtLoc1.Text
        GdvPallet.SelectedRow.Cells(3).Text = TxtRatio1.Text
        GdvPallet.SelectedRow.Cells(4).Text = TxtLoc2.Text
        GdvPallet.SelectedRow.Cells(5).Text = TxtRatio2.Text
        GdvPallet.SelectedRow.Cells(6).Text = TxtLoc3.Text
        GdvPallet.SelectedRow.Cells(7).Text = TxtRatio3.Text
    End Sub

    Private Sub TxtLoc3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtLoc3.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If TxtPalletID.Text <> "" Then
            StrSql = "SELECT * FROM Location WHERE LocCode = '" & TxtLoc3.Text & "'"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                If Rdr("PutHold") Then
                    ErrMsg.Text = "Location " & TxtLoc3.Text & " is on hold for put away. Sorry!"
                    ErrHead.Text = "Putaway Hold"
                    ModalPopupExtender1.Show()
                    TxtLoc3.Text = ""
                    TxtLoc3.Focus()
                    Exit Sub
                End If
                If Rdr("MaxPRatio") - Rdr("CurPRatio") < Val(TxtRatio3.Text) Then
                    ErrMsg.Text = "No space at location " & TxtLoc3.Text & " Sorry!"
                    ErrHead.Text = "No Space"
                    ModalPopupExtender1.Show()
                    TxtLoc3.Text = ""
                    TxtLoc3.Focus()
                    Exit Sub
                End If
                If GdvProd.Rows(0).Cells(4).Text = "False" And Rdr("RackCode") <> "" Then
                    ErrMsg.Text = "This product cannot be racked. Location " & TxtLoc1.Text & " is racked. Sorry!"
                    ErrHead.Text = "Non Rackable Item"
                    ModalPopupExtender1.Show()
                    TxtLoc2.Text = ""
                    TxtLoc2.Focus()
                    Exit Sub
                End If
                'If Rdr("Level") < RsProduct("LevelFrom") Or Rdr("Level") > RsProduct("LevelTo") Then
                '    MsgBox("Location " & TxtLoc3.Text & " is at wrong level for this product. Sorry!", vbCritical, "Wrong Level")
                '    TxtLoc3.Text = ""
                '    TxtLoc3.Focus()
                '    Exit Sub
                'End If
                GdvPallet.SelectedRow.Cells(9).Text = Rdr("ZoneCode")
            Else
                ErrMsg.Text = "Location Don't Exit. Select other location"
                ErrHead.Text = "Putaway Hold"
                ModalPopupExtender1.Show()
                Exit Sub
            End If
            Rdr.Close()
            GdvPallet.SelectedRow.Cells(2).Text = TxtLoc1.Text
            GdvPallet.SelectedRow.Cells(3).Text = TxtRatio1.Text
            GdvPallet.SelectedRow.Cells(4).Text = TxtLoc2.Text
            GdvPallet.SelectedRow.Cells(5).Text = TxtRatio2.Text
            GdvPallet.SelectedRow.Cells(6).Text = TxtLoc3.Text
            GdvPallet.SelectedRow.Cells(7).Text = TxtRatio3.Text
        End If
    End Sub

    Private Sub TxtRatio1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtRatio1.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If TxtPalletID.Text <> "" Then
            GdvPallet.SelectedRow.Cells(3).Text = TxtRatio1.Text
        End If
    End Sub

    Private Sub TxtRatio2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtRatio2.TextChanged
        If TxtPalletID.Text <> "" Then
            GdvPallet.SelectedRow.Cells(5).Text = TxtRatio2.Text
        End If
    End Sub

    Private Sub TxtRatio3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtRatio3.TextChanged
        If TxtPalletID.Text <> "" Then
            GdvPallet.SelectedRow.Cells(7).Text = TxtRatio3.Text
        End If
    End Sub

    Private Function ChkSpace(ByVal StrText As String) As String
        If StrText = "&nbsp;" Then
            Return ""
        Else
            Return StrText
        End If
    End Function

    Protected Sub TxtLotNo_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtLotNo.TextChanged
        GetPallets()
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            For Each Row As GridViewRow In GdvPallet.Rows
                'If a Location has been assigned earlier then release that location
                StrSql = "UPDATE Location SET CurPRatio = CurPRatio - " & Val(Row.Cells(3).Text) & " WHERE LocCode = "
                StrSql = StrSql & "(SELECT LocCode1 FROM Pallets WHERE PalletID = '" & ChkSpace(Row.Cells(0).Text) & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                StrSql = "UPDATE Location SET CurPRatio = CurPRatio - " & Val(Row.Cells(5).Text) & " WHERE LocCode = "
                StrSql = StrSql & "(SELECT LocCode2 FROM Pallets WHERE PalletID = '" & ChkSpace(Row.Cells(0).Text) & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                StrSql = "UPDATE Location SET CurPRatio = CurPRatio - " & Val(Row.Cells(7).Text) & " WHERE LocCode = "
                StrSql = StrSql & "(SELECT LocCode3 FROM Pallets WHERE PalletID = '" & ChkSpace(Row.Cells(0).Text) & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                'Update Current Ratio of Locations assigned
                If ChkSpace(Row.Cells(2).Text) <> "" Then
                    StrSql = "UPDATE Location SET CurPRatio = CurPRatio + " & Val(Row.Cells(3).Text) & "WHERE LocCode = '" & Row.Cells(2).Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
                If ChkSpace(Row.Cells(4).Text) <> "" Then
                    StrSql = "UPDATE Location SET CurPRatio = CurPRatio + " & Val(Row.Cells(5).Text) & "WHERE LocCode = '" & Row.Cells(4).Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
                If ChkSpace(Row.Cells(6).Text) <> "" Then
                    StrSql = "UPDATE Location SET CurPRatio = CurPRatio + " & Val(Row.Cells(7).Text) & "WHERE LocCode = '" & Row.Cells(6).Text & "'"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
                'Update Pallets with new locations and pallet ratios
                If ChkSpace(Row.Cells(2).Text) <> "" Then
                    Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                    StrSql = "UPDATE Pallets SET LocCode1 = '" & ChkSpace(Row.Cells(2).Text) & "',"
                    StrSql = StrSql & "LocCode2 = '" & ChkSpace(Row.Cells(4).Text) & "',"
                    StrSql = StrSql & "LocCode3 = '" & ChkSpace(Row.Cells(6).Text) & "',"
                    StrSql = StrSql & "DateIn = '" & ChkSpace(Row.Cells(8).Text) & "',"
                    StrSql = StrSql & "PRatio1 = " & Val(Row.Cells(3).Text) & ","
                    StrSql = StrSql & "PRatio2 = " & Val(Row.Cells(5).Text) & ","
                    StrSql = StrSql & "PRatio3 = " & Val(Row.Cells(7).Text) & ","
                    StrSql = StrSql & "ZoneCode = '" & ChkSpace(Row.Cells(9).Text) & "',"
                    StrSql = StrSql & "PutAway = 1,Cleared = 0 "
                    StrSql = StrSql & "WHERE PalletID = " & ChkSpace(Row.Cells(0).Text)
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            Trans.Commit()
            TxtBillNo_TextChanged(Me, e)
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Putaway Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ClearAll()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        TxtDate.Text = Date.Today.ToString("dd-MMM-yyyy")
        TxtDatein.Text = ""
        TxtName.Text = ""
        TxtPalletID.Text = ""
        TxtLoc1.Text = ""
        TxtLoc2.Text = ""
        TxtLoc3.Text = ""
        TxtRatio1.Text = ""
        TxtRatio2.Text = ""
        TxtRatio3.Text = ""
        TxtLotNo.Text = "1"
        CmbFpZone.SelectedIndex = 0
        CmbZone.SelectedIndex = 0
        GdvProd.DataSource = Nothing
        GdvProd.DataBind()
        GdvPallet.DataSource = Nothing
        GdvPallet.DataBind()
    End Sub
End Class