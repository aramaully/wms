﻿Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.Web.UI.WebControls

Partial Public Class BillIn
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdChkClient As New OleDb.OleDbCommand
    Dim RdrChkClient As OleDb.OleDbDataReader
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrGetItem As String
    Dim DatCont As New DataTable
    Dim CmdCont As New OleDb.OleDbCommand
    Dim RdrCont As OleDb.OleDbDataReader
    Dim CmdProd As New OleDb.OleDbCommand
    Dim RdrProd As OleDb.OleDbDataReader
    Dim CmdHsCode As New OleDb.OleDbCommand
    Dim RdrHsCode As OleDb.OleDbDataReader
    Dim CmdChkHSItem As New OleDb.OleDbCommand
    Dim IntItem As Integer = 0
    Dim IntContNo As Integer = 0

    Const MENUID = "BillInbound"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()

            StrSql = "SELECT UnitCode FROM Unit ORDER BY UnitCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbProdUnit.Items.Add("Select...")
            While Rdr.Read
                CmbProdUnit.Items.Add(Rdr("UnitCode"))
            End While
            Rdr.Close()

            StrSql = "SELECT Name FROM Traffic ORDER BY Name"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbTrafficOp.Items.Add("Select...")
            CmbTrafficOp.Items.Add("")
            While Rdr.Read
                CmbTrafficOp.Items.Add(Rdr("Name"))
            End While
            Rdr.Close()

            StrSql = "SELECT CountryCode FROM Country ORDER BY CountryCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbHSOrigin.Items.Add("Select...")
            CmbHSOrigin.Items.Add("")
            While Rdr.Read
                CmbHSOrigin.Items.Add(Rdr("CountryCode"))
            End While
            Rdr.Close()

            StrSql = "SELECT UnitCode FROM Unit ORDER BY UnitCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbHSUnit.Items.Add("Select...")
            CmbHSUnit.Items.Add("")
            While Rdr.Read
                CmbHSUnit.Items.Add(Rdr("UnitCode"))
            End While
            Rdr.Close()

            StrSql = "SELECT * FROM Regime Where [Type] = 'In' ORDER BY CpcCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbCPC.Items.Add("Select...")
            CmbCPC.Items.Add("")
            While Rdr.Read
                CmbCPC.Items.Add(Rdr("CpcCode"))
            End While
            Rdr.Close()
            TxtDate.Text = Date.Today.ToString("dd-MMM-yyyy")

            CmbItemType.Items.Add("Bonded")
            CmbItemType.Items.Add("Constructive")

        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvCont.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCont.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvProduct.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvProduct.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvHSCode.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvHSCode.UniqueID, "Select$" & i)
        Next

        MyBase.Render(writer)
    End Sub


    Private Sub ButAddCont_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAddCont.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColType As DataColumn
        Dim ColContNo As DataColumn



        dt = New DataTable()
        ColType = New DataColumn("Type", Type.GetType("System.String"))
        ColContNo = New DataColumn("ContNo", Type.GetType("System.String"))
        dt.Columns.Add(ColType)
        dt.Columns.Add(ColContNo)
        On Error Resume Next
        For Each row As GridViewRow In GdvCont.Rows
            If row.RowType = DataControlRowType.DataRow Then
                dr = dt.NewRow
                dr.Item("Type") = DirectCast(row.FindControl("Type"), TextBox).Text
                If DirectCast(row.FindControl("Type"), TextBox).Text <> "" Then
                    IntContNo = IntContNo + 1
                End If
                dr.Item("ContNo") = DirectCast(row.FindControl("ContNo"), TextBox).Text
                dt.Rows.Add(dr)
            End If
        Next
        TxtCont.Text = IntContNo
        dr = dt.NewRow()
        dr("Type") = ""
        dr("ContNo") = ""
        dt.Rows.Add(dr)
        GdvCont.DataSource = dt
        GdvCont.DataBind()

    End Sub

    Private Sub ButDelCont_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelCont.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColType As DataColumn
        Dim ColContNo As DataColumn
        Dim IntContNo As Integer = 0
        dt = New DataTable()
        ColType = New DataColumn("Type", Type.GetType("System.String"))
        ColContNo = New DataColumn("ContNo", Type.GetType("System.String"))
        dt.Columns.Add(ColType)
        dt.Columns.Add(ColContNo)

        On Error Resume Next
        For Each row As GridViewRow In GdvCont.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim ChkDel As CheckBox = DirectCast(row.FindControl("ChkSelect"), CheckBox)
                If Not ChkDel.Checked Then
                    dr = dt.NewRow
                    dr.Item("Type") = DirectCast(row.FindControl("Type"), TextBox).Text
                    If DirectCast(row.FindControl("Type"), TextBox).Text <> "" Then
                        IntContNo = IntContNo + 1
                        TxtCont.Text = IntContNo
                    End If
                    dr.Item("ContNo") = DirectCast(row.FindControl("ContNo"), TextBox).Text
                    dt.Rows.Add(dr)
                End If
            End If
        Next
        GdvCont.DataSource = dt
        GdvCont.DataBind()
    End Sub

    Private Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        ClearAll()
        TxtBillIn.Text = ""
        CmbZone.Focus()
    End Sub

    Private Sub TxtBillIn_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBillIn.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim dt As DataTable
        Dim dr As DataRow
        Dim ColType As DataColumn
        Dim ColContNo As DataColumn
        Dim StrGetItem As String
        Dim ev As New System.EventArgs
        If Trim(TxtBillIn.Text) <> "" Then
            Cmd.Connection = Con
            Cmd.CommandText = "Select *From BoeIn Where BillNo = '" & TxtBillIn.Text & "'"
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                CmbZone.Text = Rdr("WhCode")
                If Not IsDBNull(Rdr("BillDate")) Then
                    TxtDate.Text = Format(Rdr("BillDate"), "dd/MM/yyyy")
                Else
                    TxtDate.Text = ""
                End If
                TxtClientID.Text = Rdr("ClientID")
                TxtClientID_TextChanged(Me, ev)
                If Rdr("TrafficOp") <> "" Then
                    CmbTrafficOp.Text = Rdr("TrafficOp")
                End If
                TxtItems.Text = Rdr("Items")
                TxtTruckNo.Text = Rdr("TruckNo")
                TxtRemarks.Text = Rdr("Remarks")
                TxtCont.Text = Rdr("Container")
                CmbCPC.Text = Rdr("TransType")
            End If
            Rdr.Close()
            'Update Container in Grid
            CmdCont.Connection = Con
            dt = New DataTable()
            ColType = New DataColumn("Type", Type.GetType("System.String"))
            ColContNo = New DataColumn("ContNo", Type.GetType("System.String"))
            dt.Columns.Add(ColType)
            dt.Columns.Add(ColContNo)
            StrGetItem = "SELECT ContType,ContainerNo From NewContDetails "
            StrGetItem = StrGetItem & "WHERE BoeNo = '" & TxtBillIn.Text & "' Order By ContType"
            CmdCont.CommandText = StrGetItem
            RdrCont = CmdCont.ExecuteReader
            While RdrCont.Read
                dr = dt.NewRow
                dr.Item("Type") = RdrCont("ContType")
                dr.Item("ContNo") = RdrCont("ContainerNo")
                dt.Rows.Add(dr)
            End While
            GdvCont.DataSource = dt
            GdvCont.DataBind()

            CmdHsCode.Connection = Con
            CmdHsCode.CommandText = "Select ItemNo, HsCode, HsDescription, Quantity, Unit, Weight, Value, FOB, CountryofOrigin From BoeInItem Where BoeNo = '" & TxtBillIn.Text & "' Order By ItemNo"
            RdrHsCode = CmdHsCode.ExecuteReader
            GdvHSCode.DataSource = RdrHsCode
            GdvHSCode.DataBind()
            RdrHsCode.Close()


        End If
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        CmdChkClient.Connection = Con
        CmdChkClient.CommandText = "SELECT *FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        RdrChkClient = CmdChkClient.ExecuteReader
        If RdrChkClient.Read Then
            TxtName.Text = RdrChkClient("Name")
            'If RdrChkClient("Category") = "Suspended Duty" Then
            '    If CDate(RdrChkClient("PermitExp")) < CDate(TxtDate.Text) Then
            '        ErrHead.Text = "Save Failed"
            '        ErrMsg.Text = "License Expired"
            '        ModalPopupExtender1.Show()
            '        'ClearAll()
            '        Exit Sub
            '    End If
            'ElseIf RdrChkClient("Category") = "Freeport" Then
            '    If CDate(RdrChkClient("LicenceExp")) < CDate(TxtDate.Text) Then
            '        ErrHead.Text = "Save Failed"
            '        ErrMsg.Text = "License Expired"
            '        ModalPopupExtender1.Show()
            '        'ClearAll()
            '        Exit Sub
            '    End If
            'End If
        End If
        RdrChkClient.Close()

    End Sub

    Private Sub GdvProduct_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvProduct.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvProduct, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Or Txt = "Select..." Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Private Sub ButAddProd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAddProd.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim CmdProdItem As New OleDb.OleDbCommand
        Dim RdrProdItem As OleDb.OleDbDataReader
        Dim CmdProduct As New OleDb.OleDbCommand

        Dim StrProd As String
        If TxtBillIn.Text = "" Then
            LblText.Visible = True
            LblText.Text = "Please Enter Bill Number"
            Exit Sub
        End If
        If TxtHSItemNo.Text = "" Then
            LblText.Visible = True
            LblText.Text = "Please Select Under Which Item"
            Exit Sub
        End If
        CmdProdItem.Connection = Con
        CmdProduct.Connection = Con
        If TxtProductCode.Text <> "" Then
            CmdProdItem.CommandText = "Delete From BoeInProd Where BoeNo = '" & TxtBillIn.Text & "' And PCode = '" & ChkSpace(TxtProductCode.Text) & "' And ItemNo = '" & TxtHSItemNo.Text & "'"
            CmdProdItem.ExecuteNonQuery()
            StrSql = "Insert Into BoeInProd(BoeNo,ItemNo,PCode,ProdDescription,Quantity,UnitCode,BalQuantity,ItemType,DateIn)VALUES('"
            StrSql = StrSql & TxtBillIn.Text & "','"
            StrSql = StrSql & TxtHSItemNo.Text & "','"
            StrSql = StrSql & ChkSpace(TxtProductCode.Text) & "','"
            StrSql = StrSql & ChkSpace(TxtItemDesc.Text) & "',"
            StrSql = StrSql & Val(TxtProdQtyUnit.Text) & ",'"
            StrSql = StrSql & ChkSpace(CmbProdUnit.Text) & "',"
            StrSql = StrSql & Val(TxtProdQtyUnit.Text) & ",'" & CmbItemType.Text & "',"
            If TxtDateIn.Text = "" Then
                StrSql = StrSql & "Null"
            Else
                StrSql = StrSql & "'" & Format(CDate(TxtDateIn.Text), "MM/dd/yyyy") & "')"
            End If
            CmdProdItem.CommandText = StrSql
            CmdProdItem.ExecuteNonQuery()

            CmdProduct.CommandText = "Delete From Product Where ProdCode = '" & ChkSpace(TxtProductCode.Text) & "'"
            CmdProduct.ExecuteNonQuery()
            StrSql = "Insert Into Product(ProdCode, Description, HsCode, Unit)Values('"
            StrSql = StrSql & ChkSpace(TxtProductCode.Text) & "','"
            StrSql = StrSql & ChkSpace(TxtItemDesc.Text) & "','"
            StrSql = StrSql & ChkSpace(TxtHsCode.Text) & "','"
            StrSql = StrSql & ChkSpace(CmbProdUnit.Text) & "')"
            CmdProduct.CommandText = StrSql
            CmdProduct.ExecuteNonQuery()
            TxtProductCode.Text = ""
            TxtItemDesc.Text = ""
            TxtProdQtyUnit.Text = ""
            CmbProdUnit.SelectedIndex = -1
        End If
        'Update Products in Grid
        StrProd = "SELECT [LineNo],Pcode,ProdDescription,Quantity,UnitCode,ItemType,DateIn FROM BoeInProd "
        StrProd = StrProd & "WHERE BoeNo = '" & TxtBillIn.Text & "' And ItemNo = '" & TxtHSItemNo.Text & "' ORDER By PCode"
        CmdProdItem.CommandText = StrProd
        RdrProdItem = CmdProdItem.ExecuteReader
        GdvProduct.DataSource = RdrProdItem
        GdvProduct.DataBind()
        RdrProdItem.Close()
        For Each row As GridViewRow In GdvProduct.Rows
            If row.Cells(1).Text <> "" Then
                IntItem = IntItem + 1
                CmdProdItem.CommandText = "Update BoeInProd Set [LineNo] = '" & IntItem & "' WHERE BoeNo = '" & TxtBillIn.Text & "' And ItemNo = '" & TxtHSItemNo.Text & "' "
                row.Cells(0).Text = IntItem
            End If
        Next
    End Sub

    Private Sub ButDelProd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelProd.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim DatCont As New DataTable
        Dim Dtrow As DataRow
        Dim BlnDeleted As Boolean = False
        Dim CmdProdItem As New OleDb.OleDbCommand
        Dim RdrProdItem As OleDb.OleDbDataReader
        Dim StrProd As String
        Cmd.Connection = Con
        CmdProdItem.Connection = Con
        If TxtBillIn.Text <> "" Then
            Cmd.CommandText = "Delete From BoeInProd Where BoeNo = '" & TxtBillIn.Text & "' And PCode = '" & TxtProductCode.Text & "'"
            Cmd.ExecuteNonQuery()
            TxtProductCode.Text = ""
            TxtItemDesc.Text = ""
            TxtProdQtyUnit.Text = ""
            CmbProdUnit.SelectedIndex = -1
        End If

        'Update Products in Grid
        StrProd = "SELECT [LineNo],Pcode,ProdDescription,Quantity,UnitCode FROM BoeInProd "
        StrProd = StrProd & "WHERE BoeNo = '" & TxtBillIn.Text & "' ORDER By PCode"
        CmdProdItem.CommandText = StrProd
        RdrProdItem = CmdProdItem.ExecuteReader
        GdvProduct.DataSource = RdrProdItem
        GdvProduct.DataBind()
        For Each row As GridViewRow In GdvProduct.Rows
            If row.Cells(1).Text <> "" Then
                IntItem = IntItem + 1
                row.Cells(0).Text = IntItem
            End If
        Next
    End Sub

    Private Sub GdvProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvProduct.SelectedIndexChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        On Error Resume Next
        TxtProductCode.Text = GdvProduct.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
        TxtItemDesc.Text = GdvProduct.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
        TxtProdQtyUnit.Text = GdvProduct.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
        CmbProdUnit.Text = GdvProduct.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
        CmbItemType.Text = GdvProduct.SelectedRow.Cells(5).Text.Replace("&nbsp;", "")
        TxtDateIn.Text = GdvProduct.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
    End Sub

    Private Sub GdvProduct_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvProduct.SelectedIndexChanging
        GdvProduct.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim Trans As OleDb.OleDbTransaction
        Dim CmdChkProd As New OleDb.OleDbCommand
        Dim RdrChkProd As OleDb.OleDbDataReader
        Dim CmdChkRegime As New OleDb.OleDbCommand
        Dim RdrChkRegime As OleDb.OleDbDataReader
        Dim StrHSDescription As String
        Dim StrHSCode As String

        IntContNo = 0
        For Each row As GridViewRow In GdvCont.Rows
            If row.RowType = DataControlRowType.DataRow Then
                If DirectCast(row.FindControl("Type"), TextBox).Text <> "" Then
                    IntContNo = IntContNo + 1
                End If
            End If
        Next
        TxtCont.Text = IntContNo
        If CmbCPC.SelectedIndex = 0 Then
            LblText.Visible = True
            LblText.Text = "Please Input Corect CPC Code"
            Exit Sub
        End If

        'CmdChkClient.Connection = Con
        'CmdChkClient.CommandText = "SELECT *FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        'RdrChkClient = CmdChkClient.ExecuteReader
        'If RdrChkClient.Read Then
        '    TxtName.Text = RdrChkClient("Name")
        '    If RdrChkClient("Category") = "Suspended Duty" Then
        '        If CDate(RdrChkClient("PermitExp")) < CDate(TxtDate.Text) Then
        '            ErrHead.Text = "Save Failed"
        '            ErrMsg.Text = "License Expired"
        '            ModalPopupExtender1.Show()
        '            'ClearAll()
        '            Exit Sub
        '        End If
        '    ElseIf RdrChkClient("Category") = "Freeport" Then
        '        If CDate(RdrChkClient("LicenceExp")) < CDate(TxtDate.Text) Then
        '            ErrHead.Text = "Save Failed"
        '            ErrMsg.Text = "License Expired"
        '            ModalPopupExtender1.Show()
        '            'ClearAll()
        '            Exit Sub
        '        End If
        '    End If
        'End If
        'RdrChkClient.Close()



        Cmd.Connection = Con
        CmdChkProd.Connection = Con
        CmdChkRegime.Connection = Con
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Transaction = Trans
        CmdChkProd.Transaction = Trans
        CmdChkRegime.Transaction = Trans
        Cmd.CommandText = "Delete from BoeIn WHERE BillNo = '" & TxtBillIn.Text & "'"
        Cmd.ExecuteNonQuery()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Try
            StrSql = "INSERT INTO BoeIn (BillNo,BillDate,ClientID,Items,Container,TruckNo,"
            StrSql = StrSql & "TrafficOp,Remarks,UserID,WhCode,Posted,Discrepancy,CrossDock,TransType,Country,SeaAir,Transport,ContainerNo,GrnSlNo) VALUES('"
            StrSql = StrSql & TxtBillIn.Text & "',"
            If TxtDate.Text = "" Then
                StrSql = StrSql & "NULL,'"
            Else
                StrSql = StrSql & "'" & CDate(TxtDate.Text).ToString("dd-MMM-yyyy") & "','"
            End If
            StrSql = StrSql & TxtClientID.Text & "',"
            StrSql = StrSql & Val(TxtItems.Text) & ","
            StrSql = StrSql & Val(TxtCont.Text) & ",'"
            StrSql = StrSql & ChkSpace(TxtTruckNo.Text) & "','"
            StrSql = StrSql & ChkSpace(CmbTrafficOp.Text) & "','"
            StrSql = StrSql & TxtRemarks.Text & "','"
            StrSql = StrSql & Session("UserID") & "','"
            StrSql = StrSql & ChkSpace(CmbZone.Text) & "',"
            StrSql = StrSql & "0,0,0,'"
            StrSql = StrSql & ChkSpace(CmbCPC.Text) & "',"
            StrSql = StrSql & "'','','','','')"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()

            'Update ContainerDetails
            Cmd.CommandText = "DELETE FROM NewContDetails WHERE BoeNo = '" & TxtBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            For Each row As GridViewRow In GdvCont.Rows
                StrSql = "INSERT INTO NewContDetails (BoeNo,ContType,ContainerNo"
                StrSql = StrSql & ") VALUES('"
                StrSql = StrSql & TxtBillIn.Text & "','"
                If row.RowType = DataControlRowType.DataRow Then
                    If DirectCast(row.FindControl("Type"), TextBox).Text <> "" Then
                        StrSql = StrSql & DirectCast(row.FindControl("Type"), TextBox).Text & "','"
                        StrSql = StrSql & DirectCast(row.FindControl("ContNo"), TextBox).Text & "')"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If
                End If
            Next
            Trans.Commit()
            ClearAll()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Save Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCancel.Click
        ModalPopupExtender2.Hide()
        ErrHead.Text = "User Cancelled"
        ErrMsg.Text = "You have cancelled the save process. The data is not saved. To save the data Click on Save again."
        ModalPopupExtender1.Show()
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            Cmd.CommandText = "DELETE FROM BoeIn WHERE BillNo = '" & TxtBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "DELETE FROM BoeInProd WHERE BoeNo = '" & TxtBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "Delete FROM BoeInItem Where BoeNo = '" & TxtBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "Delete From NewContDetails Where BoeNo = '" & TxtBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "DELETE FROM Amendments WHERE BoeNo = '" & TxtBillIn.Text & "'"
            Cmd.ExecuteNonQuery()
            ClearAll()
            LblText.Visible = True
            LblText.Text = "Bill Deleted Successfull"
            Trans.Commit()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub TxtProductCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtProductCode.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim CmdProduct As New OleDb.OleDbCommand
        Dim RdrProduct As OleDb.OleDbDataReader
        CmdProduct.Connection = Con
        CmdProduct.CommandText = "SELECT * FROM Product WHERE ProdCode = '" & TxtProductCode.Text & "'"
        RdrProduct = CmdProduct.ExecuteReader
        If RdrProduct.Read Then
            TxtItemDesc.Text = RdrProduct("Description").ToString
            If IsDBNull(RdrProduct("Unit")) Then
                CmbProdUnit.SelectedIndex = 0
            Else
                CmbProdUnit.Text = RdrProduct("Unit").ToString
            End If

        Else
            TxtItemDesc.Text = "Invalid Product Code"
        End If
        RdrProduct.Close()
    End Sub

    Private Sub ButFindClient_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFindClient.Click
        Session.Add("PstrCode", TxtClientID.Text)
        ScriptManager.RegisterStartupScript(ButFindClient, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
    End Sub

    Private Sub ButFindProd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFindProd.Click
        Session.Add("PstrPCode", TxtProductCode.Text)
        ScriptManager.RegisterStartupScript(ButFindClient, Me.GetType, "ShowFindProd", "<script language='javascript'> ShowFindProd();</script>", False)
    End Sub

    Private Sub GdvHSCode_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvHSCode.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvHSCode, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvHSCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvHSCode.SelectedIndexChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        On Error Resume Next
        TxtHSItemNo.Text = GdvHSCode.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        TxtHsCode.Text = GdvHSCode.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
        TxtHSDescription.Text = GdvHSCode.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
        TxtHsQuantity.Text = GdvHSCode.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
        CmbHSUnit.Text = GdvHSCode.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
        TxtHsWeight.Text = GdvHSCode.SelectedRow.Cells(5).Text.Replace("&nbsp;", "")
        TxtCIF.Text = GdvHSCode.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
        TxtFOB.Text = GdvHSCode.SelectedRow.Cells(7).Text.Replace("&nbsp;", "")
        CmbHSOrigin.Text = GdvHSCode.SelectedRow.Cells(8).Text.Replace("&nbsp;", "")
        If TxtHsCode.Text <> "" Then
            TxtHsCode_TextChanged(Me, e)
        End If
        If TxtBillIn.Text <> "" Then
            If TxtHsCode.Text <> "" Then
                TxtProductCode.Text = ""
                TxtItemDesc.Text = ""
                TxtProdQtyUnit.Text = ""
                CmbProdUnit.SelectedIndex = -1
                GdvProduct.DataSource = Nothing
                GdvProduct.DataBind()
                'Update Products in Grid
                CmdProd.Connection = Con
                StrGetItem = "SELECT [LineNo],Pcode,ProdDescription,Quantity,UnitCode,ItemType,DateIn FROM BoeInProd "
                StrGetItem = StrGetItem & "WHERE BoeNo = '" & TxtBillIn.Text & "' And ItemNo = '" & TxtHSItemNo.Text & "' "
                CmdProd.CommandText = StrGetItem
                RdrProd = CmdProd.ExecuteReader
                GdvProduct.DataSource = RdrProd
                GdvProduct.DataBind()
                RdrProd.Close()
                For Each row As GridViewRow In GdvProduct.Rows
                    If row.Cells(1).Text <> "" Then
                        IntItem = IntItem + 1
                        row.Cells(0).Text = IntItem
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub GdvHSCode_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvHSCode.SelectedIndexChanging
        GdvHSCode.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub ButAddHS_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAddHS.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim CmdHSItem As New OleDb.OleDbCommand
        Dim CmdHSItemCategory As New OleDb.OleDbCommand
        Dim RdrHsItem As OleDb.OleDbDataReader
        If TxtBillIn.Text = "" Then
            LblText.Visible = True
            LblText.Text = "Please Enter Bill No"
            Exit Sub
        End If
        If TxtHSItemNo.Text = "" Then
            LblText.Visible = True
            LblText.Text = "Please Enter Item No"
            Exit Sub
        End If
        'For Each row As GridViewRow In GdvHSCode.Rows
        '    If TxtHsCode.Text = row.Cells(1).Text Then
        '        row.Cells(4).Text = TxtHsQuantity.Text
        '    End If
        'Next
        If TxtHSItemNo.Text <> "" Then
            CmdHSItem.Connection = Con
            CmdHSItem.CommandText = "Delete From BoeInItem Where BoeNo = '" & TxtBillIn.Text & "' And ItemNo = '" & TxtHSItemNo.Text & "'"
            CmdHSItem.ExecuteNonQuery()
            StrSql = "Insert Into BoeInItem(BoeNo,ItemNo,HsCode,HsDescription,Quantity,Unit,Weight,FOB,Value,CountryofOrigin)VALUES('"
            StrSql = StrSql & TxtBillIn.Text & "',"
            StrSql = StrSql & TxtHSItemNo.Text & ",'"
            StrSql = StrSql & TxtHsCode.Text & "','"
            StrSql = StrSql & TxtHSDescription.Text & "',"
            StrSql = StrSql & Val(TxtHsQuantity.Text) & ",'"
            StrSql = StrSql & CmbHSUnit.Text & "',"
            StrSql = StrSql & Val(TxtHsWeight.Text) & ","
            StrSql = StrSql & Val(TxtFOB.Text) & ","
            StrSql = StrSql & Val(TxtCIF.Text) & ",'"
            StrSql = StrSql & CmbHSOrigin.Text & "')"
            CmdHSItem.CommandText = StrSql
            CmdHSItem.ExecuteNonQuery()

            CmdHSItemCategory.Connection = Con
            CmdHSItemCategory.CommandText = "Delete From [ProdCategory] Where HSCode = '" & TxtHSItemNo.Text & "'"
            CmdHSItemCategory.ExecuteNonQuery()
            StrSql = "Insert Into ProdCategory(HsCode, Category)VALUES('"
            StrSql = StrSql & TxtHsCode.Text & "','"
            StrSql = StrSql & TxtHSDescription.Text & "')"
            CmdHSItemCategory.CommandText = StrSql
            CmdHSItemCategory.ExecuteNonQuery()

            TxtHSItemNo.Text = ""
            TxtHsCode.Text = ""
            TxtHSDescription.Text = ""
            TxtHsQuantity.Text = ""
            CmbHSUnit.SelectedIndex = -1
            TxtHsWeight.Text = ""
            TxtFOB.Text = ""
            TxtCIF.Text = ""
            CmbHSOrigin.SelectedIndex = -1
        End If
        GdvHSCode.DataSource = Nothing
        GdvHSCode.DataBind()
        'update HS Item grid
        CmdHSItem.CommandText = "Select ItemNo, HsCode, HsDescription, Quantity, Unit, Weight, Value, FOB, CountryofOrigin From BoeInItem Where BoeNo = '" & TxtBillIn.Text & "' Order By ItemNo"
        RdrHsItem = CmdHSItem.ExecuteReader
        GdvHSCode.DataSource = RdrHsItem
        GdvHSCode.DataBind()
        RdrHsItem.Close()
    End Sub

    Protected Sub TxtHsCode_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtHsCode.TextChanged
        CmdChkHSItem.Connection = Con
        CmdChkHSItem.CommandText = "Select Category From ProdCategory Where HSCode = '" & TxtHsCode.Text & "'"
        RdrHsCode = CmdChkHSItem.ExecuteReader
        If RdrHsCode.Read Then
            TxtHSDescription.Text = RdrHsCode(0)
        Else
            TxtHSDescription.Text = "Invalid HS Code"
        End If
        RdrHsCode.Close()

    End Sub

    Protected Sub ButNewHSItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNewHSItem.Click
        TxtHSItemNo.Text = ""
        TxtHsCode.Text = ""
        TxtHSDescription.Text = ""
        TxtHsQuantity.Text = ""
        CmbHSUnit.SelectedIndex = -1
        TxtHsWeight.Text = ""
        TxtFOB.Text = ""
        TxtCIF.Text = ""
        CmbHSOrigin.SelectedIndex = -1
    End Sub

    Protected Sub ButNewProd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNewProd.Click
        TxtProductCode.Text = ""
        TxtItemDesc.Text = ""
        TxtProdQtyUnit.Text = ""
        CmbProdUnit.SelectedIndex = -1
    End Sub

    Protected Sub ButDeleteHS_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDeleteHS.Click
        Dim CmdDelHsItem As New OleDb.OleDbCommand
        CmdDelHsItem.Connection = Con
        If TxtBillIn.Text = "" Then
            LblText.Visible = True
            LblText.Text = "Please Enter Bill No"
            Exit Sub
        End If
        CmdDelHsItem.CommandText = "Delete From BoeInItem Where BoeNo = '" & TxtBillIn.Text & "' And ItemNo = '" & TxtHSItemNo.Text & "' And HsCode = '" & TxtHsCode.Text & "'"
        CmdDelHsItem.ExecuteNonQuery()
        CmdDelHsItem.CommandText = "Delete From BoeInProd Where BoeNo = '" & TxtBillIn.Text & "' And ItemNo = '" & TxtHSItemNo.Text & "'"
        CmdDelHsItem.ExecuteNonQuery()
        TxtHSItemNo.Text = ""
        TxtHsCode.Text = ""
        TxtHSDescription.Text = ""
        TxtHsQuantity.Text = ""
        CmbHSUnit.SelectedIndex = -1
        TxtHsWeight.Text = ""
        TxtFOB.Text = ""
        TxtCIF.Text = ""
        CmbHSOrigin.SelectedIndex = -1
        CmdHsCode.Connection = Con
        CmdHsCode.CommandText = "Select ItemNo, HsCode, HsDescription, Quantity, Unit, Weight, Value, FOB, CountryofOrigin From BoeInItem Where BoeNo = '" & TxtBillIn.Text & "' Order By ItemNo"
        RdrHsCode = CmdHsCode.ExecuteReader
        GdvHSCode.DataSource = RdrHsCode
        GdvHSCode.DataBind()
        RdrHsCode.Close()
    End Sub

    Private Sub ClearAll()
        CmbZone.SelectedIndex = -1
        TxtBillIn.Text = ""
        TxtClientID.Text = ""
        TxtName.Text = ""
        CmbTrafficOp.SelectedIndex = -1
        TxtRemarks.Text = ""
        TxtDate.Text = Date.Today.ToString("dd-MMM-yyyy")
        TxtTruckNo.Text = ""
        TxtItems.Text = ""
        TxtCont.Text = ""
        GdvCont.DataSource = Nothing
        GdvCont.DataBind()
        TxtHSItemNo.Text = ""
        TxtHsCode.Text = ""
        TxtHSDescription.Text = ""
        TxtHsQuantity.Text = ""
        TxtHsWeight.Text = ""
        TxtCIF.Text = ""
        CmbHSUnit.SelectedIndex = -1
        TxtFOB.Text = ""
        CmbHSOrigin.Text = ""
        TxtProductCode.Text = ""
        TxtItemDesc.Text = ""
        TxtProdQtyUnit.Text = ""
        CmbProdUnit.SelectedIndex = -1
        GdvHSCode.DataSource = Nothing
        GdvHSCode.DataBind()
        GdvProduct.DataSource = Nothing
        GdvProduct.DataBind()
    End Sub
End Class