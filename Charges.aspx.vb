﻿Public Partial Class Charges
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatCharges As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Clear()
            CmbZone.Items.Add("Select ...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
                CmbDesc.Items.Add(Rdr("Name"))
            End While
            CmbZone.SelectedIndex = -1
            Rdr.Close()
        End If
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub UpdateCharges()
        StrSql = "SELECT * FROM Charges WHERE WhCode = '" & CmbZone.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        DatCharges.Clear()
        DatCharges.Load(Rdr)
        Rdr.Close()
        GdvCharges.DataSource = DatCharges
        GdvCharges.DataBind()
    End Sub

    Private Sub CmbZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbZone.SelectedIndexChanged
        If CmbZone.SelectedIndex = 0 Then
            TxtDesc.Text = ""
        Else
            TxtDesc.Text = CmbDesc.Items(CmbZone.SelectedIndex - 1).Text
        End If
        UpdateCharges()
    End Sub

    Private Sub ButAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAdd.Click
        Dim Dtrow As DataRow
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT * FROM Charges WHERE WhCode IS NULL"
        Rdr = Cmd.ExecuteReader
        DatCharges.Load(Rdr)
        Rdr.Close()
        On Error Resume Next
        For Each row As GridViewRow In GdvCharges.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dtrow = DatCharges.NewRow
                Dtrow.Item("ChCode") = DirectCast(row.FindControl("Code"), TextBox).Text
                Dtrow.Item("AccCode") = DirectCast(row.FindControl("AccCode"), TextBox).Text
                Dtrow.Item("AnalysisCode") = DirectCast(row.FindControl("AnalysisCode"), TextBox).Text
                Dtrow.Item("Description") = DirectCast(row.FindControl("Description"), TextBox).Text
                Dtrow.Item("Unit") = DirectCast(row.FindControl("Unit"), TextBox).Text
                Dtrow.Item("Normal") = DirectCast(row.FindControl("Normal"), TextBox).Text
                Dtrow.Item("Pref") = DirectCast(row.FindControl("Pref"), TextBox).Text
                DatCharges.Rows.Add(Dtrow)
            End If
        Next
        Dtrow = DatCharges.NewRow
        DatCharges.Rows.Add(Dtrow)
        GdvCharges.DataSource = DatCharges
        GdvCharges.DataBind()
        GdvCharges.Rows(GdvCharges.Rows.Count - 1).FindControl("Code").Focus()
    End Sub

    Private Sub ButDelRow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelRow.Click
        Dim Dtrow As DataRow
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT * FROM Charges WHERE WhCode IS NULL"
        Rdr = Cmd.ExecuteReader
        DatCharges.Load(Rdr)
        Rdr.Close()
        On Error Resume Next
        For Each row As GridViewRow In GdvCharges.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim ChkDel As CheckBox = DirectCast(row.FindControl("ChkSelect"), CheckBox)
                If Not ChkDel.Checked Then
                    Dtrow = DatCharges.NewRow
                    Dtrow.Item("ChCode") = DirectCast(row.FindControl("Code"), TextBox).Text
                    Dtrow.Item("AccCode") = DirectCast(row.FindControl("AccCode"), TextBox).Text
                    Dtrow.Item("AnalysisCode") = DirectCast(row.FindControl("AnalysisCode"), TextBox).Text
                    Dtrow.Item("Description") = DirectCast(row.FindControl("Description"), TextBox).Text
                    Dtrow.Item("Unit") = DirectCast(row.FindControl("Unit"), TextBox).Text
                    Dtrow.Item("Normal") = DirectCast(row.FindControl("Normal"), TextBox).Text
                    Dtrow.Item("Pref") = DirectCast(row.FindControl("Pref"), TextBox).Text
                    DatCharges.Rows.Add(Dtrow)
                End If
            End If
        Next
        GdvCharges.DataSource = DatCharges
        GdvCharges.DataBind()
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        StrSql = "DELETE FROM Charges WHERE WhCode = '" & CmbZone.Text & "'"
        Cmd.CommandText = StrSql
        Try
            Cmd.ExecuteNonQuery()
            For Each Row As GridViewRow In GdvCharges.Rows
                StrSql = "INSERT INTO Charges (ChCode,Description,Unit,Normal,Pref,WhCode,AnalysisCode,AccCode) VALUES('"
                StrSql = StrSql & DirectCast(Row.FindControl("Code"), TextBox).Text & "','"
                StrSql = StrSql & DirectCast(Row.FindControl("Description"), TextBox).Text & "','"
                StrSql = StrSql & DirectCast(Row.FindControl("Unit"), TextBox).Text & "',"
                StrSql = StrSql & Val(DirectCast(Row.FindControl("Normal"), TextBox).Text) & ","
                StrSql = StrSql & Val(DirectCast(Row.FindControl("Pref"), TextBox).Text) & ",'"
                StrSql = StrSql & CmbZone.Text & "','"
                StrSql = StrSql & DirectCast(Row.FindControl("AnalysisCode"), TextBox).Text & "','"
                StrSql = StrSql & DirectCast(Row.FindControl("AccCode"), TextBox).Text & "')"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            Next
            Trans.Commit()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub ButDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDelete.Click
        StrSql = "DELETE FROM Charges WHERE WhCode = '" & CmbZone.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Try
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class