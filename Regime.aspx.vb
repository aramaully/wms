﻿Public Partial Class Regime
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatLoc As New DataTable
    Dim StrCondition As String = ""

    Const MENUID = "Regime"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            BindRegime()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Private Sub GdvRegime_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvRegime.SelectedIndexChanged
        On Error Resume Next
        TxtCpcCode.Text = ChkSpace(GdvRegime.SelectedRow.Cells(0).Text)
        TxtRegime.Text = ChkSpace(GdvRegime.SelectedRow.Cells(1).Text)
        If GdvRegime.SelectedRow.Cells(2).Text = "In" Then
            RbnIn.Checked = True
        Else
            RbnOut.Checked = True
        End If
        TxtCategory.Text = ChkSpace(GdvRegime.SelectedRow.Cells(3).Text)
        TxtDescription.Text = ChkSpace(GdvRegime.SelectedRow.Cells(4).Text)
    End Sub

    Private Sub BindRegime()
        Cmd.Connection = Con
        StrSql = "SELECT CpcCode CPC, RegimeCode Regime, Type, Category, Name  FROM Regime"
        StrSql = StrSql & StrCondition & " ORDER BY CpcCode"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar Is Nothing Then Exit Sub
        Rdr = Cmd.ExecuteReader
        DatLoc.Load(Rdr)
        Rdr.Close()
        GdvRegime.DataSource = DatLoc
        GdvRegime.DataBind()
        If GdvRegime.Rows.Count = 1 Then
            On Error Resume Next
            TxtCpcCode.Text = ChkSpace(GdvRegime.Rows(0).Cells(0).Text)
            TxtRegime.Text = ChkSpace(GdvRegime.Rows(0).Cells(1).Text)
            If GdvRegime.Rows(0).Cells(2).Text = "In" Then
                RbnIn.Checked = True
            Else
                RbnOut.Checked = True
            End If
            TxtCategory.Text = ChkSpace(GdvRegime.Rows(0).Cells(3).Text)
            TxtDescription.Text = ChkSpace(GdvRegime.Rows(0).Cells(4).Text)
        End If
    End Sub

    Private Sub GdvLoc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvRegime.PageIndexChanging
        GdvRegime.PageIndex = e.NewPageIndex
        BindRegime()
    End Sub

    Private Sub GdvRegime_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvRegime.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvRegime, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvRegime.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvRegime.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub GdvRegime_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvRegime.SelectedIndexChanging
        GdvRegime.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub TxtCpcCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtCpcCode.TextChanged
        If TxtCpcCode.Text <> "" Then
            StrCondition = " WHERE CpcCode LIKE '%" & TxtCpcCode.Text & "%'"
        Else
            StrCondition = ""
        End If
        TxtRegime.Text = ""
        TxtDescription.Text = ""
        TxtCategory.Text = ""
        RbnIn.Checked = True
        BindRegime()
    End Sub

    Private Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        On Error Resume Next
        TxtCpcCode.Text = ""
        TxtRegime.Text = ""
        TxtDescription.Text = ""
        TxtCategory.Text = ""
        RbnIn.Checked = True
        TxtCpcCode.Focus()
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Try
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Regime WHERE CpcCode = '" & TxtCpcCode.Text & "'"
            Cmd.ExecuteNonQuery()
            ButNew_Click(Me, e)
            BindRegime()
        Catch ex As Exception
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Cmd.CommandText = "SELECT CpcCode FROM Regime WHERE CpcCode = '" & TxtCpcCode.Text & "'"
        Try
            If Cmd.ExecuteScalar Is Nothing Then
                StrSql = "INSERT INTO Regime (CpcCode,RegimeCode,Name,Category,Type) "
                StrSql = StrSql & "VALUES('" & TxtCpcCode.Text & "',"
                StrSql = StrSql & TxtRegime.Text & ",'"
                StrSql = StrSql & TxtDescription.Text & "','"
                StrSql = StrSql & TxtCategory.Text & "','"
                StrSql = StrSql & IIf(RbnIn.Checked, "In", "Out") & "')"
            Else
                StrSql = "UPDATE Regime SET CpcCode = '" & TxtCpcCode.Text & "',"
                StrSql = StrSql & "RegimeCode=" & TxtRegime.Text & ","
                StrSql = StrSql & "Name='" & TxtDescription.Text & "',"
                StrSql = StrSql & "Category='" & TxtCategory.Text & "',"
                StrSql = StrSql & "Type='" & IIf(RbnIn.Checked, "In", "Out") & "' WHERE CpcCode = '" & TxtCpcCode.Text & "'"
            End If
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ButNew_Click(Me, e)
            BindRegime()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class