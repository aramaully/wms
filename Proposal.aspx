﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Proposal.aspx.vb" Inherits="Wms.Proposal" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            height: 47px;
        }
        .ModalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .style19
        {
            width: 16px;
        }
        .style20
        {
            width: 193px;
        }
        .style21
        {
        }
        .style22
        {
            width: 107px;
        }
        .style24
        {
        }
        .style25
        {
            width: 161px;
        }
    </style>
</head>
<body style="height: 100%">
    <form id="form1" runat="server" style="height: 100%">
    <div style="height: 100%; width: 1041px;">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 101; left: 398px; position: absolute; top: 170px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" 
                    Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" Width="80%" Height="450px" BorderStyle="Groove"
                    BorderWidth="1px">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td bgcolor="#004080" class="style19">
                                <asp:Label ID="LblText" runat="server"></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" 
                                    Text="Proposal Builder"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButNew" runat="server" CausesValidation="False" Height="39px" 
                                    Style="cursor: pointer" Text="New" ToolTip="Create New Activity" 
                                    UseSubmitBehavior="False" Width="55px" />
                                <asp:Button ID="ButSave" runat="server" Height="39px" Style="cursor: pointer" 
                                    Text="Save" ToolTip="Save Activity" UseSubmitBehavior="False" 
                                    Width="55px" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" 
                                    ConfirmText="Sure to Save Client Data?" Enabled="True" 
                                    TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" CausesValidation="False" Height="39px" 
                                    Style="cursor: pointer" Text="Find" ToolTip="Find Client" 
                                    UseSubmitBehavior="False" Width="55px" />
                                <asp:Button ID="ButPrint" runat="server" Height="39px" Text="Print" 
                                    Width="55px" />
                                <asp:Button ID="ButPrevious" runat="server" CausesValidation="False" 
                                    Height="39px" Style="cursor: pointer" Text="Prev" 
                                    ToolTip="Go To Previous Client" Width="55px" />
                                <asp:Button ID="ButNext" runat="server" Height="39px" Text="Next" Style="cursor: pointer"
                                    ToolTip="Go to Next Client" Width="55px" UseSubmitBehavior="False" 
                                    CausesValidation="False" />
                                <asp:Button ID="ButDelete" runat="server" CausesValidation="False" 
                                    Height="39px" Style="cursor: pointer" Text="Delete" 
                                    ToolTip="Delete Activity Selected" UseSubmitBehavior="False" Width="55px" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" 
                                    ConfirmText="Sure to Delete Client Data?" Enabled="True" 
                                    TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButExit" runat="server" Height="39px" Text="Close" Style="cursor: pointer"
                                    ToolTip="Close this Module" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButExit_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Close this Module?"
                                    Enabled="True" TargetControlID="ButExit">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>                   
                    <table width="100%">
                        <tr>
                        <td class="style20">Proposal No</td><td class="style21" colspan="3">
                            <asp:TextBox ID="TxtPropNo" runat="server"></asp:TextBox>
                            </td>
                            <td align="right" colspan="2">
                                Date</td>
                            <td>
                                <asp:TextBox ID="TxtPropDate" runat="server"></asp:TextBox>
                                <cc1:CalendarExtender ID="TxtPropDate_CalendarExtender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtPropDate">
                                </cc1:CalendarExtender>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style20">
                                Client ID</td>
                            <td class="style25">
                                <asp:TextBox ID="TxtClientID" runat="server" Height="22px" Width="113px" 
                                    AutoPostBack="True"></asp:TextBox>
                                <asp:Button ID="ButFindClient" runat="server" Text="..." />
                            </td>
                            <td class="style24">
                                &nbsp;</td>
                            <td class="style21" colspan="5">
                                <asp:TextBox ID="TxtClient" runat="server" Width="80%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style20" valign="top">
                                Description of Facilities</td>
                            <td class="style24" colspan="7">
                                <asp:TextBox ID="TxtFacility" runat="server" MaxLength="255" 
                                    TextMode="MultiLine" Width="86%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style20">
                                Warehouse Code</td>
                            <td class="style24" colspan="2">
                                <asp:DropDownList ID="CmbFpZone" runat="server" AutoPostBack="True" 
                                    Height="22px" Width="123px">
                                </asp:DropDownList>
                            </td>
                            <td class="style21" colspan="5">
                                <asp:TextBox ID="TxtWHouse" runat="server" Width="80%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style20">
                                Period</td>
                            <td class="style24" colspan="2">
                                <asp:TextBox ID="TxtPeriod" runat="server"></asp:TextBox>
                            </td>
                            <td class="style21" colspan="2">
                                Facility Size</td>
                            <td class="style21" colspan="3">
                                <asp:TextBox ID="TxtSize" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style20">
                                Monthly Rent</td>
                            <td class="style24" colspan="2">
                                <asp:TextBox ID="TxtRent" runat="server"></asp:TextBox>
                            </td>
                            <td class="style21" colspan="2">
                                Service Charge</td>
                            <td class="style21" colspan="3">
                                <asp:TextBox ID="TxtServiceCh" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style20">
                                Monthly Storage</td>
                            <td class="style24" colspan="2">
                                <asp:TextBox ID="TxtStorage" runat="server"></asp:TextBox>
                            </td>
                            <td class="style21" colspan="2">
                                Minimum Storage</td>
                            <td class="style21" colspan="3">
                                <asp:TextBox ID="TxtMinStorage" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style20">
                                Facility Type</td>
                            <td class="style24" colspan="2">
                                <asp:DropDownList ID="CmbType" runat="server">
                                    <asp:ListItem>Warehouse and Office Space</asp:ListItem>
                                    <asp:ListItem>Outdoor Storage</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="style21" colspan="2">
                                Total</td>
                            <td class="style21" colspan="3">
                                <asp:TextBox ID="TxtTotal" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>              
                </asp:Panel>
                 <asp:Panel ID="PnlFind" Style="z-index: 101; display:none; left: 300px; position: absolute; top: 60px"
                    runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                    Visible="True" BackColor="White">
                    <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                        <tr>
                            <td class="style6" bgcolor="#004080" valign="top">
                                <asp:RadioButton ID="RbtName" runat="server" Text="Name" Checked="True" GroupName="FinOption" />
                                <br />
                                <asp:RadioButton ID="RbtCity" runat="server" Text="City" GroupName="FinOption" />
                                <br />
                                <asp:RadioButton ID="RbtContact" runat="server" Text="Contact" GroupName="FinOption" />
                            </td>
                            <td class="style5" bgcolor="#004080">
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" style="text-align:center;"
                                    Text="Enter the text here to search"></asp:Label>
                                <asp:TextBox ID="TxtSearch" runat="server" Width="100%" BackColor="White"></asp:TextBox>
                            </td>
                            <td align="right" bgcolor="#004080" class="style7">
                                <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                    Width="65px" />
                                <br />
                                <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                    Width="65px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="style4" colspan="3">
                                <asp:Panel ID="Panel8" runat="server" Width="100%" Height="300px" ScrollBars="Vertical">
                                    <asp:GridView ID="GdvFind" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                        ShowFooter="true">
                                        <HeaderStyle BackColor="#0080C0" />
                                        <FooterStyle BackColor="#0080C0" />
                                        <AlternatingRowStyle BackColor="#CAE4FF" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                
                <asp:Panel ID="PnlFindProp" Style="z-index: 101; display:none; left: 300px; position: absolute; top: 60px"
                    runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                    Visible="True" BackColor="White">
                    <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                        <tr>                            
                            <td class="style5" bgcolor="#004080">
                                <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" style="text-align:center;"
                                    Text="Enter Name of Client to search"></asp:Label>
                                <asp:TextBox ID="TxtClientName" runat="server" Width="100%" BackColor="White"></asp:TextBox>
                            </td>
                            <td align="right" bgcolor="#004080" class="style7">
                                <asp:Button ID="ButFindProp" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                    Width="65px" />
                                <br />
                                <asp:Button ID="ButCloseProp" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                    Width="65px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="style4" colspan="3">
                                <asp:Panel ID="Panel3" runat="server" Width="100%" Height="300px" ScrollBars="Vertical">
                                    <asp:GridView ID="GdvFindProp" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                        ShowFooter="true">
                                        <HeaderStyle BackColor="#0080C0" />
                                        <FooterStyle BackColor="#0080C0" />
                                        <AlternatingRowStyle BackColor="#CAE4FF" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>             
                               
                
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>

