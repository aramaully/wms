﻿Public Partial Class FindClientBill
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            TxtSearch.Text = Session("PstrCode")
            TxtSearch.Focus()
        End If
    End Sub
    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Refresh" Then
            If RbnClient.Text = "Name" Then
                StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
                If RbnClient.Checked Then
                    StrSql = StrSql & "Name"
                ElseIf RbnCountry.Checked Then
                    StrSql = StrSql & "City"
                Else
                    StrSql = StrSql & "Contact"
                End If
                StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
            Else
                StrSql = "SELECT BoeIn.BillNo, CONVERT(Char,BoeIn.BillDate,103) 'Bill Date', BoeIn.Revision, Client.Name FROM BoeIn, Client WHERE "
                StrSql = StrSql & "BoeIn.ClientID = Client.ClientID "
                If RbnClient.Checked Then
                    StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
                End If
                If RbnCountry.Checked Then
                    StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
                End If
                If RbnTraffic.Checked Then
                    StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
                End If
                If RbnCont.Checked Then
                    StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
                End If
                If ChkDate.Checked Then
                    Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                    If TxtFrom.Text <> "" And TxtTo.Text <> "" Then
                        StrSql = StrSql & "AND Boein.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
                        StrSql = StrSql & "AND Boein.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
                    End If
                End If
                End If
                Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
                Dim da As New OleDb.OleDbDataAdapter(cmd)
                Dim ds As New DataSet()
                da.Fill(ds)
                GdvFind.DataSource = ds
                GdvFind.DataBind()
        End If
    End Sub

    Private Sub GdvEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvEmp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtBoeNo.Value = GdvFind.SelectedRow.Cells(0).Text
        GdvFind.SelectedRow.Focus()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CloseMe", "GetID();", True)
    End Sub

    Private Sub GdvEmp_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub TxtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtSearch.TextChanged
        Dim ev As New EO.Web.ToolBarEventArgs(ToolBar1.Items(0))
        ToolBar1_ItemClick(Me, ev)
    End Sub
End Class