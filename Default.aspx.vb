﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb

Partial Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TxtUserName.Focus()
    End Sub

    Protected Sub ButLogin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButLogin.Click
        Dim PstrServer As String
        Dim PstrDatabase As String
        Dim Con As New OleDbConnection
        Dim StrSql As String

        'LOCAL
        PstrServer = "(local)"
        PstrDatabase = "wmstest"
        'PstrDatabase = "wms_uat"
        'PstrDatabase = "wmszone9"

        'WMS_UAT
        'PstrServer = "BFSLAPPSERVER"
        'PstrDatabase = "wms_uat"

        'WMS
        'PstrServer = "BFSLAPPSERVER"
        'PstrDatabase = "wmstest"

        'WMS_Z9
        'PstrServer = "BFSLAPPSERVER"
        'PstrDatabase = "wmszone9"

        Dim CmdUser As New OleDb.OleDbCommand
        Dim RdrUser As OleDb.OleDbDataReader
        Session("LoggedIn") = False
        Try
            Con.Close()
            Session("ConnString") = "Provider=SQLOLEDB.1;Data Source=" & PstrServer & ";User ID=admin;PWD=swathi*;Initial Catalog=" & PstrDatabase
            'MsgBox(Session("ConnString"))
            Con.ConnectionString = Session("ConnString")
            Con.Open()
        Catch ex As Data.OleDb.OleDbException
            MsgBox("Cannot Connect to Database. Please Specify Correct Server and Database", MsgBoxStyle.Critical, "Database Error")
            Exit Sub
        Finally
        End Try
        StrSql = "SELECT * FROM [User] WHERE UserID = '" & TxtUserName.Text & "'"
        CmdUser.Connection = Con
        CmdUser.CommandText = StrSql
        RdrUser = CmdUser.ExecuteReader
        If RdrUser.Read Then
            If RdrUser("Password") = TxtPassword.Text Then
                Session("LoggedIn") = True
                Session("UserID") = RdrUser("UserID").ToString
                Session("PstrZone") = RdrUser("WHCode").ToString
                Session("Server") = PstrServer
                Session("Database") = PstrDatabase
                Response.Redirect("NewMainMenu.aspx")
            Else
                LblMsg.Text = "Invalid Password"
            End If
        Else
            LblMsg.Text = "Invalid User Name"
        End If
        RdrUser.Close()
    End Sub

    Protected Sub ButCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCancel.Click
        Session("LoggedIn") = False
        Response.Write("<script language='javascript'> { window.close();}</script>")
    End Sub
End Class