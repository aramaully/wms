﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Clients.aspx.vb" Inherits="Wms.Clients" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Create / Edit Activity</title>
    <link href="styles/styles.css" type="text/css" rel="stylesheet" />
    <link href="styles/IDC_Toolbar.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="js/IDC_Core.js"></script>
    
    <script language="javascript" type="text/javascript">
        //Variables
        var w = null;
        var core = null;
        var wfind = null;
        var Index = null;
        //Initialize
        function init() {
            w = window.frameElement.IDCWindow; //Get Window Manager Window
            core = w.WindowManager.Core; //Get the Core (for mouse, keyboard, ajax and DHTML functions
            var localCore = new IDC_Core(); //Create a local core, for local window mouse coords.

            w.Icons = ['images/smv.png', 'images/smv32.png']; //Set Window Icons as an Array 16x16, 32x32.
            w.Name = 'Create / Edit Activity'; //Set the name
            w.Width = 992; //Set the size of the window dynamically, by reading the desktop size
            w.Height = 506;
            w.Top = 10;
            w.Left = 10;
            w.Show(); //Show the window
        };
    </script>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            height: 47px;
        }
        .style4
        {
        }
        .style5
        {
            width: 376px;
            height: 72px;
        }
        .style6
        {
            color: #FFFFFF;
            width: 84px;
            height: 72px;
        }
        .style7
        {
            height: 72px;
        }
                
        .ModalBackground
        {
        background-color: Gray;
	    filter: alpha(opacity=70);
	    opacity: 0.7;
        }
        </style>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <script type="text/javascript">
            // Get the instance of PageRequestManager.
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);

            // Called when async postback begins
            function prm_InitializeRequest(sender, args) {
                // get the divImage and set it to visible
                var panelProg = $get('divImage');
                panelProg.style.display = '';
                // reset label text
                var lbl = $get('<%= me.lblText.ClientID %>');
                lbl.innerHTML = '';

                // Disable button that caused a postback
                // $get(args._postBackElement.id).disabled = true;
            }

            // Called when async postback ends
            function prm_EndRequest(sender, args) {
                // get the divImage and hide it again
                var panelProg = $get('divImage');
                panelProg.style.display = 'none';

                // Enable button that caused a postback
                // $get(sender._postBackSettings.sourceElement.id).disabled = false;
            }
        </script>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel9" Style="z-index: 102; left: 500px; position: absolute; top: 200px"
                    runat="server" Height="100px" Width="100px" BorderStyle="None" Visible="True">
                    <div id="divImage" style="display: none">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/images/wait.gif" />
                    </div>
                </asp:Panel>
                <div id="Content" style="width: 100%; height: 490px; overflow: auto; padding: 2px;
                    border-right: solid 1px #cddaea;">
                    <table class="style1" cellpadding="0" cellspacing="0" frame="hsides">
                        <tr>
                            <td>
                                <asp:Label ID="lblText" runat="server" Text=""></asp:Label>
                            </td>
                            <td align="left" bgcolor="#004080" nowrap="nowrap" class="style3" width="40%">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="       Create / Edit Client Details"></asp:Label>
                            </td>
                            <td align="right" bgcolor="#004080" class="style3" nowrap="nowrap">
                                <asp:Button ID="ButNew" runat="server" Height="39px" Text="New" Style="cursor: pointer"
                                    ToolTip="Create New Activity" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButSave" runat="server" Height="39px" Text="Save" Style="cursor: pointer"
                                    ToolTip="Save Activity" Width="55px" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButSave_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Save Client Data?"
                                    Enabled="True" TargetControlID="ButSave">
                                </cc1:ConfirmButtonExtender>
                                <asp:Button ID="ButFind" runat="server" Height="39px" Text="Find" Style="cursor: pointer"
                                    ToolTip="Find Client" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButNext" runat="server" Height="39px" Text="Next" Style="cursor: pointer"
                                    ToolTip="Go to Next Client" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <asp:Button ID="ButPrevious" runat="server" Height="39px" Text="Prev" Style="cursor: pointer"
                                    ToolTip="Go To Previous Client" Width="55px" CausesValidation="False" />
                                <asp:Button ID="ButDelete" runat="server" Height="39px" Text="Delete" Style="cursor: pointer"
                                    ToolTip="Delete Activity Selected" Width="55px" CausesValidation="False" UseSubmitBehavior="False" />
                                <cc1:ConfirmButtonExtender ID="ButDelete_ConfirmButtonExtender" runat="server" ConfirmText="Sure to Delete Client Data?"
                                    Enabled="True" TargetControlID="ButDelete">
                                </cc1:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    <cc1:TabContainer ID="TabContainer1" runat="server" Width="100%" ActiveTabIndex="0"
                        Height="375px">
                        <cc1:TabPanel ID="GenTab" runat="server" HeaderText="General">
                            <HeaderTemplate>
                                General
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table class="style1" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            Client ID
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox ID="TxtClientID" runat="server" AutoPostBack="True"></asp:TextBox>
                                        </td>
                                        <td>
                                            Category
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="CmbCategory" runat="server" Width="60%">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="CmbCategory"
                                                ErrorMessage="Select a Category" Operator="NotEqual" ValueToCompare="Select ...">*</asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Name
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtName" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Address
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtAddress" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            City
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtCity" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Telephone
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtTel" runat="server" Width="95%"></asp:TextBox>
                                        </td>
                                        <td>
                                            Fax
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtFaxNo" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            E-Mail
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtEmail" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Contact
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtContact" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Position
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtPosition" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Date
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtDate" runat="server"></asp:TextBox><cc1:CalendarExtender ID="TxtDate_CalendarExtender"
                                                runat="server" Enabled="True" Format="dd/MM/yyyy" TargetControlID="TxtDate">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td>
                                            Status
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="CmbStatus" runat="server" Width="75%">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="CmbStatus"
                                                ErrorMessage="Select a Status" Operator="NotEqual" ValueToCompare="Select ...">*</asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            VAT No
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtVatNo" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            FP Zone
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="CmbZone" runat="server" Width="75%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Licence No
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtLicenceNo" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Expiry
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtLicExp" runat="server"></asp:TextBox><cc1:CalendarExtender ID="TxtLicExp_CalendarExtender"
                                                runat="server" Enabled="True" TargetControlID="TxtLicExp" 
                                                Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Storage Permit
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtStoragePermit" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Expiry
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtPermitExp" runat="server"></asp:TextBox><cc1:CalendarExtender
                                                ID="TxtPermitExp_CalendarExtender" runat="server" Enabled="True" 
                                                TargetControlID="TxtPermitExp" Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Product Type
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtProType" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Remarks
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="TxtRemarks" runat="server" Width="100%" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="LogTab" runat="server" HeaderText="Logistics" Style="height: 100%;">
                            <ContentTemplate>
                                <table class="style1" width="100%" style="height: 92%;">
                                    <tr>
                                        <td>
                                            <asp:Button ID="ButNormal" runat="server" Text="Update with Normal Charges" Style="cursor: pointer"
                                                CausesValidation="False" />
                                            <asp:Button ID="ButPreferential" runat="server" Text="Update with Preferential Charges"
                                                Style="cursor: pointer" CausesValidation="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:Panel ID="Panel6" runat="server" Width="661px" BorderStyle="Solid" BorderWidth="1px"
                                                Height="300px" ScrollBars="Vertical">
                                                <asp:GridView ID="GdvLogistic" runat="server" Height="266px" BorderColor="#E9ECEF"
                                                    AutoGenerateColumns="False" BorderStyle="None" BorderWidth="1px" CellPadding="4"
                                                    ForeColor="Black" GridLines="Vertical" ShowFooter="True" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Zone">
                                                            <ItemTemplate>
                                                                <asp:Label ID="ZoneLabel" runat="server" Text='<%# Eval("WhCode") %>'></asp:Label></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Code">
                                                            <ItemTemplate>
                                                                <asp:Label ID="CodeLabel" runat="server" Text='<%# Eval("ChCode") %>'></asp:Label></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Analysis Code">
                                                            <ItemTemplate>
                                                                <asp:Label ID="AcodeLabel" runat="server" Text='<%# Eval("AnalysisCode") %>'></asp:Label></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description">
                                                            <ItemTemplate>
                                                                <asp:Label ID="DescLabel" runat="server" Text='<%# Eval("Description") %>'></asp:Label></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Unit">
                                                            <ItemTemplate>
                                                                <asp:Label ID="UnitLabel" runat="server" Text='<%# Eval("Unit") %>'></asp:Label></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Charge">
                                                            <ItemStyle HorizontalAlign="Right" />
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="Charge" runat="server" Text='<%# Eval("Charge") %>' Width="60px"
                                                                    Style="text-align: right" Visible="True" BorderStyle="None" ToolTip="Click to Change Value"></asp:TextBox></ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#0080C0" />
                                                    <AlternatingRowStyle BackColor="#CAE4FF" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="FCTab" runat="server" HeaderText="Fixed Charges">
                            <HeaderTemplate>
                                Fixed Charges
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table width="50%">
                                    <tr>
                                        <td class="style18">
                                        </td>
                                        <td align="center">
                                            MUR
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style18">
                                            Pallet Charges
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtPallet" runat="server" Style="text-align: right" 
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            Per Day
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="middle" class="style18">
                                            Vehicles
                                        </td>
                                        <td valign="middle">
                                            <asp:TextBox ID="TxtVehicles" runat="server" Style="text-align: right" 
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td valign="top">
                                            Per
                                            <asp:TextBox ID="TxtVehicleDays" runat="server" Style="text-align: right"></asp:TextBox>Day/s
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style18">
                                            Container 20 Ft
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtCont20" runat="server" Style="text-align: right" 
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            Per Half Month exceeding 2 Days
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style18">
                                            Container 40 Ft
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtCont40" runat="server" Style="text-align: right" 
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            Per Half Month exceeding 2 Days
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style18">
                                            Discount
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtDiscount" runat="server" Style="text-align: right" 
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            TUs Per Month
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="CStatTab" runat="server" HeaderText="Client Statistics">
                            <ContentTemplate>
                                <table class="style1">
                                    <tr>
                                        <td>
                                            Zones Operated
                                        </td>
                                        <td valign="top">
                                            <asp:Panel ID="Panel5" runat="server" Width="100%" BorderStyle="Solid" BorderWidth="1px"
                                                Height="150px" ScrollBars="Vertical">
                                                <asp:GridView ID="GdvZones" runat="server" AutoGenerateColumns="False" Width="100%"
                                                    PageSize="3">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox runat="server" ID="chkZone" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="WhCode" HeaderText="Zone">
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Name" HeaderText="Description">
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#0080C0" />
                                                    <AlternatingRowStyle BackColor="#CAE4FF" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td valign="top" rowspan="4">
                                            <asp:Panel ID="Panel4" runat="server" Width="100%" BorderStyle="Solid" BorderWidth="1px"
                                                Height="170px" ScrollBars="Vertical">
                                                <asp:GridView ID="GdvActivity" runat="server" AutoGenerateColumns="False" PageSize="3"
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkActivity" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Activity" HeaderText="Activity" />
                                                    </Columns>
                                                    <HeaderStyle BackColor="#0080C0" />
                                                    <AlternatingRowStyle BackColor="#CAE4FF" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Type of Business
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtBusinessType" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            First Date of Operations
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtDateofOp" runat="server"></asp:TextBox><cc1:CalendarExtender
                                                ID="TxtDateofOp_CalendarExtender" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                TargetControlID="TxtDateofOp">
                                            </cc1:CalendarExtender>
                                            <asp:Button ID="ButAddCabin" runat="server" Text="Add a Cabin" 
                                                CausesValidation="False" />
                                             <asp:Button ID="ButDelCabin" runat="server" Text="Delete a Cabin" 
                                                CausesValidation="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            Cabins Occupied
                                        </td>
                                        <td class="style5">
                                            <asp:Panel ID="Panel7" runat="server" Width="100%" BorderStyle="Solid" BorderWidth="1px"
                                                Height="135px" ScrollBars="Vertical">
                                                <asp:GridView ID="GdvCabins" runat="server" PageSize="5" AutoGenerateColumns="False"
                                                    Width="100%">
                                                    <HeaderStyle BackColor="#0080C0" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkSelect" runat="server"  Visible="True">
                                                                </asp:CheckBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Zone">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="Zone" runat="server" Text='<%# Eval("WhCode") %>' Width="100%" Visible="True"
                                                                    BorderStyle="None">
                                                                </asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Cabin No">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="CabinNo" runat="server" Text='<%# Eval("CabinNo") %>' Width="100%"
                                                                    Visible="True" BorderStyle="None">
                                                                </asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Capacity">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="Capacity" runat="server" Text='<%# Eval("Capacity") %>' Width="100%"
                                                                    Style="text-align: right" Visible="True" BorderStyle="None">
                                                                </asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Unit">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="Unit" runat="server" Text='<%# Eval("Unit") %>' Width="100%" Visible="True"
                                                                    BorderStyle="None">
                                                                </asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="Markets">
                            <ContentTemplate>
                                <table class="style3" width="60%">
                                    <tr>
                                        <td width="50%">
                                            <asp:Panel ID="Panel3" runat="server" Width="100%" BorderStyle="Solid" BorderWidth="1px"
                                                Height="300px" ScrollBars="Vertical">
                                                <asp:GridView ID="GdvImport" runat="server" AutoGenerateColumns="False" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkImport" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Name" HeaderText="Import Market" />
                                                    </Columns>
                                                    <HeaderStyle BackColor="#0080C0" />
                                                    <AlternatingRowStyle BackColor="#CAE4FF" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td valign="top">
                                            <asp:Panel ID="Panel2" runat="server" Width="100%" BorderStyle="Solid" BorderWidth="1px"
                                                Height="300px" ScrollBars="Vertical">
                                                <asp:GridView ID="GdvExport" runat="server" AutoGenerateColumns="False" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkExport" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Name" HeaderText="Export Market" />
                                                    </Columns>
                                                    <HeaderStyle BackColor="#0080C0" />
                                                    <AlternatingRowStyle BackColor="#CAE4FF" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>                    
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                </div>
                <asp:Panel ID="PnlFind" Style="z-index: 101; display:none; left: 300px; position: absolute; top: 60px"
                    runat="server" Height="410px" Width="700px" BorderStyle="Groove" BorderWidth="2px"
                    Visible="True" BackColor="White">
                    <table class="style1" cellpadding="0" cellspacing="0" style="height: 100%;">
                        <tr>
                            <td class="style6" bgcolor="#004080" valign="top">
                                <asp:RadioButton ID="RbtName" runat="server" Text="Name" Checked="True" GroupName="FinOption" />
                                <br />
                                <asp:RadioButton ID="RbtCity" runat="server" Text="City" GroupName="FinOption" />
                                <br />
                                <asp:RadioButton ID="RbtContact" runat="server" Text="Contact" GroupName="FinOption" />
                            </td>
                            <td class="style5" bgcolor="#004080">
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" style="text-align:center;"
                                    Text="Enter the text here to search"></asp:Label>
                                <asp:TextBox ID="TxtSearch" runat="server" Width="100%" BackColor="White"></asp:TextBox>
                            </td>
                            <td align="right" bgcolor="#004080" class="style7">
                                <asp:Button ID="ButRefresh" runat="server" CausesValidation="False" Text="Find" Height="30px"
                                    Width="65px" />
                                <br />
                                <asp:Button ID="ButSelect" runat="server" CausesValidation="False" Text="Close" Height="30px"
                                    Width="65px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="style4" colspan="3">
                                <asp:Panel ID="Panel8" runat="server" Width="100%" Height="300px" ScrollBars="Vertical">
                                    <asp:GridView ID="GdvFind" runat="server" Width="100%" EnableSortingAndPagingCallbacks="false"
                                        ShowFooter="true">
                                        <HeaderStyle BackColor="#0080C0" />
                                        <FooterStyle BackColor="#0080C0" />
                                        <AlternatingRowStyle BackColor="#CAE4FF" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="ErrorPanel" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="ErrorUpdate" runat="server">
                <ContentTemplate>
                    <table width="350px" cellpadding="0" cellspacing="4" style="height: 200px; background: url(images/ErrPnl.png) no-repeat left top">
                        <tr style="height: 50px">
                            <td style="width: 6px"> 
                            </td>
                            <td align="center" valign="top">
                                <asp:Label ID="ErrHead" runat="server" Font-Bold="true" ForeColor="White"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" Style="display: block; background: url(images/close24.png) no-repeat 0px 0px;
                                    left: 320px; width: 26px; text-indent: -1000em; position: absolute; top: 15px;
                                    height: 26px;" OnClick="ButCloseErr_Click" />
                            </td>
                            <td style="width: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Label ID="ErrMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td style="width: 6px">
                            </td>
                            <td align="center" style="border-top: #ddd 2px groove;">
                                <asp:Button ID="ButCloseErr" Height="30px" Width="60" runat="server" Text="Close"
                                    OnClick="ButCloseErr_Click" Style="cursor: pointer;" UseSubmitBehavior="False"
                                    CausesValidation="False" />
                            </td>
                            <td style="width: 2px">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="ShowModal" runat="server" Style="display: none" CausesValidation="false" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="ShowModal"
            PopupControlID="ErrorPanel" BackgroundCssClass="ModalBackground">
        </cc1:ModalPopupExtender>
    </div>
    </form>
</body>
</html>
