﻿Public Partial Class MonRet
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim CmdEmp As New OleDb.OleDbCommand
    Dim StrSqlEmp As String

    Const MENUID = "ClientStock"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            CmbMonth.SelectedIndex = Date.Today.Month - 1
            TxtYear.Text = Date.Today.Year
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            'Cmd.CommandText = "SELECT DeptName FROM Dept"
            'Rdr = Cmd.ExecuteReader
            'CmbDept.Items.Add("ALL")
            'While Rdr.Read
            '    CmbDept.Items.Add(Rdr("DeptName"))
            'End While
            'Rdr.Close()
        End If
    End Sub

    Protected Sub CmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbCategory.SelectedIndexChanged
        If CmbCategory.Text = "Monthly" Then
            TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            CmbMonth.Visible = True
            TxtDate.Visible = False
            LblDate.Text = "Month"
        ElseIf CmbCategory.Text = "Weekly" Then
            CmbMonth.Visible = False
            TxtDate.Text = Date.Today
            TxtDate.Visible = True
            LblDate.Text = "Week Ending"
        Else
            CmbMonth.Visible = False
            TxtDate.Text = Date.Today
            TxtDate.Visible = True
            LblDate.Text = "Fortnight Ending"
        End If
    End Sub

    Protected Sub CmbMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbMonth.SelectedIndexChanged, TxtDate.TextChanged
        If CmbCategory.Text = "Monthly" Then
            If CmbMonth.Text = "Bonus" Then
                TxtDate.Text = CDate("28-December" & "-" & TxtYear.Text)
            Else
                TxtDate.Text = CDate("28-" & CmbMonth.Text & "-" & TxtYear.Text)
            End If

        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Dim DteSalDate As Date = CDate(TxtDate.Text)

            StrSql = "SELECT BOEINPROD.BOENO,BOEINPROD.ITEMNO, BoeInProd.UnitCode, BOEINPROD.PCODE, BoeInProd.ProdDescription, BOEINPROD.QUANTITY, BoeIn.ClientID, BoeIn.BillDate, BoeIn.WhCode "
            StrSql = StrSql & "FROM BOEINPROD INNER JOIN BoeIn ON BoeinProd.BoeNo = BoeIn.BillNo AND BoeIn.WhCode = 'FPZ06' "
            StrSql = StrSql & "WHERE (SELECT MAX(BILLDATE) FROM BOEOUTPROD, BOEOUT "
            StrSql = StrSql & "WHERE BOEINPROD.BOENO = BOEOUTPROD.BOEINNO AND BOEINPROD.PCODE "
            StrSql = StrSql & "= BOEOUTPROD.PCODE AND BOEOUTPROD.BOENO = BOEOUT.BILLNO) > '"
            StrSql = StrSql & Format(CDate(TxtDate.Text), "MM/dd/yyyy") & "' "
            StrSql = StrSql & "OR BOEINPROD.QUANTITY - ISNULL((SELECT SUM(QUANTITY) FROM  "
            StrSql = StrSql & "BOEOUTPROD WHERE BOEINPROD.BOENO = BOEOUTPROD.BOEINNO AND "
            StrSql = StrSql & "BOEINPROD.PCODE = BOEOUTPROD.PCODE AND BOEINPROD.ITEMNO = BOEOUTPROD.BOEINITEM),0) > 0 "
            StrSql = StrSql & "ORDER BY BoeIn.ClientID,BOEINPROD.BOENO"

            Dim ExcelGrid As New GridView()
            Dim DtProfileUsers As New DataTable("Table")
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            ExcelGrid.DataSource = Rdr
            ' it's my DataTable Name  
            ExcelGrid.DataBind()
            Rdr.Close()
            If ExcelGrid.Rows.Count = 0 Then
                LblResult.Text = "No Records"
                Exit Sub
            End If
            Dim ExcelTable As New Table()
            ExcelTable.GridLines = GridLines.Both
            ExcelTable.Rows.Add(ExcelGrid.HeaderRow)
            For Each GdvRow As GridViewRow In ExcelGrid.Rows
                ExcelTable.Rows.Add(GdvRow)
            Next
            Dim wrt As New IO.StringWriter()
            Dim htw As New HtmlTextWriter(wrt)
            ExcelTable.RenderControl(htw)
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=CustomRep-" & DateTime.Today & ".xls")
            Response.ContentType = "application/ms-excel"
            Response.Write(wrt.ToString())
            Response.[End]()
        End If
    End Sub

End Class