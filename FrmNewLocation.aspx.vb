﻿Public Partial Class FrmNewLocation
    Inherits System.Web.UI.Page
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatLoc As New DataTable
    Dim Con As New OleDb.OleDbConnection
    Dim StrCondition As String = ""

    Const MENUID = "Location"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            Cmd.CommandText = "SELECT ZoneCode FROM Zones ORDER BY ZoneCode"
            Rdr = Cmd.ExecuteReader
            CmbZoneCode.Items.Add("Select...")
            While Rdr.Read
                CmbZoneCode.Items.Add(Rdr("ZoneCode"))
            End While
            Rdr.Close()

            Cmd.CommandText = "SELECT RackCode FROM Racks ORDER BY RackCode"
            Rdr = Cmd.ExecuteReader
            CmbRack.Items.Add("Select...")
            While Rdr.Read
                CmbRack.Items.Add(Rdr("RackCode"))
            End While
            Rdr.Close()
            FillGrid()
        End If
    End Sub

    Protected Sub FillGrid()
        Cmd.Connection = Con
        StrSql = "SELECT LocCode Location,ZoneCode Zone ,RackCode Rack,Aisle, Bay, Level, Side, PutHold, PickHold, MaxPRatio FROM Location"
        StrSql = StrSql & StrCondition & " ORDER BY LocCode"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvLoc.DataSource = Rdr
        GdvLoc.DataBind()
        Rdr.Close()
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        Dim Ev As New System.EventArgs

        If e.Item.CommandName = "New" Then
            TxtLocationCode.Text = ""
            CmbZoneCode.Text = ""
            CmbRack.Text = ""
            TxtAisle.Text = ""
            TxtBay.Text = ""
            TxtLevel.Text = ""
            TxtSide.Text = ""
            ChkPut.Checked = False
            ChkPick.Checked = False
            TxtMaxPRatio.Text = ""
            TxtLocationCode.Focus()
        End If
        If e.Item.CommandName = "Save" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Location Where LocCode = '" & TxtLocationCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                StrSql = "INSERT INTO Location (LocCode,RackCode,ZoneCode,MaxPRatio,Aisle,Bay,Side,Level,PutHold,PickHold) "
                StrSql = StrSql & "VALUES('" & TxtLocationCode.Text & "','"
                StrSql = StrSql & CmbRack.Text & "','"
                StrSql = StrSql & CmbZoneCode.Text & "',"
                StrSql = StrSql & Val(TxtMaxPRatio.Text) & ","
                StrSql = StrSql & Val(TxtAisle.Text) & ","
                StrSql = StrSql & Val(TxtBay.Text) & ","
                StrSql = StrSql & Val(TxtSide.Text) & ","
                StrSql = StrSql & Val(TxtLevel.Text) & ","
                StrSql = StrSql & IIf(ChkPut.Checked, 1, 0) & ","
                StrSql = StrSql & IIf(ChkPick.Checked, 1, 0) & ")"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
                TxtLocationCode.Text = ""
                CmbZoneCode.Text = ""
                CmbRack.Text = ""
                TxtAisle.Text = ""
                TxtBay.Text = ""
                TxtLevel.Text = ""
                TxtSide.Text = ""
                ChkPut.Checked = False
                ChkPick.Checked = False
                TxtMaxPRatio.Text = ""
                TxtLocationCode.Focus()
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
        If e.Item.CommandName = "Delete" Then
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Location Where LocCode = '" & TxtLocationCode.Text & "'"
            Try
                Cmd.ExecuteNonQuery()
                TxtLocationCode.Text = ""
                CmbZoneCode.SelectedIndex = 0
                CmbRack.SelectedIndex = 0
                TxtAisle.Text = ""
                TxtBay.Text = ""
                TxtLevel.Text = ""
                TxtSide.Text = ""
                ChkPut.Checked = False
                ChkPick.Checked = False
                TxtMaxPRatio.Text = ""
                TxtLocationCode.Focus()
                FillGrid()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(ToolBar1, Me.GetType, "ShowFind", "<script language='javascript'> w.Alert('" & Replace(ex.Message, "'", " ") & "','Error');</script>", False)
            End Try
        End If
    End Sub

    Private Sub GdvCurrency_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLoc.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLoc, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLoc.SelectedIndexChanged
        On Error Resume Next
        TxtLocationCode.Text = GdvLoc.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        CmbZoneCode.Text = GdvLoc.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
        CmbRack.Text = GdvLoc.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
        TxtAisle.Text = GdvLoc.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
        TxtBay.Text = GdvLoc.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
        TxtLevel.Text = GdvLoc.SelectedRow.Cells(5).Text.Replace("&nbsp;", "")
        TxtSide.Text = GdvLoc.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
        ChkPut.Checked = DirectCast(GdvLoc.Rows(0).Cells(7).Controls(0), CheckBox).Checked
        ChkPick.Checked = DirectCast(GdvLoc.Rows(0).Cells(8).Controls(0), CheckBox).Checked
        TxtMaxPRatio.Text = GdvLoc.SelectedRow.Cells(9).Text.Replace("&nbsp;", "")
    End Sub

    Private Sub GdvCurrency_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLoc.SelectedIndexChanging
        GdvLoc.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For i = 0 To Me.GdvLoc.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLoc.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub


    Protected Sub TxtLocationCode_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtLocationCode.TextChanged
        If TxtLocationCode.Text <> "" Then
            StrCondition = " WHERE LocCode LIKE '%" & TxtLocationCode.Text & "%'"
        Else
            StrCondition = ""
        End If
        TxtAisle.Text = ""
        TxtBay.Text = ""
        TxtMaxPRatio.Text = ""
        TxtSide.Text = ""
        CmbRack.SelectedIndex = 0
        CmbZoneCode.SelectedIndex = 0
        FillGrid()
    End Sub
End Class