﻿Public Partial Class NewMainMenu
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT * FROM Access WHERE UserID = '" & Session("UserID") & "'"
            Rdr = Cmd.ExecuteReader
            On Error Resume Next
            While Rdr.Read
                Page.FindControl(Rdr("MenuName")).Visible = True
            End While
            Rdr.Close()
            DirectCast(Page.FindControl("CurrentUser"), Label).Text = "User :" & Session("UserID")
        End If
    End Sub

End Class