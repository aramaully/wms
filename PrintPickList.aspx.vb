﻿Public Partial Class PrintPickList
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim CmdClient As New OleDb.OleDbCommand
    Dim RdrClient As OleDb.OleDbDataReader
    Dim CmdBoe As New OleDb.OleDbCommand
    Dim RdrBoe As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrSel As String

    Const MENUID = "PickingListRep"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Cmd.Connection = Con
        'Check Access
        StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar = 0 Then
            Response.Redirect("about:blank")
        End If
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub TxtBoeNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBoeNo.TextChanged
        StrSql = "SELECT * FROM BoeOut WHERE BillNo='" & TxtBoeNo.Text & "'"
        CmdBoe.Connection = Con
        CmdBoe.CommandText = StrSql
        RdrBoe = CmdBoe.ExecuteReader
        If RdrBoe.Read Then
            TxtDate.Text = Format(RdrBoe("BillDate"), "dd/MM/yyyy")
            StrSql = "SELECT * FROM Client WHERE ClientID='" & RdrBoe("ClientID") & "'"
            CmdClient.Connection = Con
            CmdClient.CommandText = StrSql
            RdrClient = CmdClient.ExecuteReader
            If RdrClient.Read Then
                TxtName.Text = RdrClient("Name")
                TxtAddress.Text = RdrClient("Address")
            End If
            RdrClient.Close()
        End If
        RdrBoe.Close()
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        StrSel = "{PickList.BoeNo} = '" & TxtBoeNo.Text & "'"
        'Response.Redirect("Report.aspx?RptID=Print Pick List&RPTFileName=PrintPickList.aspx&RptFile=RptPickList.rpt&StrSelF=" & StrSel)
        Session("ReportFile") = "RptPickList.rpt"
        Session("Period") = ""
        Session("Zone") = ""
        Session("SelFormula") = StrSel
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        If RbnClient.Text = "Name" Then
            StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE "
            If RbnClient.Checked Then
                StrSql = StrSql & "Name"
            ElseIf RbnCountry.Checked Then
                StrSql = StrSql & "City"
            Else
                StrSql = StrSql & "Contact"
            End If
            StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' ORDER BY Name"
        Else
            StrSql = "SELECT BoeOut.BillNo, CONVERT(Char,BoeOut.BillDate,103) 'Bill Date', BoeOut.Revision, Client.Name FROM BoeOut, Client WHERE "
            StrSql = StrSql & "BoeOut.ClientID = Client.ClientID "
            If RbnClient.Checked Then
                StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCountry.Checked Then
                StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnTraffic.Checked Then
                StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
            End If
            If RbnCont.Checked Then
                StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
            End If
            If ChkDate.Checked Then
                Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                StrSql = StrSql & "AND BoeOut.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
                StrSql = StrSql & "AND BoeOut.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
            End If
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub


    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        If RbnClient.Text = "Client" Then
            TxtBoeNo.Text = GdvFind.SelectedRow.Cells(0).Text
            TxtBoeNo_TextChanged(Me, e)
        End If
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub BtnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnFind.Click
        RbnClient.Text = "Client"
        RbnCountry.Text = "Country"
        RbnTraffic.Text = "Traffic Op"
        RbnCont.Visible = True
        ChkDate.Visible = True
        TxtFrom.Visible = True
        TxtTo.Visible = True
        LblToDate.Visible = True
        GdvFind.DataSource = Nothing
        TxtTo.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtFrom.Text = Date.Today.AddDays(-10).ToString("dd/MM/yyyy")
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub
End Class