﻿Public Partial Class Unit
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatLoc As New DataTable
    Dim StrCondition As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            BindUnit()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Private Sub GdvUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvUnit.SelectedIndexChanged
        On Error Resume Next
        TxtUnitCode.Text = ChkSpace(GdvUnit.SelectedRow.Cells(0).Text)
        TxtDescription.Text = ChkSpace(GdvUnit.SelectedRow.Cells(1).Text)
    End Sub

    Private Sub BindUnit()
        Cmd.Connection = Con
        StrSql = "SELECT UnitCode,Description FROM Unit"
        StrSql = StrSql & StrCondition & " ORDER BY UnitCode"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar Is Nothing Then Exit Sub
        Rdr = Cmd.ExecuteReader
        DatLoc.Load(Rdr)
        Rdr.Close()
        GdvUnit.DataSource = DatLoc
        GdvUnit.DataBind()
        If GdvUnit.Rows.Count = 1 Then
            On Error Resume Next
            TxtUnitCode.Text = ChkSpace(GdvUnit.Rows(0).Cells(0).Text)
            TxtDescription.Text = ChkSpace(GdvUnit.Rows(0).Cells(1).Text)
        End If
    End Sub

    Private Sub GdvUnit_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvUnit.PageIndexChanging
        GdvUnit.PageIndex = e.NewPageIndex
        BindUnit()
    End Sub

    Private Sub GdvUnit_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvUnit.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvUnit, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvUnit.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvUnit.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub GdvUnit_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvUnit.SelectedIndexChanging
        GdvUnit.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub TxtUnitCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtUnitCode.TextChanged
        If TxtUnitCode.Text <> "" Then
            StrCondition = " WHERE UnitCode LIKE '%" & TxtUnitCode.Text & "%'"
        Else
            StrCondition = ""
        End If
        TxtDescription.Text = ""
        BindUnit()
    End Sub

    Private Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        On Error Resume Next
        TxtUnitCode.Text = ""
        TxtDescription.Text = ""
        TxtUnitCode.Focus()
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Try
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Unit WHERE UnitCode = '" & TxtUnitCode.Text & "'"
            Cmd.ExecuteNonQuery()
            ButNew_Click(Me, e)
            BindUnit()
        Catch ex As Exception
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Cmd.CommandText = "SELECT UnitCode FROM Unit WHERE UnitCode = '" & TxtUnitCode.Text & "'"
        Try
            If Cmd.ExecuteScalar Is Nothing Then
                StrSql = "INSERT INTO Unit (UnitCode,Description) "
                StrSql = StrSql & "VALUES('" & TxtUnitCode.Text & "','"
                StrSql = StrSql & TxtDescription.Text & "')"
            Else
                StrSql = "UPDATE Unit SET UnitCode = '" & TxtUnitCode.Text & "',"
                StrSql = StrSql & "Description='" & TxtDescription.Text & "' WHERE UnitCode = '" & TxtUnitCode.Text & "'"
            End If
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ButNew_Click(Me, e)
            BindUnit()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class