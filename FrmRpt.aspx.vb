﻿Imports System.Drawing.Printing

Partial Public Class FrmRpt
    Inherits System.Web.UI.Page
    Dim Report As New CrystalDecisions.Web.CrystalReportSource

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Report.EnableCaching = False
        Report.Report.FileName = Session("ReportFile")
        If Session("Period").ToString <> "" Then
            Report.ReportDocument.DataDefinition.FormulaFields("Period").Text = Session("Period")
        End If
        If Session("Zone").ToString <> "" Then
            Report.ReportDocument.DataDefinition.FormulaFields("Zone").Text = Session("Zone")
        End If
        RepHead.Value = Session("RepHead")
        Report.ReportDocument.Refresh()

        CrystalViewer.ReportSource = Report
        CrystalViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
        CrystalViewer.SelectionFormula = Session("SelFormula")

        'LOCAL
        CrystalViewer.LogOnInfo(0).ConnectionInfo.ServerName = "(local)"
        CrystalViewer.LogOnInfo(0).ConnectionInfo.DatabaseName = "wmstest"
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.DatabaseName = "wms_uat"
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.DatabaseName = "wmszone9"

        'WMS_UAT
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.ServerName = "BFSLAPPSERVER"
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.DatabaseName = "wms_uat"

        'WMS
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.ServerName = "BFSLAPPSERVER"
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.DatabaseName = "wmstest"

        'WMS_Z9
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.ServerName = "BFSLAPPSERVER"
        'CrystalViewer.LogOnInfo(0).ConnectionInfo.DatabaseName = "wmszone9"

        CrystalViewer.LogOnInfo(0).ConnectionInfo.UserID = "admin"
        CrystalViewer.LogOnInfo(0).ConnectionInfo.Password = "swathi*"
        CrystalViewer.DataBind()
        CrystalViewer.RefreshReport()

        'End If
    End Sub

End Class