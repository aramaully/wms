﻿Imports System.Web.UI.DataVisualization.Charting
Imports DocumentFormat.OpenXml.Packaging
Imports System.IO
'Imports Word = Microsoft.Office.Interop.Word
Imports System.Data
Imports Xceed.Words.NET

Partial Public Class FrmDashBoard
    Inherits System.Web.UI.Page
    Dim CmdChrt As New OleDb.OleDbCommand
    Dim RdrChrt As OleDb.OleDbDataReader
    Dim CmdSDTariff As New OleDb.OleDbCommand
    Dim RdrSDTariff As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim I As Integer = 0
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdArea As New OleDb.OleDbCommand
    Dim RdrArea As OleDb.OleDbDataReader
    Dim LngArea1 As Long
    Dim LngArea2 As Long
    Dim LngDepInsurance As Long
    Dim StrDepInsurance As String

    Const MENUID = "Dashboard"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        If Session("NewClientID") <> "" Then
            TxtClientID.Text = Session("NewClientID")
        End If
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Con.ConnectionString = Session("ConnString")
        Con.Open()

        Cmd.Connection = Con
        'Check Access
        StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar = 0 Then
            Response.Redirect("about:blank")
        End If

        Try
            If Not Page.IsPostBack Then

                TabContainer1.ActiveTabIndex = 0
                LblMsg.ForeColor = Drawing.Color.Black
                LblMsg.Text = "Click on the New button for new proposal or enter proposal  no to revise"
                Cmd.Connection = Con
                Cmd.CommandText = "SELECT WhCode FpZone FROM WareHouse WHERE WhCode <> ''"
                CmbPtyFPZone.Items.Add("Select...")
                CmbFreeport.Items.Add("Select...")
                CmbChargeFPZone.Items.Add("Select...")
                CmbCopyFPZone.Items.Add("Select...")
                CmbProposalFPZone.Items.Add("Select...")
                CmbLeaseZone.Items.Add("Select...")


                CmbTemplateDoc.Items.Add("Select...")
                CmbTemplateDoc.Items.Add("Lease agreement cabin")
                CmbTemplateDoc.Items.Add("Lease agreement")
                CmbTemplateDoc.Items.Add("Lease Covering letter for New Lease")
                CmbTemplateDoc.Items.Add("Lease letter-Bonded Car Yard")

                '#################################################################################
                'CmbTemplateDoc.Items.Add("Lease Special Conditions - Non Processing")
                'CmbTemplateDoc.Items.Add("Lease Special Conditions - office")
                'CmbTemplateDoc.Items.Add("Lease Special Conditions - Warehousing - Processing")
                'CmbTemplateDoc.Items.Add("Lease Special Conditions - Warehousing")
                '#################################################################################

                CmbTemplateDoc.Items.Add("Special Conditions")
                CmbTemplateDoc.Items.Add("Special Conditions (only in case of escalation)")
                CmbTemplateDoc.Items.Add("Letter for non objection termination of lease")
                CmbTemplateDoc.Items.Add("Renewal letter")


                Rdr = Cmd.ExecuteReader
                While Rdr.Read
                    CmbPtyFPZone.Items.Add(Rdr("FpZone"))
                    CmbChargeFPZone.Items.Add(Rdr("FpZone"))
                    CmbCopyFPZone.Items.Add(Rdr("FpZone"))
                    CmbProposalFPZone.Items.Add(Rdr("FpZone"))
                    CmbLeaseZone.Items.Add(Rdr("FpZone"))
                    CmbFreeport.Items.Add(Rdr("FpZone"))
                End While
                Rdr.Close()
                CmbPtyFPZone.SelectedIndex = 0

                StrSql = "SELECT [Name] FROM Country ORDER BY CountryCode"
                Cmd.Connection = Con
                Cmd.CommandText = StrSql
                CmbCountry.Items.Add("Select...")
                Rdr = Cmd.ExecuteReader
                While Rdr.Read
                    CmbCountry.Items.Add(Rdr("Name"))
                End While
                Rdr.Close()

            End If
            If Page.IsPostBack And PnlFind.Style.Item("display") = "" Then
                DragPanel.Style.Item("left") = HfLeft.Value
                DragPanel.Style.Item("top") = HfTop.Value
            End If
            UpdateProperty()
            UpdateCar()
            Session("LoggedIn") = True
        Catch ex As Exception

        End Try
    End Sub

    Private Sub UpdateChart()
        'Update Chart
        Chart1.Series(0).PostBackValue = "#VALX"
        Chart1.Series(0).LegendPostBackValue = "#VALX"
        StrSql = "SELECT COUNT(*),'Occupied' FROM Property WHERE LeaseEnd >= GETDATE() And FpZone = '" & CmbFreeport.Text & "' "
        StrSql = StrSql & " UNION ALL "
        StrSql = StrSql & "SELECT COUNT(*),'Expired' FROM Property WHERE LeaseEnd < GETDATE() And FpZone = '" & CmbFreeport.Text & "' "
        StrSql = StrSql & " UNION ALL "
        StrSql = StrSql & "SELECT COUNT(*),'Vacant' FROM Property WHERE LeaseEnd IS NULL And FpZone = '" & CmbFreeport.Text & "' "
        Chart1.Series("Series1").Label = "#VALY"
        Chart1.Series(0).LegendText = "#VALX" & " - " & "#VALY"
        CmdChrt.Connection = Con
        CmdChrt.CommandText = StrSql
        RdrChrt = CmdChrt.ExecuteReader
        ' Set Doughnut chart type
        Chart1.Series(0).ChartType = SeriesChartType.Doughnut
        Chart1.Series(0)("DoughnutRadius") = "90"
        Dim Dt As New DataTable()
        Dim Dtc As New DataColumn
        Dtc.DataType = Type.GetType("System.String")
        Dtc.ColumnName = "Type"
        Dt.Columns.Add(Dtc)
        Dtc = New DataColumn
        Dtc.DataType = Type.GetType("System.Int32")
        Dtc.ColumnName = "Count"
        Dt.Columns.Add(Dtc)
        While RdrChrt.Read
            If RdrChrt(0) > 0 Then
                Dim Dr As DataRow = Dt.NewRow
                Dr("Count") = RdrChrt(0)
                Dr("Type") = RdrChrt(1)
                Dt.Rows.Add(Dr)
            Else
                Dim Dr As DataRow = Dt.NewRow
                Dr("Count") = RdrChrt(0)
                Dr("Type") = RdrChrt(1)
                Dt.Rows.Add(Dr)
            End If
        End While
        RdrChrt.Close()
        Chart1.DataSource = Dt
        Chart1.Series(0).XValueMember = "Type"
        Chart1.Series(0).YValueMembers = "Count"
        Chart1.DataBind()
        For Each Point As DataPoint In Chart1.Series(0).Points
            If Point.AxisLabel = "Occupied" Then
                Point.Color = Drawing.Color.Yellow
            End If
            If Point.AxisLabel = "Expired" Then
                Point.Color = Drawing.Color.Blue
            End If
            If Point.AxisLabel = "Vacant" Then
                Point.Color = Drawing.Color.Green
            End If
        Next

    End Sub

    Private Sub Chart1_Click(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ImageMapEventArgs) Handles Chart1.Click
        Dim Point As DataPoint = Nothing
        For Each Point In Chart1.Series(0).Points
            Point.CustomProperties = ""
            If Point.AxisLabel = e.PostBackValue Then
                Point.CustomProperties = "Exploded = true"
            End If
        Next Point
        Select Case e.PostBackValue
            Case "Occupied"
                Session("VLHead") = "List of Cabins Occupied Currently"
                StrSql = "SELECT Client.Name,Client.Category, Property.Area,PropertyCode,Property.[Type],CONVERT(CHAR,LeaseStart,103) 'Start',"
                StrSql = StrSql & "CONVERT(CHAR,LeaseEnd,103) 'End',CONVERT(CHAR,Property.MovedIn,103) 'Date Move In', LeaseAgreement.AddRemarks Remarks FROM Property LEFT OUTER JOIN Client ON "
                StrSql = StrSql & "Client.ClientID = Property.ClientID LEFT OUTER JOIN LeaseAgreement On Property.PropertyCode=LeaseAgreement.PropCode "
                StrSql = strsql & "WHERE Property.LeaseEnd >= GETDATE() And Property.FpZone = '" & CmbFreeport.Text & "'"
            Case "Expired"
                Session("VLHead") = "List of Cabins Lease Expired Currently"
                StrSql = "SELECT Client.Name,Client.Category, Property.Area,PropertyCode,Property.[Type],CONVERT(CHAR,LeaseStart,103) 'Start',"
                StrSql = StrSql & "CONVERT(CHAR,LeaseEnd,103) 'End',CONVERT(CHAR,Property.MovedIn,103) 'Date Move In', LeaseAgreement.AddRemarks Remarks FROM Property LEFT OUTER JOIN Client ON "
                StrSql = StrSql & "Client.ClientID = Property.ClientID LEFT OUTER JOIN LeaseAgreement On Property.PropertyCode=LeaseAgreement.PropCode "
                StrSql = StrSql & "WHERE Property.LeaseEnd  < GETDATE() And Property.FpZone = '" & CmbFreeport.Text & "'"
            Case "Vacant"
                Session("VLHead") = "List of Cabins Vacant Currently"
                StrSql = "SELECT  PropertyCode,Property.Area,Property.[Type],Client.Name, "
                StrSql = StrSql & "LeaseAgreement.AddRemarks FROM Property LEFT OUTER JOIN Client ON "
                StrSql = StrSql & "Client.ClientID = Property.ClientID LEFT OUTER JOIN LeaseAgreement On Property.PropertyCode=LeaseAgreement.PropCode "
                StrSql = StrSql & "WHERE Property.LeaseEnd IS NULL And Property.FpZone = '" & CmbFreeport.Text & "'"
        End Select
        Session("ListSql") = StrSql
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        DragPanel.Style.Item("display") = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Protected Sub ButSelect_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
        DragPanel.Style.Item("display") = "none"
    End Sub

    Protected Sub ButRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRefresh.Click
        StrSql = "SELECT ClientID,Name,Address,Contact FROM Client WHERE ClientID <> '' "
        If TxtSearch.Text <> "" Then
            If RbtName.Checked Then
                StrSql = StrSql & "AND Name"
            ElseIf RbtCity.Checked Then
                StrSql = StrSql & "AND City"
            Else
                StrSql = StrSql & "AND Contact"
            End If
            StrSql = StrSql & " LIKE '%" + TxtSearch.Text + "%' "
        End If
        If ChkPotential.Checked Then
            StrSql = StrSql & "AND PotentialClient = 1 "
        End If
        StrSql = StrSql & "ORDER BY Name"
        Try
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            GdvFind.DataSource = Rdr
            GdvFind.DataBind()
            Rdr.Close()
        Catch ex As Exception
            TxtName.Text = ex.Message
        End Try
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='pointer';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvTariffCard.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvTariffCard.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvProposal.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvProposal.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvFindLease.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFindLease.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvProperty.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvProperty.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvOpReq.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvOpReq.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvCar.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvCar.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Protected Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtClientID.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtClientID_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
        DragPanel.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        StrSql = "SELECT * FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        ClearClient()
        If Rdr.Read Then
            On Error Resume Next
            If Rdr("PotentialClient") Then
                TxtName.BackColor = Drawing.Color.Green
            Else
                TxtName.BackColor = Drawing.Color.White
            End If
            TxtCategory.Text = Trim(Rdr("Category")).ToString
            TxtClientID.Text = Rdr("ClientID").ToString
            TxtPClientCode.Text = Rdr("ClientID").ToString
            TxtLeaseClient.Text = Rdr("ClientID").ToString
            TxtCarClientID.Text = Rdr("ClientID").ToString
            TxtTClientID.Text = Rdr("ClientID").ToString
            TxtPClientName.Text = Rdr("Name").ToString
            TxtTClientName.Text = Rdr("Name")
            TxtName.Text = Rdr("Name").ToString
            TxtCarClientName.Text = Rdr("Name").ToString
            TxtLeaseName.Text = Rdr("Name").ToString
            TxtAddress.Text = Rdr("Address").ToString
            TxtCity.Text = Rdr("City").ToString
            TxtTel.Text = Rdr("Telephone").ToString
            TxtFaxNo.Text = Rdr("Fax").ToString
            TxtEmail.Text = Rdr("EMail").ToString
            TxtContact.Text = Rdr("Contact").ToString
            TxtPosition.Text = Rdr("Position").ToString
            TxtCellNo.Text = Rdr("CellNo").ToString
            TxtDate.Text = Rdr("DateCreated").ToString.Substring(1, 10)
            TxtRemarks.Text = Rdr("Remarks").ToString
            TxtVatNo.Text = Rdr("VATNo").ToString
            TxtLicenceNo.Text = Rdr("LicenceNo").ToString
            TxtStoragePermit.Text = Rdr("StoragePermit").ToString
            TxtLicExp.Text = Rdr("LicenceExp").ToString.Substring(1, 10)
            TxtPermitExp.Text = Rdr("PermitExp").ToString.Substring(1, 10)
            TxtStatus.Text = Rdr("Status").ToString
            TxtProType.Text = Rdr("ProdType").ToString
            TxtActivities.Text = Rdr("Activity").ToString
            TxtBRN.Text = Rdr("ClientBRN").ToString
            TxtWeb.Text = Rdr("WebURL").ToString
            CmbCountry.Text = Rdr("Country").ToString
            CmdArea.Connection = Con
            LngArea1 = 0
            LngArea2 = 0
            CmdArea.CommandText = "Select Sum(Area) From LeaseAgreement Where ClientID = '" & TxtClientID.Text & "' And MovedIN = 1"
            LngArea1 = CmdArea.ExecuteScalar
            CmdArea.CommandText = "Select Sum(Area) From LeaseAgreement Where ClientID = '" & TxtClientID.Text & "' And MovedOUT = 1"
            LngArea2 = CmdArea.ExecuteScalar
            TxtAreaLeased.Text = LngArea1 - LngArea2
            TxtAddress2.Text = Rdr("Address2").ToString
            Select Case IIf(IsDBNull(Rdr("FPZones")), 0, Rdr("FPZones"))
                Case 1
                    ChkZone1.Checked = True
                    ChkZone6.Checked = False
                    ChkZone9.Checked = False
                Case 6
                    ChkZone1.Checked = False
                    ChkZone6.Checked = True
                    ChkZone9.Checked = False
                Case 9
                    ChkZone1.Checked = False
                    ChkZone6.Checked = False
                    ChkZone9.Checked = True
                Case 7
                    ChkZone1.Checked = True
                    ChkZone6.Checked = True
                    ChkZone9.Checked = False
                Case 10
                    ChkZone1.Checked = True
                    ChkZone6.Checked = False
                    ChkZone9.Checked = True
                Case 15
                    ChkZone1.Checked = False
                    ChkZone6.Checked = True
                    ChkZone9.Checked = True
                Case 16
                    ChkZone1.Checked = True
                    ChkZone6.Checked = True
                    ChkZone9.Checked = True
                Case Else
                    ChkZone1.Checked = False
                    ChkZone6.Checked = False
                    ChkZone9.Checked = False
            End Select
            GetTariffCard()
        End If
        Rdr.Close()
        'UpdateCharges()
        'Update Lease Agreements
        StrSql = "SELECT LeaseNo FROM LeaseAgreement WHERE ClientID = '" & TxtClientID.Text & "'"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        CmbLeaseNo.Items.Clear()
        CmbLeaseNo.Items.Add("Select...")
        While Rdr.Read
            CmbLeaseNo.Items.Add(Rdr("LeaseNo"))
        End While
        Rdr.Close()
        'CmbLeaseNo_SelectedIndexChanged(Me, e)
    End Sub

    Private Sub UpdateCharges()
        StrSql = "SELECT WhCode,ChCode,AnalysisCode,Description,Unit,Charge FROM ClientCharges WHERE ClientID = '"
        StrSql = StrSql & TxtClientID.Text & "' ORDER BY WhCode, ChCode"
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
    End Sub

    Private Sub ClearAll()
        On Error Resume Next
        TxtCategory.Text = ""
        TxtName.Text = ""
        TxtAddress.Text = ""
        TxtCity.Text = ""
        TxtTel.Text = ""
        TxtFaxNo.Text = ""
        TxtEmail.Text = ""
        TxtContact.Text = ""
        TxtVatNo.Text = ""
        TxtPosition.Text = ""
        TxtDate.Text = Date.Today
        TxtRemarks.Text = ""
        TxtLicExp.Text = ""
        TxtPermitExp.Text = ""
        TxtLicenceNo.Text = ""
        TxtStoragePermit.Text = ""
        TxtStatus.Text = ""
        TxtProType.Text = ""
        TxtProposalNo.Text = ""
        TxtPreparedBy.Text = ""
        TxtPropDate.Text = Date.Today
        ChkPrelim.Checked = True
        ChkFinal.Checked = False
        ChkRevised.Checked = False
        TxtValidity.Text = ""
        TxtRevision.Text = 0
        TxtRate.Text = ""
        TxtArea.Text = ""
        TxtRate.Text = ""
        TxtAmount.Text = ""
        TxtSCharges.Text = ""
        TxtDeposit.Text = ""
        TxtInsurance.Text = ""
        TxtBRN.Text = ""
        TxtActivities.Text = ""
        TxtAreaLeased.Text = ""
        TxtWeb.Text = ""
        TxtAddress2.Text = ""
        CmbLeaseNo.SelectedIndex = 0
        TxtLeasePropNo.Text = ""
        TxtProposalDate.Text = ""
        TxtPropRevision.Text = ""
        TxtLeaseBy.Text = ""
        TxtLeaseValidity.Text = ""
        TxtLeaseReceived.Text = ""
        TxtFittedPeriod.Text = ""

        TxtLeasePropType.Text = ""
        CmbLeaseZone.SelectedIndex = 0
        TxtLeaseFpZone.Text = ""
        TxtLeaseArea.Text = ""
        LblLeaseUnit.Text = ""
        TxtLeaseRate.Text = ""
        TxtLeaseAmount.Text = ""
        TxtLeaseSCharge.Text = ""
        TxtLeaseDeposit.Text = ""
        TxtLeaseInsurance.Text = ""
        TxtLeasePropDesc.Text = ""
        TxtLeaseDate.Text = ""
        'update status
        OptActive.Checked = False
        OptExpired.Checked = False
        OptRenewed.Checked = False
        'Update Move In Data
        TxtMoveIn.Text = ""
        TxtMoveInPropCode.Text = ""
        TxtMoveInArea.Text = ""
        TxtLeaseOn.Text = ""
        TxtLeaseExp.Text = ""
        TxtDepositAmt.Text = ""
        TxtMoveInRate.Text = ""
        TxtPropMonthlyRent.Text = ""
        TxtMoveIn.Text = ""
        TxtDepositPaidOn.Text = ""
        TxtGuaranteeNo.Text = ""
        TxtGuaranteeExp.Text = ""
        TxtFpActivity.Text = ""
        TxtAdRemarks.Text = ""
        TxtOpHours.Text = ""
        TxtElecCharges.Text = ""
        TxtWaterCharge.Text = ""
        TxtTelCharges.Text = ""
        TxtWasteWater.Text = ""
        TxtDateofEntry.Text = ""
        TxtEscalation.Text = ""
        TxtBGAmount.Text = ""
        TxtBGStartDate.Text = ""
        TxtBGEndDate.Text = ""
        TxtBillingFrom.Text = ""
        TxtBillingTo.Text = ""

        'GdvLeaseService.DataSource = Nothing
        'GdvLeaseService.DataBind()
        GdvOpReq.DataSource = Nothing
        GdvOpReq.DataBind()
        CmbLeaseNo.Items.Clear()
        CmbLeaseNo.Items.Clear()
        TxtPropDate.Text = ""
        TxtPreparedBy.Text = ""
        TxtValidity.Text = ""
        CmbProposalFPZone.Text = ""
        TxtWarehouse.Text = ""
        TxtArea.Text = ""
        LblUnit.Text = ""
        TxtRate.Text = ""
        TxtAmount.Text = ""
        TxtSCharges.Text = ""
        TxtDeposit.Text = ""
        TxtInsurance.Text = ""
        TxtZoneName.Text = ""
        'update status
        ChkAccepted.Checked = False
        ChkFinal.Checked = False
        ChkPending.Checked = False
        ChkPrelim.Checked = False
        ChkRejected.Checked = False
        ChkRevised.Checked = False
        TxtZoneCode.Text = ""
        'Clear services grid
        'GdvServices.DataSource = Nothing
        'GdvServices.DataBind()
        'Clear Move Out Panel
        TxtMoveOutAmountRemarks.Text = ""
        TxtMoveOutAmtDue.Text = ""
        TxtMoveOutArea.Text = ""
        TxtMoveOutDate.Text = ""
        TxtMoveOutDeposit.Text = ""
        TxtMoveOutDesc.Text = ""
        TxtMoveOutGoodsRemarks.Text = ""
        TxtMoveOutKeyRemarks.Text = ""
        TxtMoveOutLeaseNo.Text = ""
        TxtMoveOutPropCode.Text = ""
        TxtMoveOutRate.Text = ""
        TxtMoveOutReason.Text = ""
        TxtMoveOutRefundOn.Text = ""
        ChkMoveOutAmount.Checked = False
        ChkMoveOutGoods.Checked = False
        ChkMoveOutKeys.Checked = False
        ButNewLease.Enabled = True
        LblRenewal.Visible = False
    End Sub

    Private Sub RefreshAll()
        'client
        TxtClientID.Text = ""
        TxtCategory.Text = ""
        TxtName.Text = ""
        TxtAddress.Text = ""
        TxtAddress2.Text = ""
        TxtCity.Text = ""
        TxtTel.Text = ""
        TxtFaxNo.Text = ""
        TxtEmail.Text = ""
        TxtWeb.Text = ""
        TxtContact.Text = ""
        TxtPosition.Text = ""
        TxtCellNo.Text = ""
        TxtDate.Text = ""
        TxtStatus.Text = ""
        TxtVatNo.Text = ""
        ChkZone1.Checked = False
        ChkZone6.Checked = False
        ChkZone9.Checked = False
        TxtLicenceNo.Text = ""
        TxtLicExp.Text = ""
        TxtStoragePermit.Text = ""
        TxtPermitExp.Text = ""
        TxtBRN.Text = ""
        TxtAreaLeased.Text = ""
        TxtProType.Text = ""
        TxtActivities.Text = ""
        TxtRemarks.Text = ""
        'Property
        TxtPropertyCode.Text = ""
        CmbPtyFPZone.SelectedIndex = -1
        TxtPtyArea.Text = ""
        CmbPtyUnit.SelectedIndex = -1
        TxtPtyDesc.Text = ""
        CmbPropType.SelectedIndex = -1
        TxtPtyClientID.Text = ""
        TxtPtyClientName.Text = ""
        TxtPtyLease.Text = ""
        TxtPtyStart.Text = ""
        TxtPtyEnd.Text = ""
        TxtPtyMovedIn.Text = ""
        TxtPtyPreviousClient.Text = ""
        TxtPtyMovedOut.Text = ""
        TxtPtyBilledUntil.Text = ""
        TxtPtyRemarks.Text = ""
        TxtMonthlyInsurance.Text = ""
        GdvProperty.DataSource = Nothing
        GdvProperty.DataBind()
        UpdateProperty()
        'TrariffCard
        CmbChargeFPZone.SelectedIndex = -1
        CmbChType.SelectedIndex = -1
        TxtChCode.Text = ""
        TxtChDescription.Text = ""
        CmbCondition.SelectedIndex = -1
        CmbDaysApp.SelectedIndex = -1
        TxtFromTime.Text = ""
        TxtToTime.Text = ""
        TxtChargeRate.Text = ""
        TxtChargeUnit.Text = ""
        TxtPerChDays.Text = ""
        TxtMinCharge.Text = ""
        TxtMinUnit.Text = ""
        CmbCopyFPZone.SelectedIndex = -1
        GdvTariffCard.DataSource = Nothing
        GdvTariffCard.DataBind()
        GetTariffCard()
        'Proposal Builder
        TxtPClientCode.Text = ""
        TxtPClientName.Text = ""
        TxtProposalNo.Text = ""
        TxtPropDate.Text = ""
        TxtRevision.Text = ""
        CmbProposalFPZone.SelectedIndex = -1
        TxtValidity.Text = ""
        TxtPreparedBy.Text = ""
        ChkPrelim.Checked = True
        ChkRevised.Checked = False
        ChkFinal.Checked = False
        ChkAccepted.Checked = False
        ChkRejected.Checked = False
        ChkPending.Checked = False
        TxtWarehouse.Text = ""
        TxtZoneCode.Text = ""
        TxtZoneName.Text = ""
        TxtPropType.Text = ""
        TxtArea.Text = ""
        TxtRate.Text = ""
        TxtSCharges.Text = ""
        TxtInsurance.Text = ""
        TxtDeposit.Text = ""
        TxtAmount.Text = ""
        TxtPropMonthlyRent.Text = ""
        'GdvServices.DataSource = Nothing
        'GdvServices.DataBind()
        TxtFollowUp.Text = ""
        TxtPropRemarks.Text = ""
        'lease Agreement 
        CmbLeaseNo.SelectedIndex = -1
        TxtLeaseDate.Text = ""
        TxtLeaseClient.Text = ""
        TxtLeaseName.Text = ""
        TxtLeasePropNo.Text = ""
        TxtProposalDate.Text = ""
        TxtPropRevision.Text = ""
        CmbLeaseZone.SelectedIndex = -1
        TxtLeaseValidity.Text = ""
        TxtLeaseBy.Text = ""
        TxtFittedPeriod.Text = ""
        TxtLeaseReceived.Text = ""
        OptActive.Checked = True
        OptExpired.Checked = False
        OptRenewed.Checked = False
        TxtLeaseFpZone.Text = ""
        TxtLeasePropertyCode.Text = ""
        TxtLeasePropDesc.Text = ""
        TxtLeasePropType.Text = ""
        TxtLeaseArea.Text = ""
        TxtLeaseRate.Text = ""
        TxtLeaseSCharge.Text = ""
        TxtLeaseInsurance.Text = ""
        TxtLeaseDeposit.Text = ""
        TxtLeaseAmount.Text = ""
        TxtLeaseMonthlyCharge.Text = ""
        TxtEscalation.Text = ""
        TxtBGAmount.Text = ""
        TxtBGStartDate.Text = ""
        TxtBGEndDate.Text = ""
        TxtBillingFrom.Text = ""
        TxtBillingTo.Text = ""

        'GdvLeaseService.DataSource = Nothing
        'GdvLeaseService.DataBind()
        'movein 
        TxtMoveIn.Text = ""
        TxtMoveInPropCode.Text = ""
        TxtDateofEntry.Text = ""
        TxtMoveInArea.Text = ""
        TxtMoveInRate.Text = ""
        TxtMonthlyRent.Text = ""
        TxtLeaseOn.Text = ""
        TxtLeaseExp.Text = ""
        TxtDepositAmt.Text = ""
        TxtDepositPaidOn.Text = ""
        TxtGuaranteeNo.Text = ""
        TxtGuaranteeExp.Text = ""
        TxtOpHours.Text = ""
        TxtMoveInInsurance.Text = ""
        TxtElecCharges.Text = ""
        TxtTelCharges.Text = ""
        TxtWaterCharge.Text = ""
        TxtWasteWater.Text = ""
        TxtMoveINServiceCharge.Text = ""
        TxtFpActivity.Text = ""
        TxtAdRemarks.Text = ""
        TxtOpReq.Text = ""
        TxtFollow.Text = ""
        TxtWho.Text = ""
        ChkDone.Checked = False
        GdvOpReq.DataSource = Nothing
        GdvOpReq.DataBind()
        'moveout 
        TxtMoveOutLeaseNo.Text = ""
        TxtMoveOutPropCode.Text = ""
        TxtMoveOutDesc.Text = ""
        TxtMoveOutArea.Text = ""
        TxtMoveOutRate.Text = ""
        TxtMoveOutDeposit.Text = ""
        TxtMoveOutDate.Text = ""
        TxtMoveOutAmtDue.Text = ""
        TxtMoveOutRefundOn.Text = ""
        ChkMoveOutKeys.Checked = False
        TxtMoveOutKeyRemarks.Text = ""
        ChkMoveOutGoods.Checked = False
        TxtMoveOutGoodsRemarks.Text = ""
        ChkMoveOutAmount.Checked = False
        TxtMoveOutAmountRemarks.Text = ""
        TxtMoveOutReason.Text = ""

        'occupancyStatus
        CmbFreeport.SelectedIndex = -1
        UpdateChart()

        'template
        CmbTemplateDoc.SelectedIndex = -1

        'Car Tariff
        TxtCarClientID.Text = ""
        TxtCarClientName.Text = ""
        TxtCarAdditional.Text = ""
        TxtCar15.Text = ""
        TxtCarFreeForkLift.Text = ""
        GdvCar.DataSource = Nothing
        GdvCar.DataBind()
        UpdateCar()

    End Sub

    Private Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "CreateNewClient", "<script language='javascript'> NewClient();</script>", False)
    End Sub

    Protected Sub ButNewProp_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNewProp.Click
        TxtProposalNo.Text = GetNewProposalNo()
        TxtPreparedBy.Text = Session("UserName")
        TxtPropDate.Text = Date.Today
        ChkPrelim.Checked = True
        ChkFinal.Checked = False
        ChkRevised.Checked = False
        ChkRejected.Checked = False
        ChkPending.Checked = False
        ChkAccepted.Checked = False
        TxtValidity.Text = ""
        TxtRevision.Text = 0
        TxtDeposit.Text = ""
        TxtInsurance.Text = ""
        'GdvServices.DataSource = Nothing
        'GdvServices.DataBind()
    End Sub

    Private Function GetNewProposalNo() As String
        'Get New Proposal Ref
        Dim StrRef As String = ""
        Cmd.Connection = Con
        StrSql = "SELECT NextProposalNo From SysParam"
        Cmd.CommandText = StrSql
        StrRef = Cmd.ExecuteScalar.ToString
        StrRef = Session("UserID") & "/" & StrRef & "/" & Date.Today.ToString("yy")
        Return StrRef
    End Function

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Cmd.Connection = Con
        StrSql = "SELECT COUNT(*) FROM Proposal WHERE ProposalNo = '" & TxtProposalNo.Text & "' And Revision = " & TxtRevision.Text & ""
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar = 0 Then
            'New Proposal
            TxtSCharges_TextChanged(Me, e)
            TxtAmount_TextChanged(Me, e)
            TxtProposalNo.Text = GetNewProposalNo()
            StrSql = "INSERT INTO Proposal (ProposalNo,Revision,ClientId,PropDate,PreparedBy,Validity,"
            StrSql = StrSql & "Zone,PropCode,Area,Unit,Rate,Amount,ConCharges,Deposit,Status,Insurance,MonthlyRent,FollowUp,Remarks) VALUES('"
            StrSql = StrSql & TxtProposalNo.Text & "',"
            StrSql = StrSql & Val(TxtRevision.Text) & ",'"
            StrSql = StrSql & TxtPClientCode.Text & "',"
            If TxtProposalDate.Text <> "" Then
                StrSql = StrSql & "'" & CDate(TxtPropDate.Text).ToString("dd-MMM-yyyy") & "','"
            Else
                StrSql = StrSql & "NULL,'"
            End If

            StrSql = StrSql & TxtPreparedBy.Text & "',"
            If TxtValidity.Text <> "" Then
                StrSql = StrSql & "'" & Format(CDate(TxtValidity.Text), "MM/dd/yyyy") & "','"
            Else
                StrSql = StrSql & "NULL,'"
            End If
            StrSql = StrSql & TxtWarehouse.Text & "','"
            StrSql = StrSql & TxtZoneCode.Text & "',"
            StrSql = StrSql & Val(TxtArea.Text) & ",'"
            StrSql = StrSql & LblUnit.Text & "',"
            StrSql = StrSql & Val(TxtRate.Text) & ","
            StrSql = StrSql & Val(TxtAmount.Text) & ","
            StrSql = StrSql & Val(TxtSCharges.Text) & ","
            StrSql = StrSql & Val(TxtDeposit.Text) & ",'"
            If ChkPrelim.Checked Then
                StrSql = StrSql & ChkPrelim.Text & "',"
            ElseIf ChkRevised.Checked Then
                StrSql = StrSql & ChkRevised.Text & "',"
            ElseIf ChkFinal.Checked Then
                StrSql = StrSql & ChkFinal.Text & "',"
            ElseIf ChkAccepted.Checked Then
                StrSql = StrSql & ChkAccepted.Text & "',"
            ElseIf ChkRejected.Checked Then
                StrSql = StrSql & ChkRejected.Text & "',"
            ElseIf ChkPending.Checked Then
                StrSql = StrSql & ChkPending.Text & "',"
            End If
            StrSql = StrSql & Val(TxtInsurance.Text) & ","
            StrSql = StrSql & Val(TxtPropMonthlyRent.Text) & ","
            If TxtFollowUp.Text <> "" Then
                StrSql = StrSql & "'" & Format(CDate(TxtFollowUp.Text), "MM/dd/yyyy") & "','" & TxtPropRemarks.Text & "')"
            Else
                StrSql = StrSql & "NULL,'" & TxtPropRemarks.Text & "')"
            End If
        Else
            'New Revision
            TxtSCharges_TextChanged(Me, e)
            TxtAmount_TextChanged(Me, e)
            'Cmd.CommandText = "SELECT MAX(Revision) FROM Proposal WHERE ProposalNo = '" & TxtProposalNo.Text & "'"
            'Dim IntRev As Integer = Cmd.ExecuteScalar
            'TxtRevision.Text = IntRev + 1

            StrSql = "UPDATE Proposal SET Revision = " & TxtRevision.Text & ","
            If TxtPropDate.Text <> "" Then
                StrSql = StrSql & "PropDate = '" & Format(CDate(TxtPropDate.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "PropDate = NULL,"
            End If
            StrSql = StrSql & "PreparedBy = '" & TxtPreparedBy.Text & "',"
            If TxtValidity.Text <> "" Then
                StrSql = StrSql & "Validity = '" & Format(CDate(TxtValidity.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "Validity = NULL,"
            End If
            StrSql = StrSql & "ClientId = '" & TxtPClientCode.Text & "',"
            StrSql = StrSql & "Zone = '" & CmbProposalFPZone.Text & "',"
            StrSql = StrSql & "PropCode = '" & TxtZoneCode.Text & "',"
            StrSql = StrSql & "Area = " & Val(TxtArea.Text) & ","
            StrSql = StrSql & "Unit = '" & LblUnit.Text & "',"
            StrSql = StrSql & "Rate = " & Val(TxtRate.Text) & ","
            StrSql = StrSql & "Amount = " & Val(TxtAmount.Text) & ","
            If ChkPrelim.Checked Then
                StrSql = StrSql & "Status = '" & ChkPrelim.Text & "',"
            ElseIf ChkRevised.Checked Then
                StrSql = StrSql & "Status = '" & ChkRevised.Text & "',"
            ElseIf ChkFinal.Checked Then
                StrSql = StrSql & "Status = '" & ChkFinal.Text & "',"
            ElseIf ChkAccepted.Checked Then
                StrSql = StrSql & "Status = '" & ChkAccepted.Text & "',"
            ElseIf ChkRejected.Checked Then
                StrSql = StrSql & "Status = '" & ChkRejected.Text & "',"
            ElseIf ChkPending.Checked Then
                StrSql = StrSql & "Status = '" & ChkPending.Text & "',"
            End If
            StrSql = StrSql & "Deposit = " & Val(TxtDeposit.Text) & ","
            StrSql = StrSql & "Insurance = " & Val(TxtInsurance.Text) & ","
            StrSql = StrSql & "MonthlyRent = " & Val(TxtPropMonthlyRent.Text) & ","
            If TxtFollow.Text <> "" Then
                StrSql = StrSql & "FollowUp = '" & Format(CDate(TxtFollowUp.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "FollowUp = NULL,"
            End If
            StrSql = StrSql & "Remarks = '" & TxtPropRemarks.Text & "'"
            StrSql = StrSql & " Where ProposalNo = '" & TxtProposalNo.Text & "' And Revision = " & TxtRevision.Text & ""
        End If
        Cmd.CommandText = StrSql
        Try
            Cmd.ExecuteNonQuery()
            Dim CmdSysParam As New OleDb.OleDbCommand
            Dim RdrSysParam As OleDb.OleDbDataReader
            Dim StrNextPropNo As String = ""
            CmdSysParam.Connection = Con
            CmdSysParam.CommandText = "Select *From SysParam"
            RdrSysParam = CmdSysParam.ExecuteReader
            If RdrSysParam.Read Then
                StrNextPropNo = Format((Int(RdrSysParam("NextProposalNo")) + 1), "00000")
            End If
            RdrSysParam.Close()
            StrSql = "Update SysParam Set NextProposalNo = '" & StrNextPropNo & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()

            ''Save additional services required
            'Cmd.CommandText = "Delete From PropServices Where ProposalNo = '" & TxtProposalNo.Text & "'"
            'Cmd.ExecuteNonQuery()
            'For Each Row As GridViewRow In GdvServices.Rows
            '    StrSql = "INSERT INTO PropServices (ProposalNo,Revision,ChCode) VALUES('"
            '    StrSql = StrSql & TxtProposalNo.Text & "',"
            '    StrSql = StrSql & Val(TxtRevision.Text) & ",'"
            '    StrSql = StrSql & Row.Cells(1).Text & "')"
            '    Cmd.CommandText = StrSql
            '    Cmd.ExecuteNonQuery()
            'Next
            LblMsg.ForeColor = Drawing.Color.Green
            LblMsg.Text = "Proposal Saved. Click on the Print button to print"
            LblMsg.Visible = True
        Catch ex As Exception
            LblMsg.ForeColor = Drawing.Color.Red
            LblMsg.Text = "Error Saving " & ex.Message
            LblMsg.Visible = True
        End Try
    End Sub

    Private Sub TxtProposalNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtProposalNo.TextChanged
        Cmd.Connection = Con
        StrSql = "SELECT Proposal.*,Property.Description,Property.FpZone FROM Proposal "
        StrSql = StrSql & "INNER JOIN Property ON Property.PropertyCode=Proposal.PropCode "
        StrSql = StrSql & "WHERE Proposal.ProposalNo = '" & TxtProposalNo.Text & "' AND "
        StrSql = StrSql & "Proposal.Revision = " & Val(TxtRevision.Text)
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        Dim StrZoneCode As String = ""
        If Rdr.Read Then
            If Not IsDBNull(Rdr("PropDate")) Then
                TxtPropDate.Text = Format(Rdr("PropDate"), "dd/MM/yyyy")
            Else
                TxtPropDate.Text = ""
            End If
            TxtPreparedBy.Text = Rdr("PreparedBy")
            If Not IsDBNull(Rdr("Validity")) Then
                TxtValidity.Text = Format(Rdr("Validity"), "dd/MM/yyyy")
            Else
                TxtValidity.Text = ""
            End If
            StrZoneCode = Rdr("PropCode").ToString
            CmbProposalFPZone.Text = Rdr("FpZone").ToString
            TxtWarehouse.Text = Rdr("FpZone").ToString
            TxtArea.Text = Rdr("Area")
            LblUnit.Text = Rdr("Unit")
            TxtRate.Text = Rdr("Rate")
            TxtAmount.Text = Rdr("Amount")
            TxtSCharges.Text = Rdr("ConCharges")
            TxtDeposit.Text = Rdr("Deposit")
            TxtInsurance.Text = Rdr("Insurance")
            TxtZoneName.Text = Rdr("Description")
            TxtPropMonthlyRent.Text = Rdr("MonthlyRent")
            TxtPropRemarks.Text = Rdr("Remarks").ToString
            If Not IsDBNull(Rdr("FollowUp")) Then
                TxtFollowUp.Text = Format(Rdr("FollowUp"), "dd/MM/yyyy")
            Else
                TxtFollowUp.Text = ""
            End If
            TxtSCharges_TextChanged(Me, e)
            TxtAmount_TextChanged(Me, e)
            'update status
            ChkAccepted.Checked = False
            ChkFinal.Checked = False
            ChkPending.Checked = False
            ChkPrelim.Checked = False
            ChkRejected.Checked = False
            ChkRevised.Checked = False
            Select Case Rdr("Status")
                Case ChkAccepted.Text
                    ChkAccepted.Checked = True
                Case ChkFinal.Text
                    ChkFinal.Checked = True
                Case ChkPending.Text
                    ChkPending.Checked = True
                Case ChkPrelim.Text
                    ChkPrelim.Checked = True
                Case ChkRejected.Text
                    ChkRejected.Checked = True
                Case ChkRevised.Text
                    ChkRevised.Checked = True
            End Select
        End If
        Rdr.Close()
        'TxtWarehouse_TextChanged(Me, e)
        TxtZoneCode.Text = StrZoneCode
        'Get services grid
        'StrSql = "SELECT PropServices.ChCode Code,TariffCard.Description,TariffCard.Type,TariffCard.Rate,TariffCard.Unit,TariffCard.MinCharge "
        'StrSql = StrSql & "FROM PropServices INNER JOIN TariffCard ON TariffCard.ChCode = PropServices.ChCode "
        'StrSql = StrSql & "WHERE PropServices.ProposalNo = '" & TxtProposalNo.Text & "'"
        'Cmd.CommandText = StrSql
        'Rdr = Cmd.ExecuteReader
        'GdvServices.DataSource = Rdr
        'GdvServices.DataBind()
    End Sub

    Private Sub ButFindProp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFindProp.Click
        DragPanel.Style.Item("display") = ""
        PnlFindProp.Style.Item("display") = ""
        StrSql = "SELECT ProposalNo,Revision,CONVERT(Char,PropDate,103) 'Date',ProjectDesc 'Project',Amount+ConCharges Rate FROM Proposal WHERE "
        StrSql = StrSql & "ClientID = '" & TxtPClientCode.Text & "' ORDER BY PropDate"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Dim da As New OleDb.OleDbDataAdapter(Cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvProposal.DataSource = ds
        GdvProposal.DataBind()
    End Sub

    Private Sub ButCloseProp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseProp.Click
        PnlFindProp.Style.Item("display") = "none"
        DragPanel.Style.Item("display") = "none"
    End Sub

    Private Sub ButCloseLease_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseLease.Click
        PlnFindLease.Style.Item("display") = "none"
        DragPanel.Style.Item("display") = "none"
    End Sub


    Private Sub GdvProposal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvProposal.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='pointer';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvProposal, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFindLease_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFindLease.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='pointer';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFindLease, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvProposal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvProposal.SelectedIndexChanged
        TxtProposalNo.Text = GdvProposal.SelectedRow.Cells(0).Text
        TxtRevision.Text = GdvProposal.SelectedRow.Cells(1).Text
        TxtProposalNo_TextChanged(Me, e)
        PnlFindProp.Style.Item("display") = "none"
        DragPanel.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFindLease_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFindLease.SelectedIndexChanged
        CmbLeaseNo.Text = GdvFindLease.SelectedRow.Cells(0).Text
        CmbLeaseNo_SelectedIndexChanged(Me, e)
        PlnFindLease.Style.Item("display") = "none"
        DragPanel.Style.Item("display") = "none"
    End Sub

    Private Sub GdvProposal_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvProposal.SelectedIndexChanging
        GdvProposal.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub GdvFindLease_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFindLease.SelectedIndexChanging
        GdvFindLease.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub ButNewProperty_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNewProperty.Click
        TxtPtyArea.Text = ""
        TxtPtyBilledUntil.Text = ""
        TxtPtyClientID.Text = ""
        TxtPtyClientName.Text = ""
        TxtPtyDesc.Text = ""
        TxtPtyEnd.Text = ""
        TxtPtyLease.Text = ""
        TxtPtyMovedIn.Text = ""
        TxtPtyPreviousClient.Text = ""
        TxtPtyRemarks.Text = ""
        TxtPtyStart.Text = ""
        CmbPtyFPZone.SelectedIndex = 0
        CmbPtyUnit.SelectedIndex = 0
        TxtPropertyCode.Text = GetNewPCode()
    End Sub

    Private Function GetNewPCode() As String
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT MAX(PropertyCode) FROM Property WHERE PropertyCode LIKE '%" & TxtPropertyCode.Text & "%'"
        Dim StrPCode As String = Cmd.ExecuteScalar
        'StrPCode = "PC" & Format(Val(StrPCode.Substring(3)) + 1, "000000")
        Dim StrNo As String = ""
        For I As Integer = Len(StrPCode) - 1 To 0 Step -1
            If IsNumeric(StrPCode.Substring(I)) Then
                StrNo = StrPCode.Substring(I)
            Else
                Exit For
            End If
        Next
        StrPCode = StrPCode.Substring(0, Len(StrPCode) - Len(StrNo))
        StrPCode = StrPCode & Val(StrNo) + 1
        Return StrPCode
    End Function

    Private Sub UpdateProperty()
        Cmd.Connection = Con
        If CmbPtyFPZone.Text = "Select..." Then
            StrSql = "SELECT Property.PropertyCode Code,Property.FPZone 'FP Zone',Property.[Description],Property.[Type],Property.Area,Property.Unit,Property.EquipmentUnit,Property.Equipment,"
            StrSql = StrSql & "Property.ClientID 'Client ID',Client.Name,Property.LeaseNo 'Lease No',CONVERT(CHAR,Property.LeaseStart,103) 'Start Date',"
            StrSql = StrSql & "CONVERT(CHAR,Leaseagreement.BillingTo,103) 'End Date',CONVERT(CHAR,Property.MovedIn,103) 'Date Moved In',"
            StrSql = StrSql & "Property.PrevClientID 'Previous Client',CONVERT(CHAR,Property.PrevMovedOut,103) 'Moved Out',CONVERT(CHAR,Leaseagreement.BillingTo,103) 'Billed Until',"
            StrSql = StrSql & "Property.Remarks,(Leaseagreement.Amount + Leaseagreement.ConCharges) As 'Rental' FROM Property LEFT OUTER JOIN Client ON Client.ClientID = Property.ClientID "
            StrSql = StrSql & "LEFT OUTER JOIN Leaseagreement ON Property.LeaseNo = Leaseagreement.LeaseNo "
            StrSql = StrSql & "ORDER BY Property.ClientID ,Property.PropertyCode "
        Else
            StrSql = "SELECT Property.PropertyCode Code,Property.FPZone 'FP Zone',Property.[Description],Property.[Type],Property.Area,Property.Unit,Property.EquipmentUnit,Property.Equipment,"
            StrSql = StrSql & "Property.ClientID 'Client ID',Client.Name,Property.LeaseNo 'Lease No',CONVERT(CHAR,Property.LeaseStart,103) 'Start Date',"
            StrSql = StrSql & "CONVERT(CHAR,Leaseagreement.BillingTo,103) 'End Date',CONVERT(CHAR,Property.MovedIn,103) 'Date Moved In',"
            StrSql = StrSql & "Property.PrevClientID 'Previous Client',CONVERT(CHAR,Property.PrevMovedOut,103) 'Moved Out',CONVERT(CHAR,Leaseagreement.BillingTo,103) 'Billed Until',"
            StrSql = StrSql & "Property.Remarks,(Leaseagreement.Amount + Leaseagreement.ConCharges) As 'Rental' FROM Property LEFT OUTER JOIN Client ON Client.ClientID = Property.ClientID "
            StrSql = StrSql & "LEFT OUTER JOIN Leaseagreement ON Property.LeaseNo = Leaseagreement.LeaseNo Where Property.FPZone = '" & CmbPtyFPZone.Text & "' "
            StrSql = StrSql & "ORDER BY Property.ClientID ,Property.PropertyCode "
        End If
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvProperty.DataSource = Rdr
        GdvProperty.DataBind()
        Rdr.Close()

        Dim CmdCheckClient As New OleDb.OleDbCommand

        CmdCheckClient.Connection = Con


        For Each row As GridViewRow In GdvProperty.Rows
            CmdCheckClient.CommandText = "Select Name From Client Where ClientID ='" & row.Cells(14).Text & "'"
            row.Cells(14).Text = CmdCheckClient.ExecuteScalar
        Next

    End Sub

    Private Sub GdvProperty_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvProperty.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='pointer';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvProperty, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvCar_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvCar.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='pointer';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvCar, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub ClearClient()
        TxtCategory.Text = ""
        TxtName.Text = ""
        TxtAddress.Text = ""
        TxtAddress2.Text = ""
        TxtCity.Text = ""
        TxtTel.Text = ""
        TxtFaxNo.Text = ""
        TxtEmail.Text = ""
        TxtWeb.Text = ""
        TxtContact.Text = ""
        TxtPosition.Text = ""
        TxtCellNo.Text = ""
        TxtDate.Text = ""
        TxtStatus.Text = ""
        TxtVatNo.Text = ""
        ChkZone1.Checked = False
        ChkZone6.Checked = False
        ChkZone9.Checked = False
        TxtLicenceNo.Text = ""
        TxtLicExp.Text = ""
        TxtStoragePermit.Text = ""
        TxtPermitExp.Text = ""
        TxtBRN.Text = ""
        TxtAreaLeased.Text = ""
        TxtProType.Text = ""
        TxtActivities.Text = ""
        TxtRemarks.Text = ""
    End Sub

    Private Sub GdvProperty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvProperty.SelectedIndexChanged
        'On Error Resume Next
        'CmbPtyFPZone.SelectedIndex = 0
        'CmbPtyUnit.SelectedIndex = 0
        TxtPropertyCode.Text = GdvProperty.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        CmbPtyFPZone.Text = GdvProperty.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
        TxtPtyDesc.Text = GdvProperty.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
        CmbPropType.Text = GdvProperty.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
        TxtPtyArea.Text = GdvProperty.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
        CmbPtyUnit.Text = GdvProperty.SelectedRow.Cells(5).Text.Replace("&nbsp;", "")
        'TxtPtyEquipment.Text = GdvProperty.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
        'CmbPtyEquipment.Text = GdvProperty.SelectedRow.Cells(7).Text.Replace("&nbsp;", "")
        If GdvProperty.SelectedRow.Cells(8).Text.Replace("&nbsp;", "") = "" Then
            TxtZoneCode.Text = GdvProperty.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
            TxtWarehouse.Text = GdvProperty.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
            CmbProposalFPZone.Text = GdvProperty.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
            TxtZoneCode_TextChanged(Me, e)
        Else
            TxtPtyClientID.Text = GdvProperty.SelectedRow.Cells(8).Text.Replace("&nbsp;", "")
        End If
        TxtPtyClientName.Text = GdvProperty.SelectedRow.Cells(9).Text.Replace("&nbsp;", "")
        TxtPtyLease.Text = GdvProperty.SelectedRow.Cells(10).Text.Replace("&nbsp;", "")
        TxtPtyStart.Text = GdvProperty.SelectedRow.Cells(11).Text.Replace("&nbsp;", "")
        TxtPtyEnd.Text = GdvProperty.SelectedRow.Cells(12).Text.Replace("&nbsp;", "")
        TxtPtyMovedIn.Text = GdvProperty.SelectedRow.Cells(13).Text.Replace("&nbsp;", "")
        TxtPtyPreviousClient.Text = GdvProperty.SelectedRow.Cells(14).Text.Replace("&nbsp;", "")
        TxtPtyMovedOut.Text = GdvProperty.SelectedRow.Cells(15).Text.Replace("&nbsp;", "")
        TxtPtyBilledUntil.Text = GdvProperty.SelectedRow.Cells(16).Text.Replace("&nbsp;", "")
        TxtPtyRemarks.Text = GdvProperty.SelectedRow.Cells(17).Text.Replace("&nbsp;", "")
        TxtZoneCode.Text = GdvProperty.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        TxtPropertyMonthly.Text = GdvProperty.SelectedRow.Cells(18).Text.Replace("&nbsp;", "")
        If TxtPtyClientID.Text <> "" Then
            Cmd.Connection = Con
            StrSql = "SELECT * FROM Client WHERE ClientID = '" & TxtPtyClientID.Text & "'"
            Cmd.CommandText = StrSql
            TxtClientID.Text = ""
            TxtCategory.Text = ""
            TxtName.Text = ""
            TxtAddress.Text = ""
            TxtAddress2.Text = ""
            TxtCity.Text = ""
            TxtTel.Text = ""
            TxtFaxNo.Text = ""
            TxtEmail.Text = ""
            TxtWeb.Text = ""
            TxtContact.Text = ""
            TxtPosition.Text = ""
            TxtCellNo.Text = ""
            TxtDate.Text = ""
            TxtStatus.Text = ""
            TxtVatNo.Text = ""
            ChkZone1.Checked = False
            ChkZone6.Checked = False
            ChkZone9.Checked = False
            TxtLicenceNo.Text = ""
            TxtLicExp.Text = ""
            TxtStoragePermit.Text = ""
            TxtPermitExp.Text = ""
            TxtBRN.Text = ""
            TxtAreaLeased.Text = ""
            TxtProType.Text = ""
            TxtActivities.Text = ""
            TxtRemarks.Text = ""
            Rdr = Cmd.ExecuteReader
            If Rdr.Read Then
                On Error Resume Next
                If Rdr("PotentialClient") Then
                    TxtName.BackColor = Drawing.Color.Green
                Else
                    TxtName.BackColor = Drawing.Color.White
                End If
                TxtCategory.Text = Trim(Rdr("Category")).ToString
                TxtClientID.Text = Rdr("ClientID").ToString
                TxtPClientCode.Text = Rdr("ClientID").ToString
                TxtTClientID.Text = Rdr("ClientID").ToString
                TxtLeaseClient.Text = Rdr("ClientID").ToString
                TxtPClientName.Text = Rdr("Name").ToString
                TxtName.Text = Rdr("Name").ToString
                TxtTClientName.Text = Rdr("Name").ToString
                TxtLeaseName.Text = Rdr("Name").ToString
                TxtAddress.Text = Rdr("Address").ToString
                TxtCity.Text = Rdr("City").ToString
                TxtTel.Text = Rdr("Telephone").ToString
                TxtFaxNo.Text = Rdr("Fax").ToString
                TxtEmail.Text = Rdr("EMail").ToString
                TxtContact.Text = Rdr("Contact").ToString
                TxtPosition.Text = Rdr("Position").ToString
                TxtCellNo.Text = Rdr("CellNo").ToString
                TxtDate.Text = Format(CDate(Rdr("DateCreated").ToString), "dd/MM/yyyy")
                TxtRemarks.Text = Rdr("Remarks").ToString
                TxtVatNo.Text = Rdr("VATNo").ToString
                TxtLicenceNo.Text = Rdr("LicenceNo").ToString
                TxtStoragePermit.Text = Rdr("StoragePermit").ToString
                TxtLicExp.Text = Format(CDate(Rdr("LicenceExp").ToString), "dd/MM/yyyy")
                TxtPermitExp.Text = Format(CDate(Rdr("PermitExp").ToString), "dd/MM/yyyy")
                TxtStatus.Text = Rdr("Status").ToString
                TxtProType.Text = Rdr("ProdType").ToString
                TxtActivities.Text = Rdr("Activities").ToString
                TxtBRN.Text = Rdr("ClientBRN").ToString
                TxtWeb.Text = Rdr("WebURL").ToString
                CmdArea.Connection = Con
                LngArea1 = 0
                LngArea2 = 0
                CmdArea.CommandText = "Select Sum(Area) From LeaseAgreement Where ClientID = '" & TxtPtyClientID.Text & "' And MovedIN = 1"
                LngArea1 = CmdArea.ExecuteScalar
                CmdArea.CommandText = "Select Sum(Area) From LeaseAgreement Where ClientID = '" & TxtPtyClientID.Text & "' And MovedOUT = 1"
                LngArea2 = CmdArea.ExecuteScalar
                TxtAreaLeased.Text = LngArea1 - LngArea2
                TxtAddress2.Text = Rdr("Address2").ToString
                Select Case IIf(IsDBNull(Rdr("FPZones")), 0, Rdr("FPZones"))
                    Case 1
                        ChkZone1.Checked = True
                        ChkZone6.Checked = False
                        ChkZone9.Checked = False
                    Case 6
                        ChkZone1.Checked = False
                        ChkZone6.Checked = True
                        ChkZone9.Checked = False
                    Case 9
                        ChkZone1.Checked = False
                        ChkZone6.Checked = False
                        ChkZone9.Checked = True
                    Case 7
                        ChkZone1.Checked = True
                        ChkZone6.Checked = True
                        ChkZone9.Checked = False
                    Case 10
                        ChkZone1.Checked = True
                        ChkZone6.Checked = False
                        ChkZone9.Checked = True
                    Case 15
                        ChkZone1.Checked = False
                        ChkZone6.Checked = True
                        ChkZone9.Checked = True
                    Case 16
                        ChkZone1.Checked = True
                        ChkZone6.Checked = True
                        ChkZone9.Checked = True
                    Case Else
                        ChkZone1.Checked = False
                        ChkZone6.Checked = False
                        ChkZone9.Checked = False
                End Select
                GetTariffCard()
            End If
            Rdr.Close()
            'UpdateCharges()
            'Update Lease Agreements
            StrSql = "SELECT LeaseNo FROM LeaseAgreement WHERE ClientID = '" & TxtClientID.Text & "'"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbLeaseNo.Items.Clear()
            CmbLeaseNo.Items.Add("Select...")
            While Rdr.Read
                CmbLeaseNo.Items.Add(Rdr("LeaseNo"))
            End While
            Rdr.Close()
            CmbLeaseNo.Text = GdvProperty.SelectedRow.Cells(10).Text.Replace("&nbsp;", "")
            CmbLeaseNo_SelectedIndexChanged(Me, e)
        End If
        'If there is a client selected then check if there is a proposal for this property
        StrSql = "SELECT ProposalNo,Revision FROM Proposal WHERE ClientId = '" & TxtClientID.Text & "' "
        StrSql = StrSql & "AND PropCode = '" & TxtPropertyCode.Text & "' "
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            'TxtProposalNo.Text = Rdr("ProposalNo").ToString
            'TxtRevision.Text = Rdr("Revision").ToString
            Rdr.Close()
        Else
            Rdr.Close()
            CmbProposalFPZone.Text = CmbPtyFPZone.Text
            TxtWarehouse.Text = CmbPtyFPZone.Text
            'TxtWarehouse_TextChanged(Me, e)
            TxtZoneCode.Text = TxtPropertyCode.Text
            TxtZoneCode_TextChanged(Me, e)
        End If
    End Sub

    Private Sub GdvProperty_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvProperty.SelectedIndexChanging
        GdvProperty.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub GdvCar_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvCar.SelectedIndexChanging
        GdvCar.SelectedIndex = e.NewSelectedIndex
    End Sub


    Protected Sub ButSaveProperty_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSaveProperty.Click
        'Page.Validate("PtyGroup")
        'If Not Page.IsValid Then
        '    Dim StrScript As String = "<script language='javascript'> w.Alert('Please enter fields marked *','Incomplete Data','images/Utility.ico');</script>"
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        '    Exit Sub
        'End If
        LblPropError.Visible = False
        If TxtPropertyCode.Text = "" Then
            LblPropError.Text = "Please Enter Client Code"
            LblPropError.Visible = True
            Exit Sub
        End If
        If CmbPtyFPZone.Text = "Select..." Then
            LblPropError.Text = "Please Enter Zone"
            LblPropError.Visible = True
            Exit Sub
        End If
        If CmbPropType.Text = "Select..." Then
            LblPropError.Text = "Please Select Property Type"
            LblPropError.Visible = True
            Exit Sub
        End If
        If CmbPropType.Text <> "Equipment" Then
            If TxtPtyArea.Text = "" Then
                LblPropError.Text = "Please Enter Area"
                LblPropError.Visible = True
                Exit Sub
            End If
            If Val(TxtPtyArea.Text) = 0 Then
                LblPropError.Text = "Please Enter Area"
                LblPropError.Visible = True
                Exit Sub
            End If
            If CmbPtyUnit.Text = "Select..." Then
                LblPropError.Text = "Please Select Unit Area"
                LblPropError.Visible = True
                Exit Sub
            End If
        End If
        Cmd.Connection = Con
        StrSql = "SELECT COUNT(*) FROM Property WHERE PropertyCode = '" & TxtPropertyCode.Text & "' "
        StrSql = StrSql & "AND FpZone = '" & CmbPtyFPZone.Text & "'"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar > 0 Then
            StrSql = "UPDATE Property SET Description = '" & TxtPtyDesc.Text & "',"
            StrSql = StrSql & "Type = '" & CmbPropType.Text & "',"
            StrSql = StrSql & "Area = " & Val(TxtPtyArea.Text) & ","
            StrSql = StrSql & "Unit = '" & CmbPtyUnit.Text & "',"
            StrSql = StrSql & "Remarks = '" & TxtPtyRemarks.Text & "' WHERE "
            StrSql = StrSql & "PropertyCode = '" & TxtPropertyCode.Text & "' AND FpZone = '" & CmbPtyFPZone.Text & "'"
        Else
            StrSql = "INSERT INTO Property (PropertyCode,FpZone,Description,Type,Area,Unit,Remarks) VALUES('"
            StrSql = StrSql & TxtPropertyCode.Text & "','"
            StrSql = StrSql & CmbPtyFPZone.Text & "','"
            StrSql = StrSql & TxtPtyDesc.Text & "','"
            StrSql = StrSql & CmbPropType.Text & "',"
            StrSql = StrSql & Val(TxtPtyArea.Text) & ",'"
            StrSql = StrSql & CmbPtyUnit.Text & "','"
            StrSql = StrSql & TxtPtyRemarks.Text & "')"

        End If
        Try
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            TxtPropertyCode.Text = ""
            CmbPtyFPZone.SelectedIndex = -1
            TxtPtyDesc.Text = ""
            CmbPropType.SelectedIndex = -1
            TxtPtyArea.Text = ""
            CmbPtyUnit.SelectedIndex = -1
            TxtPtyRemarks.Text = ""
            UpdateProperty()
        Catch ex As Exception
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Saving','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try
    End Sub

    Protected Sub ButDeleteProperty_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDeleteProperty.Click
        Cmd.Connection = Con
        Cmd.CommandText = "DELETE FROM Property WHERE PropertyCode = '" & TxtPropertyCode.Text & "' AND FpZone = '" & CmbPtyFPZone.Text & "'"
        Try
            Cmd.ExecuteNonQuery()
            TxtPropertyCode.Text = ""
            CmbPtyFPZone.SelectedIndex = -1
            TxtPtyDesc.Text = ""
            CmbPropType.SelectedIndex = -1
            TxtPtyArea.Text = ""
            CmbPtyUnit.SelectedIndex = -1
            TxtPtyRemarks.Text = ""
            UpdateProperty()
        Catch ex As Exception
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Deleting','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try
    End Sub

    Protected Sub ButNewCharge_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNewCharge.Click
        TxtChargeRate.Text = ""
        TxtFromTime.Text = ""
        TxtToTime.Text = ""
        TxtChDescription.Text = ""
        TxtChargeUnit.Text = ""
        TxtMinCharge.Text = ""
        TxtMinUnit.Text = ""
        CmbCondition.SelectedIndex = 0
        CmbDaysApp.SelectedIndex = 0
        TxtPerChDays.Text = ""
        TxtChCode.Text = GetNewChargeCode()
    End Sub

    Private Function GetNewChargeCode() As String
        Dim StrChCode As String = ""
        Dim StrType As String = ""
        If CmbChargeFPZone.Text = "Select..." Then
            StrChCode = "Select Zone!"
        Else
            StrChCode = CmbChargeFPZone.Text
            If CmbChType.Text = "Standard" Then
                StrChCode = StrChCode & "STD"
                StrType = "STD"
            ElseIf CmbChType.Text = "Preferential" Then
                StrChCode = StrChCode & "PRE"
                StrType = "PRE"
            Else
                StrChCode = "Select Type!"
            End If
        End If
        If StrChCode <> "Select Zone!" AndAlso StrChCode <> "Select Type!" Then
            StrSql = "SELECT MAX(ChCode) FROM TariffCard WHERE FPZone = '" & CmbChargeFPZone.Text & "' "
            StrSql = StrSql & "AND Type = '" & CmbChType.Text & "'"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            StrChCode = Cmd.ExecuteScalar.ToString
            If StrChCode = "" Then
                StrChCode = CmbChargeFPZone.Text & StrType & "001"
            Else
                StrChCode = Format(Val(Microsoft.VisualBasic.Right(StrChCode, 3) + 1), "000")
                StrChCode = CmbChargeFPZone.Text & StrType & StrChCode
            End If
        End If
        Return StrChCode
    End Function

    Protected Sub ButSaveCharge_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSaveCharge.Click
        Dim Trans As OleDb.OleDbTransaction
        Cmd.Connection = Con
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Transaction = Trans
        Cmd.CommandText = "DELETE FROM TariffCard WHERE ChCode = '" & TxtChCode.Text & "'"
        Try
            Cmd.ExecuteNonQuery()
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "INSERT INTO TariffCard (ChCode,Type,FPZone,Description,ChargeType,"
            StrSql = StrSql & "Days,FromTime,ToTime,Rate,Unit,PerDays,MinCharge,MinUnit,StorageContainer) VALUES('"
            StrSql = StrSql & TxtChCode.Text & "','"
            StrSql = StrSql & CmbChType.Text & "','"
            StrSql = StrSql & CmbChargeFPZone.Text & "','"
            StrSql = StrSql & TxtChDescription.Text & "','"
            StrSql = StrSql & CmbCondition.Text & "','"
            StrSql = StrSql & CmbDaysApp.Text & "','"
            StrSql = StrSql & TxtFromTime.Text & "','"
            StrSql = StrSql & TxtToTime.Text & "',"
            StrSql = StrSql & Val(TxtChargeRate.Text) & ",'"
            StrSql = StrSql & TxtChargeUnit.Text & "',"
            StrSql = StrSql & Val(TxtPerChDays.Text) & ","
            StrSql = StrSql & Val(TxtMinCharge.Text) & ","
            StrSql = StrSql & Val(TxtMinUnit.Text) & ","
            StrSql = StrSql & IIf(ChkContainerStorage.Checked, 1, 0) & ")"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            TxtChCode.Text = ""
            CmbChType.SelectedIndex = -1
            CmbChargeFPZone.SelectedIndex = -1
            TxtChDescription.Text = ""
            CmbCondition.SelectedIndex = -1
            CmbDaysApp.SelectedIndex = -1
            TxtFromTime.Text = ""
            TxtToTime.Text = ""
            TxtChargeRate.Text = ""
            TxtChargeUnit.Text = ""
            TxtPerChDays.Text = ""
            TxtMinCharge.Text = ""
            TxtMinUnit.Text = ""
            ChkContainerStorage.Checked = False
            GetTariffCard()
        Catch ex As Exception
            Trans.Rollback()
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Saving','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try
    End Sub

    Private Sub GetTariffCard()
        'LblRenewal.Visible = False
        Dim CmdClientTariff As New OleDb.OleDbCommand
        Dim CmdGetTariff As New OleDb.OleDbCommand
        Dim RdrGetTariff As OleDb.OleDbDataReader
        CmdGetTariff.Connection = Con
        StrSql = "SELECT ChCode 'Ch Code',Type, FPZone 'FP Zone',Description,ChargeType 'Charge Condition',"
        StrSql = StrSql & "Days 'Days Applicable',FromTime 'From', ToTime 'To',Rate,Unit,"
        StrSql = StrSql & "MinCharge 'Min Chargeable',MinUnit 'Min Unit',PerDays 'Unit Days',StorageContainer 'Storage Container'"
        StrSql = StrSql & "FROM TariffCard WHERE FPZone = '" & CmbChargeFPZone.Text & "' "
        StrSql = StrSql & "AND Type = '" & CmbChType.Text & "'"
        CmdGetTariff.CommandText = StrSql
        RdrGetTariff = CmdGetTariff.ExecuteReader
        GdvTariffCard.DataSource = RdrGetTariff
        GdvTariffCard.DataBind()
        RdrGetTariff.Close()

        'check client tariff 
        For Each row As GridViewRow In GdvTariffCard.Rows
            If TxtTClientID.Text <> "" And CmbChType.Text <> "Select..." Then
                CmdClientTariff.Connection = Con
                CmdClientTariff.CommandText = "Select Count(*)From ClientTariff Where ClientID = '" & TxtTClientID.Text & "' And ChCode = '" & row.Cells(1).Text & "'"
                If CmdClientTariff.ExecuteScalar > 0 Then
                    DirectCast(row.FindControl("ChkSelectTariff"), CheckBox).Checked = True
                Else
                    DirectCast(row.FindControl("ChkSelectTariff"), CheckBox).Checked = False
                End If
            End If
        Next

    End Sub

    Private Sub GdvTariffCard_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvTariffCard.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='pointer';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvTariffCard, "Select$" & e.Row.RowIndex)
            For I As Integer = 0 To e.Row.Cells.Count - 1
                If IsNumeric(e.Row.Cells(I).Text) Then
                    e.Row.Cells(I).Attributes.Add("align", "right")
                Else
                    e.Row.Cells(I).Attributes.Add("align", "left")
                End If
            Next
        End If
    End Sub

    Private Sub CmbChType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbChType.SelectedIndexChanged
        GetTariffCard()
    End Sub

    Private Sub CmbCondition_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbCondition.SelectedIndexChanged
        If CmbCondition.Text = "By Unit" Then
            TxtFromTime.Text = ""
            TxtFromTime.ReadOnly = True
            TxtToTime.Text = ""
            TxtToTime.ReadOnly = True
            CmbDaysApp.Enabled = False
            TxtMinCharge.Text = ""
            TxtMinCharge.ReadOnly = True
            TxtMinUnit.ReadOnly = False
        ElseIf CmbCondition.Text = "By Period" Then
            TxtFromTime.Text = ""
            TxtFromTime.ReadOnly = False
            TxtToTime.Text = ""
            TxtToTime.ReadOnly = False
            CmbDaysApp.Enabled = True
            TxtMinCharge.ReadOnly = False
            TxtMinUnit.Text = ""
            TxtMinUnit.ReadOnly = True
        End If
    End Sub

    Private Sub GdvTariffCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvTariffCard.SelectedIndexChanged
        TxtChCode.Text = GdvTariffCard.SelectedRow.Cells(1).Text.Replace("&nbsp;", "")
        CmbChType.Text = GdvTariffCard.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
        CmbChargeFPZone.Text = GdvTariffCard.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
        TxtChDescription.Text = GdvTariffCard.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
        CmbCondition.Text = GdvTariffCard.SelectedRow.Cells(5).Text.Replace("&nbsp;", "")
        CmbDaysApp.Text = GdvTariffCard.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
        TxtFromTime.Text = GdvTariffCard.SelectedRow.Cells(7).Text.Replace("&nbsp;", "")
        TxtToTime.Text = GdvTariffCard.SelectedRow.Cells(8).Text.Replace("&nbsp;", "")
        TxtChargeRate.Text = GdvTariffCard.SelectedRow.Cells(9).Text.Replace("&nbsp;", "")
        TxtChargeUnit.Text = GdvTariffCard.SelectedRow.Cells(10).Text.Replace("&nbsp;", "")
        TxtMinCharge.Text = GdvTariffCard.SelectedRow.Cells(11).Text.Replace("&nbsp;", "")
        TxtMinUnit.Text = GdvTariffCard.SelectedRow.Cells(12).Text.Replace("&nbsp;", "")
        TxtPerChDays.Text = GdvTariffCard.SelectedRow.Cells(13).Text.Replace("&nbsp;", "")
        ChkContainerStorage.Checked = DirectCast(GdvTariffCard.SelectedRow.Cells(14).Controls(0), CheckBox).Checked
    End Sub

    Private Sub GdvTariffCard_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvTariffCard.SelectedIndexChanging
        GdvTariffCard.SelectedIndex = e.NewSelectedIndex
    End Sub

    'Protected Sub TxtWarehouse_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtWarehouse.TextChanged
    '    CmbZoneCode.Items.Clear()
    '    StrSql = "SELECT PropertyCode FROM Property WHERE FpZone = '" & TxtWarehouse.Text & "'"
    '    Cmd.Connection = Con
    '    Cmd.CommandText = StrSql
    '    Rdr = Cmd.ExecuteReader
    '    CmbZoneCode.Items.Clear()
    '    CmbZoneCode.Items.Add("Select...")
    '    While Rdr.Read
    '        CmbZoneCode.Items.Add(Rdr("PropertyCode"))
    '    End While
    '    Rdr.Close()
    '    CmbZoneCode.SelectedIndex = 0
    'End Sub

    Protected Sub CmbCopyFPZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbCopyFPZone.SelectedIndexChanged
        If CmbCopyFPZone.Text <> "Select..." Then
            ButCopyFrom_ConfirmButtonExtender.ConfirmText = "Sure to Copy Tariff Card from Zone " & CmbCopyFPZone.Text & " To " & CmbChargeFPZone.Text & "?"
            ButCopyTo_ConfirmButtonExtender.ConfirmText = "Sure to Copy Tariff Card from Zone " & CmbChargeFPZone.Text & " To " & CmbCopyFPZone.Text & "?"
        Else
            ButCopyFrom_ConfirmButtonExtender.ConfirmText = "Select the Zone to Copy From"
            ButCopyTo_ConfirmButtonExtender.ConfirmText = "Select the Zone to Copy To"
        End If
    End Sub

    Protected Sub ButCopyFrom_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCopyFrom.Click
        If CmbCopyFPZone.Text = "Select..." Then
            Dim StrScript As String = "<script language='javascript'> w.Alert('Select FP Zone to Copy From / To','Data Needed','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
            Exit Sub
        End If
        If CmbCopyFPZone.Text = CmbChargeFPZone.Text Then
            Dim StrScript As String = "<script language='javascript'> w.Alert('Cannot Copy to Same Zone','Error','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
            Exit Sub
        End If
        StrSql = "DELETE FROM TariffCard WHERE FpZone = '" & CmbChargeFPZone.Text & "' AND Type = '" & CmbChType.Text & "'"
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Cmd.CommandText = StrSql
        Try
            Cmd.ExecuteNonQuery()
            StrSql = "INSERT INTO TariffCard SELECT '" & CmbChargeFPZone.Text
            If CmbChType.Text = "Standard" Then
                StrSql = StrSql & "STD"
            Else
                StrSql = StrSql & "PRE"
            End If
            StrSql = StrSql & "'+SUBSTRING(ChCode,9,3)as ChCode,'" & CmbChType.Text & "','" & CmbChargeFPZone.Text & "',"
            StrSql = StrSql & "Description,ChargeType,Days,FromTime,ToTime,Rate,Unit,MinCharge,MinUnit,"
            StrSql = StrSql & "PerDays,StorageContainer from TariffCard WHERE FpZone = '" & CmbCopyFPZone.Text & "' AND Type = '" & CmbChType.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            GetTariffCard()
        Catch ex As Exception
            Trans.Rollback()
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Copying','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try
    End Sub

    Protected Sub ButCopyTo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCopyTo.Click
        If CmbCopyFPZone.Text = "Select..." Then
            Dim StrScript As String = "<script language='javascript'> w.Alert('Select FP Zone to Copy From / To','Data Needed','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
            Exit Sub
        End If
        If CmbCopyFPZone.Text = CmbChargeFPZone.Text Then
            Dim StrScript As String = "<script language='javascript'> w.Alert('Cannot Copy to Same Zone','Error','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
            Exit Sub
        End If
        StrSql = "DELETE FROM TariffCard WHERE FpZone = '" & CmbCopyFPZone.Text & "' AND Type = '" & CmbChType.Text & "'"
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Cmd.CommandText = StrSql
        Try
            Cmd.ExecuteNonQuery()
            StrSql = "INSERT INTO TariffCard SELECT '" & CmbCopyFPZone.Text
            If CmbChType.Text = "Standard" Then
                StrSql = StrSql & "STD"
            Else
                StrSql = StrSql & "PRE"
            End If
            StrSql = StrSql & "'+SUBSTRING(ChCode,9,3)as ChCode,'" & CmbChType.Text & "','" & CmbCopyFPZone.Text & "',"
            StrSql = StrSql & "Description,ChargeType,Days,FromTime,ToTime,Rate,Unit,MinCharge,MinUnit,"
            StrSql = StrSql & "PerDays, StorageContainer from TariffCard WHERE FpZone = '" & CmbChargeFPZone.Text & "' AND Type = '" & CmbChType.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            GetTariffCard()
        Catch ex As Exception
            Trans.Rollback()
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Copying','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try
    End Sub

    'Private Sub CmbProposalFPZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbProposalFPZone.SelectedIndexChanged
    '    If CmbProposalFPZone.Text = "Select..." Then
    '        TxtWarehouse.Text = ""
    '    Else
    '        TxtWarehouse.Text = CmbProposalFPZone.Text
    '        TxtWarehouse_TextChanged(Me, e)
    '    End If
    'End Sub

    'Private Sub ButAddtoProp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButAddtoProp.Click
    '    Dim dt As DataTable
    '    Dim dr As DataRow
    '    Dim dc As DataColumn
    '    Dim ColContNo As DataColumn
    '    dt = New DataTable()
    '    dc = New DataColumn("Code", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    dc = New DataColumn("Description", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    dc = New DataColumn("Type", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    dc = New DataColumn("Rate", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    dc = New DataColumn("EquipmentUnit", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    dc = New DataColumn("Equipment", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    On Error Resume Next
    '    For Each row As GridViewRow In GdvServices.Rows
    '        If row.RowType = DataControlRowType.DataRow Then
    '            dr = dt.NewRow
    '            dr.Item("Code") = row.Cells(1).Text
    '            dr.Item("Description") = row.Cells(2).Text
    '            dr.Item("Type") = row.Cells(3).Text
    '            dr.Item("Rate") = row.Cells(4).Text
    '            dr.Item("Unit") = row.Cells(5).Text
    '            dr.Item("Minimum") = row.Cells(6).Text
    '            dt.Rows.Add(dr)
    '        End If
    '    Next
    '    For Each row As GridViewRow In GdvTariffCard.Rows
    '        If row.RowType = DataControlRowType.DataRow Then
    '            Dim ChkBox As CheckBox = DirectCast(row.FindControl("ChkSelectTariff"), CheckBox)
    '            If ChkBox.Checked Then
    '                dr = dt.NewRow()
    '                dr("Code") = row.Cells(1).Text
    '                dr("Description") = row.Cells(4).Text
    '                dr("Type") = row.Cells(2).Text
    '                dr.Item("Rate") = row.Cells(9).Text
    '                dr.Item("Unit") = row.Cells(10).Text
    '                dr.Item("Minimum") = row.Cells(11).Text
    '                dt.Rows.Add(dr)
    '                ChkBox.Checked = False
    '            End If
    '        End If
    '    Next
    '    GdvServices.DataSource = dt
    '    GdvServices.DataBind()
    'End Sub

    'Private Sub ButRemoveService_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRemoveService.Click
    '    Dim dt As DataTable
    '    Dim dr As DataRow
    '    Dim dc As DataColumn
    '    Dim ColContNo As DataColumn
    '    dt = New DataTable()
    '    dc = New DataColumn("Code", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    dc = New DataColumn("Description", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    dc = New DataColumn("Type", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    dc = New DataColumn("Rate", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    dc = New DataColumn("Unit", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    dc = New DataColumn("Minimum", Type.GetType("System.String"))
    '    dt.Columns.Add(dc)
    '    On Error Resume Next
    '    For Each row As GridViewRow In GdvServices.Rows
    '        If row.RowType = DataControlRowType.DataRow Then
    '            If Not DirectCast(row.FindControl("ChkSelect"), CheckBox).Checked Then
    '                dr = dt.NewRow
    '                dr.Item("Code") = row.Cells(1).Text
    '                dr.Item("Description") = row.Cells(2).Text
    '                dr.Item("Type") = row.Cells(3).Text
    '                dr.Item("Rate") = row.Cells(4).Text
    '                dr.Item("Unit") = row.Cells(5).Text
    '                dr.Item("Minimum") = row.Cells(6).Text
    '                dt.Rows.Add(dr)
    '            End If
    '        End If
    '    Next
    '    GdvServices.DataSource = dt
    '    GdvServices.DataBind()
    'End Sub

    'Private Sub GdvServices_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvServices.RowDataBound
    '    On Error Resume Next
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        e.Row.Attributes("onmouseover") = "this.style.cursor='pointer';"
    '        For I As Integer = 0 To e.Row.Cells.Count - 1
    '            If IsNumeric(e.Row.Cells(I).Text) Then
    '                e.Row.Cells(I).Attributes.Add("align", "right")
    '            Else
    '                e.Row.Cells(I).Attributes.Add("align", "left")
    '            End If
    '        Next
    '    End If
    'End Sub

    Protected Sub ButLease_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButLease.Click
        Dim CmdValidateProposal As New OleDb.OleDbCommand
        CmdValidateProposal.Connection = Con
        CmdValidateProposal.CommandText = "Select Count(*)From Proposal Where ProposalNo = '" & TxtProposalNo.Text & "' And [Status]='Accepted'"
        If CmdValidateProposal.ExecuteScalar = 0 Then
            Dim StrScript As String = "<script language='javascript'> w.Alert('Please update proposal as accepted before creating lease','Error Creating Lease','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
            Exit Sub
        End If
        'Check this Proposal has been converted to Lease
        StrSql = "SELECT COUNT(*) FROM LeaseAgreement WHERE ProposalNo = '" & TxtProposalNo.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar > 0 Then
            Dim StrScript As String = "<script language='javascript'> w.Alert('This proposal Already Converted to Lease!','Error Creating Lease','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
            Exit Sub
        End If
        'Get New Lease No
        Dim StrLeaseNo As String = GetNewLeaseNo()
        'Create Record in Lease Agreement
        'Dim Trans As OleDb.OleDbTransaction
        Cmd.Connection = Con
        'Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        'Cmd.Transaction = Trans
        Try
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "INSERT INTO LeaseAgreement(LeaseNo, LeaseDate, ProposalNo, Revision, ClientID, PropDate,PreparedBy,"
            StrSql = StrSql & "Validity, ProjectDesc, Zone, PropCode, Area, Unit, Rate, Amount, CarQty, RateUpto15,RateAfter15,ConCharges,"
            StrSql = StrSql & "[Status],Deposit, Insurance,MonthlyRent,LeaseReceived,FittedPeriod,LNO)"
            StrSql = StrSql & "(SELECT '" & StrLeaseNo & "','" & Date.Today.ToString("MM/dd/yyyy") & "',ProposalNo, Revision,ClientId, PropDate, PreparedBy, Validity,"
            StrSql = StrSql & "ProjectDesc,Zone,PropCode, Area, Unit,Rate, Amount, CarQty, RateUpto15,RateAfter15,ConCharges,[Status],Deposit, Insurance, MonthlyRent,"
            If TxtLeaseReceived.Text <> "" Then
                StrSql = StrSql & "'" & Format(CDate(TxtLeaseReceived.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "NULL,"
            End If

            StrSql = StrSql & "'" & Val(TxtFittedPeriod.Text) & "',"
            StrSql = StrSql & "'" & StrLeaseNo & "'"
            StrSql = StrSql & "FROM Proposal WHERE ProposalNo = '" & TxtProposalNo.Text & "' And Revision = '" & TxtRevision.Text & "')"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            'Change Validity and Status
            StrSql = "UPDATE LeaseAgreement SET Status = 'Active',LeaseDate =  '" & Date.Today.ToString("MM/dd/yyyy") & "' "
            StrSql = StrSql & "WHERE LeaseNo = '" & StrLeaseNo & "' And ClientID = '" & TxtLeaseClient.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()

            'Change prepared by to null as user will input who preared lease 
            Cmd.CommandText = "Update LeaseAgreement Set PreparedBy =  NULL Where LeaseNo =  '" & StrLeaseNo & "' And ClientID  = '" & TxtLeaseClient.Text & "'"
            Cmd.ExecuteNonQuery()

            ''Create the Additional Services
            'StrSql = "INSERT INTO LeaseServices SELECT '" & StrLeaseNo & "',ChCode FROM PropServices WHERE ProposalNo ='" & TxtProposalNo.Text & "' "
            'StrSql = StrSql & "AND Revision = " & Val(TxtRevision.Text)
            'Cmd.CommandText = StrSql
            'Cmd.ExecuteNonQuery()
            'update next lease NO
            StrSql = "Update SysParam Set NextLeaseNo = NextLeaseNo + 1"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()

            'Trans.Commit()

            CmbLeaseNo.Items.Add(StrLeaseNo)
            CmbLeaseNo.Text = StrLeaseNo
            'CmbLeaseNo.SelectedIndex = CmbLeaseNo.Items.Count - 1
            CmbLeaseNo_SelectedIndexChanged(Me, e)

            Dim StrScript As String = "<script language='javascript'> w.Alert('Lease " & StrLeaseNo & " Created!','Created','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        Catch ex As Exception
            'Trans.Rollback()
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Creating Lease','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try
    End Sub

    Private Function GetNewLeaseNo() As String
        'Get New Lease Ref
        Dim StrRef As String = ""
        Cmd.Connection = Con
        StrSql = "SELECT NextLeaseNo FROM SysParam"
        Cmd.CommandText = StrSql
        StrRef = Cmd.ExecuteScalar.ToString
        StrRef = Session("UserID") & "/" & StrRef & "/" & Date.Today.ToString("yy")
        Return StrRef
    End Function

    Private Sub CmbLeaseNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbLeaseNo.SelectedIndexChanged
        Dim CmdGetLease As New OleDb.OleDbCommand
        Dim RdrGetLease As OleDb.OleDbDataReader

        CmdGetLease.Connection = Con
        StrSql = "SELECT LeaseAgreement.*,Property.Description,Property.FpZone,Property.Type FROM LeaseAgreement "
        StrSql = StrSql & "INNER JOIN Property ON Property.PropertyCode=LeaseAgreement.PropCode "
        StrSql = StrSql & "WHERE LeaseAgreement.LeaseNo = '" & CmbLeaseNo.Text & "'"
        CmdGetLease.CommandText = StrSql

        RdrGetLease = CmdGetLease.ExecuteReader
        Dim StrZoneCode As String = ""
        If RdrGetLease.Read Then
            TxtLeasePropNo.Text = RdrGetLease("ProposalNo").ToString
            If Not IsDBNull(RdrGetLease("PropDate")) Then
                TxtProposalDate.Text = Format(CDate(RdrGetLease("PropDate")), "dd/MM/yyyy")
            Else
                TxtProposalDate.Text = ""
            End If
            TxtPropRevision.Text = RdrGetLease("Revision").ToString
            TxtLeaseBy.Text = RdrGetLease("PreparedBy").ToString
            If Not IsDBNull(RdrGetLease("Validity")) Then
                TxtLeaseValidity.Text = Format(RdrGetLease("Validity"), "dd/MM/yyyy")
            Else
                TxtLeaseValidity.Text = ""
            End If

            StrZoneCode = RdrGetLease("PropCode").ToString
            TxtLeasePropType.Text = RdrGetLease("Type").ToString
            CmbLeaseZone.Text = RdrGetLease("FpZone").ToString
            TxtLeaseFpZone.Text = RdrGetLease("FpZone").ToString
            TxtLeaseArea.Text = RdrGetLease("Area").ToString
            LblLeaseUnit.Text = RdrGetLease("Unit").ToString
            TxtLeaseRate.Text = RdrGetLease("Rate").ToString
            TxtLeaseAmount.Text = RdrGetLease("Amount").ToString
            TxtLeaseSCharge.Text = RdrGetLease("ConCharges").ToString
            TxtLeaseDeposit.Text = RdrGetLease("Deposit").ToString
            TxtDepositAmt.Text = RdrGetLease("Deposit").ToString
            TxtLeaseInsurance.Text = RdrGetLease("Insurance").ToString
            TxtLeasePropDesc.Text = RdrGetLease("Description").ToString
            TxtFittedPeriod.Text = RdrGetLease("FittedPeriod").ToString
            TxtMonthlyRent.Text = RdrGetLease("MonthlyRent").ToString
            TxtLeaseMonthlyCharge.Text = RdrGetLease("MonthlyRent").ToString
            TxtLeaseAmount_TextChanged(Me, e)
            TxtLeaseSCharge_TextChanged(Me, e)
            TxtMoveInInsurance.Text = RdrGetLease("Insurance").ToString
            TxtMoveINServiceCharge.Text = RdrGetLease("ConCharges").ToString
            If Not IsDBNull(RdrGetLease("LeaseDate")) Then
                TxtLeaseDate.Text = Format(RdrGetLease("LeaseDate"), "dd/MM/yyyy")
            Else
                TxtLeaseDate.Text = ""
            End If

            If Not IsDBNull(RdrGetLease("LeaseReceived")) Then
                TxtLeaseReceived.Text = Format(RdrGetLease("LeaseReceived"), "dd/MM/yyyy")
            Else
                TxtLeaseReceived.Text = ""
            End If

            If Not IsDBNull(RdrGetLease("BillingStart")) Then
                TxtBillingFrom.Text = Format(RdrGetLease("BillingStart"), "dd/MM/yyyy")
            Else
                TxtBillingFrom.Text = ""
            End If
            If Not IsDBNull(RdrGetLease("BillingTo")) Then
                TxtBillingTo.Text = Format(RdrGetLease("BillingTo"), "dd/MM/yyyy")
            Else
                TxtBillingTo.Text = ""
            End If
            'update status
            OptActive.Checked = False
            OptExpired.Checked = False
            OptRenewed.Checked = False
            Select Case RdrGetLease("Status")
                Case OptActive.Text
                    OptActive.Checked = True
                Case OptExpired.Text
                    OptExpired.Checked = True
                Case OptRenewed.Text
                    OptRenewed.Checked = True
            End Select
            If RdrGetLease("MovedOut") Then
                OptMovedOut.Checked = True
            End If
            'Update Move In Data
            TxtMoveIn.Text = ""
            TxtMoveInPropCode.Text = StrZoneCode
            TxtMoveInArea.Text = TxtLeaseArea.Text & " " & LblLeaseUnit.Text
            TxtLeaseOn.Text = ""
            TxtLeaseExp.Text = ""
            TxtDepositAmt.Text = TxtLeaseDeposit.Text
            TxtMoveInRate.Text = TxtRate.Text
            TxtPropMonthlyRent.Text = TxtLeaseAmount.Text
            If Not IsDBNull(RdrGetLease("MovedIn")) AndAlso RdrGetLease("MovedIn") Then
                TxtMoveIn.Text = RdrGetLease("MoveInDate")
                TxtDepositPaidOn.Text = RdrGetLease("DepositPaidOn").ToString
                TxtGuaranteeNo.Text = RdrGetLease("GuaranteeNo").ToString
                TxtDepositAmt.Text = RdrGetLease("Deposit").ToString
                TxtGuaranteeExp.Text = RdrGetLease("GuaranteeExp").ToString
                TxtFpActivity.Text = RdrGetLease("FpActivities").ToString
                TxtAdRemarks.Text = RdrGetLease("AddRemarks").ToString
                TxtOpHours.Text = RdrGetLease("OpHours").ToString
                TxtElecCharges.Text = RdrGetLease("Electricity").ToString
                TxtWaterCharge.Text = RdrGetLease("Water").ToString
                TxtTelCharges.Text = RdrGetLease("Telephone").ToString
                TxtWasteWater.Text = RdrGetLease("WasteWater").ToString
                If Not IsDBNull(RdrGetLease("DateOfEntry")) Then
                    TxtDateofEntry.Text = Format(RdrGetLease("DateOfEntry"), "dd/MM/yyyy")
                Else
                    TxtDateofEntry.Text = ""
                End If

            End If
            'Update Moveout Details
            TxtMoveOutAmountRemarks.Text = RdrGetLease("AmountRemarks").ToString
            TxtMoveOutAmtDue.Text = RdrGetLease("AmountToPay").ToString
            TxtMoveOutArea.Text = RdrGetLease("Area").ToString
            TxtMoveOutDate.Text = RdrGetLease("MoveOutDate").ToString
            If IsDate(TxtMoveOutDate.Text) Then
                TxtMoveOutDate.Text = CDate(TxtMoveOutDate.Text).ToString("dd/MM/yyyy")
            End If
            TxtMoveOutDeposit.Text = RdrGetLease("Deposit").ToString
            TxtMoveOutDesc.Text = RdrGetLease("Description").ToString
            TxtMoveOutGoodsRemarks.Text = RdrGetLease("GoodsRemarks").ToString
            TxtMoveOutKeyRemarks.Text = RdrGetLease("KeyRemarks").ToString
            TxtMoveOutLeaseNo.Text = RdrGetLease("LeaseNo").ToString
            TxtMoveOutPropCode.Text = RdrGetLease("PropCode").ToString
            TxtMoveOutRate.Text = RdrGetLease("Rate").ToString
            TxtMoveOutReason.Text = RdrGetLease("Reason").ToString
            TxtMoveOutRefundOn.Text = RdrGetLease("RefundOn").ToString
            If IsDate(TxtMoveOutRefundOn.Text) Then
                TxtMoveOutRefundOn.Text = CDate(TxtMoveOutRefundOn.Text).ToString("dd/MM/yyyy")
            End If
            ChkMoveOutAmount.Checked = Val(RdrGetLease("AmountDue").ToString)
            ChkMoveOutGoods.Checked = Val(RdrGetLease("GoodsLeft").ToString)
            ChkMoveOutKeys.Checked = RdrGetLease("KeyReturned")
            TxtEscalation.Text = Val(RdrGetLease("Escalation").ToString)
        Else
            TxtLeasePropNo.Text = ""
            TxtProposalDate.Text = ""
            TxtPropRevision.Text = ""
            TxtLeaseBy.Text = ""
            TxtLeaseValidity.Text = ""
            TxtLeaseReceived.Text = ""
            TxtFittedPeriod.Text = ""
            StrZoneCode = ""
            TxtLeasePropType.Text = ""
            CmbLeaseZone.SelectedIndex = 0
            TxtLeaseFpZone.Text = ""
            TxtLeaseArea.Text = ""
            LblLeaseUnit.Text = ""
            TxtLeaseRate.Text = ""
            TxtLeaseAmount.Text = ""
            TxtLeaseReceived.Text = ""
            TxtLeaseSCharge.Text = ""
            TxtEscalation.Text = ""
            TxtLeaseDeposit.Text = ""
            TxtLeaseInsurance.Text = ""
            TxtLeasePropDesc.Text = ""
            TxtLeaseDate.Text = ""
            'update status
            OptActive.Checked = False
            OptExpired.Checked = False
            OptRenewed.Checked = False
            'Update Move In Data
            TxtMoveIn.Text = ""
            TxtMoveInPropCode.Text = ""
            TxtMoveInArea.Text = ""
            TxtLeaseOn.Text = ""
            TxtLeaseExp.Text = ""
            TxtDepositAmt.Text = ""
            TxtMoveInRate.Text = ""
            TxtPropMonthlyRent.Text = ""
            TxtMoveIn.Text = ""
            TxtDepositPaidOn.Text = ""
            TxtGuaranteeNo.Text = ""
            TxtGuaranteeExp.Text = ""
            TxtFpActivity.Text = ""
            TxtAdRemarks.Text = ""
            TxtOpHours.Text = ""
            TxtElecCharges.Text = ""
            TxtWaterCharge.Text = ""
            TxtTelCharges.Text = ""
            TxtWasteWater.Text = ""
            TxtDateofEntry.Text = ""
            TxtEscalation.Text = ""
            TxtBGAmount.Text = ""
            TxtBGStartDate.Text = ""
            TxtBGEndDate.Text = ""
            TxtBillingFrom.Text = ""
            TxtBillingTo.Text = ""

            'GdvLeaseService.DataSource = Nothing
            'GdvLeaseService.DataBind()
            GdvOpReq.DataSource = Nothing
            GdvOpReq.DataBind()
            Rdr.Close()
            'Clear Move Out Panel
            TxtMoveOutAmountRemarks.Text = ""
            TxtMoveOutAmtDue.Text = ""
            TxtMoveOutArea.Text = ""
            TxtMoveOutDate.Text = ""
            TxtMoveOutDeposit.Text = ""
            TxtMoveOutDesc.Text = ""
            TxtMoveOutGoodsRemarks.Text = ""
            TxtMoveOutKeyRemarks.Text = ""
            TxtMoveOutLeaseNo.Text = ""
            TxtMoveOutPropCode.Text = ""
            TxtMoveOutRate.Text = ""
            TxtMoveOutReason.Text = ""
            TxtMoveOutRefundOn.Text = ""
            ChkMoveOutAmount.Checked = False
            ChkMoveOutGoods.Checked = False
            ChkMoveOutKeys.Checked = False
            Exit Sub
        End If
        RdrGetLease.Close()

        TxtLeasePropertyCode.Text = StrZoneCode
        ''Get services grid
        'StrSql = "SELECT LeaseServices.ChCode Code,TariffCard.Description,TariffCard.Type,TariffCard.Rate,TariffCard.Unit,TariffCard.MinCharge "
        'StrSql = StrSql & "FROM LeaseServices INNER JOIN TariffCard ON TariffCard.ChCode = LeaseServices.ChCode "
        'StrSql = StrSql & "WHERE LeaseServices.LeaseNo = '" & CmbLeaseNo.Text & "'"
        'CmdGetLease.CommandText = StrSql
        'RdrGetLease = CmdGetLease.ExecuteReader
        'GdvLeaseService.DataSource = RdrGetLease
        'GdvLeaseService.DataBind()
        'RdrGetLease.Close()

        'Check if a Movein has been registered for this lease
        StrSql = "SELECT * FROM Property WHERE LeaseNo = '" & CmbLeaseNo.Text & "' AND PropertyCode = '" & TxtMoveInPropCode.Text & "'"
        CmdGetLease.CommandText = StrSql
        RdrGetLease = CmdGetLease.ExecuteReader
        If RdrGetLease.Read Then
            TxtLeaseOn.Text = RdrGetLease("LeaseStart")
            TxtLeaseExp.Text = RdrGetLease("LeaseEnd")
        End If
        RdrGetLease.Close()

        StrSql = "SELECT OpReq,Follow,Who,CASE WHEN Done =1 THEN 'True' ELSE 'False' End As Done FROM OpReq WHERE ClientID = '" & TxtClientID.Text & "'"
        CmdGetLease.CommandText = StrSql
        RdrGetLease = CmdGetLease.ExecuteReader
        GdvOpReq.DataSource = RdrGetLease
        GdvOpReq.DataBind()
        RdrGetLease.Close()

    End Sub

    Private Sub ButNewLease_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNewLease.Click
        TxtLeasePropNo.Text = ""
        TxtProposalDate.Text = ""
        TxtLeaseBy.Text = ""
        TxtLeaseValidity.Text = ""
        TxtLeaseReceived.Text = ""
        TxtFittedPeriod.Text = ""
        TxtLeasePropertyCode.Text = ""
        CmbLeaseZone.SelectedIndex = 0
        TxtLeaseFpZone.Text = ""
        TxtLeaseArea.Text = ""
        LblLeaseUnit.Text = ""
        TxtLeaseRate.Text = ""
        TxtLeaseAmount.Text = ""
        TxtLeaseSCharge.Text = ""
        TxtEscalation.Text = ""
        TxtLeaseDeposit.Text = ""
        TxtLeaseInsurance.Text = ""
        TxtLeasePropDesc.Text = ""
        TxtLeaseDate.Text = Date.Today
        TxtEscalation.Text = ""
        TxtBGAmount.Text = ""
        TxtBGStartDate.Text = ""
        TxtBGEndDate.Text = ""
        TxtBillingFrom.Text = ""
        TxtBillingTo.Text = ""
        'update status
        OptActive.Checked = True
        OptExpired.Checked = False
        OptRenewed.Checked = False
        'GdvLeaseService.DataSource = Nothing
        'GdvLeaseService.DataBind()
        CmbLeaseNo.Items.Add(GetNewLeaseNo)
        CmbLeaseNo.SelectedIndex = CmbLeaseNo.Items.Count - 1
        ButNewLease.Enabled = False
    End Sub

    'Private Sub GdvLeaseService_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLeaseService.RowDataBound
    '    On Error Resume Next
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        e.Row.Attributes("onmouseover") = "this.style.cursor='pointer';"
    '        For I As Integer = 0 To e.Row.Cells.Count - 1
    '            If IsNumeric(e.Row.Cells(I).Text) Then
    '                e.Row.Cells(I).Attributes.Add("align", "right")
    '            Else
    '                e.Row.Cells(I).Attributes.Add("align", "left")
    '            End If
    '        Next
    '    End If
    'End Sub

    Protected Sub ButSaveLease_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSaveLease.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            StrSql = "UPDATE LeaseAgreement SET "
            If TxtLeaseDate.Text <> "" Then
                StrSql = StrSql & "LeaseDate = '" & Format(CDate(TxtLeaseDate.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "LeaseDate = NULL,"
            End If
            If TxtLeaseValidity.Text <> "" Then
                StrSql = StrSql & "Validity = '" & Format(CDate(TxtLeaseValidity.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "Validity = NULL,"
            End If
            StrSql = StrSql & "FittedPeriod = " & Val(TxtFittedPeriod.Text) & ","
            StrSql = StrSql & "Rate = " & Val(TxtLeaseRate.Text) & ","
            StrSql = StrSql & "ConCharges = " & Val(TxtLeaseSCharge.Text) & ","
            StrSql = StrSql & "Deposit = " & Val(TxtLeaseDeposit.Text) & ","
            StrSql = StrSql & "Amount = " & Val(TxtLeaseAmount.Text) & ","
            StrSql = StrSql & "MonthlyRent = " & Val(TxtMonthlyRent.Text) & ","
            StrSql = StrSql & "PreparedBy = '" & TxtLeaseBy.Text & "',"
            StrSql = StrSql & "Area = " & Val(TxtLeaseArea.Text) & ","
            StrSql = StrSql & "MonthlyInsurance = " & Val(TxtMonthlyInsurance.Text) & ","
            If TxtLeaseReceived.Text <> "" Then
                StrSql = StrSql & "LeaseReceived = '" & Format(CDate(TxtLeaseReceived.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "LeaseReceived = NULL,"
            End If
            StrSql = StrSql & "BankBuarantee = " & Val(TxtBGAmount.Text) & ","
            If TxtBGStartDate.Text <> "" Then
                StrSql = StrSql & "BGStartDate = '" & Format(CDate(TxtBGStartDate.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "BGStartDate = NULL,"
            End If
            If TxtBGEndDate.Text <> "" Then
                StrSql = StrSql & "BGEndDate = '" & Format(CDate(TxtBGEndDate.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "BGEndDate = NULL,"
            End If
            If TxtBillingFrom.Text <> "" Then
                StrSql = StrSql & "BillingStart = '" & Format(CDate(TxtBillingFrom.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "BillingStart = NULL,"
            End If
            If TxtBillingTo.Text <> "" Then
                StrSql = StrSql & "BillingTo = '" & Format(CDate(TxtBillingTo.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "BillingTo = NULL,"
            End If
            StrSql = StrSql & "Escalation = " & Val(TxtEscalation.Text) & ""
            StrSql = StrSql & " Where LeaseNo = '" & CmbLeaseNo.Text & "' And ClientID  = '" & TxtLeaseClient.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            'Delete Lease Services records
            'StrSql = "DELETE FROM LeaseServices WHERE LeaseNo = '" & CmbLeaseNo.Text & "'"
            'Cmd.CommandText = StrSql
            'Cmd.ExecuteNonQuery()
            'For Each Row As GridViewRow In GdvLeaseService.Rows
            '    StrSql = "INSERT INTO LeaseServices (LeaseNo,ChCode) VALUES('"
            '    StrSql = StrSql & CmbLeaseNo.Text & "','"
            '    StrSql = StrSql & Row.Cells(1).Text & "')"
            '    Cmd.CommandText = StrSql
            '    Cmd.ExecuteNonQuery()
            'Next
            Trans.Commit()
            Dim StrScript As String = "<script language='javascript'> w.Alert('Lease " & CmbLeaseNo.Text & " Created!','Created','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        Catch ex As Exception
            Trans.Rollback()
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Saving Lease','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try
    End Sub

    Protected Sub ButSaveMoveIn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSaveMoveIn.Click
        Dim PstrDateFormat As String = "dd-MMM-yyyy"
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        If Not IsDate(TxtDateofEntry.Text) Then
            TxtDateofEntry.Text = Date.Today
        End If
        If Not IsDate(TxtMoveIn.Text) Then
            TxtMoveIn.Text = Date.Today
        End If
        Try
            'Update Client First Date
            StrSql = "UPDATE Client SET FirstDate = '" & CDate(TxtMoveIn.Text).ToString("dd-MMM-yyyy") & "' WHERE ClientID = '" & TxtClientID.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            'Delete records from ClientCabin and OpReq
            Cmd.CommandText = "DELETE FROM ClientCabin WHERE ClientID = '" & TxtClientID.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "DELETE FROM OpReq WHERE ClientID = '" & TxtClientID.Text & "'"
            Cmd.ExecuteNonQuery()
            'Save Cabin Details to property table
            TxtPtyLease.Text = CmbLeaseNo.Text
            If TxtLeaseOn.Text <> "" Then
                TxtPtyStart.Text = Format(CDate(TxtLeaseOn.Text), "dd/MM/yyyy")
            End If
            If TxtLeaseExp.Text <> "" Then
                TxtPtyEnd.Text = Format(CDate(TxtLeaseExp.Text), "dd/MM/yyyy")
            End If
            If TxtDateofEntry.Text <> "" Then
                TxtPtyMovedIn.Text = Format(CDate(TxtDateofEntry.Text), "dd/MM/yyyy")
            End If
            If TxtLeaseExp.Text <> "" Then
                TxtPtyMovedOut.Text = Format(CDate(TxtLeaseExp.Text), "dd/MM/yyyy")
            End If
            If TxtBillingTo.Text <> "" Then
                TxtPtyBilledUntil.Text = Format(CDate(TxtBillingTo.Text), "dd/MM/yyyy")
            End If
            StrSql = "UPDATE Property SET PrevClientID = ClientID, ClientID = '" & TxtClientID.Text & "',"
            StrSql = StrSql & "MovedIn = '" & CDate(TxtMoveIn.Text).ToString("dd-MMM-yyyy") & "',"
            StrSql = StrSql & "BilledUntil = '" & CDate(TxtMoveIn.Text).ToString("dd-MMM-yyyy") & "',"
            StrSql = StrSql & "LeaseNo = '" & CmbLeaseNo.Text & "', LeaseStart = '"
            StrSql = StrSql & CDate(TxtLeaseOn.Text).ToString("dd-MMM-yyyy") & "',"
            StrSql = StrSql & "LeaseEnd = '" & CDate(TxtLeaseExp.Text).ToString("dd-MMM-yyyy") & "' "
            StrSql = StrSql & "WHERE PropertyCode = '" & TxtMoveInPropCode.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            'Update Additional Fields in Lease Agreement Table
            StrSql = "UPDATE LeaseAgreement SET "
            If IsDate(TxtDepositPaidOn.Text) Then
                StrSql = StrSql & "DepositPaidOn = '" & CDate(TxtDepositPaidOn.Text).ToString("dd-MMM-yyyy") & "',"
            Else
                StrSql = StrSql & "DepositPaidOn = NULL,"
            End If
            StrSql = StrSql & "GuaranteeNo = '" & TxtGuaranteeNo.Text & "',"
            If IsDate(TxtGuaranteeExp.Text) Then
                StrSql = StrSql & "GuaranteeExp = '" & CDate(TxtGuaranteeExp.Text).ToString("dd-MMM-yyyy") & "',"
            Else
                StrSql = StrSql & "GuaranteeExp = NULL,"
            End If
            StrSql = StrSql & "FPActivities = '" & TxtFpActivity.Text & "',"
            StrSql = StrSql & "AddRemarks = '" & TxtAdRemarks.Text & "',"
            StrSql = StrSql & "OPHours = '" & TxtOpHours.Text & "',"
            StrSql = StrSql & "Insurance = " & Val(TxtLeaseInsurance.Text) & ","
            StrSql = StrSql & "Electricity = '" & TxtElecCharges.Text & "',"
            StrSql = StrSql & "Water = " & Val(TxtWaterCharge.Text) & ","
            StrSql = StrSql & "Telephone = " & Val(TxtTelCharges.Text) & ","
            StrSql = StrSql & "WasteWater = " & Val(TxtWasteWater.Text) & ","
            StrSql = StrSql & "MoveInDate = '" & CDate(TxtMoveIn.Text).ToString("dd-MMM-yyyy") & "',"
            StrSql = StrSql & "DateofEntry = '" & CDate(TxtDateofEntry.Text).ToString("dd-MMM-yyyy") & "',"
            StrSql = StrSql & "MovedIn = 1 "
            StrSql = StrSql & "WHERE LeaseNo = '" & CmbLeaseNo.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            'Update Operational Request
            StrSql = "DELETE FROM OpReq WHERE ClientID = '" & TxtClientID.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            For Each Row As GridViewRow In GdvOpReq.Rows
                StrSql = "INSERT INTO OpReq VALUES('"
                StrSql = StrSql & TxtClientID.Text & "','"
                StrSql = StrSql & Row.Cells(0).Text.Replace("&nbsp;", "") & "','"
                StrSql = StrSql & Row.Cells(1).Text.Replace("&nbsp;", "") & "','"
                StrSql = StrSql & Row.Cells(2).Text.Replace("&nbsp;", "") & "',"
                StrSql = StrSql & IIf(DirectCast(Row.Cells(3).Controls(0), CheckBox).Checked, 1, 0) & ")"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            Next
            Trans.Commit()
            UpdateProperty()
            Dim CmdCheckMoveIn As New OleDb.OleDbCommand
            Dim RdrCheckMoveIN As OleDb.OleDbDataReader
            CmdCheckMoveIn.Connection = Con
            CmdCheckMoveIn.CommandText = "Select *From LeaseAgreement Where LeaseNo = '" & CmbLeaseNo.Text & "'"
            RdrCheckMoveIN = CmdCheckMoveIn.ExecuteReader
            If RdrCheckMoveIN.Read Then
                If Not IsDBNull(RdrCheckMoveIN("LeaseType")) Then
                    If RdrCheckMoveIN("LeaseType") = "Renewal" Then
                        Dim StrScript As String = "<script language='javascript'> w.Alert('Renewal Move In Created!','Created','images/Utility.ico');</script>"
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
                    End If
                Else
                    Dim StrScript As String = "<script language='javascript'> w.Alert('Move In Created!','Created','images/Utility.ico');</script>"
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
                End If
            End If
            RdrCheckMoveIN.Close()

        Catch ex As Exception
            Trans.Rollback()
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Saving Move In','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try
    End Sub

    Protected Sub ButAddReq_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAddReq.Click
        Dim Dt As New DataTable
        Dim Dr As DataRow
        StrSql = "SELECT OpReq,Follow,Who,Done FROM OpReq WHERE ClientID = NULL"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        Dt.Load(Rdr)
        For Each Row As GridViewRow In GdvOpReq.Rows
            If Row.Cells(0).Text <> TxtOpReq.Text Then
                Dr = Dt.NewRow
                For I = 0 To GdvOpReq.Columns.Count - 1
                    If I = 3 Then
                        Dr.Item(I) = DirectCast(Row.Cells(3).Controls(0), CheckBox).Checked
                    Else
                        Dr.Item(I) = Row.Cells(I).Text
                    End If
                Next
                Dt.Rows.Add(Dr)
            End If
        Next
        Dr = Dt.NewRow
        Dr.Item(0) = TxtOpReq.Text
        Dr.Item(1) = TxtFollow.Text
        Dr.Item(2) = TxtWho.Text
        Dr.Item(3) = ChkDone.Checked
        Dt.Rows.Add(Dr)
        GdvOpReq.DataSource = Dt
        GdvOpReq.DataBind()
    End Sub

    Protected Sub ButDelReq_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDelReq.Click
        Dim Dt As New DataTable
        Dim Dr As DataRow
        StrSql = "SELECT OpReq,Follow,Who,Done FROM OpReq WHERE ClientID = NULL"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        Dt.Load(Rdr)
        For Each Row As GridViewRow In GdvOpReq.Rows
            If Row.Cells(0).Text <> TxtOpReq.Text Then
                Dr = Dt.NewRow
                For I As Integer = 0 To GdvOpReq.Columns.Count - 1
                    If I = 3 Then
                        Dr.Item(I) = DirectCast(Row.Cells(3).Controls(0), CheckBox).Checked
                    Else
                        Dr.Item(I) = Row.Cells(I).Text
                    End If
                Next
                Dt.Rows.Add(Dr)
            End If
        Next
        GdvOpReq.DataSource = Dt
        GdvOpReq.DataBind()
    End Sub

    Private Sub GdvOpReq_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvOpReq.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='pointer';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvOpReq, "Select$" & e.Row.RowIndex)
            For I As Integer = 0 To e.Row.Cells.Count - 1
                If IsNumeric(e.Row.Cells(I).Text) Then
                    e.Row.Cells(I).Attributes.Add("align", "right")
                Else
                    e.Row.Cells(I).Attributes.Add("align", "left")
                End If
            Next
        End If
    End Sub

    Private Sub GdvOpReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvOpReq.SelectedIndexChanged
        TxtOpReq.Text = GdvOpReq.SelectedRow.Cells(0).Text
        TxtFollow.Text = GdvOpReq.SelectedRow.Cells(1).Text
        TxtWho.Text = GdvOpReq.SelectedRow.Cells(2).Text
        ChkDone.Checked = DirectCast(GdvOpReq.SelectedRow.Cells(3).Controls(0), CheckBox).Checked
    End Sub

    Private Sub GdvOpReq_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvOpReq.SelectedIndexChanging
        GdvOpReq.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Sub ButMoveOut_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButMoveOut.Click
        StrSql = "UPDATE LeaseAgreement SET "
        StrSql = StrSql & "MovedOut = 1,"
        StrSql = StrSql & "MoveOutDate = '" & CDate(TxtMoveOutDate.Text).ToString("dd-MMM-yyyy") & "',"
        StrSql = StrSql & "Reason = '" & TxtMoveOutReason.Text & "',"
        StrSql = StrSql & "AmountToPay = " & Val(TxtMoveOutAmtDue.Text) & ","
        If IsDate(TxtMoveOutRefundOn.Text) Then
            StrSql = StrSql & "RefundOn = '" & CDate(TxtMoveOutRefundOn.Text).ToString("dd-MMM-yyyy") & "',"
        Else
            StrSql = StrSql & "RefundOn = NULL,"
        End If
        StrSql = StrSql & "AmountRemarks = '" & TxtMoveOutAmountRemarks.Text & "',"
        StrSql = StrSql & "GoodsRemarks = '" & TxtMoveOutGoodsRemarks.Text & "',"
        StrSql = StrSql & "KeyRemarks = '" & TxtMoveOutKeyRemarks.Text & "',"
        StrSql = StrSql & "LeaseStatus = 'Expired',"
        If ChkMoveOutAmount.Checked Then
            StrSql = StrSql & "AmountDue = 1,"
        Else
            StrSql = StrSql & "AmountDue = 0,"
        End If
        If ChkMoveOutGoods.Checked Then
            StrSql = StrSql & "GoodsLeft = 1,"
        Else
            StrSql = StrSql & "GoodsLeft = 0,"
        End If
        If ChkMoveOutKeys.Checked Then
            StrSql = StrSql & "KeyReturned = 1 "
        Else
            StrSql = StrSql & "KeyReturned = 0 "
        End If
        StrSql = StrSql & "WHERE LeaseNo = '" & TxtMoveOutLeaseNo.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Cmd.ExecuteNonQuery()

        StrSql = "UPDATE Property SET "
        StrSql = StrSql & "ClientID = NULL,"
        StrSql = StrSql & "LeaseNo = NULL,"
        StrSql = StrSql & "LeaseStart = NULL,"
        StrSql = StrSql & "LeaseEnd = NULL,"
        StrSql = StrSql & "MovedIn = NULL,"
        StrSql = StrSql & "PrevClientID = '" & TxtPtyClientID.Text & "',"
        If TxtMoveOutDate.Text <> "" Then
            StrSql = StrSql & "PrevMovedOut = '" & Format(CDate(TxtMoveOutDate.Text), "MM/dd/yyyy") & "',"
        Else
            StrSql = StrSql & "PrevMovedOut = NULL,"
        End If

        StrSql = StrSql & "BilledUntil = NULL "
        StrSql = StrSql & "WHERE PropertyCode = '" & TxtMoveInPropCode.Text & "'"
        Cmd.CommandText = StrSql
        Cmd.ExecuteNonQuery()
        UpdateProperty()
        Try
            Dim StrScript As String = "<script language='javascript'> w.Alert('Move Out Updated!','Created','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        Catch ex As Exception
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Saving Move Out','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try
    End Sub

    Private Sub HidClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles HidClientID.TextChanged
        TxtClientID.Text = HidClientID.Text
        TxtClientID_TextChanged(Me, e)
    End Sub

    Protected Sub CmbPtyFPZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbPtyFPZone.SelectedIndexChanged
        UpdateProperty()
    End Sub

    Protected Sub BtnPrintMoveIN_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnPrintMoveIN.Click
        Dim StrSelFormula As String
        Session("ReportFile") = "RptMoveIn1.rpt"
        Session("Period") = ""
        Session("Zone") = ""
        StrSelFormula = "{Client.ClientID} = '" & TxtClientID.Text & "' And {LeaseAgreement.LeaseNo} ='" & CmbLeaseNo.Text & "' And {Property.PropertyCode}='" & TxtLeasePropertyCode.Text & "'"
        Session("SelFormula") = StrSelFormula
        Session("RepHead") = "Move In for " & TxtPClientName.Text
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub

    Dim AppWord As Object

    Public Function MapURL(ByVal Path As String) As String
        Dim AppPath As String =
        HttpContext.Current.Server.MapPath("~")
        Dim url As String = String.Format("~{0}" _
        , Path.Replace(AppPath, "").Replace("\", "/"))
        Return url
    End Function

    Public Shared Sub SearchAndReplace(ByVal document As String, ByVal dict As Dictionary(Of String, String))
        Using wordDoc As WordprocessingDocument = WordprocessingDocument.Open(document, True)
            Dim docText As String = Nothing
            Using sr As New StreamReader(wordDoc.MainDocumentPart.GetStream())
                docText = sr.ReadToEnd()
            End Using

            For Each item As KeyValuePair(Of String, String) In dict
                Dim regexText As New Regex(item.Key)
                docText = regexText.Replace(docText, item.Value)
            Next

            Using sw As New StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create))
                sw.Write(docText)

            End Using
        End Using
    End Sub

    Protected Sub BtnGenTemplate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnGenTemplate.Click


        Try
            Dim sourceFile As String = ""
            Dim destinationFile As String = ""
            LblError.Text = "Start creating word file"


            If CmbTemplateDoc.Text = "Lease agreement cabin" Then
                sourceFile = "/Template/Lease agreement cabin.docx"
                destinationFile = "/NewDocuments/Lease agreement cabin.docx"
            End If
            If CmbTemplateDoc.Text = "Lease agreement" Then
                sourceFile = "/Template/Lease agreement.docx"
                destinationFile = "/NewDocuments/Lease agreement.docx"
            End If
            If CmbTemplateDoc.Text = "Lease Covering letter for New Lease" Then
                sourceFile = "/Template/Lease Covering letter for New Lease.docx"
                destinationFile = "/NewDocuments/Lease Covering letter for New Lease.docx"
            End If
            If CmbTemplateDoc.Text = "Lease letter-Bonded Car Yard" Then
                sourceFile = "/Template/Lease letter-Bonded Car Yard.docx"
                destinationFile = "/NewDocuments/Lease letter-Bonded Car Yard.docx"
            End If

            '#########################'#########################'#########################'#########################'#########################
            'If CmbTemplateDoc.Text = "Lease Special Conditions - Non Processing" Then
            '    sourceFile = "/Template/Lease Special Conditions - Non Processing.docx"
            '    destinationFile = "/NewDocuments/Lease Special Conditions - Non Processing.docx"
            'End If
            'If CmbTemplateDoc.Text = "Lease Special Conditions - office" Then
            '    sourceFile = "/Template/Lease Special Conditions - office.docx"
            '    destinationFile = "/NewDocuments/Lease Special Conditions - office.docx"
            'End If
            'If CmbTemplateDoc.Text = "Lease Special Conditions - Warehousing - Processing" Then
            '    sourceFile = "/Template/Lease Special Conditions - Warehousing - Processing.docx"
            '    destinationFile = "/NewDocuments/Lease Special Conditions - Warehousing - Processing.docx"
            'End If
            'If CmbTemplateDoc.Text = "Lease Special Conditions - Warehousing" Then
            '    sourceFile = "/Template/Lease Special Conditions - Warehousing.docx"
            '    destinationFile = "/NewDocuments/Lease Special Conditions - Warehousing.docx"
            'End If
            '#########################'#########################'#########################'#########################'#########################
            If CmbTemplateDoc.Text = "Special Conditions" Then
                sourceFile = "/Template/Special Conditions.docx"
                destinationFile = "/NewDocuments/Special Conditions.docx"
            End If
            If CmbTemplateDoc.Text = "Special Conditions (only in case of escalation)" Then
                sourceFile = "/Template/Special Conditions (only in case of escalation).docx"
                destinationFile = "/NewDocuments/Special Conditions (only in case of escalation).docx"
            End If

            If CmbTemplateDoc.Text = "Letter for non objection termination of lease" Then
                sourceFile = "/Template/Letter for non objection termination of lease.docx"
                destinationFile = "/NewDocuments/Letter for non objection termination of lease.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Bonded Car yard Non Freeport Activity zone 6" Then
                sourceFile = "/Template/Proposal Bonded Car yard Non Freeport Activity zone 6.docx"
                destinationFile = "/NewDocuments/Proposal Bonded Car yard Non Freeport Activity zone 6.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Manufacturing Freeport activity WH1 zone 6" Then
                sourceFile = "/Template/Proposal Manufacturing Freeport activity WH1 zone 6.docx"
                destinationFile = "/NewDocuments/Proposal Manufacturing Freeport activity WH1 zone 6.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Non Warehousing services  Freeport Activities  Zone 1" Then
                sourceFile = "/Template/Proposal Non Warehousing services  Freeport Activities  Zone 1.docx"
                destinationFile = "/NewDocuments/Proposal Non Warehousing services  Freeport Activities  Zone 1.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Non Warehousing services  Non Freeport Activities  Zone 1" Then
                sourceFile = "/Template/Proposal Non Warehousing services  Non Freeport Activities  Zone 1.docx"
                destinationFile = "/NewDocuments/Proposal Non Warehousing services  Non Freeport Activities  Zone 1.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Non Warehousing services Freeport activity zone 6" Then
                sourceFile = "/Template/Proposal Non Warehousing services Freeport activity zone 6.docx"
                destinationFile = "/NewDocuments/Proposal Non Warehousing services Freeport activity zone 6.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Non Warehousing services Non Freeport activity zone 6" Then
                sourceFile = "/Template/Proposal Non Warehousing services Non Freeport activity zone 6.docx"
                destinationFile = "/NewDocuments/Proposal Non Warehousing services Non Freeport activity zone 6.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal outdoor storage Freeport Activities  Zone 1" Then
                sourceFile = "/Template/Proposal outdoor storage Freeport Activities  Zone 1.docx"
                destinationFile = "/NewDocuments/Proposal outdoor storage Freeport Activities  Zone 1.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal outdoor storage Freeport Activities  Zone 6" Then
                sourceFile = "/Template/Proposal outdoor storage Freeport Activities  Zone 6.docx"
                destinationFile = "/NewDocuments/Proposal outdoor storage Freeport Activities  Zone 6.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal outdoor storage Non Freeport Activities  Zone 1" Then
                sourceFile = "/Template/Proposal outdoor storage Non Freeport Activities  Zone 1.docx"
                destinationFile = "/NewDocuments/Proposal outdoor storage Non Freeport Activities  Zone 1.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal outdoor storage Non Freeport Activities  Zone 6" Then
                sourceFile = "/Template\Proposal outdoor storage Non Freeport Activities  Zone 6.docx"
                destinationFile = "/NewDocuments/Proposal outdoor storage Non Freeport Activities  Zone 6.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Rental of  strong room Freeport Activity Zone 9" Then
                sourceFile = "/Template/Proposal Rental of  strong room Freeport Activity Zone 9.docx"
                destinationFile = "/NewDocuments/Proposal Rental of  strong room Freeport Activity Zone 9.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Rental of Cabin Freeport Activity Zone9" Then
                sourceFile = "/Template/Proposal Rental of Cabin Freeport Activity Zone9.docx"
                destinationFile = "/NewDocuments/Proposal Rental of Cabin Freeport Activity Zone9.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Rental of Cabin Non Freeport Activity Zone9" Then
                sourceFile = "/Template/Proposal Rental of Cabin Non Freeport Activity Zone9.docx"
                destinationFile = "/NewDocuments/Proposal Rental of Cabin Non Freeport Activity Zone9.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Rental of Office Freeport Activity Zone9" Then
                sourceFile = "/Template/Proposal Rental of Office Freeport Activity Zone9.docx"
                destinationFile = "/NewDocuments/Proposal Rental of Office Freeport Activity Zone9.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Rental of warehouse  Non freeport activities zone 1" Then
                sourceFile = "/Template/Proposal Rental of warehouse  Non freeport activities zone 1.docx"
                destinationFile = "/NewDocuments/Proposal Rental of warehouse  Non freeport activities zone 1.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Rental of Warehouse 4 Freeport Activities Zone 6" Then
                sourceFile = "/Template/Proposal Rental of Warehouse 4 Freeport Activities Zone 6.docx"
                destinationFile = "/NewDocuments/Proposal Rental of Warehouse 4 Freeport Activities Zone 6.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Rental of Warehouse 4 Non Freeport  activities zone 6" Then
                sourceFile = "/Template/Proposal Rental of Warehouse 4 Non Freeport  activities zone 6.docx"
                destinationFile = "\NewDocuments\Proposal Rental of Warehouse 4 Non Freeport  activities zone 6.docx"
            End If
            If CmbTemplateDoc.Text = "Proposal Rental of warehouse freeport activities zone 1" Then
                sourceFile = "/Template/Proposal Rental of warehouse freeport activities zone 1.docx"
                destinationFile = "/NewDocuments/Proposal Rental of warehouse freeport activities zone 1.docx"
            End If
            If CmbTemplateDoc.Text = "Renewal letter" Then
                sourceFile = "/Template/Renewal letter.docx"
                destinationFile = "/NewDocuments/Renewal letter.docx"
            End If

            File.Copy(Server.MapPath("~" & sourceFile), Server.MapPath("~" & destinationFile), True)
            sourceFile = Server.MapPath("~" & sourceFile)
            destinationFile = Server.MapPath("~" & destinationFile)

            Dim missing As Object = System.Type.Missing
            Dim doc As DocX = Nothing
            Try

                doc = DocX.Load(sourceFile)
                If TxtName.Text <> "" Then
                    doc.ReplaceText("#Companyname", TxtName.Text)
                End If
                doc.ReplaceText("#Todaydate", Format(CDate(Date.Today), "dd-MMM-yyyy"))
                If TxtContact.Text <> "" Then
                    doc.ReplaceText("#Contactperson", TxtContact.Text)
                End If
                If TxtPosition.Text <> "" Then
                    doc.ReplaceText("#Position", TxtPosition.Text)
                End If
                If TxtAddress.Text <> "" Then
                    doc.ReplaceText("#Address1", TxtAddress.Text)
                End If
                If TxtAddress2.Text <> "" Then
                    doc.ReplaceText("#Address2", TxtAddress2.Text)
                End If
                If TxtAddress2.Text <> "" Then
                    doc.ReplaceText("#Address3", TxtCity.Text)
                End If
                If TxtEmail.Text <> "" Then
                    doc.ReplaceText("#Email", TxtEmail.Text)
                End If
                If TxtArea.Text <> "" Then
                    doc.ReplaceText("#Area", TxtLeaseArea.Text)
                End If
                If TxtLeaseArea.Text <> "" Then
                    doc.ReplaceText("#Area", TxtLeaseArea.Text)
                End If
                If TxtLeaseMonthlyCharge.Text <> "" Then
                    doc.ReplaceText("#Monthlyrent", FormatNumber(Val(TxtLeaseMonthlyCharge.Text), 2, , , TriState.True))
                End If
                If TxtPropMonthlyRent.Text <> "" Then
                    doc.ReplaceText("#Monthlyrent", FormatNumber(Val(TxtPropMonthlyRent.Text), 2, , , TriState.True))
                End If
                If TxtLeaseDeposit.Text <> "" Then
                    doc.ReplaceText("#Deposit", FormatNumber(Val(TxtLeaseDeposit.Text), 2, , , TriState.True))
                End If
                If TxtDeposit.Text <> "" Then
                    doc.ReplaceText("#Deposit", FormatNumber(Val(TxtDeposit.Text), 2, , , TriState.True))
                End If
                If TxtLeaseInsurance.Text <> "" Then
                    doc.ReplaceText("#Insurance", FormatNumber(Val(TxtLeaseInsurance.Text), 2, , , TriState.True))
                End If
                If TxtInsurance.Text <> "" Then
                    doc.ReplaceText("#Insurance", FormatNumber(Val(TxtInsurance.Text), 2, , , TriState.True))
                End If
                If TxtLeaseOn.Text <> "" Then
                    doc.ReplaceText("#Leasestart", Format(CDate(TxtLeaseOn.Text), "dd-MMM-yyyy"))
                End If
                If TxtLeaseExp.Text <> "" Then
                    doc.ReplaceText("#Leaseend", Format(CDate(TxtLeaseExp.Text), "dd-MMM-yyyy"))
                End If
                If TxtLeaseSCharge.Text <> "" Then
                    doc.ReplaceText("#Servicecharge", FormatNumber(Val(TxtLeaseSCharge.Text), 2, , , TriState.True))
                End If
                If TxtSCharges.Text <> "" Then
                    doc.ReplaceText("#Servicecharge", FormatNumber(Val(TxtSCharges.Text), 2, , , TriState.True))
                End If
                doc.SaveAs(destinationFile)
            Catch ex As Exception
                'Response.Write(ex.Message)
                LblError.Text = ex.Message
            Finally
                'doc.Close(missing, missing, missing)
                'DirectCast(word, Word._Application).Quit()
                Session("Output") = destinationFile
                Dim ResponseHelper As New ResponseHelper
                ResponseHelper.Redirect("FileViewer.aspx", "_blank", "menubar=0,width=800,height=600")
            End Try
        Catch ex As Exception
            LblError.Text = ex.Message
        End Try
    End Sub

    Private Sub TxtLeaseInsurance_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtLeaseInsurance.TextChanged
        TxtMoveInInsurance.Text = Val(TxtLeaseInsurance.Text)
    End Sub

    Protected Sub TxtInsurance_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtInsurance.TextChanged
        TxtMoveInInsurance.Text = Val(TxtLeaseInsurance.Text)
    End Sub

    Protected Sub TxtZoneCode_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtZoneCode.TextChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Description,Type,Area,Unit FROM Property WHERE PropertyCode = '" & TxtZoneCode.Text & "'"
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtZoneName.Text = Rdr("Description").ToString
            TxtPropType.Text = Rdr("Type").ToString
            TxtArea.Text = Rdr("Area").ToString
            LblUnit.Text = Rdr("Unit").ToString
        End If
        Rdr.Close()
    End Sub

    Protected Sub ButRefreshAll_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRefreshAll.Click
        RefreshAll()
    End Sub

    Private Function GetNewRenewal() As String
        'Get New Proposal Ref
        Dim StrRef As String = ""
        Cmd.Connection = Con
        StrSql = "SELECT NextRenNo From SysParam"
        Cmd.CommandText = StrSql
        StrRef = Cmd.ExecuteScalar.ToString
        StrRef = Session("UserID") & "/" & StrRef & "/" & Date.Today.ToString("yy")
        Return StrRef
    End Function

    Protected Sub ButRenewLease_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRenewLease.Click
        Dim CmdChkRen As New OleDb.OleDbCommand
        Dim CmdRenNo As New OleDb.OleDbCommand
        Dim CmdRen As New OleDb.OleDbCommand
        Dim RdrChkRen As OleDb.OleDbDataReader
        Dim IntRenewal As Integer
        Dim StrCounter As String = ""
        Dim StrLease As String = ""
        Dim StrOriginalLease As String = ""

        If OptRenewed.Checked = False Then
            LblLeaseError.Visible = True
            LblLeaseError.Text = "Please Select As Renewed"
            Exit Sub
        End If
        CmdChkRen.Connection = Con
        CmdChkRen.CommandText = "Select *From LeaseAgreement Where LeaseNo = '" & CmbLeaseNo.Text & "'"
        RdrChkRen = CmdChkRen.ExecuteReader
        If RdrChkRen.Read Then
            If RdrChkRen("LeaseStatus") = "Expired" Then
                Dim StrScript As String = "<script language='javascript'> w.Alert('This Lease Is Already Expired. Please Select Active Lease','Renewal','images/Utility.ico');</script>"
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
                Exit Sub
            End If
            If Not IsDBNull(RdrChkRen("LeaseType")) Then
                If RdrChkRen("LeaseType") = "Renewal" Then
                    StrLease = RdrChkRen("LNO")
                    StrOriginalLease = RdrChkRen("LNO")
                End If
            Else
                StrLease = RdrChkRen("LeaseNo")
                StrOriginalLease = RdrChkRen("LeaseNo")
            End If
        End If
        RdrChkRen.Close()

        CmdRenNo.Connection = Con
        CmdRenNo.CommandText = "Select Count(*)From LeaseAgreement Where LNO = '" & StrLease & "'"
        If CmdRenNo.ExecuteScalar >= 0 Then

            If TxtLeaseClient.Text = "400C8449" Then
                'Counter issue with client Sun Packaging Co (Mauritius) Ltd 
                IntRenewal = CmdRenNo.ExecuteScalar + 2
                'ElseIf TxtLeaseClient.Text = "400C8322" Then
                'Counter issue with client UFS Logistics Ltd
                'IntRenewal = CmdRenNo.ExecuteScalar + 2
            Else
                IntRenewal = CmdRenNo.ExecuteScalar + 1
            End If
            StrCounter = IntRenewal.ToString
            StrLease = StrLease & "/Ren" & StrCounter
            TxtLeaseDate.Text = ""
            TxtLeaseReceived.Text = ""
            TxtLeaseSCharge.Text = ""
            TxtLeaseRate.Text = ""
            TxtLeaseDeposit.Text = ""
            TxtLeaseAmount.Text = ""
            TxtLeaseInsurance.Text = ""
            TxtMoveIn.Text = ""
            TxtDateofEntry.Text = ""
            TxtLeaseOn.Text = ""
            TxtLeaseExp.Text = ""
            TxtDepositPaidOn.Text = ""
            TxtOpHours.Text = ""
            TxtTelCharges.Text = ""
            TxtFpActivity.Text = ""
            TxtAdRemarks.Text = ""
            TxtGuaranteeNo.Text = ""
            TxtWaterCharge.Text = ""
            TxtGuaranteeExp.Text = ""
            TxtElecCharges.Text = ""
            TxtWasteWater.Text = ""
            TxtMoveINServiceCharge.Text = ""
            TxtEscalation.Text = ""
            TxtBGAmount.Text = ""
            TxtBGStartDate.Text = ""
            TxtBGEndDate.Text = ""
            TxtBillingFrom.Text = ""
            TxtBillingTo.Text = ""
            CmdRen.Connection = Con
            StrSql = "INSERT INTO LeaseAgreement (LeaseNo,LeaseDate,ProposalNo,ClientID,PropDate,PreparedBy,Validity,ProjectDesc,Zone,PropCode,Area,Unit,Rate,"
            StrSql = StrSql & "Amount,ConCharges, Status,Deposit,Insurance,LeaseReceived, LNO,LeaseType,MonthlyRent,BankBuarantee,BGStartDate,BGEndDate,BillingStart,BillingTo,Escalation) VALUES("
            StrSql = StrSql & "'" & StrLease & "',"
            If TxtLeaseDate.Text <> "" Then
                StrSql = StrSql & "'" & Format(CDate(TxtLeaseDate.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "NULL,"
            End If
            StrSql = StrSql & "'" & TxtLeasePropNo.Text & "',"
            StrSql = StrSql & "'" & TxtLeaseClient.Text & "',"
            If TxtProposalDate.Text <> "" Then
                StrSql = StrSql & "'" & Format(CDate(TxtProposalDate.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "NULL,"
            End If
            StrSql = StrSql & "'" & TxtLeaseBy.Text & "',"
            If TxtLeaseValidity.Text <> "" Then
                StrSql = StrSql & "'" & Format(CDate(TxtLeaseValidity.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "NULL,"
            End If
            StrSql = StrSql & "'" & TxtLeasePropDesc.Text & "',"
            StrSql = StrSql & "'" & TxtLeaseFpZone.Text & "',"
            StrSql = StrSql & "'" & TxtLeasePropertyCode.Text & "',"
            StrSql = StrSql & "" & Val(TxtLeaseArea.Text) & ","
            StrSql = StrSql & "'" & LblLeaseUnit.Text & "',"
            StrSql = StrSql & "" & Val(TxtLeaseRate.Text) & ","
            StrSql = StrSql & "" & Val(TxtLeaseAmount.Text) & ","
            StrSql = StrSql & "" & Val(TxtLeaseSCharge.Text) & ","
            StrSql = StrSql & "'Active',"
            StrSql = StrSql & "" & Val(TxtLeaseDeposit.Text) & ","
            StrSql = StrSql & "" & Val(TxtLeaseInsurance.Text) & ","
            If TxtLeaseReceived.Text <> "" Then
                StrSql = StrSql & "'" & Format(CDate(TxtLeaseReceived.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "NULL,"
            End If
            StrSql = StrSql & "'" & StrOriginalLease & "',"
            StrSql = StrSql & "'Renewal',"
            StrSql = StrSql & "" & Val(TxtLeaseMonthlyCharge.Text) & ","
            StrSql = StrSql & "" & Val(TxtBGAmount.Text) & ","
            If TxtBGStartDate.Text <> "" Then
                StrSql = StrSql & "'" & Format(CDate(TxtBGStartDate.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "NULL,"
            End If
            If TxtBGEndDate.Text <> "" Then
                StrSql = StrSql & "'" & Format(CDate(TxtBGEndDate.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "NULL,"
            End If
            If TxtBillingFrom.Text <> "" Then
                StrSql = StrSql & "'" & Format(CDate(TxtBillingFrom.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "NULL,"
            End If
            If TxtBillingTo.Text <> "" Then
                StrSql = StrSql & "'" & Format(CDate(TxtBillingTo.Text), "MM/dd/yyyy") & "',"
            Else
                StrSql = StrSql & "NULL,"
            End If
            StrSql = StrSql & "" & Val(TxtEscalation.Text) & ")"
            CmdRen.CommandText = StrSql
            CmdRen.ExecuteNonQuery()
            'CmdRen.CommandText = "Delete From LeaseServices Where LeaseNo= '" & StrLease & "'"
            'For Each Row As GridViewRow In GdvLeaseService.Rows
            '    StrSql = "INSERT INTO LeaseServices (LeaseNo,ChCode) VALUES('"
            '    StrSql = StrSql & StrLease & "','"
            '    StrSql = StrSql & Row.Cells(1).Text & "')"
            '    CmdRen.CommandText = StrSql
            '    CmdRen.ExecuteNonQuery()
            'Next

            'Update previous lease agreement as mouved out 
            StrSql = "Update LeaseAgreement Set MovedOut =1, [Status] = 'Moved Out',LeaseStatus = 'Expired'  WHERE LeaseNo = '" & StrOriginalLease & "' And ClientID = '" & TxtLeaseClient.Text & "'"
            CmdRen.CommandText = StrSql
            CmdRen.ExecuteNonQuery()

            'Update Lease Agreements
            StrSql = "SELECT LeaseNo FROM LeaseAgreement WHERE LNO = '" & StrOriginalLease & "'"
            CmdRen.CommandText = StrSql
            RdrChkRen = CmdRen.ExecuteReader
            CmbLeaseNo.Items.Clear()
            CmbLeaseNo.Items.Add("Select...")
            While RdrChkRen.Read
                CmbLeaseNo.Items.Add(RdrChkRen("LeaseNo"))
            End While
            RdrChkRen.Close()
            CmbLeaseNo.Text = StrLease
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub TxtSCharges_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtSCharges.TextChanged
        TxtPropMonthlyRent.Text = Format((Val(TxtSCharges.Text) + Val(TxtAmount.Text)), "0.00")
    End Sub

    Protected Sub TxtAmount_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtAmount.TextChanged
        TxtPropMonthlyRent.Text = Format((Val(TxtSCharges.Text) + Val(TxtAmount.Text)), "0.00")
    End Sub

    Protected Sub TxtLeaseSCharge_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtLeaseSCharge.TextChanged
        TxtLeaseMonthlyCharge.Text = Val(TxtLeaseSCharge.Text) + Val(TxtLeaseAmount.Text)
        TxtMoveINServiceCharge.Text = Val(TxtLeaseSCharge.Text)
        TxtMonthlyRent.Text = Val(TxtLeaseSCharge.Text) + Val(TxtLeaseAmount.Text)
    End Sub

    Protected Sub TxtLeaseAmount_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtLeaseAmount.TextChanged
        TxtLeaseMonthlyCharge.Text = Val(TxtLeaseSCharge.Text) + Val(TxtLeaseAmount.Text)
        TxtMonthlyRent.Text = Val(TxtLeaseSCharge.Text) + Val(TxtLeaseAmount.Text)
    End Sub

    Protected Sub CmbFreeport_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbFreeport.SelectedIndexChanged
        UpdateChart()
    End Sub

    Protected Sub ButDeleteCharge_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDeleteCharge.Click
        Cmd.Connection = Con

        Cmd.CommandText = "Delete From TariffCard Where ChCode = '" & TxtChCode.Text & "'"
        Cmd.ExecuteNonQuery()

        CmbChargeFPZone.SelectedIndex = -1
        CmbChType.SelectedIndex = -1
        TxtChCode.Text = ""
        TxtChDescription.Text = ""
        CmbCondition.SelectedIndex = -1
        CmbDaysApp.SelectedIndex = -1
        TxtFromTime.Text = ""
        TxtToTime.Text = ""
        TxtChargeRate.Text = ""
        TxtChargeUnit.Text = ""
        TxtPerChDays.Text = ""
        TxtMinCharge.Text = ""
        TxtMinUnit.Text = ""
        CmbCopyFPZone.SelectedIndex = -1
        GetTariffCard()
    End Sub

    Protected Sub ButFindLease_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindLease.Click
        DragPanel.Style.Item("display") = ""
        PlnFindLease.Style.Item("display") = ""
        If TxtProposalNo.Text <> "" Then
            StrSql = "Select LeaseNo, CONVERT(Char,LeaseDate,103), ProposalNo, ProjectDesc, Zone, PropCode,Area,Unit,Amount, MonthlyRent From LeaseAgreement Where "
            StrSql = StrSql & "ClientID = '" & TxtClientID.Text & "' And ProposalNo = '" & TxtProposalNo.Text & "'"
        Else
            StrSql = "Select LeaseNo, CONVERT(Char,LeaseDate,103), ProposalNo, ProjectDesc, Zone, PropCode,Area,Unit,Amount, MonthlyRent From LeaseAgreement Where "
            StrSql = StrSql & "ClientID = '" & TxtClientID.Text & "'"
        End If
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Dim da As New OleDb.OleDbDataAdapter(Cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFindLease.DataSource = ds
        GdvFindLease.DataBind()
    End Sub

    Protected Sub ButFindCar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindCar.Click
        DragPanel.Style.Item("display") = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub UpdateCar()
        Dim CmdCar As New OleDb.OleDbCommand
        Dim RdrCar As OleDb.OleDbDataReader
        CmdCar.Connection = Con
        CmdCar.CommandText = "SELECT CarTariff.ClientID, Client.Name, CarTariff.Fortnight  As FirstFortnight, CarTariff.AddRate As AdditionalRatePerDay, FreeFolkLift, FreeDaysContainer_20ft, FreeDaysContainer_40ft FROM CarTariff, Client WHERE CarTariff.ClientID =  Client.ClientID"
        RdrCar = CmdCar.ExecuteReader
        GdvCar.DataSource = RdrCar
        GdvCar.DataBind()
        RdrCar.Close()
    End Sub

    Protected Sub ButCarSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCarSave.Click
        lblCarError.Visible = False

        If TxtCarClientID.Text = "" Then
            lblCarError.Text = "Please Select Client To Save Rate"
            lblCarError.Visible = True
            Exit Sub
        End If

        Cmd.Connection = Con
        StrSql = "SELECT COUNT(*) FROM CarTariff WHERE ClientID = '" & TxtCarClientID.Text & "' "
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar > 0 Then
            StrSql = "UPDATE CarTariff SET Fortnight = " & Val(TxtCar15.Text) & ","
            StrSql = StrSql & "AddRate = " & Val(TxtCarAdditional.Text) & ","
            StrSql = StrSql & "FreeFolkLift = " & Val(TxtCarFreeForkLift.Text) & ","
            StrSql = StrSql & "FreeDaysContainer_20ft = " & Val(TxtFreeContPerDay20.Text) & ","
            StrSql = StrSql & "FreeDaysContainer_40ft = " & Val(TxtFreeContPerDay40.Text) & ""
            StrSql = StrSql & " Where ClientID = '" & TxtCarClientID.Text & "'"
        Else
            StrSql = "INSERT INTO CarTariff (ClientID, Fortnight, AddRate, FreeFolkLift, FreeDaysContainer_20ft, FreeDaysContainer_20ft) VALUES('"
            StrSql = StrSql & TxtCarClientID.Text & "',"
            StrSql = StrSql & Val(TxtCar15.Text) & ","
            StrSql = StrSql & Val(TxtCarAdditional.Text) & ","
            StrSql = StrSql & Val(TxtCarFreeForkLift.Text) & ","
            StrSql = StrSql & Val(TxtFreeContPerDay20.Text) & ","
            StrSql = StrSql & Val(TxtFreeContPerDay40.Text) & ")"
        End If
        Try
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            TxtCarClientID.Text = ""
            TxtCarClientName.Text = ""
            TxtCar15.Text = ""
            TxtCarAdditional.Text = ""
            TxtCarFreeForkLift.Text = ""
            TxtFreeContPerDay20.Text = ""
            TxtFreeContPerDay40.Text = ""
            UpdateCar()
        Catch ex As Exception
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Saving','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try

    End Sub

    Protected Sub ButCarDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCarDelete.Click
        Cmd.Connection = Con
        Cmd.CommandText = "DELETE FROM CarTariff WHERE ClientID = '" & TxtCarClientID.Text & "'"
        Try
            Cmd.ExecuteNonQuery()
            TxtPropertyCode.Text = ""
            TxtCarClientID.Text = ""
            TxtCarClientName.Text = ""
            TxtCar15.Text = ""
            TxtCarAdditional.Text = ""
            TxtCarFreeForkLift.Text = ""
            TxtFreeContPerDay20.Text = ""
            TxtFreeContPerDay40.Text = ""
            UpdateCar()
        Catch ex As Exception
            Dim StrScript As String = "<script language='javascript'> w.Alert('" & ex.Message.Replace("'", "") & "','Error Deleting','images/Utility.ico');</script>"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
        End Try
    End Sub

    Private Sub GdvCar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvCar.SelectedIndexChanged
        Dim CmdCarClient As New OleDb.OleDbCommand
        CmdCarClient.Connection = Con
        CmdCarClient.CommandText = "Select [Name] From Client Where ClientID = '" & GdvCar.SelectedRow.Cells(0).Text.Replace("&nbsp;", "") & "'"
        TxtCarClientID.Text = GdvCar.SelectedRow.Cells(0).Text.Replace("&nbsp;", "")
        TxtCarClientName.Text = CmdCarClient.ExecuteScalar
        TxtCar15.Text = GdvCar.SelectedRow.Cells(2).Text.Replace("&nbsp;", "")
        TxtCarAdditional.Text = GdvCar.SelectedRow.Cells(3).Text.Replace("&nbsp;", "")
        TxtCarFreeForkLift.Text = GdvCar.SelectedRow.Cells(4).Text.Replace("&nbsp;", "")
        TxtFreeContPerDay20.Text = GdvCar.SelectedRow.Cells(5).Text.Replace("&nbsp;", "")
        TxtFreeContPerDay40.Text = GdvCar.SelectedRow.Cells(6).Text.Replace("&nbsp;", "")
    End Sub

    Protected Sub ButSaveTariff_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSaveTariff.Click
        Dim CmdTariff As New OleDb.OleDbCommand
        CmdTariff.Connection = Con
        For Each row As GridViewRow In GdvTariffCard.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim ChkBox As CheckBox = DirectCast(row.FindControl("ChkSelectTariff"), CheckBox)
                If ChkBox.Checked Then
                    CmdTariff.CommandText = "Delete From ClientTariff Where ClientID = '" & TxtTClientID.Text & "' And ChCode = '" & row.Cells(1).Text & "'"
                    CmdTariff.ExecuteNonQuery()
                    StrSql = "INSERT INTO ClientTariff (ClientID,ChCode) VALUES('"
                    StrSql = StrSql & TxtTClientID.Text & "','"
                    StrSql = StrSql & row.Cells(1).Text & "')"
                    CmdTariff.CommandText = StrSql
                    CmdTariff.ExecuteNonQuery()
                Else
                    CmdTariff.CommandText = "Delete From ClientTariff Where ClientID = '" & TxtTClientID.Text & "' And ChCode = '" & row.Cells(1).Text & "'"
                    CmdTariff.ExecuteNonQuery()
                End If
            End If
        Next
        LblRenewal.Visible = True
        LblRenewal.Text = "Tariff Client Save Sucessfully"
        TxtChCode.Text = ""
        CmbChType.SelectedIndex = -1
        CmbChargeFPZone.SelectedIndex = -1
        TxtChDescription.Text = ""
        CmbCondition.SelectedIndex = -1
        CmbDaysApp.SelectedIndex = -1
        TxtFromTime.Text = ""
        TxtToTime.Text = ""
        TxtChargeRate.Text = ""
        TxtChargeUnit.Text = ""
        TxtPerChDays.Text = ""
        TxtMinCharge.Text = ""
        TxtMinUnit.Text = ""
        ChkContainerStorage.Checked = False
        GetTariffCard()
    End Sub

    Protected Sub ButDeleteLease_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButDeleteLease.Click
        Dim CmdDeleteLease As New OleDb.OleDbCommand
        CmdDeleteLease.Connection = Con
        CmdDeleteLease.CommandText = "Delete From LeaseAgreement Where LeaseNo  = '" & CmbLeaseNo.Text & "' And ClientID  = '" & TxtLeaseClient.Text & "'"
        CmdDeleteLease.ExecuteNonQuery()
        CmbLeaseNo.SelectedIndex = 0
        TxtLeaseDate.Text = ""
        TxtLeaseClient.Text = ""
        TxtLeaseName.Text = ""
        TxtLeasePropNo.Text = ""
        TxtProposalDate.Text = ""
        TxtPropRevision.Text = ""
        CmbLeaseZone.SelectedIndex = 0
        TxtLeaseBy.Text = ""
        TxtLeaseValidity.Text = ""
        TxtFittedPeriod.Text = ""
        TxtLeaseReceived.Text = ""
        TxtLeaseFpZone.Text = ""
        TxtLeaseReceived.Text = ""
        TxtLeasePropertyCode.Text = ""
        TxtLeasePropDesc.Text = ""
        TxtLeasePropType.Text = ""
        TxtLeaseArea.Text = ""
        TxtLeaseRate.Text = ""
        TxtLeaseSCharge.Text = ""
        TxtLeaseInsurance.Text = ""
        TxtLeaseDeposit.Text = ""
        TxtLeaseAmount.Text = ""
        TxtBGAmount.Text = ""
        TxtEscalation.Text = ""
        TxtLeaseMonthlyCharge.Text = ""
        TxtBGStartDate.Text = ""
        TxtBGEndDate.Text = ""
        TxtBillingFrom.Text = ""
        TxtBillingTo.Text = ""
    End Sub

    Protected Sub TxtLeaseDeposit_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtLeaseDeposit.TextChanged
        TxtDepositAmt.Text = Format(Val(TxtLeaseDeposit.Text), "0.00")
    End Sub

    Protected Sub ButPrintMoveOut_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrintMoveOut.Click
        Dim StrSelFormula As String
        Session("ReportFile") = "RptMoveOut.rpt"
        Session("Period") = ""
        Session("Zone") = ""
        StrSelFormula = "{LeaseAgreement.LeaseNo} ='" & CmbLeaseNo.Text & "'"
        Session("SelFormula") = StrSelFormula
        Session("RepHead") = "Move Out for " & TxtPClientName.Text
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
    End Sub

    Protected Sub DelProposal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles DelProposal.Click
        Cmd.Connection = Con
        Cmd.CommandText = "Delete From Proposal Where ProposalNo = '" & TxtProposalNo.Text & "'"
        Cmd.ExecuteNonQuery()
        TxtProposalNo.Text = ""
        TxtPropDate.Text = ""
        TxtRevision.Text = ""
        TxtFollowUp.Text = ""
        CmbProposalFPZone.SelectedIndex = 0
        TxtValidity.Text = ""
        TxtPreparedBy.Text = ""
        TxtPropRemarks.Text = ""
        TxtZoneName.Text = ""
        TxtArea.Text = ""
        TxtInsurance.Text = ""
        TxtRate.Text = ""
        TxtDeposit.Text = ""
        TxtSCharges.Text = ""
        TxtAmount.Text = ""
        TxtPropMonthlyRent.Text = ""
        Dim StrScript As String = "<script language='javascript'> w.Alert('Proposal Deleted','Delete','images/Utility.ico');</script>"
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "ErrMsg", StrScript, False)
    End Sub

    Protected Sub btnSaveSD_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveSD.Click
        CmdSDTariff.Connection = Con
        StrSql = "UPDATE SDTariff SET TariffValue= " & txtSDGateIn.Text & " WHERE TariffID='SDGateIn'"
        CmdSDTariff.CommandText = StrSql
        CmdSDTariff.ExecuteNonQuery()

        StrSql = "UPDATE SDTariff SET TariffValue= " & txtSDGateOut.Text & " WHERE TariffID='SDGateOut'"
        CmdSDTariff.CommandText = StrSql
        CmdSDTariff.ExecuteNonQuery()

        StrSql = "UPDATE SDTariff SET TariffValue= " & txtSDFullCar.Text & " WHERE TariffID='SDFullCar'"
        CmdSDTariff.CommandText = StrSql
        CmdSDTariff.ExecuteNonQuery()

        StrSql = "UPDATE SDTariff SET TariffValue= " & txtSDHalfCar.Text & " WHERE TariffID='SDHalfCar'"
        CmdSDTariff.CommandText = StrSql
        CmdSDTariff.ExecuteNonQuery()

        StrSql = "UPDATE SDTariff SET TariffValue= " & txtSDFullDblCar.Text & " WHERE TariffID='SDFullDblCar'"
        CmdSDTariff.CommandText = StrSql
        CmdSDTariff.ExecuteNonQuery()

        StrSql = "UPDATE SDTariff SET TariffValue= " & txtSDHalfDblCar.Text & " WHERE TariffID='SDHalfDblCar'"
        CmdSDTariff.CommandText = StrSql
        CmdSDTariff.ExecuteNonQuery()

        StrSql = "UPDATE SDTariff SET TariffValue= " & txtSDFullZPC.Text & " WHERE TariffID='SDFullZPC'"
        CmdSDTariff.CommandText = StrSql
        CmdSDTariff.ExecuteNonQuery()

        StrSql = "UPDATE SDTariff SET TariffValue= " & txtSDHalfZPC.Text & " WHERE TariffID='SDHalfZPC'"
        CmdSDTariff.CommandText = StrSql
        CmdSDTariff.ExecuteNonQuery()
    End Sub

    Protected Sub btnRefreshSD_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRefreshSD.Click
        'Dim CmdSDTariff As New OleDb.OleDbCommand
        'Dim RdrSDTariff As OleDb.OleDbDataReader
        CmdSDTariff.Connection = Con
        StrSql = "SELECT * FROM SDTariff"
        CmdSDTariff.CommandText = StrSql
        RdrSDTariff = CmdSDTariff.ExecuteReader
        While RdrSDTariff.Read
            If RdrSDTariff("TariffID") = "SDGateIn" Then
                txtSDGateIn.Text = RdrSDTariff("TariffValue")
            End If
            If RdrSDTariff("TariffID") = "SDGateOut" Then
                txtSDGateOut.Text = RdrSDTariff("TariffValue")
            End If
            If RdrSDTariff("TariffID") = "SDFullCar" Then
                txtSDFullCar.Text = RdrSDTariff("TariffValue")
            End If
            If RdrSDTariff("TariffID") = "SDHalfCar" Then
                txtSDHalfCar.Text = RdrSDTariff("TariffValue")
            End If
            If RdrSDTariff("TariffID") = "SDFullDblCar" Then
                txtSDFullDblCar.Text = RdrSDTariff("TariffValue")
            End If
            If RdrSDTariff("TariffID") = "SDHalfDblCar" Then
                txtSDHalfDblCar.Text = RdrSDTariff("TariffValue")
            End If
            If RdrSDTariff("TariffID") = "SDFullZPC" Then
                txtSDFullZPC.Text = RdrSDTariff("TariffValue")
            End If
            If RdrSDTariff("TariffID") = "SDHalfZPC" Then
                txtSDHalfZPC.Text = RdrSDTariff("TariffValue")
            End If
        End While
        RdrSDTariff.Close()
    End Sub
End Class