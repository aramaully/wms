﻿Public Class FileViewer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Context.Response.Clear()
        Dim file As IO.FileInfo = New IO.FileInfo(Session("Output"))
        Context.Response.ContentType = "Application/msword"
        Context.Response.AppendHeader("Content-Disposition", "inline; filename=" + file.Name)
        Context.Response.AppendHeader("Content-Length", file.Length.ToString())
        Context.Response.WriteFile(file.FullName)
        Context.Response.Flush()
        IO.File.Delete(Session("Output"))
        Context.Response.End()
    End Sub

End Class