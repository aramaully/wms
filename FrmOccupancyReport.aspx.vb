﻿Partial Public Class FrmOccupancyReport
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection

    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader

    Dim CmdInvoice As New OleDb.OleDbCommand
    Dim RdrInvoice As OleDb.OleDbDataReader

    Dim CmdBillingProp As New OleDb.OleDbCommand
    Dim RdrBillingProp As OleDb.OleDbDataReader

    Dim CmdProperty As New OleDb.OleDbCommand
    Dim RdrProperty As OleDb.OleDbDataReader

    Dim CmdBilling As New OleDb.OleDbCommand
    Dim RdrBilling As OleDb.OleDbDataReader

    Dim CmdLogistics As New OleDb.OleDbCommand
    Dim RdrLogistics As OleDb.OleDbDataReader

    Dim CmdContDetail As New OleDb.OleDbCommand
    Dim RdrContDetail As OleDb.OleDbDataReader

    Dim CmdOccupancyReport As New OleDb.OleDbCommand
    Dim RdrOccupancyReport As OleDb.OleDbDataReader

    Dim StrSql As String
    Dim StrSel As String
    Dim StrSelect As String
    Dim StrFrom As String
    Dim StrWhere As String

    Dim SelectedZone As String
    Dim SelectedYear As Integer
    Dim SelectedMonth As String
    Dim SelectedCategory As String
    Dim FileName As String
    Dim IsRental As Boolean

    Dim Header1 As String = "BPML FREEPORT SERVICES LTD"
    Dim Header2 As String
    Dim Header3 As String

    Const MENUID = "OccupancyReport"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            'Select current month
            CmbMonth.SelectedIndex = Date.Today.Month - 1

            'Set this year
            TxtYear.Text = Date.Today.Year

            'Load Warehouse Codes
            Cmd.CommandText = "SELECT Whcode FROM [System]"
            Rdr = Cmd.ExecuteReader
            Me.CmbZone.Items.Add("Select...")
            While Rdr.Read
                Me.CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Me.CmbZone.SelectedIndex = 0
            Rdr.Close()

            'Load Invoice Type
            Me.CmbType.Items.Add("Select...")
            Me.CmbType.Items.Add("Rental")
            Me.CmbType.Items.Add("Other Services")
            Me.CmbType.SelectedIndex = 0

            'Load Combobox Category
            Me.CmbCategory.Items.Add("Freeport")
            Me.CmbCategory.Items.Add("Non-Freeport")
            Me.CmbCategory.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Populate" Then
            Populate()
        End If
        If e.Item.CommandName = "Pdf" Then
            View_Pdf()
        End If
        If e.Item.CommandName = "AllExcel" Then
            'All columns from database
            PrintToExcel()
        End If
        If e.Item.CommandName = "CustomiseExcel" Then
            'Customised columns from database
            Export2Excel()
        End If
    End Sub

    Public Sub Populate()
        Try
            SelectedZone = Me.CmbZone.Text
            SelectedYear = Int(Me.TxtYear.Text)
            SelectedMonth = Me.CmbMonth.Text

            'Validation
            LblResult.Visible = False
            LblResult.Text = ""
            LblResult.ForeColor = Drawing.Color.Red
            If SelectedZone = "Select..." Then
                LblResult.Visible = True
                LblResult.Text = "Please select Zone!"
                Exit Sub
            ElseIf SelectedZone <> "FPZ6SD" Then
                If Me.CmbType.Text = "Rental" Then
                    IsRental = True
                ElseIf Me.CmbType.Text = "Other Services" Then
                    IsRental = False
                Else
                    LblResult.Visible = True
                    LblResult.Text = "Please select type to populate data"
                    Exit Sub
                End If
            End If

            'Car Bonded Yard
            If SelectedZone = "FPZ6SD" Then
                PopulateCarBondedYard()
            End If

            'Rental for Zones 1 & 6
            If ((SelectedZone = "FPZ01" Or SelectedZone = "FPZ06") And IsRental) Then
                PopulateRental()
            End If

            'Other Services for Zones 1 & 6
            If ((SelectedZone = "FPZ01" Or SelectedZone = "FPZ06") And Not IsRental) Then
                PopulateOtherServices()
            End If
            LblResult.ForeColor = Drawing.Color.Green
            LblResult.Text = "Populate Data success!"
            LblResult.Visible = True

        Catch ex As Exception
            MsgBox("Poplulate Data: " & ex.Message)
        End Try
    End Sub

    Public Sub PopulateCarBondedYard()
        Try
            Cmd.Connection = Con
            CmdInvoice.Connection = Con
            CmdBillingProp.Connection = Con
            CmdProperty.Connection = Con
            CmdBilling.Connection = Con
            CmdLogistics.Connection = Con
            CmdOccupancyReport.Connection = Con
            CmdContDetail.Connection = Con

            'Clean OccupancyReport
            Cmd.CommandText = "DELETE FROM OccupancyReport WHERE WhCode='FPZ6SD' AND Month='" & SelectedMonth & "' AND Year='" & SelectedYear & "' AND IsRental='No'"
            Cmd.ExecuteNonQuery()

            'Verify if OccupancyReport exist
            StrSql = "SELECT COUNT(*) FROM OccupancyReport"
            StrSql = StrSql & " WHERE Year='" & SelectedYear & "'"
            StrSql = StrSql & " AND Month='" & SelectedMonth & "'"
            StrSql = StrSql & " AND WhCode='" & SelectedZone & "'"
            Cmd.CommandText = StrSql
            If Not Cmd.ExecuteScalar > 0 Then
                'Read Invoices
                StrSql = "SELECT * FROM Invoice"
                StrSql = StrSql & " WHERE Year=" & SelectedYear & ""
                StrSql = StrSql & " AND Month='" & SelectedMonth & "'"
                StrSql = StrSql & " AND WhCode='" & SelectedZone & "'"
                StrSql = StrSql & " AND InvoiceNo < 8800000"
                StrSql = StrSql & " ORDER BY ClientID"
                CmdInvoice.CommandText = StrSql
                RdrInvoice = CmdInvoice.ExecuteReader
                While RdrInvoice.Read
                    Dim DblYard = 0.00
                    Dim DblGateInGateOut = 0.00
                    Dim DblConstructive = 0.00
                    Dim DblSupervision = 0.00
                    Dim DblGroupage = 0.00
                    Dim DblTransitContainers = 0.00
                    'Read Billings
                    StrSql = "SELECT * FROM Billing"
                    StrSql = StrSql & " WHERE Year=" & SelectedYear & ""
                    StrSql = StrSql & " AND Month='" & SelectedMonth & "'"
                    StrSql = StrSql & " AND WhCode='" & SelectedZone & "'"
                    StrSql = StrSql & " AND InvoiceNo=" & RdrInvoice("InvoiceNo") & ""
                    StrSql = StrSql & " ORDER BY Description"
                    CmdBilling.CommandText = StrSql
                    RdrBilling = CmdBilling.ExecuteReader
                    While RdrBilling.Read
                        If RdrBilling("Description").StartsWith("Storage") Then
                            DblYard += RdrBilling("Amount")
                        End If
                        If RdrBilling("Description").StartsWith("Gate") Then
                            DblGateInGateOut += RdrBilling("Amount")
                        End If
                        If RdrBilling("Description") = "Other Charges" Then
                            'Read Logistics
                            StrSql = "SELECT * FROM Logistics"
                            StrSql = StrSql & " WHERE WhCode='" & SelectedZone & "'"
                            StrSql = StrSql & " AND InvoiceNo=" & RdrBilling("InvoiceNo") & ""
                            StrSql = StrSql & " AND ClientID='" & RdrBilling("ClientID") & "'"
                            StrSql = StrSql & " ORDER BY Description"
                            CmdLogistics.CommandText = StrSql
                            RdrLogistics = CmdLogistics.ExecuteReader
                            While RdrLogistics.Read
                                If RdrLogistics("Description").StartsWith("Constructive") Then
                                    DblConstructive += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Supervision") Then
                                    DblSupervision += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Consolidated Groupage") Then
                                    DblGroupage += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Transit") Then
                                    DblTransitContainers += RdrLogistics("Amount")
                                End If

                            End While
                            RdrLogistics.Close()
                        End If
                    End While
                    RdrBilling.Close()

                    'Write OccupancyReport
                    StrSql = "INSERT INTO OccupancyReport"
                    StrSql = StrSql & "(InvoiceNo, InvDate, ClientID, WhCode, Month, Year, IsRental, Yard, GateInGateOut, Constructive, Supervision, TransitContainers, Groupage)"
                    StrSql = StrSql & "VALUES("
                    StrSql = StrSql & "" & RdrInvoice("InvoiceNo") & ", "
                    StrSql = StrSql & "'" & Format(RdrInvoice("InvDate"), "yyyy-MM-dd") & "', "
                    StrSql = StrSql & "'" & RdrInvoice("ClientID") & "', "
                    StrSql = StrSql & "'" & RdrInvoice("WhCode") & "', "
                    StrSql = StrSql & "'" & RdrInvoice("Month") & "', "
                    StrSql = StrSql & "" & RdrInvoice("Year") & ", "
                    StrSql = StrSql & "'" & "No" & "', "
                    StrSql = StrSql & "" & DblYard & ", "
                    StrSql = StrSql & "" & DblGateInGateOut & ", "
                    StrSql = StrSql & "" & DblConstructive & ", "
                    StrSql = StrSql & "" & DblSupervision & ", "
                    StrSql = StrSql & "" & DblTransitContainers & ", "
                    StrSql = StrSql & "" & DblGroupage & ""
                    StrSql = StrSql & ")"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End While
                RdrInvoice.Close()
            End If
        Catch ex As Exception
            MsgBox("FPZ6SD: " & ex.Message)
        End Try
    End Sub

    Public Sub PopulateRental()
        Try
            Cmd.Connection = Con
            CmdInvoice.Connection = Con
            CmdBillingProp.Connection = Con
            CmdProperty.Connection = Con
            CmdBilling.Connection = Con
            CmdLogistics.Connection = Con
            CmdOccupancyReport.Connection = Con
            CmdContDetail.Connection = Con

            'Clean OccupancyReport
            Cmd.CommandText = "DELETE FROM OccupancyReport WHERE WhCode='" & SelectedZone & "' AND Month='" & SelectedMonth & "' AND Year='" & SelectedYear & "' AND IsRental='Yes'"
            Cmd.ExecuteNonQuery()

            'Verify if OccupancyReport exist
            StrSql = "SELECT COUNT(*) FROM OccupancyReport"
            StrSql = StrSql & " WHERE Year='" & SelectedYear & "'"
            StrSql = StrSql & " AND Month='" & SelectedMonth & "'"
            StrSql = StrSql & " AND WhCode='" & SelectedZone & "'"
            StrSql = StrSql & " AND IsRental='" & "Yes" & "'"
            Cmd.CommandText = StrSql
            If Not Cmd.ExecuteScalar > 0 Then
                'Read Invoices
                StrSql = "SELECT * FROM Invoice"
                StrSql = StrSql & " WHERE Year=" & SelectedYear & ""
                StrSql = StrSql & " AND Month='" & SelectedMonth & "'"
                StrSql = StrSql & " AND WhCode='" & SelectedZone & "'"
                StrSql = StrSql & " AND InvoiceNo < 8800000"
                StrSql = StrSql & " ORDER BY ClientID"
                CmdInvoice.CommandText = StrSql
                RdrInvoice = CmdInvoice.ExecuteReader
                While RdrInvoice.Read
                    'Read distinct Property code
                    StrSql = "SELECT DISTINCT(PropCode) FROM Billing"
                    StrSql = StrSql & " WHERE Year=" & SelectedYear & ""
                    StrSql = StrSql & " AND Month='" & SelectedMonth & "'"
                    StrSql = StrSql & " AND WhCode='" & SelectedZone & "'"
                    StrSql = StrSql & " AND InvoiceNo=" & RdrInvoice("InvoiceNo") & ""
                    StrSql = StrSql & " AND Unit='Cabin'"
                    CmdBillingProp.CommandText = StrSql
                    RdrBillingProp = CmdBillingProp.ExecuteReader
                    While RdrBillingProp.Read
                        'Read Property details
                        Dim PropertyCode = RdrBillingProp("PropCode")
                        Dim RentalComments = ""
                        Dim PropertyType = ""
                        StrSql = "SELECT PropertyCode, Area, Unit, [Type] AS PropertyType FROM Property WHERE PropertyCode='" & PropertyCode & "'"
                        CmdProperty.CommandText = StrSql
                        RdrProperty = CmdProperty.ExecuteReader
                        While RdrProperty.Read
                            RentalComments = RdrProperty("PropertyCode") & " - " & RdrProperty("Area").ToString & " " & RdrProperty("Unit")
                            PropertyType = RdrProperty("PropertyType")
                        End While
                        RdrProperty.Close()

                        'Read Billings for selected property
                        StrSql = "SELECT * FROM Billing"
                        StrSql = StrSql & " WHERE Year=" & SelectedYear & ""
                        StrSql = StrSql & " AND Month='" & SelectedMonth & "'"
                        StrSql = StrSql & " AND WhCode='" & SelectedZone & "'"
                        StrSql = StrSql & " AND InvoiceNo=" & RdrInvoice("InvoiceNo") & ""
                        StrSql = StrSql & " AND PropCode='" & PropertyCode & "'"
                        CmdBilling.CommandText = StrSql
                        RdrBilling = CmdBilling.ExecuteReader
                        Dim CarPark = 0.00
                        Dim Office = 0.00
                        Dim Warehouse = 0.00
                        Dim YardRental = 0.00
                        Dim Others = 0.00
                        Dim ServiceCharge = 0.00
                        While RdrBilling.Read
                            'If RdrBilling("Description").StartsWith("RENTAL") Or RdrBilling("Description").StartsWith("USE AND OCCUPATION") Then
                            '
                            'End If
                            If RdrBilling("Description").StartsWith("Service Charge") Then
                                ServiceCharge += RdrBilling("Amount")
                            Else
                                'Rental / Use and Occupation / ...
                                Select Case PropertyType
                                    Case "Car Park"
                                        CarPark += RdrBilling("Amount")
                                    Case "Office"
                                        Office += RdrBilling("Amount")
                                    Case "Warehouse"
                                        Warehouse += RdrBilling("Amount")
                                    Case "Yard"
                                        YardRental += RdrBilling("Amount")
                                    Case Else
                                        Others += RdrBilling("Amount")
                                End Select
                            End If
                        End While
                        RdrBilling.Close()

                        'Write OccupancyReport
                        StrSql = "INSERT INTO OccupancyReport"
                        StrSql = StrSql & "(InvoiceNo, InvDate, ClientID, WhCode, Month, Year, IsRental, Comments, CarPark, Office, Warehouse, YardRental, Others, ServiceCharge)"
                        StrSql = StrSql & "VALUES("
                        StrSql = StrSql & "" & RdrInvoice("InvoiceNo") & ", "
                        StrSql = StrSql & "'" & Format(RdrInvoice("InvDate"), "yyyy-MM-dd") & "', "
                        StrSql = StrSql & "'" & RdrInvoice("ClientID") & "', "
                        StrSql = StrSql & "'" & RdrInvoice("WhCode") & "', "
                        StrSql = StrSql & "'" & RdrInvoice("Month") & "', "
                        StrSql = StrSql & "" & RdrInvoice("Year") & ", "
                        StrSql = StrSql & "'" & "Yes" & "', "
                        StrSql = StrSql & "'" & RentalComments & "', "
                        StrSql = StrSql & "" & CarPark & ", "
                        StrSql = StrSql & "" & Office & ", "
                        StrSql = StrSql & "" & Warehouse & ", "
                        StrSql = StrSql & "" & YardRental & ", "
                        StrSql = StrSql & "" & Others & ", "
                        StrSql = StrSql & "" & ServiceCharge & ""
                        StrSql = StrSql & ")"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End While
                    RdrBillingProp.Close()
                End While
                RdrInvoice.Close()
            End If
        Catch ex As Exception
            MsgBox("Rental: " & ex.Message)
        End Try
    End Sub

    Public Sub PopulateOtherServices()
        Try
            Cmd.Connection = Con
            CmdInvoice.Connection = Con
            CmdBillingProp.Connection = Con
            CmdProperty.Connection = Con
            CmdBilling.Connection = Con
            CmdLogistics.Connection = Con
            CmdOccupancyReport.Connection = Con
            CmdContDetail.Connection = Con

            'Clean OccupancyReport
            Cmd.CommandText = "DELETE FROM OccupancyReport WHERE WhCode='" & SelectedZone & "' AND Month='" & SelectedMonth & "' AND Year='" & SelectedYear & "' AND IsRental='No'"
            Cmd.ExecuteNonQuery()

            'Verify if OccupancyReport exist
            StrSql = "SELECT COUNT(*) FROM OccupancyReport"
            StrSql = StrSql & " WHERE Year='" & SelectedYear & "'"
            StrSql = StrSql & " AND Month='" & SelectedMonth & "'"
            StrSql = StrSql & " AND WhCode='" & SelectedZone & "'"
            StrSql = StrSql & " AND IsRental='" & "No" & "'"
            Cmd.CommandText = StrSql
            If Not Cmd.ExecuteScalar > 0 Then
                'Read Invoices
                StrSql = "SELECT * FROM Invoice"
                StrSql = StrSql & " WHERE Year=" & SelectedYear & ""
                StrSql = StrSql & " AND Month='" & SelectedMonth & "'"
                StrSql = StrSql & " AND WhCode='" & SelectedZone & "'"
                StrSql = StrSql & " AND InvoiceNo < 8800000"
                StrSql = StrSql & " ORDER BY ClientID"
                CmdInvoice.CommandText = StrSql
                RdrInvoice = CmdInvoice.ExecuteReader
                While RdrInvoice.Read
                    Dim ContainerEntryFee = 0.00 'done
                    Dim Reefer20 = 0.00
                    Dim Reefer40 = 0.00
                    Dim TransitContainers = 0.00
                    Dim Groupage = 0.00
                    Dim ForkliftHandling = 0.00
                    Dim Supervision = 0.00
                    Dim TransshipmentLCL = 0.00
                    Dim CrossDock20 = 0.00
                    Dim CrossDock40 = 0.00
                    Dim CrossDockEntryFee = 0.00
                    Dim Security = 0.00
                    Dim LoadUnload20 = 0.00
                    Dim LoadUnload40 = 0.00
                    Dim StorageHeavy = 0.00
                    Dim Parking20 = 0.00
                    Dim Parking40 = 0.00
                    Dim StorageGoods = 0.00
                    Dim AccessFee = 0.00

                    Dim WriteOccupancy = False

                    'Read Billings
                    StrSql = "SELECT * FROM Billing"
                    StrSql = StrSql & " WHERE Year=" & SelectedYear & ""
                    StrSql = StrSql & " AND Month='" & SelectedMonth & "'"
                    StrSql = StrSql & " AND WhCode='" & SelectedZone & "'"
                    StrSql = StrSql & " AND InvoiceNo=" & RdrInvoice("InvoiceNo") & ""
                    StrSql = StrSql & " AND Unit<>'Cabin'"
                    CmdBilling.CommandText = StrSql
                    RdrBilling = CmdBilling.ExecuteReader
                    While RdrBilling.Read
                        WriteOccupancy = True
                        If RdrBilling("Description") = "Other Charges" Then
                            'Read Logistics
                            StrSql = "SELECT * FROM Logistics"
                            StrSql = StrSql & " WHERE WhCode='" & SelectedZone & "'"
                            StrSql = StrSql & " AND InvoiceNo=" & RdrBilling("InvoiceNo") & ""
                            StrSql = StrSql & " AND ClientID='" & RdrBilling("ClientID") & "'"
                            StrSql = StrSql & " ORDER BY Description"
                            CmdLogistics.CommandText = StrSql
                            RdrLogistics = CmdLogistics.ExecuteReader
                            While RdrLogistics.Read
                                If RdrLogistics("Description").StartsWith("Entry fee for consolidated Groupage Container - 20/40ft") Then
                                    ContainerEntryFee += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Entry Fee of Full/Empty container (20ft/40ft)") Then
                                    ContainerEntryFee += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Reefer - 20Ft") Then
                                    Reefer20 += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Reefer - 40Ft") Then
                                    Reefer40 += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Transit of container 20ft/40ft") Then
                                    TransitContainers += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Consolidated Groupage Container - 20/40ft") Then
                                    Groupage += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Forklift") Then
                                    ForkliftHandling += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Handling") Then
                                    ForkliftHandling += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Supervision") Then
                                    Supervision += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Transhipment of loose cargo/LCL") Then
                                    TransshipmentLCL += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Cross docking of 20ft Container") Then
                                    CrossDock20 += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Cross docking of 40ft Container") Then
                                    CrossDock40 += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Entry Fee for Cross Docking") Then
                                    CrossDockEntryFee += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Security Charges") Then
                                    Security += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Loading and unloading of lorries/trucks/equipment-20ft") Then
                                    LoadUnload40 += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Loading and unloading of lorries/trucks/equipment-40ft") Then
                                    LoadUnload40 += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Storage of heavy equipment and materials (crane, tractor etc)") Then
                                    StorageHeavy += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Parking of 20ft Trailer") Then
                                    Parking20 += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Parking of 40ft Trailer") Then
                                    Parking40 += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Storage of goods outside the leased premises") Then
                                    StorageGoods += RdrLogistics("Amount")
                                End If
                                If RdrLogistics("Description").StartsWith("Access Fee") Then
                                    AccessFee += RdrLogistics("Amount")
                                End If
                            End While
                            RdrLogistics.Close()
                        End If
                    End While
                    RdrBilling.Close()

                    'Storage of Containers
                    Dim Container20Tenant = 0.00
                    Dim Container20NonTenant = 0.00
                    Dim Container40Tenant = 0.00
                    Dim Container40NonTenant = 0.00

                    'Read ContDetail
                    StrSql = "SELECT * FROM ContDetail"
                    StrSql = StrSql & " WHERE WhCode='" & SelectedZone & "'"
                    StrSql = StrSql & " AND InvoiceNo=" & RdrInvoice("InvoiceNo") & ""
                    CmdContDetail.CommandText = StrSql
                    RdrContDetail = CmdContDetail.ExecuteReader
                    While RdrContDetail.Read
                        WriteOccupancy = True
                        If RdrContDetail("Description").StartsWith("Storage of 20 ft Containers - Empty/Full (tenant)") Then
                            Container20Tenant += RdrContDetail("Amount")
                        End If
                        If RdrContDetail("Description").StartsWith("Storage of 20 ft Containers - Empty/Full (non tenant)") Then
                            Container20NonTenant += RdrContDetail("Amount")
                        End If
                        If RdrContDetail("Description").StartsWith("Storage of 40 ft Containers - Empty/Full (tenant)") Then
                            Container40Tenant += RdrContDetail("Amount")
                        End If
                        If RdrContDetail("Description").StartsWith("Storage of 40 ft Containers - Empty/Full (non tenant)") Then
                            Container40NonTenant += RdrContDetail("Amount")
                        End If
                    End While
                    RdrContDetail.Close()

                    If (WriteOccupancy) Then
                        'Write OccupancyReport
                        StrSql = "INSERT INTO OccupancyReport"
                        StrSql = StrSql & "(InvoiceNo, InvDate, ClientID, WhCode, Month, Year, IsRental, "
                        StrSql = StrSql & "Container20Tenant, Container20NTenant, Container40Tenant, Container40NTenant, ContainerEntryFee, Reefer20, Reefer40, "
                        StrSql = StrSql & "TransitContainers, Groupage, ForkliftHandling, Supervision, TransshipmentLCL, CrossDock20, CrossDock40, CrossDockEntryFee, "
                        StrSql = StrSql & "Security, LoadUnload20, LoadUnload40, StorageHeavy, Parking20, Parking40, StorageGoods, AccessFee)"
                        StrSql = StrSql & "VALUES("
                        StrSql = StrSql & "" & RdrInvoice("InvoiceNo") & ", "
                        StrSql = StrSql & "'" & Format(RdrInvoice("InvDate"), "yyyy-MM-dd") & "', "
                        StrSql = StrSql & "'" & RdrInvoice("ClientID") & "', "
                        StrSql = StrSql & "'" & RdrInvoice("WhCode") & "', "
                        StrSql = StrSql & "'" & RdrInvoice("Month") & "', "
                        StrSql = StrSql & "" & RdrInvoice("Year") & ", "
                        StrSql = StrSql & "'" & "No" & "', "

                        StrSql = StrSql & "" & Container20Tenant & ", "
                        StrSql = StrSql & "" & Container20NonTenant & ", "
                        StrSql = StrSql & "" & Container40Tenant & ", "
                        StrSql = StrSql & "" & Container40NonTenant & ", "
                        StrSql = StrSql & "" & ContainerEntryFee & ", "
                        StrSql = StrSql & "" & Reefer20 & ", "
                        StrSql = StrSql & "" & Reefer40 & ", "
                        StrSql = StrSql & "" & TransitContainers & ", "
                        StrSql = StrSql & "" & Groupage & ", "
                        StrSql = StrSql & "" & ForkliftHandling & ", "
                        StrSql = StrSql & "" & Supervision & ", "
                        StrSql = StrSql & "" & TransshipmentLCL & ", "
                        StrSql = StrSql & "" & CrossDock20 & ", "
                        StrSql = StrSql & "" & CrossDock40 & ", "
                        StrSql = StrSql & "" & CrossDockEntryFee & ", "
                        StrSql = StrSql & "" & Security & ", "
                        StrSql = StrSql & "" & LoadUnload20 & ", "
                        StrSql = StrSql & "" & LoadUnload40 & ", "
                        StrSql = StrSql & "" & StorageHeavy & ","
                        StrSql = StrSql & "" & Parking20 & ","
                        StrSql = StrSql & "" & Parking40 & ","
                        StrSql = StrSql & "" & StorageGoods & ","
                        StrSql = StrSql & "" & AccessFee & ""
                        StrSql = StrSql & ")"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If
                End While
                RdrInvoice.Close()
            End If
        Catch ex As Exception
            MsgBox("Other Services: " & ex.Message)
        End Try
    End Sub

    Public Sub View_Pdf()
        Try
            SelectedZone = Me.CmbZone.Text
            SelectedYear = Int(Me.TxtYear.Text)
            SelectedMonth = Me.CmbMonth.Text

            'Select Category
            SelectedCategory = Me.CmbCategory.Text


            'Validation
            LblResult.Visible = False
            LblResult.Text = ""
            LblResult.ForeColor = Drawing.Color.Red
            If SelectedZone = "Select..." Then
                LblResult.Visible = True
                LblResult.Text = "Please select Zone!"
                Exit Sub
            ElseIf SelectedZone <> "FPZ6SD" Then
                If Me.CmbType.Text = "Rental" Then
                    IsRental = True
                ElseIf Me.CmbType.Text = "Other Services" Then
                    IsRental = False
                Else
                    LblResult.Visible = True
                    LblResult.Text = "Please select type to view occupancy report"
                    Exit Sub
                End If
            End If

            'Car Bonded Yard
            If SelectedZone = "FPZ6SD" Then
                'PopulateCarBondedYard()
                'Load Crystal Report
                StrSel = ""
                StrSel = "{OccupancyReport.WhCode}= '" & SelectedZone & "'"
                StrSel = StrSel & " And {OccupancyReport.Year}='" & SelectedYear & "'"
                StrSel = StrSel & " And {OccupancyReport.Month}= '" & SelectedMonth & "'"
                Session("ReportFile") = "RptOccCBY.rpt"
                Session("Period") = ""
                Session("Zone") = ""
                Session("RepHead") = "Occupancy Report - Car Bonded Yard"
                Session("SelFormula") = StrSel
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
            End If

            'Rental for Zones 1 & 6
            If ((SelectedZone = "FPZ01" Or SelectedZone = "FPZ06") And IsRental) Then
                'PopulateRental()
                'Load Crystal Report
                StrSel = ""
                StrSel = "{OccupancyReport.WhCode}= '" & SelectedZone & "'"
                StrSel = StrSel & " And {OccupancyReport.Year}='" & SelectedYear & "'"
                StrSel = StrSel & " And {OccupancyReport.Month}= '" & SelectedMonth & "'"
                StrSel = StrSel & " And {OccupancyReport.IsRental}= '" & "Yes" & "'"
                If SelectedCategory <> "Freeport" Then
                    StrSel = StrSel & " And {Client.Category}<> '" & "Freeport" & "'"
                Else
                    StrSel = StrSel & " And {Client.Category}= '" & SelectedCategory & "'"
                End If

                Session("ReportFile") = "RptOccRental.rpt"
                Session("Period") = ""
                Session("Zone") = ""
                Session("RepHead") = "Occupancy Report - Rental"
                Session("SelFormula") = StrSel
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
            End If

            'Other Services for Zones 1 & 6
            If ((SelectedZone = "FPZ01" Or SelectedZone = "FPZ06") And Not IsRental) Then
                'PopulateOtherServices()
                'Load Crystal Report
                StrSel = ""
                StrSel = "{OccupancyReport.WhCode}= '" & SelectedZone & "'"
                StrSel = StrSel & " And {OccupancyReport.Year}='" & SelectedYear & "'"
                StrSel = StrSel & " And {OccupancyReport.Month}= '" & SelectedMonth & "'"
                StrSel = StrSel & " And {OccupancyReport.IsRental}= '" & "No" & "'"
                If SelectedCategory <> "Freeport" Then
                    StrSel = StrSel & " And {Client.Category}<> '" & "Freeport" & "'"
                Else
                    StrSel = StrSel & " And {Client.Category}= '" & SelectedCategory & "'"
                End If
                Session("ReportFile") = "RptOccOtherCharges.rpt"
                Session("Period") = ""
                Session("Zone") = ""
                Session("RepHead") = "Occupancy Report - Other Services"
                Session("SelFormula") = StrSel
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'All columns from database
    Public Sub PrintToExcel()
        Cmd.Connection = Con
        SelectedZone = Me.CmbZone.Text
        SelectedYear = Int(Me.TxtYear.Text)
        SelectedMonth = Me.CmbMonth.Text
        SelectedCategory = Me.CmbCategory.Text
        FileName = ""

        'Validation
        LblResult.Visible = False
        LblResult.Text = ""
        LblResult.ForeColor = Drawing.Color.Red
        If SelectedZone = "Select..." Then
            LblResult.Visible = True
            LblResult.Text = "Please select Zone!"
            Exit Sub
        ElseIf SelectedZone <> "FPZ6SD" Then
            If Me.CmbType.Text = "Rental" Then
                IsRental = True
                If SelectedCategory = "Freeport" Then
                    'Excluding VAT
                    StrSql = "SELECT 'Z'+SUBSTRING(OccupancyReport.WhCode,5,1)+'/'+OccupancyReport.InvoiceNo AS InvoiceNo,"
                    StrSql = StrSql & " Client.VatNo, Client.ClientID, Client.Name,"
                    StrSql = StrSql & " OccupancyReport.Comments,"
                    StrSql = StrSql & " OccupancyReport.CarPark,"
                    StrSql = StrSql & " OccupancyReport.Office,"
                    StrSql = StrSql & " OccupancyReport.Warehouse,"
                    StrSql = StrSql & " OccupancyReport.YardRental As Yard,"
                    StrSql = StrSql & " OccupancyReport.Others,"
                    StrSql = StrSql & " OccupancyReport.ServiceCharge,"
                    StrSql = StrSql & " (CarPark+Office+Warehouse+YardRental+Others+ServiceCharge) AS Amount ,"
                    StrSql = StrSql & " (CarPark+Office+Warehouse+YardRental+Others+ServiceCharge) AS NETAmount"
                    StrSql = StrSql & " FROM OccupancyReport, Client"
                    StrSql = StrSql & " WHERE OccupancyReport.ClientID = Client.ClientID"
                    StrSql = StrSql & " And OccupancyReport.Year=" & SelectedYear
                    StrSql = StrSql & " And OccupancyReport.Month='" & SelectedMonth & "'"
                    StrSql = StrSql & " AND OccupancyReport.WhCode='" & SelectedZone & "'"
                    StrSql = StrSql & " AND Client.Category='" & SelectedCategory & "'"
                    StrSql = StrSql & " AND OccupancyReport.IsRental='" & "Yes" & "'"

                    '#Caption
                    FileName = "Occupancy Report - Rental (Freeport) - " & SelectedMonth & " - " & SelectedYear
                    Header2 = "RENTAL"
                    Header3 = "FREEPORT " & SelectedZone & " - " & SelectedMonth & " " & SelectedYear
                Else
                    'Including VAT
                    StrSql = "SELECT 'Z'+SUBSTRING(OccupancyReport.WhCode,5,1)+'/'+OccupancyReport.InvoiceNo AS InvoiceNo,"
                    StrSql = StrSql & " Client.VatNo, Client.ClientID, Client.Name,"
                    StrSql = StrSql & " OccupancyReport.Comments,"
                    StrSql = StrSql & " OccupancyReport.CarPark,"
                    StrSql = StrSql & " OccupancyReport.Office,"
                    StrSql = StrSql & " OccupancyReport.Warehouse,"
                    StrSql = StrSql & " OccupancyReport.YardRental As Yard,"
                    StrSql = StrSql & " OccupancyReport.Others,"
                    StrSql = StrSql & " OccupancyReport.ServiceCharge,"
                    StrSql = StrSql & " (CarPark+Office+Warehouse+YardRental+Others+ServiceCharge) AS Amount ,"
                    StrSql = StrSql & " (CarPark+Office+Warehouse+YardRental+Others+ServiceCharge)*0.15 AS VAT ,"
                    StrSql = StrSql & " (CarPark+Office+Warehouse+YardRental+Others+ServiceCharge) + ((CarPark+Office+Warehouse+YardRental+Others+ServiceCharge)*0.15) AS NETAmount"
                    StrSql = StrSql & " FROM OccupancyReport, Client"
                    StrSql = StrSql & " WHERE OccupancyReport.ClientID = Client.ClientID"
                    StrSql = StrSql & " AND OccupancyReport.Year=" & SelectedYear
                    StrSql = StrSql & " AND OccupancyReport.Month='" & SelectedMonth & "'"
                    StrSql = StrSql & " AND OccupancyReport.WhCode='" & SelectedZone & "'"
                    StrSql = StrSql & " AND Client.Category<>'" & "Freeport" & "'"
                    StrSql = StrSql & " AND OccupancyReport.IsRental='" & "Yes" & "'"

                    '#Caption
                    FileName = "Occupancy Report - Rental (Non-Freeport) - " & SelectedMonth & " - " & SelectedYear
                    Header2 = "RENTAL"
                    Header3 = "NON FREEPORT " & SelectedZone & " - " & SelectedMonth & " " & SelectedYear
                End If
            ElseIf Me.CmbType.Text = "Other Services" Then
                IsRental = False
                If SelectedCategory = "Freeport" Then
                    'Excluding VAT
                    StrSql = "SELECT 'Z'+SUBSTRING(OccupancyReport.WhCode,5,1)+'/'+OccupancyReport.InvoiceNo AS InvoiceNo,"
                    StrSql = StrSql & " Client.VatNo, Client.ClientID, Client.Name,"
                    StrSql = StrSql & " OccupancyReport.Container20Tenant AS ContainerStorageL20Ft,"
                    StrSql = StrSql & " OccupancyReport.Container40Tenant AS ContainerStorageL40Ft,"
                    StrSql = StrSql & " OccupancyReport.Container20NTenant AS ContainerStorageNL20Ft,"
                    StrSql = StrSql & " OccupancyReport.Container40NTenant AS ContainerStorageNL40Ft,"
                    StrSql = StrSql & " OccupancyReport.ContainerEntryFee AS EntryFeeContainer20Ft40Ft,"
                    StrSql = StrSql & " OccupancyReport.Reefer20 AS Reefer20Ft,"
                    StrSql = StrSql & " OccupancyReport.Reefer40 AS Reefer40Ft,"
                    StrSql = StrSql & " OccupancyReport.TransitContainers AS TransitContainers20Ft40Ft,"
                    StrSql = StrSql & " OccupancyReport.Groupage AS ConsolidatedGroupage,"
                    StrSql = StrSql & " OccupancyReport.ForkliftHandling AS HandlingServices,"
                    StrSql = StrSql & " OccupancyReport.Supervision AS Supervision,"
                    StrSql = StrSql & " OccupancyReport.TransshipmentLCL AS TransshipmentLCL,"
                    StrSql = StrSql & " OccupancyReport.CrossDock20 AS CrossDocking20Ft,"
                    StrSql = StrSql & " OccupancyReport.CrossDock40 AS CrossDocking40Ft,"
                    StrSql = StrSql & " OccupancyReport.CrossDockEntryFee AS EntryFeeCD20Ft40Ft,"
                    StrSql = StrSql & " OccupancyReport.Security AS SecurityServices,"
                    StrSql = StrSql & " OccupancyReport.LoadUnload20 AS LoadingUnloading20Ft,"
                    StrSql = StrSql & " OccupancyReport.LoadUnload40 AS LoadingUnloading40Ft,"
                    StrSql = StrSql & " OccupancyReport.StorageHeavy AS StorageOfHeavyEquipment,"
                    StrSql = StrSql & " OccupancyReport.Parking20 AS ParkingTrailer20Ft,"
                    StrSql = StrSql & " OccupancyReport.Parking40 AS ParkingTrailer40Ft,"
                    StrSql = StrSql & " OccupancyReport.StorageGoods AS StorageOfGoods,"
                    StrSql = StrSql & " OccupancyReport.AccessFee AS AccessFee,"
                    StrSql = StrSql & " (Container20Tenant+Container40Tenant+Container20NTenant+Container40NTenant+ContainerEntryFee+Reefer20+Reefer40+TransitContainers+Groupage+ForkliftHandling+Supervision+TransshipmentLCL+CrossDock20+CrossDock40+CrossDockEntryFee+Security+LoadUnload20+LoadUnload40+StorageHeavy+Parking20+Parking40+StorageGoods+AccessFee) AS Amount,"
                    StrSql = StrSql & " (Container20Tenant+Container40Tenant+Container20NTenant+Container40NTenant+ContainerEntryFee+Reefer20+Reefer40+TransitContainers+Groupage+ForkliftHandling+Supervision+TransshipmentLCL+CrossDock20+CrossDock40+CrossDockEntryFee+Security+LoadUnload20+LoadUnload40+StorageHeavy+Parking20+Parking40+StorageGoods+AccessFee) AS NETAmount"
                    StrSql = StrSql & " FROM OccupancyReport, Client"
                    StrSql = StrSql & " WHERE OccupancyReport.ClientID = Client.ClientID"
                    StrSql = StrSql & " And OccupancyReport.Year=" & SelectedYear
                    StrSql = StrSql & " And OccupancyReport.Month='" & SelectedMonth & "'"
                    StrSql = StrSql & " AND OccupancyReport.WhCode='" & SelectedZone & "'"
                    StrSql = StrSql & " AND Client.Category='" & SelectedCategory & "'"
                    StrSql = StrSql & " AND OccupancyReport.IsRental='" & "No" & "'"

                    '#Caption
                    FileName = "Occupancy Report - Other Charges (Freeport) - " & SelectedMonth & " - " & SelectedYear
                    Header2 = "OTHER CHARGES"
                    Header3 = "FREEPORT " & SelectedZone & " - " & SelectedMonth & " " & SelectedYear
                Else
                    'Including VAT
                    StrSql = "SELECT 'Z'+SUBSTRING(OccupancyReport.WhCode,5,1)+'/'+OccupancyReport.InvoiceNo AS InvoiceNo,"
                    StrSql = StrSql & " Client.VatNo, Client.ClientID, Client.Name,"
                    StrSql = StrSql & " OccupancyReport.Container20Tenant AS ContainerStorageL20Ft,"
                    StrSql = StrSql & " OccupancyReport.Container40Tenant AS ContainerStorageL40Ft,"
                    StrSql = StrSql & " OccupancyReport.Container20NTenant AS ContainerStorageNL20Ft,"
                    StrSql = StrSql & " OccupancyReport.Container40NTenant AS ContainerStorageNL40Ft,"
                    StrSql = StrSql & " OccupancyReport.ContainerEntryFee AS EntryFeeContainer20Ft40Ft,"
                    StrSql = StrSql & " OccupancyReport.Reefer20 AS Reefer20Ft,"
                    StrSql = StrSql & " OccupancyReport.Reefer40 AS Reefer40Ft,"
                    StrSql = StrSql & " OccupancyReport.TransitContainers AS TransitContainers20Ft40Ft,"
                    StrSql = StrSql & " OccupancyReport.Groupage AS ConsolidatedGroupage,"
                    StrSql = StrSql & " OccupancyReport.ForkliftHandling AS HandlingServices,"
                    StrSql = StrSql & " OccupancyReport.Supervision AS Supervision,"
                    StrSql = StrSql & " OccupancyReport.TransshipmentLCL AS TransshipmentLCL,"
                    StrSql = StrSql & " OccupancyReport.CrossDock20 AS CrossDocking20Ft,"
                    StrSql = StrSql & " OccupancyReport.CrossDock40 AS CrossDocking40Ft,"
                    StrSql = StrSql & " OccupancyReport.CrossDockEntryFee AS EntryFeeCD20Ft40Ft,"
                    StrSql = StrSql & " OccupancyReport.Security AS SecurityServices,"
                    StrSql = StrSql & " OccupancyReport.LoadUnload20 AS LoadingUnloading20Ft,"
                    StrSql = StrSql & " OccupancyReport.LoadUnload40 AS LoadingUnloading40Ft,"
                    StrSql = StrSql & " OccupancyReport.StorageHeavy AS StorageOfHeavyEquipment,"
                    StrSql = StrSql & " OccupancyReport.Parking20 AS ParkingTrailer20Ft,"
                    StrSql = StrSql & " OccupancyReport.Parking40 AS ParkingTrailer40Ft,"
                    StrSql = StrSql & " OccupancyReport.StorageGoods AS StorageOfGoods,"
                    StrSql = StrSql & " OccupancyReport.AccessFee AS AccessFee,"
                    StrSql = StrSql & " (Container20Tenant+Container40Tenant+Container20NTenant+Container40NTenant+ContainerEntryFee+Reefer20+Reefer40+TransitContainers+Groupage+ForkliftHandling+Supervision+TransshipmentLCL+CrossDock20+CrossDock40+CrossDockEntryFee+Security+LoadUnload20+LoadUnload40+StorageHeavy+Parking20+Parking40+StorageGoods+AccessFee) AS Amount,"
                    StrSql = StrSql & " (Container20Tenant+Container40Tenant+Container20NTenant+Container40NTenant+ContainerEntryFee+Reefer20+Reefer40+TransitContainers+Groupage+ForkliftHandling+Supervision+TransshipmentLCL+CrossDock20+CrossDock40+CrossDockEntryFee+Security+LoadUnload20+LoadUnload40+StorageHeavy+Parking20+Parking40+StorageGoods+AccessFee)*0.15 AS VAT,"
                    StrSql = StrSql & " (Container20Tenant+Container40Tenant+Container20NTenant+Container40NTenant+ContainerEntryFee+Reefer20+Reefer40+TransitContainers+Groupage+ForkliftHandling+Supervision+TransshipmentLCL+CrossDock20+CrossDock40+CrossDockEntryFee+Security+LoadUnload20+LoadUnload40+StorageHeavy+Parking20+Parking40+StorageGoods+AccessFee) + ((Container20Tenant+Container40Tenant+Container20NTenant+Container40NTenant+ContainerEntryFee+Reefer20+Reefer40+TransitContainers+Groupage+ForkliftHandling+Supervision+TransshipmentLCL+CrossDock20+CrossDock40+CrossDockEntryFee+Security+LoadUnload20+LoadUnload40+StorageHeavy+Parking20+Parking40+StorageGoods+AccessFee)*0.15) AS NETAmount"
                    StrSql = StrSql & " FROM OccupancyReport, Client"
                    StrSql = StrSql & " WHERE OccupancyReport.ClientID = Client.ClientID"
                    StrSql = StrSql & " And OccupancyReport.Year=" & SelectedYear
                    StrSql = StrSql & " And OccupancyReport.Month='" & SelectedMonth & "'"
                    StrSql = StrSql & " AND OccupancyReport.WhCode='" & SelectedZone & "'"
                    StrSql = StrSql & " AND Client.Category<>'" & "Freeport" & "'"
                    StrSql = StrSql & " AND OccupancyReport.IsRental='" & "No" & "'"
                    '#Caption
                    FileName = "Occupancy Report - Other Charges (Non-Freeport) - " & SelectedMonth & " - " & SelectedYear
                    Header2 = "OTHER CHARGES"
                    Header3 = "NON FREEPORT " & SelectedZone & " - " & SelectedMonth & " " & SelectedYear
                End If

            Else
                LblResult.Visible = True
                LblResult.Text = "Please select type to view occupancy report"
                Exit Sub
            End If
        Else
            'Car Bonded Yard
            StrSql = "SELECT 'Z'+SUBSTRING(OccupancyReport.WhCode,4,1)+'/'+OccupancyReport.InvoiceNo AS InvoiceNo,"
            StrSql = StrSql & " Client.VatNo, Client.ClientID, Client.Name,"
            StrSql = StrSql & " OccupancyReport.Yard,"
            StrSql = StrSql & " OccupancyReport.GateInGateOut,"
            StrSql = StrSql & " OccupancyReport.Constructive,"
            StrSql = StrSql & " OccupancyReport.Supervision,"
            StrSql = StrSql & " OccupancyReport.TransitContainers,"
            StrSql = StrSql & " OccupancyReport.Groupage,"
            StrSql = StrSql & " (Yard+GateInGateOut+Constructive+Supervision+TransitContainers+Groupage) AS Amount ,"
            StrSql = StrSql & " (Yard+GateInGateOut+Constructive+Supervision+TransitContainers+Groupage)*0.15 AS VAT ,"
            StrSql = StrSql & " (Yard+GateInGateOut+Constructive+Supervision+TransitContainers+Groupage) + ((Yard+GateInGateOut+Constructive+Supervision+TransitContainers+Groupage)*0.15) AS NETAmount"
            StrSql = StrSql & " FROM OccupancyReport, Client"
            StrSql = StrSql & " WHERE OccupancyReport.ClientID = Client.ClientID"
            StrSql = StrSql & " AND OccupancyReport.Year=" & SelectedYear
            StrSql = StrSql & " AND OccupancyReport.Month='" & SelectedMonth & "'"
            StrSql = StrSql & " AND OccupancyReport.WhCode='" & SelectedZone & "'"
            '#Caption
            FileName = "Occupancy Report - Car Bonded Yard - " & SelectedMonth & " - " & SelectedYear
            Header2 = "CAR BONDED YARD"
            Header3 = "NON FREEPORT " & SelectedZone & " - " & SelectedMonth & " " & SelectedYear
        End If

        '########## PRINT TO EXCEL
        If StrSql <> "" Then

            '###HEADER
            Dim dt_Header As DataTable
            dt_Header = New DataTable()
            Dim ColHeader As DataColumn
            ColHeader = New DataColumn("Header", Type.GetType("System.String"))
            dt_Header.Columns.Add(ColHeader)
            Dim dr_Header As DataRow
            dr_Header = dt_Header.NewRow
            dr_Header.Item("Header") = Header1
            dt_Header.Rows.Add(dr_Header)
            dr_Header = dt_Header.NewRow
            dr_Header.Item("Header") = Header2
            dt_Header.Rows.Add(dr_Header)
            dr_Header = dt_Header.NewRow
            dr_Header.Item("Header") = Header3
            dt_Header.Rows.Add(dr_Header)
            dr_Header = dt_Header.NewRow
            dr_Header.Item("Header") = ""
            dt_Header.Rows.Add(dr_Header)
            Dim ExcelGridHeader As New GridView()
            ExcelGridHeader.DataSource = dt_Header
            ExcelGridHeader.DataBind()
            '###HEADER

            '###FOOTER
            Dim dt_Footer As DataTable
            dt_Footer = New DataTable()
            Dim ColFooter As DataColumn
            ColFooter = New DataColumn("Footer", Type.GetType("System.String"))
            dt_Footer.Columns.Add(ColFooter)
            Dim dr_Footer As DataRow
            dr_Footer = dt_Footer.NewRow
            dr_Footer.Item("Footer") = "TOTAL"
            dt_Footer.Rows.Add(dr_Footer)
            dr_Footer = dt_Footer.NewRow
            dr_Footer.Item("Footer") = ""
            dt_Footer.Rows.Add(dr_Footer)
            dr_Footer = dt_Footer.NewRow
            dr_Footer.Item("Footer") = "Prepare and Examine :"
            dt_Footer.Rows.Add(dr_Footer)
            dr_Footer = dt_Footer.NewRow
            dr_Footer.Item("Footer") = "Input :"
            dt_Footer.Rows.Add(dr_Footer)
            dr_Footer = dt_Footer.NewRow
            dr_Footer.Item("Footer") = "Invoice :"
            dt_Footer.Rows.Add(dr_Footer)

            Dim ExcelGridFooter As New GridView()
            ExcelGridFooter.DataSource = dt_Footer
            ExcelGridFooter.DataBind()
            '###FOOTER

            Dim ExcelGrid As New GridView()
            Dim DtProfileUsers As New DataTable("Table")
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            ExcelGrid.DataSource = Rdr
            ExcelGrid.DataBind()
            Rdr.Close()
            If ExcelGrid.Rows.Count = 0 Then
                LblResult.Text = "No Records"
                Exit Sub
            End If
            Dim ExcelTable As New Table()
            'ExcelTable.Caption = FileName
            ExcelTable.GridLines = GridLines.Both
            For Each GdvRow As GridViewRow In ExcelGridHeader.Rows
                ExcelTable.Rows.Add(GdvRow)
            Next
            ExcelTable.Rows.Add(ExcelGrid.HeaderRow)
            For Each GdvRow As GridViewRow In ExcelGrid.Rows
                ExcelTable.Rows.Add(GdvRow)
            Next
            For Each GdvRow As GridViewRow In ExcelGridFooter.Rows
                ExcelTable.Rows.Add(GdvRow)
            Next
            Dim wrt As New IO.StringWriter()
            Dim htw As New HtmlTextWriter(wrt)
            ExcelTable.RenderControl(htw)
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & FileName & " - " & DateTime.Today & ".xls")
            Response.ContentType = "application/ms-excel"
            Response.Write(wrt.ToString())
            Response.[End]()
        Else
            MsgBox("Query empty for export!!")
        End If

    End Sub

    'Customised columns from database
    'Not used
    Public Sub Export2Excel()
        SelectedZone = Me.CmbZone.Text
        SelectedYear = Int(Me.TxtYear.Text)
        SelectedMonth = Me.CmbMonth.Text

        'Validation
        LblResult.Visible = False
        LblResult.Text = ""
        LblResult.ForeColor = Drawing.Color.Red
        If SelectedZone = "Select..." Then
            LblResult.Visible = True
            LblResult.Text = "Please select Zone!"
            Exit Sub
        ElseIf SelectedZone <> "FPZ6SD" Then
            If Me.CmbType.Text = "Rental" Then
                IsRental = True
            ElseIf Me.CmbType.Text = "Other Services" Then
                IsRental = False
            Else
                LblResult.Visible = True
                LblResult.Text = "Please select type to view occupancy report"
                Exit Sub
            End If
        End If

        'Other Services for Zones 1 & 6
        If ((SelectedZone = "FPZ01" Or SelectedZone = "FPZ06") And Not IsRental) Then
        Else
            LblResult.Visible = True
            LblResult.Text = "Excel available ONLY for Zones (1&6) And Other Services"
            Exit Sub
        End If

        Dim dt As DataTable
        Dim dr As DataRow

        Dim ColMonth As DataColumn
        Dim ColYear As DataColumn
        Dim ColZone As DataColumn

        ColMonth = New DataColumn("Month", Type.GetType("System.String"))
        ColYear = New DataColumn("Year", Type.GetType("System.String"))
        ColZone = New DataColumn("Zone", Type.GetType("System.String"))

        dt = New DataTable()

        dt.Columns.Add(ColMonth)
        dt.Columns.Add(ColYear)
        dt.Columns.Add(ColZone)

        dr = dt.NewRow
        dr.Item("Month") = "AAAA"
        dr.Item("Year") = "BBBBB"
        dr.Item("Zone") = "CCCCC"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr.Item("Month") = "DDDDD"
        dr.Item("Year") = "EEEEEE"
        dr.Item("Zone") = "FFFFFF"
        dt.Rows.Add(dr)


        dr = dt.NewRow
        dr.Item("Month") = "GGGGGG"
        dr.Item("Year") = "HHHHHHHH"
        dr.Item("Zone") = "IIIIII"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr.Item("Month") = "JJJJJJ"
        dr.Item("Year") = "KKKKK"
        dr.Item("Zone") = "LLLLLL"
        dt.Rows.Add(dr)

        Dim ExcelGrid As New GridView()
        Dim DtProfileUsers As New DataTable("Table")
        ExcelGrid.DataSource = dt
        ' it's my DataTable Name  
        ExcelGrid.DataBind()
        If ExcelGrid.Rows.Count = 0 Then
            LblResult.Text = "No Records"
            Exit Sub
        End If
        Dim ExcelTable As New Table()
        ExcelTable.GridLines = GridLines.Both
        ExcelTable.Rows.Add(ExcelGrid.HeaderRow)
        For Each GdvRow As GridViewRow In ExcelGrid.Rows
            ExcelTable.Rows.Add(GdvRow)
        Next

        Dim wrt As New IO.StringWriter()
        Dim htw As New HtmlTextWriter(wrt)
        ExcelTable.RenderControl(htw)

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=OCCTEST-" & DateTime.Today & ".xls")
        Response.ContentType = "application/ms-excel"
        Response.Write(wrt.ToString())
        Response.[End]()

    End Sub

End Class