﻿Partial Public Class FrmContainerReport
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdZone As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim Cmd As New OleDb.OleDbCommand
    Dim StrSql As String

    Const MENUID = "ContainerList"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            CmdZone.Connection = Con
            CmdZone.CommandText = "Select WhCode From Warehouse"
            Rdr = CmdZone.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Dim StrSelFormula As String
            Dim StrFrom As String = Format(CDate(TxtDateFrom.Text), "yyyy,MM,dd")
            Dim StrTo As String = Format(CDate(TxtDateTo.Text), "yyyy,MM,dd")

            StrSelFormula = "((({Containers.DateIn}>=date(" & StrFrom & ") And {Containers.DateIn}<=date(" & StrTo & ")))  Or IsNull({Containers.DateOut})) "
            If CmbZone.Text <> "Select..." Then
                StrSelFormula = StrSelFormula & " And {Containers.WhCode}= '" & CmbZone.Text & "'"
            End If
            If TxtClientID.Text <> "ALL" Then
                StrSelFormula = StrSelFormula & " And {Containers.ClientID}= '" & TxtClientID.Text & "'"
            End If
            Session("ReportFile") = "RptContainerList.rpt"
            Session("Period") = ""
            Session("Zone") = ""
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Container List Report"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub

    Protected Sub ButFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnFind.Click
        Session.Add("PstrCode", TxtClientID.Text)
        ScriptManager.RegisterStartupScript(BtnFind, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        Cmd.Connection = Con
        Cmd.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Try
            TxtClient.Text = Cmd.ExecuteScalar.ToString
        Catch ex As Exception
            TxtClient.Text = "Invalid Client"
        End Try
    End Sub
End Class