﻿Imports System.Web.Security
Imports System.Data
Imports System.Data.OleDb
Imports System.IO

Imports System.Configuration
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Drawing.Printing

Partial Public Class Report
    Inherits System.Web.UI.Page
    Dim Report As New ReportDocument()

    Private Sub Report_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            LblText.Text = LblText.Text & " - " & Request("RptID").ToString
            ' do all your reporting stuff here, then add it to session like so 
            Dim Report As New ReportDocument()
            Dim StrSelFormula As String = ""
            Report.Load(Server.MapPath(Request("RptFile").ToString))

            SetTableLocation(Report.Database.Tables)

            CrystalReportViewer1.ReportSource = Report
            CrystalReportViewer1.PrintMode = CrystalDecisions.Web.PrintMode.ActiveX
            Report.DataDefinition.RecordSelectionFormula = Request("StrSelF").ToString
            If Request("RptFile").ToString = "RptMonStkRtn.rpt" Then
                Report.DataDefinition.FormulaFields("DetailDate").Text = Request("StrFF").ToString
            End If
            '_reportViewer is the crystalviewer which you have on ur aspx form 
            Session("ReportDocument") = Report
        Else
            Dim doc As ReportDocument = DirectCast(Session("ReportDocument"), ReportDocument)
            CrystalReportViewer1.ReportSource = doc
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CrystalReportViewer1.Visible = True
        'BindReport()
        If Not Page.IsPostBack Then
            DDLPrinter.DataSource = System.Drawing.Printing.PrinterSettings.InstalledPrinters
            DDLPrinter.DataBind()
        End If
    End Sub

    Private Sub BindReport()

        Dim Report As New ReportDocument()
        Dim StrSelFormula As String = ""
        Report.Load(Server.MapPath(Request("RptFile").ToString))

        SetTableLocation(Report.Database.Tables)

        CrystalReportViewer1.ReportSource = Report
        CrystalReportViewer1.PrintMode = CrystalDecisions.Web.PrintMode.ActiveX
        Report.DataDefinition.RecordSelectionFormula = Request("StrSelF").ToString

        'Report.PrintOptions.PrinterName = Ps.PrinterName
        'Report.PrintToPrinter(1, False, 0, 0)

    End Sub

    Private Sub SetTableLocation(ByVal tables As Tables)
        Dim connectionInfo As New ConnectionInfo()

        'LOCAL
        connectionInfo.ServerName = "(local)"
        connectionInfo.DatabaseName = "wmstest"
        'connectionInfo.DatabaseName = "wmszone9"

        'WMS_UAT
        'connectionInfo.ServerName = "BFSLAPPSERVER"
        'connectionInfo.DatabaseName = "wms_uat"

        'WMS
        'connectionInfo.ServerName = "BFSLAPPSERVER"
        'connectionInfo.DatabaseName = "wmstest"

        'WMS_Z9
        'connectionInfo.ServerName = "BFSLAPPSERVER"
        'connectionInfo.DatabaseName = "wmszone9"

        connectionInfo.UserID = "admin"
        connectionInfo.Password = "swathi*"

        For Each table As CrystalDecisions.CrystalReports.Engine.Table In tables
            Dim tableLogOnInfo As TableLogOnInfo = table.LogOnInfo
            tableLogOnInfo.ConnectionInfo = connectionInfo
            table.ApplyLogOnInfo(tableLogOnInfo)
        Next
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        'Response.Redirect("about:blank")
        Response.Redirect(Request("RptFileName").ToString)
    End Sub

    Protected Sub ButPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrint.Click
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub ButPrintRpt_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim doc As ReportDocument = DirectCast(Session("ReportDocument"), ReportDocument)
        doc.PrintToPrinter(Val(TxtCopies.Text), ChkCollated.Checked, Val(TxtPFrom.Text), Val(TxtPTo.Text))
        ModalPopupExtender1.Hide()
    End Sub
End Class