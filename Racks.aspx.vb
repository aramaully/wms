﻿Public Partial Class Racks
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatLoc As New DataTable
    Dim StrCondition As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT ZoneCode FROM Zones ORDER BY ZoneCode"
            Rdr = Cmd.ExecuteReader
            CmbZoneCode.Items.Add("Select...")
            While Rdr.Read
                CmbZoneCode.Items.Add(Rdr("ZoneCode"))
            End While
            Rdr.Close()
            Cmd.CommandText = "SELECT WhCode FROM WareHouse ORDER BY WhCode"
            Rdr = Cmd.ExecuteReader
            CmbWarehouse.Items.Add("Select...")
            While Rdr.Read
                CmbWarehouse.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            BindRack()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Private Sub GdvRack_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvRack.SelectedIndexChanged
        On Error Resume Next
        TxtRackCode.Text = ChkSpace(GdvRack.SelectedRow.Cells(0).Text)
        TxtMaxPRatio.Text = ChkSpace(GdvRack.SelectedRow.Cells(1).Text)
        CmbZoneCode.Text = ChkSpace(GdvRack.SelectedRow.Cells(2).Text)
        CmbWarehouse.Text = ChkSpace(GdvRack.SelectedRow.Cells(3).Text)
    End Sub

    Private Sub BindRack()
        Cmd.Connection = Con
        StrSql = "SELECT RackCode, MaxPRatio,ZoneCode Zone ,WhCode Warehouse,CurOccupancy Occupancy FROM Racks"
        StrSql = StrSql & StrCondition & " ORDER BY RackCode"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar Is Nothing Then Exit Sub
        Rdr = Cmd.ExecuteReader
        DatLoc.Load(Rdr)
        Rdr.Close()
        GdvRack.DataSource = DatLoc
        GdvRack.DataBind()
        If GdvRack.Rows.Count = 1 Then
            On Error Resume Next
            TxtRackCode.Text = ChkSpace(GdvRack.SelectedRow.Cells(0).Text)
            TxtMaxPRatio.Text = ChkSpace(GdvRack.SelectedRow.Cells(1).Text)
            CmbZoneCode.Text = ChkSpace(GdvRack.SelectedRow.Cells(2).Text)
            CmbWarehouse.Text = ChkSpace(GdvRack.SelectedRow.Cells(3).Text)
        End If
    End Sub

    Private Sub GdvLoc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvRack.PageIndexChanging
        GdvRack.PageIndex = e.NewPageIndex
        BindRack()
    End Sub

    Private Sub GdvLoc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvRack.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvRack, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvRack.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvRack.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub GdvLoc_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvRack.SelectedIndexChanging
        GdvRack.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub TxtRackCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtRackCode.TextChanged
        If TxtRackCode.Text <> "" Then
            StrCondition = " WHERE RackCode LIKE '%" & TxtRackCode.Text & "%'"
        Else
            StrCondition = ""
        End If
        TxtMaxPRatio.Text = ""
        CmbWarehouse.SelectedIndex = 0
        CmbZoneCode.SelectedIndex = 0
        BindRack()
    End Sub

    Private Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        On Error Resume Next
        TxtRackCode.Text = ""
        CmbZoneCode.SelectedIndex = 0
        CmbWarehouse.SelectedIndex = 0
        TxtMaxPRatio.Text = ""
        TxtRackCode.Focus()
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Try
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Racks WHERE RackCode = '" & TxtRackCode.Text & "'"
            Cmd.ExecuteNonQuery()
            ButNew_Click(Me, e)
            BindRack()
        Catch ex As Exception
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Cmd.CommandText = "SELECT RackCode FROM Racks WHERE RackCode = '" & TxtRackCode.Text & "'"
        Try
            If Cmd.ExecuteScalar Is Nothing Then
                StrSql = "INSERT INTO Racks (RackCode,MaxPRatio,ZoneCode,WhCode) "
                StrSql = StrSql & "VALUES('" & TxtRackCode.Text & "',"
                StrSql = StrSql & Val(TxtMaxPRatio.Text) & ",'"
                StrSql = StrSql & IIf(CmbZoneCode.Text = "Select...", "", CmbZoneCode.Text) & "','"
                StrSql = StrSql & IIf(CmbWarehouse.Text = "Select...", "", CmbWarehouse.Text) & "')"
            Else
                StrSql = "UPDATE Racks SET RackCode = '" & TxtRackCode.Text & "',"
                StrSql = StrSql & "MaxPRatio=" & Val(TxtMaxPRatio.Text) & ","
                StrSql = StrSql & "ZoneCode='" & IIf(CmbZoneCode.Text = "Select...", "", CmbZoneCode.Text) & "',"
                StrSql = StrSql & "WhCode='" & IIf(CmbWarehouse.Text = "Select...", "", CmbWarehouse.Text) & "' WHERE RackCode = '" & TxtRackCode.Text & "'"
            End If
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ButNew_Click(Me, e)
            BindRack()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class