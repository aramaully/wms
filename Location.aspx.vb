﻿Public Partial Class Location
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim DatLoc As New DataTable
    Dim StrCondition As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            Cmd.CommandText = "SELECT ZoneCode FROM Zones ORDER BY ZoneCode"
            Rdr = Cmd.ExecuteReader
            CmbZoneCode.Items.Add("Select...")
            While Rdr.Read
                CmbZoneCode.Items.Add(Rdr("ZoneCode"))
            End While
            Rdr.Close()
            Cmd.CommandText = "SELECT RackCode FROM Racks ORDER BY RackCode"
            Rdr = Cmd.ExecuteReader
            CmbRack.Items.Add("Select...")
            While Rdr.Read
                CmbRack.Items.Add(Rdr("RackCode"))
            End While
            Rdr.Close()
            BindLoc()
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Function ChkSpace(ByVal Txt As String) As String
        If Txt = "&nbsp;" Then
            Return ""
        Else
            Return Txt
        End If
    End Function

    Private Sub GdvLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvLoc.SelectedIndexChanged
        On Error Resume Next
        TxtLocCode.Text = ChkSpace(GdvLoc.SelectedRow.Cells(0).Text)
        CmbZoneCode.Text = ChkSpace(GdvLoc.SelectedRow.Cells(1).Text)
        CmbRack.Text = ChkSpace(GdvLoc.SelectedRow.Cells(2).Text)
        TxtAisle.Text = ChkSpace(GdvLoc.SelectedRow.Cells(3).Text)
        TxtBay.Text = ChkSpace(GdvLoc.SelectedRow.Cells(4).Text)
        TxtLevel.Text = ChkSpace(GdvLoc.SelectedRow.Cells(5).Text)
        TxtSide.Text = ChkSpace(GdvLoc.SelectedRow.Cells(6).Text)
        ChkPut.Checked = DirectCast(GdvLoc.SelectedRow.Cells(7).Controls(0), CheckBox).Checked
        ChkPick.Checked = DirectCast(GdvLoc.SelectedRow.Cells(8).Controls(0), CheckBox).Checked
        TxtMaxPRatio.Text = ChkSpace(GdvLoc.SelectedRow.Cells(9).Text)
    End Sub

    Private Sub BindLoc()
        Cmd.Connection = Con
        StrSql = "SELECT LocCode Location,ZoneCode Zone ,RackCode Rack,Aisle, Bay, Level, Side, PutHold, PickHold, MaxPRatio FROM Location"
        StrSql = StrSql & StrCondition & " ORDER BY LocCode"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar Is Nothing Then Exit Sub
        Rdr = Cmd.ExecuteReader
        DatLoc.Load(Rdr)
        Rdr.Close()
        GdvLoc.DataSource = DatLoc
        GdvLoc.DataBind()
        If GdvLoc.Rows.Count = 1 Then
            On Error Resume Next
            TxtLocCode.Text = ChkSpace(GdvLoc.Rows(0).Cells(0).Text)
            CmbZoneCode.Text = ChkSpace(GdvLoc.Rows(0).Cells(1).Text)
            CmbRack.Text = ChkSpace(GdvLoc.Rows(0).Cells(2).Text)
            TxtAisle.Text = ChkSpace(GdvLoc.Rows(0).Cells(3).Text)
            TxtBay.Text = ChkSpace(GdvLoc.Rows(0).Cells(4).Text)
            TxtLevel.Text = ChkSpace(GdvLoc.Rows(0).Cells(5).Text)
            TxtSide.Text = ChkSpace(GdvLoc.Rows(0).Cells(6).Text)
            ChkPut.Checked = DirectCast(GdvLoc.Rows(0).Cells(7).Controls(0), CheckBox).Checked
            ChkPick.Checked = DirectCast(GdvLoc.Rows(0).Cells(8).Controls(0), CheckBox).Checked
            TxtMaxPRatio.Text = ChkSpace(GdvLoc.Rows(0).Cells(9).Text)
        End If
    End Sub

    Private Sub GdvLoc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GdvLoc.PageIndexChanging
        GdvLoc.PageIndex = e.NewPageIndex
        BindLoc()
    End Sub

    Private Sub GdvLoc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvLoc.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvLoc, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvLoc.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvLoc.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub GdvLoc_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvLoc.SelectedIndexChanging
        GdvLoc.SelectedIndex = e.NewSelectedIndex
    End Sub

    Private Sub TxtLocCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtLocCode.TextChanged
        If TxtLocCode.Text <> "" Then
            StrCondition = " WHERE LocCode LIKE '%" & TxtLocCode.Text & "%'"
        Else
            StrCondition = ""
        End If
        TxtAisle.Text = ""
        TxtBay.Text = ""
        TxtMaxPRatio.Text = ""
        TxtSide.Text = ""
        CmbRack.SelectedIndex = 0
        CmbZoneCode.SelectedIndex = 0
        BindLoc()
    End Sub

    Private Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        On Error Resume Next
        TxtLocCode.Text = ""
        CmbZoneCode.Text = ""
        CmbRack.Text = ""
        TxtAisle.Text = ""
        TxtBay.Text = ""
        TxtLevel.Text = ""
        TxtSide.Text = ""
        ChkPut.Checked = False
        ChkPick.Checked = False
        TxtMaxPRatio.Text = ""
        TxtLocCode.Focus()
    End Sub

    Private Sub ButDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDelete.Click
        Try
            Cmd.Connection = Con
            Cmd.CommandText = "DELETE FROM Location WHERE LocCode = '" & TxtLocCode.Text & "'"
            Cmd.ExecuteNonQuery()
            ButNew_Click(Me, e)
            BindLoc()
        Catch ex As Exception
            ErrHead.Text = "Delete Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Cmd.CommandText = "SELECT LocCode FROM Location WHERE LocCode = '" & TxtLocCode.Text & "'"
        Try
            If Cmd.ExecuteScalar Is Nothing Then
                StrSql = "INSERT INTO Location (LocCode,RackCode,ZoneCode,MaxPRatio,Aisle,Bay,Side,Level,PutHold,PickHold) "
                StrSql = StrSql & "VALUES('" & TxtLocCode.Text & "','"
                StrSql = StrSql & CmbRack.Text & "','"
                StrSql = StrSql & CmbZoneCode.Text & "',"
                StrSql = StrSql & Val(TxtMaxPRatio.Text) & ",'"
                StrSql = StrSql & TxtAisle.Text & "','"
                StrSql = StrSql & TxtBay.Text & "','"
                StrSql = StrSql & TxtSide.Text & "','"
                StrSql = StrSql & TxtLevel.Text & "',"
                StrSql = StrSql & IIf(ChkPut.Checked, 1, 0) & ","
                StrSql = StrSql & IIf(ChkPick.Checked, 1, 0) & ")"
            Else
                StrSql = "UPDATE Location SET LocCode = '" & TxtLocCode.Text & "',"
                StrSql = StrSql & "RackCode='" & CmbZoneCode.Text & "',"
                StrSql = StrSql & "ZoneCode='" & CmbZoneCode.Text & "',"
                StrSql = StrSql & "MaxPRatio=" & Val(TxtMaxPRatio.Text) & ","
                StrSql = StrSql & "Aisle='" & TxtAisle.Text & "',"
                StrSql = StrSql & "Bay='" & TxtBay.Text & "',"
                StrSql = StrSql & "Side='" & TxtSide.Text & "',"
                StrSql = StrSql & "Level='" & TxtLevel.Text & "',"
                StrSql = StrSql & "PutHold=" & IIf(ChkPut.Checked, 1, 0) & ","
                StrSql = StrSql & "PickHold=" & IIf(ChkPick.Checked, 1, 0) & " WHERE LocCode = '" & TxtLocCode.Text & "'"
            End If
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            ButNew_Click(Me, e)
            BindLoc()
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub
End Class