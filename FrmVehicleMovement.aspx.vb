﻿Imports System.IO
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Configuration
Partial Public Class FrmVehicleMovement
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdClient As New OleDb.OleDbCommand
    Dim StrSql As String
    Dim CmdAudit As New OleDb.OleDbCommand
    Dim StrAudit As String
    Dim CmdBoeOut As New OleDb.OleDbCommand
    Dim RdrBoeout As OleDb.OleDbDataReader
    Dim Lngdel As Long
    Dim DteDateIn As Date
    Dim CmdPicking As New OleDb.OleDbCommand
    Dim RdrPicking As OleDb.OleDbDataReader
    Dim DtePickDate As Date
    Dim StrBoeOut As String
    Dim LngBalance As Long
    Dim LngInQty As Long
    Dim StrPickDate As String
    Dim CmdBoeInItem As New OleDb.OleDbCommand
    Dim RdrBoeInItem As OleDb.OleDbDataReader
    Dim LngFob As Long
    Dim LngWeight As Long

    Const MENUID = "VehicleMovement"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbWhCode.Items.Add("Select...")
            CmbWhCode.Items.Add("ALL")
            While Rdr.Read
                CmbWhCode.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            StrSql = "SELECT Distinct(Category) FROM Regime ORDER BY Category"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbCat.Items.Add("ALL")
            While Rdr.Read
                CmbCat.Items.Add(Rdr("Category"))
            End While
            Rdr.Close()
        End If
    End Sub

    Private Sub TxtClientID_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtClientID.TextChanged
        CmdClient.Connection = Con
        CmdClient.CommandText = "SELECT Name FROM Client WHERE ClientID = '" & TxtClientID.Text & "'"
        Try
            TxtName.Text = CmdClient.ExecuteScalar.ToString
        Catch ex As Exception
            TxtName.Text = "Invalid Client"
        End Try
    End Sub

    Protected Sub ButFindClient_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButFindClient.Click
        Session.Add("PstrCode", TxtClientID.Text)
        ScriptManager.RegisterStartupScript(ButFindClient, Me.GetType, "ShowFind", "<script language='javascript'> ShowFind();</script>", False)
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Refresh" Then
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ColClientID As DataColumn
            Dim ColClientName As DataColumn
            Dim ColProdCode As DataColumn
            Dim ColItemNo As DataColumn
            Dim ColDescription As DataColumn
            Dim ColUnit As DataColumn
            Dim ColReceived As DataColumn
            Dim ColDateIn As DataColumn
            Dim ColBoeIn As DataColumn
            Dim ColDelivered As DataColumn
            Dim ColDelDate As DataColumn
            Dim ColBoeOut As DataColumn
            Dim ColBalance As DataColumn
            Dim ColFOB As DataColumn
            Dim ColWeight As DataColumn
            Dim BlnContinue As Boolean
            Dim ColClientIDMov As DataColumn
            Dim ColClientNameMov As DataColumn
            Dim ColOpeningStock As DataColumn
            Dim ColInStock As DataColumn
            Dim ColOutStock As DataColumn


            Dim DtMovement As DataTable
            Dim DrMovement As DataRow


            DtMovement = New DataTable()

            ColClientIDMov = New DataColumn("ClientIDMov", Type.GetType("System.String"))
            ColClientNameMov = New DataColumn("ClientNameMov", Type.GetType("System.String"))
            ColOpeningStock = New DataColumn("OpeningStock", Type.GetType("System.String"))
            ColInStock = New DataColumn("InStock", Type.GetType("System.String"))
            ColOutStock = New DataColumn("OutStock", Type.GetType("System.String"))

            DtMovement.Columns.Add(ColClientIDMov)
            DtMovement.Columns.Add(ColClientNameMov)
            DtMovement.Columns.Add(ColOpeningStock)
            DtMovement.Columns.Add(ColInStock)
            DtMovement.Columns.Add(ColOutStock)

            dt = New DataTable()

            ColClientID = New DataColumn("ClientID", Type.GetType("System.String"))
            ColClientName = New DataColumn("Name", Type.GetType("System.String"))
            ColProdCode = New DataColumn("PCode", Type.GetType("System.String"))
            ColItemNo = New DataColumn("ItemNo", Type.GetType("System.String"))
            ColDescription = New DataColumn("ProdDescription", Type.GetType("System.String"))
            ColUnit = New DataColumn("UnitCode", Type.GetType("System.String"))
            ColReceived = New DataColumn("Quantity", Type.GetType("System.String"))
            ColDateIn = New DataColumn("BillDate", Type.GetType("System.String"))
            ColBoeIn = New DataColumn("BoeNo", Type.GetType("System.String"))
            ColDelivered = New DataColumn("Delivered", Type.GetType("System.String"))
            ColDelDate = New DataColumn("DeliveredDate", Type.GetType("System.String"))
            ColBoeOut = New DataColumn("BoeOut", Type.GetType("System.String"))
            ColBalance = New DataColumn("Balance", Type.GetType("System.String"))
            ColFOB = New DataColumn("FOB", Type.GetType("System.String"))
            ColWeight = New DataColumn("Weight", Type.GetType("System.String"))



            dt.Columns.Add(ColClientID)
            dt.Columns.Add(ColClientName)
            dt.Columns.Add(ColProdCode)
            dt.Columns.Add(ColItemNo)
            dt.Columns.Add(ColDescription)
            dt.Columns.Add(ColUnit)
            dt.Columns.Add(ColReceived)
            dt.Columns.Add(ColDateIn)
            dt.Columns.Add(ColBoeIn)
            dt.Columns.Add(ColDelivered)
            dt.Columns.Add(ColDelDate)
            dt.Columns.Add(ColBoeOut)
            dt.Columns.Add(ColBalance)
            dt.Columns.Add(ColFOB)
            dt.Columns.Add(ColWeight)


            If TxtStart.Text = "" Then
                Lblerror.Visible = True
                Lblerror.Text = "Please Select Date"
                Exit Sub
            End If
            If TxtEnd.Text = "" Then
                Lblerror.Visible = True
                Lblerror.Text = "Please Select Date"
                Exit Sub
            End If
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "SELECT BOEINPROD.BOENO,BOEINPROD.ITEMNO, BoeInProd.UnitCode,BoeInProd.PCode,BoeInProd.ItemNo,0 As Balance,0 As FOB, 0 As Weight, "
            StrSql = StrSql & "BOEINPROD.PCODE, BoeInProd.ProdDescription, BOEINPROD.QUANTITY, BoeIn.ClientID,Client.Category,Client.Name, "
            StrSql = StrSql & "BoeIn.BillDate, BoeIn.WhCode FROM BOEINPROD "
            StrSql = StrSql & "INNER JOIN BoeIn ON BoeinProd.BoeNo = BoeIn.BillNo "
            If TxtClientID.Text <> "ALL" Then
                StrSql = StrSql & "Inner Join Client ON  BoeIn.ClientID = Client.ClientID And Client.ClientID  = '" & TxtClientID.Text & "' "
                If CmbCat.Text <> "ALL" Then
                    StrSql = StrSql & " And Client.Category = '" & CmbCat.Text & "' "
                End If
            Else
                StrSql = StrSql & "Inner Join Client ON  BoeIn.ClientID = Client.ClientID"
                If CmbCat.Text <> "ALL" Then
                    StrSql = StrSql & " And Client.Category = '" & CmbCat.Text & "' "
                End If
            End If
            StrSql = StrSql & " AND BoeIn.WhCode = '" & CmbWhCode.Text & "' "
            StrSql = StrSql & "WHERE (SELECT MAX(BILLDATE) FROM BOEOUTPROD, BOEOUT WHERE BOEINPROD.BOENO = BOEOUTPROD.BOEINNO AND BOEINPROD.PCODE = BOEOUTPROD.PCODE "
            StrSql = StrSql & "AND BOEOUTPROD.BOENO = BOEOUT.BILLNO) >= '" & Format(CDate(TxtStart.Text), "MM/dd/yyyy") & "' OR BOEINPROD.QUANTITY - ISNULL((SELECT SUM(QUANTITY) "
            StrSql = StrSql & "FROM  BOEOUTPROD WHERE BOEINPROD.BOENO = BOEOUTPROD.BOEINNO AND BOEINPROD.PCODE = BOEOUTPROD.PCODE AND "
            StrSql = StrSql & "BOEINPROD.ITEMNO = BOEOUTPROD.BOEINITEM),0) > 0 "
            StrSql = StrSql & "ORDER BY BoeIn.ClientID,BOEINPROD.BOENO"

            Cmd.Connection = Con
            CmdBoeOut.Connection = Con
            CmdPicking.Connection = Con
            CmdBoeInItem.Connection = Con

            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                BlnContinue = True
                LngBalance = 0
                LngInQty = 0
                StrBoeOut = ""
                LngInQty = Rdr("QUANTITY")
                LngBalance = Rdr("QUANTITY")
                DteDateIn = Rdr("BillDate")
                If DteDateIn > CDate(TxtEnd.Text) Then
                    BlnContinue = False
                End If
                StrSql = "SELECT BoeoutProd.*, BoeOut.BillDate FROM BoeOutProd "
                StrSql = StrSql & "INNER JOIN BoeOut ON BoeoutProd.BoeNo = BoeOut.BillNo "
                StrSql = StrSql & "WHERE BoeInNo = '" & Rdr("BoeNo") & "' AND "
                StrSql = StrSql & " BoeoutProd.BoeInItem = '" & Rdr("ItemNo") & "' AND "
                StrSql = StrSql & " BoeoutProd.PCode = '" & Rdr("PCode") & "' "
                StrSql = StrSql & "ORDER BY BoeOut.BillDate"
                CmdBoeOut.CommandText = StrSql
                RdrBoeout = CmdBoeOut.ExecuteReader
                StrPickDate = ""
                Lngdel = 0
                While RdrBoeout.Read
                    StrBoeOut = RdrBoeout("BoeNo")
                    DtePickDate = RdrBoeout("BillDate")
                    StrPickDate = DtePickDate
                    If DtePickDate <= CDate(TxtEnd.Text) Then
                        Lngdel = Lngdel + RdrBoeout("Quantity")
                        LngBalance = Math.Round((LngInQty - Lngdel), 2, MidpointRounding.AwayFromZero)
                    Else
                        StrPickDate = ""
                        StrBoeOut = ""
                    End If
                End While
                RdrBoeout.Close()
                LngFob = 0
                LngWeight = 0
                StrSql = "Select *From BoeInItem Where BoeNo = '" & Rdr("BoeNo") & "' And ItemNo = '" & Rdr("ItemNo") & "'"
                CmdBoeInItem.CommandText = StrSql
                RdrBoeInItem = CmdBoeInItem.ExecuteReader
                If RdrBoeInItem.Read Then
                    LngFob = RdrBoeInItem("FOB")
                    LngWeight = RdrBoeInItem("Weight")
                End If
                RdrBoeInItem.Close()
                If BlnContinue Then
                    dr = dt.NewRow
                    dr.Item("ClientID") = Rdr("ClientID")
                    dr.Item("Name") = Rdr("Name")
                    dr.Item("PCode") = Rdr("PCode")
                    dr.Item("ItemNo") = Rdr("ItemNo")
                    dr.Item("ProdDescription") = Rdr("ProdDescription")
                    dr.Item("UnitCode") = Rdr("UnitCode")
                    dr.Item("Quantity") = Rdr("Quantity")
                    dr.Item("BillDate") = Format(DteDateIn, "dd-MMM-yyyy")
                    dr.Item("BoeNo") = Rdr("BoeNo")
                    dr.Item("Delivered") = Lngdel
                    dr.Item("DeliveredDate") = StrPickDate
                    dr.Item("BoeOut") = StrBoeOut
                    dr.Item("Balance") = LngBalance
                    dr.Item("FOB") = LngFob
                    dr.Item("Weight") = LngWeight
                    dt.Rows.Add(dr)
                End If
            End While
            Rdr.Close()
            Dim LngTest As Long
            Dim DteInDate As Date
            Dim LngInQuantity As Long
            Dim DteOutDate As Date
            Dim LngOutQuantity As Long

            LngTest = 1
            LngInQuantity = 1
            LngOutQuantity = 1

            For i As Integer = 0 To dt.Rows.Count - 1
                Dim row As DataRow = dt.Rows(i)
                If i <> dt.Rows.Count - 1 Then
                    If row("BillDate") <> "" Then
                        DteInDate = Format(CDate(row("BillDate").ToString), "dd/MM/yyyy")
                    End If
                    If row("DeliveredDate") <> "" Then
                        DteOutDate = Format(CDate(row("DeliveredDate").ToString), "dd/MM/yyyy")
                    End If
                    Dim nextRow As DataRow = dt(i + 1)
                    If row("ClientID").Equals(nextRow("ClientID")) Then
                        LngTest = LngTest + Val(row("Quantity"))
                        If row("BillDate") <> "" Then
                            If DteInDate >= CDate(TxtStart.Text) And DteInDate <= CDate(TxtEnd.Text) Then
                                LngInQuantity = LngInQuantity + Val(row("Quantity"))
                            End If
                        End If
                        If row("DeliveredDate") <> "" Then
                            If DteOutDate >= CDate(TxtStart.Text) And DteOutDate <= CDate(TxtEnd.Text) Then
                                LngOutQuantity = LngOutQuantity + Val(row("Delivered"))
                            End If
                        End If
                    Else
                        ColClientIDMov = New DataColumn("ClientIDMov", Type.GetType("System.String"))
                        ColClientNameMov = New DataColumn("ClientNameMov", Type.GetType("System.String"))
                        ColOpeningStock = New DataColumn("OpeningStock", Type.GetType("System.String"))
                        ColInStock = New DataColumn("InStock", Type.GetType("System.String"))
                        ColOutStock = New DataColumn("OutStock", Type.GetType("System.String"))

                        DrMovement = DtMovement.NewRow
                        DrMovement.Item("ClientIDMov") = row("ClientID")
                        DrMovement.Item("ClientNameMov") = row("Name")
                        DrMovement.Item("OpeningStock") = LngTest - LngInQuantity
                        DrMovement.Item("InStock") = LngInQuantity
                        DrMovement.Item("OutStock") = LngOutQuantity
                        DtMovement.Rows.Add(DrMovement)
                        LngTest = 1
                        LngInQuantity = 1
                        LngOutQuantity = 1
                    End If
                End If
            Next


            GdvRate.DataSource = DtMovement
            GdvRate.DataBind()
        End If
        If e.Item.CommandName = "Export" Then
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ColClientID As DataColumn
            Dim ColClientName As DataColumn
            Dim ColProdCode As DataColumn
            Dim ColItemNo As DataColumn
            Dim ColDescription As DataColumn
            Dim ColUnit As DataColumn
            Dim ColReceived As DataColumn
            Dim ColDateIn As DataColumn
            Dim ColBoeIn As DataColumn
            Dim ColDelivered As DataColumn
            Dim ColDelDate As DataColumn
            Dim ColBoeOut As DataColumn
            Dim ColBalance As DataColumn
            Dim ColFOB As DataColumn
            Dim ColWeight As DataColumn
            Dim BlnContinue As Boolean

            dt = New DataTable()

            ColClientID = New DataColumn("ClientID", Type.GetType("System.String"))
            ColClientName = New DataColumn("Name", Type.GetType("System.String"))
            ColProdCode = New DataColumn("PCode", Type.GetType("System.String"))
            ColItemNo = New DataColumn("ItemNo", Type.GetType("System.String"))
            ColDescription = New DataColumn("ProdDescription", Type.GetType("System.String"))
            ColUnit = New DataColumn("UnitCode", Type.GetType("System.String"))
            ColReceived = New DataColumn("Quantity", Type.GetType("System.String"))
            ColDateIn = New DataColumn("BillDate", Type.GetType("System.String"))
            ColBoeIn = New DataColumn("BoeNo", Type.GetType("System.String"))
            ColDelivered = New DataColumn("Delivered", Type.GetType("System.String"))
            ColDelDate = New DataColumn("DeliveredDate", Type.GetType("System.String"))
            ColBoeOut = New DataColumn("BoeOut", Type.GetType("System.String"))
            ColBalance = New DataColumn("Balance", Type.GetType("System.String"))
            ColFOB = New DataColumn("FOB", Type.GetType("System.String"))
            ColWeight = New DataColumn("Weight", Type.GetType("System.String"))


            dt.Columns.Add(ColClientID)
            dt.Columns.Add(ColClientName)
            dt.Columns.Add(ColProdCode)
            dt.Columns.Add(ColItemNo)
            dt.Columns.Add(ColDescription)
            dt.Columns.Add(ColUnit)
            dt.Columns.Add(ColReceived)
            dt.Columns.Add(ColDateIn)
            dt.Columns.Add(ColBoeIn)
            dt.Columns.Add(ColDelivered)
            dt.Columns.Add(ColDelDate)
            dt.Columns.Add(ColBoeOut)
            dt.Columns.Add(ColBalance)
            dt.Columns.Add(ColFOB)
            dt.Columns.Add(ColWeight)



            If TxtStart.Text = "" Then
                Lblerror.Visible = True
                Lblerror.Text = "Please Select Date"
                Exit Sub
            End If
            If TxtEnd.Text = "" Then
                Lblerror.Visible = True
                Lblerror.Text = "Please Select Date"
                Exit Sub
            End If
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "SELECT BOEINPROD.BOENO,BOEINPROD.ITEMNO, BoeInProd.UnitCode,BoeInProd.PCode,BoeInProd.ItemNo,0 As Balance, "
            StrSql = StrSql & "BOEINPROD.PCODE, BoeInProd.ProdDescription, BOEINPROD.QUANTITY, BoeIn.ClientID,Client.Category,Client.Name, "
            StrSql = StrSql & "BoeIn.BillDate, BoeIn.WhCode FROM BOEINPROD "
            StrSql = StrSql & "INNER JOIN BoeIn ON BoeinProd.BoeNo = BoeIn.BillNo "
            If TxtClientID.Text <> "ALL" Then
                StrSql = StrSql & "Inner Join Client ON  BoeIn.ClientID = Client.ClientID And Client.ClientID  = '" & TxtClientID.Text & "' "
                If CmbCat.Text <> "ALL" Then
                    StrSql = StrSql & " And Client.Category = '" & CmbCat.Text & "' "
                End If
            Else
                StrSql = StrSql & "Inner Join Client ON  BoeIn.ClientID = Client.ClientID"
                If CmbCat.Text <> "ALL" Then
                    StrSql = StrSql & " And Client.Category = '" & CmbCat.Text & "' "
                End If
            End If
            StrSql = StrSql & " AND BoeIn.WhCode = '" & CmbWhCode.Text & "' "
            StrSql = StrSql & "WHERE (SELECT MAX(BILLDATE) FROM BOEOUTPROD, BOEOUT WHERE BOEINPROD.BOENO = BOEOUTPROD.BOEINNO AND BOEINPROD.PCODE = BOEOUTPROD.PCODE "
            StrSql = StrSql & "AND BOEOUTPROD.BOENO = BOEOUT.BILLNO) >= '" & Format(CDate(TxtStart.Text), "MM/dd/yyyy") & "' OR BOEINPROD.QUANTITY - ISNULL((SELECT SUM(QUANTITY) "
            StrSql = StrSql & "FROM  BOEOUTPROD WHERE BOEINPROD.BOENO = BOEOUTPROD.BOEINNO AND BOEINPROD.PCODE = BOEOUTPROD.PCODE AND "
            StrSql = StrSql & "BOEINPROD.ITEMNO = BOEOUTPROD.BOEINITEM),0) > 0 "
            StrSql = StrSql & "ORDER BY BoeIn.ClientID,BOEINPROD.BOENO"

            Cmd.Connection = Con
            CmdBoeOut.Connection = Con
            CmdPicking.Connection = Con
            CmdBoeInItem.Connection = Con

            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            While Rdr.Read
                BlnContinue = True
                LngBalance = 0
                LngInQty = 0
                StrBoeOut = ""
                LngInQty = Rdr("QUANTITY")
                LngBalance = Rdr("QUANTITY")
                DteDateIn = Rdr("BillDate")
                If DteDateIn > CDate(TxtEnd.Text) Then
                    BlnContinue = False
                End If




                StrSql = "SELECT BoeoutProd.*, BoeOut.BillDate FROM BoeOutProd "
                StrSql = StrSql & "INNER JOIN BoeOut ON BoeoutProd.BoeNo = BoeOut.BillNo "
                StrSql = StrSql & "WHERE BoeInNo = '" & Rdr("BoeNo") & "' AND "
                StrSql = StrSql & " BoeoutProd.BoeInItem = '" & Rdr("ItemNo") & "' AND "
                StrSql = StrSql & " BoeoutProd.PCode = '" & Rdr("PCode") & "' "
                StrSql = StrSql & "ORDER BY BoeOut.BillDate"
                CmdBoeOut.CommandText = StrSql
                RdrBoeout = CmdBoeOut.ExecuteReader
                StrPickDate = ""
                Lngdel = 0
                While RdrBoeout.Read
                    StrBoeOut = RdrBoeout("BoeNo")
                    DtePickDate = RdrBoeout("BillDate")
                    StrPickDate = DtePickDate
                    If DtePickDate <= CDate(TxtEnd.Text) Then
                        Lngdel = Lngdel + RdrBoeout("Quantity")
                        LngBalance = Math.Round((LngInQty - Lngdel), 2, MidpointRounding.AwayFromZero)
                    Else
                        StrPickDate = ""
                        StrBoeOut = ""
                    End If
                End While
                RdrBoeout.Close()
                LngFob = 0
                LngWeight = 0
                StrSql = "Select *From BoeInItem Where BoeNo = '" & Rdr("BoeNo") & "' And ItemNo = '" & Rdr("ItemNo") & "'"
                CmdBoeInItem.CommandText = StrSql
                RdrBoeInItem = CmdBoeInItem.ExecuteReader
                If RdrBoeInItem.Read Then
                    LngFob = RdrBoeInItem("FOB")
                    LngWeight = RdrBoeInItem("Weight")
                End If
                RdrBoeInItem.Close()
                If BlnContinue Then
                    dr = dt.NewRow
                    dr.Item("ClientID") = Rdr("ClientID")
                    dr.Item("Name") = Rdr("Name")
                    dr.Item("PCode") = Rdr("PCode")
                    dr.Item("ItemNo") = Rdr("ItemNo")
                    dr.Item("ProdDescription") = Rdr("ProdDescription")
                    dr.Item("UnitCode") = Rdr("UnitCode")
                    dr.Item("Quantity") = Rdr("Quantity")
                    dr.Item("BillDate") = Format(DteDateIn, "dd-MMM-yyyy")
                    dr.Item("BoeNo") = Rdr("BoeNo")
                    dr.Item("Delivered") = Lngdel
                    dr.Item("DeliveredDate") = StrPickDate
                    dr.Item("BoeOut") = StrBoeOut
                    dr.Item("Balance") = LngBalance
                    dr.Item("FOB") = LngFob
                    dr.Item("Weight") = LngWeight
                    dt.Rows.Add(dr)
                End If
            End While
            Rdr.Close()

            Dim ExcelGrid As New GridView()
            Dim DtProfileUsers As New DataTable("Table")
            ExcelGrid.DataSource = dt
            ' it's my DataTable Name  
            ExcelGrid.DataBind()
            GdvRate.DataSource = dt
            GdvRate.DataBind()
            If ExcelGrid.Rows.Count = 0 Then
                Lblerror.Text = "No Records"
                Exit Sub
            End If
            Dim ExcelTable As New Table()
            ExcelTable.GridLines = GridLines.Both
            ExcelTable.Rows.Add(ExcelGrid.HeaderRow)
            For Each GdvRow As GridViewRow In ExcelGrid.Rows
                ExcelTable.Rows.Add(GdvRow)
            Next
            Dim wrt As New IO.StringWriter()
            Dim htw As New HtmlTextWriter(wrt)
            ExcelTable.RenderControl(htw)
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=StockReturn-" & DateTime.Today & ".xls")
            Response.ContentType = "application/ms-excel"
            Response.Write(wrt.ToString())
            Response.[End]()
        End If
    End Sub
End Class