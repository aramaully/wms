﻿Public Partial Class FrmAccSupervisionRep
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdZone As New OleDb.OleDbCommand
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "SupervisionRep"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            CmdZone.Connection = Con
            CmdZone.CommandText = "Select WhCode From Warehouse"
            Rdr = CmdZone.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Dim StrSelFormula As String
            Dim StrFrom As String = Format(CDate(TxtDateFrom.Text), "yyyy,MM,dd")
            Dim StrTo As String = Format(CDate(TxtDateTo.Text), "yyyy,MM,dd")

            StrSelFormula = "{Logistics.ChDate}>=date(" & StrFrom & ") And {Logistics.ChDate}<=date(" & StrTo & ") And 'Supervision' In {Logistics.Description} "
            If CmbZone.Text <> "Select..." Then
                StrSelFormula = StrSelFormula & " And {Logistics.WhCode}= '" & CmbZone.Text & "'"
            End If
            Session("ReportFile") = "RptSupervision.rpt"
            Session("Period") = ""
            Session("Zone") = ""
            Session("SelFormula") = StrSelFormula
            Session("RepHead") = "Supervision Report"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        End If
    End Sub

End Class