﻿Partial Public Class PhTally
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "PhTally"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            StrSql = "SELECT * FROM WareHouse ORDER BY WhCode"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            TxtLotNo.Style("text-align") = "right"
        End If
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ModalPopupExtender1.Hide()
    End Sub

    Private Sub ButRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButRefresh.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT BoeIn.BillNo, CONVERT(Char,BoeIn.BillDate,103) 'Bill Date', BoeIn.Revision, Client.Name FROM BoeIn, Client WHERE "
        StrSql = StrSql & "BoeIn.ClientID = Client.ClientID "
        If RbnClient.Checked Then
            StrSql = StrSql & "AND Client.Name LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCountry.Checked Then
            StrSql = StrSql & "AND Country LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnTraffic.Checked Then
            StrSql = StrSql & "AND TrafficOp LIKE '%" & TxtSearch.Text & "%' "
        End If
        If RbnCont.Checked Then
            StrSql = StrSql & "AND ContainerNo LIKE '%" & TxtSearch.Text & "%' "
        End If
        If ChkDate.Checked Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = StrSql & "AND Boein.BillDate >= '" & CDate(TxtFrom.Text).ToString("dd-MMM-yyyy") & "' "
            StrSql = StrSql & "AND Boein.BillDate <= '" & CDate(TxtTo.Text).ToString("dd-MMM-yyyy") & "' "
        End If
        Dim cmd As New OleDb.OleDbCommand(StrSql, Con)
        Dim da As New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet()
        da.Fill(ds)
        GdvFind.DataSource = ds
        GdvFind.DataBind()
    End Sub

    Private Sub ButFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButFind.Click
        RbnClient.Text = "Client"
        RbnCountry.Text = "Country"
        RbnTraffic.Text = "Traffic Op"
        RbnCont.Visible = True
        ChkDate.Visible = True
        TxtFrom.Visible = True
        TxtTo.Visible = True
        LblToDate.Visible = True
        GdvFind.DataSource = Nothing
        TxtTo.Text = Date.Today.ToString("dd/MM/yyyy")
        TxtFrom.Text = Date.Today.AddDays(-10).ToString("dd/MM/yyyy")
        GdvFind.DataBind()
        TxtSearch.Text = ""
        PnlFind.Style.Item("display") = ""
        TxtSearch.Focus()
    End Sub

    Private Sub ButSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSelect.Click
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvFind.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvFind, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvFind_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvFind.SelectedIndexChanged
        TxtBillNo.Text = GdvFind.SelectedRow.Cells(0).Text
        TxtBillNo_TextChanged(Me, e)
        PnlFind.Style.Item("display") = "none"
    End Sub

    Private Sub GdvFind_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvFind.SelectedIndexChanging
        GdvFind.SelectedIndex = e.NewSelectedIndex
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        On Error Resume Next
        For i As Integer = 0 To Me.GdvFind.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvFind.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvBoe.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvBoe.UniqueID, "Select$" & i)
        Next
        For i As Integer = 0 To Me.GdvPhTally.Rows.Count - 1
            ClientScript.RegisterForEventValidation(Me.GdvPhTally.UniqueID, "Select$" & i)
        Next
        MyBase.Render(writer)
    End Sub

    Private Sub TxtBillNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtBillNo.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        ClearAll()
        StrSql = "SELECT BoeIn.WhCode,BoeIn.BillDate,Client.Name FROM BoeIn,Client WHERE BoeIn.ClientID = Client.ClientID AND  BillNo = '" & TxtBillNo.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        On Error Resume Next
        If Rdr.Read Then
            TxtBillDate.Text = Format(Rdr("BillDate"), "dd-MMM-yyyy")
            TxtName.Text = Rdr("Name").ToString
            CmbZone.Text = Rdr("WhCode").ToString
        Else
            TxtName.Text = "Invalid Bill Inbound"
            Exit Sub
        End If
        Rdr.Close()
        StrSql = "SELECT MAX(LotNo) FROM PhTally WHERE BoeNo = '" & TxtBillNo.Text & "'"
        Cmd.CommandText = StrSql
        TxtLotNo.Text = Cmd.ExecuteScalar.ToString
        TxtLotNo.Text = Val(TxtLotNo.Text) + 1
        TxtDateIn.Text = Date.Today.ToString("dd-MMM-yyyy")
        StrSql = "SELECT * FROM BoeInProd WHERE BoeNo = '" & TxtBillNo.Text & "' ORDER BY PCode"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvBoe.DataSource = Rdr
        GdvBoe.DataBind()
        Rdr.Close()
        StrSql = "SELECT PCode,DateIn,LotNo,NoPallet,QtyPerPallet,NoPallet*QtyPerPallet AS Quantity,Shared,PRatio,"
        StrSql = StrSql & "CASE WHEN Posted = 1 THEN 'Yes' ELSE 'No' END AS Palleted "
        StrSql = StrSql & "FROM PhTally WHERE BoeNo = '" & TxtBillNo.Text & "' ORDER BY LotNo, PCode"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvPhTally.DataSource = Rdr
        GdvPhTally.DataBind()
        Rdr.Close()
        UpdateStatus()
    End Sub

    Private Sub UpdateStatus()
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT BoeInProd.PCode AS Product, Product.Description, BoeInProd.Quantity AS Declared,"
        StrSql = StrSql & " QPhTally.Quantity AS Received, BoeInProd.Quantity - QPhTally.Quantity as Variance "
        StrSql = StrSql & " FROM (BoeInProd INNER JOIN QPhTally ON (BoeInProd.PCode = QPhTally.PCode) AND (BoeInProd.BoeNo = QPhTally.Boeno)) INNER JOIN Product ON BoeInProd.PCode = Product.ProdCode"
        StrSql = StrSql & " WHERE BoeInProd.Boeno = '" & TxtBillNo.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        GdvStatus.DataSource = Rdr
        GdvStatus.DataBind()
        Rdr.Close()
        lblStatus.Text = "Status (" & GdvStatus.Rows.Count & ")"
    End Sub

    'Protected Sub ButNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButNext.Click
    '    StrSql = "SELECT Top 1 BillNo FROM BoeIn WHERE BillNo > '" & TxtBillNo.Text & "'"
    '    Cmd.Connection = Con
    '    Cmd.CommandText = StrSql
    '    TxtBillNo.Text = Cmd.ExecuteScalar
    '    If TxtBillNo.Text = "" Then
    '        StrSql = "SELECT MIN(BillNo) FROM BoeIn"
    '        Cmd.CommandText = StrSql
    '        TxtBillNo.Text = Cmd.ExecuteScalar
    '    End If
    '    TxtBillNo_TextChanged(Me, e)
    'End Sub

    'Protected Sub ButPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPrevious.Click
    '    StrSql = "SELECT Top 1 BillNo FROM BoeIn WHERE BillNo < '" & TxtBillNo.Text & "'"
    '    StrSql = StrSql & "ORDER BY BillNo DESC"
    '    Cmd.Connection = Con
    '    Cmd.CommandText = StrSql
    '    TxtBillNo.Text = Cmd.ExecuteScalar
    '    If TxtBillNo.Text = "" Then
    '        StrSql = "SELECT MAX(BillNo) FROM BoeIn"
    '        Cmd.CommandText = StrSql
    '        TxtBillNo.Text = Cmd.ExecuteScalar
    '    End If
    '    TxtBillNo_TextChanged(Me, e)
    'End Sub

    Private Sub GdvBoe_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvBoe.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvBoe, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvBoe_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GdvBoe.SelectedIndexChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        ButAdd.Text = "Add " & GdvBoe.SelectedRow.Cells(0).Text & " to Tally Sheet"
        TxtPCode.Text = GdvBoe.SelectedRow.Cells(0).Text
        StrSql = "SELECT QtyPallet, PRatio FROM Product WHERE ProdCode = '" & GdvBoe.SelectedRow.Cells(0).Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtPerPallet.Text = Rdr("QtyPallet").ToString
            TxtPRatio.Text = Rdr("PRatio").ToString
        End If
        Rdr.Close()
        If Val(TxtPerPallet.Text) = 0 Then TxtPerPallet.Text = 1
        If Val(TxtPRatio.Text) = 0 Then TxtPRatio.Text = 1

        'wms-43 - Default values
        TxtPallets.Text = 1
        TxtPerPallet.Text = 1
        TxtQuantityTest.Text = 1
        TxtPRatio.Text = 10
        TxtPallets.Focus()
    End Sub

    Private Sub GdvBoe_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvBoe.SelectedIndexChanging
        'GdvBoe.SelectedIndex = e.NewSelectedIndex
        Dim row As GridViewRow = GdvBoe.Rows(e.NewSelectedIndex)
    End Sub

    Private Sub ClearAll()
        TxtBillDate.Text = ""
        TxtDateIn.Text = Date.Today.ToString("dd-MMM-yyyy")
        TxtLotNo.Text = ""
        TxtName.Text = ""
        TxtPallets.Text = ""
        TxtPCode.Text = ""
        TxtPRatio.Text = ""
        TxtQuantityTest.Text = ""
        TxtShared.Text = ""
        GdvBoe.DataSource = Nothing
        GdvBoe.DataBind()
        GdvBoe.SelectedIndex = -1
        ButAdd.Text = "Select Product from Inbound to add  to Tally Sheet"
        ButPalletId.Enabled = False
    End Sub

    Protected Sub ButAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButAdd.Click
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim DatTally As New DataTable
        Dim Dtrow As DataRow
        Cmd.Connection = Con
        StrSql = "SELECT PCode,DateIn,LotNo,NoPallet,QtyPerPallet,NoPallet*QtyPerPallet AS Quantity,Shared,PRatio,"
        StrSql = StrSql & "CASE WHEN Posted = 1 THEN 'Yes' ELSE 'No' END AS Palleted "
        StrSql = StrSql & "FROM PhTally WHERE PCode IS NULL"
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        DatTally.Load(Rdr)
        Rdr.Close()
        For Each row As GridViewRow In GdvPhTally.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dtrow = DatTally.NewRow
                For i = 0 To GdvPhTally.Columns.Count - 1
                    Dtrow.Item(i) = row.Cells(i).Text
                Next
                DatTally.Rows.Add(Dtrow)
            End If
        Next
        Dtrow = DatTally.NewRow
        Dtrow("PCode") = TxtPCode.Text
        Dtrow("DateIn") = CDate(TxtDateIn.Text)
        Dtrow("LotNo") = TxtLotNo.Text
        Dtrow("NoPallet") = Val(TxtPallets.Text)
        Dtrow("QtyPerPallet") = Val(TxtPerPallet.Text)
        Dtrow("Quantity") = Val(TxtQuantityTest.Text)
        Dtrow("Shared") = IIf(TxtShared.Text = "", "N", TxtShared.Text)
        Dtrow("PRatio") = Val(TxtPRatio.Text)
        Dtrow("Palleted") = "No"
        DatTally.Rows.Add(Dtrow)
        GdvPhTally.DataSource = DatTally
        GdvPhTally.DataBind()
        'Update Status Grid
        Dim BlnDone As Boolean = False
        For Each row As GridViewRow In GdvStatus.Rows
            If row.Cells(0).Text = TxtPCode.Text Then
                row.Cells(3).Text = Val(row.Cells(3).Text) + Val(TxtQuantityTest.Text)
                row.Cells(4).Text = Val(row.Cells(2).Text) - Val(row.Cells(3).Text)
                BlnDone = True
            End If
        Next
        If Not BlnDone Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "SELECT BoeInProd.PCode AS Product, Product.Description, BoeInProd.Quantity AS Declared,"
            StrSql = StrSql & "QPhTally.Quantity AS Received, BoeInProd.Quantity - QPhTally.Quantity as Variance "
            StrSql = StrSql & "FROM (BoeInProd INNER JOIN QPhTally ON (BoeInProd.PCode = QPhTally.PCode) "
            StrSql = StrSql & "AND (BoeInProd.BoeNo = QPhTally.Boeno)) INNER JOIN Product ON BoeInProd.PCode = Product.ProdCode "
            StrSql = StrSql & "WHERE BoeInProd.Boeno = '" & TxtBillNo.Text & "'"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            Dim DatStat As New DataTable
            DatStat.Load(Rdr)
            Rdr.Close()
            DatStat.Clear()
            For Each Row As GridViewRow In GdvStatus.Rows
                If Row.RowType = DataControlRowType.DataRow Then
                    Dtrow = DatStat.NewRow
                    Dtrow = DatStat.NewRow
                    Dtrow.Item("Product") = Row.Cells(0).Text
                    Dtrow.Item("Description") = Row.Cells(1).Text
                    Dtrow.Item("Declared") = Row.Cells(2).Text
                    Dtrow.Item("Received") = Row.Cells(3).Text
                    Dtrow.Item("Variance") = Row.Cells(4).Text
                    DatStat.Rows.Add(Dtrow)
                End If
            Next
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Dtrow = DatStat.NewRow
            Dtrow.Item("Product") = TxtPCode.Text
            Dtrow.Item("Description") = GdvBoe.SelectedRow.Cells(1).Text
            Dtrow.Item("Declared") = GdvBoe.SelectedRow.Cells(2).Text
            Dtrow.Item("Received") = Val(TxtQuantityTest.Text)
            Dtrow.Item("Variance") = Val(GdvBoe.SelectedRow.Cells(2).Text) - Val(TxtQuantityTest.Text)
            DatStat.Rows.Add(Dtrow)
            GdvStatus.DataSource = DatStat
            GdvStatus.DataBind()
            lblStatus.Text = "Status (" & GdvStatus.Rows.Count & ")"
        End If
    End Sub

    Protected Sub TxtPerPallet_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtPerPallet.TextChanged
        TxtQuantityTest.Text = Val(TxtPallets.Text) * Val(TxtPerPallet.Text)
    End Sub

    Private Sub TxtPallets_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtPallets.TextChanged
        TxtQuantityTest.Text = Val(TxtPallets.Text) * Val(TxtPerPallet.Text)
    End Sub

    Private Sub GdvPhTally_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GdvPhTally.RowDataBound
        On Error Resume Next
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';"
            e.Row.Attributes("onclick") = ClientScript.GetPostBackClientHyperlink(Me.GdvPhTally, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Private Sub GdvPhTally_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvPhTally.SelectedIndexChanging
        Dim row As GridViewRow = GdvPhTally.Rows(e.NewSelectedIndex)
    End Sub

    Protected Sub ButRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButRemove.Click
        Try
            If GdvPhTally.SelectedRow.Cells(8).Text = "Yes" Then
                Exit Sub
            End If
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            Dim IntRow As Integer = GdvPhTally.SelectedRow.RowIndex
            Dim DatTally As New DataTable
            Dim Dtrow As DataRow
            Dim StrPCode As String = ""
            Dim IntDelQty As Integer = 0
            Cmd.Connection = Con
            StrSql = "SELECT PCode,DateIn,LotNo,NoPallet,QtyPerPallet,NoPallet*QtyPerPallet AS Quantity,Shared,PRatio,"
            StrSql = StrSql & "CASE WHEN Posted = 1 THEN 'Yes' ELSE 'No' END AS Palleted "
            StrSql = StrSql & "FROM PhTally WHERE PCode IS NULL"
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            DatTally.Load(Rdr)
            Rdr.Close()
            For Each row As GridViewRow In GdvPhTally.Rows
                If row.RowType = DataControlRowType.DataRow Then
                    If row.RowIndex <> IntRow Then
                        Dtrow = DatTally.NewRow
                        For i = 0 To GdvPhTally.Columns.Count - 1
                            Dtrow.Item(i) = row.Cells(i).Text
                        Next
                        DatTally.Rows.Add(Dtrow)
                    Else
                        StrPCode = row.Cells(0).Text
                        IntDelQty = Val(row.Cells(5).Text)
                    End If
                End If
            Next
            GdvPhTally.DataSource = DatTally
            GdvPhTally.DataBind()
            If StrPCode <> "" Then
                For Each row As GridViewRow In GdvStatus.Rows
                    If row.Cells(0).Text = StrPCode Then
                        row.Cells(3).Text = Val(row.Cells(3).Text) - IntDelQty
                        row.Cells(4).Text = Val(row.Cells(2).Text) - Val(row.Cells(3).Text)
                    End If
                Next
            End If
            lblStatus.Text = "Status (" & GdvStatus.Rows.Count & ")"
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSave.Click
        Dim Trans As OleDb.OleDbTransaction
        If RbnNo.Checked Then
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            For Each Row As GridViewRow In GdvStatus.Rows
                If Val(Row.Cells(4).Text) <> 0 Then
                    ErrHead.Text = "Discrepancy Noted"
                    ErrMsg.Text = "Please Check Discrepancy Noted as Yes to Save"
                    ModalPopupExtender1.Show()
                    Exit Sub
                End If
            Next
        End If
        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "DELETE  FROM PhTally WHERE BoeNo = '" & TxtBillNo.Text & "' AND LotNo = " & TxtLotNo.Text
            StrSql = StrSql & " AND Posted = 0"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            For Each Row As GridViewRow In GdvPhTally.Rows
                If Row.Cells(2).Text = TxtLotNo.Text And Row.Cells(8).Text = "No" Then
                    Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
                    StrSql = "INSERT INTO PhTally (BoeNo,PCode,NoPallet,QtyPerPallet,Shared,PRatio,DateIn,LotNo,Posted,Unit) "
                    StrSql = StrSql & " VALUES('" & TxtBillNo.Text & "','"
                    StrSql = StrSql & Row.Cells(0).Text & "',"
                    StrSql = StrSql & Val(Row.Cells(3).Text) & ","
                    StrSql = StrSql & Val(Row.Cells(4).Text) & ",'"
                    StrSql = StrSql & Row.Cells(6).Text & "',"
                    StrSql = StrSql & Val(Row.Cells(7).Text) & ",'"
                    StrSql = StrSql & Format(CDate(Row.Cells(1).Text), "MM/dd/yyyy") & "',"
                    StrSql = StrSql & Val(TxtLotNo.Text) & ","
                    StrSql = StrSql & "0,'"
                    For Each BRow As GridViewRow In GdvBoe.Rows
                        If BRow.Cells(0).Text = Row.Cells(0).Text Then
                            StrSql = StrSql & BRow.Cells(3).Text & "')"
                        End If
                    Next
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                End If
            Next
            If RbnYes.Checked Then
                StrSql = "UPDATE BoeIn SET Discrepancy = 1 WHERE BillNo = '" & TxtBillNo.Text & "'"
                Cmd.CommandText = StrSql
                Cmd.ExecuteNonQuery()
            End If
            Trans.Commit()
            LblMessage.Visible = True
            LblMessage.Text = "Tally Sheet Successfull. Click on Pallet ID to generate Pallets."
            ButPalletId.Enabled = True
        Catch ex As Exception
            Trans.Rollback()
            ButPalletId.Enabled = False
            ErrHead.Text = "Save Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    'aramaully | V1.0.0.3
    Protected Sub ButPalletId_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPalletId.Click
        Dim CmdSDTariff As New OleDb.OleDbCommand
        Dim IntPalletID As Integer
        Dim BlnDone As Boolean
        Dim Trans As OleDb.OleDbTransaction
        Dim I As Integer
        Dim gateInRate = 200.0
        Dim gateInFees As Double

        CmdSDTariff.Connection = Con
        StrSql = "Select TariffValue FROM SDTariff WHERE TariffID='SDGateIn'"
        CmdSDTariff.CommandText = StrSql
        gateInRate = CmdSDTariff.ExecuteScalar

        Trans = Con.BeginTransaction(IsolationLevel.ReadCommitted)
        Cmd.Connection = Con
        Cmd.Transaction = Trans
        Try
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            ' First Update all Non-Shared Pallets
            StrSql = "SELECT NextPalletNo FROM [System] WHERE WhCode = '" & CmbZone.Text & "'"
            Cmd.CommandText = StrSql
            IntPalletID = Cmd.ExecuteScalar
            StrSql = "SELECT PhTally.*,BoeIn.ClientID FROM PhTally,BoeIn WHERE BoeNo = '" & TxtBillNo.Text & "' AND LotNo = " & TxtLotNo.Text
            StrSql = StrSql & " AND PhTally.BoeNo = BoeIn.BillNo AND PhTally.Posted = 0 And Shared = 'N'  ORDER BY LotNo, PCode"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            Dim DatTally As New DataTable
            DatTally.Load(Rdr)
            Rdr.Close()
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            For Each Row As DataRow In DatTally.Rows
                For I = 1 To Row.Item("NoPallet")
                    'Add Pallet in Pallets Table
                    'aramaully | V1.0.0.3
                    If CmbZone.Text = "FPZ6SD" Then
                        gateInFees = (Row.Item("PRatio") / 10) * gateInRate
                        StrSql = "INSERT INTO Pallets (PalletID, BoeNo, PRatio, LotNo, Shared, DateIn, RenewalDate, ClientID, Abandoned, Cleared, PutAway, WhCode, GateInRate, GateInFees) "
                        StrSql = StrSql & "VALUES(" & IntPalletID & ",'"
                        StrSql = StrSql & TxtBillNo.Text & "',"
                        StrSql = StrSql & Row.Item("PRatio") & ","
                        StrSql = StrSql & Row.Item("LotNo") & ","
                        StrSql = StrSql & "0,'"
                        StrSql = StrSql & Format(CDate(Row.Item("DateIn")), "MM/dd/yyyy") & "','"
                        StrSql = StrSql & Format(CDate(Row.Item("DateIn")), "MM/dd/yyyy") & "','"
                        StrSql = StrSql & Row.Item("ClientID") & "',"
                        StrSql = StrSql & "0,"
                        StrSql = StrSql & "0,"
                        StrSql = StrSql & "0,'"
                        StrSql = StrSql & CmbZone.Text & "',"
                        StrSql = StrSql & gateInRate & ","
                        StrSql = StrSql & gateInFees & ")"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    Else
                        StrSql = "INSERT INTO Pallets (PalletID, BoeNo, PRatio, LotNo, Shared, DateIn, RenewalDate, ClientID, Abandoned, Cleared, PutAway, WhCode) "
                        StrSql = StrSql & "VALUES(" & IntPalletID & ",'"
                        StrSql = StrSql & TxtBillNo.Text & "',"
                        StrSql = StrSql & Row.Item("PRatio") & ","
                        StrSql = StrSql & Row.Item("LotNo") & ","
                        StrSql = StrSql & "0,'"
                        StrSql = StrSql & Format(CDate(Row.Item("DateIn")), "MM/dd/yyyy") & "','"
                        StrSql = StrSql & Format(CDate(Row.Item("DateIn")), "MM/dd/yyyy") & "','"
                        StrSql = StrSql & Row.Item("ClientID") & "',"
                        StrSql = StrSql & "0,"
                        StrSql = StrSql & "0,"
                        StrSql = StrSql & "0,'"
                        StrSql = StrSql & CmbZone.Text & "')"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                    End If

                    'Update Pallet Details
                    StrSql = "INSERT INTO PaltDetail (BoeNo,PalletID,PCode,Quantity,BalQuantity,Unit,PRatio,PutAway)"
                    StrSql = StrSql & "VALUES('" & TxtBillNo.Text & "',"
                    StrSql = StrSql & IntPalletID & ",'"
                    StrSql = StrSql & Row.Item("PCode") & "',"
                    StrSql = StrSql & Row.Item("QtyPerPallet") & ","
                    StrSql = StrSql & Row.Item("QtyPerPallet") & ",'"
                    StrSql = StrSql & Row.Item("Unit") & "',"
                    StrSql = StrSql & Row.Item("PRatio") & ","
                    StrSql = StrSql & "0)"
                    Cmd.CommandText = StrSql
                    Cmd.ExecuteNonQuery()
                    IntPalletID = IntPalletID + 1
                Next
            Next
            'Now Start with Shared Pallets
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "SELECT Distinct(Shared) FROM PhTally WHERE BoeNo = '" & TxtBillNo.Text & "' AND LotNo = " & TxtLotNo.Text
            StrSql = StrSql & " AND PhTally.Posted = 0 And Shared <> 'N'  ORDER BY Shared"
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Rdr = Cmd.ExecuteReader
            Dim DatShare As New DataTable
            DatShare.Load(Rdr)
            Rdr.Close()
            BlnDone = False
            Dim IntPalletStart As Integer = IntPalletID
            For Each Row As DataRow In DatShare.Rows
                StrSql = "SELECT PhTally.*,BoeIn.ClientID FROM PhTally,BoeIn WHERE BoeNo = '" & TxtBillNo.Text & "' AND LotNo = " & TxtLotNo.Text
                StrSql = StrSql & " AND PhTally.BoeNo = BoeIn.BillNo AND PhTally.Posted = 0 And Shared = '" & Row.Item(0)
                StrSql = StrSql & "' ORDER BY LotNo, PCode"
                Cmd.Connection = Con
                Cmd.CommandText = StrSql
                Rdr = Cmd.ExecuteReader
                Dim DatTbl As New DataTable
                DatTbl.Load(Rdr)
                Rdr.Close()
                For Each DRow As DataRow In DatTbl.Rows
                    IntPalletID = IntPalletStart
                    For I = 1 To DRow.Item("NoPallet")
                        If Not BlnDone Then
                            'Add Pallet in Pallets Table
                            StrSql = "INSERT INTO Pallets (PalletID,BoeNo,PRatio,LotNo,Shared,DateIn,RenewalDate,ClientID,Abandoned,Cleared,PutAway,WhCode) "
                            StrSql = StrSql & "VALUES(" & IntPalletID & ",'"
                            StrSql = StrSql & TxtBillNo.Text & "',"
                            StrSql = StrSql & DRow.Item("PRatio") & ","
                            StrSql = StrSql & DRow.Item("LotNo") & ","
                            StrSql = StrSql & "0,'"
                            StrSql = StrSql & TxtDateIn.Text & "','"
                            StrSql = StrSql & TxtDateIn.Text & "','"
                            StrSql = StrSql & DRow.Item("ClientID") & "',"
                            StrSql = StrSql & "0,"
                            StrSql = StrSql & "0,"
                            StrSql = StrSql & "0,'"
                            StrSql = StrSql & CmbZone.Text & "')"
                            Cmd.CommandText = StrSql
                            Cmd.ExecuteNonQuery()
                        End If
                        'Update Pallet Details
                        StrSql = "INSERT INTO PaltDetail (BoeNo,PalletID,PCode,Quantity,BalQuantity,Unit,PRatio,PutAway)"
                        StrSql = StrSql & "VALUES('" & TxtBillNo.Text & "',"
                        StrSql = StrSql & IntPalletID & ",'"
                        StrSql = StrSql & DRow.Item("PCode") & "',"
                        StrSql = StrSql & DRow.Item("QtyPerPallet") & ","
                        StrSql = StrSql & DRow.Item("QtyPerPallet") & ",'"
                        StrSql = StrSql & DRow.Item("Unit") & "',"
                        StrSql = StrSql & DRow.Item("PRatio") & ","
                        StrSql = StrSql & "0)"
                        Cmd.CommandText = StrSql
                        Cmd.ExecuteNonQuery()
                        IntPalletID = IntPalletID + 1
                    Next
                    BlnDone = True
                Next
            Next
            'Update System File with next pallet id
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            StrSql = "UPDATE [System] SET NextPalletNo = " & IntPalletID & " WHERE WhCode = '" & CmbZone.Text & "'"
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            'Mark PhTally as Palleted
            StrSql = "UPDATE PhTally SET Posted = 1 WHERE BoeNo = '" & TxtBillNo.Text & "' AND LotNo = " & TxtLotNo.Text
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
            Trans.Commit()
            LblMessage.Visible = True
            LblMessage.Text = "Pallet ID Created Successfully"
            TxtBillNo_TextChanged(Me, e)
        Catch ex As Exception
            Trans.Rollback()
            ErrHead.Text = "Operation Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Protected Sub TxtLotNo_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TxtLotNo.TextChanged
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        StrSql = "SELECT * FROM PhTally WHERE BoeNo = '" & TxtBillNo.Text & "' AND LotNo = " & TxtLotNo.Text & " AND Posted = 0  ORDER BY LotNo, PCode"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            ButPalletId.Enabled = True
            ButChDateIn.Enabled = False
        Else
            ButPalletId.Enabled = False
            ButChDateIn.Enabled = True
        End If
        Rdr.Close()
    End Sub

    Protected Sub ButChDateIn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButChDateIn.Click
        Try
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
            'Change Date in for already Generated Pallets
            StrSql = "UPDATE Pallets SET DateIn '" & Format(CDate(TxtDateIn.Text), "MM/dd/yyyy") & "', RenewalDate = '" & Format(CDate(TxtDateIn.Text), "MM/dd/yyyy") & "' "
            StrSql = StrSql & "WHERE BoeNo = '" & TxtBillNo.Text & "' AND LotNo = " & TxtLotNo.Text
            Cmd.Connection = Con
            Cmd.CommandText = StrSql
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            ErrHead.Text = "Update Failed"
            ErrMsg.Text = ex.Message
            ModalPopupExtender1.Show()
        End Try
    End Sub

    Private Sub GdvStatus_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GdvStatus.SelectedIndexChanging
        Dim row As GridViewRow = GdvStatus.Rows(e.NewSelectedIndex)
    End Sub
End Class