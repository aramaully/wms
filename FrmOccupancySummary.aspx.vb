﻿Partial Public Class FrmOccupancySummary
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim CmdSystem As New OleDb.OleDbCommand
    Dim RdrSystem As OleDb.OleDbDataReader
    Dim CmdProperty As New OleDb.OleDbCommand
    Dim RdrProperty As OleDb.OleDbDataReader
    Dim StrSql As String

    Const MENUID = "OccupancySummary"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        If Not Page.IsPostBack Then
            Cmd.Connection = Con
            'Check Access
            StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
            Cmd.CommandText = StrSql
            If Cmd.ExecuteScalar = 0 Then
                Response.Redirect("about:blank")
            End If

            Cmd.CommandText = "SELECT Whcode FROM [System] Where WhCode <> 'FPZ6SD'"
            Rdr = Cmd.ExecuteReader
            CmbZone.Items.Add("Select...")
            While Rdr.Read
                CmbZone.Items.Add(Rdr("WhCode"))
            End While
            Rdr.Close()
            TxtYearFrom.Text = Year(Date.Today)
            TxtYearTo.Text = Year(Date.Today)
        End If
    End Sub

    Protected Sub ToolBar1_ItemClick(ByVal sender As Object, ByVal e As EO.Web.ToolBarEventArgs) Handles ToolBar1.ItemClick
        If e.Item.CommandName = "Print" Then
            Dim dt As DataTable

            Dim dr As DataRow

            Dim ColMonth As DataColumn
            Dim ColYear As DataColumn
            Dim ColZone As DataColumn
            Dim ColWarehouseCapacity As DataColumn
            Dim ColRentable As DataColumn
            Dim ColSegment As DataColumn
            Dim ColAreaRented As DataColumn
            Dim ColAreaAvailable As DataColumn
            Dim ColOccupancy As DataColumn

            Dim DteStart As Date
            Dim DteEnd As Date
            Dim StrMonth As String
            Dim StrYear As String
            Dim StrZone As String = ""
            Dim LngCapacity As Long
            Dim LngTotalSpace As Long
            Dim LngAreaFreeport As Long
            Dim LngAreaNonFreeport As Long
            Dim LngSpaceRented As Long
            Dim LngPercentOCC As Long
            Dim LngSubTotalZ1 As Long
            Dim LngSubTotalZ6 As Long
            Dim LngSubTotalZ9 As Long
            Dim DteFrom As Date

            ColMonth = New DataColumn("Month", Type.GetType("System.String"))
            ColYear = New DataColumn("Year", Type.GetType("System.String"))
            ColZone = New DataColumn("Zone", Type.GetType("System.String"))
            ColWarehouseCapacity = New DataColumn("WarehouseCapacity", Type.GetType("System.String"))
            ColRentable = New DataColumn("Rentable", Type.GetType("System.String"))
            ColSegment = New DataColumn("Segment", Type.GetType("System.String"))
            ColAreaRented = New DataColumn("AreaRented", Type.GetType("System.String"))
            ColAreaAvailable = New DataColumn("AreaAvailable", Type.GetType("System.String"))
            ColOccupancy = New DataColumn("Occupancy", Type.GetType("System.String"))
            

            dt = New DataTable()

            dt.Columns.Add(ColMonth)
            dt.Columns.Add(ColYear)

            dt.Columns.Add(ColZone)
            dt.Columns.Add(ColWarehouseCapacity)
            dt.Columns.Add(ColRentable)
            dt.Columns.Add(ColSegment)
            dt.Columns.Add(ColAreaRented)
            dt.Columns.Add(ColAreaAvailable)
            dt.Columns.Add(ColOccupancy)

            

            DteStart = CDate("01-" & CmbMonthFrom.Text & "-" & TxtYearFrom.Text)
            DteFrom = CDate("01-" & CmbMonthFrom.Text & "-" & TxtYearFrom.Text)
            DteEnd = CDate("28-" & CmbMonthTo.Text & "-" & TxtYearTo.Text)

            Do While DteStart <= DteEnd
                StrMonth = Format(DteStart, "MMM")
                StrYear = DteStart.Year

                dr = dt.NewRow
                dr.Item("Month") = StrMonth
                dr.Item("Year") = StrYear
                dt.Rows.Add(dr)

                CmdSystem.Connection = Con
                CmdProperty.Connection = Con
                If CmbZone.Text = "Select..." Then
                    CmdSystem.CommandText = "Select *From [System] Where WhCode <> 'FPZ6SD'"
                Else
                    CmdSystem.CommandText = "Select *From [System] Where WhCode = '" & CmbZone.Text & "'"
                End If
                RdrSystem = CmdSystem.ExecuteReader
                While RdrSystem.Read
                    LngTotalSpace = 0
                    LngSpaceRented = 0
                    LngPercentOCC = 0

                    If RdrSystem("WhCode") = "FPZ01" Then
                        StrZone = "FPZ01"
                        LngCapacity = 12700
                        LngTotalSpace = 10698
                        StrSql = "SELECT Sum(Property.Area) As Area FROM LeaseAgreement "
                        StrSql = StrSql & "Inner Join Property ON LeaseAgreement.PropCode = Property.PropertyCode "
                        StrSql = StrSql & "Inner Join Client ON LeaseAgreement.ClientID = Client.ClientID "
                        StrSql = StrSql & "WHERE LeaseAgreement.MoveInDate >= '" & Format(DteFrom, "MM/dd/yyyy") & "' And LeaseAgreement.MoveInDate <= '" & Format(DteStart, "MM/dd/yyyy") & "' And LeaseAgreement.Zone = 'FPZ01' And Client.Category = 'Freeport' "
                        StrSql = StrSql & "And (Property.[Type] = 'Warehouse' or Property.[Type] = 'Shed')"
                        CmdProperty.CommandText = StrSql
                        RdrProperty = CmdProperty.ExecuteReader
                        While RdrProperty.Read
                            If Not (IsDBNull(RdrProperty("Area"))) Then
                                LngAreaFreeport = Val(RdrProperty("Area"))
                            End If
                        End While
                        RdrProperty.Close()
                        StrSql = "SELECT Sum(Property.Area) As Area FROM LeaseAgreement "
                        StrSql = StrSql & "Inner Join Property ON LeaseAgreement.PropCode = Property.PropertyCode "
                        StrSql = StrSql & "Inner Join Client ON LeaseAgreement.ClientID = Client.ClientID "
                        StrSql = StrSql & "WHERE LeaseAgreement.MoveInDate >= '" & Format(DteFrom, "MM/dd/yyyy") & "' And LeaseAgreement.MoveInDate <= '" & Format(DteStart, "MM/dd/yyyy") & "' And LeaseAgreement.Zone = 'FPZ01' And Client.Category <> 'Freeport' "
                        StrSql = StrSql & "And (Property.[Type] = 'Warehouse' or Property.[Type] = 'Shed')"
                        CmdProperty.CommandText = StrSql
                        RdrProperty = CmdProperty.ExecuteReader
                        While RdrProperty.Read
                            If Not (IsDBNull(RdrProperty("Area"))) Then
                                LngAreaNonFreeport = Val(RdrProperty("Area"))
                            End If
                        End While
                        dr = dt.NewRow
                        dr.Item("Segment") = "Non Freeport"
                        dr.Item("AreaRented") = LngAreaNonFreeport
                        dr.Item("Occupancy") = Math.Round((LngAreaNonFreeport / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        dt.Rows.Add(dr)
                        RdrProperty.Close()

                        LngSubTotalZ1 = LngAreaFreeport + LngAreaNonFreeport


                        dr = dt.NewRow
                        dr.Item("Segment") = "Freeport"
                        dr.Item("AreaRented") = LngAreaFreeport
                        dr.Item("AreaAvailable") = LngTotalSpace - (LngAreaFreeport + LngAreaNonFreeport)
                        dr.Item("Occupancy") = Math.Round((LngAreaFreeport / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        'Math.Round((LngSpaceRented / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        dt.Rows.Add(dr)


                        dr = dt.NewRow
                        dr.Item("Zone") = StrZone
                        dr.Item("WarehouseCapacity") = LngCapacity
                        dr.Item("Rentable") = LngTotalSpace
                        dr.Item("Segment") = "Total"
                        dr.Item("AreaRented") = LngAreaFreeport + LngAreaNonFreeport
                        dr.Item("AreaAvailable") = LngTotalSpace - (LngAreaFreeport + LngAreaNonFreeport)
                        dr.Item("Occupancy") = Math.Round(((LngAreaFreeport + LngAreaNonFreeport) / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        'Math.Round((LngSpaceRented / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        dt.Rows.Add(dr)

                    End If
                    If RdrSystem("WhCode") = "FPZ06" Then
                        StrZone = "FPZ06"
                        LngCapacity = 24000
                        LngTotalSpace = 19361
                        StrSql = "SELECT sum(Property.Area) As Area FROM LeaseAgreement "
                        StrSql = StrSql & "Inner Join Property ON LeaseAgreement.PropCode = Property.PropertyCode "
                        StrSql = StrSql & "Inner Join Client ON LeaseAgreement.ClientID = Client.ClientID "
                        StrSql = StrSql & "WHERE LeaseAgreement.MoveInDate >= '" & Format(DteFrom, "MM/dd/yyyy") & "' And LeaseAgreement.MoveInDate <= '" & Format(DteStart, "MM/dd/yyyy") & "' And LeaseAgreement.Zone = 'FPZ06' And Client.Category = 'Freeport' "
                        StrSql = StrSql & "And (Property.[Type] = 'Warehouse' or Property.[Type] = 'Shed')"
                        CmdProperty.CommandText = StrSql
                        RdrProperty = CmdProperty.ExecuteReader
                        While RdrProperty.Read
                            If Not (IsDBNull(RdrProperty("Area"))) Then
                                LngAreaFreeport = Val(RdrProperty("Area"))
                            End If
                        End While
                        RdrProperty.Close()
                        StrSql = "SELECT sum(Property.Area) As Area FROM LeaseAgreement "
                        StrSql = StrSql & "Inner Join Property ON LeaseAgreement.PropCode = Property.PropertyCode "
                        StrSql = StrSql & "Inner Join Client ON LeaseAgreement.ClientID = Client.ClientID "
                        StrSql = StrSql & "WHERE LeaseAgreement.MoveInDate >= '" & Format(DteFrom, "MM/dd/yyyy") & "' And LeaseAgreement.MoveInDate <= '" & Format(DteStart, "MM/dd/yyyy") & "' And LeaseAgreement.Zone = 'FPZ06' And Client.Category <> 'Freeport' "
                        StrSql = StrSql & "And (Property.[Type] = 'Warehouse' or Property.[Type] = 'Shed')"
                        CmdProperty.CommandText = StrSql
                        RdrProperty = CmdProperty.ExecuteReader
                        While RdrProperty.Read
                            If Not (IsDBNull(RdrProperty("Area"))) Then
                                LngAreaNonFreeport = Val(RdrProperty("Area"))
                            End If
                        End While
                        dr = dt.NewRow
                        dr.Item("Segment") = "Non Freeport"
                        dr.Item("AreaRented") = LngAreaNonFreeport
                        dr.Item("Occupancy") = Math.Round((LngAreaNonFreeport / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        dt.Rows.Add(dr)
                        RdrProperty.Close()

                        dr = dt.NewRow
                        dr.Item("Segment") = "Freeport"
                        dr.Item("AreaRented") = LngAreaFreeport
                        dr.Item("AreaAvailable") = LngTotalSpace - (LngAreaFreeport + LngAreaNonFreeport)
                        dr.Item("Occupancy") = Math.Round((LngAreaFreeport / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        'Math.Round((LngSpaceRented / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        dt.Rows.Add(dr)

                        LngSubTotalZ6 = LngAreaFreeport + LngAreaNonFreeport

                        dr = dt.NewRow
                        dr.Item("Zone") = StrZone
                        dr.Item("WarehouseCapacity") = LngCapacity
                        dr.Item("Rentable") = LngTotalSpace
                        dr.Item("Segment") = "Total"
                        dr.Item("AreaRented") = LngAreaFreeport + LngAreaNonFreeport
                        dr.Item("AreaAvailable") = LngTotalSpace - (LngAreaFreeport + LngAreaNonFreeport)
                        dr.Item("Occupancy") = Math.Round(((LngAreaFreeport + LngAreaNonFreeport) / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        'Math.Round((LngSpaceRented / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        dt.Rows.Add(dr)
                    End If
                    If RdrSystem("WhCode") = "FPZ09" Then
                        StrZone = "FPZ09"
                        LngCapacity = 4000
                        LngTotalSpace = 3395
                        StrSql = "SELECT sum(Property.Area) As Area FROM LeaseAgreement "
                        StrSql = StrSql & "Inner Join Property ON LeaseAgreement.PropCode = Property.PropertyCode "
                        StrSql = StrSql & "Inner Join Client ON LeaseAgreement.ClientID = Client.ClientID "
                        StrSql = StrSql & "WHERE LeaseAgreement.MoveInDate >= '" & Format(DteFrom, "MM/dd/yyyy") & "' And LeaseAgreement.MoveInDate <= '" & Format(DteStart, "MM/dd/yyyy") & "' And LeaseAgreement.Zone = 'FPZ09' And Client.Category = 'Freeport' "
                        StrSql = StrSql & "And (Property.[Type] = 'Cabin' or Property.[Type] = 'Office')"
                        CmdProperty.CommandText = StrSql
                        RdrProperty = CmdProperty.ExecuteReader
                        While RdrProperty.Read
                            If Not (IsDBNull(RdrProperty("Area"))) Then
                                LngAreaFreeport = Val(RdrProperty("Area"))
                            End If
                        End While
                        RdrProperty.Close()
                        StrSql = "SELECT sum(Property.Area) As Area FROM LeaseAgreement "
                        StrSql = StrSql & "Inner Join Property ON LeaseAgreement.PropCode = Property.PropertyCode "
                        StrSql = StrSql & "Inner Join Client ON LeaseAgreement.ClientID = Client.ClientID "
                        StrSql = StrSql & "WHERE LeaseAgreement.MoveInDate >= '" & Format(DteFrom, "MM/dd/yyyy") & "' And LeaseAgreement.MoveInDate <= '" & Format(DteStart, "MM/dd/yyyy") & "' And LeaseAgreement.Zone = 'FPZ09' And Client.Category <> 'Freeport' "
                        StrSql = StrSql & "And (Property.[Type] = 'Cabin' or Property.[Type] = 'Office')"
                        CmdProperty.CommandText = StrSql
                        RdrProperty = CmdProperty.ExecuteReader
                        While RdrProperty.Read
                            If Not (IsDBNull(RdrProperty("Area"))) Then
                                LngAreaNonFreeport = Val(RdrProperty("Area"))
                            End If
                        End While
                        dr = dt.NewRow
                        dr.Item("Segment") = "Non Freeport"
                        dr.Item("AreaRented") = LngAreaNonFreeport
                        dr.Item("Occupancy") = Math.Round((LngAreaNonFreeport / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        dt.Rows.Add(dr)
                        RdrProperty.Close()

                        dr = dt.NewRow
                        dr.Item("Segment") = "Freeport"
                        dr.Item("AreaRented") = LngAreaFreeport
                        dr.Item("AreaAvailable") = LngTotalSpace - (LngAreaFreeport + LngAreaNonFreeport)
                        dr.Item("Occupancy") = Math.Round((LngAreaFreeport / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        'Math.Round((LngSpaceRented / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        dt.Rows.Add(dr)

                        LngSubTotalZ9 = LngAreaFreeport + LngAreaNonFreeport

                        dr = dt.NewRow
                        dr.Item("Zone") = StrZone
                        dr.Item("WarehouseCapacity") = LngCapacity
                        dr.Item("Rentable") = LngTotalSpace
                        dr.Item("Segment") = "Total"
                        dr.Item("AreaRented") = LngAreaFreeport + LngAreaNonFreeport
                        dr.Item("AreaAvailable") = LngTotalSpace - (LngAreaFreeport + LngAreaNonFreeport)
                        dr.Item("Occupancy") = Math.Round(((LngAreaFreeport + LngAreaNonFreeport) / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        'Math.Round((LngSpaceRented / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                        dt.Rows.Add(dr)
                    End If
                End While
                RdrSystem.Close()
                dr = dt.NewRow
                dr.Item("Zone") = "ALL Zones"
                dr.Item("WarehouseCapacity") = 12700 + 24000 + 4000
                dr.Item("Rentable") = 10698 + 19361 + 3395
                dr.Item("Segment") = "Grand Total"
                dr.Item("AreaRented") = LngSubTotalZ1 + LngSubTotalZ6 + LngSubTotalZ9
                dr.Item("AreaAvailable") = (12700 + 24000 + 4000) - (LngSubTotalZ1 + LngSubTotalZ6 + LngSubTotalZ9)
                dr.Item("Occupancy") = Math.Round(((LngSubTotalZ1 + LngSubTotalZ6 + LngSubTotalZ9) / (12700 + 24000 + 4000)) * 100, 2, MidpointRounding.AwayFromZero)
                'Math.Round((LngSpaceRented / LngTotalSpace) * 100, 2, MidpointRounding.AwayFromZero)
                dt.Rows.Add(dr)
                DteStart = DteStart.AddMonths(1)
            Loop

            
                Dim ExcelGrid As New GridView()
                Dim DtProfileUsers As New DataTable("Table")
                ExcelGrid.DataSource = dt
                ' it's my DataTable Name  
                ExcelGrid.DataBind()
                If ExcelGrid.Rows.Count = 0 Then
                    LblResult.Text = "No Records"
                    Exit Sub
                End If
                Dim ExcelTable As New Table()
                ExcelTable.GridLines = GridLines.Both
                ExcelTable.Rows.Add(ExcelGrid.HeaderRow)
                For Each GdvRow As GridViewRow In ExcelGrid.Rows
                    ExcelTable.Rows.Add(GdvRow)
                Next
                Dim wrt As New IO.StringWriter()
                Dim htw As New HtmlTextWriter(wrt)
                ExcelTable.RenderControl(htw)
                Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=WarehouseOccBySegment-" & DateTime.Today & ".xls")
                Response.ContentType = "application/ms-excel"
                Response.Write(wrt.ToString())
                Response.[End]()
        End If
    End Sub
End Class