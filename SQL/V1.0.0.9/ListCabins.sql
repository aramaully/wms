﻿--List of Cabins Occupied Currently
SELECT Client.Name,Client.Category, Property.Area,PropertyCode,Property.[Type],CONVERT(CHAR,LeaseStart,103) 'Start',
CONVERT(CHAR,LeaseEnd,103) 'End',CONVERT(CHAR,Property.MovedIn,103) 'Date Move In', LeaseAgreement.AddRemarks Remarks FROM Property LEFT OUTER JOIN Client ON 
Client.ClientID = Property.ClientID LEFT OUTER JOIN LeaseAgreement On Property.PropertyCode=LeaseAgreement.PropCode 
WHERE Property.LeaseEnd >= GETDATE() And Property.FpZone = 'FPZ06'

--List of Cabins Lease Expired Currently
SELECT Client.Name,Client.Category, Property.Area,PropertyCode,Property.[Type],CONVERT(CHAR,LeaseStart,103) 'Start',
CONVERT(CHAR,LeaseEnd,103) 'End',CONVERT(CHAR,Property.MovedIn,103) 'Date Move In', LeaseAgreement.AddRemarks Remarks FROM Property LEFT OUTER JOIN Client ON 
Client.ClientID = Property.ClientID LEFT OUTER JOIN LeaseAgreement On Property.PropertyCode=LeaseAgreement.PropCode 
WHERE Property.LeaseEnd  < GETDATE() And Property.FpZone = 'FPZ06'

--List of Cabins Vacant Currently
SELECT  PropertyCode,Property.Area,Property.[Type],Client.Name,
LeaseAgreement.AddRemarks FROM Property LEFT OUTER JOIN Client ON
Client.ClientID = Property.ClientID LEFT OUTER JOIN LeaseAgreement On Property.PropertyCode=LeaseAgreement.PropCode
WHERE Property.LeaseEnd IS NULL And Property.FpZone = 'FPZ06'

--C:\Bitbucket\Wms\FrmDashBoard.aspx.vb
--C:\Bitbucket\Wms\FrmViewList.aspx
--C:\Bitbucket\Wms\FrmViewList.aspx.vb