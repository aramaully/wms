﻿--Version 1.0.0.3
ALTER TABLE [wmstest].[dbo].[Pallets] ADD GateInRate float, GateInFees float
ALTER TABLE [wmstest].[dbo].[PickList] ADD GateOutRate float, GateOutFees float
ALTER TABLE [wmstest].[dbo].[Occupancy] ADD GateInRate float, GateInFees float, GateOutRate float, GateOutFees float