﻿--Kissen:
--Please delete all the CONTAINER for Perfect C/o Afcons for the month of October that is 5/10 , 6/10 and 7/10 . I will reinput them.
select * from Containers where ClientId='400C30010A' and dateIn >= '2020-10-05 00:00:00'
delete from Containers where ClientId='400C30010A' and dateIn >= '2020-10-05 00:00:00'

--Ajay
--Client : ABC  MOTORS  LTD
--WH BOE : 7202018344
--Pallet ID : 67920
--Date of WH : 6th OCT 2020
--Dear ARSHAD , 
--Refer to the  attached list  , kindly be informed that the date of entry/warehousing  had been wrongly recorded on WMS Put Away Report . 
--(ie : Date of WH should be read as 06/10/2020 instead of 07/10/2020 ) .
--However the Inbound BOE & Bill Status Report were correctly recorded on WMS .
--Grateful if you could  amend the date of WH on both Physical Tally sheet and Put Away List accordingly at your end at the earliest .
--Rgds
--Ajay R
select * from BoeIn where BillNo='72020183443'
select * from BoeInItem where BoeNo='72020183443'
select * from BoeInProd where BoeNo='72020183443'
select * from PhTally where BoeNo='72020183443'
select * from Pallets where BoeNo='72020183443'
select * from PaltDetail where PalletID='67920'
UPDATE PhTally SET DateIn='2020-10-06 00:00:00' where BoeNo='72020183443'
UPDATE Pallets SET DateIn='2020-10-06 00:00:00' ,RenewalDate='2020-10-06 00:00:00' where BoeNo='72020183443'

--Ajay
--CLIENT : SEABRIDGE TRADING LTD
--WH BOE : 92020196696
--WH DATE : 23RD OCT 2020
--Dear Arshad ,
--Kindly refer to the two(2) WMS Reports for item 1 up to item 4 concerning the product   J Walker RED LABEL (LT) .
--As per entry made on WMS BOE INBOUND , 
--Item 1 = 145 ctns of J WALKER RED LABEL LT
--Item 2 = 17 ctns of J WALKER RED LABEL LT
--Item 3 = 200 ctns of J WALKER RED LABEL LT
--Item 4 = 58 ctns of J WALKER RED LABEL LT 
--         420
--At time of verifying same in the Stock Returns Report ,
--we found out that it was appropriately & accordingly recorded but unfortunately it was wrongly reported in the Bill Status Report ( refer to the annexed docs) .
--Kindly treat as urgent to remedy the issue at the earliest .
--Rgds
--Ajay R
--Solution: Add Ordering in Bill Status Report : BillNo, ItemNo, ProdCode


