﻿USE [wmstest]
GO

/****** Object:  Table [dbo].[[RptLeaseAnalysisHistory]]   
Script Date: 01/12/2020 08:49:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[RptLeaseAnalysisHistory]

CREATE TABLE [dbo].[RptLeaseAnalysisHistory](
	[UserID] [varchar](50) NULL,
	--Client
	[CName] [varchar](50) NULL,
	[CCategory] [varchar](50) NULL,
	[CLicenceExp] [smalldatetime] NULL,
	[CPermitExp] [smalldatetime] NULL,
	--LeaseAgreement
	[LZone] [varchar] (15) NULL,
	[LArea] [int] NULL,
	[LLeaseNo] [varchar] (25) NULL,
	[LMonthlyRent] [decimal] (18,2) NULL,
	[LDeposit] [int] NULL,
	[LInsurance] [int] NULL,
	[LConCharges] [decimal] (12,2) NULL,
	[LAmount] [decimal] (12,2) NULL,
	[LLeaseStatus] [varchar] (25) NULL,
	[LPropCode] [varchar] (25) NULL,
	[LProjectDesc] [varchar] (255) NULL,
	[LMoveInDate] [smalldatetime] NULL,
	[LMoveOutDate] [smalldatetime] NULL,
	[LAddRemarks] [text] NULL,
	--Property
	[PType] [varchar] (30) NULL,
	[PLeaseStart] [smalldatetime] NULL,
	[PLeaseEnd] [smalldatetime] NULL,
)ON [PRIMARY]
GO
