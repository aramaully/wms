﻿USE [wmstest]
GO

/****** Object:  Table [dbo].[OccupancyReport]   
Script Date: 30/07/2020 08:49:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[OccupancyReport]


CREATE TABLE [dbo].[OccupancyReport](
	[InvoiceNo] [varchar](50) NULL,
	[InvDate] [smalldatetime] NULL,
	[ClientID] [varchar](50) NULL,
	[WhCode] [varchar](50) NULL,
	[Month] [varchar](50) NULL,
	[Year] [varchar](50) NULL,
	[IsRental] [varchar](50) NULL,
	-------------------------------------------CBY
	[Yard] [decimal](18,2) NULL,
	[GateInGateOut] [decimal](18,2) NULL,
	[Constructive] [decimal](18,2) NULL,
	-------------------------------------------CBY | Other Services
	[Supervision] [decimal](18,2) NULL,
	[TransitContainers] [decimal](18,2) NULL,
	[Groupage] [decimal](18,2) NULL,

	-------------------------------------------Rental
	[Comments] [varchar](255) NULL,
	[CarPark] [decimal](18,2) NULL,
	[Office] [decimal](18,2) NULL,
	[Warehouse] [decimal](18,2) NULL,
	[YardRental] [decimal](18,2) NULL,
	[Others] [decimal](18,2) NULL,
	[ServiceCharge] [decimal](18,2) NULL,
	-------------------------------------------Other Services
	[Container20Tenant] [decimal](18,2) NULL,
	[Container20NTenant] [decimal](18,2) NULL,
	[Container40Tenant] [decimal](18,2) NULL,
	[Container40NTenant] [decimal](18,2) NULL,
	[ContainerEntryFee] [decimal](18,2) NULL,
	[Reefer20] [decimal](18,2) NULL,
	[Reefer40] [decimal](18,2) NULL,
	[ForkliftHandling] [decimal](18,2) NULL,
	[TransshipmentLCL] [decimal](18,2) NULL,
	[CrossDock20] [decimal](18,2) NULL,
	[CrossDock40] [decimal](18,2) NULL,
	[CrossDockEntryFee] [decimal](18,2) NULL,
	[Security] [decimal](18,2) NULL,
	[LoadUnload20] [decimal](18,2) NULL,
	[LoadUnload40] [decimal](18,2) NULL,
	[StorageHeavy] [decimal](18,2) NULL,
	[Parking20] [decimal](18,2) NULL,
	[Parking40] [decimal](18,2) NULL,
	[StorageGoods] [decimal](18,2) NULL,
	[AccessFee] [decimal](18,2) NULL,
) ON [PRIMARY]
GO

	-------------------------------------------CBY
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Yard]  DEFAULT (0) FOR [Yard]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_GateInGateOut]  DEFAULT (0) FOR [GateInGateOut]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Constructive]  DEFAULT (0) FOR [Constructive]
GO
	-------------------------------------------CBY | Other Services
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Supervision]  DEFAULT (0) FOR [Supervision]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_TransitContainers]  DEFAULT (0) FOR [TransitContainers]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Groupage]  DEFAULT (0) FOR [Groupage]
GO
	-------------------------------------------Rental
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_CarPark]  DEFAULT (0) FOR [CarPark]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Office]  DEFAULT (0) FOR [Office]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Warehouse]  DEFAULT (0) FOR [Warehouse]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_YardRental]  DEFAULT (0) FOR [YardRental]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Others]  DEFAULT (0) FOR [Others]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_ServiceCharge]  DEFAULT (0) FOR [ServiceCharge]
GO
	-------------------------------------------Other Services
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Container20Tenant]  DEFAULT (0) FOR [Container20Tenant]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Container20NTenant]  DEFAULT (0) FOR [Container20NTenant]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Container40Tenant]  DEFAULT (0) FOR [Container40Tenant]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Container40NTenant]  DEFAULT (0) FOR [Container40NTenant]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_ContainerEntryFee]  DEFAULT (0) FOR [ContainerEntryFee]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Reefer20]  DEFAULT (0) FOR [Reefer20]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Reefer40]  DEFAULT (0) FOR [Reefer40]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_ForkliftHandling]  DEFAULT (0) FOR [ForkliftHandling]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_TransshipmentLCL]  DEFAULT (0) FOR [TransshipmentLCL]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_CrossDock20]  DEFAULT (0) FOR [CrossDock20]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_CrossDock40]  DEFAULT (0) FOR [CrossDock40]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_CrossDockEntryFee]  DEFAULT (0) FOR [CrossDockEntryFee]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Security]  DEFAULT (0) FOR [Security]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_LoadUnload20]  DEFAULT (0) FOR [LoadUnload20]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_LoadUnload40]  DEFAULT (0) FOR [LoadUnload40]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_StorageHeavy]  DEFAULT (0) FOR [StorageHeavy]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Parking20]  DEFAULT (0) FOR [Parking20]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_Parking40]  DEFAULT (0) FOR [Parking40]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_StorageGoods]  DEFAULT (0) FOR [StorageGoods]
GO
ALTER TABLE [dbo].[OccupancyReport] ADD  CONSTRAINT [DF_OccupancyReport_AccessFee]  DEFAULT (0) FOR [AccessFee]
GO