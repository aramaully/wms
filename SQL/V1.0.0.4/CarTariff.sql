﻿--Version 1.0.0.4
--START
SP_RENAME '[CarTariff].FreeDaysContainer','FreeDaysContainer_20ft'
ALTER TABLE [CarTariff] ADD  FreeDaysContainer_40ft DECIMAL (18, 2) NULL
UPDATE [CarTariff] SET FreeDaysContainer_40ft=0.00
SELECT * FROM [CarTariff]
--END

