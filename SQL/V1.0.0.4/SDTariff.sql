﻿--Version 1.0.0.4
CREATE TABLE [SDTariff] (
    [TariffID]		VARCHAR (50) NOT NULl,
    [TariffValue]	DECIMAL (18, 2) DEFAULT (0) ,
);

INSERT INTO [SDTariff] VALUES
('SDGateIn', 200),
('SDGateOut', 200),
('SDFullCar', 1500),
('SDHalfCar', 850),
('SDFullDblCar', 3000),
('SDHalfDblCar', 1700),
('SDFullZPC', 800),
('SDHalfZPC', 425)