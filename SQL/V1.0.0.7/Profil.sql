﻿CREATE TABLE Profil (
	ProfilID INTEGER DEFAULT(0),
    Profils VARCHAR(255) NULL
	PRIMARY KEY (ProfilID)
);

CREATE SEQUENCE profil_seq
  AS BIGINT
  START WITH 1
  INCREMENT BY 1
  MINVALUE 1
  MAXVALUE 99999
  NO CYCLE
  CACHE 10;

INSERT INTO Profil (ProfilID, Profils)
VALUES (NEXT VALUE FOR profil_seq, 'Select...'),
(NEXT VALUE FOR profil_seq, 'Marketing'),
(NEXT VALUE FOR profil_seq, 'Operation'),
(NEXT VALUE FOR profil_seq, 'Finance'),
(NEXT VALUE FOR profil_seq, 'Corporate');