﻿--WMS-80
-- Create new table Warehouse Contribution

CREATE SEQUENCE warehouseContribution_seq
  AS BIGINT
  START WITH 1
  INCREMENT BY 1
  MINVALUE 1
  MAXVALUE 99999
  NO CYCLE
  CACHE 10;

CREATE TABLE WarehouseContribution (
	WC_ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	WC_CombineZone INT DEFAULT ((0)),
	WC_Zone INT DEFAULT ((0)),
    WC_Description VARCHAR(50) NULL
);