﻿--Update Tariff Cards
UPDATE TariffCard SET Description ='Storage of 20 ft Containers' WHERE ChCode='FPZ01PRE024'
UPDATE TariffCard SET Description ='Storage of 20 ft Containers - Empty/Full' WHERE ChCode IN ('FPZ06PRE041', 'FPZ01PRE001', 'FPZ06PRE001')
UPDATE TariffCard SET Description ='Storage of 20 ft Containers - Empty/Full (non tenant)' WHERE ChCode IN ('FPZ01STD032', 'FPZ06STD032', 'FPZ06PRE022', 'FPZ01PRE022')
UPDATE TariffCard SET Description ='Storage of 20 ft Containers - Empty/Full (tenant)' WHERE ChCode IN ('FPZ01STD001', 'FPZ06PRE033', 'FPZ06STD001')
UPDATE TariffCard SET Description ='Storage of 20 ft Containers - Empty/Full (tenant) - C/o Afcons - Consignment STC cement' WHERE ChCode IN ('FPZ06PRE085')
UPDATE TariffCard SET Description ='Storage of 40 ft Containers - Empty/Full (tenant)' WHERE ChCode IN ('FPZ06PRE080')
UPDATE TariffCard SET Description ='Storage of 40 ft Containers - Empty/Full' WHERE ChCode IN ('FPZ01PRE002', 'FPZ06PRE002')
UPDATE TariffCard SET Description ='Storage of 40 ft Containers - Empty/Full (non tenant)' WHERE ChCode IN ('FPZ01STD033', 'FPZ06STD033', 'FPZ06PRE023', 'FPZ01PRE023')
UPDATE TariffCard SET Description ='Storage of 40 ft Containers - Empty/Full (tenant)' WHERE ChCode IN ('FPZ01STD002', 'FPZ06PRE034', 'FPZ06STD002')
UPDATE TariffCard SET Description ='Storage of 40 ft Containers -Empty/Full' WHERE ChCode IN ('FPZ06PRE042')

--Update ContDetails
UPDATE ContDetail SET Description='Storage of 40 ft Containers - Empty/Full (tenant)' WHERE Description='Storage of 40ft Containers - Empty/Full (tenant)'       
UPDATE ContDetail SET Description='Storage of 40 ft Containers - Empty/Full (non tenant)' WHERE Description='Storage of 40ft Containers - Empty/Full (non tenants)' 
UPDATE ContDetail SET Description='Storage of 40 ft Containers - Empty/Full (non tenant)' WHERE Description='Storage of 40ft Containers - Empty/Full (non tenant)'
UPDATE ContDetail SET Description='Storage of 40 ft Containers - Empty/Full' WHERE Description='Storage of 40ft Containers - Empty/Full'
UPDATE ContDetail SET Description='Storage of 40 ft Containers' WHERE Description='Storage of 40ft Container'
UPDATE ContDetail SET Description='Storage of 40 ft Containers - Empty/Full (tenant)' WHERE Description='Storage of 40 ft containers - Empty /Full (tenant)'
UPDATE ContDetail SET Description='Storage of 40 ft Containers - Empty/Full' WHERE Description='Storage of 40 ft containers - Empty /Full'
UPDATE ContDetail SET Description='Storage of 20 ft Containers - Empty/Full (tenant)' WHERE Description='Storage of 20ft Containers - Empty/Full (tenant)'
UPDATE ContDetail SET Description='Storage of 20 ft Containers - Empty/Full (non tenant)' WHERE Description='Storage of 20ft Containers - Empty/Full (non tenant)'
UPDATE ContDetail SET Description='Storage of 20 ft Containers - Empty/Full' WHERE Description='Storage of 20ft Containers - Empty/Full'
UPDATE ContDetail SET Description='Storage of 20 ft Containers' WHERE Description='Storage of 20ft Container'
UPDATE ContDetail SET Description='Storage of 20 ft Containers - Empty /Full' WHERE Description='Storage of 20 ft containers - Empty /Full'
UPDATE ContDetail SET Description='Storage of 20 ft Containers - Empty/Full (tenant) - C/o Afcons - Consignment STC cement' WHERE Description='Storage of 20 ft Containers - C/o Afcons - Consignment STC cement'  

--Security Charges
UPDATE TariffCard SET Description='Security Charges during weekdays as from 16:00 to 23:00' WHERE ChCode='FPZ06PRE046'
UPDATE Logistics SET Description='Security Charges during weekdays as from 16:00 to 23:00' WHERE Description='Security charges during weekdays as from 16:00 to 23:00'
UPDATE Logistics SET Description='Security Charges' WHERE Description='Security charges'

--Supervision
UPDATE TariffCard SET Description='Supervision of operational activities during weekdays for any additional after 17:00' WHERE ChCode='FPZ06PRE061'
UPDATE TariffCard SET Description='Supervision of operational activities during weekdays as from 16:00 to 17:00' WHERE ChCode='FPZ06PRE060'
UPDATE Logistics SET Description='Supervision of operational activities during weekdays for any additional after 17pm' WHERE Description='supervision of operational activities during weekdays for any additional after 17pm'
UPDATE Logistics SET Description='Supervision of operational activities during weekdays as from 16pm to 17pm' WHERE Description='supervision of operational activities during weekdays as from 16pm to 17pm'

--Sea Bridge Ltd
Update BoeInProd SET BalQuantity=0, BalSubQuantity=0 where BoeNo='92020196696'