﻿--2021 01 14

--IKS Renew Lease Agreeement
INSERT INTO LeaseAgreement (LeaseNo, ProposalNo, ClientId, LNO) 
VALUES
('hmu/100026/14/Ren4', 'hmu/00038/14', '400C8190', 'hmu/100026/14')
INSERT INTO LeaseAgreement (LeaseNo, ProposalNo, ClientId, LNO) 
VALUES
('hmu/100026/14/Ren2', 'hmu/00038/14', '400C8190', 'hmu/100026/14')

--364 Sq Ft to 34 Sq M
Update Property set Area=34, Unit='Sq M' WHERE Clientid='400C30000' AND Area=364
--4962 => 461 SQM
Update Property set Area=461, Unit='Sq M' WHERE Clientid='400C30000' AND Area=4962

--
UPDATE TariffCard SET Days='Sundays And PHs' WHERE Days='Sundays & PHs'