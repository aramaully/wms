﻿Public Partial Class Expired
    Inherits System.Web.UI.Page
    Dim Con As New OleDb.OleDbConnection
    Dim CmdClient As New OleDb.OleDbCommand
    Dim RdrClient As OleDb.OleDbDataReader
    Dim CmdBoe As New OleDb.OleDbCommand
    Dim RdrBoe As OleDb.OleDbDataReader
    Dim Cmd As New OleDb.OleDbCommand
    Dim Rdr As OleDb.OleDbDataReader
    Dim StrSql As String
    Dim StrSel As String

    Const MENUID = "ExpiredInbound"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn") Then
            Response.Redirect("default.aspx")
        End If
        Con.ConnectionString = Session("ConnString")
        Con.Open()
        Cmd.Connection = Con
        'Check Access
        StrSql = "SELECT COUNT(*) FROM Access WHERE UserId='" & Session("UserID") & "' AND MenuName='" & MENUID & "'"
        Cmd.CommandText = StrSql
        If Cmd.ExecuteScalar = 0 Then
            Response.Redirect("about:blank")
        End If
    End Sub

    Protected Sub ButExit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExit.Click
        Response.Redirect("about:blank")
    End Sub

    Protected Sub ButCloseErr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButCloseErr.Click
        ModalPopupExtender1.Hide()
    End Sub


    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnView.Click
        'StrSel = ""
        'If TxtBoeNo.Text <> "ALL" Then
        '    StrSel = "{BoeIn.BillNo} = '" & TxtBoeNo.Text & "'"
        'Else
        '    If TxtClient.Text <> "ALL" Then
        '        StrSel = "{BoeIn.ClientID} = '" & TxtClient.Text & "'"
        '    End If
        '    If ChkBDate.Checked = True Then
        '        If Len(StrSel) > 0 Then
        '            StrSel = StrSel & " And "
        '        End If
        '        StrSel = StrSel & "{Boein.BillDate} >= Date(" & Format(CDate(TxtBFrom.Text), "yyyy,MM,dd") & ") And "
        '        StrSel = StrSel & "{Boein.BillDate} <= Date(" & Format(CDate(TxtBTo.Text), "yyyy,MM,dd") & ")"
        '    End If
        'End If
        'If Session("PstrZone").ToString <> "ALL" Then
        '    If Len(StrSel) > 0 Then
        '        StrSel = StrSel & " And "
        '    End If
        '    StrSel = StrSel & "{Boein.WhCode} = '" & Session("PstrZone").ToString & "'"
        'End If
        Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB")
        Dim StrSelFormula As String
        StrSelFormula = "{ExpiredInbound.WarehousingDate} <= Date(" & Format(DateAdd("M", -Val(TxtMonth.Text), CDate(TxtDate.Text)), "yyyy,MM,dd") & ")"
        StrSelFormula = StrSelFormula & " And {ExpiredInbound.BalQuantity} = 1"
        If TxtClientID.Text <> "ALL" Then
            StrSelFormula = StrSelFormula & " And {ExpiredInbound.ClientID} = '" & TxtClientID.Text & "'"
        End If
        Session("ReportFile") = "RptExInbound.rpt"
        Session("Period") = "'" & Format(CDate(TxtDate.Text), "dd/MM/yy") & "'"
        Session("Zone") = ""
        Session("SelFormula") = StrSelFormula
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Report", "ShowReport();", True)
        'Response.Redirect("Report.aspx?RptID=Bill Status&RPTFileName=BillStatus.aspx&RptFile=RptBillStatus.rpt&StrSelF=" & StrSel)

    End Sub

    Private Sub TxtClient_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtClientID.TextChanged
        StrSql = "SELECT * FROM Client WHERE ClientID='" & TxtClientID.Text & "'"
        Cmd.Connection = Con
        Cmd.CommandText = StrSql
        Rdr = Cmd.ExecuteReader
        If Rdr.Read Then
            TxtName.Text = Rdr("Name")
        End If
        Rdr.Close()
    End Sub

    Protected Sub BtnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnFind.Click
        Session.Add("PstrCode", TxtClientID.Text)
        ScriptManager.RegisterStartupScript(BtnFind, Me.GetType, "ShowFindBillStatus", "<script language='javascript'> ShowFind();</script>", False)
    End Sub

End Class